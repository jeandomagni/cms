﻿using Cms.Model.Enum;
using Cms.Model.Services;
using Cms.Tests.Fixtures;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace Cms.Tests.Tests
{
    public class AppConfigTester : IClassFixture<StartupFixture>
    {
        private readonly IConnectionStringService _connectionStringService;
        public AppConfigTester(StartupFixture startup) {
            _connectionStringService = startup.Services.GetService<IConnectionStringService>();
        }

        [Fact]
        public void Should_Read_Config()
        {
            Assert.NotEmpty(_connectionStringService.GetVendorConnectionString(Repository.CoreDb));
        }
    }
}
