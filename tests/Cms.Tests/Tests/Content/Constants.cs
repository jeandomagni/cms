﻿using Cms.Domain.Enum;
using CmsContent = Cms.Model.Models.Content.Content;

namespace Cms.Tests.Tests.Content
{
    public static class Constants
    {
        public static CmsContent TestArticle = CreateBaseTestArticle();
        public static CmsContent TestEvent = CreateBaseTestEvent();

        private static CmsContent CreateBaseTestArticle()
        {
            var content = new CmsContent();
            content.Title = "Test Content";
            content.Type = ContentType.Article;
            content.Layout = ContentLayout.ContentImage;
            return content;
        }

        private static CmsContent CreateBaseTestEvent()
        {
            var content = new CmsContent();
            content.Title = "Test Event";
            content.Type = ContentType.Event;
            content.Layout = ContentLayout.ContentImage;
            return content;
        }
    }
}
