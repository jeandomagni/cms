﻿using Cms.Domain.Enum;
using Cms.Domain.Extensions;
using Cms.Domain.Mappers;
using Cms.Domain.Model;
using Cms.Domain.Services.Solr;
using Cms.Model.Repositories.Content;
using Cms.Tests.Constants;
using Cms.Tests.Fixtures;
using Cms.Web.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;
using Xunit;
using CmsContent = Cms.Model.Models.Content.Content;

namespace Cms.Tests.Tests.Content
{
    [Collection(Collections.Constants.StartupCollection)]
    public class ContentServiceTester
    {
        private readonly IContentMappers _mappers;
        private readonly IContentRepository _repo;
        private readonly IContentService _service;
        private readonly IContentEventRepository _eventRepo;
        private readonly ISolrContentService _solrService;
        private readonly AppUser _user;
        public ContentServiceTester(StartupFixture startup) {
            _mappers = startup.Services.GetService<IContentMappers>();
            _repo = startup.Services.GetService<IContentRepository>();
            _service = startup.Services.GetService<IContentService>();
            _eventRepo = startup.Services.GetService<IContentEventRepository>();
            _solrService = startup.Services.GetService<ISolrContentService>();

            _user = UserConstants.TestUser;
        }

        [Fact]
        public async void Should_Create_Content_In_Db()
        {
            var content = await CreateTestContentAsync(Constants.TestArticle);
            Assert.NotNull(await _repo.GetById(content.Id, _user.Email));
            await _service.HardDelete(
                await _mappers.MapToDbContentAsync(content, _user),
                _user
            );
        }

        [Fact]
        public async void Should_Create_Content_In_Solr()
        {
            var content = await CreateTestContentAsync(Constants.TestArticle);
            Assert.NotNull(await _solrService.GetByIdAsync(content.Id));
            await _service.HardDelete(
                await _mappers.MapToDbContentAsync(content, _user),
                _user
            );
        }

        [Fact]
        public async void Should_Delete_Content_In_Db()
        {
            var content = await CreateTestContentAsync(Constants.TestArticle);
            await _service.HardDelete(
                await _mappers.MapToDbContentAsync(content, _user),
                _user
            );
            Assert.Null(await _repo.GetById(content.Id, _user.Email));
        }

        [Fact]
        public async void Should_Delete_Content_In_Solr()
        {
            var content = await CreateTestContentAsync(Constants.TestArticle);
            await _service.HardDelete(
                await _mappers.MapToDbContentAsync(content, _user),
                _user
            );
            Assert.Null(await _solrService.GetByIdAsync(content.Id));
        }

        [Fact]
        public async void Should_Create_Event_In_Db()
        {
            var content = await CreateTestContentAsync(Constants.TestEvent);
            Assert.NotNull(await _repo.GetById(content.Id, _user.Email));
            Assert.NotNull(await _eventRepo.GetForContentByCode(content.Code));
            await _service.HardDelete(
                await _mappers.MapToDbContentAsync(content, _user),
                _user
            );
        }

        [Fact]
        public async void Should_Delete_Event_In_Db()
        {
            var content = await CreateTestContentAsync(Constants.TestEvent);
            await _service.HardDelete(
                await _mappers.MapToDbContentAsync(content, _user),
                _user
            );
            Assert.Null(await _eventRepo.GetForContentByCode(content.Code));
        }

        [Fact]
        public async void Should_Update_Url_When_Segment_Changes()
        {
            var content = await CreateTestContentAsync(Constants.TestArticle);
            content.ArticleUrl.Segment = "my-awesome-custom-url";
            await _service.UpdateAsync(content, _user);

            var updatedDbContent = await _repo.GetById(content.Id, _user.Email);

            await _service.HardDelete(
                await _mappers.MapToDbContentAsync(content, _user),
                _user
            );

            Assert.Contains("my-awesome-custom-url", updatedDbContent.Canonical);
        }

        [Fact]
        public async void Should_Update_Url_When_Topic_Changes()
        {
            const string topicToUpdate = "Tips";

            var content = await CreateTestContentAsync(Constants.TestArticle);
            content.PinnedTopic = topicToUpdate;
            await _service.UpdateAsync(content, _user);

            var updatedDbContent = await _repo.GetById(content.Id, _user.Email);

            await _service.HardDelete(
                await _mappers.MapToDbContentAsync(content, _user),
                _user
            );

            Assert.Contains(topicToUpdate.ToUrlSlug(), updatedDbContent.Canonical);
        }

        [Fact(Skip = "Agreed with Tevin to review as he is more aware of this test")]
        public async void Should_Not_Update_Topic_Url_Segment_If_Published()
        {
            var content = await CreateTestContentAsync(Constants.TestArticle);
            content.ContentHtml = "Test body!";
            var updatedContent = await _service.UpdateAsync(content, _user);

            updatedContent.Status = ContentStatus.Published;
            updatedContent.PublishDate = DateTimeOffset.Now;
            updatedContent.PinnedTopic = Topics.Tips;
            await _service.UpdateAsync(updatedContent, _user);

            var updatedDbContent = await _repo.GetById(content.Id, _user.Email);

            Assert.DoesNotContain(Topics.Tips.ToUrlSlug(), updatedDbContent.Canonical);
            
            await _service.HardDelete(
                await _mappers.MapToDbContentAsync(content, _user),
                _user
            );
        }

        [Fact]
        public async void Should_Update_Topic_Url_Segment_If_Unpublished()
        {
            var content = await CreateTestContentAsync(Constants.TestArticle);
            content.PublishDate = DateTimeOffset.Now;
            content.PinnedTopic = Topics.Tips;
            var updatedContent = await _service.UpdateAsync(content, _user);

            Assert.Contains(Topics.Tips.ToUrlSlug(), updatedContent.ArticleUrl.Full);

            await _service.HardDelete(
                await _mappers.MapToDbContentAsync(content, _user),
                _user
            );
        }

        [Fact]
        public async void Should_Allow_Manual_Modified_Date_Changes()
        {
            var canParseDateString = DateTimeOffset.TryParse("10/1/2015", out var targetModifiedDate);

            Assert.True(canParseDateString);

            var content = await CreateTestContentAsync(Constants.TestArticle);
            content.ModifiedDate = targetModifiedDate;
            var updatedContent = await _service.UpdateAsync(content, _user);

            Assert.Equal(
                targetModifiedDate,
                updatedContent.ModifiedDate);

            await _service.HardDelete(
                await _mappers.MapToDbContentAsync(content, _user),
                _user
            );
        }

        private async Task<CmsContent> CreateTestContentAsync(CmsContent contentToCreate)
        {
            return await _service.CreateAsync(contentToCreate, _user);
        }
    }
}
