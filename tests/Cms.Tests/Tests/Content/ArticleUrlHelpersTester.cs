﻿using Cms.Domain.Helpers;
using Xunit;

namespace Cms.Tests.Tests.Content
{
    public class ArticleUrlHelpersTester
    {
        [Fact]
        public void Should_Extract_Topic_Url_Segment()
        {
            var exampleNewsReleaseUrl = "https://cbbbtest.com/article/news-releases/12345-my-news-release";
            Assert.Equal("news-releases", ArticleUrlHelpers.ExtractTopicUrlSegment(exampleNewsReleaseUrl));

            var exampleTipUrl = "https://cbbbtest.com/article/tips/54321-my-tip";
            Assert.Equal("tips", ArticleUrlHelpers.ExtractTopicUrlSegment(exampleTipUrl));

            var exampleNearMeUrl = "https://cbbbtest.com/near-me/roofing-contractors";
            Assert.Null(ArticleUrlHelpers.ExtractTopicUrlSegment(exampleNearMeUrl));
        }

        [Fact]
        public void Should_Extract_Editable_Url_Segment()
        {
            var exampleNewsReleaseUrl = "https://cbbbtest.com/article/news-releases/12345-my-news-release";
            Assert.Equal("my-news-release", ArticleUrlHelpers.ExtractEditableUrlSegment(exampleNewsReleaseUrl, 12345));

            var exampleTipUrl = "https://cbbbtest.com/article/tips/54321-my-custom-tip";
            Assert.Equal("my-custom-tip", ArticleUrlHelpers.ExtractEditableUrlSegment(exampleTipUrl, 54321));

            var exampleNearMeUrl = "https://cbbbtest.com/near-me/roofing-contractors";
            Assert.Null(ArticleUrlHelpers.ExtractEditableUrlSegment(exampleNearMeUrl, 123));
        }
    }
}
