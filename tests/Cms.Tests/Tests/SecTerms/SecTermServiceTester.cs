﻿using Cms.Domain.Mappers;
using Cms.Model.Constants;
using Cms.Model.Repositories.SecTerms;
using Cms.Tests.Fixtures;
using Cms.Web.Services;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;
using Xunit;

namespace Cms.Tests.Tests.SecTerms
{
    [Collection(Collections.Constants.StartupCollection)]
    public class SecTermServiceTester
    {
        private readonly string TOBID_TO_UPDATE = "30534-200";
        private readonly string EDITABLE_TOBID_TO_UPDATE = "55509-000";
        private readonly string UPDATED_PAGE_TITLE = "New Page Title!";
        private readonly string UPDATED_SPANISH_NAME = "Category name... in Spanish!";

        private readonly ICoreSecTermMappers _coreSecTermMappers;
        private readonly ISecTermRepository _repo;
        private readonly ISecTermService _service;
        public SecTermServiceTester(StartupFixture startup) {
            _coreSecTermMappers = startup.Services.GetService<ICoreSecTermMappers>();
            _repo = startup.Services.GetService<ISecTermRepository>();
            _service = startup.Services.GetService<ISecTermService>();
        }

        [Fact(Skip = "Randomly started failing in Jenkins. Fix later.")]
        public async void Should_Update_Changed_Metadata_In_Db()
        {
            var coreSecTerm = await _repo.GetByTobId(TOBID_TO_UPDATE);
            var secTerm = _coreSecTermMappers.MapToSecTerm(coreSecTerm);

            Assert.NotNull(secTerm);

            var originalMxStatePageTitle = secTerm.Metadata
                .Where(a => a.CultureIds.Contains("es-MX") && a.Type == MetadataConstants.StateKey)
                .FirstOrDefault()
                .PageTitle;

            foreach (var metadata in secTerm.Metadata
                .Where(a => a.CultureIds.Contains("es-MX") && a.Type == MetadataConstants.StateKey))
            {
                metadata.PageTitle = UPDATED_PAGE_TITLE;
            }

            await _service.Update(secTerm);

            var updatedCoreSecTerm = await _repo.GetByTobId(TOBID_TO_UPDATE);
            var updatedSecTerm = _coreSecTermMappers.MapToSecTerm(updatedCoreSecTerm);

            var updatedMxStatePageTitle = updatedSecTerm.Metadata
                .Where(a => a.CultureIds.Contains("es-MX") && a.Type == MetadataConstants.StateKey)
                .FirstOrDefault()
                .PageTitle;

            Assert.Equal(UPDATED_PAGE_TITLE, updatedMxStatePageTitle);

            foreach (var metadata in secTerm.Metadata
                .Where(a => a.CultureIds.Contains("es-MX") && a.Type == MetadataConstants.StateKey))
            {
                metadata.PageTitle = originalMxStatePageTitle;
            }

            var restoredSecTerm = await _service.Update(secTerm);

            var restoredMxStatePageTitle = restoredSecTerm.Metadata
                .Where(a => a.CultureIds.Contains("es-MX") && a.Type == MetadataConstants.StateKey)
                .FirstOrDefault()
                .PageTitle;

            Assert.Equal(originalMxStatePageTitle, restoredMxStatePageTitle);
        }

        [Fact(Skip = "Replace with editable term")]
        public async void If_Editable_Should_Update_Changed_Spanish_Name_In_Db()
        {
            var coreSecTerm = await _repo.GetByTobId(EDITABLE_TOBID_TO_UPDATE);
            var secTerm = _coreSecTermMappers.MapToSecTerm(coreSecTerm);

            Assert.NotNull(secTerm);
            Assert.True(secTerm.IsEditable);

            var originalSpanishName = secTerm.SpanishName;
            secTerm.SpanishName = UPDATED_SPANISH_NAME;

            await _service.Update(secTerm);

            var updatedCoreSecTerm = await _repo.GetByTobId(EDITABLE_TOBID_TO_UPDATE);
            var updatedSecTerm = _coreSecTermMappers.MapToSecTerm(updatedCoreSecTerm);

            Assert.Equal(UPDATED_SPANISH_NAME, updatedSecTerm.SpanishName);

            updatedSecTerm.SpanishName = originalSpanishName;
            await _service.Update(updatedSecTerm);

            var restoredCoreSecTerm = await _repo.GetByTobId(EDITABLE_TOBID_TO_UPDATE);
            var restoredSecTerm = _coreSecTermMappers.MapToSecTerm(restoredCoreSecTerm);

            Assert.Equal(originalSpanishName, restoredSecTerm.SpanishName);
        }
    }
}
