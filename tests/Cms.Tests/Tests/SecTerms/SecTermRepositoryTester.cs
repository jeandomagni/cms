﻿using Cms.Domain.Mappers;
using Cms.Model.Repositories.SecTerms;
using Cms.Tests.Fixtures;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace Cms.Tests.Tests.SecTerms
{
    [Collection(Collections.Constants.StartupCollection)]
    public class SecTermRepositoryTester
    {
        private readonly ICoreSecTermMappers _coreSecTermMappers;
        private readonly ISecTermRepository _secTermRepository;
        public SecTermRepositoryTester(StartupFixture startup) {
            _coreSecTermMappers = startup.Services.GetService<ICoreSecTermMappers>();
            _secTermRepository = startup.Services.GetService<ISecTermRepository>();
        }

        [Fact]
        public async void Should_Get_Metadata_For_TobId()
        {
            var coreSecTerm = await _secTermRepository.GetByTobId("60075-000");
            var secTerm = _coreSecTermMappers.MapToSecTerm(coreSecTerm);
            Assert.NotNull(secTerm?.Metadata);
            Assert.NotNull(secTerm?.MetadataBusiness);
        }
    }
}
