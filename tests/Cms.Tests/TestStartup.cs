using Cms.Web;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace Cms.Tests
{
    public class TestStartup : Startup
    {
        public TestStartup(IHostingEnvironment env) : base(env) { }
        
        public void ConfigureTestServices(IServiceCollection services) {
            ConfigureServices(services);
        }

        protected override void RegisterSolrSearch(string solrBaseUrl)
        {
            
        }
    }
}
