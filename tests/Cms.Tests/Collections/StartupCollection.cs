﻿using Cms.Tests.Fixtures;
using Xunit;

namespace Cms.Tests.Collections
{
    [CollectionDefinition(Constants.StartupCollection)]
    public class StartupCollection : ICollectionFixture<StartupFixture>
    {
        // This class has no code, and is never created. Its purpose is simply
        // to be the place to apply [CollectionDefinition] and all the
        // ICollectionFixture<> interfaces.
    }
}
