﻿using Cms.Model.Models.User;
using Cms.Model.Repositories;
using Cms.Tests.Constants;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;

namespace Cms.Tests.Fixtures
{
    public class StartupFixture : IDisposable
    {
        private readonly IUserProfileRepository _userProfileRepository;
        public StartupFixture()
        {
            var host = new WebHostBuilder().UseStartup<TestStartup>().Build();
            Services = host.Services;

            _userProfileRepository = Services.GetService<IUserProfileRepository>();
            Task.Run(async () => await SetupTestUserAsync());
        }

        public void Dispose()
        { 
        }

        public IServiceProvider Services { get; private set; }

        private async Task SetupTestUserAsync()
        {
            var userProfile = await _userProfileRepository.GetForUser(UserConstants.TestUser.UserId);

            if (userProfile == null)
            {
                var profileToAdd = new UserProfile();
                profileToAdd.UserId = UserConstants.TestUser.UserId;
                profileToAdd.CreatedBy = UserConstants.TestUser.Email;
                profileToAdd.DefaultSite = UserConstants.TestUser.DefaultSite;
                profileToAdd.OpenContentInNewTab = false;
                await _userProfileRepository.Add(profileToAdd);
            }
        }
    }
}
