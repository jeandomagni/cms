﻿using Cms.Domain.Model;
using Cms.Model.Models.User;
using System;
using System.Collections.Generic;

namespace Cms.Tests.Constants
{
    public static class UserConstants
    {
        /*
        new UserProfile {
                UserId = user.UserId.ToString(),
                CreatedBy = user.Email,
                DefaultSite = defaultSite,
                OpenContentInNewTab = false,
            }
            */

        public static AppUser TestUser = CreateTestUser();

        private static AppUser CreateTestUser()
        {
            var user = new AppUser();
            user.DefaultSite = "0021";
            user.Email = "xunit@council.bbb.org";
            user.Iat = DateTime.Now.ToString();
            user.UserId = "xunit@council.bbb.org";
            user.Roles = new List<UserRole>()
            {
                new UserRole()
                {
                    Email = "xunit@council.bbb.org",
                    RoleName = "episerveradmin",
                    SiteId = "0021",
                },
            };
            return user;
        }
    }
}
