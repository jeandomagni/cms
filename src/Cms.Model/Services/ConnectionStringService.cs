﻿using System;
using Cms.Model.Enum;
using Cms.Model.Models.Config;
using Flurl;
using Microsoft.Extensions.Options;

namespace Cms.Model.Services
{
    public interface IConnectionStringService
    {
        string GetVendorConnectionString(Repository vendor);
    }

    public class ConnectionStringService : IConnectionStringService
    {
        private readonly ConnectionStrings _config;
        public ConnectionStringService(
            IOptions<ConnectionStrings> config) {
            _config = config.Value;
        }

        public string GetVendorConnectionString(Repository vendor) {
            if (_config == null)  {
                return null;
            }

            switch (vendor) {
                case Repository.CoreDb:
                    return _config.CoreDb;
                case Repository.ApiCoreDb:
                    return _config.ApiCoreDb;
                case Repository.CoreCmsDb:
                    return _config.CoreCmsDb;
                case Repository.SolrContentCore:
                    return Url.Combine(_config.SolrDb, "Article");
                case Repository.SolrLocationCore:
                    return Url.Combine(_config.SolrDb, "locations");
                case Repository.SolrBbbInfoCore:
                    return Url.Combine(_config.SolrDb, "BbbInfo");
                case Repository.SolrBranchCore:
                    return Url.Combine(_config.SolrDb, "branch");
                case Repository.SolrCategoryCore:
                    return Url.Combine(_config.SolrDb, "searchtypeahead");
                case Repository.PheonixApi:
                    return _config.PheonixApi;
                case Repository.BbbInfoApi:
                    return _config.BbbInfoApiUrl;
                case Repository.LeadsApiUrl:
                    return _config.LeadsApiUrl;
                case Repository.ArticleOrderSummaryApi:
                    return _config.ArticleOrderSummaryApiUrl;
                case Repository.ArticleTypeToListingTypeMappingApi:
                    return _config.ArticleTypeToListingTypeMappingApiUrl;
                default:
                    throw new ArgumentOutOfRangeException(
                        nameof(vendor), vendor, null);
            }
        }
    }
}
