﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cms.Model.Contract;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SqlKata;

namespace Cms.Model.Helpers
{
    public class QueryBuilderHelper
    {
        public Dictionary<string, object> ConvertToDictionary<T>(T obj, IEnumerable<string> exclusionList = null)
        {
            var dctParams = JsonConvert.DeserializeObject<Dictionary<string, object>>(
                JsonConvert.SerializeObject(obj, new JsonSerializerSettings
                {
                    ContractResolver = new DefaultContractResolver(),
                    NullValueHandling = NullValueHandling.Ignore
                }));
            if (typeof(IPagingSettings).IsAssignableFrom(typeof(T)))
            {
                var props = typeof(IPagingSettings).GetProperties().Select(c => c.Name);

                foreach (var prop in props)
                {
                    if (dctParams.ContainsKey(prop))
                    {
                        dctParams = dctParams.RemoveKey(prop);
                    }
                }
            }

            if (exclusionList == null) return dctParams;
            foreach (var exc in exclusionList)
            {
                if (dctParams.ContainsKey(exc))
                {
                    dctParams.RemoveKey(exc);
                }
            }
            return dctParams;
        }
        public static bool IsNullableType(Type type)
        {
            if (!type.IsGenericType)
            {
                return false;
            }
            return type.GetGenericTypeDefinition() == typeof(Nullable<>);
        }
        public Dictionary<string, Type> GetObjectPropertyNameAndTypes<T>()
        {
            var ret = new Dictionary<string, Type>();
            var props = typeof(T).GetProperties();
            foreach (var propertyInfo in props)
            {
                if (!IsNullableType(propertyInfo.PropertyType))
                {
                    ret.Add(propertyInfo.Name, propertyInfo.PropertyType);
                    continue;
                }
                ret.Add(propertyInfo.Name, Nullable.GetUnderlyingType(propertyInfo.PropertyType));
            }
            return ret;
        }

        public static bool IsString(Type o)
        {
            return Type.GetTypeCode(o) == TypeCode.String;
        }

        public static bool IsBoolean(Type o)
        {
            return Type.GetTypeCode(o) == TypeCode.Boolean;
        }

        public static bool IsDate(Type o)
        {
            return Type.GetTypeCode(o) == TypeCode.DateTime;
        }

        public static bool IsNumericType(Type o)
        {
            switch (Type.GetTypeCode(o))
            {
                case TypeCode.Byte:
                case TypeCode.SByte:
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                case TypeCode.UInt64:
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                case TypeCode.Decimal:
                case TypeCode.Double:
                case TypeCode.Single:
                    return true;
                default:
                    return false;
            }
        }

        private Query buildQueryForNumeric(Query during, string propertyName, object propertyValue,
            string op)
        {
            switch (op.ToLower())
            {
                case ">":
                    during = during.OrWhere($"{propertyName}", ">", propertyValue);
                    break;
                case ">=":
                    during = during.OrWhere($"{propertyName}", ">=", propertyValue);
                    break;
                case "<":
                    during = during.OrWhere($"{propertyName}", "<", propertyValue);
                    break;
                case "<=":
                    during = during.OrWhere($"{propertyName}", "<=", propertyValue);
                    break;
                case "!=":
                    during = during.OrWhereNot($"{propertyName}", propertyValue);
                    break;
                default:
                    during = during.OrWhere($"{propertyName}", propertyValue);
                    break;
            }
            return during;
        }

        private Query buildQueryForDate(Query during, string propertyName, object propertyValue,
            string op)
        {
            switch (op.ToLower())
            {
                case ">":
                    during = during.OrWhere($"{propertyName}", ">", propertyValue);
                    break;
                case ">=":
                    during = during.OrWhere($"{propertyName}", ">=", propertyValue);
                    break;
                case "<":
                    during = during.OrWhere($"{propertyName}", "<", propertyValue);
                    break;
                case "<=":
                    during = during.OrWhere($"{propertyName}", "<=", propertyValue);
                    break;
                case "!=":
                    during = during.OrWhereNot($"{propertyName}", propertyValue);
                    break;
                default:
                    during = during.OrWhere($"{propertyName}", propertyValue);
                    break;
            }
            return during;
        }

        private Query buildQueryForBoolean(Query during, string propertyName, object propertyValue,
            string op)
        {
            switch (op.ToLower())
            {
                case "!=":
                    during = during.OrWhereNot($"{propertyName}", Convert.ToBoolean(propertyValue) ? 1 : 0);
                    break;
                default:
                    during = during.OrWhere($"{propertyName}", Convert.ToBoolean(propertyValue) ? 1 : 0);
                    break;
            }
            return during;
        }

        private Query buildQueryForString(Query during, string propertyName, object propertyValue,
            string op)
        {
            switch (op.ToLower())
            {
                case "contains":
                    during = during.OrWhereLike($"{propertyName}", $"{propertyValue}");
                    break;
                case "!contains":
                    during = during.OrWhereNotContains($"{propertyName}", $"{propertyValue}");
                    break;
                case "endswith":
                    during = during.OrWhereEnds($"{propertyName}", $"{propertyValue}");
                    break;
                case "!endswith":
                    during = during.OrWhereNotEnds($"{propertyName}", $"{propertyValue}");
                    break;
                case "startswith":
                    during = during.OrWhereStarts($"{propertyName}", $"{propertyValue}");
                    break;
                case "!startswith":
                    during = during.OrWhereNotStarts($"{propertyName}", $"{propertyValue}");
                    break;
                case "!equal":
                case "!=":
                    during = during.OrWhereNot($"{propertyName}", $"{propertyValue}");
                    break;
                case "equal":
                case "=":
                    during = during.OrWhere($"{propertyName}", $"{propertyValue}");
                    break;
                default:
                    during = during.OrWhereContains($"{propertyName}", $"{propertyValue}");
                    break;
            }
            return during;
        }


        public Query BuildQuery<T>(Query builder, Dictionary<string, object> dictionary, Dictionary<string, string> fieldOperators = null)
        {
            var typeProperties = GetObjectPropertyNameAndTypes<T>();
            if (!dictionary.Keys.Any())
            {
                return builder;
            }

            var dctOps = new Dictionary<string, string>();
            if (fieldOperators != null)
            {
                dctOps = fieldOperators;
            }
            builder.Where(during =>
            {
                foreach (var dictionaryKey in dictionary.Keys)
                {
                    if (dictionary[dictionaryKey] == null)
                    {
                        continue;
                    }
                    var propertyType = typeProperties[dictionaryKey];
                    var op = string.Empty;
                    if (dctOps.ContainsKey(dictionaryKey))
                    {
                        op = dctOps[dictionaryKey];
                    }
                    if (IsNumericType(propertyType))
                    {
                        during = buildQueryForNumeric(during, dictionaryKey, dictionary[dictionaryKey], op);
                        continue;
                    }
                    if (IsBoolean(propertyType))
                    {
                        during = buildQueryForBoolean(during, dictionaryKey, dictionary[dictionaryKey], op);
                        continue;
                    }
                    if (IsDate(propertyType))
                    {
                        during = buildQueryForDate(during, dictionaryKey, dictionary[dictionaryKey], op);
                        continue;
                    }
                    during = buildQueryForString(during, dictionaryKey, dictionary[dictionaryKey], op);
                }
                return during;
            });
            return builder;
        }


    }
}