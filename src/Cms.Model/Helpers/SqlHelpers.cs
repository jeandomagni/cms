﻿using Newtonsoft.Json;

namespace Cms.Model.Helpers
{
    public class SqlHelpers
    {
        public static string GetXml(object fields)
        {
            var doc =
                JsonConvert.DeserializeXmlNode(
                    "{\"Row\":" + JsonConvert.SerializeObject(fields) + "}", "root");
            return doc.OuterXml;
        }

        public static string SanitizeString(string str)
        {
            if (string.IsNullOrWhiteSpace(str))
            {
                return str;
            }
            return str.Replace("'", "''");
        }
    }
}
