﻿using System.Collections.Generic;

namespace Cms.Model.Helpers
{
    public static class DictionaryExtensions
    {
        public static Dictionary<TKey, TValue> RemoveKey<TKey, TValue>(this Dictionary<TKey, TValue> map, TKey key)
        {
            map?.Remove(key);
            return map;
        }
    }
}
