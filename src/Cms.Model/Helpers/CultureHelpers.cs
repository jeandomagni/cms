﻿using Bbb.Core.Solr.Enums;

namespace Cms.Domain.Helpers
{
    public static class CultureHelpers
    {

        public static string GetThreeLetterCountry(string country)
        {
            var c = country?.ToLower();
            switch (c)
            {
                case "us":
                    return CountryCode.USA.ToString();
                case "ca":
                    return CountryCode.CAN.ToString();
                case "mx":
                    return CountryCode.MEX.ToString();
                default:
                    return null;
            }
        }
    }
}
