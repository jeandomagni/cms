﻿
namespace Cms.Model.Dto.UserManagement
{
    public class UserRolesDto
    {
        public string Email { get; set; }
        public string RoleName { get; set; }
        public string SiteId { get; set; }
        public string UserId { get; set; }
        public string RoleId { get; set; }
        public string BBBName { get; set; }
    }

}
