﻿using System.Collections.Generic;

namespace Cms.Model.Dto.UserManagement
{
    public class ServiceResponseDto<T> : ServiceResponseDto
    {
        public List<T> Items { get; set; } = new List<T>();
    }

    public class ServiceResponseDto
    {
        public bool Success { get; set; }
        public List<string> ErrorMessages { get; set; } = new List<string>();
        public List<string> WarningMessages { get; set; } = new List<string>();
    }
}