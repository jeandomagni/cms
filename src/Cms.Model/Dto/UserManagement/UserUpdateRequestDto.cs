﻿namespace Cms.Model.Dto.UserManagement
{
    public partial class UserUpdateRequestDto
    {
        public System.Guid? UserId { get; set; }
        public string BBBId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
    }
}
