﻿namespace Cms.Model.Dto.UserManagement
{
    public partial class UserStatusUpdateRequestDto
    {

        public System.Guid? UserId { get; set; }
        public string Email { get; set; }
        public string Status { get; set; }
        public bool? EmailPassword { get; set; }
    }
}