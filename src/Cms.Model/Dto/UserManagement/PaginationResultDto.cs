﻿using System.Collections.Generic;

namespace Cms.Model.Dto.UserManagement
{
    public class PaginationResultDto<T> 
    {
        public IEnumerable<T> Items { get; set; }
        public long TotalRecords { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public int TotalPages { get; set; }
        public bool IsFirst { get; set; }
        public bool IsLast { get; set; }
        public bool HasNext { get; set; }
        public bool HasPrevious { get; set; }
    }
}
