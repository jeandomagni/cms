﻿
namespace Cms.Model.Dto.UserManagement
{
    public class RolesDto
    {
        public System.Guid? RoleId { get; set; }
        public string RoleName { get; set; }
        public string LoweredRoleName { get; set; }
        public string Description { get; set; }
    }
}