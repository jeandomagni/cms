﻿using System.Collections.Generic;

namespace Cms.Model.Dto.UserManagement
{
    public partial class AddUpdateUserRequestDto
    {
        public System.Guid? UserId { get; set; }
        public string SiteId { get; set; }
        public string BBBId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public List<RolesDto> Roles { get; set; }
        public bool? EmailPassword { get; set; }
    }

  
}
