﻿
namespace Cms.Model.Dto.UserManagement
{
    public class SiteContentDto
    {
        public int? Id { get; set; }
        public string SiteId { get; set; }
        public string ContentId { get; set; }
    }
}