﻿using System.Collections.Generic;

namespace Cms.Model.Dto.UserManagement
{
    public partial class UserMembershipInfoDto
    {
        public System.Guid UserId { get; set; }
        public string BBBId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public int mId { get; set; }
        public string SiteId { get; set; }
        public string Status { get; set; }
        public System.DateTime? LastLoginDate { get; set; }
        public System.DateTime? LastPasswordChangedDate { get; set; }
        public System.DateTime? LastLockoutDate { get; set; }
        public List<UserRolesDto> Roles { get; set; }

    }
}