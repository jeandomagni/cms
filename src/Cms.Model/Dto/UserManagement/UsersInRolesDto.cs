﻿
namespace Cms.Model.Dto.UserManagement
{
    public class UsersInRolesDto
    {
        public System.Guid? UserId { get; set; }
        public System.Guid? RoleId { get; set; }
    }
}