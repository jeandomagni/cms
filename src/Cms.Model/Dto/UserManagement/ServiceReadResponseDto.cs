﻿namespace Cms.Model.Dto.UserManagement
{
    public class ServiceReadResponseDto<T> : ServiceResponseDto
    {
        public T Data { get; set; }
    }
}