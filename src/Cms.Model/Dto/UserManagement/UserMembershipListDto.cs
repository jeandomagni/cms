﻿using Cms.Model.Contract;

namespace Cms.Model.Dto.UserManagement
{
    public partial class UserMembershipListDto : IPagingSettings
    {
        public System.Guid? UserId { get; set; }
        public string BBBId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public int? mId { get; set; }
        public string SiteId { get; set; }
        public string Status { get; set; }
        public System.DateTime? LastLoginDate { get; set; }
        public System.DateTime? LastPasswordChangedDate { get; set; }
        public System.DateTime? LastLockoutDate { get; set; }
        public int? FailedPasswordAttemptCount { get; set; }
        public System.DateTime? FailedPasswordAttemptStart { get; set; }
        public string Comment { get; set; }
        public int? Page { get; set; } = 1;
        public int? PageSize { get; set; } = 10;
        public string SortColumn { get; set; }
        public string Direction { get; set; } = "ASC";
    }

    public partial class UserMembershipSearchDto : IPagingSettings
    {
        public string Search { get; set; }
        public int? Page { get; set; } = 1;
        public int? PageSize { get; set; } = 10;
        public string SortColumn { get; set; }
        public string Direction { get; set; } = "ASC";
    }
}