﻿namespace Cms.Model.Dto.UserManagement
{
    public partial class UserPasswordUpdateRequestDto
    {
        public System.Guid? UserId { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string NewPassword { get; set; }
        public bool? EmailPassword { get; set; }
        public string  VerifyPassword { get; set; }
    }
}