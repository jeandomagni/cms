﻿using System;

namespace Cms.Model.Attributes
{
    public class CompressedAttribute : Attribute
    {
        public CompressedAttribute() { }
    }
}
