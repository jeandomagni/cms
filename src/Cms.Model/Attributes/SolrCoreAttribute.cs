﻿using Cms.Model.Enum;
using System;

namespace Cms.Model.Attributes
{
    public class SolrCoreAttribute : Attribute
    {
        public SolrCoreAttribute(String coreName)
        {
            CoreName = coreName;
        }
        public SolrCoreAttribute(SolrCore core)
        {
            Core = core;
        }

        public SolrCore Core { get; private set; }

        public string CoreName { get; private set; }

        public string GetCoreName()
        {
            return !String.IsNullOrEmpty(CoreName) ? CoreName : Core.ToRelativeName();
        }
    }
}
