﻿using System;

namespace Cms.Model.Attributes
{
    [AttributeUsage(AttributeTargets.All)]
    public class ActionAttribute : Attribute
    {
        private string value;

        public ActionAttribute(string value)
        {
            this.value = value;
        }

        public virtual string Value
        {
            get { return value; }
        }
    }
}
