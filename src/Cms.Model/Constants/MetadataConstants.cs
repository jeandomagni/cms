﻿using Cms.Model.Models.SecTerm;
using System.Collections.Generic;

namespace Cms.Model.Constants
{
    public static class MetadataConstants
    {
        public const string AccreditedCountryKey = "accreditedCountry";
        public const string CountryKey = "country";
        public const string AccreditedStateKey = "accreditedState";
        public const string StateKey = "state";
        public const string AccreditedCityKey = "accreditedCity";
        public const string CityKey = "city";

        public static Metadata AccreditedCountryMetadataEn = new Metadata()
        {
            Type = AccreditedCountryKey,
            PageTitle = SecTermConstants.DefaultAccreditedCountryPageTitleEn,
            MetaDesc = SecTermConstants.DefaultAccreditedCountryMetaDescriptionEn,
            CultureIds = CultureConstants.SupportedEnCultures,
        };

        public static Metadata AccreditedCountryMetadataEs = new Metadata()
        {
            Type = AccreditedCountryKey,
            PageTitle = SecTermConstants.DefaultAccreditedCountryPageTitleEs,
            MetaDesc = SecTermConstants.DefaultAccreditedCountryMetaDescriptionEs,
            CultureIds = CultureConstants.SupportedEsCultures,
        };

        public static Metadata CountryMetadataEn = new Metadata()
        {
            Type = CountryKey,
            PageTitle = SecTermConstants.DefaultCountryPageTitleEn,
            MetaDesc = SecTermConstants.DefaultCountryMetaDescriptionEn,
            CultureIds = CultureConstants.SupportedEnCultures,
        };

        public static Metadata CountryMetadataEs = new Metadata()
        {
            Type = CountryKey,
            PageTitle = SecTermConstants.DefaultCountryPageTitleEs,
            MetaDesc = SecTermConstants.DefaultCountryMetaDescriptionEs,
            CultureIds = CultureConstants.SupportedEsCultures,
        };

        public static Metadata AccreditedStateMetadataEn = new Metadata()
        {
            Type = AccreditedStateKey,
            PageTitle = SecTermConstants.DefaultAccreditedStatePageTitleEn,
            MetaDesc = SecTermConstants.DefaultAccreditedStateMetaDescriptionEn,
            CultureIds = CultureConstants.SupportedEnCultures,
        };

        public static Metadata AccreditedStateMetadataEs = new Metadata()
        {
            Type = AccreditedStateKey,
            PageTitle = SecTermConstants.DefaultAccreditedStatePageTitleEs,
            MetaDesc = SecTermConstants.DefaultAccreditedStateMetaDescriptionEs,
            CultureIds = CultureConstants.SupportedEsCultures,
        };

        public static Metadata StateMetadataEn = new Metadata()
        {
            Type = StateKey,
            PageTitle = SecTermConstants.DefaultStatePageTitleEn,
            MetaDesc = SecTermConstants.DefaultStateMetaDescriptionEn,
            CultureIds = CultureConstants.SupportedEnCultures,
        };

        public static Metadata StateMetadataEs = new Metadata()
        {
            Type = StateKey,
            PageTitle = SecTermConstants.DefaultStatePageTitleEs,
            MetaDesc = SecTermConstants.DefaultStateMetaDescriptionEs,
            CultureIds = CultureConstants.SupportedEsCultures,
        };

        public static Metadata AccreditedCityMetadataEn = new Metadata()
        {
            Type = AccreditedCityKey,
            PageTitle = SecTermConstants.DefaultAccreditedCityPageTitleEn,
            MetaDesc = SecTermConstants.DefaultAccreditedCityMetaDescriptionEn,
            CultureIds = CultureConstants.SupportedEnCultures,
        };

        public static Metadata AccreditedCityMetadataEs = new Metadata()
        {
            Type = AccreditedCityKey,
            PageTitle = SecTermConstants.DefaultAccreditedCityPageTitleEs,
            MetaDesc = SecTermConstants.DefaultAccreditedCityMetaDescriptionEs,
            CultureIds = CultureConstants.SupportedEsCultures,
        };

        public static Metadata CityMetadataEn = new Metadata()
        {
            Type = CityKey,
            PageTitle = SecTermConstants.DefaultCityPageTitleEn,
            MetaDesc = SecTermConstants.DefaultCityMetaDescriptionEn,
            CultureIds = CultureConstants.SupportedEnCultures,
        };

        public static Metadata CityMetadataEs = new Metadata()
        {
            Type = CityKey,
            PageTitle = SecTermConstants.DefaultCityPageTitleEs,
            MetaDesc = SecTermConstants.DefaultCityMetaDescriptionEs,
            CultureIds = CultureConstants.SupportedEsCultures,
        };
        
        public static List<Metadata> DefaultMetadata = new List<Metadata>()
        {
            AccreditedCountryMetadataEn,
            AccreditedCountryMetadataEs,
            CountryMetadataEn,
            CountryMetadataEs,
            AccreditedStateMetadataEn,
            AccreditedStateMetadataEs,
            StateMetadataEn,
            StateMetadataEs,
            AccreditedCityMetadataEn,
            AccreditedCityMetadataEs,
            CityMetadataEn,
            CityMetadataEs,
        };
    }
}
