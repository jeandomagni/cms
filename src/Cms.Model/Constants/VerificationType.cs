﻿namespace Cms.Model.Constants
{
    public class VerificationType
    {
        public const string EMAIL = "email";
        public const string PHONE = "phone";
    }
}
