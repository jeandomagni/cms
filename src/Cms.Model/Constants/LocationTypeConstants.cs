﻿using System.Linq;
using System.Reflection;

namespace Cms.Model.Constants
{
    public static class LocationTypeConstants
    {
        public const string Country = "Country";
        public const string StateProvince = "StateProvince";
        public const string City = "City";
    }
}
