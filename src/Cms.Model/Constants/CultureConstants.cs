﻿using System.Collections.Generic;

namespace Cms.Model.Constants
{
    public static class CultureConstants
    {
        public const string EnUsCultureId = "en-US";
        public const string EnCaCultureId = "en-CA";
        public const string EsMxCultureId = "es-MX";

        public static List<string> SupportedEnCultures = new List<string>() { EnUsCultureId, EnCaCultureId };
        public static List<string> SupportedEsCultures = new List<string>() { EsMxCultureId };
    }
}
