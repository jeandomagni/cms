﻿namespace Cms.Model.Constants
{
    public class SecTermConstants
    {
        public const string SolrContentType = "SecTerm";

        #region Country Metadata
        public const string DefaultAccreditedCountryPageTitleEn = "BBB Accredited {category} in {country} | Better Business Bureau. Start with Trust ®";
        public const string DefaultAccreditedCountryMetaDescriptionEn = "BBB Accredited {category} in {country}. BBB Start with Trust ®. Your guide to trusted BBB Ratings, customer reviews and BBB Accredited businesses.";

        public const string DefaultAccreditedCountryPageTitleEs = "{category} con acreditación del BBB en {country} | Better Business Bureau. Start with Trust ®";
        public const string DefaultAccreditedCountryMetaDescriptionEs = "{category} con acreditación del BBB en {country} | Better Business Bureau. Start with Trust ®. Su guía para las calificaciones de confianza de BBB, las reseñas de clientes y las empresas acreditadas por BBB.";

        public const string DefaultCountryPageTitleEn = "{category} in {country} | Better Business Bureau. Start with Trust ®";
        public const string DefaultCountryMetaDescriptionEn = "BBB Directory of {category} in {country}. BBB Start with Trust ®. Your guide to trusted BBB Ratings, customer reviews and BBB Accredited businesses.";
        
        public const string DefaultCountryPageTitleEs = "{categoría} en {país} | Better Business Bureau. Start with Trust ®";
        public const string DefaultCountryMetaDescriptionEs = "Directorio BBB de {categoría} en {país}. BBB Start with Trust ®. Su guía para calificaciones de confianza BBB, comentarios de clientes y negocios acreditados BBB.";
        #endregion

        #region State Metadata
        public const string DefaultAccreditedStatePageTitleEn = "BBB Accredited {category} in {state} | Better Business Bureau. Start with Trust ®";
        public const string DefaultAccreditedStateMetaDescriptionEn = "BBB Accredited {category} in {state}. BBB Start with Trust ®. Your guide to trusted BBB Ratings, customer reviews and BBB Accredited businesses.";

        public const string DefaultAccreditedStatePageTitleEs = "{category} con acreditación del BBB en {state} | Better Business Bureau. Start with Trust ®";
        public const string DefaultAccreditedStateMetaDescriptionEs = "{category} con acreditación del BBB en {state} | Better Business Bureau. Start with Trust ®. Su guía para las calificaciones de confianza de BBB, las reseñas de clientes y las empresas acreditadas por BBB.";

        public const string DefaultStatePageTitleEn = "{category} in {state} | Better Business Bureau. Start with Trust ®";
        public const string DefaultStateMetaDescriptionEn = "BBB Directory of {category} in {state}. BBB Start with Trust ®. Your guide to trusted BBB Ratings, customer reviews and BBB Accredited businesses.";

        public const string DefaultStatePageTitleEs = "{categoría} en {estado} | Better Business Bureau. Start with Trust ®";
        public const string DefaultStateMetaDescriptionEs = "Directorio BBB de {categoría} en {estado}. BBB Start with Trust ®. Su guía para calificaciones de confianza BBB, comentarios de clientes y negocios acreditados BBB.";
        #endregion

        #region City Metadata
        public const string DefaultAccreditedCityPageTitleEn = "BBB Accredited {category} near {city}, {state} | Better Business Bureau. Start with Trust ®";
        public const string DefaultAccreditedCityMetaDescriptionEn = "BBB Accredited {category} near {city}, {state}. BBB Start with Trust ®. Your guide to trusted BBB Ratings, customer reviews and BBB Accredited businesses.";

        public const string DefaultAccreditedCityPageTitleEs = "{category} con acreditación del BBB cerca de {city}, {state} | Better Business Bureau. Start with Trust ®";
        public const string DefaultAccreditedCityMetaDescriptionEs = "{category} con acreditación del BBB cerca de {city}, {state} | Better Business Bureau. Start with Trust ®. Su guía para las calificaciones de confianza de BBB, las reseñas de clientes y las empresas acreditadas por BBB.";

        public const string DefaultCityPageTitleEn = "{category} near {city}, {state} | Better Business Bureau. Start with Trust ®";
        public const string DefaultCityMetaDescriptionEn = "BBB Directory of {category} near {city}, {state}. BBB Start with Trust ®. Your guide to trusted BBB Ratings, customer reviews and BBB Accredited businesses.";
        
        public const string DefaultCityPageTitleEs = "{categoría} ordenado {ciudad}, {estado} | Better Business Bureau. Start with Trust ®";
        public const string DefaultCityMetaDescriptionEs = "Directorio BBB de {categoría} ordenado {ciudad}, {estado}. BBB Start with Trust ®. Su guía para calificaciones de confianza BBB, comentarios de clientes y negocios acreditados BBB.";
        #endregion

        #region Business Metadata
        public const string DefaultBusinessPageTitleEn = "{name} | Better Business Bureau® Profile";
        public const string DefaultBusinessMetaDescriptionEn = "This organization is not BBB accredited. {category} in {city}, {state}. See BBB rating, reviews, complaints, & more";

        public const string DefaultBusinessPageTitleEs = "{name} | Perfil de Better Business Bureau®";
        public const string DefaultBusinessMetaDescriptionEs = "Esta organización no está acreditada por BBB. {category} en {city}, {state}. Consulte las calificaciones BBB, revisiones, quejas y más";
        #endregion

        #region Accredited Business Metadata
        public const string DefaultAccreditedBusinessPageTitleEn = "{name} | Better Business Bureau® Profile";
        public const string DefaultAccreditedBusinessMetaDescriptionEn = "BBB accredited since {date}. {category} in {city}, {state}. See BBB rating, reviews, complaints, & more";

        public const string DefaultAccreditedBusinessPageTitleEs = "{name} | Perfil de Better Business Bureau®";
        public const string DefaultAccreditedBusinessMetaDescriptionEs = "Acreditado BBB desde {date}. {category} en {city}, {state}. Consulte las calificaciones BBB, revisiones, quejas y más";
        #endregion

        #region RAQ Accredited Business Metadata
        public const string DefaultRaqAccreditedBusinessPageTitleEn = "{name} | Better Business Bureau® Profile";
        public const string DefaultRaqAccreditedBusinessMetaDescriptionEn = "BBB accredited since {date}. {category} in {city}, {state}. See BBB rating, reviews, complaints, request a quote & more";

        public const string DefaultRaqAccreditedBusinessPageTitleEs = "{name} | Perfil de Better Business Bureau®";
        public const string DefaultRaqAccreditedBusinessMetaDescriptionEs = "Acreditado BBB desde {date}. {category} en {city}, {state}. Consulte las calificaciones BBB, comentarios, quejas, solicite un presupuesto y más";
        #endregion
    }
}
