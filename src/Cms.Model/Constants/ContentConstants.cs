﻿using Cms.Model.Models.Content.Entities;

namespace Cms.Model.Constants
{
    public static class ContentConstants
    {
        public static Author DefaultAuthor = CreateDefaultAuthor();

        private static Author CreateDefaultAuthor()
        {
            var author = new Author();
            author.Id = "bbb";
            author.Name = "Better Business Bureau";
            author.Url = "https://www.bbb.org";
            return author;
        }
    }
}
