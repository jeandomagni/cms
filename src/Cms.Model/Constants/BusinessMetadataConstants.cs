﻿using Cms.Model.Models.SecTerm;
using System.Collections.Generic;

namespace Cms.Model.Constants
{
    public static class MetadataBusinessConstants
    {
        public const string MetadataBusinessKey = "business";
        public const string MetadataAccreditedBusinessKey = "accreditedBusiness";
        public const string MetadataRaqAccreditedBusinessKey = "raqAccreditedBusiness";

        public static Metadata BusinessMetadataEn = new Metadata()
        {
            Type = MetadataBusinessKey,
            PageTitle = SecTermConstants.DefaultBusinessPageTitleEn,
            MetaDesc = SecTermConstants.DefaultBusinessMetaDescriptionEn,
            CultureIds = CultureConstants.SupportedEnCultures,
        };

        public static Metadata BusinessMetadataEs = new Metadata()
        {
            Type = MetadataBusinessKey,
            PageTitle = SecTermConstants.DefaultBusinessPageTitleEs,
            MetaDesc = SecTermConstants.DefaultBusinessMetaDescriptionEs,
            CultureIds = CultureConstants.SupportedEsCultures,
        };
        
        public static Metadata AccreditedBusinessMetadataEn = new Metadata()
        {
            Type = MetadataAccreditedBusinessKey,
            PageTitle = SecTermConstants.DefaultAccreditedBusinessPageTitleEn,
            MetaDesc = SecTermConstants.DefaultAccreditedBusinessMetaDescriptionEn,
            CultureIds = CultureConstants.SupportedEnCultures,
        };

        public static Metadata AccreditedBusinessMetadataEs = new Metadata()
        {
            Type = MetadataAccreditedBusinessKey,
            PageTitle = SecTermConstants.DefaultAccreditedBusinessPageTitleEs,
            MetaDesc = SecTermConstants.DefaultAccreditedBusinessMetaDescriptionEs,
            CultureIds = CultureConstants.SupportedEsCultures,
        };

        public static Metadata RaqAccreditedBusinessMetadataEn = new Metadata()
        {
            Type = MetadataRaqAccreditedBusinessKey,
            PageTitle = SecTermConstants.DefaultRaqAccreditedBusinessPageTitleEn,
            MetaDesc = SecTermConstants.DefaultRaqAccreditedBusinessMetaDescriptionEn,
            CultureIds = CultureConstants.SupportedEnCultures,
        };

        public static Metadata RaqAccreditedBusinessMetadataEs = new Metadata()
        {
            Type = MetadataRaqAccreditedBusinessKey,
            PageTitle = SecTermConstants.DefaultRaqAccreditedBusinessPageTitleEs,
            MetaDesc = SecTermConstants.DefaultRaqAccreditedBusinessMetaDescriptionEs,
            CultureIds = CultureConstants.SupportedEsCultures,
        };

        public static List<Metadata> DefaultBusinessMetadata = new List<Metadata>()
        {
            BusinessMetadataEn,
            BusinessMetadataEs,
            AccreditedBusinessMetadataEn,
            AccreditedBusinessMetadataEs,
            RaqAccreditedBusinessMetadataEn,
            RaqAccreditedBusinessMetadataEs,
        };
    }
}
