﻿namespace Cms.Model.Contract
{
    public interface IPagingSettings
    {
        int? Page { get; set; }
        int? PageSize { get; set; }
        string SortColumn { get; set; }
        string Direction { get; set; }
    }
}
