﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cms.Model.Models
{
    public class ArticleTypeToListingTypeMapping
    {
        public string ArticleType { get; set; }
        public string[] ListingTypes { get; set; }
    }
}
