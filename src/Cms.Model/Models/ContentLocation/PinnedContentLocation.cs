﻿using System;

namespace Cms.Model.Models.ContentLocation
{
    public class PinnedContentLocation
    {
        public int Id { get; set; }
        public string PinnedLocation { get; set; }
        public string LocationType { get; set; }
        public DateTimeOffset PinExpirationDate { get; set; }
    }
}
