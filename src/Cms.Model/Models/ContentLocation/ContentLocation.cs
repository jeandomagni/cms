﻿namespace Cms.Model.Models.ContentLocation
{
    public class ContentLocation
    {
        public string Type { get; set; }
        public string Location { get; set; }
    }
}
