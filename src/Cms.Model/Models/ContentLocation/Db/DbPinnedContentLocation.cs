﻿using System;

namespace Cms.Model.Models.ContentLocation.Db
{
    public class DbPinnedContentLocation
    {
        public int Id { get; set; }
        public string ContentCode { get; set; }
        public string PinnedLocationId { get; set; }
        public string PinnedLocationType { get; set; }
        public DateTimeOffset PinExpirationDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
    }
}
