﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cms.Model.Models.BaselinePages
{
    public class BaselinePage
    {
        public int Id { get; set; }
        public string UrlSegment { get; set; }
        public string RedirectUrl { get; set; }
        public string ContentCode { get; set; }
        public int TotalCount { get; set; }

        public string Title { get; set; }
        public string Layout { get; set; }
        public string Type { get; set; }
        public DateTimeOffset? ModifiedDate { get; set; }
    }
}
