﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cms.Model.Models.BaselinePages
{
    public class IsSegmentUrlExistsModel
    {
        public string UrlSegment { get; set; }
    }
}
