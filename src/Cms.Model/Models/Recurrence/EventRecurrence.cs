﻿using Newtonsoft.Json;
using RecurrenceCalculator;
using System;

namespace Cms.Model.Models.Recurrence
{
    public class EventRecurrence : IRecurrence
    {

        public int DayOfMonth { get; set; }

        public bool Sunday { get; set; }

        public bool Monday { get; set; }

        public bool Tuesday { get; set; }

        public bool Wednesday { get; set; }

        public bool Thursday { get; set; }

        public bool Friday { get; set; }

        public bool Saturday { get; set; }

        public int Instance { get; set; }

        public int Day { get; set; }

        public int Interval { get; set; }

        public int MonthOfYear { get; set; }

        public int Occurrences { get; set; }

        public DateTime? StartTime { get; set; }
        public DateTime StartDate { get; set; }

        public RecurrenceType RecurrenceType { get; set; }

        public DateTime? EndDate { get; set; }
        public DateTime? EndTime { get; set; }

        public DailyType DailyType { get; set; }

        public EndDayType EndDayType { get; set; }

        public string DayOfWeek { get; set; }

        public static bool TryParse(string json, out EventRecurrence eventRecurrence)
        {
            try
            {
                var settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };
                eventRecurrence = JsonConvert.DeserializeObject<EventRecurrence>(json, settings);
                return true;
            }
            catch (Exception)
            {
                eventRecurrence = null;
                return false;
            }
        }


        public void Init()
        {
            if (RecurrenceType == RecurrenceType.MonthNth)
            {
                Sunday = DayOfWeek == "Sunday";
                Monday = DayOfWeek == "Monday";
                Tuesday = DayOfWeek == "Tuesday";
                Wednesday = DayOfWeek == "Wednesday";
                Thursday = DayOfWeek == "Thursday";
                Friday = DayOfWeek == "Friday";
                Saturday = DayOfWeek == "Saturday";
            }

            if (RecurrenceType == RecurrenceType.Daily && DailyType == DailyType.Weekdays)
            {
                Sunday = false;
                Monday = true;
                Tuesday = true;
                Wednesday = true;
                Thursday = true;
                Friday = true;
                Saturday = false;
            }

            if (RecurrenceType == RecurrenceType.Daily && DailyType == DailyType.NDays)
            {
                Sunday = Monday = Tuesday = Wednesday = Thursday = Friday = Saturday = true;
            }

            if (StartTime.HasValue)
            {
                StartDate = new DateTime(StartDate.Year, StartDate.Month, StartDate.Day, StartTime.Value.Hour, StartTime.Value.Minute, 0);
            }

            if (EndTime.HasValue && EndDate.HasValue)
            {
                EndDate = new DateTime(EndDate.Value.Year, EndDate.Value.Month, EndDate.Value.Day, EndTime.Value.Hour, EndTime.Value.Minute, 0);
            }
        }
    }
}
