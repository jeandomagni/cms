﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cms.Model.Models.Recurrence
{
    public class ContentEventOccurrence
    {
        public int Id { get; set; }
        public int EventId { get; set; }
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset EndDate { get; set; }
    }
}
