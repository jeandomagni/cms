﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cms.Model.Models.Recurrence
{
    public enum DailyType
    {
        NDays = 0,
        Weekdays = 1
    }
}
