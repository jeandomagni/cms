﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cms.Model.Models.Recurrence
{
    public enum EndDayType
    {
        None = 0,
        EndBy = 1,
        EndAfter = 2
    }
}
