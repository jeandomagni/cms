﻿using System.ComponentModel.DataAnnotations;

namespace Cms.Model.Models
{
    public class SiteContent
    {
        [Key]
        public int Id { get; set; }
        public string SiteId { get; set; }
        public string ContentCode { get; set; }
    }
}
