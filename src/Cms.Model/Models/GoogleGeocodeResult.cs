﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cms.Model.Models
{
    public class GoogleGeocodeInfo
    {
        public List<GoogleGeocodeResult> Results { get; set; }
    }

    public class GoogleGeocodeResult
    {
        public Geometry Geometry { get; set; }
    }

    public class Geometry
    {
        public GoogleLocation Location { get; set; }
    }

    public class GoogleLocation
    {
        public double Lat { get; set; }
        public double Lng { get; set; }
    }


}
