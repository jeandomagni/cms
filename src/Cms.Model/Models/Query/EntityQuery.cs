﻿namespace Cms.Model.Models.Query
{
    public class EntityQuery
    {
        public string Type { get; set; }
        public string Query { get; set; }
        public int ResultLimit { get; set; }
    }
}
