﻿namespace Cms.Model.Models.Query
{
    public class ContentMediaOrder
    {
        public int Order { get; set; }
        public int ContentMediaId { get; set; }
    }
}
