﻿namespace Cms.Model.Models.Query.DataTable
{
    public class Filter
    {
        public string Column { get; set; }
        public string Type { get; set; }
        public string Keyword { get; set; }
    }
}
