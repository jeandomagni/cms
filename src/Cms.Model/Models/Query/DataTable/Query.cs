﻿using System.Collections.Generic;

namespace Cms.Model.Models.Query.DataTable
{
    public class Query
    {
        public string CheckedOutBy { get; set; }
        public bool DomainOnly { get; set; }
        public bool IncludeExpired { get; set; }
        public int Limit 
        { get; set; }
        public int Page { get; set; }
        public string Locations { get; set; }
        public string OrderBy { get; set; }
        public IList<Filter> Filters { get; set; }
    }
}
