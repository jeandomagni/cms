﻿using System;
using System.Collections.Generic;

namespace Cms.Model.Models.Query.KendoGrid
{
    [Obsolete("Use Query.DataTable. WEB-3200's move away from Kendo makes this obsolete. " +
        "Used only for SQL stored procedures but will eventually be deprecated there as well.")]
    public class Data
    {
        public int take { get; set; }
        public int skip { get; set; }
        public int page { get; set; }
        public int pageSize { get; set; }
        public List<Sort> sort { get; set; }
        public KendoFilter filter { get; set; }
        public string locations { get; set; }
        public bool Domain { get; set; }
        public bool includeExpired { get; set; }
        public string CheckedOutBy { get; set; }
    }
}
