﻿using System;

namespace Cms.Model.Models.Query.KendoGrid
{
    [Obsolete("Use Query.DataTable. WEB-3200's move away from Kendo makes this obsolete. " +
        "Used only for SQL stored procedures but will eventually be deprecated there as well.")]
    public class Sort
    {
        public string field { get; set; }
        public string dir { get; set; }
    }
}
