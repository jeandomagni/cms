﻿using System;
using System.Linq;

namespace Cms.Model.Models.Query
{
    public class ArticleOrderParams
    {
        public int Id { get; set; }
        public string ListingType { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string ServiceArea { get; set; }
        public string Topic { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }

        public string GenerateQueryString()
        {
            var keyValueDictionary = typeof(ArticleOrderParams)
                    .GetProperties()
                    .Where(x => x.CanRead)
                    .Where(x => x.GetValue(this, null) != null)
                    .ToDictionary(x => x.Name, x => x.GetValue(this, null));

            return string.Join("&", keyValueDictionary
                .Select(x => string.Concat(
                    char.ToLowerInvariant(x.Key[0]) + x.Key.Substring(1), "=",
                    Uri.EscapeDataString(x.Value.ToString()))));
        }
    }
}
