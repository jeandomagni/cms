﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cms.Model.Models.Query
{
    public class UpdateArticleOrderParams : ArticleOrderParams
    {
        public string[] Order { get; set; }
    }
}
