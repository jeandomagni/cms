﻿using System.ComponentModel.DataAnnotations;

namespace Cms.Model.Models.UserManagement
{
    public partial class Membership
    {
        [Key]
        public int mId { get; set; }
        public System.Guid? UserId { get; set; }
        public string SiteId { get; set; }
        public string Status { get; set; }
        public System.DateTime? LastLoginDate { get; set; }
        public System.DateTime? LastPasswordChangedDate { get; set; }
        public System.DateTime? LastLockoutDate { get; set; }
        public int? FailedPasswordAttemptCount { get; set; }
        public System.DateTime? FailedPasswordAttemptStart { get; set; }
        public string Comment { get; set; }
    }
}