﻿using System.ComponentModel.DataAnnotations;

namespace Cms.Model.Models.UserManagement
{
    public partial class UserMembership
    {
        public System.Guid UserId { get; set; }
        public string BBBId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string PasswordSalt { get; set; }
        public string PasswordQuestion { get; set; }
        public string PasswordAnswer { get; set; }
        public int mId { get; set; }
        public string SiteId { get; set; }
        public string Status { get; set; }
        public System.DateTime? LastLoginDate { get; set; }
        public System.DateTime? LastPasswordChangedDate { get; set; }
        public System.DateTime? LastLockoutDate { get; set; }
        public int? FailedPasswordAttemptCount { get; set; }
        public System.DateTime? FailedPasswordAttemptStart { get; set; }
        public string Comment { get; set; }
    }
}
