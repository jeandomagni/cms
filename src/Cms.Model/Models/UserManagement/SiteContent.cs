﻿using System.ComponentModel.DataAnnotations;

namespace Cms.Model.Models.UserManagement
{
    public class SiteContent
    {
        [Key]
        public int Id { get; set; }
        public string SiteId { get; set; }
        public string ContentId { get; set; }
    }
}