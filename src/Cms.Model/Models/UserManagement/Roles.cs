﻿using System.ComponentModel.DataAnnotations;

namespace Cms.Model.Models.UserManagement
{
    public class Roles
    {
        [Key]
        public System.Guid RoleId { get; set; }
        public string RoleName { get; set; }
        public string LoweredRoleName { get; set; }
        public string Description { get; set; }
    }
}