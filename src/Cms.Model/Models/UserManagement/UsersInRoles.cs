﻿using System.ComponentModel.DataAnnotations;

namespace Cms.Model.Models.UserManagement
{
    public class UsersInRoles
    {
        [Key]
        public System.Guid UserId { get; set; }
        [Key]
        public System.Guid RoleId { get; set; }
    }
}