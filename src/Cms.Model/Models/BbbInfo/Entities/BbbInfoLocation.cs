﻿using System.Collections.Generic;

namespace Cms.Model.Models.BbbInfo.Entities
{
    public class BbbInfoLocation
    {
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Country { get; set; }
        public string Type { get; set; }
        public List<string> Jurisdiction { get; set; }
        public List<string> OfficeHours { get; set; }
        public List<string> PhoneHours { get; set; }
        public string ContactEmail { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string TextNumber { get; set; }
        public bool ExcludeFromLocator { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public string ContactEmailPr { get; set; }
        public string PhoneNumberPr { get; set; }
    }
}
