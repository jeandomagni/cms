﻿using System.Collections.Generic;
using System.Linq;

namespace Cms.Model.Models.BbbInfo.Entities
{
    public class BbbInfoApplyForAccreditationInfo
    {
        public string Terms { get; set; }
        public bool? ShowTerms { get; set; }
        public string ApplyEmail { get; set; }
        public string ApplicationSubmission { get; set; }
        public bool? SendEmail { get; set; }
        public IList<BbbApplyForAccreditationInfoQuestion> Questions { get; set; } = new List<BbbApplyForAccreditationInfoQuestion> ();
    }

    public class BbbApplyForAccreditationInfoQuestion
    {

        public bool Editable { get; set; }
        public bool Required { get; set; }
        public string Question { get; set; }
        public QuestionType Type { get; set; }
        public List<string> Values { get => ValuesString?.Split (",").Select (x => x.Trim ()).ToList (); set => ValuesString = value != null ? string.Join (", ", value) : string.Empty; }
        public string ValuesString { get; set; }
        public string Helptext { get; set; }
        public bool ShowSubquestionOnNo { get; set; }
        public IList<BbbApplyForAccreditationInfoQuestion> Subquestion { get; set; } = new List<BbbApplyForAccreditationInfoQuestion> ();
    }

    public enum QuestionType
    {
        Radiobutton,
        String,
        Dropdown,
        Number,
        Datetime
    }
}
