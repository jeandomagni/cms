﻿namespace Cms.Model.Models.BbbInfo.Entities
{
    public class BbbInfoProgram
    {
        public string Title { get; set; }
        public int Priority { get; set; }
        public string Icon { get; set; }
        public string LinkUrl { get; set; }
        public string SummaryText { get; set; }
    }
}
