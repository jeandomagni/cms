﻿using System.Collections.Generic;

namespace Cms.Model.Models.BbbInfo.Entities
{
    public class BbbInfoPersonGroup
    {
        public string Name { get; set; }
        public List<BbbInfoPerson> Persons { get; set; } = new List<BbbInfoPerson>();
    }
}
