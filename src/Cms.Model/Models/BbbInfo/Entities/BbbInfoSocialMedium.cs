﻿namespace Cms.Model.Models.BbbInfo.Entities
{
    public class BbbInfoSocialMedium
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public string Url { get; set; }
    }
}
