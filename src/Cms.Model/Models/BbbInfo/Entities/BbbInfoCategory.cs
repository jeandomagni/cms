﻿namespace Cms.Model.Models.BbbInfo.Entities
{
    public class BbbInfoCategory
    {
        public string Name { get; set; }
        public string TobId { get; set; }
    }
}
