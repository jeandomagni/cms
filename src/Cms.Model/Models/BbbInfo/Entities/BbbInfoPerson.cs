﻿using System;
using System.Collections.Generic;

namespace Cms.Model.Models.BbbInfo.Entities
{
    public class BbbInfoPerson
    {
        public string Name { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? Priority { get; set; }
        public string Type { get; set; }
        public string Company { get; set; }
        public string Title { get; set; }
        public string ImageUrl { get; set; }
        public string Bio { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Website { get; set; }
        public IEnumerable<BbbInfoSocialMedium> SocialMedia { get; set; }
        public string BusinessName { get; set; }
        public DateTime? AccreditedDate { get; set; }
    }
}
