﻿namespace Cms.Model.Models.BbbInfo.Entities
{
    public class BbbInfoFileComplaintQuestion
    {
        public bool Editable { get; set; }
        public string QualificationHtml { get; set; }
        public string Question { get; set; }
    }
}
