﻿using Cms.Model.Attributes;
using Cms.Model.Enum;
using System;
using System.ComponentModel.DataAnnotations;

namespace Cms.Model.Models.BbbInfo.Db
{
    public class DbBbbInfo
    {
        [Key]
        public int Id { get; set; }

        #region Fields ignored in audit log
        public string LegacySiteId { get; set; }
        public string LegacyId { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public bool Active { get; set; }
        #endregion

        #region Fields tracked in audit log
        [Action(EntityActions.UPDATE_ABOUT_US_TEXT)]
        public string AboutUsText { get; set; }

        [Action(EntityActions.UPDATE_ADDITIONAL_RESOURCES)]
        public string AdditionalResources { get; set; }

        [Action(EntityActions.UPDATE_VENDOR_ALIASES)]
        public bool AllowVendorUriAliases { get; set; }

        [Action(EntityActions.UPDATE_ANNUAL_REPORTS_URL)]
        public string AnnualReportsUrl { get; set; }

        [Action(EntityActions.UPDATE_ACCREDITATION_APPLY_URL)]
        public string ApplyForAccreditationUrl { get; set; }

        [Action(EntityActions.UPDATE_JOIN_APPLY_URL)]
        public string JoinApplyUrl { get; set; }

        [Action(EntityActions.UPDATE_AREA_CODES)]
        public string AreaCodes { get; set; }

        [Action(EntityActions.UPDATE_BASE_URI_ALIASES)]
        public string BaseUriAliases { get; set; }
        
        [Action(EntityActions.UPDATE_BBB_URL_SEGMENT)]
        public string BbbUrlSegment { get; set; }

        [Action(EntityActions.UPDATE_BUSINESS_LOGIN_URL)]
        public string BusinessLoginUrl { get; set; }

        [Action(EntityActions.UPDATE_CHARITY_PROFILE_SOURCE)]
        public string CharityProfileSource { get; set; }

        [Action(EntityActions.UPDATE_CLAIMS_LISTING_URL)]
        public string ClaimListingUrl { get; set; }

        [Action(EntityActions.UPDATE_CLOSE_BODY_SCRIPT)]
        public string CloseBodyScript { get; set; }

        [Action(EntityActions.UPDATE_COMPLAINT_QUALIFYING_QUESTIONS)]
        public string ComplaintQualifyingQuestionsJson { get; set; }

        [Action(EntityActions.UPDATE_CONSUMER_LOGIN_URL)]
        public string ConsumerLoginUrl { get; set; }

        [Action(EntityActions.UPDATE_EMPLOYMENT_OPPORTUNITIES_URL)]
        public string EmploymentOpportunitiesUrl { get; set; }

        [Action(EntityActions.UPDATE_EVENTS_PAGE_LAYOUT)]
        public string EventsPageLayout { get; set; }

        [Action(EntityActions.UPDATE_FILE_COMPLAINT_URL)]
        public string FileComplaintUrl { get; set; }

        [Action(EntityActions.UPDATE_FOOTER_LINKS)]
        public string FooterLinks { get; set; }

        [Action(EntityActions.UPDATE_GATEWAY_ENABLED_FLAG)]
        public bool GatewayEnabled { get; set; }

        [Action(EntityActions.UPDATE_RAQ_ENABLED_FLAG)]
        public bool RaqEnabled { get; set; }

        [Action(EntityActions.UPDATE_GOOGLE_ANALYTICS_ID)]
        public string GoogleAnalyticsId { get; set; }

        [Action(EntityActions.UPDATE_HEADER_FOOTER_FLAG)]
        public bool HeaderFooterFlag { get; set; }

        [Action(EntityActions.UPDATE_HEADER_LINKS)]
        public string HeaderLinks { get; set; }

        [Action(EntityActions.UPDATE_HEADER_LINKS)]
        public string MenuLinks { get; set; }

        [Action(EntityActions.UPDATE_HEAD_SCRIPT)]
        public string HeadScript { get; set; }

        [Action(EntityActions.UPDATE_INFORMATION_PAGE_IMAGE_URL)]
        public string InformationPageImageUrl { get; set; }

        [Action(EntityActions.UPDATE_LEADSGEN_LINK_ENABLED)]
        public bool LeadsgenLinkEnabled {get; set;}

        [Action(EntityActions.UPDATE_LOCATIONS)]
        public string Locations { get; set; }

        [Action(EntityActions.UPDATE_NEWS_LETTER_SIGNUP_URL)]
        public string NewsLetterSignupUrl { get; set; }

        [Action(EntityActions.UPDATE_OPEN_BODY_SCRIPT)]
        public string OpenBodyScript { get; set; }

        [Action(EntityActions.UPDATE_PERSONS)]
        public string Persons { get; set; }

        [Action(EntityActions.UPDATE_PHONE_LEADS_EMAILS)]
        public string PhoneLeadsEmails { get; set; }

        [Action(EntityActions.UPDATE_PRIMARY_COUNTRY)]
        public string PrimaryCountry { get; set; }

        [Action(EntityActions.UPDATE_PROGRAMS)]
        public string Programs { get; set; }

        [Action(EntityActions.UPDATE_SOCIAL_MEDIA)]
        public string SocialMedia { get; set; }

        [Action(EntityActions.UPDATE_SUBDOMAINS)]
        public string Subdomains { get; set; }

        [Action(EntityActions.UPDATE_SUBMIT_REVIEW_URL)]
        public string SubmitReviewUrl { get; set; }

        [Action(EntityActions.UPDATE_TAGLINE)]
        public string Tagline { get; set; }

        [Action(EntityActions.UPDATE_VENDOR)]
        public string Vendor { get; set; }

        [Action(EntityActions.UPDATE_VENDOR_HOST_HEADER)]
        public string VendorHostHeader { get; set; }

        [Action(EntityActions.UPDATE_LANGUAGES)]
        public string Languages { get; set; }

        [Action(EntityActions.UPDATE_POPULAR_CATEGORIES)]
        public string PopularCategoriesJson { get; set; }

        [Action(EntityActions.UPDATE_APPLY_FOR_ACCREDITATION_INFO)]
        public string ApplyForAccreditationInfoJson { get; set; }

        #endregion

        public string Name { get; set; }
        public string PrimaryLanguage { get; set; }
        [Action(EntityActions.UPDATE_PRIMARY_URL_ALIAS)]
        public string PrimaryUrlAlias { get; set; }
        [Action(EntityActions.UPDATE_BASE_URL)]
        public string BaseUrl { get; set; }
        [Action(EntityActions.UPDATE_BUSSINESS_LEAD_EMAIL_NOTIFICATION_ENABLED)]
        public bool BusinessLeadEmailNotficationEnabled  { get; set; }
    }
}
