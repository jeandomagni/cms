﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cms.Model.Models.BbbInfo
{
    public class BbbServiceArea
    {
        public string BbbId { get; set; }
        public List<string> Countries { get; set; }
        public List<string> States { get; set; }
        public string PrimaryCountry { get; set; }
    }
}
