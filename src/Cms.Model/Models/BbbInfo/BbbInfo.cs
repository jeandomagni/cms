﻿using Cms.Model.Models.BbbInfo.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Cms.Model.Models.BbbInfo
{
    public class BbbInfo
    {
        [Key]
        public int Id { get; set; }
        public string LegacySiteId { get; set; }
        public string LegacyId { get; set; }
        public string HeadScript { get; set; }
        public string OpenBodyScript { get; set; }
        public string CloseBodyScript { get; set; }
        public string Name { get; set; }
        public string Tagline { get; set; }
        public string AboutUsText { get; set; }
        public string InformationPageImageUrl { get; set; }
        public string EmploymentOpportunitiesUrl { get; set; }
        public string ApplyForAccreditationUrl { get; set; }
        public string FileComplaintUrl { get; set; }
        public string SubmitReviewUrl { get; set; }
        public string NewsLetterSignupUrl { get; set; }
        public string ClaimListingUrl { get; set; }
        public string BusinessLoginUrl { get; set; }
        public string ConsumerLoginUrl { get; set; }
        public string AnnualReportsUrl { get; set; }
        public string JoinApplyUrl { get; set; }
        public IList<BbbInfoLocation> Locations { get; set; }
        public string AreaCodes { get; set; }
        public string Subdomains { get; set; }
        public string BaseUriAliases { get; set; }
        public string PhoneLeadsEmails { get; set; }
        public string Vendor { get; set; }
        public bool HeaderFooterFlag { get; set; }
        public string VendorHostHeader { get; set; }
        public bool AllowVendorUriAliases { get; set; }
        public bool GatewayEnabled { get; set; }
        public bool RaqEnabled { get; set; }
        public string PrimaryCountry { get; set; }
        public string PrimaryLanguage { get; set; }
        public IList<BbbInfoPersonGroup> Persons { get; set; }
        public string CharityProfileSource { get; set; }
        public IList<BbbInfoProgram> Programs { get; set; }
        public string GoogleAnalyticsId { get; set; }
        public IList<BbbInfoSocialMedium> SocialMedia { get; set; }
        public IList<BbbInfoAdditionalResources> AdditionalResources { get; set; }
        public string EventsPageLayout { get; set; }
        public DateTimeOffset ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public bool Active { get; set; }
        public IList<BbbLink> HeaderLinks { get; set; }
        public IList<BbbLink> FooterLinks { get; set; }
        public string BbbUrlSegment { get; set; }
        public IList<BbbLink> MenuLinks { get; set; }
        public string Languages { get; set; }
        public IList<BbbInfoCategory> PopularCategories { get; set; }
        public IList<BbbInfoFileComplaintQuestion> ComplaintQualifyingQuestions { get; set; }
        public BbbInfoApplyForAccreditationInfo ApplyForAccreditationInfo { get; set; }
        public bool LeadsgenLinkEnabled {get; set;}
        public string PrimaryUrlAlias { get; set; }
        public string BaseUrl { get; set; }
        public bool BusinessLeadEmailNotficationEnabled  { get; set; }
    }
}
