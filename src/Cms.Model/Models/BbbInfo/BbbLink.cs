﻿using System.Collections.Generic;

namespace Cms.Model.Models.BbbInfo
{
    public class BbbLink
    {
        public string LinkText { get; set; }
        public string LinkUrl { get; set; }
        public int Priority { get; set; }
        public bool Disabled { get; set; }
        public List<BbbLink> Children { get; set; }
    }
}
