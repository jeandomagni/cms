﻿using Cms.Model.Enum;
using System.Collections.Generic;

namespace Cms.Model.Models
{
    public class Location
    {
        public string Id { get; set; }
        public List<string> BbbIds { get; set; }
        public string City { get; set; }
        public string CitySeo { get; set; }
        public string CountryCode { get; set; }
        public string State { get; set; }
        public string StateFull { get; set; }

        public string CountryName {
            get
            {
                var countryCode = System.Enum.IsDefined(typeof(CountryCode), CountryCode)
                    ? System.Enum.Parse(typeof(CountryCode), CountryCode.ToString())
                    : Enum.CountryCode.USA;
                switch (countryCode)
                {
                    case Enum.CountryCode.USA:
                        return "United States";
                    case Enum.CountryCode.CAN:
                        return "Canada";
                    case Enum.CountryCode.MEX:
                        return "Mexico";
                    default:
                        return "United States";
                }
            }
        }
    }
}
