﻿using System;
using System.Collections.Generic;

namespace Cms.Model.Models
{
    public sealed class ImageTagType
    {
        public const String All = "*";
        public const string Generic = "generic";
        public const string State = "state";
        public const string City = "city";
        public const string Country = "country";
    }
    public class ImageTag : IEquatable<ImageTag>, IComparable<ImageTag>
    {
        public static readonly ImageTag[] CommonImageTags = new ImageTag[]
        {
            new ImageTag { Name = "Hero", Value="hero", Type = ImageTagType.Generic}
        };
        public string Name { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }

        public string Label
        {
            get
            {
                return $"{Name} :: {Type}";
            }
        }

        public int CompareTo(ImageTag other)
        {
            return Name.CompareTo(other.Name);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as ImageTag);
        }

        public bool Equals(ImageTag other)
        {
            return other != null &&
                   Name == other.Name;
        }

        public override int GetHashCode()
        {
            return 539060726 + EqualityComparer<string>.Default.GetHashCode(Name);
        }
    }

   
}
