﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cms.Model.Models
{
    public class OrderedArticlesByLocation
    {
        public IEnumerable<ArticleOrderSummary> Articles { get; set; } = new List<ArticleOrderSummary>();
        public int TotalCount { get; set; } = 0;
        public int ArticleOrderId { get; set; }
        public int? OverrideFeedId { get; set; }
    }
}
