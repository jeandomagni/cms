﻿using System;

namespace Cms.Model.Models.Content.Entities
{
    public class Geotag
    {
        public string ContentCode { get; set; }
        public bool IsPinned { get; set; }
        public string LocationId { get; set; }
        public string LocationType { get; set; }
        public DateTime? PinExpirationDate { get; set; }
        public int? PinId { get; set; }
    }
}
