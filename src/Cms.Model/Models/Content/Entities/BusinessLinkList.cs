﻿using Cms.Model.Models.BbbInfo;
using System.Collections.Generic;

namespace Cms.Model.Models.Content.Entities
{
    public class BusinessLinkList
    {
        public string Title { get; set; }
        public IEnumerable<BbbLink> BusinessLinks { get; set; }
    }
}
