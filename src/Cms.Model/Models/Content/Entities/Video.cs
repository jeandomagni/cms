﻿using System;

namespace Cms.Model.Models.Content.Entities
{
    public class Video
    {
        public int Type { get; set; }
        public Uri Url { get; set; }
        public string Title { get; set; }
        public string Caption { get; set; }
        public int Position { get; set; }
    }
}
