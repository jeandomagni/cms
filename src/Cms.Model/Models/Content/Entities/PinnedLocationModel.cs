﻿using System;

namespace Cms.Model.Models.Content.Entities
{
    public class PinnedLocationModel
    {
        public string Code { get; set; }
        public int Id { get; set; }
        public string LocationType { get; set; }
        public DateTime? PinExpirationDate { get; set; }
        public string PinnedLocation { get; set; }
    }
}
