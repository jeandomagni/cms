﻿using System;
using System.Collections.Generic;

namespace Cms.Model.Models.Content.Entities
{
    public class Author : IEquatable<Author>
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string Email { get; set; }


        public override bool Equals(object obj)
        {
            return Equals(obj as Author);
        }

        public bool Equals(Author other)
        {
            return other != null && Id == other.Id;
        }

        public override int GetHashCode()
        {
            return 1877310944 + EqualityComparer<string>.Default.GetHashCode(Id);
        }
    }
}
