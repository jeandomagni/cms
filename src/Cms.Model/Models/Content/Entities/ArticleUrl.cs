﻿namespace Cms.Model.Models.Content.Entities
{
    public class ArticleUrl
    {
        public string Prefix { get; set; }
        public string Segment { get; set; }
        public string Full => $"{Prefix}{Segment}";
    }
}
