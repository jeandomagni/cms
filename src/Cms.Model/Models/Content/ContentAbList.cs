﻿using Cms.Model.Models.Content.Entities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Cms.Model.Models.Content
{
    public class ContentAbList
    {
        [Key]
        public int Id { get; set; }
        public string ContentCode { get; set; }
        public IEnumerable<BusinessLinkList> BusinessLinkLists { get; set; }
    }
}
