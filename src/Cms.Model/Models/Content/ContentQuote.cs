﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Cms.Model.Models.Content
{
    public class ContentQuote
    {
        [Key]
        public int Id { get; set; }
        public string Code { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Text { get; set; }
        public string Tags { get; set; }
        public List<QuoteTag> TagItems => DeserializeTags();
        public DateTimeOffset? CreateDate { get; set; }
        public string CreatedBy { get; set; }
        public bool Deleted { get; set; }
        public string OwnerSiteId { get; set; }
        public bool Published { get; set; }
        public int TotalCount { get; set; }
        private List<QuoteTag> DeserializeTags()
        {

            try
            {
                var tagList = !String.IsNullOrEmpty(Tags) ? JsonConvert.DeserializeObject<List<QuoteTag>>(Tags) : new List<QuoteTag>();

                return tagList;
            }
            catch
            {
                return new List<QuoteTag>();
            }
        }
    }
}