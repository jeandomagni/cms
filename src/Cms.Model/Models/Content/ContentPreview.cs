﻿using Cms.Model.Models.Content.Entities;
using System;
using System.Collections.Generic;

namespace Cms.Model.Models.Content
{
    public class ContentPreview
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Code { get; set; }
        public string Type { get; set; }
        public IEnumerable<string> Tags { get; set; }
        public string Status { get; set; }
        public DateTimeOffset? CreateDate { get; set; }
        public DateTimeOffset? PublishDate { get; set; }
        public DateTimeOffset? ExpirationDate { get; set; }
        public string CreatedBy { get; set; }
        public int TotalCount { get; set; }
        public List<Location> Cities { get; set; }
        public List<Geotag> Countries { get; set; }
        public List<Geotag> States { get; set; }
        public string TopicsAndTags { get; set; }
        public string GeoTags { get; set; }
        public string CheckoutBy { get; set; }
        public bool CanEdit { get; set; }
        public bool IsCheckedOut { get; set; }
        public List<Geotag> ServiceArea { get; set; } = new List<Geotag>();
        public bool IsInMyNewsfeed { get; set; }
    }
}
