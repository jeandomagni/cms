﻿using System.ComponentModel.DataAnnotations;

namespace Cms.Model.Models.Content
{
    public class ContentMedia
    {
        [Key]
        public int Id { get; set; }
        public string Url { get; set; }
        public string FileCode { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ContentCode { get; set; }
        public bool MainImage { get; set; }
        public int Order { get; set; }
        public string ImageTitle { get; set; }
        public string ImageCaption { get; set; }
        public string Credit { get; set; }
    }
}