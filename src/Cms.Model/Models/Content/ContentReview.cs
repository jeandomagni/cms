﻿using System;

namespace Cms.Model.Models.Content
{
    public class ContentReview
    {
        public int Id { get; set; }
        public string ContentCode { get; set; }
        public string CreatedBy { get; set; }
        public string CompletedBy { get; set; }
        public string Comment { get; set; }
        public string Status { get; set; }
        public string Serialized { get; set; }
        public DateTimeOffset Completed { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
    }
}
