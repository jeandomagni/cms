﻿using Bbb.Core.Solr.Model;
using Cms.Model.Models.Content.Entities;
using Cms.Model.Models.ContentLocation;
using Cms.Model.Models.Recurrence;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Cms.Model.Models.Content
{
    public class Content
    {
        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        public string ContentHtml { get; set; }
        public string Summary { get; set; }
        public string Code { get; set; }
        public string Type { get; set; }
        public string Layout { get; set; }
        public IEnumerable<string> Tags { get; set; }
        public IEnumerable<SearchTypeahead> Categories { get; set; }
        public string Status { get; set; }
        public DateTimeOffset? PublishDate { get; set; }
        public string Image { get; set; }
        public DateTimeOffset? ExpirationDate { get; set; }
        public DateTimeOffset? CreateDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public List<Author> Authors { get; set; }
        public string BbbAuthor { get; set; }
        public string Locations { get; set; }
        public bool Deleted { get; set; }
        public List<Location> Cities { get; set; }
        public List<Location> PinnedCities { get; set; }
        public List<Geotag> Countries { get; set; }
        public List<Geotag> States { get; set; }
        public List<Geotag> BbbIds { get; set; }
        public string MobileHeadline { get; set; }
        public string UriSegment { get; set; }
        public string PageTitle { get; set; }
        public List<string> Topics { get; set; }
        public bool IsIndustryTip { get; set; }
        public string PinnedTopic { get; set; }
        public LegacyBbbInfo EventBbb { get; set; }
        public int Priority { get; set; }
        public string MetaDescription { get; set; }
        public IEnumerable<LegacyBbbInfo> Sites { get; set; }
        public ArticleUrl ArticleUrl { get; set; }
        public string OwnerSiteId { get; set; }
        public bool NoIndex { get; set; }
        public IEnumerable<Video> Videos { get; set; }
        public IEnumerable<PinnedContentLocation> PinnedLocations { get; set; }
        public DateTimeOffset? CheckoutDate { get; set; }
        public string CheckoutBy { get; set; }
        public bool CanEdit { get; set; }
        public bool CanCheckOut { get; set; }
        public List<Author> AvailableAuthors { get; set; }
        public IEnumerable<ContentEventOccurrence> Occurrences { get; set; }
        public bool IsCheckedOut { get; set; }
        public bool HasRedirectUrl { get; set; }
        public string RedirectUrl { get; set; }
        public bool HasSeoCanonical { get; set; }
        public string SeoCanonical { get; set; }
        public List<Geotag> PinnedStates { get; set; }
        public List<Geotag> PinnedCountries { get; set; }
        public List<string> PinnedBbbIds { get; set; }
    }
}
