﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Cms.Model.Models.Content
{
    public class ArticleOrder
    {
        public ArticleOrder()
        {
            ModifiedDate = DateTime.UtcNow;
        }

        [Key]
        public int Id { get; set; }

        private string _country = string.Empty;
        public string Country { get { return _country; } set { _country = value?.ToUpper() ?? string.Empty; } }

        private string _state = string.Empty;
        public string State
        {
            get { return _state; }
            set { _state = value?.ToUpper() ?? string.Empty; }
        }

        private string _city = string.Empty;
        public string City
        {
            get { return _city; }
            set { _city = value?.ToLower() ?? string.Empty; }
        }

        private string _serviceArea = string.Empty;
        public string ServiceArea
        {
            get { return _serviceArea; }
            set { _serviceArea = value ?? string.Empty; }
        }
        public string OrderedArticleIds { get; set; } = string.Empty;

        private string _listingType;
        public string ListingType
        {
            get { return _listingType; }
            set { _listingType = value.Replace("-", " ").ToLower(); }
        }
        private string _topic;
        public string Topic
        {
            get { return _topic; }
            set { _topic = value?.ToLower(); }
        }
        public DateTime ModifiedDate { get; set; }

        public int? OverrideFeedId { get; set; }

        [NotMapped]
        public List<string> OrderedArticleIdsAsArray
        {
            get
            {
                return OrderedArticleIds?.Split(",", StringSplitOptions.RemoveEmptyEntries).ToList() ?? new List<string>();
            }
            set
            {
                if (value != null)
                {
                    OrderedArticleIds = string.Join(",", value);
                }
                else
                {
                    OrderedArticleIds = string.Empty;
                }
            }
        }

        [NotMapped]
        public bool NotTagged { get; set; }

        public bool IsCountryLevel() => string.IsNullOrWhiteSpace(City) && string.IsNullOrWhiteSpace(State) && string.IsNullOrWhiteSpace(ServiceArea);
        public bool IsStateLevel() => string.IsNullOrWhiteSpace(City) && string.IsNullOrWhiteSpace(ServiceArea) && !string.IsNullOrWhiteSpace(State);
        public bool IsCityLevel() => !string.IsNullOrWhiteSpace(City) && string.IsNullOrWhiteSpace(ServiceArea) && !string.IsNullOrWhiteSpace(State);
        public bool IsBbbLevel() => string.IsNullOrWhiteSpace(City) && !string.IsNullOrWhiteSpace(ServiceArea) && string.IsNullOrWhiteSpace(State);
    }
}
