﻿namespace Cms.Model.Models.Content
{
    public class ReviewRequest
    {
        public ContentReview ContentReview;
        public string NewStatus;
    }
}
