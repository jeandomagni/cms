﻿using Cms.Model.Attributes;
using Cms.Model.Enum;
using System;
using System.ComponentModel.DataAnnotations;

namespace Cms.Model.Models.Content.Db
{
    public class DbContentEvent
    {
        [Key]
        public int Id { get; set; }

        #region Fields ignored in audit log
        public string ContentCode { get; set; }
        #endregion

        #region Fields tracked in audit log
        [Action(EntityActions.UPDATE_START_DATE)]
        public DateTimeOffset? StartDate { get; set; }

        [Action(EntityActions.UPDATE_END_DATE)]
        public DateTimeOffset? EndDate { get; set; }

        [Action(EntityActions.UPDATE_REGISTATION_LINK)]
        public string RegistrationLink { get; set; }

        [Action(EntityActions.UPDATE_RECURRENCE)]
        public string Recurrence { get; set; }
        #endregion

        public string LocationName { get; set; }
        public string AddressLine { get; set; }
        public string AddressLine2 { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public bool IsVirtualEvent { get; set; }
    }
}
