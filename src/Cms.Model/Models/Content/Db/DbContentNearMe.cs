﻿using System.ComponentModel.DataAnnotations;

namespace Cms.Model.Models.Content.Db
{
    public class DbContentNearMe
    {
        [Key]
        public int Id { get; set; }
        public string ContentCode { get; set; }
        public string AboveListContent { get; set; }
        public string RelatedInformationHeadline { get; set; }
        public string RelatedInformationText { get; set; }
    }
}
