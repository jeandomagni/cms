﻿using Cms.Model.Attributes;
using Cms.Model.Enum;
using System;
using System.ComponentModel.DataAnnotations;

namespace Cms.Model.Models.Content.Db
{
    public class DbContent
    {
        [Key]
        public int Id { get; set; }

        #region Fields ignored in audit log
        public string Code { get; set; }
        public DateTimeOffset? CreateDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string Status { get; set; }
        public string CheckoutBy { get; set; }
        public int TotalCount { get; set; }
        public string EventBbb { get; set; }
        public string TopicsAndTags { get; set; }
        public bool Deleted { get; set; }
        public string GeoTags { get; set; }
        public string PinnedLocations { get; set; }
        public string OwnerSiteId { get; set; }
        public string Locations { get; set; }
        public DateTimeOffset? CheckoutDate { get; set; }
        public string BbbAuthor { get; set; }
        public string UriSegment { get; set; }
        #endregion

        #region Fields tracked in audit log
        [Action(EntityActions.UPDATE_HEADLINE)]
        public string Title { get; set; }

        [Action(EntityActions.UPDATE_TEXT)]
        public string ContentHtml { get; set; }

        [Action(EntityActions.UPDATE_SUMMARY)]
        public string Summary { get; set; }

        [Action(EntityActions.UPDATE_TYPE)]
        public string Type { get; set; }

        [Action(EntityActions.UPDATE_TAGS)]
        public string Tags { get; set; }

        [Action(EntityActions.UPDATE_CATEGORIES)]
        public string Categories { get; set; }

        [Action(EntityActions.UPDATE_PUBLISH_DATE)]
        public DateTimeOffset? PublishDate { get; set; }

        [Action(EntityActions.UPDATE_EXPIRATION_DATE)]
        public DateTimeOffset? ExpirationDate { get; set; }

        [Action(EntityActions.UPDATE_AUTHOR)]
        public string Authors { get; set; }

        [Action(EntityActions.UPDATE_STATES)]
        public string States { get; set; }

        [Action(EntityActions.UPDATE_PINNED_STATES)]
        public string PinnedStates { get; set; }

        [Action(EntityActions.UPDATE_CITIES)]
        public string Cities { get; set; }

        [Action(EntityActions.UPDATE_PINNED_CITIES)]
        public string PinnedCities { get; set; }

        [Action(EntityActions.UPDATE_COUNTRIES)]
        public string Countries { get; set; }

        [Action(EntityActions.UPDATE_PINNED_COUNTRIES)]
        public string PinnedCountries { get; set; }

        [Action(EntityActions.UPDATE_PAGE_TITLE)]
        public string PageTitle { get; set; }

        [Action(EntityActions.UPDATE_TOPICS)]
        public string Topics { get; set; }

        [Action(EntityActions.UPDATE_PRIORITY)]
        public int Priority { get; set; }
        
        [Action(EntityActions.UPDATE_META_DESCRIPTION)]
        public string MetaDescription { get; set; }

        [Action(EntityActions.UPDATE_SITES)]
        public string Sites { get; set; }

        [Action(EntityActions.UPDATE_NO_INDEX)]
        public bool NoIndex { get; set; }
        
        [Action(EntityActions.UPDATE_LAYOUT)]
        public string Layout { get; set; }

        [Action(EntityActions.UPDATE_IMAGE)]
        public string Image { get; set; }

        [Action(EntityActions.UPDATE_PINNED_TOPIC)]
        public string PinnedTopic { get; set; }

        [Action(EntityActions.UPDATE_MOBILE_HEADLINE)]
        public string MobileHeadline { get; set; }

        [Action(EntityActions.UPDATE_VIDEOS)]
        public string Videos { get; set; }

        [Action(EntityActions.UPDATE_URL)]
        public string Canonical { get; set; }

        [Action(EntityActions.UPDATE_SEO_CANONICAL)]
        public string SeoCanonical { get; set; }

        [Action(EntityActions.UPDATE_SERVICE_AREA)]
        public string BbbIds { get; set; }

        [Action(EntityActions.UPDATE_PINNED_SERVICE_AREA)]
        public string PinnedBbbIds { get; set; }

        [Action(EntityActions.UPDATE_REDIRECT_URL)]
        public string RedirectUrl { get; set; }
        #endregion
    }
}
