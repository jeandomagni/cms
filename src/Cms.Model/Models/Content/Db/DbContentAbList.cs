﻿using System.ComponentModel.DataAnnotations;

namespace Cms.Model.Models.Content.Db
{
    public class DbContentAbList
    {
        [Key]
        public int Id { get; set; }
        public string ContentCode { get; set; }
        public string BusinessLinkLists { get; set; }
    }
}
