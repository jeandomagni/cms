﻿using Cms.Model.Models.Recurrence;
using System;
using System.ComponentModel.DataAnnotations;

namespace Cms.Model.Models.Content
{
    public class ContentEvent
    {
        [Key]
        public int Id { get; set; }
        public string ContentCode { get; set; }
        public DateTimeOffset? StartDate { get; set; }
        public DateTimeOffset? EndDate { get; set; }
        public string RegistrationLink { get; set; }
        public string LocationName { get; set; }
        public string AddressLine { get; set; }
        public string AddressLine2 { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public EventRecurrence Recurrence { get; set; }
        public bool HasRecurrence { get; set; }
        public bool IsVirtualEvent { get; set; }
    }
}
