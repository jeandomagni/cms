﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cms.Model.Models.General
{
    public class LeadsResultsModel
    {
        public int TotalResults { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public List<LeadVM> LeadsResults { get; set; }
    }

    public class LeadVM
    {
        public int LeadId { get; set; }
        public string LeadType { get; set; }
        public string OrganizationType { get; set; }
        public string OrganizationName { get; set; }
        public string BBBId { get; set; }
        public int? OrganizationId { get; set; }
        public int? RequestCount { get; set; }
        public string PrimaryCategory { get; set; }
        public string ReporterFirstName { get; set; }
        public string ReporterLastName { get; set; }
        public string ReporterEmail { get; set; }
        public string ReporterNotes { get; set; }
        public string ReporterType { get; set; }
        public string ReporterTOB { get; set; }
        public string ReporterPhoneNumber { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string StateCode { get; set; }
        public string PostalCode { get; set; }
        public string CountryCode { get; set; }
        public string Phone { get; set; }
        public string URL { get; set; }
        public string Email { get; set; }
        public string AreaCode { get; set; }
        public DateTime? TimeStamp { get; set; }
        public DateTime DateTimeCreated { get; set; }
        public bool Deleted { get; set; }
        public int? NumberEmployees { get; set; }
        public int? NumberPartTimeEmployees { get; set; }
        public string BusinessStartDate { get; set; }
    }
}
