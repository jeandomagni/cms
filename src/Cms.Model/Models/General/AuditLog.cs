﻿using System;

namespace Cms.Model.Models.General
{
    public class AuditLog
    {
        public DateTimeOffset CreatedDate { get; set; }
        public string Summary { get; set; }
    }
}
