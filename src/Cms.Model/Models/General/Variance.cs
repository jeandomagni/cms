﻿namespace Cms.Model.Models.General
{
    public class Variance
    {
        public string Action { get; set; }
        public string PropertyName { get; set; }
        public object ValueA { get; set; }
        public object ValueB { get; set; }
    }
}
