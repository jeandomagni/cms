﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Cms.Model.Models.General
{
    public class DbAuditLog
    {
        [Key]
        public int Id { get; set; }
        public string UserId { get; set; }
        public int EntityId { get; set; }
        public string EntityCode { get; set; }
        public string ActionCode { get; set; }
        public string Comment { get; set; }
        public string Serialized { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
    }
}
