﻿using Bbb.Core.Solr.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cms.Model.Models
{
    public sealed class QuoteTagType
    {
        public const string All = "*";
        public const string Generic = "generic";
        public const string State = "state";
        public const string City = "city";
        public const string Country = "country";
    }

    public class QuoteTag : IEquatable<QuoteTag>, IComparable<QuoteTag>
    {
        /// <summary>
        /// Creates a quote tag from a location and a type specifier
        /// </summary>
        /// <param name="type"></param>
        /// <param name="location"></param>
        public QuoteTag(Bbb.Core.Solr.Model.Location location, string type, bool includeAssociatedTags = true)
        {
            if (string.Equals(type, QuoteTagType.City, StringComparison.InvariantCultureIgnoreCase))
            {
                DisplayLocation = $"{location.City}, {location.State}";
                Name = $"{location.City}, {location.State} {location.CountryCode}";
                Value = $"{location.CitySeo}-{location.State}-{location.CountryCode}".ToLower();
                Type = ImageTagType.City;

                if (includeAssociatedTags) {
                    AssociatedTags = new List<QuoteTag> {
                        new QuoteTag(location, QuoteTagType.State, false),
                        new QuoteTag(location.CountryCode)
                    };
                };
            }

            else if (string.Equals(type, QuoteTagType.State, StringComparison.InvariantCultureIgnoreCase))
            {
                DisplayLocation = $"{location.StateFull}, {location.CountryCode}";
                Name = location.StateFull;
                Value = location.State.ToLower();
                Type = QuoteTagType.State;

                if (includeAssociatedTags)
                {
                    AssociatedTags = new List<QuoteTag> { new QuoteTag(location.CountryCode) };
                };
            }
        }

        /// <summary>
        /// Creates a quote tag from a country code
        /// </summary>
        /// <param name="countryCode"></param>
        public QuoteTag(CountryCode countryCode)
        {
            var matchingCountryTag = CountryTags
                .Where(x => string.Equals(x.Value, countryCode.ToString(), StringComparison.InvariantCultureIgnoreCase))
                .FirstOrDefault();

            if (matchingCountryTag != null)
            {
                DisplayLocation = matchingCountryTag.DisplayLocation;
                Name = matchingCountryTag.Name;
                Value = matchingCountryTag.Value;
                Type = QuoteTagType.Country;
            }
        }
        
        /// <summary>
        /// Creates an empty quote tag
        /// </summary>
        public QuoteTag() { }

        public static readonly QuoteTag[] CountryTags = new QuoteTag[]
        {
            new QuoteTag { DisplayLocation = "United States", Name = "United States", Value = "usa", Type = QuoteTagType.Country },
            new QuoteTag { DisplayLocation = "Canada", Name = "Canada", Value = "can", Type = QuoteTagType.Country },
            new QuoteTag { DisplayLocation = "Mexico", Name = "Mexico", Value = "mex", Type = QuoteTagType.Country }
        };

        public static readonly QuoteTag[] CommonImageTags = new QuoteTag[]
        {
            new QuoteTag { Name = "Quote", Value="quote", Type = QuoteTagType.Generic}
        };

        public string Name { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
        public string DisplayLocation { get; set; }
        
        /// <summary>
        /// Tracks other values that should be automatically tagged.
        /// (e.g., state and country for city tags -- see WEB-4167)
        /// </summary>
        public IList<QuoteTag> AssociatedTags { get; set; }

        public int CompareTo(QuoteTag other)
        {
            return Name.CompareTo(other.Name);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as QuoteTag);
        }

        public bool Equals(QuoteTag other)
        {
            return other != null && Name == other.Name;
        }

        public override int GetHashCode()
        {
            return 539060726 + EqualityComparer<string>.Default.GetHashCode(Name);
        }
    }

   
}
