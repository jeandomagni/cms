﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cms.Model.Models
{
    public class TobAndCultureId
    {
        /// <summary>
        /// Tob Id, Name, Title or Url
        /// </summary>
        public string Tob { get; set; }
        public string CultureInfo { get; set; }
    }
}
