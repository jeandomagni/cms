﻿
using Cms.Model.Models.Email;
using System.Collections.Generic;

namespace Cms.Model.Models.Config
{
    public class ConnectionStrings
    {
        public string CoreCmsDb { get; set; }
        public string CoreDb { get; set; }
        public string ApiCoreDb { get; set; }
        public string SolrDb { get; set; }
        public string SolrCloudDb { get; set; }
        public string PheonixApi { get; set; }
        public string TerminusBaseUrl { get; set; }
        public string BbbInfoApiUrl { get; set; }
        public string LeadsApiUrl { get; set; }
        public string ArticleOrderSummaryApiUrl { get; set; }
        public string ArticleTypeToListingTypeMappingApiUrl { get; set; }
    }

    public class MediaSettings
    {
        public string FileUploadPath { get; set; }
        public string BaseUrl { get; set; }
    }

    public class AppSecurity
    {
        public string TokenSigningKey { get; set; }
        public string CorecmsApiKey { get; set; }
    }

    public class LogLevel
    {
        public string Default { get; set; }
    }

    public class General
    {
        public string DefaultBbbId { get; set; }
        public bool EnableSoftDelete { get; set; }
        public string GoogleMapsKey { get; set; }
        public string BbbHomePageBaseUrl { get; set; }
        public string CouncilBbbId { get; set; }
        public bool UseV2Database { get; set; }
        public bool ShowUnifiedComplaintFields { get; set; }
    }

    public class Logging
    {
        public bool IncludeScopes { get; set; }
        public LogLevel LogLevel { get; set; }
    }

    public class EmailSettings
    {
        public string SmtpServer { get; set; }
        public int SmtpPort { get; set; }
        public string SmtpUsername { get; set; }
        public string SmtpPassword { get; set; }
    }

    public class TwilioSettings
    {
        public string Sid { get; set; }
        public string Token { get; set; }
        public string Phone { get; set; }
    }

    public class ContentReviewSettings
    {
        public List<EmailAddress> FromEmailAddresses { get; set; }
        public List<EmailAddress> ToEmailAddresses { get; set; }
    }

    public class Authentication
    {
        public bool EnableAccountLocking { get; set; } = true;
        public int MaxLoginAttempt { get; set; } = 3;


    }


    public class LeadsApiSettings
    {
        public string Token { get; set; }
    }
}
