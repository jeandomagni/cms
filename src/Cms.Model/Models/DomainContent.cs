﻿using System;

namespace Cms.Model.Models
{
    public class DomainContent
    {
        public int Id { get; set; }
        public string ContentCode { get; set; }
        public string Domain { get; set; }
        public string DomainTitle { get; set; }
        public string Tags { get; set; }
        public DateTimeOffset DomainExpirationDate { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset ModifyDate { get; set; }
        public string ModifiedBy { get; set; }
    }
}
