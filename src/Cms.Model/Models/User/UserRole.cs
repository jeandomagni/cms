﻿using System;

namespace Cms.Model.Models.User
{
    public class UserRole
    {
        public Guid? UserId { get; set; }
        public Guid? RoleId { get; set; }
        public string SiteId { get; set; }
        public string LegacyBBBId { get; set; }
        public string Email { get; set; }
        public string RoleName { get; set; }
        public string BBBName { get; set; }
    }
}