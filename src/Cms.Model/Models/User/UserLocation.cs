﻿using System.ComponentModel.DataAnnotations;

namespace Cms.Model.Models.User
{
    public class UserLocation
    {
        [Key]
        public int Id { get; set; }
        public string UserId { get; set; }
        public string LocationId { get; set; }
        public string DisplayName { get; set; }
    }
}
