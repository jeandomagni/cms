﻿using System;
using System.Collections.Generic;

namespace Cms.Model.Models.User
{
    public class UserProfile
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string BaseUriCulture { get; set; }
        public string BaseUriCountry { get; set; }
        public string DefaultSite { get; set; }
        public bool? OpenContentInNewTab { get; set; }
        public string PinnedTopic { get; set; }
        public string Topics { get; set; }
        public bool TwoFactorAuthEnabled { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string PhoneNumber { get; set; }
        public bool PhoneNumberVerified { get; set; }
        public string DisplayPhoneNumber
        {
            get
            {
                return string.IsNullOrEmpty(PhoneNumber) ?
                    string.Empty : $"***-***-{PhoneNumber.Substring(Math.Max(0, PhoneNumber.Length - 4))}";
            }
        }

        public Location DefaultSiteCity { get; set; }
        public string DefaultSiteState { get; set; }
        public string DefaultSiteName { get; set; }
        public string DefaultSiteCountry { get; set; }
    }
}
