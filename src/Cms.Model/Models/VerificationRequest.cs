﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Cms.Model.Models
{
    public class VerificationRequest
    {
        [Key]
        public int Id { get; set; }
        public string Code { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? CreatedDate { get; set; }
    }
}
