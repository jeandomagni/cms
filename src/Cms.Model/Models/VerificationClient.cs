﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Cms.Model.Models
{
    public class VerificationClient
    { 
        [Key]
        public int Id { get; set; }
        public string ClientId { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset? CreatedDate { get; set; }
    }
}
