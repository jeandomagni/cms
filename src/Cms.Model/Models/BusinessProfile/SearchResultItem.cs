﻿namespace Cms.Model.Models.BusinessProfile
{
    public class SearchResultItem
    {
        public string BbbId { get; set; }
        public string BusinessId { get; set; }
        public string BusinessName { get; set; }
        public string DisplayAddress { get; set; }
        public string Id { get; set; }
        public string LogoUrl { get; set; }
        public string ProfileUrl { get; set; }
    }
}
