﻿namespace Cms.Model.Models.BusinessProfile.Db
{
    public class DbBusinessProfileSeo
    {
        public int Id { get; set; }
        public string BbbId { get; set; }
        public string BusinessId { get; set; }
        public string MetadataJson { get; set; }
    }
}
