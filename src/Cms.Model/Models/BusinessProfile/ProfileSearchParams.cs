﻿namespace Cms.Model.Models.BusinessProfile
{
    public class ProfileSearchParams
    {
        public int PageNumber { get; set; } = 1;
        public int PageSize { get; set; } = 15;
        public string SearchText { get; set; }
    }
}
