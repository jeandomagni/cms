﻿using System.Collections.Generic;

namespace Cms.Model.Models.BusinessProfile
{
    public class BusinessSearchResult
    {
        public IEnumerable<SearchResultItem> Items { get; set; }
        public int PageCount { get; set; }
        public int ResultCount { get; set; }
        public ProfileSearchParams SearchParams { get; set; }
    }
}
