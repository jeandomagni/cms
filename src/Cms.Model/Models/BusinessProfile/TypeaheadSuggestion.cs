﻿namespace Cms.Model.Models.BusinessProfile
{
    public class TypeaheadSuggestion
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string SecondaryTitle { get; set; }
    }
}
