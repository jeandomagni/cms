﻿using Cms.Model.Models.SecTerm;
using System.Collections.Generic;

namespace Cms.Model.Models.BusinessProfile
{
    public class BusinessProfile
    {
        public string BbbId { get; set; }
        public string BusinessId { get; set; }
        public string BusinessName { get; set; }
        public string DisplayAddress { get; set; }
        public string Id { get; set; }
        public bool IsAccredited { get; set; }
        public bool IsRaqActive { get; set; }
        public string LogoUrl { get; set; }
        public Metadata Metadata { get; set; }
        public int? ProfileSeoId { get; set; }
        public string ProfileUrl { get; set; }
    }
}
