﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;

namespace Cms.Model.Models
{
    public class File
    {
        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        public string Uri { get; set; }
        public string Code { get; set; }
        public string Path { get; set; }
        public string Tags { get; set; }
        public List<ImageTag> TagItems => DeserializeTags();
        public string ContentType { get; set; }
        public DateTimeOffset? CreateDate { get; set; }
        public string CreatedBy { get; set; }
        public bool Deleted { get; set; }
        public int TotalCount { get; set; }
        public string OwnerSiteId { get; set; }
        public string Type { get; set; }
        public bool Published { get; set; }
        public int FileSizeKb { get; set; }
        public string Caption { get; set; }
        public string Credit { get; set; }

        private List<ImageTag> DeserializeTags()
        {

            try
            {
                var tagList = !String.IsNullOrEmpty(Tags) ? JsonConvert.DeserializeObject<List<ImageTag>>(Tags) : new List<ImageTag>();

                foreach (var tag in tagList.Where(x => x.Type == "generic"))
                {
                    if (string.IsNullOrWhiteSpace(tag.Value)) { tag.Value = Thread.CurrentThread.CurrentCulture.TextInfo.ToLower(tag.Name); };
                    tag.Name = Thread.CurrentThread.CurrentCulture.TextInfo.ToLower(tag.Name);
                }

                var groupedTagList = tagList.GroupBy(x => x.Name); // we group and remove duplicates for items with same label but different case

                return groupedTagList.Select(x => x.FirstOrDefault()).ToList();
            }
            catch
            {
                return new List<ImageTag>();
            }
        }
    }
}
