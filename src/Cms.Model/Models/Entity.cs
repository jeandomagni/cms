﻿using System.ComponentModel.DataAnnotations;

namespace Cms.Model.Models
{
    public class Entity
    {
        [Key]
        public string Code;
        public string Type;
        public string Data;
    }
}
