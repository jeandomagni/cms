﻿namespace Cms.Model.Models.SecTerm
{
    public class AddEditAliasParams
    {
        public SecTerm SecTerm { get; set; }
        public TobAlias Alias { get; set; }
    }
}
