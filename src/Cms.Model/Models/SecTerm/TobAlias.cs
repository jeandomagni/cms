﻿using System;

namespace Cms.Model.Models.SecTerm
{
    public class TobAlias
    {
        public int? AliasId { get; set; }
        public string Name { get; set; }
        public string Spanish { get; set; }
        public DateTime? DateTimeCreated { get; set; }
        public int? CreatorUserId { get; set; }
    }
}
