﻿using System;

namespace Cms.Model.Models.SecTerm.Db
{
    public class DbTobAlias
    {
        public int? AliasId { get; set; }
        public string TobId { get; set; }
        public string Description { get; set; }
        public string TobAlias { get; set; }
        public string UrlSegment { get; set; }
        public string Spanish { get; set; }
        public int CreatorUserId { get; set; }
        public DateTime DateTimeCreated { get; set; }
    }
}
