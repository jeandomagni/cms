﻿namespace Cms.Model.Models.SecTerm.Db
{
    public class DbCategory
    {
        public int CategoryId { get; set; }
        public string TOBId { get; set; }
        public string CategoryName { get; set; }
        public string CategoryURL { get; set; }
        public string Spanish { get; set; }
    }
}
