﻿using System;

namespace Cms.Model.Models.SecTerm.Db
{
    public class DbSecTerm
    {
        public int CategoryId { get; set; }
        public string CategoryCode { get; set; }
        public string TobId { get; set; }
        public string Name { get; set; }
        public string UrlSegment { get; set; }
        public string SpanishName { get; set; }
        public string SpanishUrlSegment { get; set; }
        public bool IsActive { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ModifiedUser { get; set; }
        public bool IsEditable { get; set; }
        public int TotalCount { get; set; }
        public string AliasesJson { get; set; }
        public string MetadataJson { get; set; }
        public string MetadataBusinessJson { get; set; }
    }
}
