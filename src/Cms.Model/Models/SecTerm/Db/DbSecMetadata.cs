﻿using System;

namespace Cms.Model.Models.SecTerm.Db
{
    public class DbSecMetadata
    {
        public string TobId { get; set; }
        public bool IsActive { get; set; }
        public string ModifiedUser { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string MetadataJson { get; set; }
        public string MetadataBusinessJson { get; set; }
    }
}
