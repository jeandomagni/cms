﻿using System;
using System.Collections.Generic;

namespace Cms.Model.Models.SecTerm
{
    public class SecTerm
    {
        public int CategoryId { get; set; }
        public string CategoryCode { get; set; }
        public string TobId { get; set; }
        public string Name { get; set; }
        public string UrlSegment { get; set; }
        public string SpanishName { get; set; }
        public string SpanishUrlSegment { get; set; }
        public bool IsActive { get; set; }
        public bool IsEditable { get; set; }
        public string ModifiedUser { get; set; }
        public DateTime ModifiedDate { get; set; }
        public IEnumerable<TobAlias> Aliases { get; set; }
        public IEnumerable<Metadata> Metadata { get; set; }
        public IEnumerable<Metadata> MetadataBusiness { get; set; }
    }
}