﻿using System.Collections.Generic;

namespace Cms.Model.Models.SecTerm
{
    public class SecTermResults
    {
        public int TotalCount { get; set; }
        public IEnumerable<SecTerm> Results { get; set; }
    }
}
