﻿namespace Cms.Model.Models.SecTerm
{
    public class MetadataByType
    {
        public string Heading { get; set; }
        public string PageTitle { get; set; }
        public string MetaDesc { get; set; }
    }
}
