﻿using System.Collections.Generic;

namespace Cms.Model.Models.SecTerm
{
    public class Metadata
    {
        public string Type { get; set; }
        public List<string> CultureIds { get; set; }
        public string PageTitle { get; set; }
        public string MetaDesc { get; set; }
    }
}
