﻿using System.Collections.Generic;

public class LegacyBbbInfo
{
    public string AbDirectoryUrl { get; set; }
    public string Active { get; set; }
    public string Addressline1 { get; set; }
    public string Addressline2 { get; set; }
    public string AllowURIAliasesForVendorContent { get; set; }
    public string BaseUrl { get; set; }
    public int BaselineSiteId { get; set; }
    public string BbbFolderName { get; set; }
    public string BbbLocationName { get; set; }
    public string BbbName { get; set; }
    public string Businessprofilesource { get; set; }
    public string Charityprofilesource { get; set; }
    public string City { get; set; }
    public bool DisableNewHeaderAndFooter { get; set; }
    public string Email { get; set; }
    public string EpomKey130x130HomePageSquarePromo { get; set; }
    public string EpomKey160x100RightRailPromo { get; set; }
    public string EpomKey216x60HalfBannerPromo { get; set; }
    public string EpomKey228x100LeftRailPromo { get; set; }
    public string EpomKey286x125GeneralPromo { get; set; }
    public string EpomKey287x148LocalSponsor { get; set; }
    public string EpomKey468x60FullBannerPromo { get; set; }
    public string GoogleAnalyticsId { get; set; }
    public string LegacyBBBID { get; set; }
    public int LegacySiteID { get; set; }
    public string Phonenumber { get; set; }
    public string PrimaryUrlAlias { get; set; }
    public string State { get; set; }
    public List<string> Urlaliases { get; set; }
    public string Vendor { get; set; }
    public string Vendorhostheader { get; set; }
    public string Zip { get; set; }
    public string CountryCode { get; set; }
    
}
