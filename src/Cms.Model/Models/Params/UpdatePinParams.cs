﻿using Cms.Model.Models.ContentLocation;

namespace Cms.Model.Models.Params
{
    public class UpdatePinParams
    {
        public Content.Content Content { get; set; }
        public PinnedContentLocation Pin { get; set; }
    }
}
