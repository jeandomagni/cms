﻿using System.Collections.Generic;

namespace Cms.Model.Models.Params
{
    public class PaginateCategoryParams
    {
        public string FilterValue;
        public int PageSize;
        public IList<string> ExcludedCategories;
        public IList<string> Countries;
        public bool QueryPrimaryOnly;
        public bool ShowHighRiskCategories;
    }
}
