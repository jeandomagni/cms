﻿namespace Cms.Model.Models.Params
{
    public class UpdateLocationParams
    {
        public Content.Content Content { get; set; }
        public ContentLocation.ContentLocation Location { get; set; }
    }
}
