﻿namespace Cms.Model.Models.Params
{
    public class QueryLocationParams
    {
        public string Query;
        public string[] CountryCodes;
        public string Context;
        public string LocationType;
    }
}
