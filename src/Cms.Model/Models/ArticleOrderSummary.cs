﻿using System;

namespace Cms.Model.Models
{
    public class ArticleOrderSummary
    {
        public string Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string PinnedCity { get; set; }
        public string PinnedState { get; set; }
        public string PinnedCountry { get; set; }
        public DateTime? PublishDate { get; set; }
        public int? Priority { get; set; }
        public int Order { get; set; }
        public string ImageUrl { get; set; }
    }
}
