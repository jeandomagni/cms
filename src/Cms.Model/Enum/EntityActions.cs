﻿using System.Collections.Generic;

namespace Cms.Model.Enum
{
    public static class EntityActions
    {
        public const string CREATE = "CREATE";
        public const string UPDATE = "UPDATE";
        public const string DELETE = "DELETE";
        public const string PUBLISH = "PUBLISH";
        public const string UNPUBLISH = "UNPUBLISH";
        public const string APPROVE = "APPROVE";
        public const string LOGIN = "LOGIN";
        public const string CHECKOUT = "CHECKOUT";
        public const string CHECKIN = "CHECKIN";

        // content
        public const string UPDATE_IMAGE = "UPDATE_IMAGE";
        public const string UPDATE_CAROUSEL = "UPDATE_CAROUSEL";
        public const string UPDATE_TEXT = "UPDATE_TEXT";
        public const string UPDATE_SUMMARY = "UPDATE_SUMMARY";
        public const string UPDATE_HEADLINE = "UPDATE_HEADLINE";
        public const string UPDATE_PUBLISH_DATE = "UPDATE_PUBLISH_DATE";
        public const string UPDATE_CITIES = "UPDATE_CITIES";
        public const string UPDATE_COUNTRIES = "UPDATE_COUNTRIES";
        public const string UPDATE_STATES = "UPDATE_STATES";
        public const string UPDATE_AUTHOR = "UPDATE_AUTHOR";
        public const string UPDATE_PRIORITY = "UPDATE_PRIORITY";
        public const string UPDATE_PAGE_TITLE = "UPDATE_PAGE_TITLE";
        public const string UPDATE_PINNED_CITIES = "UPDATE_PINNED_CITIES";
        public const string UPDATE_PINNED_COUNTRIES = "UPDATE_PINNED_COUNTRIES";
        public const string UPDATE_PINNED_STATES = "UPDATE_PINNED_STATES";
        public const string UPDATE_TOPICS = "UPDATE_TOPICS";
        public const string UPDATE_SITES= "UPDATE_SITES";
        public const string UPDATE_CATEGORIES= "UPDATE_CATEGORIES";
        public const string UPDATE_TYPE = "UPDATE_TYPE";
        public const string UPDATE_TAGS = "UPDATE_TAGS";
        public const string UPDATE_META_DESCRIPTION = "UPDATE_META_DESCRIPTION";
        public const string UPDATE_MOBILE_HEADLINE = "UPDATE_MOBILE_HEADLINE";
        public const string UPDATE_NO_INDEX = "UPDATE_NO_INDEX";
        public const string UPDATE_EXPIRATION_DATE = "UPDATE_EXPIRATION_DATE";
        public const string UPDATE_LAYOUT = "UPDATE_LAYOUT";
        public const string UPDATE_PINNED_TOPIC = "UPDATE_PINNED_TOPIC";
        public const string UPDATE_VIDEOS = "UPDATE_VIDEOS";
        public const string UPDATE_URL = "UPDATE_URL";
        public const string UPDATE_SEO_CANONICAL = "UPDATE_SEO_CANONICAL";
        public const string UPDATE_SERVICE_AREA = "UPDATE_SERVICE_AREA";
        public const string UPDATE_PINNED_SERVICE_AREA = "UPDATE_PINNED_SERVICE_AREA";
        public const string UPDATE_REDIRECT_URL = "UPDATE_REDIRECT_URL";

        // events
        public const string UPDATE_START_DATE = "UPDATE_START_DATE";
        public const string UPDATE_END_DATE = "UPDATE_END_DATE";
        public const string UPDATE_REGISTATION_LINK = "UPDATE_REGISTATION_LINK";
        public const string EVENT_BBB = "EVENT_BBB";
        public const string UPDATE_RECURRENCE = "UPDATE_RECURRENCE";

        // bbb info
        public const string ADDITIONAL_RESOURCES = "ADDITIONAL_RESOURCES";
        public const string UPDATE_VENDOR_HOST_HEADER = "UPDATE_VENDOR_HOST_HEADER";
        public const string UPDATE_VENDOR = "UPDATE_VENDOR";
        public const string UPDATE_TAGLINE = "UPDATE_TAGLINE";
        public const string UPDATE_SUBMIT_REVIEW_URL = "UPDATE_SUBMIT_REVIEW_URL";
        public const string UPDATE_SUBDOMAINS = "UPDATE_SUBDOMAINS";
        public const string UPDATE_SOCIAL_MEDIA = "UPDATE_SOCIAL_MEDIA";
        public const string UPDATE_PROGRAMS = "UPDATE_PROGRAMS";
        public const string UPDATE_PRIMARY_COUNTRY = "UPDATE_PRIMARY_COUNTRY";
        public const string UPDATE_PHONE_LEADS_EMAILS = "UPDATE_PHONE_LEADS_EMAILS";
        public const string UPDATE_PERSONS = "UPDATE_PERSONS";
        public const string UPDATE_OPEN_BODY_SCRIPT = "UPDATE_OPEN_BODY_SCRIPT";
        public const string UPDATE_NEWS_LETTER_SIGNUP_URL = "UPDATE_NEWS_LETTER_SIGNUP_URL";
        public const string UPDATE_LEADSGEN_LINK_ENABLED = "UPDATE_LEADSGEN_LINK_ENABLED";
        public const string UPDATE_LOCATIONS = "UPDATE_LOCATIONS";
        public const string UPDATE_INFORMATION_PAGE_IMAGE_URL = "UPDATE_INFORMATION_PAGE_IMAGE_URL";
        public const string UPDATE_HEAD_SCRIPT = "UPDATE_HEAD_SCRIPT";
        public const string UPDATE_HEADER_LINKS = "UPDATE_HEADER_LINKS";
        public const string UPDATE_MENU_LINKS = "UPDATE_MENU_LINKS";
        public const string UPDATE_HEADER_FOOTER_FLAG = "UPDATE_HEADER_FOOTER_FLAG";
        public const string UPDATE_GOOGLE_ANALYTICS_ID = "UPDATE_GOOGLE_ANALYTICS_ID";
        public const string UPDATE_GATEWAY_ENABLED_FLAG = "UPDATE_GATEWAY_ENABLED_FLAG";
        public const string UPDATE_RAQ_ENABLED_FLAG = "UPDATE_RAQ_ENABLED_FLAG";
        public const string UPDATE_FOOTER_LINKS = "UPDATE_FOOTER_LINKS";
        public const string UPDATE_FILE_COMPLAINT_URL = "UPDATE_FILE_COMPLAINT_URL";
        public const string UPDATE_EVENTS_PAGE_LAYOUT = "UPDATE_EVENTS_PAGE_LAYOUT";
        public const string UPDATE_EMPLOYMENT_OPPORTUNITIES_URL  = "UPDATE_EMPLOYMENT_OPPORTUNITIES_URL";
        public const string UPDATE_CONSUMER_LOGIN_URL = "UPDATE_CONSUMER_LOGIN_URL";
        public const string UPDATE_CLOSE_BODY_SCRIPT = "UPDATE_CLOSE_BODY_SCRIPT";
        public const string UPDATE_CLAIMS_LISTING_URL  = "UPDATE_CLAIMS_LISTING_URL";
        public const string UPDATE_CHARITY_PROFILE_SOURCE  = "UPDATE_CHARITY_PROFILE_SOURCE";
        public const string UPDATE_BUSINESS_LOGIN_URL  = "UPDATE_BUSINESS_LOGIN_URL";
        public const string UPDATE_BBB_URL_SEGMENT  = "UPDATE_BBB_URL_SEGMENT";
        public const string UPDATE_BASE_URI_ALIASES  = "UPDATE_BASE_URI_ALIASES";
        public const string UPDATE_AREA_CODES  = "UPDATE_AREA_CODES";
        public const string UPDATE_ACCREDITATION_APPLY_URL  = "UPDATE_ACCREDITATION_APPLY_URL";
        public const string UPDATE_JOIN_APPLY_URL  = "UPDATE_JOIN_APPLY_URL";
        public const string UPDATE_ANNUAL_REPORTS_URL  = "UPDATE_ANNUAL_REPORTS_URL";
        public const string UPDATE_VENDOR_ALIASES  = "UPDATE_VENDOR_ALIASES";
        public const string UPDATE_ADDITIONAL_RESOURCES  = "UPDATE_ADDITIONAL_RESOURCES";
        public const string UPDATE_ABOUT_US_TEXT  = "UPDATE_ABOUT_US_TEXT";
        public const string UPDATE_LANGUAGES  = "UPDATE_LANGUAGES";
        public const string UPDATE_POPULAR_CATEGORIES = "UPDATE_POPULAR_CATEGORIES";
        public const string UPDATE_COMPLAINT_QUALIFYING_QUESTIONS = "UPDATE_COMPLAINT_QUALIFYING_QUESTIONS";
        public const string UPDATE_PRIMARY_URL_ALIAS = "UPDATE_PRIMARY_URL_ALIAS";
        public const string UPDATE_APPLY_FOR_ACCREDITATION_INFO = "UPDATE_APPLY_FOR_ACCREDITATION_INFO";
        public const string UPDATE_BASE_URL= "UPDATE_BASE_URL";
        public const string UPDATE_BUSSINESS_LEAD_EMAIL_NOTIFICATION_ENABLED = "UPDATE_BUSSINESS_LEAD_EMAIL_NOTIFICATION_ENABLED";


        // #WEB-1732 introduces a ton of updates that should have no bearing on modified date.
        // Instead of specifying those, it's easier to specify what should update modified date.
        public static List<string> VISIBLE_UPDATES = new List<string>()
        {
            UPDATE_TEXT,
            UPDATE_SUMMARY,
            UPDATE_HEADLINE,
            UPDATE_AUTHOR,
            UPDATE_PAGE_TITLE,
            UPDATE_TYPE,
            UPDATE_META_DESCRIPTION,
            UPDATE_MOBILE_HEADLINE,
            UPDATE_START_DATE,
            UPDATE_END_DATE,
            UPDATE_REGISTATION_LINK,
            EVENT_BBB,
        };
    }
}
