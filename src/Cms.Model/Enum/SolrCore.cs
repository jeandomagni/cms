﻿namespace Cms.Model.Enum
{
    public enum SolrCore
    {
        Branch,
        Cibr,
        CityState,
        Content,
        PostCode,
        SubjectCode,
        Wga,
        Yppa,
        APIAuthToken,
        Locations,
        SearchTypeahead,
        Unknown,
        Article,
        BbbInfo,
        Reviews,
        Complaints,
        SecTerms,
        ArticleOrder
    }

    public static class CoreExtensions
    {
        public static string ToRelativeName(this SolrCore core)
        {
            switch (core)
            {
                case SolrCore.SubjectCode:
                    return "episubjectcodesta";
                case SolrCore.Cibr:
                case SolrCore.Wga:
                    return core.ToString().ToUpperInvariant();
                case SolrCore.Article:
                    return "Article";
                case SolrCore.BbbInfo:
                    return "BbbInfo";
                case SolrCore.Reviews:
                    return "reviews";
                case SolrCore.Complaints:
                    return "complaints";
                case SolrCore.ArticleOrder:
                    return "ArticleOrder";
                default:
                    return core.ToString().ToLowerInvariant();
            }
        }
    }
}
