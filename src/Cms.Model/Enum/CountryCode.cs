﻿using System;

namespace Cms.Model.Enum
{
    [Flags]
    public enum CountryCode
    {
        Unknown = 0,
        ALL = 1,
        USA = 2,
        CAN = 4,
        MEX = 8
    }
}
