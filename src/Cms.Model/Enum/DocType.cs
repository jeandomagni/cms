﻿namespace Cms.Model.Enum
{
    public enum DocType
    {
        Combined,
        Business,
        Charity,
        WebPage,
        All
    }
}
