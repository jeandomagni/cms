﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cms.Model.Enum
{
    public enum EntityType
    {
        YPPA,
        NAICS,
        Organization,
        Brand,
        ProdServ,
        Category,
        Unknown
    }
}
