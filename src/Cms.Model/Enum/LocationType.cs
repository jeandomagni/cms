﻿namespace Cms.Model.Enum
{
    public enum LocationType
    {
        PostalCode,
        CityState,
        CityStatePostalCode,
        LatLng,
        Unknown
    }
}
