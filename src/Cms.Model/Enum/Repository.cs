﻿namespace Cms.Model.Enum
{
    public enum Repository
    {
        CoreCmsDb,
        CoreDb,
        ApiCoreDb,
        SolrContentCore,
        SolrBranchCore,
        SolrCategoryCore,
        SolrLocationCore,
        SolrBbbInfoCore,
        PheonixApi,
        BbbInfoApi,
        ArticleOrderSummaryApi,
        ArticleTypeToListingTypeMappingApi,
        LeadsApiUrl
    }
}
