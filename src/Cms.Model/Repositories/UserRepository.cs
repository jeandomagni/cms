﻿using Cms.Model.Enum;
using Cms.Model.Models.User;
using Cms.Model.Services;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Threading.Tasks;
using Cms.Model.Repositories.Helpers;
using SqlKata;
using SqlKata.Compilers;
using SqlKata.Execution;
using User = Cms.Model.Models.UserManagement.User;

namespace Cms.Model.Repositories
{
    public interface IUserRepository
    {
        Task<User> GetByEmail(string email);
        Task<IEnumerable<UserRole>> GetRolesByEmail(string email);
    }

    public class UserRepository : IUserRepository
    {
        private readonly IConnectionStringService _connectionStringService;
        public UserRepository(IConnectionStringService connectionStringService)
        {
            _connectionStringService = connectionStringService;
        }

        public IDbConnection Connection => new SqlConnection
            (_connectionStringService.GetVendorConnectionString(Repository.CoreCmsDb));

        public async Task<User> GetByEmail(string email)
        {
            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                return await dbConnection.GetUserByEmailAsync(email);
            }
        }

        public async Task<IEnumerable<UserRole>> GetRolesByEmail(string email)
        {
            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                return await dbConnection.GetUserRolesByEmailAsync(email);
            }
        }


    }
}
