﻿using Cms.Model.Enum;
using Cms.Model.Models.User;
using Cms.Model.Services;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Cms.Model.Repositories
{
    public interface IUserLocationRepository
    {
        Task<IEnumerable<UserLocation>> GetAllForUser(string userId);
        Task<UserLocation> Add(string userId, string locationId, string locationDisplayName);
        Task<int> Delete(int Id);
    }

    public class UserLocationRepository : IUserLocationRepository
    {
        private readonly IConnectionStringService _connectionStringService;
        public UserLocationRepository(IConnectionStringService connectionStringService)
        {
            _connectionStringService = connectionStringService;
        }
        public IDbConnection Connection => new SqlConnection(
            _connectionStringService.GetVendorConnectionString(Repository.CoreCmsDb));
        public async Task<IEnumerable<UserLocation>> GetAllForUser(string userId)
        {
            using (var dbConnection = Connection)
            {
                var query = @"select * from [dbo].[UserLocation] where UserId = @UserId";

                dbConnection.Open();
                return 
                    await dbConnection.QueryAsync<UserLocation>(query, new { UserId = userId });
            }
        }

        public async Task<UserLocation> Add(string userId, string locationId, string locationDisplayName)
        {
            using (var dbConnection = Connection)
            {
                var query = @"insert into [dbo].[UserLocation] (
                                UserId, 
                                LocationId, 
                                DisplayName)
                                values (
                                @UserId, 
                                @LocationId, 
                                @DisplayName) 
                                select * from [dbo].[UserLocation] where Id = cast(scope_identity() as int)";
                dbConnection.Open();
                return
                    (await dbConnection.QueryAsync<UserLocation>(
                        query, new {
                            UserId = userId,
                            LocationId = locationId,
                            DisplayName = locationDisplayName})).FirstOrDefault();
            }
        }

        public async Task<int> Delete(int id)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"delete from dbo.[UserLocation] where Id = @Id";
                dbConnection.Open();

                return
                    await
                     dbConnection.ExecuteAsync(query, new { Id = id });
            }
        }
    }
}
