﻿using Cms.Model.Enum;
using Cms.Model.Models;
using Cms.Model.Services;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Cms.Model.Repositories.Content
{
    public interface IDomainContentRepository
    {
        Task <DomainContent> Add(DomainContent content);
        Task<DomainContent> Update(DomainContent content);
        Task<DomainContent> GetById(int domainContentId);
        Task<DomainContent> GetByContentCode(string contentCode, string domain);
        Task<int> Delete(int domainContentId);
    }

    public class DomainContentRepository : IDomainContentRepository
    {
        private readonly IConnectionStringService _connectionStringService;

        public DomainContentRepository(IConnectionStringService connectionStringService)
        {
            _connectionStringService = connectionStringService;
        }
        public IDbConnection Connection =>
            new SqlConnection(
                _connectionStringService.GetVendorConnectionString(
                    Repository.CoreCmsDb));

        public async Task<DomainContent> Add(DomainContent domainContent)
        {
            using (var dbConnection = Connection)
            {               
                const string query = @"insert into [dbo].[DomainContent] (	
	                                    ContentCode,
	                                    DomainTitle,
	                                    Domain,
                                        Tags,
	                                    DomainExpirationDate,
	                                    CreatedDate,
                                        CreatedBy                               
                                        )
                                    values (
	                                    @ContentCode,
	                                    @DomainTitle,
	                                    @Domain,
                                        @Tags,
	                                    @DomainExpirationDate,
	                                    sysdatetimeoffset(),
                                        @CreatedBy
                                        )
                                 select * from [dbo].[DomainContent] where Id = cast(scope_identity() as int)"; 
                dbConnection.Open();
                return 
                    (await dbConnection.QueryAsync<DomainContent>(
                        query, domainContent)).FirstOrDefault();
            }
        }

        public async Task<DomainContent> Update(DomainContent domainContent)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"update [dbo].[DomainContent] set 
                                        DomainTitle          = @DomainTitle,
                                        DomainExpirationDate = @DomainExpirationDate,
                                        ModifiedDate         = @ModifiedDate,
                                        ModifiedBy           = @ModifiedBy

                                        where Id = @Id 

                                        select * from [dbo].[DomainContent] where Id = @Id";

                dbConnection.Open();
                return 
                    (await dbConnection.QueryAsync<DomainContent>(
                        query, domainContent)).FirstOrDefault();
            }
        }

        public async Task<DomainContent> GetByContentCode(string contentCode, string domain)
        {
            const string query = @"select * from [dbo].[DomainContent]
                                   where ContentCode = @Code and Domain = @Domain";

            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                return 
                    (await dbConnection.QueryAsync<DomainContent>(
                        query, new { Code = contentCode, Domain  = domain })).FirstOrDefault();
            }
        }

        public async Task<DomainContent> GetById(int domainContentId)
        {
            const string query = @"select * from [dbo].[DomainContent]
                                   where Id = @Id";
            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                return
                    (await dbConnection.QueryAsync<DomainContent>(
                        query, new { Id = domainContentId })).FirstOrDefault();
            }
        }

        public async Task<int> Delete(int domainContentId)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"delete from [dbo].[DomainContent] where Id = @Id";
                dbConnection.Open();

                return
                    await 
                        dbConnection.ExecuteAsync(query, new { Id = domainContentId });
            }
        }
    }
}
