﻿using Cms.Model.Enum;
using Cms.Model.Services;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Cms.Model.Repositories.Content
{
    public interface IContentLocationRepository
    {
        Task<int> DeleteForContent(int contentId);
        Task<bool> Add(int contentId, string locationId, string displayName);
    }

    public class ContentLocationRepository : IContentLocationRepository
    {
        private readonly IConnectionStringService _connectionStringService;

        public ContentLocationRepository(IConnectionStringService connectionStringService)
        {
            _connectionStringService = connectionStringService;
        }

        public IDbConnection Connection =>
            new SqlConnection(
                _connectionStringService.GetVendorConnectionString(Repository.CoreCmsDb));

        public async Task<bool> Add(int contentId, string locationId, string displayName)
        {
            using (var dbConnection = Connection) {
                const string query = @"insert into [dbo].[ContentLocation] (	
                                ContentId, 
                                LocationId,
                                DisplayName)
                            values (
	                            @ContentId,
	                            @LocationId,
	                            @DisplayName)";
                dbConnection.Open();
                return
                    (await dbConnection.ExecuteAsync(
                        query, new { ContentId = contentId, LocationId = locationId, DisplayName = displayName })) > 0;
            }
        }

        public async Task<int> DeleteForContent(int contentId)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"delete from [dbo].[ContentLocation] where ContentId = @ContentId";
                dbConnection.Open();

                return 
                    await dbConnection.ExecuteAsync(query, new { ContentId = contentId });
            }
        }
    }
}
