﻿using Cms.Model.Enum;
using Cms.Model.Models.Content.Db;
using Cms.Model.Services;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Cms.Model.Repositories.Content
{
    public interface IContentAbListRepository
    {
        Task<DbContentAbList> GetByContentCode(string contentCode);
        Task<DbContentAbList> Add(DbContentAbList contentAbList);
        Task<DbContentAbList> Update(DbContentAbList contentAbList);
    }

    public class ContentAbListRepository : IContentAbListRepository
    {
        private readonly IConnectionStringService _connectionStringService;

        public ContentAbListRepository(IConnectionStringService connectionStringService)
        {
            _connectionStringService = connectionStringService;
        }

        public IDbConnection Connection =>
            new SqlConnection(
                _connectionStringService.GetVendorConnectionString(
                    Repository.CoreCmsDb));

        public async Task<DbContentAbList> GetByContentCode(string contentCode)
        {
            const string query = @"select * from [dbo].[ContentAbList] where ContentCode = @Code";

            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                return
                    (await dbConnection.QueryAsync<DbContentAbList>(
                        query, new { Code = contentCode })).FirstOrDefault();
            }
        }

        public async Task<DbContentAbList> Add(DbContentAbList content)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"insert into [dbo].[ContentAbList] (	                         
                                ContentCode, 
                                BusinessLinkLists)
                            values (
	                            @ContentCode,
	                            @BusinessLinkLists)

                             select * from [dbo].[ContentAbList] where Id = cast(scope_identity() as int)";
                dbConnection.Open();
                return
                    (await dbConnection.QueryAsync<DbContentAbList>(query, content)).FirstOrDefault();
            }
        }

        public async Task<DbContentAbList> Update(DbContentAbList content)
        {
            const string query = @"update [dbo].[ContentAbList] set
                                BusinessLinkLists   = @BusinessLinkLists
                  
                                where Id = @Id

                                select * from [dbo].[ContentAbList] where Id = @Id";

            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                return
                    (await dbConnection.QueryAsync<DbContentAbList>(query, content)).FirstOrDefault();
            }
        }
    }
}
