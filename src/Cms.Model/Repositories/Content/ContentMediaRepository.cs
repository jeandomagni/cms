﻿using Cms.Model.Enum;
using Cms.Model.Helpers;
using Cms.Model.Models.Content;
using Cms.Model.Models.Query;
using Cms.Model.Services;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Cms.Model.Repositories.Content
{
    public interface IContentMediaRepository
    {
        Task<ContentMedia> Add(ContentMedia content);
        Task<ContentMedia> GetById(int id);
        Task<IEnumerable<ContentMedia>> GetByContentCode(string contentCode);
        Task<int> Delete(int contentMediaId);
        Task<int> Update(ContentMedia content);
        Task<int> ReOrder(string contentCode, IEnumerable<ContentMediaOrder> contentMediaOrder);
        Task<int> DeleteByContentCode(string contentCode);
    }

    public class ContentMediaRepository : IContentMediaRepository
    {
        private readonly IConnectionStringService _connectionStringService;
        public ContentMediaRepository(IConnectionStringService connectionStringService)
        {
            _connectionStringService = connectionStringService;
        }

        public IDbConnection Connection =>
            new SqlConnection(
                _connectionStringService.GetVendorConnectionString(
                    Repository.CoreCmsDb));

        public async Task<ContentMedia> Add(ContentMedia content)
        {
            using (var dbConnection = Connection)
            {               
                var query = @"insert into [dbo].[ContentMedia] (	
	                            Url,
	                            FileCode,
	                            Title,
	                            Description,
	                            ContentCode,
	                            [Order],
                                MainImage,
                                ImageTitle,
                                ImageCaption)
                            values (
	                            @Url,
	                            @FileCode,
	                            @Title,
	                            @Description,
	                            @ContentCode,
	                            @Order,
                                @MainImage,
                                @ImageTitle,
                                @ImageCaption)
                             select * from [dbo].[ContentMedia] where Id = cast(scope_identity() as int)"; 
                dbConnection.Open();
                return 
                    (await dbConnection.QueryAsync<ContentMedia>(query, content)).FirstOrDefault();
            }
        }

        public async Task<int> Update(ContentMedia content)
        {
            using (var dbConnection = Connection)
            {
                var query = @"update [dbo].[ContentMedia] set
	                            Url = @Url,
	                            FileCode = @FileCode,
	                            Title = @Title,
	                            Description = @Description,
	                            ContentCode = @ContentCode,
	                            [Order] = @Order,
                                MainImage = @MainImage,
                                ImageTitle = @ImageTitle,
                                ImageCaption = @ImageCaption

                                where Id = @Id";
                dbConnection.Open();
                return (await dbConnection.ExecuteAsync(query, content));
            }
        }

        public async Task<ContentMedia> GetById(int id)
        {
            using (var dbConnection = Connection)
            {
                var sQuery = @"select * from [dbo].[ContentMedia] where Id = @Id";
                dbConnection.Open();
                return (await dbConnection.QueryAsync<ContentMedia>(sQuery, new {Id = id })).FirstOrDefault();
            }
        }

        public async Task<IEnumerable<ContentMedia>> GetByContentCode(string contentCode)
        {
            const string query = @"SELECT [CMSCore].[dbo].[ContentMedia].[Id]
                ,[CMSCore].[dbo].[ContentMedia].[Url]
                ,[CMSCore].[dbo].[ContentMedia].[FileCode]
                ,[CMSCore].[dbo].[ContentMedia].[Title]
                ,[CMSCore].[dbo].[ContentMedia].[Description]
                ,[CMSCore].[dbo].[ContentMedia].[ContentCode]
                ,[CMSCore].[dbo].[ContentMedia].[Order]
                ,[CMSCore].[dbo].[ContentMedia].[MainImage]
                ,[CMSCore].[dbo].[ContentMedia].[ImageTitle]
                ,[CMSCore].[dbo].[ContentMedia].[ImageCaption]
	            ,[CMSCore].[dbo].[Files].Credit
            FROM ([CMSCore].[dbo].[ContentMedia]
            LEFT JOIN Files ON [CMSCore].[dbo].[ContentMedia].FileCode = [CMSCore].[dbo].[Files].Code)
            WHERE ContentCode = @Code";

            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                return 
                    await dbConnection.QueryAsync<ContentMedia>(query, new { Code = contentCode });
            }
        }

        public async Task<int> Delete(int contentMediaId)
        {
            const string query = @"delete from [dbo].[ContentMedia] where Id = @Id";

            using (var dbConnection = Connection)
            {
                dbConnection.Open();

                return 
                    await dbConnection.ExecuteAsync(query, new { Id = contentMediaId });
            }
        }
       
        public async Task<int> ReOrder(string contentCode , IEnumerable<ContentMediaOrder> contentMediaOrder)
        {
            using (var dbConnection = Connection)
            {
                try
                {
                    var pars = new DynamicParameters();
                    var filters = SqlHelpers.GetXml(contentMediaOrder);

                    if (string.IsNullOrEmpty(filters)) {
                        filters = null;
                    }

                    pars.Add("@FilterList", filters);
                    pars.Add("@ContentCode", contentCode);

                    dbConnection.Open();

                    return
                        await dbConnection.ExecuteAsync(
                            "ContentMediaReOrder", pars, commandType: CommandType.StoredProcedure);
                }
                finally
                {
                    if (dbConnection.State == ConnectionState.Open)
                    {
                        dbConnection.Close();
                    }
                }
            }
        }     
       
        public async Task<int> DeleteByContentCode(string contentCode)
        {
            const string query = @"delete from [dbo].[ContentMedia] where ContentCode = @ContentCode";

            using (var dbConnection = Connection)
            {
                dbConnection.Open();

                return
                    await dbConnection.ExecuteAsync(query, new { ContentCode = contentCode });
            }
        }
    }
}
