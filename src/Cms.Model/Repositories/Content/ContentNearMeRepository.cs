﻿using Cms.Model.Enum;
using Cms.Model.Models.Content.Db;
using Cms.Model.Services;
using Dapper;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Bbb.Core.Solr.Model;

namespace Cms.Model.Repositories.Content
{
    public interface IContentNearMeRepository
    {
        Task<DbContentNearMe> Add(DbContentNearMe content);
        Task<DbContentNearMe> GetById(int id);
        Task<int> Delete(int id);
        Task<DbContentNearMe> GetForContentByCode(string contentCode);
        Task<DbContentNearMe> Update(DbContentNearMe content);
        Task<int> DeleteByContentCode(string contentCode);
        Task<List<string>> GetSelectedTobIds();
    }

    public class ContentNearMeRepository : IContentNearMeRepository
    {
        private readonly IConnectionStringService _connectionStringService;

        public ContentNearMeRepository(IConnectionStringService connectionStringService)
        {
            _connectionStringService = connectionStringService;
        }

        public IDbConnection Connection =>
            new SqlConnection(
                _connectionStringService.GetVendorConnectionString(
                    Repository.CoreCmsDb));

        public async Task<DbContentNearMe> Add(DbContentNearMe content)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"insert into [dbo].[ContentNearMe] (	                         
                                ContentCode, 
                                AboveListContent,
                                RelatedInformationHeadline,
                                RelatedInformationText)
                            values (
	                            @ContentCode,
	                            @AboveListContent,
                                @RelatedInformationHeadline,
                                @RelatedInformationText)

                             select * from [dbo].[ContentNearMe] where Id = cast(scope_identity() as int)";
                dbConnection.Open();
                return
                    (await dbConnection.QueryAsync<DbContentNearMe>(query, content)).FirstOrDefault();
            }
        }

        public async Task<DbContentNearMe> Update(DbContentNearMe content)
        {
            const string query = @"update [dbo].[ContentNearMe] set
                                AboveListContent           = @AboveListContent, 
                                RelatedInformationHeadline = @RelatedInformationHeadline, 
                                RelatedInformationText     = @RelatedInformationText
                  
                                where Id = @Id

                                select * from [dbo].[ContentNearMe] where Id = @Id";

            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                return
                    (await dbConnection.QueryAsync<DbContentNearMe>(query, content)).FirstOrDefault();
            }
        }

        public async Task<DbContentNearMe> GetById(int id)
        {
            using (var dbConnection = Connection)
            {
                var query = @"select * from [dbo].[ContentNearMe] where Id = @Id";
                dbConnection.Open();
                return (await dbConnection.QueryAsync<DbContentNearMe>(
                    query, new { Id = id })).FirstOrDefault();
            }
        }

        public async Task<DbContentNearMe> GetForContentByCode(string contentCode)
        {
            const string query = @"select * from [dbo].[ContentNearMe] where ContentCode = @Code";

            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                return
                    (await dbConnection.QueryAsync<DbContentNearMe>(
                        query, new { Code = contentCode })).FirstOrDefault();
            }
        }

        public async Task<int> Delete(int contentNearMeId)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"delete from [dbo].[ContentNearMe] where Id = @Id";
                dbConnection.Open();

               return 
                    await dbConnection.ExecuteAsync(
                        query, new { Id = contentNearMeId });
            }
        }

        public async Task<int> DeleteByContentCode(string contentCode)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"delete from [dbo].[ContentNearMe] where ContentCode = @ContentCode";
                dbConnection.Open();

                return
                     await dbConnection.ExecuteAsync(
                         query, new { ContentCode = contentCode });
            }
        }

        public async Task<List<string>> GetSelectedTobIds()
        {
            using (var dbConnection = Connection)
            {
                var query = @"select [dbo].[Content].[Categories]
                                from [dbo].[Content]
                                right join [dbo].[ContentNearMe]
                                on [dbo].[Content].[Code] = [dbo].[ContentNearMe].[ContentCode]
                                where [dbo].[Content].[Categories] is not null";

                dbConnection.Open();
                var result = await dbConnection.QueryAsync<string>(query);
                return result
                    .Select(json => JsonConvert.DeserializeObject<List<SearchTypeahead>>(json))?
                    .SelectMany(x => x)
                    .Select(cat => cat.Id)
                    .Where(id => !string.IsNullOrWhiteSpace(id))?
                    .ToList();

            }
        }
    }
}
