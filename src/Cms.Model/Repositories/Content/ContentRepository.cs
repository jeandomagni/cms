﻿using Cms.Model.Enum;
using Cms.Model.Helpers;
using Cms.Model.Models;
using Cms.Model.Models.Content.Db;
using Cms.Model.Models.Query.KendoGrid;
using Cms.Model.Services;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Cms.Model.Repositories.Content
{
    public interface IContentRepository
    {
        Task<DbContent> Add(DbContent content);
        Task<DbContent> GetById(int id, string user);
        Task<string> GetCodeById(int id);
        Task<int> DeleteSoft(int id);
        Task<int> Delete(int id);
        Task<DbContent> Update(DbContent content);
        Task<DbContent> GetByCode(string code, string user);
        Task<IEnumerable<DbContent>> Paginate(GridOptions options, string userId);
        Task<string> GetUniqueUri(string uri);
        Task<int> GetIdByCode(string code);
    }

    public class ContentRepository : IContentRepository
    {
        private readonly string querySelectContent = @"select
                     c.*
                    ,[dbo].[fn_XmlToJson_Get]((
							SELECT
								p.Id as id,
                                p.PinnedLocationId as pinnedLocation,
                                p.PinnedLocationType as locationType,
                                p.PinExpirationDate as pinExpirationDate
							FROM [CMSCore].[dbo].[PinnedContentLocation] p
							WHERE p.ContentCode = c.Code
							FOR XML PATH, ROOT)) AS PinnedLocations
                    from [dbo].[Content] c where Code = @Code";

        private readonly IConnectionStringService _connectionStringService;

        public ContentRepository(
            IConnectionStringService connectionStringService) {
            _connectionStringService = connectionStringService;
        }

        public IDbConnection Connection =>
            new SqlConnection(
                _connectionStringService.GetVendorConnectionString(
                    Repository.CoreCmsDb));

        public async Task<DbContent> Add(DbContent content)
        {
            using (var dbConnection = Connection)
            {
                
                const string query = @"insert into [dbo].[Content] (	
	                            Title,
	                            ContentHtml,
	                            Summary,
	                            Code,
	                            PublishDate,
	                            ExpirationDate,
	                            Tags,
	                            Categories,
	                            Type,
                                Layout,
	                            Status,
	                            CreateDate,
	                            CreatedBy,
	                            ModifiedBy,
	                            ModifiedDate,
	                            Deleted,
                                UriSegment,
                                MobileHeadline,
                                PageTitle,
                                Topics,
                                MetaDescription,
                                Priority,
                                PinnedTopic,
                                Canonical,
                                Sites,
                                OwnerSiteId,
                                NoIndex,
                                TopicsAndTags,
                                GeoTags,
                                Videos,
                                BbbAuthor,
                                Authors,
                                CheckoutBy,
                                CheckoutDate,
                                BbbIds,
                                PinnedBbbIds,
                                RedirectUrl,
                                SeoCanonical,
                                Countries
                                )
                            values (
	                            @Title,
	                            @ContentHtml,
	                            @Summary,
	                            @Code,
	                            @PublishDate,
	                            @ExpirationDate,
	                            @Tags,
	                            @Categories,
	                            @Type,
                                @Layout,
	                            @Status,
	                            @CreateDate,
	                            @CreatedBy,
	                            @ModifiedBy,
	                            @ModifiedDate,
	                            0,
                                @UriSegment,
                                @MobileHeadline,
                                @PageTitle,
                                @Topics,
                                @MetaDescription,
                                @Priority,
                                @PinnedTopic,
                                @Canonical,
                                @Sites,
                                @OwnerSiteId,
                                @NoIndex,
                                @TopicsAndTags,
                                @GeoTags,
                                @Videos,
                                @BbbAuthor,
                                @Authors,
                                @CheckoutBy,
                                @CheckoutDate,
                                @BbbIds,
                                @PinnedBbbIds,
                                @RedirectUrl,
                                @SeoCanonical,
                                @Countries)

                             select * from [dbo].[Content] where Id = cast(scope_identity() as int)"; 
                dbConnection.Open();
                return (await dbConnection.QueryAsync<DbContent>(query, content)).FirstOrDefault();
            }
        }

        public async Task<IEnumerable<DbContent>> Paginate(GridOptions options, string userId)
        {
            if (options?.data == null)
                return null;

            using (var dbConnection = Connection)
            {
                try
                {              
                    var pars = new DynamicParameters();
                    var sortField = "id";
                    var sortDirection = "desc";

                    if (options.data.sort != null && options.data.sort.Any()) {
                        sortField = options.data.sort[0].field;
                        sortDirection = options.data.sort[0].dir;
                    }

                    var filters = string.Empty;
                    if (options.data.filter?.filters != null) {
                        filters = SqlHelpers.GetXml(options.data.filter.filters);
                    }

                    if (string.IsNullOrEmpty(filters)) {
                        filters = null;
                    }

                    pars.Add("@FilterList", filters);
                    pars.Add("@UserId", userId);
                    pars.Add("@PageNumber", options.data.page);
                    pars.Add("@PageSize", options.data.pageSize);
                    pars.Add("@SortField", sortField);
                    pars.Add("@SortDir", sortDirection);
                    pars.Add("@LocationIds", options.data.locations);
                    pars.Add("@IncludeExpired", options.data.includeExpired);
                    pars.Add("@CheckedOutBy", options.data.CheckedOutBy);

                    dbConnection.Open();
                    
                    return 
                        await dbConnection.QueryAsync<DbContent>(
                            "PaginateContent", pars, commandType: CommandType.StoredProcedure);                
                }
                finally {
                    if (dbConnection.State == ConnectionState.Open) { 
                        dbConnection.Close();
                    }
                }
            }      
        }

        public async Task<DbContent> GetById(int id, string user)
        {
            using (var dbConnection = Connection)
            {
                dbConnection.Open();
              
                return 
                    (await dbConnection.QueryAsync<DbContent>(
                        GetSelectQuery("Id"), new { Id = id, userId = user })).FirstOrDefault();         
            }
        }

        public async Task<string> GetCodeById(int id)
        {
            using (var dbConnection = Connection)
            {
                dbConnection.Open();

                const string query = "select Code from [dbo].[Content] where Id = @Id";

                return
                    (await dbConnection.QueryAsync<string>(
                        query, new { Id = id })).FirstOrDefault();
            }
        }

        public async Task<DbContent> GetByCode(string code, string user)
        {
            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                return (await dbConnection.QueryAsync<DbContent>(
                    querySelectContent, new { Code = code })).FirstOrDefault();
            }
        }

        public async Task<int> DeleteSoft(int id)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"update [dbo].[Content] set Deleted = 1 where Id = @Id";
                dbConnection.Open();
                return
                    await dbConnection.ExecuteAsync(query, new {Id = id});
            }
        }

        public async Task<DbContent> Update(DbContent content)
        {
            using (var dbConnection = Connection)
            {
                string query = $@"update [dbo].[Content] set 
                              Title           = @Title,
                              ContentHtml     = @ContentHtml,
                              PublishDate     = @PublishDate,
                              Categories      = @Categories,
                              Summary         = @Summary,
                              ExpirationDate  = @ExpirationDate,
                              Status          = @Status,
                              ModifiedDate    = @ModifiedDate,
                              Layout          = @Layout,
                              Image           = @Image,
                              ModifiedBy      = @ModifiedBy,
                              MobileHeadline  = @MobileHeadline,
                              PageTitle       = @PageTitle,                                           
                              Cities          = @Cities,
                              PinnedCities    = @PinnedCities,
                              States          = @States,
                              PinnedStates    = @PinnedStates,
                              Countries       = @Countries,
                              PinnedCountries = @PinnedCountries,
                              Topics          = @Topics,
                              Type            = @Type,
                              EventBbb        = @EventBbb,
                              MetaDescription = @MetaDescription,
                              UriSegment      = @UriSegment,
                              Priority        = @Priority,
                              PinnedTopic     = @PinnedTopic,
                              Canonical       = @Canonical,
                              Sites           = @Sites,
                              Authors         = @Authors,
                              OwnerSiteId     = @OwnerSiteId,
                              NoIndex         = @NoIndex,
                              Tags            = @Tags,
                              GeoTags         = @GeoTags,
                              TopicsAndTags   = @TopicsAndTags,
                              Videos          = @Videos,
                              BbbAuthor       = @BbbAuthor,
                              CheckoutDate    = @CheckoutDate,
                              CheckoutBy      = @CheckoutBy,
                              BbbIds          = @BbbIds,
                              PinnedBbbIds    = @PinnedBbbIds,
                              RedirectUrl     = @RedirectUrl,
                              SeoCanonical    = @SeoCanonical
                              where Id = @Id

                              {querySelectContent}";

               dbConnection.Open();
               return (await dbConnection.QueryAsync<DbContent>(query, content)).FirstOrDefault();
            }
        }

        public async Task<string> GetUniqueUri(string title) { 
            using (var dbConnection = Connection)
            {
                var query = @"select [dbo].slugify(@Title)";

                dbConnection.Open();
                return
                    (await dbConnection.QueryAsync<string>(query, new { Title = title })).FirstOrDefault();                 
            }
        }

        private static string GetSelectQuery(string field) {
            return $@"select * from [dbo].[Content] where @{field} = {field}";
        }

        public async Task<int> Delete(int id)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"DELETE FROM [dbo].[Content] WHERE Id = @Id";
                dbConnection.Open();
                return
                     await dbConnection.ExecuteAsync(query, new { Id = id });
            }
        }

        public async Task<int> GetIdByCode(string code)
        {
            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                return (await dbConnection.QueryAsync<int>("select Id from [dbo].[Content] where Code = @Code", new { Code = code })).FirstOrDefault();
            }
        }
    }
}
