﻿using Cms.Model.Enum;
using Cms.Model.Models;
using Cms.Model.Services;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Cms.Model.Repositories.Content
{
    public interface ISiteContentRepository
    {
        Task<SiteContent> Add(SiteContent siteContent);
        Task<int> Delete(int Id);
        Task<int> DeleteForContent(string contentCode);
    }

    public class SiteContentRepository : ISiteContentRepository
    {
        private readonly IConnectionStringService _connectionStringService;

        public SiteContentRepository(
            IConnectionStringService connectionStringService) {
            _connectionStringService = connectionStringService;
        }

        public IDbConnection Connection => new SqlConnection(
            _connectionStringService.GetVendorConnectionString(Repository.CoreCmsDb));

        public async Task<IEnumerable<SiteContent>> GetAllForUser(string userId)
        {
            using (var dbConnection = Connection)
            {
                var query = @"select * from [dbo].[SiteContent] where UserId = @UserId";

                dbConnection.Open();
                return 
                    await dbConnection.QueryAsync<SiteContent>(query, new { UserId = userId });
            }
        }

        public async Task<SiteContent> Add(SiteContent siteContent) {
            using (var dbConnection = Connection)
            {
                var query = @"insert into [dbo].[SiteContent] (SiteId, ContentId) values (@SiteId, @ContentCode) 
                              select * from [dbo].[SiteContent] where Id = cast(scope_identity() as int)";

                dbConnection.Open();
                return
                    (await dbConnection.QueryAsync<SiteContent>(query, siteContent)).FirstOrDefault();
            }
        }

        public async Task<int> DeleteForContent(string contentCode) {
            using (var dbConnection = Connection)
            {
                const string query = @"delete from [dbo].[SiteContent] where ContentId = @ContentId";
                dbConnection.Open();

                return
                    await
                     dbConnection.ExecuteAsync(query, new { ContentId = contentCode });
            }
        }

        public async Task<int> Delete(int id)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"delete from dbo.[SiteContent] where Id = @Id";
                dbConnection.Open();

                return
                    await
                     dbConnection.ExecuteAsync(query, new { Id = id });
            }
        }
    }
}
