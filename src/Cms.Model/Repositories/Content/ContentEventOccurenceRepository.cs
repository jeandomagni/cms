﻿using Cms.Model.Enum;
using Cms.Model.Models.Query.KendoGrid;
using Cms.Model.Models.Recurrence;
using Cms.Model.Services;
using Dapper;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Cms.Model.Repositories.Content
{
    public interface IContentEventOccurenceRepository
    {
        Task<int> SaveOccurrences(IEnumerable<ContentEventOccurrence> occurrences);
        Task<int> DeleteForEventId(int eventId);
        Task<IEnumerable<ContentEventOccurrence>> SelectForEventId(int eventId);
        Task<IEnumerable<ContentEventOccurrence>> SelectForContentCode(string code);
        Task<int> DeleteOccurrence(int occurrenceId);
        Task<ContentEventOccurrence> GetByOccurrenceId(int occurrenceId);
        Task<IEnumerable<ContentEventOccurrence>> Paginate(GridOptions options);
    }

    public class ContentEventOccurenceRepository : IContentEventOccurenceRepository
    {
        private readonly IConnectionStringService _connectionStringService;

        public ContentEventOccurenceRepository(
            IConnectionStringService connectionStringService)
        {
            _connectionStringService = connectionStringService;
        }

        public IDbConnection Connection =>
            new SqlConnection(
                _connectionStringService.GetVendorConnectionString(
                    Repository.CoreCmsDb));

        public async Task<int> DeleteForEventId(int eventId)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"DELETE FROM [dbo].[ContentEventOccurrence] WHERE EventId = @EventId";
                dbConnection.Open();
                return await dbConnection.ExecuteAsync(query, new { EventId = eventId });
            }
        }

        public async Task<int> DeleteOccurrence(int occurrenceId)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"DELETE FROM [dbo].[ContentEventOccurrence] WHERE Id = @Id";
                dbConnection.Open();
                return await dbConnection.ExecuteAsync(query, new { Id = occurrenceId });
            }
        }

        public async Task<int> SaveOccurrences(IEnumerable<ContentEventOccurrence> occurrences)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"INSERT INTO [dbo].[ContentEventOccurrence]
                                           ([EventId] ,[StartDate] ,[EndDate])
                                     VALUES (@EventId,@StartDate,@EndDate)";
                dbConnection.Open();
                return await dbConnection.ExecuteAsync(query, occurrences);
            }
        }

        public async Task<IEnumerable<ContentEventOccurrence>> SelectForEventId(int eventId)
        {
            using (var dbConnection = Connection)
            {
                const string query = "SELECT * FROM [dbo].[ContentEventOccurrence] WHERE EventId = @EventId";
                return await dbConnection.QueryAsync<ContentEventOccurrence>(query, new { EventId = eventId });
            }
        }

        public async Task<IEnumerable<ContentEventOccurrence>> Paginate(GridOptions options)
        {
            if (options?.data == null)
                return null;

            using (var dbConnection = Connection)
            {
                try
                {
                    var pars = new DynamicParameters();
                    var sortField = "id";
                    var sortDirection = "desc";

                    if (options.data.sort != null && options.data.sort.Any())
                    {
                        sortField = options.data.sort[0].field;
                        sortDirection = options.data.sort[0].dir;
                    }

                    var filters = string.Empty;
                    if (options.data.filter?.filters != null)
                    {
                        filters = GetXml(options.data.filter.filters);
                    }

                    if (string.IsNullOrEmpty(filters))
                    {
                        filters = null;
                    }

                    pars.Add("@FilterList", filters);
                    pars.Add("@PageNumber", options.data.page);
                    pars.Add("@PageSize", options.data.pageSize);
                    pars.Add("@SortField", sortField);
                    pars.Add("@SortDir", sortDirection);

                    dbConnection.Open();

                    return
                        await dbConnection.QueryAsync<ContentEventOccurrence>(
                            "PaginateEventOccurrence", pars, commandType: CommandType.StoredProcedure);
                }
                finally
                {
                    if (dbConnection.State == ConnectionState.Open)
                    {
                        dbConnection.Close();
                    }
                }
            }
        }

        private static string GetXml(IEnumerable<Logic> fields)
        {
            var doc =
                JsonConvert.DeserializeXmlNode(
                    "{\"Row\":" + JsonConvert.SerializeObject(fields) + "}", "root");
            return doc.OuterXml;
        }

        public async Task<IEnumerable<ContentEventOccurrence>> SelectForContentCode(string code)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"SELECT o.Id, o.EventId, o.StartDate, o.EndDate FROM [dbo].[ContentEventOccurrence] o
                                        JOIN[dbo].[ContentEvent] e ON e.Id = o.EventId
                                        WHERE e.ContentCode = @ContentCode";
                return await dbConnection.QueryAsync<ContentEventOccurrence>(query, new { ContentCode = code });
            }
        }

        public async Task<ContentEventOccurrence> GetByOccurrenceId(int occurrenceId)
        {
            using (var dbConnection = Connection)
            {
                const string query = "SELECT * FROM [dbo].[ContentEventOccurrence] WHERE Id = @Id";
                return await dbConnection.QueryFirstOrDefaultAsync<ContentEventOccurrence>(query, new { Id = occurrenceId });
            }
        }
    }
}
