﻿using Cms.Model.Enum;
using Cms.Model.Models.Content.Db;
using Cms.Model.Services;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Cms.Model.Repositories.Content
{
    public interface IContentEventRepository
    {
        Task<DbContentEvent> Add(DbContentEvent content);
        Task<DbContentEvent> GetById(int id);
        Task<int> Delete(int id);
        Task<DbContentEvent> GetForContentByCode(string contentCode);
        Task<DbContentEvent> Update(DbContentEvent content);
        Task<int> DeleteByContentCode(string contentCode);
    }

    public class ContentEventRepository : IContentEventRepository
    {
        private readonly IConnectionStringService _connectionStringService;

        public ContentEventRepository(IConnectionStringService connectionStringService)
        {
            _connectionStringService = connectionStringService;
        }

        public IDbConnection Connection =>
            new SqlConnection(
                _connectionStringService.GetVendorConnectionString(
                    Repository.CoreCmsDb));

        public async Task<DbContentEvent> Add(DbContentEvent content)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"insert into [dbo].[ContentEvent] (	                         
                                ContentCode, 
                                StartDate,
                                EndDate,
                                RegistrationLink,
                                LocationName,
                                AddressLine,
                                AddressLine2,
                                Country,
                                City,
                                State,
                                PostalCode,
                                Recurrence,
                                IsVirtualEvent)
                            values (
	                            @ContentCode,
	                            @StartDate,
	                            @EndDate,
	                            @RegistrationLink,
                                @LocationName,
	                            @AddressLine,
	                            @AddressLine2,
	                            @Country,
	                            @City,
	                            @State,
	                            @PostalCode,
                                @Recurrence,
                                @IsVirtualEvent)

                             select * from [dbo].[ContentEvent] where Id = cast(scope_identity() as int)";
                dbConnection.Open();
                return await dbConnection.QueryFirstOrDefaultAsync<DbContentEvent>(query, content);
            }
        }

        public async Task<DbContentEvent> Update(DbContentEvent content)
        {
            const string query = @"update [dbo].[ContentEvent] set
                                StartDate        = @StartDate, 
                                EndDate          = @EndDate, 
                                RegistrationLink = @RegistrationLink,
                                LocationName     = @LocationName,
                                AddressLine      = @AddressLine,
                                AddressLine2     = @AddressLine2,
                                Country          = @Country, 
                                City             = @City, 
                                State            = @State,
                                PostalCode       = @PostalCode,
                                Recurrence       = @Recurrence,
                                IsVirtualEvent   = @IsVirtualEvent
                                where Id = @Id

                                select * from [dbo].[ContentEvent] where Id = @Id";

            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                return await dbConnection.QueryFirstOrDefaultAsync<DbContentEvent>(query, content);
            }
        }

        public async Task<DbContentEvent> GetById(int id)
        {
            using (var dbConnection = Connection)
            {
                var query = @"select * from [dbo].[ContentEvent] where Id = @Id";
                dbConnection.Open();
                return (await dbConnection.QueryAsync<DbContentEvent>(
                    query, new { Id = id })).FirstOrDefault();
            }
        }

        public async Task<DbContentEvent> GetForContentByCode(string contentCode)
        {
            const string query = @"select * from [dbo].[ContentEvent] where ContentCode = @Code";

            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                return await dbConnection.QueryFirstOrDefaultAsync<DbContentEvent>(
                    query, new { Code = contentCode });
            }
        }

        public async Task<int> Delete(int contentEventId)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"delete from [dbo].[ContentEvent] where Id = @Id";
                dbConnection.Open();

               return 
                    await dbConnection.ExecuteAsync(
                        query, new { Id = contentEventId });
            }
        }

        public async Task<int> DeleteByContentCode(string contentCode)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"delete from [dbo].[ContentEvent] where ContentCode = @ContentCode";
                dbConnection.Open();

                return
                     await dbConnection.ExecuteAsync(
                         query, new { ContentCode = contentCode });
            }
        }
    }
}
