﻿using Cms.Model.Enum;
using Cms.Model.Models;
using Cms.Model.Services;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Cms.Model.Repositories.Content
{
    public interface IEntityRepository
    {
        Task<int> UpdateContentEntities(int contentId, string entityType, GenericEntity entities);
        Task<IEnumerable<Entity>> Query(string query, string entityType, int resultLimit);
        Task<bool> Add(string entityType, string data);
        Task<Entity> GetByCode(string code);
        Task<int> Delete(string code);
    }

    public class EntityRepository : IEntityRepository
    {
        private readonly IConnectionStringService _connectionStringService;

        public EntityRepository(IConnectionStringService connectionStringService)
        {
            _connectionStringService = connectionStringService;
        }

        public IDbConnection Connection =>
            new SqlConnection(_connectionStringService.GetVendorConnectionString(Repository.CoreCmsDb));


        public async Task<Entity> GetByCode(string code)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"select * from [dbo].[Entities] 
                               where code = @Code";
                dbConnection.Open();
                return
                    (await dbConnection.QueryAsync<Entity>(
                        query, new { Code = code })).FirstOrDefault();
            }
        }

        public async Task<int> UpdateContentEntities(int contentId, string entityType, GenericEntity entities)
        {
            var targetColumn = GetTargetColumnForQuery(entityType);

            using (var dbConnection = Connection)
            {
                var sQuery = $@"update [dbo].[Content] set {targetColumn} = @data where Id = @Id";
                dbConnection.Open();
                return
                    (await dbConnection.ExecuteAsync(
                        sQuery, new { col = targetColumn, data = entities.Data, Id = contentId }));
            }
        }

        public async Task<IEnumerable<Entity>> Query(string phrase, string type, int resultLimit)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"select top (@limit) data from [dbo].[Entities] 
                               where data like @query and type = @entityType order by 1";
                dbConnection.Open();
                return
                    await dbConnection.QueryAsync<Entity>(
                        query, new { limit = resultLimit, query = $"%{phrase}%", entityType = type });
            }
        }

        public async Task<bool> Add(string entityType, string data)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"insert into [dbo].[Entities] (code, type, data) 
                            values (newid(), @type, @entity)";

                return
                      (await dbConnection.ExecuteAsync(
                          query, new { type = entityType, entity = data })) > 0;
            }
        }

        public async Task<int> Delete(string code)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"delete from [dbo].[Entities] where Code = @entityCode";
                return
                    await dbConnection.ExecuteAsync(
                        query, new { entityCode = code });
            }
        }

        private static string GetTargetColumnForQuery(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType)) {
                return null;
            }

            switch (entityType.ToLower()) {
                case "author":
                    return "Authors";
                case "category":
                    return "Categories";
                case "topic":
                    return "Topics";
                case "pinnedtopic":
                    return "pinnedtopic";
                case "location":
                    return "locations";
                case "cities":
                    return "cities";
                case "pinnedcities":
                    return "pinnedcities";
                case "states":
                    return "states";
                case "pinnedstates":
                    return "pinnedstates";
                case "countries":
                    return "countries";
                case "pinnedcountries":
                    return "pinnedcountries";
                case "eventbbb":
                    return "eventbbb";
                case "sites":
                    return "sites";
            }
            throw 
                new TypeAccessException($"Requested type {entityType} has not been configured");
        }
    }

    public class GenericEntity
    {
        public string Data;
    }
}
