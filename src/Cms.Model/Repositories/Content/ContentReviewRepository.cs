﻿using Cms.Model.Enum;
using Cms.Model.Models.Content;
using Cms.Model.Services;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Cms.Model.Repositories.Content
{
    public interface IContentReviewRepository
    {
        Task<ContentReview> Add(ContentReview contentReview);
        Task<ContentReview> Update(ContentReview contentReview);
        Task<ContentReview> GetById(int id);
        Task<ContentReview> GetByContentCode(string code);
        Task<int> DeleteByContentCode(string contentCode);
    }

    public class ContentReviewRepository : IContentReviewRepository
    {
        private readonly IConnectionStringService _connectionStringService;

        public ContentReviewRepository(IConnectionStringService connectionStringService)
        {
            _connectionStringService = connectionStringService;
        }

        public IDbConnection Connection =>
            new SqlConnection(_connectionStringService.GetVendorConnectionString(Repository.CoreCmsDb));

        public async Task<ContentReview> GetByContentCode(string contentCode)
        {
            using (var dbConnection = Connection)
            {
                const string query =
                    @"select top 1 * from [dbo].[ContentReview] where ContentCode = @ContentCode and CompletedDate is null"; 
                dbConnection.Open();
                return
                    (await dbConnection.QueryAsync<ContentReview>(query, new { ContentCode  = contentCode })).FirstOrDefault();
            }
        }

        public async Task<ContentReview> GetById(int id)
        {
            using (var dbConnection = Connection)
            {
                const string query = 
                    @"select * from [dbo].[ContentReview] where Id = @Id";
                dbConnection.Open();
                return
                    (await dbConnection.QueryAsync<ContentReview>(query, new { Id = id })).FirstOrDefault();
            }
        }

        public async Task<ContentReview> Add(ContentReview contentReview)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"insert into [dbo].[ContentReview] (	
	                                        ContentCode,
	                                        CreatedDate,
                                            CreatedBy)
                                        values (
                                            @ContentCode,
	                                        sysdatetimeoffset(),
                                            @CreatedBy)

                                    select * from [dbo].[ContentReview] where Id = cast(scope_identity() as int)";
                dbConnection.Open();
                return
                    (await dbConnection.QueryAsync<ContentReview>(query, contentReview)).FirstOrDefault();
            }
        }

        public async Task<ContentReview> Update(ContentReview contentReview)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"update [dbo].[ContentReview] set	

	                                    CompletedDate = sysdatetimeoffset(),
                                        CompletedBy   = @CompletedBy,
	                                    Comment       = @Comment,
                                        Status        = @Status,
                                        Serialized    = @Serialized

                                        where Id = @Id                       
                                        select * from [dbo].[ContentReview] where Id = @Id";
                dbConnection.Open();
                return
                    (await dbConnection.QueryAsync<ContentReview>(query, contentReview)).FirstOrDefault();
            }
        }

        public async Task<int> DeleteByContentCode(string contentCode)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"DELETE FROM [dbo].[ContentReview] WHERE ContentCode = @ContentCode";
                dbConnection.Open();

                return
                     await dbConnection.ExecuteAsync(
                         query, new { ContentCode = contentCode });
            }
        }
    }
}
