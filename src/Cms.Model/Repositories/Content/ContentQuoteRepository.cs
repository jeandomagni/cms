﻿using Cms.Model.Enum;
using Cms.Model.Helpers;
using Cms.Model.Models.Content;
using Cms.Model.Models.Query.KendoGrid;
using Cms.Model.Services;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Cms.Model.Repositories.Content
{
    public interface IContentQuoteRepository
    {
        Task<ContentQuote> Add(ContentQuote quote);
        Task<IEnumerable<ContentQuote>> Paginate(GridOptions options, string userId);
        Task<ContentQuote> GetByCode(string code);
        Task<ContentQuote> Update(ContentQuote quote);
        Task<int> DeleteByCode(string code);
    }

    public class ContentQuoteRepository : IContentQuoteRepository
    {
        private readonly IConnectionStringService _connectionStringService;

        public ContentQuoteRepository(IConnectionStringService connectionStringService)
        {
            _connectionStringService = connectionStringService;
        }

        public IDbConnection Connection =>
            new SqlConnection(
                _connectionStringService.GetVendorConnectionString(
                    Repository.CoreCmsDb));

        public async Task<ContentQuote> Add(ContentQuote content)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"insert into [dbo].[Quotes] (	
                                Code,
                                FirstName,
                                LastName,
                                Text,
                                Tags,
                                CreateDate,
                                CreatedBy,
                                Deleted,
                                OwnerSiteId,
                                Published)
                            values (
	                            @Code,
                                @FirstName,
                                @LastName,
                                @Text,
                                @Tags,
                                @CreateDate,
                                @CreatedBy,
                                @Deleted,
                                @OwnerSiteId,
                                @Published)

                             select * from [dbo].[Quotes] where Id = cast(scope_identity() as int)";
                dbConnection.Open();
                return
                    (await dbConnection.QueryAsync<ContentQuote>(query, content)).FirstOrDefault();
            }
        }

        public async Task<IEnumerable<ContentQuote>> Paginate(GridOptions options, string userId)
        {
            if (options.data == null)
                return null;

            using (var dbConnection = Connection)
            {
                try
                {
                    var pars = new DynamicParameters();
                    var sortField = "id";
                    var sortDirection = "desc";

                    if (options.data.sort != null && options.data.sort.Any())
                    {
                        sortField = options.data.sort[0].field;
                        sortDirection = options.data.sort[0].dir;
                    }

                    var filters = string.Empty;
                    if (options.data.filter?.filters != null)
                    {
                        filters = SqlHelpers.GetXml(options.data.filter.filters);
                    }

                    if (string.IsNullOrEmpty(filters))
                    {
                        filters = null;
                    }

                    pars.Add("@FilterList", filters);
                    pars.Add("@UserId", userId);
                    pars.Add("@PageNumber", options.data.page);
                    pars.Add("@PageSize", options.data.pageSize);
                    pars.Add("@SortField", sortField);
                    pars.Add("@SortDir", sortDirection);
                    pars.Add("@LocationIds", options.data.locations);

                    dbConnection.Open();

                    return
                        await dbConnection.QueryAsync<ContentQuote>(
                            "PaginateQuotes", pars, commandType: CommandType.StoredProcedure);
                }
                finally
                {
                    if (dbConnection.State == ConnectionState.Open)
                    {
                        dbConnection.Close();
                    }
                }
            }
        }

        public async Task<ContentQuote> GetByCode(string code)
        {
            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                return (await dbConnection.QueryAsync<ContentQuote>(
                    GetSelectQuery("Code"), new { Code = code })).FirstOrDefault();
            }
        }

        public async Task<ContentQuote> Update(ContentQuote quote)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"update [dbo].[Quotes] set 
                                FirstName   =   @FirstName,
                                LastName    =   @LastName,
                                Text        =   @Text,
                                Tags        =   @Tags,
                                CreateDate  =   @CreateDate,
                                CreatedBy   =   @CreatedBy,
                                Deleted     =   @Deleted,
                                OwnerSiteId =   @OwnerSiteId,
                                Published   =   @Published
                                where Id = @Id
                                select * from [dbo].[Quotes] where Id = @Id";

                dbConnection.Open();
                return (await dbConnection.QueryAsync<ContentQuote>(query, quote)).FirstOrDefault();
            }
        }

        public async Task<int> DeleteByCode(string code)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"update [dbo].[Quotes] set 
                                Deleted = 1
                                where Code = @Code
                                select * from [dbo].[Quotes] where Code = @Code";

                dbConnection.Open();
                return (await dbConnection.ExecuteAsync(query, new { Code = code }));
            }
        }

        private static string GetSelectQuery(string field)
        {
            return $@"select * from [dbo].[Quotes] where @{field} = {field}";
        }
    }
}
