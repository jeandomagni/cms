﻿using Cms.Model.Enum;
using Cms.Model.Models.BusinessProfile.Db;
using Cms.Model.Services;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Cms.Model.Repositories.BusinessProfileSeo
{
    public interface IBusinessProfileSeoRepository
    {
        Task<DbBusinessProfileSeo> Add(DbBusinessProfileSeo dbBusinessProfileSeo);
        Task<DbBusinessProfileSeo> GetByBbbIdAndBusinessIdAsync(string bbbId, string businessId);
        Task<DbBusinessProfileSeo> Update(DbBusinessProfileSeo dbBusinessProfileSeo);
    }

    public class BusinessProfileSeoRepository : IBusinessProfileSeoRepository
    {
        private readonly IConnectionStringService _connectionStringService;

        public BusinessProfileSeoRepository(
            IConnectionStringService connectionStringService)
        {
            _connectionStringService = connectionStringService;
        }

        public IDbConnection Connection =>
            new SqlConnection(
                _connectionStringService.GetVendorConnectionString(
                    Repository.CoreDb));

        public async Task<DbBusinessProfileSeo> Add(DbBusinessProfileSeo dbBusinessProfileSeo)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"insert into [dbo].[BusinessProfileSeo] (
                            BbbId,
                            BusinessId,
                            MetadataJson)
                        values (
	                        @BbbId,
                            @BusinessId,
                            @MetadataJson)

                            select * from [dbo].[BusinessProfileSeo] where Id = cast(scope_identity() as int)";
                dbConnection.Open();
                return await dbConnection.QueryFirstOrDefaultAsync<DbBusinessProfileSeo>(query, dbBusinessProfileSeo);
            }
        }

        public async Task<DbBusinessProfileSeo> GetByBbbIdAndBusinessIdAsync(string bbbId, string businessId)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"select * from [dbo].[BusinessProfileSeo] where BbbId = @BbbId and BusinessId = @BusinessId";
                dbConnection.Open();
                return await dbConnection.QueryFirstOrDefaultAsync<DbBusinessProfileSeo>(
                            query, new { BbbId = bbbId, BusinessId = businessId });
            }
        }

        public async Task<DbBusinessProfileSeo> Update(DbBusinessProfileSeo metadata)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"update [dbo].[BusinessProfileSeo] set
                            MetadataJson            = @MetadataJson

                            where BbbId = @BbbId and BusinessId = @BusinessId

                            select * from [dbo].[BusinessProfileSeo] where BbbId = @BbbId and BusinessId = @BusinessId";
                dbConnection.Open();
                return await dbConnection.QueryFirstOrDefaultAsync<DbBusinessProfileSeo>(query, metadata);
            }
        }

    }
}

