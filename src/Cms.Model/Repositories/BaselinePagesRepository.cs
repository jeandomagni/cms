﻿using Cms.Model.Enum;
using Cms.Model.Helpers;
using Cms.Model.Models.BaselinePages;
using Cms.Model.Models.Query.KendoGrid;
using Cms.Model.Services;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cms.Model.Repositories
{
    public interface IBaselinePagesRepository
    {
        Task<BaselinePage> CreateBaselinePage(BaselinePage model);
        Task<IEnumerable<BaselinePage>> PaginateShortlinks(GridOptions options, bool getShortlinks);
        Task<BaselinePage> UpdateBaselinePage(BaselinePage model);
        Task<int> DeleteBaselinePage(int id);
        Task<BaselinePage> GetById(int id);
        Task<BaselinePage> GetByCode(string code);
        Task<int> DeleteBaselinePageByCode(string code);
        Task<bool> IsSegmentUrlExists(string urlSegment, int? baselinePageId);
        Task<List<BaselinePage>> GetAll();
    }

    public class BaselinePagesRepository : IBaselinePagesRepository
    {
        private readonly IConnectionStringService _connectionStringService;

        public BaselinePagesRepository(IConnectionStringService connectionStringService)
        {
            _connectionStringService = connectionStringService;
        }

        public IDbConnection Connection =>
            new SqlConnection(
                _connectionStringService.GetVendorConnectionString(
                    Repository.CoreCmsDb));
        public async Task<BaselinePage> CreateBaselinePage(BaselinePage model)
        {

            using (var dbConnection = Connection)
            {
                const string query = @"insert into [dbo].[BaselinePages] (	
                                ContentCode,
                                UrlSegment,
                                RedirectUrl)
                            values (
	                            @ContentCode,
                                @UrlSegment,
                                @RedirectUrl)

                             select * from [dbo].[BaselinePages] where Id = cast(scope_identity() as int)";
                dbConnection.Open();
                return
                    (await dbConnection.QueryAsync<BaselinePage>(query, model)).FirstOrDefault();
            }
        }

        public async Task<int> DeleteBaselinePage(int id)
        {
            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                return (await dbConnection.ExecuteAsync(GetDeleteQuery("Id"), new { Id = id }));
            }
        }

        public async Task<int> DeleteBaselinePageByCode(string code)
        {
            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                return (await dbConnection.ExecuteAsync(GetDeleteQuery("ContentCode"), new { ContentCode = code }));
            }
        }

        private static string GetDeleteQuery(string field)
        {
            return $@"DELETE [dbo].[BaselinePages] where @{field} = {field}";
        }

        public async Task<BaselinePage> GetById(int id)
        {
            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                return (await dbConnection.QueryAsync<BaselinePage>(
                    GetSelectQuery("Id"), new { Id = id })).FirstOrDefault();
            }
        }

        public async Task<IEnumerable<BaselinePage>> PaginateShortlinks(GridOptions options, bool getShortlinks)
        {
            if (options.data == null)
                return null;

            using (var dbConnection = Connection)
            {
                try
                {
                    var pars = new DynamicParameters();
                    var sortField = "id";
                    var sortDirection = "desc";

                    if (options.data.sort != null && options.data.sort.Any())
                    {
                        sortField = options.data.sort[0].field;
                        sortDirection = options.data.sort[0].dir;
                    }

                    var filters = string.Empty;
                    if (options.data.filter?.filters != null)
                    {
                        filters = SqlHelpers.GetXml(options.data.filter.filters);
                    }

                    if (string.IsNullOrEmpty(filters))
                    {
                        filters = null;
                    }

                    pars.Add("@FilterList", filters);
                    pars.Add("@PageNumber", options.data.page);
                    pars.Add("@PageSize", options.data.pageSize);
                    pars.Add("@SortField", sortField);
                    pars.Add("@SortDir", sortDirection);
                    pars.Add("@GetShortlinks", getShortlinks);

                    dbConnection.Open();

                    return
                        await dbConnection.QueryAsync<BaselinePage>(
                            "PaginateBaselinePages", pars, commandType: CommandType.StoredProcedure);
                }
                finally
                {
                    if (dbConnection.State == ConnectionState.Open)
                    {
                        dbConnection.Close();
                    }
                }
            }
        }

        public async Task<BaselinePage> UpdateBaselinePage(BaselinePage model)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"update [dbo].[BaselinePages] set 
                                RedirectUrl   =   @RedirectUrl,
                                ContentCode    =   @ContentCode,
                                UrlSegment        =   @UrlSegment
                                where Id = @Id
                                select * from [dbo].[BaselinePages] where Id = @Id";

                dbConnection.Open();
                return (await dbConnection.QueryAsync<BaselinePage>(query, model)).FirstOrDefault();
            }
        }

        private static string GetSelectQuery(string field)
        {
            return $@"select * from [dbo].[BaselinePages] where @{field} = {field}";
        }

        public async Task<BaselinePage> GetByCode(string code)
        {
            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                return (await dbConnection.QueryAsync<BaselinePage>(
                    GetSelectQuery("ContentCode"), new { ContentCode = code })).FirstOrDefault();
            }
        }

        public async Task<bool> IsSegmentUrlExists(string urlSegment, int? baselinePageId)
        {
            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                return (await dbConnection.ExecuteScalarAsync<int?>("SELECT Id FROM [dbo].[BaselinePages] WHERE UrlSegment = @UrlSegment AND Id <> @BaselinePageId", new { UrlSegment = urlSegment, BaselinePageId = baselinePageId ?? 0 })) != null;
            }
        }

        public async Task<List<BaselinePage>> GetAll()
        {
            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                return (await dbConnection.QueryAsync<BaselinePage>("SELECT * FROM [dbo].[BaselinePages]")).ToList();
            }
        }
    }
}
