﻿using Cms.Domain.Helpers;
using Cms.Model.Enum;
using Cms.Model.Models.User;
using Cms.Model.Services;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Cms.Model.Repositories.Helpers;

namespace Cms.Model.Repositories
{
    public interface IUserProfileRepository
    {
        Task<UserProfile> Add(UserProfile userProfile);
        Task<UserProfile> GetById(int id);
        Task<UserProfile> GetForUser(string UserId);
        Task<UserProfile> Update(UserProfile userProfile);
        Task<UserProfile> GetByUserLogin(string userLogin);
        Task<string> GetDefaultCountry(string userId);
    }

    public class UserProfileRepository : IUserProfileRepository
    {
        private readonly IConnectionStringService _connectionStringService;

        public UserProfileRepository(IConnectionStringService connectionStringService)
        {
            _connectionStringService = connectionStringService;
        }

        public IDbConnection Connection =>
            new SqlConnection(
                _connectionStringService.GetVendorConnectionString(
                    Repository.CoreCmsDb));

        public async Task<UserProfile> Add(UserProfile userProfile)
        {
            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                return await dbConnection.AddProfileAsync(userProfile);
            }
        }


        public async Task<UserProfile> Update(UserProfile userProfile)
        {

            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                return await dbConnection.UpdateProfileAsync(userProfile);
            }
        }

        public async Task<UserProfile> GetById(int id)
        {
            const string query = @"select * from [dbo].[UserProfile] where Id = @Id";
            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                return (await dbConnection.QueryAsync<UserProfile>(
                    query, new { Id = id })).FirstOrDefault();
            }
        }

        public async Task<UserProfile> GetByUserLogin(string userLogin)
        {
            const string query = @"select * from [dbo].[UserProfile] where CreatedBy = @UserLogin";
            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                return (await dbConnection.QueryAsync<UserProfile>(
                    query, new { UserLogin = userLogin })).FirstOrDefault();
            }
        }

        public async Task<UserProfile> GetForUser(string userId)
        {
            const string query = @"SELECT up.*, b.Name [DefaultSiteName], b.PrimaryCountry [DefaultSiteCountry]
                                    FROM [CMSCore].[dbo].[UserProfile] up
                                    LEFT JOIN BbbInfo b ON b.LegacyId = up.DefaultSite where up.UserId = @UserId";

            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                return
                    (await dbConnection.QueryAsync<UserProfile>(
                        query, new { UserId = userId })).FirstOrDefault();
            }
        }

        public async Task<string> GetDefaultCountry(string userId)
        {
            const string query = @"SELECT COALESCE(up.BaseUriCountry, b.PrimaryCountry)
                                    FROM [CMSCore].[dbo].[UserProfile] up
                                    LEFT JOIN BbbInfo b ON b.LegacyId = up.DefaultSite where up.UserId = @UserId";

            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                var country = await dbConnection.QuerySingleAsync<string>(query, new { UserId = userId });
                return CultureHelpers.GetThreeLetterCountry(country) ?? country;
            }
        }
    }
}
