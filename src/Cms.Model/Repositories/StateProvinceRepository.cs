﻿using Cms.Model.Enum;
using Cms.Model.Services;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Cms.Model.Repositories
{
    public interface IStateProvinceRepository
    {
        Task<IEnumerable<string>> GetByCountryCodeAsync(string countryCode);
        Task<string> GetCountryCodeForStateCodeAsync(string stateCode);
    }
    public class StateProvinceRepository : IStateProvinceRepository
    {
        private readonly IConnectionStringService _connectionStringService;

        public StateProvinceRepository(IConnectionStringService connectionStringService)
        {
            _connectionStringService = connectionStringService;
        }

        public IDbConnection Connection =>
            new SqlConnection(
                _connectionStringService.GetVendorConnectionString(
                    Repository.CoreDb));

        public async Task<IEnumerable<string>> GetByCountryCodeAsync(string countryCode)
        {
            const string query = @"SELECT StateCode
                FROM [dbo].[refState]
                WHERE CountryCode = @CountryCode
                ORDER BY SortOrder
            ";

            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                return await dbConnection.QueryAsync<string>(query, new { CountryCode = countryCode });
            }
        }

        public async Task<string> GetCountryCodeForStateCodeAsync(string stateCode)
        {
            const string query = @"SELECT CountryCode
                FROM [dbo].[refState]
                WHERE StateCode = @StateCode
            ";

            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                return await dbConnection.QueryFirstOrDefaultAsync<string>(query, new { StateCode = stateCode });
            }
        }
    }
}
