﻿using Cms.Model.Enum;
using Cms.Model.Models;
using Cms.Model.Services;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Cms.Model.Repositories
{
    public interface IVerificationClientRepository
    {
        Task<VerificationClient> Get(string clientId, string userLogin);
        Task<VerificationClient> Add(VerificationClient verificationClient);
        Task<VerificationClient> Update(VerificationClient verificationClient);
        Task<int> DeleteForLogin(string userLogin);
        Task<int> Delete(int id);
    }

    public class VerificationClientRepository:IVerificationClientRepository
    {
        private readonly IConnectionStringService _connectionStringService;

        public VerificationClientRepository(IConnectionStringService connectionStringService)
        {
            _connectionStringService = connectionStringService;
        }

        public IDbConnection Connection =>
            new SqlConnection(_connectionStringService.GetVendorConnectionString(Repository.CoreCmsDb));


        public async Task<VerificationClient> Get(string clientId, string userLogin)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"select * from [dbo].[VerificationClient] 
                               where ClientId = @ClientId and CreatedBy = @UserLogin";

                dbConnection.Open();
                return
                    (await dbConnection.QueryAsync<VerificationClient>(
                        query, new { ClientId = clientId, UserLogin = userLogin })).FirstOrDefault();
            }
        }

        public async Task<VerificationClient> Add(VerificationClient verificationClient)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"insert into [dbo].[VerificationClient] (
                                        ClientId,
                                        CreatedBy, 
                                        CreatedDate)
                                    values (
	                                    @ClientId,
	                                    @CreatedBy,
                                        sysdatetimeoffset())

                                    select * from [dbo].[VerificationClient] where Id = cast(scope_identity() as int)";
                dbConnection.Open();
                return
                    (await dbConnection.QueryAsync<VerificationClient>(query, verificationClient)).FirstOrDefault();
            }
        }

        public async Task<VerificationClient> Update(VerificationClient verificationClient)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"update [dbo].[VerificationClient] 
                                    set  Verified = @Verified,
                                         ClientId = @ClientId
                                    where Id = @Id
                                    select * from [dbo].[VerificationClient] where Id = @Id";
                dbConnection.Open();
                return
                    (await dbConnection.QueryAsync<VerificationClient>(query, verificationClient)).FirstOrDefault();
            }
        }

        public async Task<int> DeleteByClientId(int clientId)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"delete from [dbo].[VerificationClient] where ClientId = @ClientId";
                return
                    await dbConnection.ExecuteAsync(
                        query, new { ClientId = clientId });
            }
        }

        public async Task<int> Delete(int id)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"delete from [dbo].[VerificationClient] where Id = @Id";
                return
                    await dbConnection.ExecuteAsync(
                        query, new { Id = id });
            }
        }

        public async Task<int> DeleteForLogin(string userLogin)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"delete from [dbo].[VerificationClient] where CreatedBy = @UserLogin";
                return
                    await dbConnection.ExecuteAsync(
                        query, new { UserLogin = userLogin });
            }
        }
    }
}
