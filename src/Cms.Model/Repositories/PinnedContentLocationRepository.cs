﻿using Cms.Model.Enum;
using Cms.Model.Models.ContentLocation.Db;
using Cms.Model.Services;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Cms.Model.Repositories
{
    public interface IPinnedContentLocationRepository
    {
        Task<DbPinnedContentLocation> GetById(int id);
        Task<IEnumerable<DbPinnedContentLocation>> GetAll();
        Task<IEnumerable<DbPinnedContentLocation>> GetByContentCode(string contentCode);
        Task<DbPinnedContentLocation> Create(DbPinnedContentLocation pinnedContentLocation);
        Task<DbPinnedContentLocation> Update(DbPinnedContentLocation pinnedContentLocation);
        Task<int> Delete(int id);
        Task<int> DeleteByContentCode(string contentCode);
    }

    public class PinnedContentLocationRepository : IPinnedContentLocationRepository
    {
        private readonly IConnectionStringService _connectionStringService;

        public PinnedContentLocationRepository(
            IConnectionStringService connectionStringService) {
            _connectionStringService = connectionStringService;
        }

        public IDbConnection Connection =>
            new SqlConnection(
                _connectionStringService.GetVendorConnectionString(
                    Repository.CoreCmsDb));

        public async Task<DbPinnedContentLocation> GetById(int id)
        {
            using (var dbConnection = Connection)
            {
                dbConnection.Open();

                return
                    (await dbConnection.QueryAsync<DbPinnedContentLocation>(
                        GetSelectQuery("Id"), new { Id = id })).FirstOrDefault();
            }
        }

        public async Task<IEnumerable<DbPinnedContentLocation>> GetAll()
        {
            using (var dbConnection = Connection)
            {
                dbConnection.Open();

                return
                    (await dbConnection.QueryAsync<DbPinnedContentLocation>(
                        "select * from [dbo].[PinnedContentLocation]"));
            }
        }

        public async Task<IEnumerable<DbPinnedContentLocation>> GetByContentCode(string contentCode)
        {
            using (var dbConnection = Connection)
            {
                dbConnection.Open();

                return
                    (await dbConnection.QueryAsync<DbPinnedContentLocation>(
                        GetSelectQuery("ContentCode"), new { ContentCode = contentCode }));
            }
        }

        public async Task<DbPinnedContentLocation> Create(DbPinnedContentLocation pinnedContentLocation)
        {
            using (var dbConnection = Connection)
            {

                const string query = @"insert into [dbo].[PinnedContentLocation] (	
	                            ContentCode,
                                PinExpirationDate,
                                PinnedLocationId,
                                PinnedLocationType,
                                CreatedBy,
                                CreatedDate
                                )
                            values (
	                            @ContentCode,
                                @PinExpirationDate,
                                @PinnedLocationId,
                                @PinnedLocationType,
                                @CreatedBy,
                                @CreatedDate)

                             select * from [dbo].[PinnedContentLocation] where Id = cast(scope_identity() as int)";
                dbConnection.Open();
                return
                    (await dbConnection.QueryAsync<DbPinnedContentLocation>(query, pinnedContentLocation)).FirstOrDefault();
            }
        }

        public async Task<DbPinnedContentLocation> Update(DbPinnedContentLocation pinnedContentLocation)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"update [dbo].[PinnedContentLocation] set 
                              PinExpirationDate    = @PinExpirationDate
                              where Id = @Id
                              select * from [dbo].[PinnedContentLocation] where Id = @Id";

                dbConnection.Open();
                return (await dbConnection.QueryAsync<DbPinnedContentLocation>(query, pinnedContentLocation)).FirstOrDefault();
            }
        }
        
        public async Task<int> Delete(int id)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"DELETE FROM [dbo].[PinnedContentLocation] WHERE Id = @Id";
                dbConnection.Open();
                return
                     await dbConnection.ExecuteAsync(query, new { Id = id });
            }
        }

        public async Task<int> DeleteByContentCode(string contentCode)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"DELETE FROM [dbo].[PinnedContentLocation] WHERE ContentCode = @ContentCode";
                dbConnection.Open();
                return
                     await dbConnection.ExecuteAsync(query, new { ContentCode = contentCode });
            }
        }

        private static string GetSelectQuery(string field) {
            return $@"select * from [dbo].[PinnedContentLocation] where @{field} = {field}";
        }
    }
}
