﻿using Cms.Model.Enum;
using Cms.Model.Models.BbbInfo.Db;
using Cms.Model.Services;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Cms.Model.Repositories
{
    public interface IBbbInfoRepository
    {
        Task<DbBbbInfo> Add(DbBbbInfo BbbInfo);
        Task<DbBbbInfo> GetById(int id);
        Task<IEnumerable<DbBbbInfo>> GetAll();
        Task<DbBbbInfo> Update(DbBbbInfo BbbInfo);
        Task<DbBbbInfo> GetByLegacyId(string bbbId);
    }

    public class BbbInfoRepository : IBbbInfoRepository
    {
        private readonly IConnectionStringService _connectionStringService;

        public BbbInfoRepository(
            IConnectionStringService connectionStringService) {
            _connectionStringService = connectionStringService;
        }

        public IDbConnection Connection =>
            new SqlConnection(
                _connectionStringService.GetVendorConnectionString(
                    Repository.CoreCmsDb));

        public async Task<DbBbbInfo> Add(DbBbbInfo BbbInfo)
        {
            using (var dbConnection = Connection)
            {

                const string query = @"insert into [dbo].[BbbInfo] (	
                                         LegacySiteId,
                                         LegacyId,
                                         HeadScript,
                                         OpenBodyScript,
                                         CloseBodyScript,
                                         Name,
                                         Tagline,
                                         AboutUsText,
                                         InformationPageImageUrl,
                                         EmploymentOpportunitiesUrl,
                                         ApplyForAccreditationUrl,
                                         JoinApplyUrl,
                                         FileComplaintUrl,
                                         SubmitReviewUrl,
                                         NewsLetterSignupUrl,
                                         ClaimListingUrl,
                                         BusinessLoginUrl,
                                         ConsumerLoginUrl,
                                         AnnualReportsUrl,
                                         Locations,
                                         AreaCodes,
                                         Subdomains,
                                         BaseUriAliases,
                                         PhoneLeadsEmails,
                                         Vendor,
                                         AllowVendorUriAliases,
                                         GatewayEnabled,
                                         RaqEnabled,
                                         PrimaryCountry,
                                         PrimaryLanguage,
                                         Persons,
                                         CharityProfileSource,
                                         Programs,
                                         GoogleAnalyticsId,
                                         SocialMedia,
                                         CreatedDate,
                                         CreatedBy,
                                         Active,
                                         VendorHostHeader,
                                         HeaderFooterFlag,
                                         AdditionalResources,
                                         EventsPageLayout,
                                         HeaderLinks,
                                         FooterLinks,
                                         BbbUrlSegment,
                                         MenuLinks,
                                         Languages,
                                         PopularCategoriesJson,
                                         ComplaintQualifyingQuestionsJson,
                                         PrimaryUrlAlias,
                                         ApplyForAccreditationInfoJson,
                                         LeadsgenLinkEnabled,
                                         BaseUrl,
                                         BusinessLeadEmailNotficationEnabled)
                                    values (
                                        @LegacySiteId,
                                        @LegacyId,
                                        @HeadScript,
                                        @OpenBodyScript,
                                        @CloseBodyScript,
                                        @Name,
                                        @Tagline,
                                        @AboutUsText,
                                        @InformationPageImageUrl,
                                        @EmploymentOpportunitiesUrl,
                                        @ApplyForAccreditationUrl,
                                        @JoinApplyUrl,
                                        @FileComplaintUrl,
                                        @SubmitReviewUrl,
                                        @NewsLetterSignupUrl,
                                        @ClaimListingUrl,
                                        @BusinessLoginUrl,
                                        @ConsumerLoginUrl,
                                        @AnnualReportsUrl,
                                        @Locations,
                                        @AreaCodes,
                                        @Subdomains,
                                        @BaseUriAliases,
                                        @PhoneLeadsEmails,
                                        @Vendor,
                                        @AllowVendorUriAliases,
                                        @GatewayEnabled,
                                        @RaqEnabled,
                                        @PrimaryCountry,
                                        @PrimaryLanguage,
                                        @Persons,
                                        @CharityProfileSource,
                                        @Programs,
                                        @GoogleAnalyticsId,
                                        @SocialMedia,
                                        sysdatetimeoffset(),
                                        @CreatedBy,
                                        @Active,
                                        @VendorHostHeader,
                                        @HeaderFooterFlag,
                                        @AdditionalResources,
                                        @EventsPageLayout,
                                        @HeaderLinks,
                                        @FooterLinks,
                                        @BbbUrlSegment,
                                        @MenuLinks,
                                        @Languages,
                                        @PopularCategoriesJson,
                                        @ComplaintQualifyingQuestionsJson,
                                        @PrimaryUrlAlias,
                                        @ApplyForAccreditationInfoJson,
                                        @LeadsgenLinkEnabled,
                                        @BaseUrl,
                                        @BusinessLeadEmailNotficationEnabled)

                             select * from [dbo].[BbbInfo] where Id = cast(scope_identity() as int)";
                dbConnection.Open();
                return
                    (await dbConnection.QueryAsync<DbBbbInfo>(query, BbbInfo)).FirstOrDefault();
            }
        }

        public async Task<DbBbbInfo> GetById(int id)
        {
            using (var dbConnection = Connection)
            {
                dbConnection.Open();

                return
                    (await dbConnection.QueryAsync<DbBbbInfo>(
                        @"select * from [dbo].[BbbInfo] where Id = @Id", new { Id = id })).FirstOrDefault();
            }
        }

        public async Task<DbBbbInfo> GetByLegacyId(string bbbId)
        {
            using (var dbConnection = Connection)
            {
                dbConnection.Open();

                return
                    (await dbConnection.QueryAsync<DbBbbInfo>(
                        @"select * from [dbo].[BbbInfo] where LegacyId = @Id", new { Id = bbbId })).FirstOrDefault();
            }
        }

        public async Task<IEnumerable<DbBbbInfo>> GetAll() {
            using (var dbConnection = Connection)
            {
                dbConnection.Open();

                return
                    await dbConnection.QueryAsync<DbBbbInfo>(
                        @"select * from [dbo].[BbbInfo]");
            }
        }

        public async Task<DbBbbInfo> Update(DbBbbInfo BbbInfo)
        {
            using (var dbConnection = Connection)
            {
                var query = @"update [dbo].[BbbInfo] set 
                                 LegacySiteId                        = @LegacySiteId,
                                 LegacyId                            = @LegacyId,
                                 HeadScript                          = @HeadScript,
                                 OpenBodyScript                      = @OpenBodyScript,
                                 CloseBodyScript                     = @CloseBodyScript,
                                 Name                                = @Name,
                                 Tagline                             = @Tagline,
                                 AboutUsText                         = @AboutUsText,
                                 InformationPageImageUrl             = @InformationPageImageUrl,
                                 EmploymentOpportunitiesUrl          = @EmploymentOpportunitiesUrl,
                                 ApplyForAccreditationUrl            = @ApplyForAccreditationUrl,       
                                 JoinApplyUrl                        = @JoinApplyUrl,       
                                 FileComplaintUrl                    = @FileComplaintUrl,
                                 SubmitReviewUrl                     = @SubmitReviewUrl,
                                 NewsLetterSignupUrl                 = @NewsLetterSignupUrl,
                                 ClaimListingUrl                     = @ClaimListingUrl,
                                 BusinessLoginUrl                    = @BusinessLoginUrl,
                                 ConsumerLoginUrl                    = @ConsumerLoginUrl,
                                 AnnualReportsUrl                    = @AnnualReportsUrl,
                                 Locations                           = @Locations,
                                 AreaCodes                           = @AreaCodes,
                                 Subdomains                          = @Subdomains,
                                 BaseUriAliases                      = @BaseUriAliases,
                                 PhoneLeadsEmails                    = @PhoneLeadsEmails,
                                 Vendor                              = @Vendor,
                                 AllowVendorUriAliases               = @AllowVendorUriAliases,
                                 GatewayEnabled                      = @GatewayEnabled,
                                 RaqEnabled                          = @RaqEnabled,
                                 PrimaryCountry                      = @PrimaryCountry,
                                 PrimaryLanguage                     = @PrimaryLanguage,
                                 Persons                             = @Persons,
                                 CharityProfileSource                = @CharityProfileSource,
                                 Programs                            = @Programs,
                                 GoogleAnalyticsId                   = @GoogleAnalyticsId,
                                 SocialMedia                         = @SocialMedia,
                                 ModifiedDate                        = sysdatetimeoffset(),
                                 ModifiedBy                          = @ModifiedBy,
                                 Active                              = @Active,
                                 VendorHostHeader                    = @VendorHostHeader,
                                 HeaderFooterFlag                    = @HeaderFooterFlag,
                                 AdditionalResources                 = @AdditionalResources,
                                 EventsPageLayout                    = @EventsPageLayout,
                                 HeaderLinks                         = @HeaderLinks,
                                 FooterLinks                         = @FooterLinks,
                                 BbbUrlSegment                       = @BbbUrlSegment,
                                 MenuLinks                           = @MenuLinks,
                                 Languages                           = @Languages,
                                 PopularCategoriesJson               = @PopularCategoriesJson,
                                 ComplaintQualifyingQuestionsJson    = @ComplaintQualifyingQuestionsJson,
                                 PrimaryUrlAlias                     = @PrimaryUrlAlias,
                                 ApplyForAccreditationInfoJson       = @ApplyForAccreditationInfoJson,
                                 LeadsgenLinkEnabled                 = @LeadsgenLinkEnabled,
                                 BaseUrl                             = @BaseUrl,
                                 BusinessLeadEmailNotficationEnabled = @BusinessLeadEmailNotficationEnabled
                                 where Id = @Id
                                 select * from [dbo].[BbbInfo] where Id = @Id";

                dbConnection.Open();
                return (await dbConnection.QueryAsync<DbBbbInfo>(query, BbbInfo)).FirstOrDefault();
            }
        }
    }
}
