﻿using Cms.Model.Enum;
using Cms.Model.Helpers;
using Cms.Model.Models;
using Cms.Model.Models.Query.KendoGrid;
using Cms.Model.Services;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Cms.Model.Repositories
{
    public interface IFileRepository
    {
        Task<File> Add(File file);
        Task<File> Update(File file);
        Task<File> GetByCode(string code);
        Task<IEnumerable<File>> Paginate(GridOptions options, string userId);
        Task<IEnumerable<File>> Query(string searchText, int resultSet);
        Task<int> Delete(string code);
        Task<IEnumerable<File>> GetAll();
    }

    public class FileRepository : IFileRepository
    {
        private readonly IConnectionStringService _connectionStringService;

        public FileRepository(IConnectionStringService connectionStringService)
        {
            _connectionStringService = connectionStringService;
        }

        public IDbConnection Connection =>
            new SqlConnection(_connectionStringService.GetVendorConnectionString(
                    Repository.CoreCmsDb));

        public async Task<IEnumerable<File>> Paginate(GridOptions options, string userId)
        {
            if (options.data == null)
                return null;

            using (var dbConnection = Connection)
            {
                try
                {
                    var pars = new DynamicParameters();
                    var sortField = "id";
                    var sortDirection = "desc";

                    if (options.data.sort != null && options.data.sort.Any())
                    {
                        sortField = options.data.sort[0].field;
                        sortDirection = options.data.sort[0].dir;
                    }

                    var filters = string.Empty;
                    if (options.data.filter?.filters != null)
                    {
                        filters = SqlHelpers.GetXml(options.data.filter.filters);
                    }

                    if (string.IsNullOrEmpty(filters))
                    {
                        filters = null;
                    }

                    pars.Add("@FilterList", filters);
                    pars.Add("@UserId", userId);
                    pars.Add("@PageNumber", options.data.page);
                    pars.Add("@PageSize", options.data.pageSize);
                    pars.Add("@SortField", sortField);
                    pars.Add("@SortDir", sortDirection);
                    pars.Add("@LocationIds", options.data.locations);

                    dbConnection.Open();

                    return
                        await dbConnection.QueryAsync<File>(
                            "PaginateFiles", pars, commandType: CommandType.StoredProcedure);
                }
                finally
                {
                    if (dbConnection.State == ConnectionState.Open)
                    {
                        dbConnection.Close();
                    }
                }
            }
        }

        public async Task<IEnumerable<File>> Query(string searchText, int resultSet)
        {
            using (var dbConnection = Connection)
            {
                var query = @"SELECT TOP (@limit) * FROM [dbo].[Files]
                              WHERE ([Title] LIKE @title OR [Tags] LIKE @tags) AND Deleted = 0
                              ORDER BY 1 DESC";

                dbConnection.Open();

                searchText = searchText == "all" ? "%%" : $"%{searchText}%";
                return
                    await dbConnection.QueryAsync<File>(
                        query, new { limit = resultSet, title = searchText, tags = searchText });
            }
        }

        public async Task<File> Add(File file)
        {
            using (var dbConnection = Connection)
            {
                var query = @"INSERT INTO [dbo].[files] (
                                title,
                                code,
                                tags,
                                contentType,
                                path,
                                createDate,
                                createdBy,
                                deleted,
                                uri,
                                OwnerSiteId,
                                Type,
                                Published,
                                FileSizeKb,
                                Caption,
                                Credit) 
                                VALUES (  
                                @Title,
                                @Code,
                                @Tags,
                                @ContentType,
                                @Path,
                                sysdatetimeoffset(),
                                @CreatedBy,
                                0,
                                @Uri,
                                @OwnerSiteId,
                                @Type,
                                @Published,
                                @FileSizeKb,
                                @Caption,
                                @Credit)
                                SELECT * FROM [dbo].[Files] WHERE Id = CAST(SCOPE_IDENTITY() AS INT)";
                dbConnection.Open();                
                return 
                    (await dbConnection.QueryAsync<File>(query, file)).FirstOrDefault();
            }
        }

        public async Task<File> Update(File file)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"UPDATE [dbo].[Files] SET 
                                          Title = @Title,
                                          Tags = @Tags,
                                          Credit = @Credit,
                                          Caption = @Caption,
                                          Published = @Published
                                          WHERE Id = @Id
                                          SELECT * FROM [dbo].[Files] WHERE Id = @Id";

                dbConnection.Open();
                return 
                    (await dbConnection.QueryAsync<File>(query, file)).FirstOrDefault();
            }
        }

        public async Task<File> GetByCode(string code)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"SELECT * FROM [dbo].[Files] WHERE Code = @Code";

                dbConnection.Open();
                return
                    (await dbConnection.QueryAsync<File>(query, new { Code = code })).FirstOrDefault();
            }
        }

        public async Task<int> Delete(string code)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"DELETE FROM [dbo].[Files] WHERE Code = @Code";

                dbConnection.Open();
                return
                    await dbConnection.ExecuteAsync(query, new { Code = code });
            }
        }

        public async Task<IEnumerable<File>> GetAll()
        {
            using (var dbConnection = Connection)
            {
                const string query = @"SELECT * FROM [dbo].[Files]";

                dbConnection.Open();
                return await dbConnection.QueryAsync<File>(query);
            }
        }
    }
}
