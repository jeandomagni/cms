﻿using Cms.Model.Enum;
using Cms.Model.Services;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Cms.Model.Helpers;

namespace Cms.Model.Repositories
{
    public interface ITagRepository
    {
        Task<int> UpdateContentTags(int contentId, IEnumerable<string> tags);
        Task<IEnumerable<string>> Query(string tag, string userId);
        Task<int> InsertTags(IEnumerable<string> tags, string userId);
        Task<IEnumerable<string>> GetExistingTags(IEnumerable<string> tags);
    }

    public class TagRepository : ITagRepository
    {
        private readonly IConnectionStringService _connectionStringService;

        public TagRepository(IConnectionStringService connectionStringService)
        {
            _connectionStringService = connectionStringService;
        }

        public IDbConnection Connection =>
            new SqlConnection(
                _connectionStringService.GetVendorConnectionString(
                    Repository.CoreCmsDb));


        public async Task<int> UpdateContentTags(int contentId, IEnumerable<string> tags)
        {
            var formattedTags = FormatTags(tags);
            const string query = @"update [dbo].[Content] set Tags = @tags where Id = @Id";
            using (var dbConnection = Connection)
            {

                dbConnection.Open();
                return 
                    (await dbConnection.QueryAsync<int>(
                        query, new { Tags = formattedTags, Id = contentId })).FirstOrDefault();
            }
        }

        public async Task<IEnumerable<string>> Query(string tag, string userId)
        {
            const string query = @"select top 10 Tag from [dbo].[Tags] where Tag like @tag order by Tag";
            using (var dbConnection = Connection)
            {
                var paramTag = $"%{tag}%";
                dbConnection.Open();
                return
                    await dbConnection.QueryAsync<string>(query, new { Tag = paramTag });
            }
        }

        public async Task<IEnumerable<string>> GetExistingTags(IEnumerable<string> tags)
        {
            if (tags == null)
            {
                return new List<string>();
            }
            var tagList = tags.ToList();
            if (!tagList.Any())
            {
                return new List<string>();
            }

            var query = $@"select Tag from [dbo].[Tags] where Tag in ({
                string.Join(',', tagList.Select(tag => $"'{SqlHelpers.SanitizeString(tag)}'"))})";

            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                return
                    await dbConnection.QueryAsync<string>(query);
            }
        }

        public async Task<int> InsertTags(IEnumerable<string> tags, string userId)
        {
            var query = $@"insert into [dbo].[Tags] (tag) {
                string.Join(" union all ", tags.Select(tag => $"select '{SqlHelpers.SanitizeString(tag)}'"))
                }";

            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                return
                    await dbConnection.ExecuteAsync(query);
            }
        }

        private static string FormatTags(IEnumerable<string> input)
        {
            var tags = input as string[] ?? input.ToArray();
            if (input == null || !tags.Any())
                return null;

            return string.Join(",",
                tags.Where(t=>!string.IsNullOrWhiteSpace(t)).Select(t => t.ToLower().Trim()).Distinct());
        }
    }
}
