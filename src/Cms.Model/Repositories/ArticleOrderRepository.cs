﻿using Cms.Model.Enum;
using Cms.Model.Models.Content;
using Cms.Model.Models.Query;
using Cms.Model.Services;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cms.Model.Repositories
{
    public interface IArticleOrderRepository
    {
        Task UpdateOrder(int id, string order);
        Task<ArticleOrder> Insert(ArticleOrder articleOrder);
        Task<ArticleOrder> Get(int id);
        Task<List<ArticleOrder>> GetAll();
        Task DeleteAll();
        Task<List<ArticleOrder>> GetBySeveralFilters(List<ArticleOrder> articleOrders);
        Task UpdateOrderMultiple(List<ArticleOrder> articleOrders);
        Task<List<ArticleOrder>> InsertMultiple(List<ArticleOrder> articleOrders);
        Task<bool> HasAny();
        Task Delete(int id);
        Task<List<ArticleOrder>> GetByContentId(string id);
        Task OverrideFeed(int articleOrderId, int? overrideFeedId);
    }
    public class ArticleOrderRepository : IArticleOrderRepository
    {
        private readonly IConnectionStringService _connectionStringService;

        public ArticleOrderRepository(
            IConnectionStringService connectionStringService)
        {
            _connectionStringService = connectionStringService;
        }

        public IDbConnection Connection =>
            new SqlConnection(
                _connectionStringService.GetVendorConnectionString(
                    Repository.CoreCmsDb));

        public async Task DeleteAll()
        {
            using (var dbConnection = Connection)
            {
                const string query = @"DELETE FROM [dbo].[ArticleOrder]";
                dbConnection.Open();
                await dbConnection.ExecuteScalarAsync(query);
            }
        }

        public async Task<ArticleOrder> Get(int id)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"SELECT * FROM [dbo].[ArticleOrder] WHERE Id = @Id";
                dbConnection.Open();
                return await dbConnection.QueryFirstOrDefaultAsync<ArticleOrder>(query, new { Id = id });
            }
        }

        private void AddWhereClause(StringBuilder queryBuilder, string fieldName, string fieldValue, bool first = false)
        {
            string and = "AND";
            if (first)
            {
                and = "";
            }
            queryBuilder.AppendFormat(" {0} {1} = '{2}'", and, fieldName, fieldValue ?? string.Empty);
        }

        public async Task<List<ArticleOrder>> GetAll()
        {
            using (var dbConnection = Connection)
            {
                const string query = @"SELECT * FROM [dbo].[ArticleOrder]";
                dbConnection.Open();
                return (await dbConnection.QueryAsync<ArticleOrder>(query)).ToList();
            }
        }

        public async Task<ArticleOrder> Insert(ArticleOrder articleOrder)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"INSERT INTO [dbo].[ArticleOrder] 
                    (Country, State, City, ServiceArea, ListingType, OrderedArticleIds, ModifiedDate) 
                    VALUES
                    (@Country, @State, @City, @ServiceArea, @ListingType, @OrderedArticleIds, @ModifiedDate)
                    
                    SELECT cast(scope_identity() as int)";
                dbConnection.Open();
                var id = await dbConnection.ExecuteScalarAsync<int>(query, articleOrder);
                articleOrder.Id = id;

                return articleOrder;
            }
        }

        public async Task UpdateOrder(int id, string order)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"update [dbo].[ArticleOrder] set OrderedArticleIds = @OrderedArticleIds, ModifiedDate = @ModifiedDate where Id = @Id";
                dbConnection.Open();
                await dbConnection.ExecuteScalarAsync(query, new { Id = id, OrderedArticleIds = order, ModifiedDate = DateTime.UtcNow });
            }
        }

        public async Task<List<ArticleOrder>> GetBySeveralFilters(List<ArticleOrder> articleOrders)
        {
            if (articleOrders.Count == 0) return new List<ArticleOrder>();

            using (var dbConnection = Connection)
            {
                StringBuilder query = new StringBuilder("SELECT * FROM [dbo].[ArticleOrder] WHERE ");

                var topics = articleOrders
                    .Select(x => x.Topic)
                    .Distinct()
                    .Select(x => $"\'{x}\'")
                    .ToList();

                var listingTypes = articleOrders
                    .Select(x => x.ListingType)
                    .Distinct()
                    .Select(x => $"\'{x}\'")
                    .ToList();

                var statesWithCity = articleOrders
                    .Where(x => !string.IsNullOrWhiteSpace(x.City) && !string.IsNullOrWhiteSpace(x.State))
                    .GroupBy(x => x.State)
                    .ToList();

                var countries = articleOrders
                    .Where(x => string.IsNullOrWhiteSpace(x.City) && string.IsNullOrWhiteSpace(x.State) && string.IsNullOrWhiteSpace(x.ServiceArea) && !string.IsNullOrWhiteSpace(x.Country) && !x.NotTagged)
                    .Select(x => x.Country)
                    .Where(x => !string.IsNullOrWhiteSpace(x))
                    .Distinct()
                    .Select(x => $"\'{x}\'")
                    .ToList();

                var states = articleOrders
                    .Where(x => string.IsNullOrWhiteSpace(x.City) && !string.IsNullOrWhiteSpace(x.State) && string.IsNullOrWhiteSpace(x.ServiceArea) && !x.NotTagged)
                    .Select(x => x.State)
                    .Where(x => !string.IsNullOrWhiteSpace(x))
                    .Distinct()
                    .Select(x => $"\'{x}\'")
                    .ToList();

                var serviceAreas = articleOrders
                    .Select(x => x.ServiceArea)
                    .Where(x => !string.IsNullOrWhiteSpace(x))
                    .Distinct()
                    .Select(x => $"\'{x}\'")
                    .ToList();

                query.Append("(");
                query.Append($"ListingType  IN ({string.Join(",", listingTypes)}) AND ");
                query.Append($"Topic        IN ({string.Join(",", topics)})");
                query.Append(") AND ");
                query.AppendLine();

                query.Append("(");
                if (countries.Count > 0)
                {
                    query.Append($"(Country   IN ({string.Join(",", countries)})    AND City = '' AND State = '' AND ServiceArea = '')");
                    query.Append(" OR ");
                    query.AppendLine();
                }

                if (states.Count > 0)
                {
                    query.Append($"(State      IN ({string.Join(",", states)})       AND City = '' AND ServiceArea = '')");
                    query.Append(" OR ");
                    query.AppendLine();
                }

                if (serviceAreas.Count > 0)
                {
                    query.Append($"(ServiceArea IN ({string.Join(",", serviceAreas)}) AND City = '' AND State = '')");
                    query.Append(" OR ");
                    query.AppendLine();
                }

                for (int i = 0; i < statesWithCity.Count; i++)
                {
                    var item = statesWithCity[i];
                    query.Append($"(State = '{item.Key}' AND City IN ({string.Join(",", item.Select(x => $"\'{x.City.Replace("'", "''")}\'").Distinct())}))");

                    query.Append(" OR ");
                    query.AppendLine();
                }
                var queryString = query.ToString();
                queryString = queryString.Substring(0, queryString.LastIndexOf("OR"));
                queryString = $"{queryString})";


                dbConnection.Open();
                return (await dbConnection.QueryAsync<ArticleOrder>(queryString)).ToList();
            }
        }

        public async Task UpdateOrderMultiple(List<ArticleOrder> articleOrders)
        {
            int bulkCount = 50;
            for (int i = 0; i < articleOrders.Count; i += bulkCount)
            {
                var items = articleOrders.Skip(i).Take(bulkCount).ToList();

                StringBuilder query = new StringBuilder();
                foreach (var item in items)
                {
                    query.AppendFormat("update [dbo].[ArticleOrder] set OrderedArticleIds = '{0}', ModifiedDate = '{1}' where Id = {2}", item.OrderedArticleIds, item.ModifiedDate, item.Id);
                    query.AppendLine();
                }
                using (var dbConnection = Connection)
                {
                    dbConnection.Open();
                    await dbConnection.ExecuteScalarAsync(query.ToString(), commandTimeout: 300);
                }
            }
        }

        public async Task<List<ArticleOrder>> InsertMultiple(List<ArticleOrder> articleOrders)
        {
            if (articleOrders.Count == 0) return new List<ArticleOrder>();

            using (var dbConnection = Connection)
            {
                var query = new StringBuilder(@"INSERT INTO [dbo].[ArticleOrder] 
                    (Country, State, City, ServiceArea, ListingType, OrderedArticleIds, ModifiedDate, Topic)
                    OUTPUT inserted.Id
                    VALUES");

                foreach (var item in articleOrders)
                {
                    query.AppendLine();
                    query.AppendFormat("('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}'),", item.Country, item.State, item.City, item.ServiceArea, item.ListingType, item.OrderedArticleIds, item.ModifiedDate, item.Topic);
                }
                query = query.Remove(query.Length - 1, 1); // remove last comma

                dbConnection.Open();
                var ids = (await dbConnection.QueryAsync<int>(query.ToString())).ToList();
                for (int i = 0; i < ids.Count; i++)
                {
                    articleOrders[i].Id = ids[i];
                }

                return articleOrders;
            }
        }

        public async Task<bool> HasAny()
        {
            using (var dbConnection = Connection)
            {
                const string query = @"select TOP (1) Id FROM [dbo].[ArticleOrder]";
                dbConnection.Open();
                return await dbConnection.ExecuteScalarAsync<int>(query) != 0;

            }
        }

        public async Task Delete(int id)
        {
            using (var dbConnection = Connection)
            {
                string query = $@"DELETE FROM [dbo].[ArticleOrder] WHERE Id = '{id}'";
                dbConnection.Open();
                await dbConnection.ExecuteScalarAsync<int>(query);

            }
        }

        public async Task<List<ArticleOrder>> GetByContentId(string id)
        {
            using (var dbConnection = Connection)
            {
                var sql = $@"SELECT * FROM [dbo].[ArticleOrder] 
                            WHERE OrderedArticleIds LIKE '%{id}%'";
                return (await dbConnection.QueryAsync<ArticleOrder>(sql)).ToList();
            }
        }

        public async Task OverrideFeed(int articleOrderId, int? overrideFeedId)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"update [dbo].[ArticleOrder] set OverrideFeedId = @OverrideFeedId where Id = @Id";
                dbConnection.Open();
                await dbConnection.ExecuteScalarAsync(query, new { Id = articleOrderId, OverrideFeedId = overrideFeedId });
            }
        }
    }
}
