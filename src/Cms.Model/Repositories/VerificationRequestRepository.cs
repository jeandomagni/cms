﻿using Cms.Model.Enum;
using Cms.Model.Models;
using Cms.Model.Services;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Cms.Model.Repositories
{
    public interface IVerificationRequestRepository
    {
        Task<VerificationRequest> Get(string code, string userLogin);
        Task<VerificationRequest> Add(VerificationRequest verificationRequest);
        Task<VerificationRequest> Update(VerificationRequest verificationRequest);
        Task<int> DeleteForLogin(string userLogin);
        Task<int> Delete(int id);
    }

    public class VerificationRequestRepository : IVerificationRequestRepository
    {
        private readonly IConnectionStringService _connectionStringService;

        public VerificationRequestRepository(IConnectionStringService connectionStringService)
        {
            _connectionStringService = connectionStringService;
        }

        public IDbConnection Connection =>
            new SqlConnection(_connectionStringService.GetVendorConnectionString(Repository.CoreCmsDb));


        public async Task<VerificationRequest> Get(string code, string userLogin)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"select * from [dbo].[VerificationRequest] 
                               where code = @Code and CreatedBy = @UserLogin";

                dbConnection.Open();
                return
                    (await dbConnection.QueryAsync<VerificationRequest>(
                        query, new { Code = code, UserLogin = userLogin })).FirstOrDefault();
            }
        }

        public async Task<VerificationRequest> Add(VerificationRequest verificationRequest)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"insert into [dbo].[VerificationRequest] (
                                        Code,
                                        CreatedBy, 
                                        CreatedDate)
                                    values (
	                                    @Code,
	                                    @CreatedBy,
                                        sysdatetimeoffset())

                                    select * from [dbo].[VerificationRequest] where Id = cast(scope_identity() as int)";
                dbConnection.Open();
                return
                    (await dbConnection.QueryAsync<VerificationRequest>(query, verificationRequest)).FirstOrDefault();
            }
        }

        public async Task<VerificationRequest> Update(VerificationRequest verificationRequest)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"update [dbo].[VerificationRequest] 
                                    set  Verified = @Verified,
                                         ClientId = @ClientId
                                    where Id = @Id
                                    select * from [dbo].[VerificationRequest] where Id = @Id";
                dbConnection.Open();
                return
                    (await dbConnection.QueryAsync<VerificationRequest>(query, verificationRequest)).FirstOrDefault();
            }
        }

        public async Task<int> Delete(int id)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"delete from [dbo].[VerificationRequest] where Id = @Id";
                return
                    await dbConnection.ExecuteAsync(
                        query, new { Id = id });
            }
        }

        public async Task<int> DeleteForLogin(string userLogin)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"delete from [dbo].[VerificationRequest] where CreatedBy = @UserLogin";
                return
                    await dbConnection.ExecuteAsync(
                        query, new { UserLogin = userLogin });
            }
        }
    }
}
