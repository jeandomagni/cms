﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Cms.Model.Models.General;
using Dapper;

namespace Cms.Model.Repositories.Helpers
{
    public static class AuditLogRepositoryHelper
    {
        public static async Task AddAuditLogEntryAsync(this IDbConnection dbConnection, DbAuditLog auditLog)
        {
            const string query = @"insert into [dbo].[AuditLog] (	
                                      UserId  
                                    , EntityId 
                                    , EntityCode    
                                    , ActionCode  
                                    , Comment
                                    , Serialized
                                    , CreatedDate
                                )  
                                values (
                                      @UserId  
                                    , @EntityId 
                                    , @EntityCode    
                                    , @ActionCode   
                                    , @Comment 
                                    , @Serialized
                                    , sysdatetimeoffset())";

            await dbConnection.ExecuteAsync(query, auditLog);

        }

        public static  async Task AddManyAuditLogEntryAsync(this IDbConnection dbConnection, IEnumerable<DbAuditLog> auditLogs)
        {
            await dbConnection.ExecuteAsync(@"
                insert [dbo].[AuditLog] (	
                                      UserId  
                                    , EntityId 
                                    , EntityCode    
                                    , ActionCode  
                                    , Comment
                                    , Serialized
                                    , CreatedDate
                                )  
                                values (
                                      @UserId  
                                    , @EntityId 
                                    , @EntityCode    
                                    , @ActionCode   
                                    , @Comment 
                                    , @Serialized
                                    , sysdatetimeoffset()
                )", auditLogs);
        }

    }
}