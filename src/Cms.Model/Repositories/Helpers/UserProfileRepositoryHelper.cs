﻿using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Cms.Model.Models.User;
using Dapper;

namespace Cms.Model.Repositories.Helpers
{
    public static class UserProfileRepositoryHelper {
        public static async Task<UserProfile> AddProfileAsync(this IDbConnection dbConnection, UserProfile userProfile)
        {
            const string query = @"
                                insert into [dbo].[UserProfile] (	                         
                                    UserId, 
                                    BaseUriCulture,
                                    BaseUriCountry,
                                    DefaultSite,
                                    OpenContentInNewTab,
                                    Topics,
                                    PinnedTopic,
                                    CreatedDate,
                                    CreatedBy)
                                values (
	                                @UserId,
	                                @BaseUriCulture,
	                                @BaseUriCountry,
                                    @DefaultSite,
                                    @OpenContentInNewTab,
                                    @Topics,
                                    @PinnedTopic,
	                                sysdatetimeoffset(),
	                                @CreatedBy)

                                select * from [dbo].[UserProfile] where Id = cast(scope_identity() as int)";

            return (await dbConnection.QueryAsync<UserProfile>(query, userProfile)).FirstOrDefault();
        }

        public static async Task<UserProfile> UpdateProfileAsync(this IDbConnection dbConnection, UserProfile userProfile)
        {
            const string query = @"update [dbo].[UserProfile] set
                                    BaseUriCulture       = @BaseUriCulture,
                                    BaseUriCountry       = @BaseUriCountry,
                                    DefaultSite          = @DefaultSite,
                                    OpenContentInNewTab  = @OpenContentInNewTab,
                                    Topics               = @Topics,
                                    PinnedTopic          = @PinnedTopic,
                                    TwoFactorAuthEnabled = @TwoFactorAuthEnabled,
                                    ModifiedDate         = sysdatetimeoffset(),
                                    ModifiedBy           = @ModifiedBy,
                                    PhoneNumber          = @PhoneNumber,
                                    PhoneNumberVerified  = @PhoneNumberVerified

                                    where Id = @Id
                             select * from [dbo].[UserProfile] where Id = @Id";

            return
                (await dbConnection.QueryAsync<UserProfile>(query, userProfile)).FirstOrDefault();
        }
    }
}