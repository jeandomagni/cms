﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Threading.Tasks;
using Cms.Model.Dto.UserManagement;
using Cms.Model.Helpers;
using Cms.Model.Models.User;
using Cms.Model.Models.UserManagement;
using SqlKata;
using SqlKata.Compilers;
using SqlKata.Execution;
using User = Cms.Model.Models.UserManagement.User;

namespace Cms.Model.Repositories.Helpers
{
    public static class UserManagementRepositoryHelper
    {

        public static async Task<UserMembership> GetUserMembershipByUserIdAsync(this IDbConnection dbConnection,
            Guid userId)
        {
            var qf = new QueryFactory(dbConnection, new SqlServerCompiler());
            qf.Logger = result => Debug.WriteLine(result);
            var query = qf.FromQuery(GetUserMembershipQuery())
                .Where("Users.UserId", userId.ToString());

            var ret = await qf.FirstOrDefaultAsync<UserMembership>(query);
            return ret;
        }

        public static async Task<UserMembership> GetUserMembershipByEmailAsync(this IDbConnection dbConnection, string email)
        {
            var qf = new QueryFactory(dbConnection, new SqlServerCompiler());
            qf.Logger = result => Debug.WriteLine(result);
            var query = qf.FromQuery(GetUserMembershipQuery())
                .Where("Users.Email", email);
            var ret = await qf.FirstOrDefaultAsync<UserMembership>(query);
            return ret;
        }

        public static async Task<Membership> GetUserMembershipByIdAsync(this IDbConnection dbConnection,
            int membershipId)
        {
            var qf = new QueryFactory(dbConnection, new SqlServerCompiler());
            qf.Logger = result => Debug.WriteLine(result);
            var query = qf.Query("dbo.Membership")
                .Where("mId", membershipId);

            var ret = await qf.FirstOrDefaultAsync<Membership>(query);
            return ret;
        }

        public static async Task<UserMembership> GetUserMembershipByUserNameAsync(this IDbConnection dbConnection,
            string userName)
        {
            var qf = new QueryFactory(dbConnection, new SqlServerCompiler());
            qf.Logger = result => Debug.WriteLine(result);
            var query = qf.FromQuery(GetUserMembershipQuery())
                .Where("Users.UserName", userName);

            var ret = await qf.FirstOrDefaultAsync<UserMembership>(query);
            return ret;
        }

        private static Query GetUserMembershipQuery()
        {
            var query = new Query("dbo.Users")
                .Join("dbo.Membership", "Users.UserId", "Membership.UserId")
                .Select("Users.{UserId, BBBId, FirstName, LastName, UserName, Email, " +
                        "Password, PasswordSalt, PasswordQuestion, PasswordAnswer}",
                    "Membership.{mId, SiteId, Status, LastLoginDate, LastPasswordChangedDate, " +
                    "LastLockoutDate, FailedPasswordAttemptCount, FailedPasswordAttemptStart, Comment}");
            return query;
        }

        private static Query GetUserMembershipRolesQuery()
        {
            var query = new Query().From("dbo.Membership as m")
                .Join("dbo.Users as u", "u.UserId", "m.UserId")
                .Join("dbo.UsersInRoles as ur", "ur.UserId", "u.UserId")
                .Join("dbo.Roles as r", "r.RoleId", "ur.RoleId")
                .Join("dbo.BbbInfo as b", "b.LegacyId", "m.SiteId")
                .Select("u.UserId", "r.RoleId", "b.LegacySiteId as SiteId", "b.LegacyId as LegacyBBBId", "u.Email", "r.RoleName", "b.Name as BBBName");
            return query;
        }


        public static async Task<User> GetUserByIdAsync(this IDbConnection dbConnection, Guid userId)
        {
            var qf = new QueryFactory(dbConnection, new SqlServerCompiler())
            {
                Logger = result => Debug.WriteLine(result)
            };
            var query = qf.Query("dbo.Users")
                .Where("UserId", userId.ToString());

            var ret = await qf.FirstOrDefaultAsync<User>(query);
            return ret;
        }

        public static async Task<IEnumerable<UserRole>> GetUserRolesByEmailAsync(this IDbConnection dbConnection, string email)
        {
            var qf = new QueryFactory(dbConnection, new SqlServerCompiler())
            {
                Logger = result => Debug.WriteLine(result)
            };
            var query = GetUserMembershipRolesQuery()
                .Where("u.email", email);

            var ret = await qf.GetAsync<UserRole>(query);
            return ret;
        }
        public static async Task<IEnumerable<UserRole>> GetUserRolesByIdAsync(this IDbConnection dbConnection, Guid userId)
        {
            var qf = new QueryFactory(dbConnection, new SqlServerCompiler())
            {
                Logger = result => Debug.WriteLine(result)
            };
            var query = GetUserMembershipRolesQuery()
                .Where("u.UserId", userId);

            var ret = await qf.GetAsync<UserRole>(query);
            return ret;
        }


        public static async Task<User> GetUserByEmailAsync(this IDbConnection dbConnection, string email)
        {
            var qf = new QueryFactory(dbConnection, new SqlServerCompiler());
            qf.Logger = result => Debug.WriteLine(result);
            var query = qf.Query("dbo.Users")
                .Where("Email", email);

            var ret = await qf.FirstOrDefaultAsync<User>(query);
            return ret;
        }

        public static async Task<int> UpdateUserStatusAsync(this IDbConnection dbConnection, Guid userId,
            string status)
        {
            var user = await dbConnection.GetUserByIdAsync(userId);
            if (user == null)
            {
                //TODO 
                return -1;
            }

            var qf = new QueryFactory(dbConnection, new SqlServerCompiler());
            qf.Logger = q => Debug.WriteLine(q);
            var result = await qf
                .Query("dbo.Membership")
                .Where("UserId", user.UserId)
                .UpdateAsync(new { Status = status });
            return result;
        }

        public static async Task<int> UpdateUserSiteIdAsync(this IDbConnection dbConnection, Guid userId,
            string siteId)
        {
            var user = await dbConnection.GetUserByIdAsync(userId);
            if (user == null)
            {
                //TODO 
                return -1;
            }

            var qf = new QueryFactory(dbConnection, new SqlServerCompiler());
            qf.Logger = q => Debug.WriteLine(q);
            var result = await qf
                .Query("dbo.Membership")
                .Where("UserId", user.UserId)
                .UpdateAsync(new { SiteId = siteId});
            return result;
        }


        public static async Task UpdateUserRolesAsync(this IDbConnection dbConnection, Guid userId,
            List<Guid> rolesGuids)
        {

            var qf = new QueryFactory(dbConnection, new SqlServerCompiler());
            qf.Logger = q => Debug.WriteLine(q);
            var result = await qf
                .Query("dbo.UsersInRoles")
                .Where("UserId", userId)
                .DeleteAsync();
            foreach (var rolesGuid in rolesGuids)
            {
                await qf
                    .Query("dbo.UsersInRoles")
                    .Where("UserId", userId)
                    .InsertAsync( new
                    {
                         UserId = userId,
                         RoleId = rolesGuid
                    });

            }
        }

        public static async Task AddUserRolesAsync(this IDbConnection dbConnection, Guid userId,
            List<Guid> rolesGuids)
        {
            var qf = new QueryFactory(dbConnection, new SqlServerCompiler());
            qf.Logger = q => Debug.WriteLine(q);
            foreach (var rolesGuid in rolesGuids)
            {
                await qf
                    .Query("dbo.UsersInRoles")
                    .InsertAsync(new
                    {
                        UserId = userId,
                        RoleId = rolesGuid
                    });
            }
        }

        public static async Task DeleteUserRolesAsync(this IDbConnection dbConnection, string userId )
        {
            var qf = new QueryFactory(dbConnection, new SqlServerCompiler());
            qf.Logger = q => Debug.WriteLine(q);
            var result = await qf
                .Query("dbo.UsersInRoles")
                .Where("UserId", userId)
                .DeleteAsync();
        }

        public static async Task DeleteMembershipAsync(this IDbConnection dbConnection, string userId)
        {
            var qf = new QueryFactory(dbConnection, new SqlServerCompiler());
            qf.Logger = q => Debug.WriteLine(q);
            var result = await qf
                .Query("dbo.Membership")
                .Where("UserId", userId)
                .DeleteAsync();
        }

        public static async Task UpdateUserProfileDefaultSiteIdAsync(this IDbConnection dbConnection, Guid userId,
            string siteId)
        {

            var qf = new QueryFactory(dbConnection, new SqlServerCompiler());
            qf.Logger = q => Debug.WriteLine(q);
            var result = await qf
                .Query("dbo.UserProfile")
                .Where("UserId", Convert.ToString(userId))
                .UpdateAsync( new
                {
                    DefaultSite = siteId,


                });
        }

        public static async Task DeleteUserProfileAsync(this IDbConnection dbConnection, string userId)
        {

            var qf = new QueryFactory(dbConnection, new SqlServerCompiler());
            qf.Logger = q => Debug.WriteLine(q);
            var result = await qf
                .Query("dbo.UserProfile")
                .Where("UserId", userId)
                .DeleteAsync();
        }


        public static async Task<PaginationResult<UserMembership>> GetUserMembershipListAsync(this IDbConnection dbConnection,
            UserMembershipListDto listOptions)
        {
            var qf = new QueryFactory(dbConnection, new SqlServerCompiler());
            qf.Logger = result => Debug.WriteLine(result);
            var query = GetUserMembershipQuery();
            var qbHelper = new QueryBuilderHelper();

            //get additional filters
            var filter = qbHelper.ConvertToDictionary(listOptions, null);

            //add filter to query
            query = qbHelper.BuildQuery<UserMembershipListDto>(query, filter)
                .OrderByRaw($"{listOptions.SortColumn} {listOptions.Direction}");
            var res = await qf.PaginateAsync<UserMembership>(query, listOptions.Page.GetValueOrDefault(),
                listOptions.PageSize.GetValueOrDefault());
            return res;
        }


        public static async Task<Membership> AddMembershipAsync(this IDbConnection dbConnection, User usr)
        {
            var qf = new QueryFactory(dbConnection, new SqlServerCompiler());
            qf.Logger = result => Debug.WriteLine(result);
            var membership = new Membership
            {
                UserId = usr.UserId,
                Status = "RESET", 
                SiteId = usr.BBBId
            };
            var dctMembership = membership.ToReadOnlyDictionary(new List<string> { "mId" });
            var membershipId = await qf.Query("dbo.MemberShip").InsertGetIdAsync<int>(dctMembership);
            membership.mId = membershipId;
            return membership;
        }

        public static async Task<int> UpdateMembershipAsync(this IDbConnection dbConnection, Membership membership)
        {
            var existingMembership = await dbConnection.GetUserMembershipByIdAsync(membership.mId);
            var qf = new QueryFactory(dbConnection, new SqlServerCompiler());
            qf.Logger = result => Debug.WriteLine(result);
            existingMembership.Comment = membership.Comment;
            existingMembership.SiteId = membership.SiteId;
            existingMembership.Status = membership.Status;
            existingMembership.LastLoginDate = membership.LastLoginDate;
            existingMembership.LastPasswordChangedDate = membership.LastPasswordChangedDate;
            existingMembership.LastLockoutDate = membership.LastLockoutDate;
            existingMembership.FailedPasswordAttemptCount = membership.FailedPasswordAttemptCount;
            existingMembership.FailedPasswordAttemptStart = membership.FailedPasswordAttemptStart;
            var updated = await qf.Query("dbo.MemberShip").UpdateAsync(existingMembership);
            return updated;
        }

        public static async Task<User> AddUserAsync(this IDbConnection dbConnection, User usr)
        {

            var comp = new SqlServerCompiler();
            var qf = new QueryFactory(dbConnection, comp);
            qf.Logger = result => Debug.WriteLine(result);
            var identifier = Guid.NewGuid();
            usr.UserId = identifier;
            var dctUser = usr.ToReadOnlyDictionary(new List<string> { });
            await qf.Query("dbo.Users").InsertAsync(dctUser);
            return usr;
        }

        public static async Task<int> UpdateUserAsync(this IDbConnection dbConnection, User usr)
        {
            var qf = new QueryFactory(dbConnection, new SqlServerCompiler());
            qf.Logger = result => Debug.WriteLine(result);
            var ret = await qf.Query("dbo.Users")
                .Where("UserId", usr.UserId.ToString())
                .UpdateAsync(new
                {
                    UserName = usr.UserName,
                    BBBId = usr.BBBId,
                    Email = usr.Email,
                    FirstName = usr.FirstName,
                    LastName = usr.LastName
                });
            return ret;
        }

        public static async Task<int> DeleteUserAsync(this IDbConnection dbConnection, string userId)
        {
            var qf = new QueryFactory(dbConnection, new SqlServerCompiler());
            qf.Logger = result => Debug.WriteLine(result);
            var ret = await qf.Query("dbo.Users")
                .Where("UserId", userId)
                .DeleteAsync();
            return ret;
        }


        public static async Task<int> UpdateUserPasswordAsync(this IDbConnection dbConnection, User usr)
        {
            var qf = new QueryFactory(dbConnection, new SqlServerCompiler());
            qf.Logger = result => Debug.WriteLine(result);
            var ret = await qf.Query("dbo.Users")
                .Where("UserId", usr.UserId.ToString())
                .UpdateAsync(new
                {
                    Password = usr.Password,
                    PasswordSalt = usr.PasswordSalt
                });
            return ret;
        }

        public static async Task<IEnumerable<Roles>> GetRolesAsync(this IDbConnection dbConnection)
        {
            var qf = new QueryFactory(dbConnection, new SqlServerCompiler());
            qf.Logger = result => Debug.WriteLine(result);
            var qry = qf.Query().From("dbo.Roles");
            var ret = await qf.GetAsync<Roles>(qry);
            return ret;
        }

        public static async Task<int> UpdateLastLoginDateAsync(this IDbConnection dbConnection, Guid userId,
            DateTime loginDate)
        {
            var qf = new QueryFactory(dbConnection, new SqlServerCompiler());
            qf.Logger = result => Debug.WriteLine(result);
            var updated = await qf.Query("dbo.MemberShip")
                .Where("UserId", userId)
                .UpdateAsync(new
                {
                    LastLoginDate = loginDate
                });
            return updated;
        }

        public static async Task<int> UpdateLastPasswordChangeDateAsync(this IDbConnection dbConnection, Guid userId,
            DateTime date)
        {
            var qf = new QueryFactory(dbConnection, new SqlServerCompiler());
            qf.Logger = result => Debug.WriteLine(result);
            var updated = await qf.Query("dbo.MemberShip")
                .Where("UserId", userId)
                .UpdateAsync(new
                {
                    LastPasswordChangedDate = date
                });
            return updated;
        }

        public static async Task<int> ResetLoginAttemptCountAsync(this IDbConnection dbConnection, Guid userId
            )
        {
            var qf = new QueryFactory(dbConnection, new SqlServerCompiler());
            qf.Logger = result => Debug.WriteLine(result);
            var updated = await qf.Query("dbo.MemberShip")
                .Where("UserId", userId)
                .UpdateAsync(new
                {
                    FailedPasswordAttemptCount = 0
                });
            return updated;
        }

        public static async Task<int> UpdateLoginAttemptCountAsync(this IDbConnection dbConnection, Guid userId, int nextNumber)
        {
            var qf = new QueryFactory(dbConnection, new SqlServerCompiler());
            qf.Logger = result => Debug.WriteLine(result);
            var updated = await qf.Query("dbo.MemberShip")
                .Where("UserId", userId)
                .UpdateAsync(new
                {
                    FailedPasswordAttemptCount = nextNumber
                });
            return updated;

        }

    }
}

