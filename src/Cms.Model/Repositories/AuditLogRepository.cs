﻿using Cms.Model.Enum;
using Cms.Model.Models.General;
using Cms.Model.Services;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Cms.Model.Repositories.Helpers;

namespace Cms.Model.Repositories
{
    public interface IAuditLogRepository
    {
        Task Add(string userId, int entityId, string entityCode,
            string actionCode, string comment, string serialized);
        Task Add(string userId, int entityId, string entityCode, string actionCode);
        Task AddMany(IEnumerable<DbAuditLog> auditLogs);
        Task<IEnumerable<DbAuditLog>> GetLatestForEntity(int entityId, string entityCode, int page, int pageSize);
    }

    public class AuditLogRepository : IAuditLogRepository
    {
        private readonly IConnectionStringService _connectionStringService;

        public AuditLogRepository(
            IConnectionStringService connectionStringService)
        {
            _connectionStringService = connectionStringService;
        }

        public IDbConnection Connection =>
            new SqlConnection(
                _connectionStringService.GetVendorConnectionString(Repository.CoreCmsDb));

        public async Task Add(string userId, int entityId, string entityCode, string actionCode) {
            await Add(userId, entityId, entityCode, actionCode, null, null);
        }

        public async Task Add
            (string userId, int entityId, string entityCode, 
            string actionCode, string comment, string serialized) {
            var auditLog = new DbAuditLog {
                UserId = userId,
                EntityCode = entityCode,
                EntityId = entityId,
                ActionCode = actionCode,
                Comment = comment,
                Serialized = serialized
            };

            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                await dbConnection.AddAuditLogEntryAsync(auditLog);
            }
        }

        public async Task AddMany(IEnumerable<DbAuditLog> auditLogs) {

            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                await dbConnection.AddManyAuditLogEntryAsync(auditLogs);
            }
        }

        public async Task<IEnumerable<DbAuditLog>> GetLatestForEntity(
            int entityId, string entityCode, int page, int pageSize) {
            var query = $@"select *
                          from dbo.auditlog
                          where EntityId = @EntityId and EntityCode = @EntityCode
                          order by id desc 
                          offset @PageSize * (@PageNumber - 1) row
                          fetch next @PageSize rows only";

            using (var dbConnection = Connection)
            {

                dbConnection.Open();

                return await dbConnection.QueryAsync<DbAuditLog>(
                    query, new {
                        EntityId = entityId,
                        EntityCode = entityCode,
                        PageNumber = page,
                        PageSize = pageSize > 0 ? pageSize : 10
                    }
                );
            }
        }
    }
}