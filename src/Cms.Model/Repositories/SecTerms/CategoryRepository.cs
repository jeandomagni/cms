﻿using Cms.Model.Enum;
using Cms.Model.Models.SecTerm.Db;
using Cms.Model.Services;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Cms.Model.Repositories.SecTerms
{
    public interface ICategoryRepository
    {
        Task<IEnumerable<string>> GetAllHighRiskAndMonitoredTobIdsAsync();
        Task<DbCategory> Update(DbCategory category);
    }

    public class CategoryRepository : ICategoryRepository
    {
        private readonly IConnectionStringService _connectionStringService;

        public CategoryRepository(IConnectionStringService connectionStringService)
        {
            _connectionStringService = connectionStringService;
        }

        public IDbConnection Connection =>
            new SqlConnection(
                _connectionStringService.GetVendorConnectionString(
                    Repository.CoreDb));

        public async Task<DbCategory> Update(DbCategory category)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"update [dbo].[refCategory] set 
                            CategoryName    = @CategoryName,
                            CategoryURL     = [dbo].[SetSEOFriendlyURLs](@CategoryName),
                            Spanish         = @Spanish
                            where TOBId = @TOBId

                            select * from [dbo].[refCategory] where TOBId = @TOBId";
                dbConnection.Open();
                return
                    (await dbConnection.QueryAsync<DbCategory>(query, category)).FirstOrDefault();
            }
        }

        public async Task<IEnumerable<string>> GetAllHighRiskAndMonitoredTobIdsAsync()
        {
            var query = @"SELECT TOBId
                            FROM [dbo].[refCategory]
                            WHERE Tier IN ('monitored','High Risk')";

            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                return await dbConnection.QueryAsync<string>(query);
            }
        }
    }
}
