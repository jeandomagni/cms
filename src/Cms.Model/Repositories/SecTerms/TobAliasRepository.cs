﻿using Cms.Model.Enum;
using Cms.Model.Models.SecTerm;
using Cms.Model.Models.SecTerm.Db;
using Cms.Model.Services;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Cms.Model.Repositories.SecTerms
{
    public interface ITobAliasRepository
    {
        Task<DbTobAlias> Add(DbTobAlias coreTobAlias);
        Task<DbTobAlias> Update(DbTobAlias coreTobAlias);
        Task<int> DeleteByAliasId(int aliasId);
        Task<TobAlias> GetByName(string name, string spanishName);
    }

    public class TobAliasRepository : ITobAliasRepository
    {
        private readonly IConnectionStringService _connectionStringService;

        public TobAliasRepository(IConnectionStringService connectionStringService)
        {
            _connectionStringService = connectionStringService;
        }

        public IDbConnection Connection =>
            new SqlConnection(
                _connectionStringService.GetVendorConnectionString(
                    Repository.CoreDb));
  
        public async Task<DbTobAlias> Add(DbTobAlias coreTobAlias)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"insert into [dbo].[refTOBAlias] (
                                TOBId,
                                Description,
                                TOBAlias,
                                URISegment,
                                Spanish,
                                CreatorUserId,
                                DateTimeCreated)
                            values (
                                @TobId,
                                @Description,
                                @TobAlias,
                                @UrlSegment,
                                @Spanish,
                                @CreatorUserId,
                                @DateTimeCreated)

                             select * from [dbo].[refTOBAlias] where AliasId = cast(scope_identity() as int)";
                dbConnection.Open();
                return
                    (await dbConnection.QueryAsync<DbTobAlias>(query, coreTobAlias)).FirstOrDefault();
            }
        }

        public async Task<DbTobAlias> Update(DbTobAlias coreTobAlias)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"update [dbo].[refTOBAlias] set 
                            TOBId           = @TobId,
                            Description     = @Description,
                            TOBAlias        = @TobAlias,
                            URISegment      = @UrlSegment,
                            Spanish         = @Spanish
                            where AliasId = @AliasId

                            select * from [dbo].[refTOBAlias] where AliasId = @AliasId";
                dbConnection.Open();
                return
                    (await dbConnection.QueryAsync<DbTobAlias>(query, coreTobAlias)).FirstOrDefault();
            }
        }

        public async Task<int> DeleteByAliasId(int aliasId)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"DELETE FROM [dbo].[refTobAlias] WHERE AliasId = @AliasId";
                dbConnection.Open();
                return
                     await dbConnection.ExecuteAsync(query, new { AliasId = aliasId });
            }
        }

        public async Task<TobAlias> GetByName(string name, string spanishName)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"SELECT TOP(1) *
                    FROM [Core].[dbo].[refTOBAlias]
                    WHERE [TOBAlias] = @TOBAlias OR [Spanish] = @Spanish";
                dbConnection.Open();
                return await dbConnection.QueryFirstOrDefaultAsync<TobAlias>(
                        query, new { TOBAlias = name, Spanish = spanishName });
            }
        }
    }
}