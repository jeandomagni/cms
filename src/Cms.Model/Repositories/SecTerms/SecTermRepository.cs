﻿using Cms.Model.Enum;
using Cms.Model.Helpers;
using Cms.Model.Models.Query.KendoGrid;
using Cms.Model.Models.SecTerm.Db;
using Cms.Model.Services;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Cms.Model.Repositories.SecTerms
{
    public interface ISecTermRepository
    {
        Task<IEnumerable<DbSecTerm>> Paginate(GridOptions options);
        Task<DbSecTerm> GetByTobId(string tobId);
    }

    public class SecTermRepository : ISecTermRepository
    {
        private readonly IConnectionStringService _connectionStringService;

        public SecTermRepository(IConnectionStringService connectionStringService)
        {
            _connectionStringService = connectionStringService;
        }

        public IDbConnection Connection =>
            new SqlConnection(
                _connectionStringService.GetVendorConnectionString(
                    Repository.CoreDb));
        
        public async Task<IEnumerable<DbSecTerm>> Paginate(GridOptions options)
        {
            if (options?.data == null)
                return null;

            using (var dbConnection = Connection)
            {
                try
                {
                    var pars = new DynamicParameters();
                    var sortField = "name";
                    var sortDirection = "asc";

                    if (options.data.sort != null && options.data.sort.Any())
                    {
                        sortField = options.data.sort[0].field;
                        sortDirection = options.data.sort[0].dir;
                    }

                    var filters = string.Empty;
                    if (options.data.filter?.filters != null)
                    {
                        filters = SqlHelpers.GetXml(options.data.filter.filters);
                    }

                    if (string.IsNullOrEmpty(filters))
                    {
                        filters = null;
                    }

                    pars.Add("@FilterList", filters);
                    pars.Add("@PageNumber", options.data.page);
                    pars.Add("@PageSize", options.data.pageSize);
                    pars.Add("@SortField", sortField);
                    pars.Add("@SortDir", sortDirection);

                    dbConnection.Open();

                    return
                        await dbConnection.QueryAsync<DbSecTerm>(
                            "PaginateCategories", pars, commandType: CommandType.StoredProcedure);
                }
                finally
                {
                    if (dbConnection.State == ConnectionState.Open)
                    {
                        dbConnection.Close();
                    }
                }
            }
        }
        
        public async Task<DbSecTerm> GetByTobId(string tobId)
        {
            const string query = @"SELECT [CategoryId]
                  ,[CategoryCode]
                  ,[TobId]
	              ,[Name]
                  ,[UrlSegment]
                  ,[SpanishName]
                  ,[SpanishUrlSegment]
	              ,[MetadataJson]
                  ,[MetadataBusinessJson]
				  ,[ModifiedDate]
                  ,[IsActive]
				  ,[IsEditable]
	              ,[AliasesJson]
              FROM [Core].[dbo].[vwSearchEngineCategories]
              WHERE [TobId] = @TOBId";

            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                return (await dbConnection.QueryAsync<DbSecTerm>(query, new { TOBId = tobId })).FirstOrDefault();
            }
        }
    }
}
