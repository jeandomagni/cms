﻿using Cms.Model.Enum;
using Cms.Model.Models.SecTerm.Db;
using Cms.Model.Services;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Cms.Model.Repositories.SecTerms.V2
{
    public class SecMetadataRepository : ISecMetadataRepository
    {
        private readonly IConnectionStringService _connectionStringService;

        public SecMetadataRepository(IConnectionStringService connectionStringService)
        {
            _connectionStringService = connectionStringService;
        }

        public IDbConnection Connection =>
            new SqlConnection(
                _connectionStringService.GetVendorConnectionString(
                    Repository.ApiCoreDb));
        
        public async Task<DbSecMetadata> Update(DbSecMetadata metadata)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"update [dbo].[srcSECategoryTerms] set 
                            MetadataJson            = @MetadataJson,
                            MetadataBusinessJson    = @MetadataBusinessJson,
                            IsActive                = @IsActive,
                            ModifiedUser            = @ModifiedUser,
                            ModifiedDate            = @ModifiedDate
                            where TOBId = @TobId

                            select * from [dbo].[srcSECategoryTerms] where TOBId = @TobId";
                dbConnection.Open();
                return
                    (await dbConnection.QueryAsync<DbSecMetadata>(query, metadata)).FirstOrDefault();
            }
        }

        public async Task<DbSecMetadata> GetByTobId(string tobId)
        {
            const string query = @"SELECT [secTerms].[TOBId],
                  [secTerms].[MetadataJson],
                  [secTerms].[MetadataBusinessJson]
                  FROM [dbo].[srcSECategoryTerms] secTerms
                  WHERE [secTerms].[TOBId] = @TOBId";

            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                return (await dbConnection.QueryAsync<DbSecMetadata>(query, new { TOBId = tobId })).FirstOrDefault();
            }
        }
        
        public async Task<int> PublishByTobId(string tobId)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"update [dbo].[srcSECategoryTerms] set IsActive = 1 where TOBId = @TOBId";
                dbConnection.Open();
                return
                     await dbConnection.ExecuteAsync(query, new { TOBId = tobId });
            }
        }

        public async Task<int> UnpublishByTobId(string tobId)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"update [dbo].[srcSECategoryTerms] set IsActive = 0 where TOBId = @TOBId";
                dbConnection.Open();
                return
                     await dbConnection.ExecuteAsync(query, new { TOBId = tobId });
            }
        }

        public async Task<DbSecMetadata> Add(DbSecMetadata secMetadata) 
        {
            using (var dbConnection = Connection)
            {
                const string query = @"insert into [dbo].[srcSECategoryTerms] (	                         
                            TobId,
                            MetadataJson,
                            MetadataBusinessJson,
                            IsActive,
                            ModifiedUser,
                            ModifiedDate)
                        values (
	                        @TobId,
                            @MetadataJson,
                            @MetadataBusinessJson,
	                        @IsActive,
                            @ModifiedUser,
                            @ModifiedDate)
                            select * from [dbo].[srcSECategoryTerms] where SECId = cast(scope_identity() as int)";
                dbConnection.Open();
                return
                    (await dbConnection.QueryAsync<DbSecMetadata>(query, secMetadata)).FirstOrDefault();
            }
        }
    }
}
