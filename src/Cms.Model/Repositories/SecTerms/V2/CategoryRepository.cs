﻿using Cms.Model.Enum;
using Cms.Model.Models.SecTerm.Db;
using Cms.Model.Services;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Cms.Model.Repositories.SecTerms.V2
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly IConnectionStringService _connectionStringService;

        public CategoryRepository(IConnectionStringService connectionStringService)
        {
            _connectionStringService = connectionStringService;
        }

        public IDbConnection Connection =>
            new SqlConnection(
                _connectionStringService.GetVendorConnectionString(
                    Repository.CoreDb));

        public async Task<DbCategory> Update(DbCategory category)
        {
            using (var dbConnection = Connection)
            {
                const string query = @"update [dbo].[srcCategory] set 
                            CategoryName    = @CategoryName,
                            CategoryURL     = [dbo].[SetSEOFriendlyURLs](@CategoryName),
                            Spanish         = @Spanish
                            where TOBId = @TOBId

                            select * from [dbo].[srcCategory] where TOBId = @TOBId";
                dbConnection.Open();
                return
                    (await dbConnection.QueryAsync<DbCategory>(query, category)).FirstOrDefault();
            }
        }

        public async Task<IEnumerable<string>> GetAllHighRiskAndMonitoredTobIdsAsync()
        {
            using (var dbConnection = Connection)
            {
                const string query = @"SELECT TOBId
                            FROM [dbo].[refCategory]
                            WHERE Tier IN ('monitored','High Risk')";

                dbConnection.Open();
                return await dbConnection.QueryAsync<string>(query);
            }
        }
    }
}
