﻿

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PaginateContent]
(
		 @UserId varchar(250)
		,@PageNumber int = 1
		,@PageSize int = 10
		,@SortField varchar(250)
		,@SortDir varchar(250) 
		,@filterList xml
)
	as
	begin

	set nocount on 
	set transaction isolation level read  uncommitted

	-- start building dynamic query
	declare @FilterTable dbo.FilterList

	insert into @FilterTable(Field, Op, Value)
	select
		Field = XCol.value('(field)[1]','varchar(250)'),
		Op = XCol.value('(operator)[1]','varchar(250)'),
		Value = XCol.value('(value)[1]','varchar(250)')
	from 
		@FilterList.nodes('/root/Row') AS XTbl(XCol)
	  

	declare @sqlCommand nvarchar(max)
	declare @cols varchar(max)= ''
	declare @cols2 varchar(max) = ''

	declare @tableName varchar(250) 
	set @tableName = '[dbo].[Content]'

	if (@sortField is null) 
	begin
		-- default sort
		set @SortField = (select top 1 name from sys.columns where object_id = object_id(@tableName))
		set @SortDir = 'desc'
	end

	select @cols = 
		coalesce (case when @cols = '' then name else @cols + ',' + name end, '')
				  from sys.columns where object_id = object_id(@tableName)

	select @cols2 = 
		coalesce (case when @cols2 = '' then name else @cols2 + ',cte.' + name end, '')
				  from sys.columns where object_id = object_id(@tableName)

	set @Cols2 = concat('cte.',@cols2)

	set @sqlCommand = 
		N'declare 
		@lPageNbr int,
		@lPageSize int,
		@lFirstRec int,
		@lLastRec int,
		@lTotalRows int

		set @lPageNbr = ' + convert(varchar, @PageNumber) + ' 
		set @lPageSize = ' + convert(varchar, @pageSize) + ' 

		set @lFirstRec = (@lPageNbr - 1) * @lPageSize
		set @lLastRec = (@lPageNbr * @lPageSize + 1)
		set @lTotalRows = @lFirstRec - @lLastRec + 1

		; with sorted
		as (
		select row_number() over (order by cte.' 
					+ quotename(@sortField) 
					+ case when @sortDir = 'asc' then ' asc'  
						   when	@sortDir = 'desc' then ' desc' 
					  end 
		+' ) as rownum,'
		+ @Cols2 + ', count(*) over () as TotalCount'

		set @sqlCommand += ' from ' + @tableName + ' cte where cte.deleted = 0  and 1 = 1 '

		if exists(select top 1 field from @FilterTable) 
			begin
				set @sqlCommand += dbo.fnBuildPredicateStringFromFilters(@FilterTable)
			end

		set @sqlCommand += ')'
		set @sqlCommand += ' select 
			' + @cols + ', TotalCount ' +
		   N' from sorted as result
				where
				rownum > @lFirstRec
				and rownum < @lLastRec
				order by rownum asc'

		   -- exec and error handling

		   -- TODO: PARAMETIZE THIS OR SQL INJECTIONS
		   --  print @SqlCommand
			execute sp_executesql @SqlCommand

			-- uncomment for testing
			
			if @@error <> 0 goto ErrorHandler
			set nocount off
			return(0)

			ErrorHandler:
			return(@@error)
end
