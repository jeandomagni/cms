﻿using System;
using System.Collections.Generic;
using Cms.Domain.Contracts;
using Microsoft.Extensions.DependencyInjection;

namespace Cms.Web.Configuration
{
    public class ServiceLocator : IServiceLocator
    {
        private IServiceProvider _currentServiceProvider;

        public ServiceLocator(IServiceProvider currentServiceProvider)
        {
            _currentServiceProvider = currentServiceProvider;
        }


        public object Create(Type serviceType)
        {
            return _currentServiceProvider.GetService(serviceType);
        }

        public object TryToCreate(Type requestedType)
        {
            try
            {
                return Create(requestedType);
            }
            catch (System.Exception)
            {
                return null;
            }
        }


        public T Create<T>()
        {
            return _currentServiceProvider.GetService<T>();
        }

        public T TryToCreate<T>()
        {
            try
            {
                return Create<T>();
            }
            catch (System.Exception)
            {
                return default(T);
            }
        }

        public IEnumerable<T> CreateMany<T>()
        {
            return _currentServiceProvider.GetServices<T>();
        }

    }
}
