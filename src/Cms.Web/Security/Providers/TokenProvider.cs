﻿using Cms.Domain.Cache;
using Cms.Domain.Enum;
using Cms.Domain.Extensions;
using Cms.Domain.Handlers;
using Cms.Domain.Mappers;
using Cms.Domain.Services.Solr;
using Cms.Model.Enum;
using Cms.Model.Models.Config;
using Cms.Model.Models.User;
using Cms.Model.Repositories;
using Cms.Web.Model.Security;
using Cms.Web.Security.Interfaces;
using Cms.Web.Services;
using log4net;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Claims;
using System.Threading.Tasks;
using Cms.Model.Models.UserManagement;
using Cms.Web.Services.UserManagement;
using User = Cms.Model.Models.User.User;

namespace Cms.Web.Security.Providers
{
    public class TokenProvider : ITokenProvider
    {
        private readonly RequestDelegate _next;
        private readonly TokenProviderOptions _options;
        private readonly IUserRepository _userRepository;
        private readonly IAuthenticationHandler _authenticationHandler;
        private readonly IUserProfileRepository _userProfileRepository;
        private readonly IUserLocationRepository _userLocationRepository;
        private readonly ILegacyBbbSiteCache _bbbSiteCache;
        private readonly IAuditLogRepository _auditLogRepository;
        private readonly IVerificationRequestRepository _verificationRequestRepository;
        private readonly IVerificationClientRepository _verificationClientRepository;
        private readonly ISolrLocationService _solrLocationService;
        private readonly ILocationService _locationService;
        private readonly IUserManagementService _managementService;
        private readonly IUserProfileService _userProfileService;
        private readonly ConnectionStrings _config;

        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly string _cookieName = "Cms.AuthCookie";

        public TokenProvider(
            RequestDelegate next,
            IOptions<TokenProviderOptions> options,
            IAuthenticationHandler authenticationHandler,
            IUserProfileRepository userProfileRepository,
            IUserRepository userRepository,
            IUserLocationRepository userLocationRepository,
            ILegacyBbbSiteCache bbbSiteCache,
            IAuditLogRepository auditLogRepository,
            IVerificationRequestRepository verificationRequestRepository,
            IVerificationClientRepository verificationClientRepository,
            IOptions<ConnectionStrings> config,
            ISolrLocationService solrLocationService,
            ILocationService locationService,
            IUserManagementService managementService,

            IUserProfileService userProfileService)
        {
            _next = next;
            _options = options.Value;
            _config = config.Value;
            _authenticationHandler = authenticationHandler;
            _userProfileRepository = userProfileRepository;
            _userRepository = userRepository;
            _userLocationRepository = userLocationRepository;
            _bbbSiteCache = bbbSiteCache;
            _auditLogRepository = auditLogRepository;
            _verificationRequestRepository = verificationRequestRepository;
            _verificationClientRepository = verificationClientRepository;
            _solrLocationService = solrLocationService;
            _locationService = locationService;
            _managementService = managementService;
            _userProfileService = userProfileService;
        }

        public Task Invoke(HttpContext context)
        {
            if (!context.Request.Path.Equals(_options.Path, StringComparison.Ordinal))
                return _next(context);

            if (context.Request.Method.Equals("POST") && context.Request.HasFormContentType)
                return GenerateToken(context);

            context.Response.StatusCode = 400;
            return context.Response.WriteAsync("Bad request.");
        }

        private async Task GenerateToken(HttpContext context)
        {
            try
            {

                var username = context.Request.Form["username"];
                var password = context.Request.Form["password"];
                var clientId = context.Request.Form["clientId"];
                var userByEmail = await _managementService.GetUserByEmailAsync(username);

                if (userByEmail == null)
                {
                    context.Response.StatusCode = (int)HttpStatusCode.NotFound;
                    await context.Response.WriteAsync("Invalid username or password.");
                    return;
                }

                var userId = userByEmail.UserId.ToString().ToLower();
                var userProfile = await _userProfileService.GetForUser(userId);
                var twoFactorFlow = false;
                if (userProfile != null)
                {
                    if (userProfile.TwoFactorAuthEnabled)
                    {
                        var verificationClient = await _verificationClientRepository.Get(clientId, username.ToString());
                        var verificationRequest = await _verificationRequestRepository.Get(password.ToString(), username.ToString());
                        if (verificationClient != null)
                        {
                            twoFactorFlow = true;
                        }
                        else
                        {
                            if (verificationRequest != null
                                && verificationRequest.CreatedDate < DateTimeOffset.Now.AddDays(-1))
                            {
                                await _verificationRequestRepository.Delete(verificationRequest.Id);
                            }
                            else
                            {
                                if (verificationRequest?.Code != password)
                                {
                                    context.Response.StatusCode = 400;
                                    await context.Response.WriteAsync("Invalid username or password.");
                                    return;
                                }
                                twoFactorFlow = true;
                            }
                        }

                    }
                }


                (AuthorizationResultEnum, UserMembership) ret;
                try
                {

                    ret = await _managementService.ValidateCredentialsAsync(username, password, twoFactorFlow);
                    switch (ret.Item1)
                    {
                        case AuthorizationResultEnum.Success:
                            break;
                        case AuthorizationResultEnum.NotFound:
                            context.Response.StatusCode = (int)HttpStatusCode.NotFound;
                            await context.Response.WriteAsync("Invalid username or password.");
                            return;
                        case AuthorizationResultEnum.InvalidPassword:
                            context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                            await context.Response.WriteAsync("Invalid username or password.");
                            return;
                        case AuthorizationResultEnum.AccountLocked:
                            context.Response.StatusCode = (int)HttpStatusCode.Conflict;
                            await context.Response.WriteAsync("Account locked..Please contact system support.");
                            return;
                        case AuthorizationResultEnum.PasswordResetNeeded:
                            context.Response.StatusCode = (int)HttpStatusCode.TemporaryRedirect;
                            await context.Response.WriteAsync("Password change needed.");
                            return;
                        case AuthorizationResultEnum.AccountDisabled:
                        case AuthorizationResultEnum.AccountInactive:
                            context.Response.StatusCode = (int)HttpStatusCode.ExpectationFailed;
                            await context.Response.WriteAsync("Account disabled..Please contact system support.");
                            return;
                        case AuthorizationResultEnum.UnknownError:
                            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                            await context.Response.WriteAsync("An internal error occurred..Please contact system support.");
                            return;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }
                catch (Exception e)
                {
                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    await context.Response.WriteAsync("An internal error occurred..Please contact system support.");
                    return;
                }

                var now = DateTime.UtcNow;
                var user = ret.Item2;

                //var user = await _userRepository.GetByEmail(username);

                if (user == null)
                {
                    context.Response.StatusCode = 400;
                    await context.Response.WriteAsync("Invalid username or password.");
                    return;
                }


                var userRoles =
                    await _userRepository.GetRolesByEmail(
                        user.Email) ?? new List<UserRole>();

                var iat = now.ToString(CultureInfo.InvariantCulture);
                var roles = userRoles as UserRole[] ?? userRoles.ToArray();
                var userSites = await GetUserSites(roles);

                foreach (var item in userSites)
                {
                    item.CountryCode = await GetCountryCodeByState(item.State);
                }

                if (userProfile == null)
                {
                    userProfile = await CreateUserProfile(user, userSites);
                }


                var rolesString = string.Join(",", roles.Select(r => $"{r.RoleName}~{r.SiteId}").Distinct());

                var adminSites = userRoles.Where(
                        r => !string.IsNullOrWhiteSpace(r.RoleName) && r.RoleName.ToLower().Contains("admin")).Select(
                        r => r.SiteId).ToList().Distinct();


                var globalAdmin = roles.FirstOrDefault(c =>
                    c.RoleName.Equals("episerveradmin", StringComparison.OrdinalIgnoreCase)) != null;

                var localAdmin = roles
                    .Any(c => !c.RoleName.Equals("episerveradmin", StringComparison.OrdinalIgnoreCase)
                              && c.RoleName.ToLower().Contains("admin"));
                    


                var adminLegacySites = userSites != null ? string.Join(",", userSites.Where(
                    s => adminSites.Contains(s.LegacySiteID.ToString())).Select(r => r.LegacyBBBID)) : string.Empty;

                var claims = new[]  {
                    new Claim(JwtRegisteredClaimNames.Email, user.Email),
                    new Claim(JwtRegisteredClaimNames.UniqueName, user.UserId.ToString()),
                    new Claim(JwtRegisteredClaimNames.Actort, rolesString),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim(JwtRegisteredClaimNames.Iat, iat, ClaimValueTypes.Integer64),
                    new Claim(BbbCustomClaims.GlobalAdmin, globalAdmin.ToString()),
                    new Claim(BbbCustomClaims.LocalAdmin, localAdmin.ToString()),
                    new Claim(BbbCustomClaims.AdminSites, adminLegacySites),
                };

                var jwt = new JwtSecurityToken(
                    _options.Issuer,
                    _options.Audience,
                    claims,
                    now,
                    now.Add(_options.Expiration),
                    _options.SigningCredentials);

                var encodedJwt =
                    new JwtSecurityTokenHandler().WriteToken(jwt);

                var userLocations =
                    await _userLocationRepository.GetAllForUser(
                        userId);
                var siteCities = (await _solrLocationService.GetCitiesForBbb(userProfile.DefaultSite))
                    .Select(x => LocationMappers.MapToLocation(x))
                    .ToList();
                var siteStates = (await _solrLocationService.GetStatesForBbb(userProfile.DefaultSite))
                    .ToList();

                userProfile.DefaultSiteCity = siteCities.Count >= 1 ? siteCities.FirstOrDefault() : null;
                userProfile.DefaultSiteState = siteStates.Count >= 1 ? siteStates.FirstOrDefault() : null;

                var response = new
                {
                    appUser = new
                    {
                        role = roles.Select(role => role.RoleName),
                        sites = userSites,
                        adminSites = adminLegacySites?.Split(',', StringSplitOptions.RemoveEmptyEntries),
                        locations = userLocations,
                        userId = user.UserId.ToString(),
                        email = user.Email,
                        profile = userProfile,
                        iat,
                        topics = Topics.GetAll(), 
                        localAdmin = localAdmin,
                        globalAdmin = globalAdmin
                    },
                    access_token = encodedJwt,
                    expires_in = (int)_options.Expiration.TotalSeconds,
                    login = user.Email
                };

                context.Response.ContentType = "application/json";

                await _auditLogRepository.Add(
                    user.Email, 1, EntityCodes.USER, EntityActions.LOGIN);

                WriteAuthCookie(context, user.UserId.ToString(), _options.Expiration);

                await context.Response.WriteAsync(
                        JsonConvert.SerializeObject(
                            response, new JsonSerializerSettings
                            {
                                Formatting = Formatting.Indented,
                                ContractResolver =
                                new CamelCasePropertyNamesContractResolver()
                            }));
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                context.Response.StatusCode = 500;
                await context.Response.WriteAsync(
                  JsonConvert.SerializeObject(
                          new
                          {
                              message = "Error authenticating user",
#if DEBUG
                              error = ex.ToString()
#endif
                          }, new JsonSerializerSettings
                          {
                              Formatting = Formatting.Indented,
                              ContractResolver =
                              new CamelCasePropertyNamesContractResolver()
                          }));
            }
        }

        private async Task<string> GetCountryCodeByState(string state)
        {
            var allStates = await _locationService.GetAllStates();
            return allStates.FirstOrDefault(x => x.State == state)?.CountryCode;
        }

        private void WriteAuthCookie(HttpContext context, string userId, TimeSpan expiration)
        {
            var terminusBaseDomain = new Uri(_config.TerminusBaseUrl).GetBaseDomain();

            var options = new CookieOptions();
            options.Domain = $".{terminusBaseDomain}";
            options.HttpOnly = false;
            options.Expires = DateTime.Now.Add(expiration);

            context.Response.Cookies.Append(_cookieName, userId, options);
        }

        private async Task<UserProfile> CreateUserProfile(UserMembership user, IEnumerable<LegacyBbbInfo> userSites)
        {
            var defaultSite = userSites?.FirstOrDefault();

            var createdProfile = await _userProfileService.Add(new UserProfile
            {
                UserId = user.UserId.ToString(),
                CreatedBy = user.Email,
                DefaultSite = defaultSite?.LegacyBBBID,
                DefaultSiteName = defaultSite?.BbbName,
                OpenContentInNewTab = false,
            });

            await _auditLogRepository.Add(
                user.Email, createdProfile.Id, EntityCodes.USER, EntityActions.CREATE);

            return createdProfile;
        }

        private async Task<IEnumerable<LegacyBbbInfo>> GetUserSites(IEnumerable<UserRole> roles)
        {

            var sites = await _bbbSiteCache.GetAllForUserAsync(new Domain.Model.AppUser { Roles = roles });

            return sites?.Where(
                site =>
                    !string.IsNullOrWhiteSpace(site.BbbName)
                    && !string.IsNullOrWhiteSpace(site.LegacyBBBID));
        }
    }
}
