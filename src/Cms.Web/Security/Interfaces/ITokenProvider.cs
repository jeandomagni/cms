﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Cms.Web.Security.Interfaces
{
    public interface ITokenProvider
    {
        Task Invoke(HttpContext context);
    }
}
