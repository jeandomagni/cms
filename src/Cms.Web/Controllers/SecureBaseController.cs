﻿using System;
using Cms.Domain.Enum;
using Cms.Domain.Model;
using Cms.Model.Models.User;
using Cms.Web.Filters;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;

namespace Cms.Web.Controllers
{
    [AuthorizationHeader]
    public class SecureBaseController: Controller
    {
        /// <summary>
        /// Deserialized from Claims
        /// </summary>
        public AppUser CurrentUser => new AppUser  {
            UserId = User.GetClaim(BbbCustomClaims.UserId),
            Email = User.GetClaim(BbbCustomClaims.Email),
            Iat = User.GetClaim(JwtRegisteredClaimNames.Iat),
            IsLocalAdmin = GetBooleanClaim( () => User.GetClaim(BbbCustomClaims.LocalAdmin)),
            IsGlobalAdmin = GetBooleanClaim( ()=>User.GetClaim(BbbCustomClaims.GlobalAdmin)),
            Roles = GenerateRoles(
                User.GetClaim(BbbCustomClaims.Roles),
                User.GetClaim(BbbCustomClaims.Email)),
            AdminSites = GetUserSites(User.GetClaim(BbbCustomClaims.AdminSites)),
        };

        private static bool GetBooleanClaim(Func<string> fnFunc)
        {
            try
            {
                var ret = fnFunc();
                return bool.Parse(ret);
            }
            catch
            {
                return false;
            }

        }
        private static IEnumerable<UserRole> GenerateRoles(string delimited, string email)
        {
            var ret=  new List<UserRole>();
            var roleList = delimited?.Split(',');
            foreach (var role in roleList)
            {
                var roleNameSiteId = role.Split('~');
             ret.Add( new UserRole
             {
                 RoleName = roleNameSiteId[0],
                 Email = email,
                 SiteId = roleNameSiteId[1]
             });   

            }
            return ret;
        }

        private static string[] GetUserSites(string delimited) {
            return delimited?.Split(',');
        }
    }

    public static class Extensions
    {
        public static string GetClaim(this ClaimsPrincipal principal, string claimType)
        {
            return principal.Claims.FirstOrDefault(c => c.Type == claimType)?.Value;
        }

        public static string RemoveAccent(this string txt)
        {
            var bytes = System.Text.Encoding.GetEncoding("Cyrillic").GetBytes(txt);
            return System.Text.Encoding.ASCII.GetString(bytes);
        }

        public static string GetSubstringSafe(this string input, int limit)
        {
            if (string.IsNullOrWhiteSpace(input))
                return input;

            return input.Length >= limit ? input.Substring(0, limit) : input;
        }
    }
}
