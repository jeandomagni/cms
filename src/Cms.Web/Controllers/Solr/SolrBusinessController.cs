﻿using Cms.Domain.Mappers;
using Cms.Domain.Services.Solr;
using Cms.Model.Models.BusinessProfile;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cms.Web.Controllers.Solr
{
    [Route("solrproxy/businesses")]
    public class SolrBusinessController : SecureBaseController
    {
        private readonly IBusinessProfileMappers _businessProfileMappers;
        private readonly ISolrCibrService _solrCibrService;
        public SolrBusinessController(
            IBusinessProfileMappers businessProfileMappers,
            ISolrCibrService solrCibrService) {
            _businessProfileMappers = businessProfileMappers;
            _solrCibrService = solrCibrService;
        }

        [HttpGet("suggest/{searchText}")]
        public async Task<IEnumerable<TypeaheadSuggestion>> Suggest(string searchText)
        {
            // Because we don't want to show local reports, we'll have to use the CIBR core :(
            var cibrDocs = await _solrCibrService.SearchAsync(new ProfileSearchParams { SearchText = searchText });
            return cibrDocs.Select(_businessProfileMappers.MapToTypeaheadSuggestion);
        }

        [HttpPost]
        [Route("search")]
        public async Task<BusinessSearchResult> Search([FromBody] ProfileSearchParams profileSearchParams)
        {
            var solrResult = await _solrCibrService.SearchAsync(profileSearchParams);
            return _businessProfileMappers.MapToBusinessSearchResult(solrResult, profileSearchParams);
        }
    }
}
