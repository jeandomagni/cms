﻿using Cms.Domain.Cache;
using Cms.Domain.Enum;
using Cms.Domain.Mappers;
using Cms.Domain.Services.Solr;
using Cms.Model.Models;
using Cms.Model.Models.Params;
using Cms.Model.Models.User;
using Cms.Model.Repositories;
using Cms.Web.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cms.Web.Controllers.Solr
{
    [Route("solrproxy/locations")]
    public class SolrLocationController : SecureBaseController
    {
        private readonly ISolrLocationService _solrService;
        private readonly ILegacyBbbSiteCache _bbbSiteCache;
        private readonly IUserProfileRepository _userProfileRepository;
        private readonly ILocationService _locationService;
        public SolrLocationController(
            ISolrLocationService solrService, 
            ILegacyBbbSiteCache bbbSiteCache,
            IUserProfileRepository userProfileRepository,
            IMemoryCache cache,
            ILocationService locationService) {
            _solrService = solrService;
            _bbbSiteCache = bbbSiteCache;
            _userProfileRepository = userProfileRepository;
            _locationService = locationService;
        }

        [HttpPost]
        [Route("query")]
        public async Task<IEnumerable<Location>> Paginate([FromBody] QueryLocationParams options)
        {
            var allSites = await _bbbSiteCache.GetAllForUserLocationsAsync(CurrentUser);

            var userSites = 
                allSites.Select(userSite => userSite.LegacyBBBID).ToArray();

            var userProfile =
                await _userProfileRepository.GetForUser(CurrentUser.UserId)
                ?? new UserProfile();

            if (!userSites.Any()) {
                if (!CurrentUser.GlobalOrLocalAdmin) {
                    return null;
                }
            }

            if (CurrentUser.GlobalOrLocalAdmin){
                userSites = null;
            }

            var solrLocations = await _solrService.PaginateLocations(
                    options.Query,
                    options.Context,
                    options.LocationType,
                    userSites,
                    options.CountryCodes,
                    userProfile.DefaultSite);
            return solrLocations?.Select(LocationMappers.MapToLocation);
        }

        [HttpGet]
        [Route("statesprovinces")]
        public async Task<IEnumerable<Location>> Query()
        {
            return await _locationService.GetAllStates();
        }
        
        [HttpGet("statesprovinces/{countryCode}")]
        public async Task<IEnumerable<Location>> GetStatesByCountryCode(string countryCode)
        {
            return (await Query()).Where(x => string.Equals(x.CountryCode.ToString(), countryCode, StringComparison.InvariantCultureIgnoreCase));
        }
    }
}
