﻿using Bbb.Core.Solr.Model;
using Cms.Domain.Cache;
using Cms.Domain.Services.Solr;
using Cms.Model.Models.Params;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cms.Web.Controllers.Solr
{
    [Route("solrproxy/category")]
    public class SolrCategoryController: SecureBaseController
    {
        private readonly ISolrSearchTypeaheadService _solrService;
        private readonly ITobCache _tobCache;
        public SolrCategoryController(
            ISolrSearchTypeaheadService solrService,
            ITobCache tobCache) {
            _solrService = solrService;
            _tobCache = tobCache;
        }

        [HttpPost]
        [Route("paginate")]
        public async Task<IList<SearchTypeahead>> Paginate([FromBody] PaginateCategoryParams options) {
            var categories = await _solrService.PaginateCategories(options);
            
            if (options.ShowHighRiskCategories)
            {
                return categories;
            }

            var highRiskCategories = await _tobCache.GetHighRiskTobIdsAsync();
            return categories.Where(x => !highRiskCategories.Contains(x.EntityId)).ToList();
        }
    }
}
