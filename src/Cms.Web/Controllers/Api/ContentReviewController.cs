﻿using Ardalis.GuardClauses;
using Cms.Domain.ContentGuardClauses;
using Cms.Domain.ContentReviewGuardClauses;
using Cms.Domain.Enum;
using Cms.Domain.Services.Solr;
using Cms.Model.Enum;
using Cms.Model.Models.Content;
using Cms.Model.Repositories;
using Cms.Model.Repositories.Content;
using Cms.Web.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Cms.Web.Controllers.Api
{
    [Route("api/[controller]")]
    public class ContentReviewController : SecureBaseController
    {
        private readonly IContentReviewRepository _contentReviewRepository;
        private readonly IContentRepository _contentRepository;
        private readonly ISolrContentService _solrService;
        private readonly IContentService _contentService;
        private readonly IAuditLogRepository _auditLogRepository;

        public ContentReviewController(
            IContentReviewRepository contentReviewRepository, 
            IContentRepository contentRepository,
            ISolrContentService solrService,
            IContentService contentService,
            IAuditLogRepository auditLogRepository) {
            _contentReviewRepository = contentReviewRepository;
            _contentRepository = contentRepository;
            _solrService = solrService;
            _contentService = contentService;
            _auditLogRepository = auditLogRepository;
        }

        [HttpPost]
        [Route("complete")]
        public async Task<OkResult> Complete([FromBody] ReviewRequest request)
        {
            Guard.Against.MissingReviewRequest(request);
            Guard.Against.InvalidReviewStatus(request);

            var contentCode = request.ContentReview.ContentCode;
            var dbContent = await _contentRepository.GetByCode(contentCode, CurrentUser.Email);
            Guard.Against.RecordNotFound(dbContent);
            
            var contentReview = 
                await _contentReviewRepository.GetById(
                    request.ContentReview.Id);

            Guard.Against.RecordNotFound(contentReview);

            contentReview.CompletedBy = CurrentUser.Email;
            contentReview.Comment = request.ContentReview.Comment;
            await _contentReviewRepository.Update(contentReview);

            switch (request.NewStatus)
            {
                case "rejected":
                    dbContent.Status = ContentStatus.Deleted;
                    break;
                case "approved":
                    dbContent.Status = ContentStatus.Published;
                    break;
            }

            dbContent.ModifiedBy = CurrentUser.Email;
            dbContent.ModifiedDate = DateTimeOffset.Now;
            dbContent = await _contentRepository.Update(dbContent);

            await _auditLogRepository.Add(
                CurrentUser.Email, contentReview.Id, EntityCodes.ARTICLE, EntityActions.UPDATE);
            
            await _solrService.PostContent(dbContent);
            return Ok();
        }
        
    }
}
