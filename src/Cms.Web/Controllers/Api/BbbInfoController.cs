﻿using Ardalis.GuardClauses;
using Cms.Domain.BbbInfoGuardClauses;
using Cms.Domain.Enum;
using Cms.Domain.Mappers;
using Cms.Domain.Services;
using Cms.Domain.Services.Solr;
using Cms.Model.Models.BbbInfo;
using Cms.Model.Models.General;
using Cms.Model.Repositories;
using Cms.Web.Filters;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cms.Web.Controllers.Api
{
    [Route("api/[controller]")]
    public class BbbInfoController : SecureBaseController
    {
        private readonly IBbbAuditLogMappers _auditLogMappers;
        private readonly IBbbInfoMappers _bbbInfoMappers;
        private readonly IBbbInfoRepository _bbbInfoRepository;
        private readonly ISolrBbbInfoService _solrBbbInfoService;
        private readonly IAuditLogRepository _auditLogRepository;
        private readonly ILeadService _leadService;

        public BbbInfoController(
            IBbbAuditLogMappers auditLogMappers,
            IBbbInfoMappers bbbInfoMappers,
            IBbbInfoRepository bbbInfoRepository,
            ISolrBbbInfoService solrBbbInfoService, 
            ILeadService leadService,
            IAuditLogRepository auditLogRepository) {
            _auditLogMappers = auditLogMappers;
            _bbbInfoMappers = bbbInfoMappers;
            _bbbInfoRepository = bbbInfoRepository;
            _solrBbbInfoService = solrBbbInfoService;
            _auditLogRepository = auditLogRepository;
            _leadService = leadService;
        }

        [HttpGet]
        [Route("{id}")]
        [TypeFilter(typeof(ApiAuthorizationHeader))]
        public async Task<BbbInfo> GetById(int id)
        {
            var dbBbbInfo = await _bbbInfoRepository.GetById(id);
            return await _bbbInfoMappers.MapToBbbInfoAsync(dbBbbInfo);
        }

        [HttpGet]
        [Route("legacy/{bbbId}")]
        public async Task<BbbInfo> GetByLegacyId(string bbbId)
        {
            var dbBbbInfo = await _bbbInfoRepository.GetByLegacyId(bbbId);
            var bbbInfo = await _bbbInfoMappers.MapToBbbInfoAsync(dbBbbInfo);
            return bbbInfo;
        }

        [HttpPost]
        public async Task<BbbInfo> Post([FromBody] BbbInfo bbbInfo)
        {
            var existingDbBbbInfo = await _bbbInfoRepository.GetById(bbbInfo.Id);
            Guard.Against.InvalidRequestor(existingDbBbbInfo, CurrentUser);
            
            bbbInfo.ModifiedBy = CurrentUser.Email;
            bbbInfo.EventsPageLayout = bbbInfo.EventsPageLayout ?? ListingsPageLayouts.Standard;

            var dbBbbInfo = _bbbInfoMappers.MapToDbBbbInfo(bbbInfo);
            var updatedDbBbbInfo = await _bbbInfoRepository.Update(dbBbbInfo);
            var updatedBbbInfo = await _bbbInfoMappers.MapToBbbInfoAsync(updatedDbBbbInfo);

            await _solrBbbInfoService.Update(updatedBbbInfo);

            var dbAuditLogs = _auditLogMappers.MapToDbAuditLogs(existingDbBbbInfo, updatedDbBbbInfo, CurrentUser.Email);

            if (dbAuditLogs.Any()) {
                await _auditLogRepository.AddMany(dbAuditLogs);
            }

            return updatedBbbInfo;
        }

        [HttpGet]
        [Route("audit/{id}/{page}/{pageSize}")]
        public async Task<IList<AuditLog>> Audit(int id, int page, int pageSize)
        {
            var existingDbBbbInfo = await _bbbInfoRepository.GetById(id);
            var existingBbbInfo = await _bbbInfoMappers.MapToBbbInfoAsync(existingDbBbbInfo);

            Guard.Against.RecordNotFound(existingBbbInfo);
            Guard.Against.InvalidRequestor(existingBbbInfo, CurrentUser);

            var dbAuditLogs = await _auditLogRepository.GetLatestForEntity(id, EntityCodes.BBB, page, pageSize);
            return dbAuditLogs.Select(_auditLogMappers.MapToAuditLog).ToList();
        }

        [HttpGet]
        [Route ("listedLeads/{id}/{rangeStart}")]
        public async Task<IActionResult> ListedLeads (int id, DateTime rangeStart)
        {
            return await ListedLeads (id, rangeStart, null);
        }

        [HttpGet]
        [Route ("listedLeads/{id}/{rangeStart}/{rangeEnd}")]
        public async Task<IActionResult> ListedLeads (int id, DateTime rangeStart, DateTime? rangeEnd)
        {
            var existingDbBbbInfo = await _bbbInfoRepository.GetById (id);            
            var bytes = await _leadService.GetListed (existingDbBbbInfo.LegacyId, rangeStart, rangeEnd);

            if (bytes == null)
                return NotFound (); // returns a NotFoundResult with Status404NotFound response.

            return File (bytes, "text/csv");
        }

        [HttpGet]
        [Route ("accreditationLeads/{id}/{rangeStart}")]
        public async Task<IActionResult> AccreditationLeads (int id, DateTime rangeStart)
        {
            return await AccreditationLeads (id, rangeStart, null);
        }

        [HttpGet]
        [Route ("accreditationLeads/{id}/{rangeStart}/{rangeEnd}")]
        public async Task<IActionResult> AccreditationLeads (int id, DateTime rangeStart, DateTime? rangeEnd)
        {
            var existingDbBbbInfo = await _bbbInfoRepository.GetById (id);            
            var bytes = await _leadService.GetAccreditation (existingDbBbbInfo.LegacyId, rangeStart, rangeEnd);

            if (bytes == null)
                return NotFound (); // returns a NotFoundResult with Status404NotFound response.

            return File (bytes, "text/csv");
        }
    }
}
