﻿using Cms.Model.Repositories;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cms.Web.Controllers.Api
{
    [Route("api/[controller]")]
    public class StateProvinceController : SecureBaseController
    {
        private readonly IStateProvinceRepository _stateProvinceRepository;

        public StateProvinceController(
            IStateProvinceRepository stateProvinceRepository) {
            _stateProvinceRepository = stateProvinceRepository;
        }

        [HttpGet("{countryCode}")]
        public async Task<IEnumerable<string>> GetByCountryCode(string countryCode)
        {
            var states = await _stateProvinceRepository.GetByCountryCodeAsync(countryCode);
            return states;
        }
    }
}
