﻿using Cms.Domain.Exception;
using Cms.Model.Models;
using Cms.Model.Repositories.Content;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Cms.Web.Controllers.Api
{
    [Route("api/[controller]")]
    public class DomainContentController : SecureBaseController
    {
        private readonly IContentRepository _contentRepository;
        private readonly IDomainContentRepository _domainContentRepository;
 
        public DomainContentController(IContentRepository contentRepository,
            IDomainContentRepository domainContentRepository) {
            _contentRepository = contentRepository;
            _domainContentRepository = domainContentRepository;
        }

        [HttpGet("{contentCode}")]
        public async Task<DomainContent> Get(string contentCode)
        {
            return 
                await _domainContentRepository.GetByContentCode(contentCode, CurrentUser.Domain);
        }

        [HttpPost]
        public async Task<DomainContent> Post([FromBody] DomainContent domainContent)
        {
            if (domainContent == null)
                throw new GeneralApiException();

            var storedDomainContent = 
                await _domainContentRepository.GetByContentCode(
                    domainContent.ContentCode, CurrentUser.Domain);

            if (storedDomainContent == null) 
                throw new RecordNotFoundException();
            
            if (storedDomainContent.Domain != CurrentUser.Domain)
                throw new InvalidDomainRequestorException();

            domainContent.ModifiedBy = CurrentUser.Email;
            domainContent.DomainTitle = FormatStringField("Domain Title", "DomainTitle", 200);
            
            return
               await 
                _domainContentRepository.Update(domainContent);
        }

        [HttpPut]
        public async Task<DomainContent> Put([FromBody] DomainContent domainContent)
        {
            if (domainContent == null)
                throw new NullParameterException();

            var storedContent = 
                await _contentRepository.GetByCode(
                    domainContent.ContentCode, CurrentUser.Email);


            if (storedContent == null) 
                throw new RecordNotFoundException();
            
            domainContent.Domain = CurrentUser.Domain;
            domainContent.CreatedBy = CurrentUser.Email;

             return 
                await _domainContentRepository.Add(domainContent);
        }

        [HttpDelete("{id}")]
        public async Task<OkResult> Delete(int id)
        {
            var storedDomainContent =
                await _domainContentRepository.GetById(id);

            if (storedDomainContent == null) {
                throw new InvalidRequestorException();
            }

            if (storedDomainContent.Domain != CurrentUser.Domain)
                throw new InvalidDomainRequestorException();

            await _domainContentRepository.Delete(
                storedDomainContent.Id);

            return Ok();
        }

        private static string FormatStringField(string fieldName, string field, int maxLength)
        {
            if (string.IsNullOrWhiteSpace(field)) {
                throw new FieldRequiredException(
                    fieldName);
            }
            var formatted = field.Trim();
            if (formatted.Length > maxLength) {
                throw new StringLengthLimitException(
                    fieldName, maxLength.ToString());
            }
            return formatted;
        }
    }
}
