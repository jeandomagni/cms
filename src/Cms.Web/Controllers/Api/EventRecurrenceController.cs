﻿using Cms.Model.Models.Recurrence;
using Microsoft.AspNetCore.Mvc;
using RecurrenceCalculator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cms.Web.Controllers.Api
{
    [Route("api/[controller]")]
    public class EventRecurrenceController : SecureBaseController
    {
        [HttpPost]
        [Route("validate")]
        public string Validate([FromBody] EventRecurrence recurrence)
        {
            recurrence.Init();
            return new Calculator().ValidateRecurrence(recurrence);
        }
    }
}
