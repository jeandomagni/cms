﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cms.Domain.Exception;
using Cms.Domain.Mappers;
using Cms.Model.Models.BaselinePages;
using Cms.Model.Models.Query.DataTable;
using Cms.Web.Services;
using Microsoft.AspNetCore.Mvc;

namespace Cms.Web.Controllers.Api
{
    [Route("api/[controller]")]
    public class ShortlinksController : Controller
    {
        private readonly IBaselinePagesService _baselinePagesService;

        public ShortlinksController(IBaselinePagesService baselinePagesService)
        {
            _baselinePagesService = baselinePagesService;
        }

        [HttpPut]
        public async Task<BaselinePage> Put([FromBody] BaselinePage baselinePage)
        {
            if (baselinePage.Id == 0)
            {
                await _baselinePagesService.CreateShortlink(baselinePage);
            }
            else
            {
                await _baselinePagesService.UpdateShortlink(baselinePage);
            }

            return baselinePage;
        }

        [HttpPost]
        [Route("paginate")]
        public Task<IEnumerable<BaselinePage>> Paginate([FromBody] Query query)
        {
            var options = QueryMappers.MapToGridOptions(query);
            return _baselinePagesService.PaginateBaselinePages(options, true);
        }

        [HttpDelete("{id}")]
        public async Task<OkResult> Delete(int id)
        {
            var baselinePage = await _baselinePagesService.GetById(id);

            if (baselinePage == null)
            {
                throw new RecordNotFoundException();
            }

            await _baselinePagesService.DeleteBaselinePage(baselinePage.Id);

            return Ok();
        }
    }
}