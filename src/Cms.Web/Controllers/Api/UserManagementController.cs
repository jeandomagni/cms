﻿using System.Collections.Generic;
using Cms.Model.Repositories;
using Cms.Web.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Cms.Model.Dto.UserManagement;
using Cms.Model.Models.UserManagement;
using Cms.Web.Services.UserManagement;
using Microsoft.AspNetCore.Authorization;

namespace Cms.Web.Controllers.Api
{
    [Route("api/[controller]")]
    public class UserManagementController : SecureBaseController
    {
        private readonly IUserProfileService _userProfileService;
        private readonly IUserManagementService _userManagementService;
        private readonly IAuditLogRepository _auditLogRepository;

        public UserManagementController(
            IUserProfileService userProfileService,
            IUserManagementService userManagementService,
            IAuditLogRepository auditLogRepository
            )
        {
            _userProfileService = userProfileService;
            _userManagementService = userManagementService;
            _auditLogRepository = auditLogRepository;
        }

        [HttpPost]
        [Route("paginate")]
        public async Task<PaginationResultDto<UserMembershipDto>> Paginate([FromBody] UserMembershipSearchDto query)
        {

            return await _userManagementService.GetUserMemberShipListAsync(query);
        }
        [HttpPut]
        [Route("UpdateStatus")]
        public async Task<ServiceResponseDto> UpdateStatus([FromBody]  UserStatusUpdateRequestDto request)
        {
            return await _userManagementService.UpdateUserStatusAsync(request);
        }
        [HttpPut]
        [AllowAnonymous]
        [Route("ChangePasswordByEmail")]
        public async Task<ServiceResponseDto> UpdatePasswordByEmail([FromBody] UserPasswordUpdateRequestDto request)
        {
            return await _userManagementService.UpdateUserPasswordAsync(request);
        }
        [HttpPut]
        [Route("ChangePassword")]
        public async Task<ServiceResponseDto> UpdatePassword([FromBody] UserPasswordUpdateRequestDto request)
        {
            return await _userManagementService.UpdateUserPasswordAsync(request);
        }


        [HttpPut]
        [Route("Update")]
        public async Task<ServiceResponseDto<AddUpdateUserRequestDto>> UpdateUser([FromBody] AddUpdateUserRequestDto request)
        {
            return await _userManagementService.UpdateUserAsync(request);
        }
        [HttpPost]
        [Route("Add")]
        public async Task<ServiceResponseDto<AddUpdateUserRequestDto>> AddUser([FromBody] AddUpdateUserRequestDto request)
        {
            return await _userManagementService.AddUserAsync(request);
        }
        [HttpDelete("{userId}")]
        [Route("Delete")]
        public async Task<ServiceResponseDto> DeleteUser(string userId)
        {
            return await _userManagementService.DeleteUserAsync(userId);
        }

        [HttpGet]
        [Route("Roles")]
        public async Task<IEnumerable<RolesDto>> RoleList()
        {
            return await _userManagementService.GetRolesListAsync();
        }

        [HttpGet]
        [Route("Info")]
        public async Task<UserMembershipInfoDto> MembershipInfo(string userId)
        {
            return await _userManagementService.GetUserMemberShipWithRolesAsync(userId);
        }
        [HttpGet]
        [Route("UserByEmail")]
        public async Task<UserMembershipDto> GetUserByEmail(string email)
        {
            return await _userManagementService.GetUserByEmailAsync(email);
        }

    }
}