﻿using Ardalis.GuardClauses;
using Cms.Domain.ContentAbListGuardClauses;
using Cms.Domain.Exception;
using Cms.Domain.Mappers;
using Cms.Model.Models.Content;
using Cms.Model.Models.Content.Db;
using Cms.Model.Models.Content.Entities;
using Cms.Model.Repositories.Content;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cms.Web.Controllers.Api
{
    [Route("api/[controller]")]
    public class ContentAbListController : SecureBaseController
    {
        private readonly IContentRepository _contentRepository;
        private readonly IContentAbListRepository _contentAbListRepository;
        private readonly IContentAbListMappers _contentAbListMappers;

        public ContentAbListController(
            IContentRepository contentRepository,
            IContentAbListRepository contentAbListRepository,
            IContentAbListMappers contentAbListMappers) {
            _contentRepository = contentRepository;
            _contentAbListRepository = contentAbListRepository;
            _contentAbListMappers = contentAbListMappers;
        }

        [HttpPut]
        public async Task<ContentAbList> Put([FromBody] ContentAbList contentAbList)
        {
            var content =
                await _contentRepository.GetByCode(
                    contentAbList.ContentCode, CurrentUser.Email);

            if (content == null)
            {
                throw new RecordNotFoundException();
            }

            if (!CurrentUser.HasAccessToRecord(
                content.CreatedBy, content.OwnerSiteId))
            {
                throw new InvalidRequestorException();
            }

            var dbContentAbList = _contentAbListMappers.MapToDbContentAbList(contentAbList);
            var createdDbContentAbList = await _contentAbListRepository.Add(dbContentAbList);
            return _contentAbListMappers.MapToContentAbList(createdDbContentAbList);
        }

        [HttpGet]
        [Route("content/{contentCode}")]
        public async Task<ContentAbList> GetForContentByCode(string contentCode)
        {
            var dbContentAbList =
                await _contentAbListRepository.GetByContentCode(contentCode);

            return dbContentAbList != null
                ? _contentAbListMappers.MapToContentAbList(dbContentAbList)
                : new ContentAbList() {
                    ContentCode = contentCode,
                    BusinessLinkLists = new List<BusinessLinkList>()
                };
        }

        [HttpPost]
        [Route("update")]
        public async Task<ContentAbList> Update([FromBody] ContentAbList contentAbList)
        {
            Guard.Against.InvalidLinkLists(contentAbList);

            var existingDbContentAbList =
                await _contentAbListRepository.GetByContentCode(contentAbList.ContentCode);

            var dbContentAbList = _contentAbListMappers.MapToDbContentAbList(contentAbList);

            DbContentAbList updatedDbContentAbList;
            if (existingDbContentAbList == null)
            {
                updatedDbContentAbList = await _contentAbListRepository.Add(dbContentAbList);
            }
            else
            {
                dbContentAbList.Id = existingDbContentAbList.Id;
                updatedDbContentAbList = await _contentAbListRepository.Update(dbContentAbList);
            }

            return _contentAbListMappers.MapToContentAbList(updatedDbContentAbList);
        }
    }
}
