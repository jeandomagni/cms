﻿using Cms.Domain.Enum;
using Cms.Domain.Exception;
using Cms.Domain.Mappers;
using Cms.Domain.Services.Solr;
using Cms.Model.Models.Content;
using Cms.Model.Models.Query.DataTable;
using Cms.Model.Models.Query.KendoGrid;
using Cms.Model.Repositories;
using Cms.Model.Repositories.Content;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cms.Model.Models.Content.Db;

namespace Cms.Web.Controllers.Api
{
    [Route("api/[controller]")]
    public class ContentQuoteController : SecureBaseController
    {
        private readonly IContentRepository _contentRepository;
        private readonly IContentQuoteRepository _contentQuoteRepository;
        private readonly ISolrContentService _solrService;
        private readonly IUserProfileRepository _userProfileRepository;

        public ContentQuoteController(
            IContentRepository contentRepository,
            IContentQuoteRepository contentQuoteRepository,
            ISolrContentService solrService,
            IUserProfileRepository userProfileRepository) {
            _contentQuoteRepository = contentQuoteRepository;
            _contentRepository = contentRepository;
            _solrService = solrService;
            _userProfileRepository = userProfileRepository;
        }

        [HttpPut]
        public async Task<ContentQuote> Put([FromBody] ContentQuote contentQuote)
        {
            var userProfile =
                    await _userProfileRepository.GetForUser(CurrentUser.UserId);
            
            contentQuote.CreatedBy = CurrentUser.Email;
            contentQuote.CreateDate = DateTime.Now;
            contentQuote.OwnerSiteId = userProfile?.DefaultSite ?? "2000";

            var quote = await _contentQuoteRepository.GetByCode(contentQuote.Code);
            
            if (quote == null)
            {
                contentQuote.Code = Guid.NewGuid().ToString();
                await _contentQuoteRepository.Add(contentQuote);
            }
            else
            {
                await _contentQuoteRepository.Update(contentQuote);
            }

            if (contentQuote.Published)
            {
                Publish(contentQuote, CurrentUser.Email);
            }
            else
            {
                Unpublish(contentQuote);
            }

            return contentQuote;
        }

        [HttpPost]
        [Route("paginate")]
        public Task<IEnumerable<ContentQuote>> Paginate([FromBody] Query query)
        {
            var options = QueryMappers.MapToGridOptions(query);
            return _contentQuoteRepository.Paginate(options, CurrentUser.Email);
        }

        [HttpDelete("{code}")]
        public async Task<OkResult> Delete(string code)
        {
            var storedFile = await _contentQuoteRepository.GetByCode(code);

            if (storedFile == null)
            {
                throw new RecordNotFoundException();
            }

            if (!CurrentUser.HasAccessToRecord(storedFile.CreatedBy, string.Empty))
            {
                throw new InvalidRequestorException();
            }

            Unpublish(storedFile);

            await _contentQuoteRepository.DeleteByCode(storedFile.Code);

            return Ok();
        }

        private async void Publish(ContentQuote quote, string userEmail)
        {
            var existingContent = await _contentRepository.GetByCode(quote.Code, userEmail);

            DbContent contentToSave;
            if (existingContent == null)
            {
                contentToSave = ContentQuoteMappers.MapToDbContent(quote);
                contentToSave.CreatedBy = userEmail;
                contentToSave.CreateDate = DateTime.Now;
                contentToSave = await _contentRepository.Add(contentToSave);
            }
            else
            {
                contentToSave = ContentQuoteMappers.MapToDbContent(quote);
                contentToSave.Id = existingContent.Id;
                contentToSave.Code = existingContent.Code;
                contentToSave.CreatedBy = existingContent.CreatedBy;
                contentToSave.CreateDate = existingContent.CreateDate;
                contentToSave = await _contentRepository.Update(contentToSave);
            }


            await _solrService.PostContent(contentToSave);


        }

        private async void Unpublish(ContentQuote quote)
        {
            var content = await _contentRepository.GetByCode(quote.Code, CurrentUser.Email);

            if (content == null) return;

            await _contentRepository.Delete(content.Id);

            await _solrService.Delete(content.Id.ToString());
        }
    }
}
