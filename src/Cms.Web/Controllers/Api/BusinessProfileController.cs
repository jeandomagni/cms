﻿using Ardalis.GuardClauses;
using Cms.Domain.BusinessProfileGuardClauses;
using Cms.Domain.Mappers;
using Cms.Domain.Services.Solr;
using Cms.Model.Models.BusinessProfile;
using Cms.Model.Repositories.BusinessProfileSeo;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Cms.Web.Controllers.Api
{
    [Route("api/[controller]")]
    public class BusinessProfileController : SecureBaseController
    {
        private readonly IBusinessProfileMappers _businessProfileMappers;
        private readonly IBusinessProfileSeoRepository _businessProfileSeoRepository;
        private readonly ISolrCibrService _solrService;

        public BusinessProfileController(
            IBusinessProfileMappers businessProfileMappers,
            IBusinessProfileSeoRepository businessProfileSeoRepository,
            ISolrCibrService solrService) {
            _businessProfileMappers = businessProfileMappers;
            _businessProfileSeoRepository = businessProfileSeoRepository;
            _solrService = solrService;
        }
        
        [HttpGet("{id}")]
        public async Task<BusinessProfile> Get(string id)
        {
            Guard.Against.InvalidId(id);

            var idParts = id.Split('-');
            var bbbId = idParts[0];
            var businessId = idParts[1];

            var cibrDoc = await _solrService.GetPrimaryRecordByBbbIdAndBusinessIdAsync(bbbId, businessId);

            Guard.Against.RecordNotFound(cibrDoc);

            var dbBusinessProfileSeo = await _businessProfileSeoRepository.GetByBbbIdAndBusinessIdAsync(bbbId, businessId);
            return await _businessProfileMappers.MapToBusinessProfileAsync(cibrDoc, dbBusinessProfileSeo);
        }

        [HttpPost("update")]
        public async Task Update([FromBody] BusinessProfile businessProfile)
        {
            var dbBusinessProfileSeo = BusinessProfileMappers.MapToDbBusinessProfileSeo(businessProfile);
            var existingDbBusinessProfileSeo = await _businessProfileSeoRepository.GetByBbbIdAndBusinessIdAsync(businessProfile.BbbId, businessProfile.BusinessId);

            if (existingDbBusinessProfileSeo == null)
            {
                await _businessProfileSeoRepository.Add(dbBusinessProfileSeo);
            }
            else
            {
                await _businessProfileSeoRepository.Update(dbBusinessProfileSeo);
            }
        }
    }
}
