﻿using Cms.Domain.Services.Solr;
using Cms.Model.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Cms.Web.Controllers.Api
{
    [Produces("application/json")]
    [Route("api/quoteTag")]
    public class QuoteTagController : Controller
    {
        private readonly ISolrLocationService _solrLocationService;

        public QuoteTagController(
            ISolrLocationService solrLocationService)
        {
            _solrLocationService = solrLocationService;
        }

        [HttpGet]
        [Route("suggest")]
        public async Task<IEnumerable<QuoteTag>> Query(string input, string filter)
        {
            filter = string.IsNullOrWhiteSpace(filter) || "undefined".Equals(filter) ? QuoteTagType.All : filter;

            var suggestedTags = new HashSet<QuoteTag>();
            
            var cityStateResult = await _solrLocationService.SuggestCityStates(input);
            var stateResult = await _solrLocationService.GetSuggestedStateProvinces(input);

            var textInfo = Thread.CurrentThread.CurrentCulture.TextInfo;

            var locationTags = QuoteTagType.All.Equals(filter) || QuoteTagType.City.Equals(filter)
                ? cityStateResult.Select(x => new QuoteTag(x, QuoteTagType.City))
                : Enumerable.Empty<QuoteTag>();

            var stateTags = QuoteTagType.All.Equals(filter) || QuoteTagType.State.Equals(filter)
                ? stateResult.Select(x => new QuoteTag(x, QuoteTagType.State))
                : Enumerable.Empty<QuoteTag>();

            var countryTags = QuoteTagType.All.Equals(filter) || QuoteTagType.Country.Equals(filter)
                ? QuoteTag.CountryTags.Where(x => x.Name.StartsWith(input, StringComparison.OrdinalIgnoreCase) || x.Value.StartsWith(input, StringComparison.OrdinalIgnoreCase))
                : Enumerable.Empty<QuoteTag>();
            
            suggestedTags.UnionWith(countryTags);
            suggestedTags.UnionWith(stateTags);
            suggestedTags.UnionWith(locationTags);

            return suggestedTags;
        }
    }
}