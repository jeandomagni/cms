﻿using Cms.Domain.Services.Solr;
using Cms.Model.Models;
using Cms.Model.Repositories;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Cms.Web.Controllers.Api
{
    [Produces("application/json")]
    [Route("api/ImageTag")]
    public class ImageTagController : Controller
    {
        private readonly ITagRepository _tagRepository;
        private readonly ISolrLocationService _solrLocationService;

        private static readonly ImageTag[] CountryTags = new ImageTag[]
        {
            new ImageTag { Name = "United States", Value = "usa", Type = ImageTagType.Country },
            new ImageTag { Name = "Canada", Value = "can", Type = ImageTagType.Country },
            new ImageTag { Name = "Mexico", Value = "mex", Type = ImageTagType.Country }
        };

        public ImageTagController(
            ITagRepository tagRepository,
            ISolrLocationService solrLocationService)
        {
            _tagRepository = tagRepository;
            _solrLocationService = solrLocationService;
        }

        [HttpGet]
        [Route("suggest")]
        public async Task<IEnumerable<ImageTag>> Query(string input, string filter)
        {
            filter = string.IsNullOrWhiteSpace(filter) || "undefined".Equals(filter) ? ImageTagType.All : filter;

            var suggestedTags = new HashSet<ImageTag>();

            var tagResult = await _tagRepository.Query(input, null);
            var cityStateResult = await _solrLocationService.SuggestCityStates(input);
            var stateResult = await _solrLocationService.GetSuggestedStateProvinces(input);

            var textInfo = Thread.CurrentThread.CurrentCulture.TextInfo;

            var commonTags = ImageTagType.All.Equals(filter) || ImageTagType.Generic.Equals(filter) ? 
                tagResult.Select(x => new ImageTag { Name = textInfo.ToLower(x), Value = x, Type = ImageTagType.Generic }) 
                : Enumerable.Empty<ImageTag>();

            var locationTags = ImageTagType.All.Equals(filter) || ImageTagType.City.Equals(filter) ? 
                cityStateResult.Select(x => new ImageTag { Name = $"{x.City}, {x.State} {x.CountryCode}", Value = $"{x.CitySeo}-{x.State}-{x.CountryCode}".ToLower(), Type = ImageTagType.City }) 
                : Enumerable.Empty<ImageTag>();

            var stateTags = ImageTagType.All.Equals(filter) || ImageTagType.State.Equals(filter) ? 
                stateResult.Select(x => new ImageTag { Name = x.StateFull, Value = x.State.ToLower(), Type = ImageTagType.State })
                : Enumerable.Empty<ImageTag>();

            var countryTags = ImageTagType.All.Equals(filter) || ImageTagType.Country.Equals(filter) ? 
                CountryTags.Where(x => x.Name.StartsWith(input, StringComparison.OrdinalIgnoreCase) || x.Value.StartsWith(input, StringComparison.OrdinalIgnoreCase))
                : Enumerable.Empty<ImageTag>();

            suggestedTags.UnionWith(commonTags);
            suggestedTags.UnionWith(countryTags);
            suggestedTags.UnionWith(stateTags);
            suggestedTags.UnionWith(locationTags);

            return suggestedTags;
        }
    }
}