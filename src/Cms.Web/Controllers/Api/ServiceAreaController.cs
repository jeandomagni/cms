﻿using Cms.Domain.Services.Solr;
using Cms.Web.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cms.Web.Controllers.Api
{
    [Route("api/[controller]")]
    public class ServiceAreaController : SecureBaseController
    {
        private readonly IServiceAreaService _serviceAreaService;
        private readonly ILocationService _locationService;

        public ServiceAreaController(
            IServiceAreaService serviceAreaService,
            ILocationService locationService) {
            _serviceAreaService = serviceAreaService;
            _locationService = locationService;
        }

        [HttpGet]
        [Route("countries")]
        public async Task<IEnumerable<string>> GetServiceableCountries()
        {
            var states = await _serviceAreaService.GetServiceableCountriesForUser(CurrentUser);
            return states;
        }

        [HttpGet]
        [Route("states")]
        public async Task<IEnumerable<string>> GetServiceableStates()
        {
            var states = await _serviceAreaService.GetServiceableStatesForUser(CurrentUser);
            return states;
        }

        [HttpGet]
        [Route("states/{countriesParam}")]
        public async Task<IEnumerable<string>> GetServiceableStates(string countriesParam)
        {
            var countries = countriesParam.Split(",");
            var statesForUser = await _serviceAreaService.GetServiceableStatesForUser(CurrentUser);

            var statesWithCountry = await _locationService.GetAllStates();
            var statesFilteredByCountries = statesWithCountry.Where(x => countries.Length == 0 || countries.Contains(x.CountryCode.ToString()));
            var result = statesFilteredByCountries
                .Where(x => statesForUser.Contains(x.State))
                .Select(x => string.Format("{0} {1}", x.State, x.CountryCode))
                .ToList();

            return result;
        }
    }
}
