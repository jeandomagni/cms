﻿using Cms.Model.Models.SecTerm;
using Cms.Web.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Cms.Web.Controllers.Api
{
    [Route("api/[controller]")]
    public class TobAliasController : SecureBaseController
    {
        private readonly ITobAliasService _tobAliasService;

        public TobAliasController(
            ITobAliasService tobAliasService) {
            _tobAliasService = tobAliasService;
        }
        
        [HttpPost]
        [Route("delete")]
        public async Task<IActionResult> Delete([FromBody] AddEditAliasParams addEditAliasParams)
        {
            await _tobAliasService.Delete(addEditAliasParams);
            return Ok(addEditAliasParams.Alias.AliasId);
        }

        [HttpPost]
        [Route("add")]
        public async Task<IActionResult> Add([FromBody] AddEditAliasParams addEditAliasParams)
        {
            var alias = await _tobAliasService.Add(addEditAliasParams);
            return Ok(alias);
        }

        [HttpPost]
        [Route("update")]
        public async Task<IActionResult> Update([FromBody] AddEditAliasParams addEditAliasParams)
        {
            var alias = await _tobAliasService.Update(addEditAliasParams);
            return Ok(alias);
        }
    }
}
