﻿using Cms.Domain.Enum;
using Cms.Domain.Exception;
using Cms.Domain.Mappers;
using Cms.Model.Enum;
using Cms.Model.Models.Content;
using Cms.Model.Repositories;
using Cms.Model.Repositories.Content;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cms.Web.Controllers.Api
{
    [Route("api/[controller]")]
    public class ContentNearMeController : SecureBaseController
    {
        private readonly IContentRepository _contentRepository;
        private readonly IContentNearMeRepository _contentNearMeRepository;
        private readonly IAuditLogRepository _auditLogRepository;
        private readonly IContentNearMeMappers _contentNearMeMappers;

        public ContentNearMeController(
            IContentRepository contentRepository,
            IContentNearMeRepository contentNearMeRepository,
            IAuditLogRepository auditLogRepository,
            IContentNearMeMappers contentNearMeMappers) {
            _contentNearMeRepository = contentNearMeRepository;
            _contentRepository = contentRepository;
            _auditLogRepository = auditLogRepository;
            _contentNearMeMappers = contentNearMeMappers;
        }

        [HttpGet]
        [Route("selectedTobIds")]
        public async Task<List<string>> GetSelectedTobIds()
        {
            return await _contentNearMeRepository.GetSelectedTobIds();
        }

        [HttpGet]
        [Route("content/{contentCode}")]
        public async Task<ContentNearMe> GetForContentByCode(string contentCode)
        {
            var dbContentNearMe = await _contentNearMeRepository.GetForContentByCode(contentCode);
            return _contentNearMeMappers.MapToContentNearMe(dbContentNearMe);
        }

        [HttpPut]
        public async Task<ContentNearMe> Put([FromBody] ContentNearMe contentNearMe)
        {
            var content = await _contentRepository.GetByCode(
                    contentNearMe.ContentCode, CurrentUser.Email);

            if (content == null)  {
                throw new RecordNotFoundException();
            }

            if (!CurrentUser.HasAccessToRecord(
                content.CreatedBy, content.OwnerSiteId))  {
                throw new InvalidRequestorException();
            }

            var dbContentNearMe = _contentNearMeMappers.MapToDbContentNearMe(contentNearMe);
            var createdDbContentNearMe = await _contentNearMeRepository.Add(dbContentNearMe);

            await _auditLogRepository.Add(
                CurrentUser.Email, content.Id, EntityCodes.NEARME, EntityActions.CREATE);

            return _contentNearMeMappers.MapToContentNearMe(createdDbContentNearMe);
        }

        [HttpPost]
        public async Task<ContentNearMe> Post([FromBody] ContentNearMe contentNearMe)
        {
            var content =
                await _contentRepository.GetByCode(
                    contentNearMe.ContentCode, CurrentUser.Email);

            if (content == null) {
                throw new RecordNotFoundException();
            }

            if (!CurrentUser.HasAccessToRecord(
                content.CreatedBy, content.OwnerSiteId)) {
                throw new InvalidRequestorException();
            }
            var dbContentNearMe = _contentNearMeMappers.MapToDbContentNearMe(contentNearMe);
            var updatedDbContentNearMe =  await _contentNearMeRepository.Update(dbContentNearMe);

            await _auditLogRepository.Add(
                CurrentUser.Email, content.Id, EntityCodes.NEARME, EntityActions.UPDATE);

            return _contentNearMeMappers.MapToContentNearMe(updatedDbContentNearMe);
        }

        [HttpDelete("{contentCode}/{id}")]
        public async Task<OkResult> Delete(string contentCode, int id)
        {
            var storedContent = 
                await _contentRepository.GetByCode(
                    contentCode, CurrentUser.Email);

            if (storedContent == null)
                throw new RecordNotFoundException();

            if (!CurrentUser.HasAccessToRecord(
                storedContent.CreatedBy, storedContent.OwnerSiteId))
                throw new InvalidRequestorException();

            await _contentNearMeRepository.Delete(id);

            await _auditLogRepository.Add(
                CurrentUser.Email, id, EntityCodes.NEARME, EntityActions.DELETE);

            return Ok();
        }
    }
}
