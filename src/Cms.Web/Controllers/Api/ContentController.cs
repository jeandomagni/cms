﻿using Ardalis.GuardClauses;
using Cms.Domain.ContentGuardClauses;
using Cms.Domain.Enum;
using Cms.Domain.Helpers;
using Cms.Domain.Mappers;
using Cms.Domain.Services.Solr;
using Cms.Model;
using Cms.Model.Enum;
using Cms.Model.Models;
using Cms.Model.Models.Config;
using Cms.Model.Models.Content;
using Cms.Model.Models.General;
using Cms.Model.Models.Query;
using Cms.Model.Models.Query.DataTable;
using Cms.Model.Models.Query.KendoGrid;
using Cms.Model.Repositories;
using Cms.Model.Repositories.Content;
using Cms.Web.Services;
using Cms.Web.Services.Email;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cms.Domain.Contracts;

namespace Cms.Web.Controllers.Api
{
    [Route("api/[controller]")]
    public class ContentController : SecureBaseController
    {
        private readonly IContentAuditLogMappers _auditLogMappers;
        private readonly IContentRepository _contentRepository;
        private readonly IContentMediaRepository _contentMediaRepository;
        private readonly IContentService _contentService;
        private readonly IAuditLogRepository _auditLogRepository;
        private readonly IPinnedContentLocationRepository _pinnedContentLocationRepository;
        private readonly IArticleOrderSummaryService _articleOrderSummaryService;
        private readonly IContentReviewRepository _contentReviewRepository;
        private readonly IEmailContentService _emailService;
        private readonly IContentMappers _contentMappers;
        private readonly IUserProfileService _userProfileService;
        private readonly ISolrContentService _solrContentService;
        private readonly IContentReviewMappers _reviewMappers;
        private readonly IArticleOrderService _articleOrderService;
        private readonly General _generalConfig;

        public ContentController(
            IContentAuditLogMappers auditLogMappers,
            IContentRepository contentRepository,
            IContentService contentService,
            IContentMediaRepository contentMediaRepository,
            IAuditLogRepository auditLogRepository,
            IPinnedContentLocationRepository pinnedContentLocationRepository,
            IArticleOrderSummaryService articleOrderSummaryService,
            IContentReviewRepository contentReviewRepository,
            IEmailContentService emailService,
            IContentMappers contentMappers,
            IContentReviewMappers reviewMappers,
            IOptions<General> generalConfig,
            IArticleOrderService articleOrderService,
            IUserProfileService userProfileService,
            ISolrContentService solrContentService) {
            _auditLogMappers = auditLogMappers;
            _contentRepository = contentRepository;
            _contentMediaRepository = contentMediaRepository;
            _contentService = contentService;
            _auditLogRepository = auditLogRepository;
            _articleOrderSummaryService = articleOrderSummaryService;
            _pinnedContentLocationRepository = pinnedContentLocationRepository;
            _contentReviewRepository = contentReviewRepository;
            _emailService = emailService;
            _contentMappers = contentMappers;
            _reviewMappers = reviewMappers;
            this._articleOrderService = articleOrderService;
            _generalConfig = generalConfig.Value;
            _userProfileService = userProfileService;
            this._solrContentService = solrContentService;
        }

        [HttpGet("{code}")]
        public async Task<Content> Get(string code)
        {
            var dbContent = await _contentRepository.GetByCode(code, CurrentUser.Email);

            Guard.Against.RecordNotFound(dbContent);

            return await _contentMappers.MapToContentAsync(dbContent, CurrentUser);
        }

        [HttpPost]
        public async Task<Content> Post([FromBody] Content content)
        {
            Guard.Against.CheckedOutContent(content, CurrentUser);
            return await _contentService.UpdateAsync(content, CurrentUser);
        }

        [HttpPost]
        [Route("paginate")]
        public async Task<IList<ContentPreview>> Paginate([FromBody] Query query)
        {
            var options = QueryMappers.MapToGridOptions(query);

            if (options.data.filter == null) options.data.filter = KendoFilter.Empty;

            options.data.filter.filters.Add(new Logic { field = "type", @operator = "neq", value = "Image" });
            options.data.filter.filters.Add(new Logic { field = "type", @operator = "neq", value = "Quote" });

            var userProfile = await _userProfileService.GetForUser(CurrentUser.UserId);
            
            var paginatedDbContent = await _contentRepository.Paginate(options, CurrentUser.Email);
            return paginatedDbContent
                .Select(content => _contentMappers.MapToContentPreview(content, CurrentUser, userProfile.DefaultSite))
                .ToList();
        }

        [HttpPut]
        public async Task<string> Put([FromBody] Content content)
        {
            var createdContent = await _contentService.CreateAsync(content, CurrentUser);

            await _auditLogRepository.Add(
                CurrentUser.Email,
                createdContent.Id,
                ContentHelpers.MapContentTypeToEntity(createdContent.Type),
                EntityActions.CREATE);

            return createdContent.Code;
        }

        [HttpPost]
        [Route("publish")]
        public async Task<Content> Publish([FromBody] Content content)
        {
            Guard.Against.RecordNotFound(content);
            Guard.Against.InvalidRequestor(content, CurrentUser);

            if (content.Status == ContentStatus.PendingReview)
            {
                var contentReview = await _contentReviewRepository.GetByContentCode(content.Code);
                if (contentReview != null)
                {
                    contentReview.CompletedBy = CurrentUser.Email;
                    contentReview.Status = ContentReviewStatus.Approved;
                    contentReview.Serialized = JsonConvert.SerializeObject(content);
                    await _contentReviewRepository.Update(contentReview);
                }
            }

            Guard.Against.InvalidUpdates(content);
            Guard.Against.InvalidAuthors(content);
            Guard.Against.MissingLocationTag(content);

            var images = await _contentMediaRepository.GetByContentCode(content.Code);
            Guard.Against.InvalidCarousel(content, images);

            // Check if content requires approval
            if (!CurrentUser.IsGlobalAdmin && content.Type != ContentType.Event)
            {
                await _contentReviewRepository.Add(_reviewMappers.MapToContentReview(content, CurrentUser));
                _emailService.SendReviewNotification(content);
                content.Status = ContentStatus.PendingReview;
            }
            else if (CurrentUser.IsGlobalAdmin && content.Status == ContentStatus.PendingReview)
            {
                // #WEB-3387: upon article approval, notify BBB admin
                var existingContent = await _contentRepository.GetByCode(content.Code, CurrentUser.Email);
                _emailService.SendApprovalNotification(content, existingContent);

                content.Status = ContentStatus.Published;
            }
            else
            {
                content.Status = ContentStatus.Published;
            }

            content.PublishDate = content.PublishDate ?? DateTimeOffset.Now;
            var updatedContent = await _contentService.UpdateAsync(content, CurrentUser);

            await _auditLogRepository.Add(
                CurrentUser.Email,
                content.Id,
                ContentHelpers.MapContentTypeToEntity(updatedContent.Type),
                EntityActions.PUBLISH);

            return updatedContent;
        }

        [HttpPost]
        [Route("unpublish/{code}")]
        public async Task<Content> Unpublish(string code)
        {
            var dbContent = await _contentRepository.GetByCode(code, CurrentUser.Email);

            Guard.Against.RecordNotFound(dbContent);

            var content = await _contentMappers.MapToContentAsync(dbContent, CurrentUser);

            Guard.Against.InvalidRequestor(content, CurrentUser);

            content.PublishDate = null;
            content.Status = ContentStatus.Draft;
            var updatedContent = await _contentService.UpdateAsync(content, CurrentUser);

            await _auditLogRepository.Add(
                CurrentUser.Email,
                updatedContent.Id,
                ContentHelpers.MapContentTypeToEntity(updatedContent.Type),
                EntityActions.UNPUBLISH);

            return updatedContent;
        }

        [HttpPost]
        [Route("checkOut/{code}")]
        public async Task<Content> CheckOut(string code)
        {
            var existingContent = await _contentRepository.GetByCode(code, CurrentUser.Email);
            Guard.Against.RecordNotFound(existingContent);
            Guard.Against.InvalidRequestor(existingContent, CurrentUser);

            existingContent.CheckoutDate = DateTimeOffset.Now;
            existingContent.CheckoutBy = CurrentUser.Email;
            var updatedDbContent = await _contentRepository.Update(existingContent);

            await _auditLogRepository.Add(
                CurrentUser.Email, updatedDbContent.Id,
                ContentHelpers.MapContentTypeToEntity(updatedDbContent.Type),
                EntityActions.CHECKOUT);

            return await _contentMappers.MapToContentAsync(updatedDbContent, CurrentUser);
        }

        [HttpPost]
        [Route("checkIn/{code}")]
        public async Task<Content> CheckIn(string code)
        {
            var existingContent = await _contentRepository.GetByCode(code, CurrentUser.Email);
            Guard.Against.RecordNotFound(existingContent);
            Guard.Against.InvalidRequestor(existingContent, CurrentUser);

            existingContent.CheckoutDate = null;
            existingContent.CheckoutBy = null;
            var updatedDbContent = await _contentRepository.Update(existingContent);

            await _auditLogRepository.Add(
                CurrentUser.Email,
                updatedDbContent.Id,
                ContentHelpers.MapContentTypeToEntity(updatedDbContent.Type),
                EntityActions.CHECKIN);

            return await _contentMappers.MapToContentAsync(updatedDbContent, CurrentUser);
        }

        [HttpDelete("{id}")]
        public async Task<OkResult> Delete(int id)
        {
            await _contentService.Delete(id, CurrentUser, _generalConfig.EnableSoftDelete);

            return Ok();
        }

        [HttpPost]
        [Route("updateArticleOrder")]
        public async Task<int?> UpdateArticleOrder([FromBody] UpdateArticleOrderParams articleOrder)
        {
            try
            {
                var storedArticleOrder = await _articleOrderService.GetOrCreate(articleOrder);

                List<string> futureArticleIds = await _solrContentService.GetFutureArticles();

                var futureArticleOrder = futureArticleIds
                    .Where(x => storedArticleOrder.OrderedArticleIdsAsArray.Contains(x))
                    .Select(x => new { Id = x, Order = storedArticleOrder.OrderedArticleIdsAsArray.IndexOf(x) })
                    .ToList();

                // TODO update order in articleOrder
                var originalOrder = storedArticleOrder.OrderedArticleIdsAsArray
                    .Where(x => !string.IsNullOrWhiteSpace(x)) // Filter empty strings
                    .Where(x => futureArticleIds.All(y => y != x)) // Filter all articles from future
                    .ToList();

                var newOrder = articleOrder.Order;
                for (int i = 0; i < newOrder.Length; i++)
                {
                    originalOrder[i + ((articleOrder.Page - 1) * articleOrder.PageSize)] = newOrder[i];
                }

                foreach (var order in futureArticleOrder)
                {
                    originalOrder.Insert(order.Order, order.Id);
                }

                storedArticleOrder.OrderedArticleIdsAsArray = originalOrder.ToList();
                await _articleOrderService.UpdateOrderMultiple(new List<ArticleOrder> { storedArticleOrder });
                return storedArticleOrder.Id;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        [HttpPost]
        [Route("paginateListChildren")]
        public async Task<OrderedArticlesByLocation> PaginateListChildren([FromBody] ArticleOrderParams orderParams)
        {
            var articleOrder = await _articleOrderService.Get(orderParams);

            if (articleOrder != null)
            {
                return await _contentService.GetOrderedArticles(articleOrder, orderParams.Page, (orderParams.Page - 1) * orderParams.PageSize, orderParams.PageSize + 1);
            }

            return new OrderedArticlesByLocation();
        }

        [HttpGet]
        [Route("audit/{id}/{page}/{pageSize}")]
        public async Task<IList<AuditLog>> Audit(int id, int page, int pageSize)
        {
            var storedContent = await _contentRepository.GetById(id, CurrentUser.Email);

            Guard.Against.RecordNotFound(storedContent);

            var dbAuditLogs = await _auditLogRepository.GetLatestForEntity(
                    storedContent.Id,
                    ContentHelpers.MapContentTypeToEntity(storedContent.Type),
                    page,
                    pageSize);

            return dbAuditLogs.Select(_auditLogMappers.MapToAuditLog).ToList();
        }

        [HttpDelete]
        [Route("{code}/removeTopic/{topic}")]
        public async Task<bool> RemoveTopic(string code, string topic)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(topic) || !Topics.IsValidTopic(topic))
                {
                    return false;
                }

                var lowerCaseTopic = topic.ToLower();

                var content = await _contentService.GetContentByCodeAsync(code, CurrentUser);
                await _articleOrderService.DeleteTopicFromContentOrders(content, lowerCaseTopic);

                content.Topics = content.Topics?.Where(x => x?.ToLower() != lowerCaseTopic).ToList();
                await _contentService.UpdateAsync(content, CurrentUser);
                return true;
            }
            catch (System.Exception e)
            {
                throw;
            }
        }

        [HttpPost]
        [Route("switchToServiceAreaNewsfeed")]
        public async Task<bool> SwitchToServiceAreaNewsfeed([FromBody] ArticleOrderParams orderParams)
        {
            if (orderParams != null)
            {
                await _articleOrderService.SwitchToServiceAreaNewsfeed(orderParams);
            }

            return true;
        }

        [HttpPost]
        [Route("switchToCountryNewsfeed")]
        public async Task<bool> SwitchToCountryNewsfeed([FromBody] ArticleOrderParams orderParams)
        {
            if (orderParams != null)
            {
                await _articleOrderService.SwitchToCountryNewsfeed(orderParams);
            }

            return true;
        }

        [HttpPost]
        [Route("clearNewsfeedOverride")]
        public async Task<bool> ClearNewsfeedOverride([FromBody] ArticleOrderParams orderParams)
        {
            if (orderParams != null)
            {
                await _articleOrderService.ClearNewsfeedOverride(orderParams);
            }

            return true;
        }
    }
}
