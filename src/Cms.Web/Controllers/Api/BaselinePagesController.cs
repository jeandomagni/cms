﻿using Cms.Domain.Exception;
using Cms.Domain.Mappers;
using Cms.Model.Models.BaselinePages;
using Cms.Model.Models.Config;
using Cms.Model.Models.Query.DataTable;
using Cms.Model.Repositories;
using Cms.Model.Repositories.Content;
using Cms.Web.Filters;
using Cms.Web.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cms.Web.Controllers.Api
{
    [Route("api/[controller]")]
    public class BaselinePagesController : SecureBaseController
    {
        private readonly IBaselinePagesService _baselinePagesService;
        private readonly IContentService _contentService;
        private readonly IPinnedContentLocationRepository pinnedContentLocationRepository;
        private readonly General _generalConfig;

        public BaselinePagesController(IBaselinePagesService baselinePagesService, IContentService contentService, IContentRepository contentRepository, IPinnedContentLocationRepository pinnedContentLocationRepository, IOptions<General> generalConfig)
        {
            _baselinePagesService = baselinePagesService;
            this._contentService = contentService;
            this.pinnedContentLocationRepository = pinnedContentLocationRepository;
            _generalConfig = generalConfig.Value;
        }

        [HttpGet]
        [TypeFilter(typeof(ApiAuthorizationHeader))]
        public async Task<List<BaselinePage>> GetAll()
        {
            return await _baselinePagesService.GetAll();
        }

        [HttpPut]
        public async Task<BaselinePage> Put([FromBody] BaselinePage model)
        {
            if (model.Id == 0)
            {
                var content = await _contentService.CreateAsync(new Cms.Model.Models.Content.Content { Title = model.Title, Layout = model.Layout, ModifiedDate = model.ModifiedDate, UriSegment = model.UrlSegment, ArticleUrl = new Cms.Model.Models.Content.Entities.ArticleUrl { Segment = model.UrlSegment }, Type = "BaselinePage" }, CurrentUser);
                model.ContentCode = content.Code;
                await _baselinePagesService.CreateBaselinePage(model);
            }
            else
            {
                await _baselinePagesService.UpdateBaselinePage(model);
            }

            return model;
        }

        [HttpPost]
        [Route("paginate")]
        public Task<IEnumerable<BaselinePage>> Paginate([FromBody] Query query)
        {
            var options = QueryMappers.MapToGridOptions(query);
            return _baselinePagesService.PaginateBaselinePages(options, false);
        }

        [HttpDelete("{id}")]
        public async Task<OkResult> Delete(int id)
        {
            var baselinePage = await _baselinePagesService.GetById(id);

            if (baselinePage == null)
            {
                throw new RecordNotFoundException();
            }

            if (!string.IsNullOrWhiteSpace(baselinePage.ContentCode))
            {
                await _contentService.DeleteByCode(baselinePage.ContentCode, CurrentUser, _generalConfig.EnableSoftDelete);
            }

            return Ok();
        }

        [HttpPost("{baselinePageId}/isSegmentUrlExists")]
        public async Task<bool> IsSegmentUrlExists(int? baselinePageId, [FromBody] IsSegmentUrlExistsModel model)
        {
            var result = await _baselinePagesService.IsSegmentUrlExists(model.UrlSegment, baselinePageId);
            return result;
        }
    }
}
