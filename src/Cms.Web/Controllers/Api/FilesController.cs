﻿using Ardalis.GuardClauses;
using Cms.Domain.ContentGuardClauses;
using Cms.Domain.Enum;
using Cms.Domain.Exception;
using Cms.Domain.Mappers;
using Cms.Domain.Services.Solr;
using Cms.Model.Enum;
using Cms.Model.Models.Config;
using Cms.Model.Models.Query.KendoGrid;
using Cms.Model.Repositories;
using Cms.Model.Repositories.Content;
using Cms.Web.Attributes;
using Cms.Web.Services;
using Cms.Web.Util;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.Net.Http.Headers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Cms.Web.Controllers.Api
{
    [Route("api/[controller]")]
    public class FilesController: SecureBaseController
    {
        private readonly IFileRepository _fileRepository;
        private readonly IUserProfileRepository _userProfileRepository;
        private readonly MediaSettings _mediaSettings;
        private readonly ITagService _tagService;
        private readonly IContentRepository _contentRepository;
        private readonly ISolrContentService _solrService;
        private readonly IAuditLogRepository _auditLogRepository;

        public FilesController(
            IFileRepository fileRepository,
            IUserProfileRepository userProfileRepository,
            IOptions<MediaSettings> config,
            ITagService tagService,
            IContentRepository contentRepository,
            ISolrContentService solrService,
            IAuditLogRepository auditLogRepository) {
            _fileRepository = fileRepository;
            _userProfileRepository = userProfileRepository;
            _mediaSettings = config.Value;
            _tagService = tagService;
            _contentRepository = contentRepository;
            _solrService = solrService;
            _auditLogRepository = auditLogRepository;
        }

        [RequestFormSizeLimit(valueCountLimit: 3000000)]
        public async Task<ActionResult> Post()
        {
            const int MAX_UPLOAD_SIZE_IN_KB = 400;
            foreach (var file in Request.Form.Files)
            {
                var fileSizeInKb = (int)(file.Length / 1024);
                if (fileSizeInKb > MAX_UPLOAD_SIZE_IN_KB)
                {
                    throw new MaxFileSizeException(file.FileName, MAX_UPLOAD_SIZE_IN_KB);
                }

                string contentCode = Request.Form["contentCode"];
                if (!string.IsNullOrWhiteSpace(contentCode))
                {
                    var content = await _contentRepository.GetByCode(contentCode, CurrentUser.Email);
                    if (content != null)
                    {
                        Guard.Against.InvalidRequestor(content, CurrentUser);
                    }
                }

                var filename = ContentDispositionHeaderValue
                                .Parse(file.ContentDisposition)
                                .FileName.ToString().Trim('"');

                var code = Guid.NewGuid().ToString();
                var ext = Path.GetExtension(filename)?.ToLower();

                var savePath = Path.Combine(_mediaSettings.FileUploadPath, code + ext);
                var url = _mediaSettings.BaseUrl + code + ext;

                var allowedExtentions = new[] { "jpg", "png", "jpeg", "gif" };
                if (ext != null && !allowedExtentions.Contains(ext.Trim('.')))
                {
                    throw new InvalidFileExtException(ext);
                }

                ImageUtil.SaveAsOptimizedImage(file.OpenReadStream(), savePath);

                var userProfile =
                    await _userProfileRepository.GetForUser(CurrentUser.UserId).ConfigureAwait(false);

                var addedFile = await _fileRepository.Add(
                     new Cms.Model.Models.File
                     {
                         CreatedBy = CurrentUser.Email,
                         Title = (string)Request.Form["imageTitle"] ?? string.Empty,
                         Code = code,
                         Path = savePath,
                         Uri = url,
                         ContentType = file.ContentType,
                         OwnerSiteId = userProfile.DefaultSite,
                         Type = MediaType.ContentImage,
                         Tags = Request.Form["tags"],
                         FileSizeKb = fileSizeInKb,
                         Caption = Request.Form["imageCaption"],
                         Credit = Request.Form["imageCredit"]
                     }).ConfigureAwait(false);

                await _auditLogRepository.Add(
                    CurrentUser.Email, addedFile.Id, EntityCodes.FILE, EntityActions.CREATE).ConfigureAwait(false);

                return Created(savePath, new { file, code = code + ext, uri = url });
            }

            return BadRequest();
        }

        [HttpPost]
        [Route("paginate")]
        public Task<IEnumerable<Cms.Model.Models.File>> Paginate([FromBody] GridOptions options)
        {
            return _fileRepository.Paginate(options, CurrentUser.Email);
        }

        [HttpGet]
        [Route("{searchText}/{limit}")]
        public Task<IEnumerable<Cms.Model.Models.File>> Query(string searchText, int limit)
        {
            return
                _fileRepository.Query(searchText, limit > 50 ? 50 : limit);
        }

        [HttpPost("update")]
        public async Task<OkResult> Update([FromBody] Cms.Model.Models.File file)
        {
            var storedFile = await _fileRepository.GetByCode(file.Code).ConfigureAwait(false);

            if (storedFile == null)
                throw new RecordNotFoundException();

            if (!CurrentUser.HasAccessToRecord(storedFile.CreatedBy, null))
                throw new InvalidRequestorException();

            if (string.IsNullOrWhiteSpace(file.Title))
                throw new TitleRequiredException();

            if (string.IsNullOrWhiteSpace(file.Credit))
                throw new CreditRequiredException();

            storedFile.Title = file.Title;
            storedFile.Tags = file.Tags;
            storedFile.Published = file.Published;
            storedFile.Caption = file.Caption;
            storedFile.Credit = file.Credit;

            await _tagService.UpdateTags(
                file.TagItems?.Where(x => x.Type == "generic").Select(
                    x => x.Value), CurrentUser.UserId).ConfigureAwait(false);

            await _fileRepository.Update(storedFile).ConfigureAwait(false);

            if (storedFile.Published)
            {
                Publish(storedFile);
            }
            else
            {
                Unpublish(storedFile);
            }

            await _auditLogRepository.Add(
                CurrentUser.Email, storedFile.Id, EntityCodes.FILE, EntityActions.UPDATE).ConfigureAwait(false);

            return Ok();
        }

        [HttpDelete("{code}")]
        public async Task<OkResult> Delete(string code)
        {
            var storedFile = await _fileRepository.GetByCode(code).ConfigureAwait(false);

            if (storedFile == null)
                throw new RecordNotFoundException();

            if (!CurrentUser.HasAccessToRecord(storedFile.CreatedBy, string.Empty))
                throw new InvalidRequestorException();

            Unpublish(storedFile);

            await _fileRepository.Delete(storedFile.Code).ConfigureAwait(false);

            var fileUploadDirectory = new DirectoryInfo(_mediaSettings.FileUploadPath);

            var filesToDelete = fileUploadDirectory.GetFiles($"{code}.*");
            foreach (var fileToDelete in filesToDelete)
            {
                try
                {
                    fileToDelete.Delete();
                }
                catch { }
            }

            await _auditLogRepository.Add(
                CurrentUser.Email, storedFile.Id, EntityCodes.FILE, EntityActions.DELETE).ConfigureAwait(false);

            return Ok();
        }

        private async void Publish(Cms.Model.Models.File file)
        {
            var userEmail = CurrentUser.Email;

            var dbContent = await _contentRepository.GetByCode(file.Code, userEmail).ConfigureAwait(false);

            if (dbContent == null)
            {
                dbContent = FileMappers.MapToDbContent(file, CurrentUser);

                dbContent = await _contentRepository.Add(dbContent).ConfigureAwait(false);

                await _auditLogRepository.Add(
                    userEmail, dbContent.Id, EntityCodes.ARTICLE, EntityActions.CREATE).ConfigureAwait(false);
            }
            else
            {
                dbContent.ModifiedDate = DateTimeOffset.Now;
                dbContent.PublishDate = DateTimeOffset.Now;
                dbContent.Status = ContentStatus.Published;
                dbContent.Tags = file.TagItems != null ?  string.Join(",", file.TagItems.Select(x => x.Value)) : null;

                dbContent = await _contentRepository.Update(dbContent).ConfigureAwait(false);

                await _auditLogRepository.Add(
                    userEmail, dbContent.Id, EntityCodes.ARTICLE, EntityActions.UPDATE).ConfigureAwait(false);
            }
            
            await _solrService.PostContent(dbContent).ConfigureAwait(false);
        }

        private async void Unpublish(Cms.Model.Models.File file)
        {
            var content = await _contentRepository.GetByCode(file.Code, CurrentUser.Email).ConfigureAwait(false);

            if (content == null) return;

            await _contentRepository.Delete(content.Id).ConfigureAwait(false);

            await _auditLogRepository.Add(
                CurrentUser.Email, content.Id, EntityCodes.ARTICLE, EntityActions.DELETE).ConfigureAwait(false);

            await _solrService.Delete(content.Id.ToString()).ConfigureAwait(false);
        }
    }
}
