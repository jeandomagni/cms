﻿using Ardalis.GuardClauses;
using Cms.Domain.Enum;
using Cms.Domain.Exception;
using Cms.Domain.GenericGuardClauses;
using Cms.Domain.Mappers;
using Cms.Model.Enum;
using Cms.Model.Models.Content;
using Cms.Model.Repositories;
using Cms.Model.Repositories.Content;
using Cms.Web.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cms.Web.Controllers.Api
{
    [Route("api/[controller]")]
    public class ContentEventController : SecureBaseController
    {
        private readonly IContentAuditLogMappers _auditLogMappers;
        private readonly IContentRepository _contentRepository;
        private readonly IContentEventRepository _contentEventRepository;
        private readonly IAuditLogRepository _auditLogRepository;
        private readonly IContentEventRecurrenceService _contentEventRecurrenceService;
        private readonly IContentEventMappers _contentEventMappers;
        private readonly IContentMappers _contentMappers;

        public ContentEventController(
            IContentAuditLogMappers auditLogMappers,
            IContentRepository contentRepository,
            IContentEventRepository contentEventRepository,
            IAuditLogRepository auditLogRepository,
            IContentEventRecurrenceService contentEventRecurrenceService,
            IContentEventMappers contentEventMappers,
            IContentMappers contentMappers) {
            _auditLogMappers = auditLogMappers;
            _contentEventRepository = contentEventRepository;
            _contentRepository = contentRepository;
            _auditLogRepository = auditLogRepository;
            _contentEventRecurrenceService = contentEventRecurrenceService;
            _contentEventMappers = contentEventMappers;
            _contentMappers = contentMappers;
        }

        [HttpGet]
        [Route("content/{contentCode}")]
        public async Task<ContentEvent> GetForContentByCode(string contentCode)
        {
            var dbContentEvent = await _contentEventRepository.GetForContentByCode(contentCode);
            return _contentEventMappers.MapToContentEvent(dbContentEvent);
        }

        [HttpPut]
        public async Task<ContentEvent> Put([FromBody] ContentEvent contentEvent)
        {
            var content =
                await _contentRepository.GetByCode(
                    contentEvent.ContentCode, CurrentUser.Email);

            if (content == null)  {
                throw new RecordNotFoundException();
            }

            if (!CurrentUser.HasAccessToRecord(content.CreatedBy, content.OwnerSiteId))  {
                throw new InvalidRequestorException();
            }

            contentEvent.RegistrationLink = PrependUrl(contentEvent.RegistrationLink);

            var dbContentEvent = _contentEventMappers.MapToDbContentEvent(contentEvent);
            var createdDbEvent = await _contentEventRepository.Add(dbContentEvent);

            await _auditLogRepository.Add(
                CurrentUser.Email, content.Id, EntityCodes.EVENT, EntityActions.CREATE);

            return _contentEventMappers.MapToContentEvent(createdDbEvent);
        }

        [HttpPost]
        public async Task<ContentEvent> Post([FromBody] ContentEvent contentEvent)
        {
            var content = await _contentRepository.GetByCode(
                    contentEvent.ContentCode, CurrentUser.Email);

            if (content == null) {
                throw new RecordNotFoundException();
            }

            if (!CurrentUser.HasAccessToRecord(
                content.CreatedBy, content.OwnerSiteId)) {
                throw new InvalidRequestorException();
            }

            var storedDbContentEvent = await _contentEventRepository.GetById(contentEvent.Id);
            var dbContentEvent = _contentEventMappers.MapToDbContentEvent(contentEvent);

            if (storedDbContentEvent.Recurrence != dbContentEvent.Recurrence)
            {
                await _contentEventRecurrenceService.CreateOccurrences(contentEvent);
            }

            if (content.Status != ContentStatus.Draft)
            {
                var audits = _auditLogMappers.MapToDbAuditLogs(
                    storedDbContentEvent,
                    dbContentEvent,
                    content.Id,
                    CurrentUser.Email);
                await _auditLogRepository.AddMany(audits);
            }

            if (!string.IsNullOrEmpty(contentEvent.RegistrationLink)) {
                contentEvent.RegistrationLink = PrependUrl(contentEvent.RegistrationLink);
                Guard.Against.InvalidUrl(contentEvent.RegistrationLink, "Invalid event registration link provided.");
            }

            var updatedDbEvent = await _contentEventRepository.Update(dbContentEvent);

            return _contentEventMappers.MapToContentEvent(updatedDbEvent);
        }

        [HttpDelete("{contentCode}/{id}")]
        public async Task<OkResult> Delete(string contentCode, int id)
        {
            var storedContent = 
                await _contentRepository.GetByCode(
                    contentCode, CurrentUser.Email);

            if (storedContent == null)
                throw new RecordNotFoundException();

            if (!CurrentUser.HasAccessToRecord(
                storedContent.CreatedBy, storedContent.OwnerSiteId))
                throw new InvalidRequestorException();

            await _contentEventRepository.Delete(id);

            await _auditLogRepository.Add(
                CurrentUser.Email, storedContent.Id, EntityCodes.EVENT, EntityActions.DELETE);

            return Ok();
        }


        private static string PrependUrl(string url)
        {
            if (string.IsNullOrEmpty(url))
            {
                return url;
            }
            if (!url.StartsWith("http://") && !url.StartsWith("https://"))
            {
                return $"http://{url}";
            }
            return url;
        }
    }
}
