﻿using Cms.Domain.Enum;
using Cms.Domain.Exception;
using Cms.Domain.Extensions;
using Cms.Domain.Services.Solr;
using Cms.Model.Models;
using Cms.Model.Models.Content.Db;
using Cms.Model.Models.Query;
using Cms.Model.Repositories;
using Cms.Model.Repositories.Content;
using Cms.Web.Services;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SolrLocation = Bbb.Core.Solr.Model.Location;

namespace Cms.Web.Controllers.Api
{
    [Route("api/[controller]")]
    public class EntityController : SecureBaseController
    {
        private readonly IEntityRepository _entityRepository;
        private readonly IContentRepository _contentRepository;
        private readonly IContentService _contentService;
        private readonly ISolrContentService _solrService;
        private readonly IContentMediaRepository _contentMediaRepository;
        private readonly IUserProfileRepository _userProfileRepository;
        private readonly IServiceAreaService _serviceAreaService;

        public EntityController(
            IEntityRepository entityRepository,
            IContentRepository contentRepository,
            IContentService contentService,
            ISolrContentService solrService,
            IContentMediaRepository contentMediaRepository,
            IUserProfileRepository userProfileRepository,
            IContentEventRepository contentEventRepository,
            IServiceAreaService serviceAreaService) {
            _entityRepository = entityRepository;
            _contentRepository = contentRepository;
            _contentMediaRepository = contentMediaRepository;
            _contentService = contentService;
            _userProfileRepository = userProfileRepository;
            _solrService = solrService;
            _serviceAreaService = serviceAreaService;
        }

        [HttpPost]
        [Route("query")]
        public async Task<IEnumerable<string>> Query([FromBody] EntityQuery entity)
        {
            var items =
                   await _entityRepository.Query(
                       entity.Query, entity.Type, entity.ResultLimit);

            return items?.Select(i => i.Data);
        }

        [HttpPut]
        public async Task<bool> Put([FromBody] Entity entity)
        {
            return
                await _entityRepository.Add(
                    entity.Type, entity.Data);
        }

        [HttpDelete]
        [Route("{code}")]
        public async Task<bool> Delete(string code)
        {
            var existing =
                await _entityRepository.GetByCode(code);

            if (existing == null) {
                throw new RecordNotFoundException();
            }

            return
                await _entityRepository.Delete(code) > 0;
        }

        [HttpPost]
        [Route("content/{id}/{type}")]
        public async Task<int> UpdateContent([FromBody] GenericEntity entities, int id, string type)
        {
            var contentRecord =
                await _contentRepository.GetById(
                    id, CurrentUser.Email);

            if (contentRecord == null)
                throw new RecordNotFoundException();

            var userIsInServiceArea = await IsInServiceArea(entities, contentRecord, type);
            var userHasUpdateAccess = CurrentUser.HasAccessToRecord(
                contentRecord.CreatedBy, contentRecord.OwnerSiteId);

            if (!userIsInServiceArea && (!userHasUpdateAccess && type != "sites"))
                throw new InvalidRequestorException();

            var updated =
                await _entityRepository.UpdateContentEntities(
                    id, type, entities);

            if (type == "sites") {
                if (contentRecord.Sites != entities.Data)
                {
                    var userProfile = 
                        await _userProfileRepository.GetForUser(CurrentUser.UserId);

                    string[] sites = {
                        userProfile.DefaultSite
                    };

                    if (!string.IsNullOrWhiteSpace(entities.Data)) {
                        sites =
                           JsonConvert.DeserializeObject<IEnumerable<LegacyBbbInfo>>(
                               entities.Data).Select(site => site.LegacyBBBID).ToArray();

                    }

                    await _contentService.UpdateContentSites(
                        contentRecord.Code, sites);
                }
            }

            var updatedContentRecord = 
                await _contentRepository.GetById(
                    id, CurrentUser.Email);

            var contentMedia =
                await _contentMediaRepository.GetByContentCode(
                    updatedContentRecord.Code);

            if (contentMedia != null && contentMedia.Any()) {
                updatedContentRecord.Image = 
                    JsonConvert.SerializeObject(contentMedia);
            }

            await _solrService.PostContent(updatedContentRecord);

            return updated;
        }

        private async Task<bool> IsInServiceArea(GenericEntity entities, DbContent content, string type)
        {
            if (CurrentUser.GlobalOrLocalAdmin)
            {
                return true;
            }

            switch (type) {
                case "pinnedCountries":
                    var existingPinnedCountries = content.PinnedStates.DeserializeOrDefault(new List<string>());
                    var newPinnedCountries = entities.Data.DeserializeOrDefault(new List<string>());
                    var countryToUpdate = existingPinnedCountries.Except(newPinnedCountries).Concat(newPinnedCountries.Except(existingPinnedCountries)).FirstOrDefault();
                    return await _serviceAreaService.IsServiceableCountryForUser(countryToUpdate, CurrentUser);
                case "pinnedStates":
                    var existingPinnedStates = content.PinnedStates.DeserializeOrDefault(new List<string>());
                    var newPinnedStates = entities.Data.DeserializeOrDefault(new List<string>());
                    var stateToUpdate = existingPinnedStates.Except(newPinnedStates).Concat(newPinnedStates.Except(existingPinnedStates)).FirstOrDefault();
                    return await _serviceAreaService.IsServiceableStateForUser(stateToUpdate, CurrentUser);
                case "pinnedCities":
                    var existingPinnedCities = content.PinnedCities.DeserializeOrDefault(new List<SolrLocation>());
                    var newPinnedCities = entities.Data.DeserializeOrDefault(new List<SolrLocation>());
                    var cityToUpdate = existingPinnedCities.Except(newPinnedCities).Concat(newPinnedCities.Except(existingPinnedCities)).FirstOrDefault();
                    return await _serviceAreaService.IsServiceableCityForUser(cityToUpdate, CurrentUser);
                default:
                    return true;
            }
        }
    }
}
