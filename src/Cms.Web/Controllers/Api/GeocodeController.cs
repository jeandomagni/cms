﻿using Cms.Model.Models;
using Cms.Model.Models.Config;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System.Linq;

namespace Cms.Web.Controllers.Api
{
    [Produces("application/json")]
    [Route("api/geocode")]
    public class GeocodeController : SecureBaseController
    {
        private readonly General _generalConfig;

        public GeocodeController(IOptions<General> generalConfig)
        {
            _generalConfig = generalConfig.Value;
        }

        [HttpGet]
        public GoogleLocation GeoCode(string address)
        {
            using (var webClient = new System.Net.WebClient())
            {
                var url = $"https://maps.googleapis.com/maps/api/geocode/json?address={address}&key={_generalConfig.GoogleMapsKey}";
                var json = webClient.DownloadString(url);
                var results = JsonConvert.DeserializeObject<GoogleGeocodeInfo>(json);
                return results?.Results?.FirstOrDefault()?.Geometry?.Location;
            }
        }
    }
}