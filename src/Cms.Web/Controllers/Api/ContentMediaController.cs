﻿using Cms.Domain.Enum;
using Cms.Domain.Exception;
using Cms.Model.Enum;
using Cms.Model.Models.Content;
using Cms.Model.Models.Query;
using Cms.Model.Repositories;
using Cms.Model.Repositories.Content;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cms.Web.Controllers.Api
{
    [Route("api/[controller]")]
    public class ContentMediaController: SecureBaseController
    {
        private readonly IContentRepository _contentRepository;
        private readonly IContentMediaRepository _contentMediaRepository;
        private readonly IAuditLogRepository _auditLogRepository;

        public ContentMediaController(
            IContentRepository contentRepository,
            IContentMediaRepository contentMediaRepository,
            IAuditLogRepository auditLogRepository) {
            _contentMediaRepository = contentMediaRepository;
            _contentRepository = contentRepository;
            _auditLogRepository = auditLogRepository;
        }

        [HttpGet]
        [Route("contentcode/{contentCode}")]
        public async Task<IEnumerable<ContentMedia>> GetForContentByCode(string contentCode)
        {
            return 
                await _contentMediaRepository.GetByContentCode(contentCode);
        }

        [HttpPut]
        public async Task<ContentMedia> Put([FromBody] ContentMedia contentMedia)
        {
            var content = 
                await _contentRepository.GetByCode(
                    contentMedia.ContentCode, CurrentUser.Email);

            if (content == null) {
                throw new RecordNotFoundException();
            }

            if (!CurrentUser.HasAccessToRecord(
                content.CreatedBy, content.OwnerSiteId)) {
                throw new InvalidRequestorException();
            }

            var existingContentmedia =
                await _contentMediaRepository.GetByContentCode(content.Code);

            var order = 1;
            var contentMediae = existingContentmedia as ContentMedia[] ?? existingContentmedia.ToArray();
            if (contentMediae.Any()) {
                var carouselMedia = contentMediae.Where(cm => !cm.MainImage);
                var enumerable = carouselMedia as ContentMedia[] ?? carouselMedia.ToArray();
                if (!enumerable.Any()) {
                    order = 1;
                }
                else {
                    order = enumerable.Select(cm => cm.Order).Max() + 1;
                }
                if (contentMedia.MainImage) {
                    order = 0;
                }
            }

            contentMedia.Order = order;

            var updatedContentMedia =  
                await _contentMediaRepository.Add(contentMedia);

            await _auditLogRepository.Add(CurrentUser.Email, content.Id,
            getEntityFromType(content.Type), EntityActions.UPDATE_IMAGE);

            return updatedContentMedia;
        }

        [HttpPost]
        [Route("{contentCode}")]
        public async Task<OkResult> ReOrder([FromBody] IEnumerable<ContentMedia> contentMediaList, string contentCode)
        {
            var storedContent = 
                await _contentRepository.GetByCode(
                    contentCode, CurrentUser.Email);

            if (storedContent == null)
                throw new RecordNotFoundException();

            if (!CurrentUser.HasAccessToRecord(
                storedContent.CreatedBy, storedContent.OwnerSiteId))
                throw new InvalidRequestorException();

            await 
                _contentMediaRepository.ReOrder(
                    contentCode, OrderFromMediaList(contentMediaList));

            await _auditLogRepository.Add(CurrentUser.Email, storedContent.Id,
                        getEntityFromType(storedContent.Type), EntityActions.UPDATE_IMAGE);

            return Ok();
        }

        [HttpPost]
        [Route("update")]
        public async Task<OkResult> Update([FromBody] ContentMedia contentMedia)
        {
            await _contentMediaRepository.Update(contentMedia);
            return Ok();
        }

        [HttpDelete("{contentCode}/{id}")]
        public async Task<OkResult> Delete(string contentCode, int id)
        {
            var storedContent = 
                await _contentRepository.GetByCode(contentCode, CurrentUser.Email);

            if (storedContent == null)
                throw new RecordNotFoundException();

            if (!CurrentUser.HasAccessToRecord(
                storedContent.CreatedBy, storedContent.OwnerSiteId))
                throw new InvalidRequestorException();

            await _contentMediaRepository.Delete(id);

            var existingContentmedia =
                (await _contentMediaRepository.GetByContentCode(contentCode));

            await _auditLogRepository.Add(CurrentUser.Email, storedContent.Id,
                        getEntityFromType(storedContent.Type), EntityActions.UPDATE_IMAGE);

            await
                _contentMediaRepository.ReOrder(contentCode, OrderFromMediaList(existingContentmedia));

            return Ok();
        }

        private IEnumerable<ContentMediaOrder> OrderFromMediaList(IEnumerable<ContentMedia> contentMediaList)
        {
            var ret = new List<ContentMediaOrder>();
            var order = 1;
            foreach (var contentMedia in contentMediaList.OrderBy(cm => cm.Order)) {
                if (contentMedia.MainImage)
                    continue;

                ret.Add(new ContentMediaOrder {
                    ContentMediaId = contentMedia.Id, Order = order
                });
                order++;
            }
            return ret;
        }

        private string getEntityFromType(string type)
        {
            switch (type)
            {
                case ContentType.Article:
                    return EntityCodes.ARTICLE;
                case ContentType.Event:
                    return EntityCodes.EVENT;
                case ContentType.NearMe:
                    return EntityCodes.NEARME;
                case ContentType.AccreditedBusinessList:
                    return EntityCodes.ABLIST;
                case ContentType.BaselinePage:
                    return EntityCodes.BLPAGE;
            }
            return null;
        }
    }

}
