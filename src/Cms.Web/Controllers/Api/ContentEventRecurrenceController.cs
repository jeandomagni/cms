﻿using Cms.Domain.Mappers;
using Cms.Model.Models.Content;
using Cms.Model.Models.Recurrence;
using Cms.Model.Repositories.Content;
using Cms.Web.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Cms.Web.Controllers.Api
{
    [Route("api/[controller]")]
    public class ContentEventRecurrenceController : SecureBaseController
    {
        private readonly IContentEventRecurrenceService _contentEventRecurrenceService;
        private readonly IContentEventRepository _contentEventRepository;
        private readonly IContentEventMappers _contentEventMappers;

        public ContentEventRecurrenceController(
            IContentEventRecurrenceService contentEventRecurrenceService,
            IContentEventRepository contentEventRepository,
            IContentEventMappers contentEventMappers)
        {
            _contentEventRecurrenceService = contentEventRecurrenceService;
            _contentEventRepository = contentEventRepository;
            _contentEventMappers = contentEventMappers;
        }

        [HttpPost]
        [Route("validate")]
        public string Validate([FromBody] EventRecurrence eventRecurrence)
        {
            return _contentEventRecurrenceService.Validate(eventRecurrence);
        }


        [HttpDelete("{id}")]
        public async Task<OkResult> Delete(int id)
        {
            await _contentEventRecurrenceService.DeleteOccurrence(id, CurrentUser);
            return Ok();
        }

        [HttpDelete("byCode/{contentCode}")]
        public async Task<ContentEvent> DeleteAllForContentCode(string contentCode)
        {
            await _contentEventRecurrenceService.DeleteAllForContentCode(contentCode, CurrentUser);

            var dbContentEventToUpdate = await _contentEventRepository.GetForContentByCode(contentCode);
            dbContentEventToUpdate.Recurrence = null;
            return _contentEventMappers.MapToContentEvent(await _contentEventRepository.Update(dbContentEventToUpdate));
        }
    }
}
