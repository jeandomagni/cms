﻿using Cms.Model.Models.Config;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Cms.Web.Controllers.Api
{
    [Route("api/[controller]")]
    public class AppSettingsController : Controller
    {
        private readonly ConnectionStrings _config;
        private readonly General _generalConfig;
        public AppSettingsController(IOptions<ConnectionStrings> config, IOptions<General> generalConfig) {
            _config = config.Value;
            _generalConfig = generalConfig.Value;
        }
        
        [HttpGet]
        [Route("connectionString/{key}")]
        public IActionResult GetConnectionString(string key)
        {
            var connectionString = _config.GetType().GetProperty(key).GetValue(_config, null);
            return Ok(new { connectionString });
        }

        [HttpGet]
        [Route("generalSetting/{key}")]
        public IActionResult GetGeneralSetting(string key)
        {
            var setting = _generalConfig.GetType().GetProperty(key).GetValue(_generalConfig, null);
            return Ok(new { setting });
        }
    }
}
