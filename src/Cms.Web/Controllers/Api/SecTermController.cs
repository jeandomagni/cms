﻿using Ardalis.GuardClauses;
using Cms.Domain.Mappers;
using Cms.Domain.SecTermGuardClauses;
using Cms.Model.Models.Query.KendoGrid;
using Cms.Model.Models.SecTerm;
using Cms.Model.Repositories.SecTerms;
using Cms.Web.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Cms.Web.Controllers.Api
{
    [Route("api/[controller]")]
    public class SecTermController : SecureBaseController
    {
        private readonly ICoreSecTermMappers _coreSecTermMappers;
        private readonly ISecTermRepository _secTermRepository;
        private readonly ISecTermService _secTermService;

        public SecTermController(
            ICoreSecTermMappers coreSecTermMappers,
            ISecTermService secTermService,
            ISecTermRepository secTermRepository) {
            _coreSecTermMappers = coreSecTermMappers;
            _secTermRepository = secTermRepository;
            _secTermService = secTermService;
        }
        
        [HttpPost]
        [Route("paginate")]
        public async Task<SecTermResults> Paginate([FromBody] GridOptions options)
        {
            if (options.data.filter == null) options.data.filter = KendoFilter.Empty;
            var paginated = await _secTermRepository.Paginate(options);
            return _coreSecTermMappers.MapToSecTermResults(paginated);
        }

        [HttpGet("{tobId}")]
        public async Task<SecTerm> Get(string tobId)
        {
            var coreSec = await _secTermRepository.GetByTobId(tobId);
            return _coreSecTermMappers.MapToSecTerm(coreSec);
        }
        
        [HttpPost]
        [Route("update")]
        public async Task<SecTerm> Update([FromBody] SecTerm secTerm)
        {
            Guard.Against.EmptyMetadata(secTerm.Metadata);
            
            secTerm.ModifiedDate = DateTime.Now;
            secTerm.ModifiedUser = CurrentUser.Email;
            return await _secTermService.Update(secTerm);
        }
    }
}
