﻿using Cms.Domain.Enum;
using Cms.Domain.Exception;
using Cms.Model.Enum;
using Cms.Model.Repositories;
using Cms.Model.Repositories.Content;
using Cms.Web.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cms.Web.Controllers.Api
{
    [Route("api/[controller]")]
    public class TagController : SecureBaseController
    {
        private readonly ITagRepository _tagRepository;
        private readonly IContentRepository _contentRepository;
        private readonly ITagService _tagService;
        private readonly IAuditLogRepository _auditLogRepository;
        public TagController(
            ITagRepository tagRepository,
            IContentRepository contentRepository,
            ITagService tagService,
            IAuditLogRepository auditLogRepository) {
            _tagRepository = tagRepository;
            _contentRepository = contentRepository;
            _tagService = tagService;
            _auditLogRepository = auditLogRepository;
        }

        [HttpGet]
        [Route("query/{tag}")]
        public async Task<IEnumerable<string>> Query(string tag)
        { 
            return 
                await 
                _tagRepository.Query(tag, CurrentUser.UserId);
        }

        [HttpPost]
        [Route("content/{contentId}")]
        public async Task<int> UpdateContentTags(int contentId, [FromBody] IEnumerable<string> tags)
        {
            var contentStore = 
                await _contentRepository.GetById(
                    contentId, CurrentUser.Email);

            if (contentStore == null)
                throw new RecordNotFoundException();

            if (!CurrentUser.HasAccessToRecord(
                contentStore.CreatedBy, contentStore.OwnerSiteId))
                throw new InvalidRequestorException();

            await _tagService.UpdateTags(
                tags, CurrentUser.UserId);

            var updatedContentTags = 
                await _tagRepository.UpdateContentTags(contentId, tags);

            await _auditLogRepository.Add(
                CurrentUser.Email, contentId, EntityCodes.ARTICLE, EntityActions.UPDATE_TAGS);

            return updatedContentTags;
        }
    }
}
