﻿using Cms.Domain.Handlers;
using Cms.Model.Constants;
using Cms.Model.Models;
using Cms.Model.Models.Config;
using Cms.Model.Repositories;
using Cms.Web.Model.Security;
using Cms.Web.Services.Email;
using Cms.Web.Services.Sms;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Linq;
using System.Threading.Tasks;
using Cms.Domain.Contracts;

namespace Cms.Web.Controllers.Api
{
    [Route("api/[controller]")]
    public class AccountController : Controller
    {
        private readonly IUserProfileRepository _userProfileRepository;
        private readonly IEmailLoginService _emailLoginService;
        private readonly IVerificationRequestRepository _verificationRequestRepository;
        private readonly IAuthenticationHandler _authenticationHandler;
        private readonly IVerificationClientRepository _verificationClientRepository;
        private readonly ITwilioService _twilioService;
        private readonly ConnectionStrings _config;

        public AccountController(
            IUserRepository userRepository,
            IOptions<ConnectionStrings> config,
            IUserProfileRepository userProfileRepository,
            IEmailLoginService emailLoginService,
            IVerificationRequestRepository verificationRequestRepository,
            IAuthenticationHandler authenticationHandler,
            IVerificationClientRepository verificationClientRepository,
            ITwilioService twilioService) {
            _userProfileRepository = userProfileRepository;
            _emailLoginService = emailLoginService;
            _verificationRequestRepository = verificationRequestRepository;
            _authenticationHandler = authenticationHandler;
            _verificationClientRepository = verificationClientRepository;
            _twilioService = twilioService;
            _config = config.Value;
        }
        
        [HttpPut]
        public async Task<IActionResult> Put([FromBody] PreVerificationData verificationData)
        {
            var userProfile = await _userProfileRepository.GetByUserLogin(verificationData.UserLogin);
            if (!await _authenticationHandler.AuthenticateExternalUser(
                verificationData.UserLogin, verificationData.Password))
            {
                return BadRequest();
            }

            bool verificationRequired;
            string formattedEmail;

            if (userProfile == null)
            {
                verificationRequired = false;
                formattedEmail = FormatEmail(verificationData.UserLogin);
            }
            else
            {
                var verificationClient =
                    await _verificationClientRepository.Get(verificationData.ClientId, verificationData.UserLogin);
                verificationRequired = userProfile.TwoFactorAuthEnabled && verificationClient == null;
                formattedEmail = FormatEmail(userProfile.CreatedBy);
            }

            return Ok(new
            {
                verificationRequired,
                verificationMethods = userProfile != null && userProfile.PhoneNumberVerified ? 
                new[] {
                    new { value = VerificationType.EMAIL, description = FormatEmail(formattedEmail) },
                    new { value = VerificationType.PHONE, description = userProfile.DisplayPhoneNumber },
                } :
                new[] {
                    new { value = VerificationType.EMAIL, description = FormatEmail(formattedEmail) } },
                clientId = Guid.NewGuid().ToString()
            });
        }

        [HttpGet]
        [Route("{userlogin}/{type}/{clientId}")]
        public async Task<IActionResult> Get(string userlogin, string type, string clientId)
        {
            var userProfile = await _userProfileRepository.GetByUserLogin(userlogin);

            if (userProfile == null)
            {
                return BadRequest();
            }

            await _verificationRequestRepository.DeleteForLogin(userProfile.CreatedBy);

            var code = new Random().Next(100000, 999999).ToString();

            var verificationRequest =
                new VerificationRequest {
                    Code = code,
                    CreatedBy = userlogin,
                };

            var verificationCode = 
                await _verificationRequestRepository.Add(verificationRequest);

            if (type == VerificationType.EMAIL) {
                _emailLoginService.SendVerificationCode(userProfile.CreatedBy, code);
            }
            else if (type == VerificationType.PHONE
                && userProfile.PhoneNumberVerified) {
                await _twilioService.SendSms(userProfile.PhoneNumber, code);
            }
            else {
                await _verificationRequestRepository.DeleteForLogin(userProfile.CreatedBy);
                return BadRequest("Invalid verification method");
            }

            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] VerificationData verificationData)
        {
            if (verificationData == null)
            {
                return BadRequest();
            }

            var verificationRequest =
                    await _verificationRequestRepository.Get(
                        verificationData.VerificationCode, verificationData.UserLogin);
         
            if (verificationRequest == null)
            {
                return BadRequest();
            }

            if (verificationData.RememberMe && 
                !string.IsNullOrWhiteSpace(verificationData.ClientId)) {
                await _verificationClientRepository.Add(
                    new VerificationClient {
                        ClientId = verificationData.ClientId,
                        CreatedBy = verificationRequest.CreatedBy
                    });
            }
            return Ok();
        }

        private string FormatEmail(string email)
        {
            return $"{email.Split('@')[0].First()}*******@{email.Split('@')[1]}";
        }
    }
}
