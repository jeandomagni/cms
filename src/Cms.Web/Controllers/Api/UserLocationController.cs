﻿using Cms.Domain.Exception;
using Cms.Model.Models.User;
using Cms.Model.Repositories;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cms.Web.Controllers.Api
{
    [Route("api/[controller]")]
    public class UserLocationController : SecureBaseController
    {
        private readonly IUserLocationRepository _userLocationRepository;

        public UserLocationController(IUserLocationRepository userLocationRepository) {
            _userLocationRepository = userLocationRepository;
        }

        [HttpGet]
        public async Task<IEnumerable<UserLocation>> Get()
        {
            return
                await _userLocationRepository.GetAllForUser(CurrentUser.UserId);
        }

        [HttpPut]
        public async Task<UserLocation> Put([FromBody] UserLocation userLocation)
        {
            if (string.IsNullOrEmpty(userLocation.DisplayName)) {
                throw new FieldRequiredException("DisplayName");
            }

            if (string.IsNullOrEmpty(userLocation.LocationId)) {
                throw new FieldRequiredException("LocationId");
            }

            return
                await _userLocationRepository.Add(
                    CurrentUser.UserId, 
                    userLocation.LocationId,
                    userLocation.DisplayName);
        }

        [HttpDelete("{id}")]
        public async Task<OkResult> Delete(int id)
        {
            await _userLocationRepository.Delete(id);
            return Ok();
        }
    }
}
