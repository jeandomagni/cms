﻿using System.Collections.Generic;
using Cms.Model.Repositories;
using Cms.Web.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Cms.Model.Dto.UserManagement;
using Cms.Model.Models.UserManagement;
using Cms.Web.Services.UserManagement;

namespace Cms.Web.Controllers.Api
{
    [Route("api/[controller]")]
    public class PasswordManagementController : Controller
    {
        private readonly IUserManagementService _userManagementService;

        public PasswordManagementController(
            IUserManagementService userManagementService
            )
        {
            _userManagementService = userManagementService;
        }

        [HttpPut]
        [Route("ChangePassword")]
        public async Task<ServiceResponseDto> UpdatePassword([FromBody] UserPasswordUpdateRequestDto request)
        {
            return await _userManagementService.UpdateUserPasswordByEmailAsync(request);
        }
        [HttpPut]
        [Route("ResetPassword")]
        public async Task<ServiceResponseDto> ResetPassword([FromBody] UserPasswordUpdateRequestDto request)
        {
            return await _userManagementService.ResetUserPasswordByEmailAsync(request);
        }



    }
}