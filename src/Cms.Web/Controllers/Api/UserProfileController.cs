﻿using Cms.Domain.Enum;
using Cms.Domain.Exception;
using Cms.Model.Enum;
using Cms.Model.Models;
using Cms.Model.Models.User;
using Cms.Model.Repositories;
using Cms.Web.Services;
using Cms.Web.Services.Sms;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Cms.Web.Controllers.Api
{
    [Route("api/[controller]")]
    public class UserProfileController : SecureBaseController
    {
        private readonly IUserProfileService _userProfileService;
        private readonly IAuditLogRepository _auditLogRepository;
        private readonly IVerificationRequestRepository _verificationRequestRepository;
        private readonly IVerificationClientRepository _verificationClientRepository;
        private readonly ITwilioService _twilioService;

        public UserProfileController(
            IUserProfileService userProfileService,
            IAuditLogRepository auditLogRepository,
            IVerificationRequestRepository verificationRequestRepository,
            IVerificationClientRepository verificationClientRepository,
            ITwilioService twilioService)
        {
            _verificationRequestRepository = verificationRequestRepository;
            _verificationClientRepository = verificationClientRepository;
            _userProfileService = userProfileService;
            _auditLogRepository = auditLogRepository;
            _twilioService = twilioService;
        }

        [HttpGet]
        [Route("user")]
        public async Task<UserProfile> GetForUser()
        {
            var userProfile = await _userProfileService.GetForUser(CurrentUser.UserId);

            if (userProfile == null)
            {
                throw new UserProfileNotConfiguredException();
            }

            return
                 userProfile;
        }

        [HttpPost]
        public async Task<UserProfile> Post([FromBody] UserProfile userProfile)
        {
            var existingProfile = await _userProfileService.GetById(userProfile.Id);

            if (!CurrentUser.HasAccessToRecord(
                existingProfile.CreatedBy, string.Empty))
            {
                throw new InvalidRequestorException();
            }

            userProfile.ModifiedBy =
                CurrentUser.Email;

            if (!userProfile.TwoFactorAuthEnabled && existingProfile.TwoFactorAuthEnabled) {
                await _verificationClientRepository.DeleteForLogin(CurrentUser.Email);
            }

            var updatedProfile = await _userProfileService.Update(userProfile);

            await _auditLogRepository.Add(
                CurrentUser.Email, updatedProfile.Id, EntityCodes.USER, EntityActions.UPDATE);

            return updatedProfile;
        }

        [HttpGet]
        [Route("verify/{phone}")]
        public async Task<IActionResult> VerifyPhone(string phone)
        {
            var userProfile = await _userProfileService.GetForUser(CurrentUser.UserId);

            if (userProfile == null)
            {
                throw new UserProfileNotConfiguredException();
            }

            var code = new Random().Next(100000, 999999).ToString();

            await _twilioService.SendSms(phone, code);

            userProfile.PhoneNumber = new Regex(@"[^\d]").Replace(phone, "");

            await _userProfileService.Update(userProfile);

            await _verificationRequestRepository.DeleteForLogin(CurrentUser.Email);

            await _verificationRequestRepository.Add(
                new VerificationRequest
                {
                    Code = code,
                    CreatedBy = CurrentUser.Email
                });

            return Ok();
        }


        [HttpGet]
        [Route("save/{code}")]
        public async Task<IActionResult> SavePhone(string code)
        {
            var userProfile =
                await _userProfileService.GetForUser(CurrentUser.UserId);

            if (userProfile == null)
            {
                throw new UserProfileNotConfiguredException();
            }

            var verification = await _verificationRequestRepository.Get(code, CurrentUser.Email);

            if (verification == null)
            {
                return BadRequest();
            }

            userProfile.PhoneNumberVerified = true;

            await _userProfileService.Update(userProfile);

            await _verificationRequestRepository.DeleteForLogin(CurrentUser.Email);

            return Ok();
        }
    }
}