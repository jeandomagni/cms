﻿using Cms.Domain.Cache;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cms.Web.Controllers.Api
{
    [Route("api/[controller]")]
    public class SiteController: SecureBaseController
    {
        private readonly ILegacyBbbSiteCache _bbbSiteCache;
        public SiteController(ILegacyBbbSiteCache bbbSiteCache) {
            _bbbSiteCache = bbbSiteCache;
        }

        [HttpGet]
        public async Task<IEnumerable<LegacyBbbInfo>> Get() {
            return await _bbbSiteCache.GetAllForUserLocationsAsync(CurrentUser);
        }
    }
}
