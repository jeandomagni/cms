﻿using Ardalis.GuardClauses;
using Bbb.Core.Solr.Enums;
using Cms.Domain.ContentGuardClauses;
using Cms.Domain.Enum;
using Cms.Domain.Mappers;
using Cms.Domain.Services.Solr;
using Cms.Model.Models;
using Cms.Model.Models.Content;
using Cms.Model.Models.Content.Entities;
using Cms.Model.Models.Params;
using Cms.Model.Repositories;
using Cms.Web.Extensions;
using Cms.Web.Services;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SolrLocation = Bbb.Core.Solr.Model.Location;

namespace Cms.Web.Controllers.Api
{
    [Route("api/[controller]")]
    public class ContentLocationController : SecureBaseController
    {
        private readonly IPinnedContentLocationRepository _pinnedContentLocationRepository;
        private readonly IServiceAreaService _serviceAreaService;
        private readonly IContentGeotagMappers _geotagMappers;
        private readonly IContentService _contentService;
        private readonly IArticleOrderService _articleOrderService;
        private readonly IArticleOrderMappers _articleOrderMappers;
        private readonly IUserProfileService _userProfileService;
        private readonly ISolrLocationService _solrLocationService;

        public ContentLocationController(
            IPinnedContentLocationRepository pinnedContentLocationRepository,
            IServiceAreaService serviceAreaService,
            IContentGeotagMappers geotagMappers,
            IContentService contentService,
            IArticleOrderService articleOrderService,
            IArticleOrderMappers articleOrderMappers,
            IUserProfileService userProfileService,
            ISolrLocationService solrLocationService)
        {
            _pinnedContentLocationRepository = pinnedContentLocationRepository;
            _serviceAreaService = serviceAreaService;
            _geotagMappers = geotagMappers;
            _contentService = contentService;
            _articleOrderService = articleOrderService;
            _articleOrderMappers = articleOrderMappers;
            _userProfileService = userProfileService;
            _solrLocationService = solrLocationService;
        }

        [HttpPost]
        [Route("add")]
        public async Task<Content> Add([FromBody] UpdateLocationParams updateLocationParams)
        {
            var content = updateLocationParams.Content;
            var loc = updateLocationParams.Location;
            Guard.Against.RecordNotFound(content);

            var isInServiceArea = await IsInServiceArea(loc.Location, loc.Type);
            Guard.Against.InvalidRequestor(isInServiceArea, content, CurrentUser);

            switch (loc.Type)
            {
                case LocationTypes.City:
                    var cityLocationDocument = JsonConvert.DeserializeObject<Location>(loc.Location);

                    var countryGeotag = _geotagMappers.MapToGeotag(
                        cityLocationDocument.CountryCode.ToString(),
                        content.Code,
                        LocationTypes.Country,
                        null);

                    var stateGeotag = _geotagMappers.MapToGeotag(
                        cityLocationDocument.State,
                        content.Code,
                        LocationTypes.State,
                        null);

                    content.AddCity(cityLocationDocument);
                    content.AddStateGeotag(stateGeotag);
                    content.AddCountryGeotag(countryGeotag);
                    break;
                default:
                    break;
            }

            await _contentService.UpdateAsync(content, CurrentUser);
            return await _contentService.GetContentByCodeAsync(content.Code, CurrentUser);
        }

        [HttpPost]
        [Route("delete")]
        public async Task<Content> DeleteCity([FromBody] UpdateLocationParams updateLocationParams)
        {
            var content = updateLocationParams.Content;
            var location = JsonConvert.DeserializeObject<Location>(updateLocationParams.Location.Location);

            return await DeleteGeotagAction(new Geotag { LocationId = location.Id, LocationType = LocationTypes.City }, content);
        }

        [HttpPost]
        [Route("addPin")]
        public async Task<Content> AddPin([FromBody] UpdatePinParams updatePinParams)
        {
            var content = updatePinParams.Content;
            var pin = updatePinParams.Pin;
            Guard.Against.RecordNotFound(content);

            var isInServiceArea = await IsInServiceArea(pin.PinnedLocation, pin.LocationType);
            Guard.Against.InvalidRequestor(isInServiceArea, content, CurrentUser);

            switch (pin.LocationType)
            {
                case LocationTypes.City:
                    var city = JsonConvert.DeserializeObject<Location>(pin.PinnedLocation);
                    content.AddCityPin(city);
                    break;
                default:
                    break;
            }

            var toAdd = PinnedContentLocationMappers.MapToDbPinnedContentLocation(pin, content.Code, CurrentUser.Email);
            await _pinnedContentLocationRepository.Create(toAdd);

            await _contentService.UpdateAsync(content, CurrentUser);
            return await _contentService.GetContentByCodeAsync(content.Code, CurrentUser);
        }

        [HttpPost]
        [Route("updatePin")]
        public async Task<Content> UpdatePin([FromBody] UpdatePinParams updatePinParams)
        {
            var content = updatePinParams.Content;
            var pin = updatePinParams.Pin;

            Guard.Against.RecordNotFound(content);
            var isInServiceArea = await IsInServiceArea(pin.PinnedLocation, pin.LocationType);
            Guard.Against.InvalidRequestor(isInServiceArea, content, CurrentUser);

            var toUpdate = PinnedContentLocationMappers.MapToDbPinnedContentLocation(pin);
            await _pinnedContentLocationRepository.Update(toUpdate);

            return await _contentService.GetContentByCodeAsync(content.Code, CurrentUser);
        }

        [HttpPost]
        [Route("deletePin")]
        public async Task<Content> DeletePin([FromBody] UpdatePinParams updatePinParams)
        {
            var content = updatePinParams.Content;
            var pin = updatePinParams.Pin;
            var location = JsonConvert.DeserializeObject<Location>(updatePinParams.Pin.PinnedLocation);

            return await DeleteGeotagAction(new Geotag { LocationId = location.Id, PinId = pin.Id, LocationType = LocationTypes.City }, content);
        }

        [HttpPost]
        [Route("updateGeotag")]
        public async Task<Content> UpdateGeotag([FromBody] Geotag geotag)
        {
            try
            {
                var contentToUpdate = await AddGeotag(new List<Geotag> { geotag });
                return await _contentService.GetContentByCodeAsync(contentToUpdate.Code, CurrentUser);
            }
            catch (Exception e)
            {
                throw new Exception($"{e.Message}. Content: {JsonConvert.SerializeObject(geotag)}");
            }
        }

        private async Task<Content> AddGeotag(List<Geotag> geotags)
        {
            var firstGeotag = geotags.FirstOrDefault();
            var contentCode = firstGeotag.ContentCode;
            Guard.Against.RecordNotFound(contentCode);

            var contentToUpdate = await _contentService.GetContentByCodeAsync(contentCode, CurrentUser);
            Guard.Against.RecordNotFound(contentToUpdate);

            var resultingGeotags = new List<Geotag>();
            if (firstGeotag.LocationType == LocationTypes.Country)
            {
                foreach (var item in geotags)
                {
                    if (!contentToUpdate.Countries.Any(x => x.LocationId == item.LocationId && x.IsPinned == item.IsPinned))
                    {
                        resultingGeotags.Add(item);
                    }
                }
                if (resultingGeotags.Count == 0) return contentToUpdate;
            }
            else
            {
                resultingGeotags = geotags.ToList();
            }

            foreach (var geotag in resultingGeotags)
            {
                var isInServiceArea = await IsInServiceArea(geotag.LocationId, geotag.LocationType);
                Guard.Against.InvalidRequestor(isInServiceArea, contentToUpdate, CurrentUser);

                switch (geotag.LocationType)
                {
                    case LocationTypes.Country:
                        contentToUpdate.AddCountryGeotag(geotag);
                        break;
                    case LocationTypes.State:
                        var countryForState = _geotagMappers.MapToGeotag(
                            await _serviceAreaService.GetCountryForBranchState(geotag.LocationId),
                            contentToUpdate.Code,
                            LocationTypes.Country,
                            null);

                        contentToUpdate.AddStateGeotag(geotag);
                        contentToUpdate.AddCountryGeotag(countryForState);
                        break;
                    case LocationTypes.City:
                        break;
                    case LocationTypes.BbbId:
                        contentToUpdate.AddBbbIdGeotag(geotag);
                        break;
                    default:
                        break;
                }

                if (!geotag.IsPinned)
                {
                    if (geotag.PinId.HasValue && geotag.PinId != 0)
                    {
                        await _pinnedContentLocationRepository.Delete(geotag.PinId.Value);
                    }

                    await _contentService.UpdateAsync(contentToUpdate, CurrentUser);
                    return await _contentService.GetContentByCodeAsync(contentToUpdate.Code, CurrentUser);
                }

                var pinToUpdate = PinnedContentLocationMappers.MapToDbPinnedContentLocation(geotag, CurrentUser.Email);

                if (pinToUpdate.Id != 0)
                {
                    await _pinnedContentLocationRepository.Update(pinToUpdate);
                }
                else
                {
                    await _pinnedContentLocationRepository.Create(pinToUpdate);
                }
            }

            await _contentService.UpdateAsync(contentToUpdate, CurrentUser);
            return contentToUpdate;
        }

        [HttpPost]
        [Route("deleteGeotag")]
        public async Task<Content> DeleteGeotagAction([FromBody] Geotag geotag, Content content = null)
        {
            Content contentToUpdate = await DeleteGeotag(geotag, content);

            return await _contentService.GetContentByCodeAsync(contentToUpdate.Code, CurrentUser);
        }

        private async Task<Content> DeleteGeotag(Geotag geotag, Content content = null)
        {
            var contentToUpdate = content;
            if (content == null || (content.Id == 0 && content.Code == null))
            {
                contentToUpdate = await _contentService.GetContentByCodeAsync(geotag.ContentCode, CurrentUser);
            }

            Guard.Against.RecordNotFound(contentToUpdate);
            var isInServiceArea = await IsInServiceArea(geotag.LocationId, geotag.LocationType);
            Guard.Against.InvalidRequestor(isInServiceArea, contentToUpdate, CurrentUser);

            if (geotag.PinId.HasValue && geotag.PinId != 0)
            {
                await _pinnedContentLocationRepository.Delete(geotag.PinId.Value);
            }

            bool updated = false;
            switch (geotag.LocationType)
            {
                case LocationTypes.Country:
                    updated = contentToUpdate.RemoveCountryGeotag(geotag);
                    break;
                case LocationTypes.State:
                    updated = contentToUpdate.RemoveStateGeotag(geotag);
                    break;
                case LocationTypes.City:
                    updated = contentToUpdate.RemoveCityAndPin(new Location { Id = geotag.LocationId });
                    break;
                case LocationTypes.BbbId:
                    updated = contentToUpdate.RemoveBbbIdGeotag(geotag);
                    break;
                default:
                    break;
            }

            if (updated)
            {
                await _contentService.UpdateAsync(contentToUpdate, CurrentUser);
                await _articleOrderService.DeleteFromLocation(contentToUpdate, geotag);
            }

            return contentToUpdate;
        }

        [HttpDelete]
        [Route("{code}/deleteFromFeed/{articleOrderId}")]
        public async Task<bool> DeleteFromFeed(string code, int articleOrderId)
        {
            try
            {
                var articleOrder = await _articleOrderService.Get(articleOrderId);
                var geotag = _articleOrderMappers.MapToGeotag(articleOrder);
                geotag.ContentCode = code;

                var content = await _contentService.GetContentByCodeAsync(geotag.ContentCode, CurrentUser);
                await _articleOrderService.DeleteContent(articleOrder, content.Id.ToString());

                if (!string.IsNullOrWhiteSpace(articleOrder.Topic))
                {
                    content.Topics = content.Topics.Where(x => x != articleOrder.Topic).ToList();
                    await _contentService.UpdateAsync(content, CurrentUser);
                }
                else
                {
                    await DeleteGeotag(geotag, content);
                }


                return true;
            }
            catch (System.Exception e)
            {
                throw;
            }
        }

        [HttpPost]
        [Route("addToMyNewsfeed/{code}")]
        public async Task AddToMyNewsfeed(string code)
        {
            var userProfile = await _userProfileService.GetForUser(CurrentUser.UserId);
            await AddGeotag(new List<Geotag> { new Geotag { ContentCode = code, LocationId = userProfile.DefaultSite, LocationType = LocationTypes.BbbId } });
        }

        [HttpPost]
        [Route("addToCountriesNewsfeed/{code}/{countries}")]
        public async Task AddToCountriesNewsfeed(string code, string countries)
        {
            var tags = countries
                .Split(",")
                .Select(x => new Geotag
                {
                    ContentCode = code,
                    LocationId = x,
                    LocationType = LocationTypes.Country,
                    IsPinned = true,
                    PinExpirationDate = DateTime.UtcNow.AddDays(14)
                })
                .ToList();

            var userProfile = await _userProfileService.GetForUser(CurrentUser.UserId);
            await AddGeotag(tags);
        }

        [HttpPost]
        [Route("deleteFromMyNewsfeed/{code}")]
        public async Task DeleteFromMyNewsfeed(string code)
        {
            var userProfile = await _userProfileService.GetForUser(CurrentUser.UserId);
            await DeleteGeotag(new Geotag { ContentCode = code, LocationId = userProfile.DefaultSite, LocationType = LocationTypes.BbbId });
        }

        [HttpPost]
        [Route("addToTop/{articleId}")]
        public async Task AddToTop(string articleId, [FromBody] Cms.Model.Models.Query.ArticleOrderParams data)
        {
            var storedArticleOrder = await _articleOrderService.GetOrCreate(data);
            var list = storedArticleOrder.OrderedArticleIdsAsArray;
            list = list.Where(x => x != articleId).ToList();
            list.Insert(0, articleId);
            storedArticleOrder.OrderedArticleIdsAsArray = list;
            storedArticleOrder.ModifiedDate = DateTime.UtcNow;

            await _articleOrderService.UpdateOrderMultiple(new List<ArticleOrder> { storedArticleOrder });
        }

        #region Generic Helper Methods
        private async Task<bool> IsInServiceArea(string location, string type)
        {
            if (CurrentUser.GlobalOrLocalAdmin)
            {
                return true;
            }

            switch (type)
            {
                case LocationTypes.Country:
                    return await _serviceAreaService.IsServiceableCountryForUser(location, CurrentUser);
                case LocationTypes.State:
                    return await _serviceAreaService.IsServiceableStateForUser(location, CurrentUser);
                case LocationTypes.City:
                    var locationArray = location.Split(' ');
                    var country = locationArray[locationArray.Length - 1];
                    var state = locationArray[locationArray.Length - 2];
                    var cityName = location.Replace($" {state} {country}", "");

                    var city = await _solrLocationService.GetCity(cityName, state, country);
                    return city != null && await _serviceAreaService.IsServiceableCityForUser(city, CurrentUser);
                case LocationTypes.BbbId:
                    return await _serviceAreaService.IsServiceableBbbIdForUser(location, CurrentUser);
                default:
                    return true;
            }
        }
        #endregion
    }
}
