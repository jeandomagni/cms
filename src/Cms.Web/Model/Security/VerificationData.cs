﻿namespace Cms.Web.Model.Security
{
    public class VerificationData
    {
        public string VerificationCode { get; set; }
        public string UserLogin { get; set; }
        public string ClientId { get; set; }
        public bool RememberMe { get; set; }
    }
}
