﻿namespace Cms.Web.Model.Security
{
    public class PreVerificationData
    {
        public string UserLogin { get; set; }
        public string Password { get; set; }
        public string ClientId { get; set; }
    }
}
