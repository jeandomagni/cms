﻿using Cms.Model.Enum;
using Cms.Model.Models;
using Cms.Model.Services;
using LazyCache;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;

namespace Cms.Web.Services
{
    public interface IListinngTypeToArticleTypeMappingService
    {
        Task<List<ArticleTypeToListingTypeMapping>> Get();
    }
    public class ListinngTypeToArticleTypeMappingService : IListinngTypeToArticleTypeMappingService
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IConnectionStringService _connectionStringService;
        private IAppCache _cache;
        public double ItemExpiration { get; }

        public ListinngTypeToArticleTypeMappingService(IConnectionStringService connectionStringService, IAppCache cache)
        {
            _connectionStringService = connectionStringService;
            _cache = cache;
            ItemExpiration = TimeSpan.FromHours(24).TotalMinutes;
        }

        private string GenerateKey(string uniqueId, string methodName) => $"{MethodBase.GetCurrentMethod().DeclaringType.Name}-{methodName}-{uniqueId}".ToLowerInvariant();

        public async Task<List<ArticleTypeToListingTypeMapping>> Get()
        {
            var key = GenerateKey("listinng-type-to-article-type-mapping-service", MethodBase.GetCurrentMethod().Name);
            return await _cache.GetOrAddAsync(key, async () =>
            {
                try
                {
                    var baseUrl =
                            _connectionStringService.GetVendorConnectionString(
                                Repository.ArticleTypeToListingTypeMappingApi);

                    using (var client = new HttpClient())
                    {
                        var result = JsonConvert.DeserializeObject<List<ArticleTypeToListingTypeMapping>>(await (await client.GetAsync(baseUrl)).Content.ReadAsStringAsync());
                        foreach (var item in result)
                        {
                            item.ListingTypes = item.ListingTypes.Select(x => x?.ToLower()).ToArray();
                            item.ArticleType = item.ArticleType.ToLower();
                        }
                        return result;
                    }
                }
                catch (Exception exception)
                {
                    _log.Error(exception);
                    return null;
                }
            }, DateTime.Now.AddMinutes(ItemExpiration));
            
        }
    }
}
