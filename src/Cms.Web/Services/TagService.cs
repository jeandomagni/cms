﻿using Cms.Model.Repositories;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cms.Web.Services
{
    public interface ITagService {
        Task<int> UpdateTags(IEnumerable<string> tags, string userId);
    }

    public class TagService : ITagService
    {
        private readonly ITagRepository _tagRepository;

        public TagService(
            ITagRepository tagRepository) {
            _tagRepository = tagRepository;
        }

        public async Task<int> UpdateTags(IEnumerable<string> tags, string userId)
        {
            var tagList = tags.ToList();
            if (!tagList.Any()) {
                return 0;
            }
            
            var uniqueLowercaseTags = tagList
                .Where(tag => !string.IsNullOrEmpty(tag))?
                .Select(tag => tag.Trim().ToLower())?
                .Distinct()
                .ToList();

            if (!uniqueLowercaseTags.Any())
            {
                return 0;
            }

            var tagsToInsert =
                uniqueLowercaseTags.Except(
                    await _tagRepository.GetExistingTags(uniqueLowercaseTags));

            if (tagsToInsert == null || !tagsToInsert.Any()) {
                return 0;
            }

            return 
                await _tagRepository.InsertTags(
                    tagsToInsert, userId);           
        }
    }
}
