﻿using Cms.Model.Enum;
using Cms.Model.Models;
using Cms.Model.Models.Query;
using Cms.Model.Services;
using log4net;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Cms.Web.Services
{
    [Obsolete("Should be removed after new article order deployed and tested in production")]
    public interface IArticleOrderSummaryService
    {
        Task<OrderedArticlesByLocation> PaginateArticles(ArticleOrderParams orderParams);
    }

    [Obsolete("Should be removed after new article order deployed and tested in production")]
    public class ArticleOrderSummaryService : IArticleOrderSummaryService
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IConnectionStringService _connectionStringService;

        public ArticleOrderSummaryService(
            IConnectionStringService connectionStringService)
        {
            _connectionStringService = connectionStringService;
        }

        public async Task<OrderedArticlesByLocation> PaginateArticles(ArticleOrderParams orderParams)
        {
            try
            {
                var baseUrl =
                        _connectionStringService.GetVendorConnectionString(
                            Repository.ArticleOrderSummaryApi);

                var urlWithQueries = $"{baseUrl}?{orderParams.GenerateQueryString()}";

                using (var client = new HttpClient())
                {
                    return
                        JsonConvert.DeserializeObject<OrderedArticlesByLocation>(
                            await (await client.GetAsync(urlWithQueries)).Content.ReadAsStringAsync());
                }
            }
            catch (Exception exception)
            {
                _log.Error(exception);
                return null;
            }
        }
        
    }
}
