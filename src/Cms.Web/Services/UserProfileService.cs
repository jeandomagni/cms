﻿using Cms.Domain.Cache;
using Cms.Model.Models.User;
using Cms.Model.Repositories;
using LazyCache;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cms.Web.Services
{
    public interface IUserProfileService
    {
        Task<UserProfile> GetForUser(string userId);
        Task<UserProfile> Update(UserProfile userProfile);
        Task<UserProfile> Add(UserProfile userProfile);
        Task<UserProfile> GetById(int id);
    }
    public class UserProfileService : IUserProfileService
    {
        private readonly IUserProfileCache _userProfileCache;
        private readonly IUserProfileRepository _userProfileRepository;

        public UserProfileService(IUserProfileCache userProfileCache, IUserProfileRepository userProfileRepository)
        {
            _userProfileRepository = userProfileRepository;
            _userProfileCache = userProfileCache;
        }

        public async Task<UserProfile> GetForUser(string userId)
        {
            var cache = _userProfileCache.GetProfile(userId);
            if (cache != null) return cache;

            cache = await _userProfileRepository.GetForUser(userId);
            if (cache != null) _userProfileCache.AddProfile(cache);

            return cache;
        }

        public Task<UserProfile> Update(UserProfile userProfile)
        {
            _userProfileCache.RemoveProfile(userProfile.UserId);
            return _userProfileRepository.Update(userProfile);
        }

        public Task<UserProfile> Add(UserProfile userProfile)
        {
            _userProfileCache.RemoveProfile(userProfile.UserId);
            return _userProfileRepository.Add(userProfile);
        }

        public Task<UserProfile> GetById(int id)
        {
            return _userProfileRepository.GetById(id);
        }
    }
}
