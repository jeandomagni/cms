﻿using AutoMapper;
using Cms.Model.Dto.UserManagement;
using Cms.Model.Models.UserManagement;

namespace Cms.Web.Services.UserManagement.Mapper
{
    public class UserManagementProfile : Profile
    {
        public UserManagementProfile()
        {
            CreateMap<Roles, RolesDto>().ReverseMap();

        }
    }
}
