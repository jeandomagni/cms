﻿using System;
using System.Data.SqlClient;
using Bbb.Core.Utilities.Async;
using Cms.Domain.Contracts;
using Cms.Model.Dto.UserManagement;
using Cms.Model.Enum;
using Cms.Model.Repositories.Helpers;
using Cms.Model.Services;
using FluentValidation;

namespace Cms.Web.Services.UserManagement.Validation
{
    public class UpdateUserPasswordValidator : AbstractValidator<UserPasswordUpdateRequestDto>
    {
        private readonly IServiceLocator _serviceLocator;
        private readonly bool _fromReset = false;

        public UpdateUserPasswordValidator(IServiceLocator serviceLocator, bool fromReset = false)
        {
            _fromReset = fromReset;
            _serviceLocator = serviceLocator;
            RuleFor(c => c.Email)
                .NotEmpty()
                .Custom((email, context) =>
                {
                    var _connectionStringService = _serviceLocator.Create<IConnectionStringService>();
                    using (var dbConnection = new SqlConnection(
                        _connectionStringService.GetVendorConnectionString(
                            Repository.CoreCmsDb)))
                    {
                        dbConnection.Open();
                        var userRecord = AsyncHelpers.RunSync(()=>  dbConnection.GetUserMembershipByEmailAsync(email));
                        if (userRecord == null)
                        {
                            context.AddFailure("User email not found.");
                        }

                        if (!_fromReset)
                        {
                            return;
                        }

                        if (!userRecord.Status.Equals("RESET", StringComparison.OrdinalIgnoreCase))
                        {
                            context.AddFailure("User status is invalid.");
                        }
                    }
                });
            

            //RuleFor(c => c.Password).NotEmpty();
            RuleFor(c => c.NewPassword).NotEmpty();
            When(c => !string.IsNullOrEmpty(c.VerifyPassword), () =>
            {
                RuleFor(c => c)
                    .Must(c => c.NewPassword.Equals(c.VerifyPassword))
                    .WithMessage("Passwords verification does not match.");

            });
        }
    }
}