﻿using Cms.Domain.Contracts;
using Cms.Model.Dto.UserManagement;
using FluentValidation;
using FluentValidation.Validators;

namespace Cms.Web.Services.UserManagement.Validation
{
    public class UserValidator : AbstractValidator<AddUpdateUserRequestDto>
    {
        private readonly IServiceLocator _serviceLocator;

        public UserValidator(IServiceLocator serviceLocator, bool? IsUpdate = false)
        {
            _serviceLocator = serviceLocator;
            RuleFor(c => c.FirstName).NotEmpty();
            RuleFor(c => c.LastName).NotEmpty();
            RuleFor(c => c.Email)
                .EmailAddress(EmailValidationMode.AspNetCoreCompatible)
                .NotEmpty();
            if (IsUpdate.Value)
            {
                RuleFor(c => c.UserId)
                    .NotNull()
                    .NotEmpty();

            }
        }
    }
}
