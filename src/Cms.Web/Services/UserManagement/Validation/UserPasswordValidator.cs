﻿using Cms.Domain.Contracts;
using Cms.Model.Dto.UserManagement;
using FluentValidation;

namespace Cms.Web.Services.UserManagement.Validation
{
    public class UserPasswordValidator : AbstractValidator<UserPasswordUpdateRequestDto>
    {
        private readonly IServiceLocator _serviceLocator;

        public UserPasswordValidator(IServiceLocator serviceLocator)
        {
            _serviceLocator = serviceLocator;
            RuleFor(c => c.UserId).NotNull().NotEmpty();
            RuleFor(c => c.Email).NotEmpty();
            RuleFor(c => c.Password).NotEmpty();
            RuleFor(c => c.NewPassword).NotEmpty();

        }
    }
}