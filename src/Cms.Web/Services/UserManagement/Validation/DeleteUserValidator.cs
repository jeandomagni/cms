﻿using System;
using Cms.Domain.Contracts;
using FluentValidation;

namespace Cms.Web.Services.UserManagement.Validation
{
    public class DeleteUserValidator : AbstractValidator<string>
    {
        private readonly IServiceLocator _serviceLocator;

        public DeleteUserValidator(IServiceLocator serviceLocator)
        {
            _serviceLocator = serviceLocator;
            RuleFor(c => c).Custom((s, context) =>
            {
                var isValid = Guid.TryParse(s, out var userId);
                if (!isValid)
                {
                    context.AddFailure("Invalid User Id.");
                }
            });
        }
    }
}
