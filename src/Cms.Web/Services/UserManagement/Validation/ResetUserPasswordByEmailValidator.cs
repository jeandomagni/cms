﻿using System;
using Cms.Domain.Contracts;
using Cms.Model.Dto.UserManagement;
using FluentValidation;

namespace Cms.Web.Services.UserManagement.Validation
{
    public class ResetUserPasswordByEmailValidator : AbstractValidator<UserPasswordUpdateRequestDto>
    {
        private readonly IServiceLocator _serviceLocator;

        public ResetUserPasswordByEmailValidator(IServiceLocator serviceLocator)
        {
            _serviceLocator = serviceLocator;
            RuleFor(c => c.Email).NotEmpty();
        }
    }
}