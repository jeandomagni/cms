﻿using Cms.Domain.Contracts;
using Cms.Model.Dto.UserManagement;
using FluentValidation;

namespace Cms.Web.Services.UserManagement.Validation
{
    public class UpdateUserStatusValidator : AbstractValidator<UserStatusUpdateRequestDto>
    {
        private readonly IServiceLocator _serviceLocator;

        public UpdateUserStatusValidator(IServiceLocator serviceLocator)
        {
            _serviceLocator = serviceLocator;
            RuleFor(c => c.UserId).NotNull().NotEmpty();
            RuleFor(c => c.Email).NotEmpty();
            RuleFor(c => c.Status).NotEmpty();
        }
    }
}