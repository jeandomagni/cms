﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Transactions;
using AutoMapper;
using Cms.Domain.Contracts;
using Cms.Domain.Enum;
using Cms.Model.Dto.UserManagement;
using Cms.Model.Enum;
using Cms.Model.Helpers;
using Cms.Model.Models.Config;
using Cms.Model.Models.General;
using Cms.Model.Models.User;
using Cms.Model.Models.UserManagement;
using Cms.Model.Repositories;
using Cms.Model.Repositories.Helpers;
using Cms.Model.Services;
using Cms.Web.Services.Email;
using Cms.Web.Services.UserManagement.Validation;
using FluentValidation;
using log4net;
using Microsoft.Extensions.Options;
using User = Cms.Model.Models.UserManagement.User;

namespace Cms.Web.Services.UserManagement
{
    public interface IUserManagementService
    {
        Task<ServiceResponseDto<AddUpdateUserRequestDto>> AddUserAsync(AddUpdateUserRequestDto addUpdateUserRequestInfo);
        Task<(AuthorizationResultEnum, UserMembership)> ValidateCredentialsAsync(string email, string password, bool fromTwoFactorFlow = false);
        Task<PaginationResultDto<UserMembershipDto>> GetUserMemberShipListAsync(UserMembershipSearchDto requestDto);

        Task<ServiceResponseDto> UpdateUserStatusAsync(UserStatusUpdateRequestDto userInfo);
        Task<ServiceResponseDto> UpdateUserPasswordAsync(UserPasswordUpdateRequestDto userInfo, bool fromReset = false);
        Task<ServiceResponseDto> UpdateUserPasswordByEmailAsync(UserPasswordUpdateRequestDto userInfo);

        Task<ServiceResponseDto<AddUpdateUserRequestDto>> UpdateUserAsync(
            AddUpdateUserRequestDto addUpdateUserRequestInfo);

        Task<IEnumerable<RolesDto>> GetRolesListAsync();

        Task<UserMembershipInfoDto> GetUserMemberShipWithRolesAsync(
            string userId);
        Task<UserMembershipDto> GetUserByEmailAsync(
            string email);

        Task<ServiceResponseDto> DeleteUserAsync(string userId);
        Task<ServiceResponseDto> ResetUserPasswordByEmailAsync(UserPasswordUpdateRequestDto userInfo);
    }

    public enum AuthorizationResultEnum
    {
        Success,
        NotFound,
        InvalidPassword,
        AccountLocked,
        PasswordResetNeeded,
        AccountDisabled,
        AccountInactive,
        UnknownError,

    }
    public class UserManagementService : IUserManagementService
    {
        private readonly IConnectionStringService _connectionStringService;
        private readonly IServiceLocator _serviceLocator;
        private readonly IUserRepository _userRepository;
        private readonly IOptions<Authentication> _authenticationSettings;
        private readonly IAccountPasswordService _accountPasswordService;
        private readonly IMapper _mapper;
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public UserManagementService(IConnectionStringService connectionStringService,
            IServiceLocator serviceLocator,
            IUserRepository userRepository,
            IOptions<Authentication> authenticationSettings,
            IAccountPasswordService accountPasswordService,
            IMapper mapper)
        {
            _connectionStringService = connectionStringService;
            _serviceLocator = serviceLocator;
            _userRepository = userRepository;
            _authenticationSettings = authenticationSettings;
            _accountPasswordService = accountPasswordService;
            _mapper = mapper;
        }

        public async Task<(AuthorizationResultEnum, UserMembership)> ValidateCredentialsAsync(string email, string password, bool fromTwoFactorFlow = false)
        {
            using (var dbConnection = Connection)
            {
                await dbConnection.OpenAsync();
                var userRecord = await dbConnection.GetUserMembershipByEmailAsync(email);
                if (userRecord == null)
                {
                    return (AuthorizationResultEnum.NotFound, null);
                }

                var userStatusAccount = userRecord.Status.ToUpper();
                switch (userStatusAccount)
                {
                    case "INACTIVE":
                        return (AuthorizationResultEnum.AccountInactive, null);
                    case "DISABLED":
                        return (AuthorizationResultEnum.AccountDisabled, null);
                    case "LOCKED":
                        return (AuthorizationResultEnum.AccountLocked, null);
                }

                try
                {
                    var existingPwd = Convert.FromBase64String(userRecord.Password);
                    var salt = Convert.FromBase64String(userRecord.PasswordSalt);
                    byte[] providedPwd;

                    //in two factor flow, the password is replaced by the verification code.
                    if (fromTwoFactorFlow)
                    {
                        providedPwd = existingPwd;
                    }
                    else
                    {
                        providedPwd = CreatePasswordWithSalt(salt, password.Trim());
                    }

                    var valid = IsValidHash(existingPwd, providedPwd);
                    var ret = !valid ? (AuthorizationResultEnum.InvalidPassword, null)
                        : (AuthorizationResultEnum.Success, userRecord);

                    if (valid && userStatusAccount.Equals("RESET", StringComparison.OrdinalIgnoreCase))
                    {
                        return (AuthorizationResultEnum.PasswordResetNeeded, null);
                    }


                    if (!valid)
                    {
                        if (!_authenticationSettings.Value.EnableAccountLocking)
                        {
                            return ret;
                        }
                        var attempt = userRecord.FailedPasswordAttemptCount ?? 0;
                        attempt++;
                        await dbConnection.UpdateLoginAttemptCountAsync(userRecord.UserId, attempt);
                        if (attempt >= _authenticationSettings.Value.MaxLoginAttempt)
                        {
                            await dbConnection.UpdateUserStatusAsync(userRecord.UserId, "LOCKED");
                        }
                        return ret;
                    }

                    await dbConnection.UpdateLastLoginDateAsync(userRecord.UserId, DateTime.UtcNow);
                    if (_authenticationSettings.Value.EnableAccountLocking)
                    {
                        await dbConnection.ResetLoginAttemptCountAsync(userRecord.UserId);
                    }
                    return ret;
                }
                catch (Exception e)
                {
                    _log.Error("Error validating credentials", e);
                    return (AuthorizationResultEnum.UnknownError, null);
                }

            }
        }

        public async Task<PaginationResultDto<UserMembershipDto>> GetUserMemberShipListAsync(
            UserMembershipSearchDto requestDto)
        {


            var userMembershipListDto = new UserMembershipListDto
            {
                Page = requestDto.Page,
                PageSize = requestDto.PageSize,
                Direction = requestDto.Direction,
                SortColumn = requestDto.SortColumn,
                Email = requestDto.Search,
                FirstName = requestDto.Search,
                LastName = requestDto.Search,
                Status = requestDto.Search
            };

            if (Guid.TryParse(requestDto.Search, out var userId))
            {
                userMembershipListDto.UserId = userId;
            }

            var result = new PaginationResultDto<UserMembershipDto>
            {
                Items = new List<UserMembershipDto>()
            };

            var qbHelper = new QueryBuilderHelper();
            var filter = qbHelper.ConvertToDictionary(requestDto, null);
            using (var dbConnection = Connection)
            {
                await dbConnection.OpenAsync();
                var pagingResults = await dbConnection.GetUserMembershipListAsync(userMembershipListDto);

                return new PaginationResultDto<UserMembershipDto>
                {
                    Items = pagingResults.List.Select(c => new UserMembershipDto
                    {
                        UserName = c.UserName,
                        Email = c.Email,
                        LastName = c.LastName,
                        Status = c.Status,
                        BBBId = c.BBBId,
                        UserId = c.UserId,
                        FirstName = c.FirstName,
                        LastLockoutDate = c.LastLockoutDate,
                        LastLoginDate = c.LastLoginDate,
                        LastPasswordChangedDate = c.LastPasswordChangedDate,
                        SiteId = c.SiteId,
                        mId = c.mId
                    }),
                    HasNext = pagingResults.HasNext,
                    HasPrevious = pagingResults.HasPrevious,
                    IsFirst = pagingResults.IsFirst,
                    IsLast = pagingResults.IsLast,
                    Page = pagingResults.Page,
                    PageSize = pagingResults.PerPage,
                    TotalPages = pagingResults.TotalPages,
                    TotalRecords = pagingResults.Count
                };
            }


        }

        public async Task<UserMembershipInfoDto> GetUserMemberShipWithRolesAsync(
            string userId)
        {
            var id = Guid.Parse(userId);
            using (var dbConnection = Connection)
            {
                await dbConnection.OpenAsync();
                var membership = await dbConnection.GetUserMembershipByUserIdAsync(id);
                if (membership == null)
                {
                    return null;
                }
                var roles = await dbConnection.GetUserRolesByIdAsync(id);
                var ret = new UserMembershipInfoDto
                {
                    UserId = membership.UserId,
                    BBBId = membership.BBBId,
                    FirstName = membership.FirstName,
                    LastName = membership.LastName,
                    UserName = membership.UserName,
                    Email = membership.Email,
                    mId = membership.mId,
                    SiteId = membership.SiteId,
                    Status = membership.Status,
                    LastLoginDate = membership.LastLoginDate,
                    LastPasswordChangedDate = membership.LastPasswordChangedDate,
                    LastLockoutDate = membership.LastLockoutDate,
                    Roles = new List<UserRolesDto>()
                };
                foreach (var r in roles)
                {
                    var role = new UserRolesDto
                    {
                        Email = r.Email,
                        RoleName = r.RoleName,
                        SiteId = r.SiteId,
                        UserId = Convert.ToString(r.UserId),
                        RoleId = Convert.ToString(r.RoleId),
                        BBBName = r.BBBName
                    };
                    ret.Roles.Add(role);
                }
                return ret;
            }
        }

        public async Task<UserMembershipDto> GetUserByEmailAsync(string email)
        {
            using (var dbConnection = Connection)
            {
                await dbConnection.OpenAsync();
                var membership = await dbConnection.GetUserMembershipByEmailAsync(email);
                if (membership == null)
                {
                    return null;
                }

                var ret = new UserMembershipDto
                {
                    UserId = membership.UserId,
                    BBBId = membership.BBBId,
                    FirstName = membership.FirstName,
                    LastName = membership.LastName,
                    UserName = membership.UserName,
                    Email = membership.Email,
                    mId = membership.mId,
                    SiteId = membership.SiteId,
                    Status = membership.Status,
                    LastLoginDate = membership.LastLoginDate,
                    LastPasswordChangedDate = membership.LastPasswordChangedDate,
                    LastLockoutDate = membership.LastLockoutDate,
                };
                return ret;

            }
        }

        private string RandomString(int length)
        {
            var random = new Random();
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        public async Task<ServiceResponseDto<AddUpdateUserRequestDto>> AddUserAsync(AddUpdateUserRequestDto addUpdateUserRequestInfo)
        {
            var ret = new ServiceResponseDto<AddUpdateUserRequestDto>();
            var validator = new UserValidator(_serviceLocator) { CascadeMode = CascadeMode.Stop };
            var validationResults = await validator.ValidateAsync(addUpdateUserRequestInfo);
            if (!validationResults.IsValid)
            {
                foreach (var validationResult in validationResults.Errors)
                {
                    ret.ErrorMessages.Add(validationResult.ErrorMessage);
                }
                return ret;
            }

            var errorRefId = Guid.NewGuid().ToString();
            var passwordString = RandomString(15);
            var sendEmail = addUpdateUserRequestInfo.EmailPassword.GetValueOrDefault();

            var userRecord = new Cms.Model.Models.UserManagement.User
            {
                UserName = addUpdateUserRequestInfo.Email,
                Email = addUpdateUserRequestInfo.Email,
                FirstName = addUpdateUserRequestInfo.FirstName,
                LastName = addUpdateUserRequestInfo.LastName,
                BBBId = addUpdateUserRequestInfo.SiteId
            };

            try
            {
                addUpdateUserRequestInfo.Password = passwordString;

                var pwdInfo = CreateSaltAndPassword(addUpdateUserRequestInfo.Password);
                var salt64 = Convert.ToBase64String(pwdInfo.Item1);
                var hash64 = Convert.ToBase64String(pwdInfo.Item2);
                userRecord.Password = hash64;
                userRecord.PasswordSalt = salt64;
            }
            catch (Exception e)
            {
                _log.Error($"Could not create hash password. Ref Id: {errorRefId}", e);

                ret.ErrorMessages.Add($"Could not create hash password. Ref Id: {errorRefId}");
                return ret;
            }

            try
            {
                using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    using (var dbConnection = Connection)
                    {
                        await dbConnection.OpenAsync();
                        await dbConnection.AddUserAsync(userRecord);
                        await dbConnection.AddMembershipAsync(userRecord);
                        await dbConnection.AddUserRolesAsync(userRecord.UserId,
                            addUpdateUserRequestInfo
                                .Roles
                                .Select(c => c.RoleId.GetValueOrDefault())
                                .Distinct()
                                .ToList());
                        var profileId = await dbConnection.AddProfileAsync(new UserProfile
                        {
                            UserId = userRecord.UserId.ToString(),
                            CreatedBy = userRecord.Email,
                            DefaultSite = userRecord.BBBId,
                            OpenContentInNewTab = false,
                        });

                        var auditLog = new DbAuditLog
                        {
                            UserId = userRecord.UserId.ToString(),
                            EntityCode = EntityCodes.USER,
                            EntityId = profileId.Id,
                            ActionCode = EntityActions.CREATE
                        };
                        await dbConnection.AddAuditLogEntryAsync(auditLog);

                        scope.Complete();
                        scope.Dispose();
                        ret.Items.Add(addUpdateUserRequestInfo);
                        ret.Success = true;
                    }
                }

                try
                {
                    if (sendEmail)
                    {
                        _accountPasswordService.EmailPassword(addUpdateUserRequestInfo.Email, passwordString);
                    }
                }
                catch (Exception e)
                {
                    ret.WarningMessages.Add("Failed sending email to user.");
                }

            }
            catch (Exception e)
            {
                _log.Error($"Could not create user. Ref Id: {errorRefId}", e);
                ret.ErrorMessages.Add($"Error creating user. Ref Id: {errorRefId}");
                return ret;
            }


            return ret;
        }

        public async Task<ServiceResponseDto> UpdateUserPasswordByEmailAsync(UserPasswordUpdateRequestDto userInfo)
        {
            var ret = new ServiceResponseDto();
            var validator = new UpdateUserPasswordValidator(_serviceLocator, true) { CascadeMode = CascadeMode.Stop };
            var validationResults = await validator.ValidateAsync(userInfo);
            if (!validationResults.IsValid)
            {
                foreach (var validationResult in validationResults.Errors)
                {
                    ret.ErrorMessages.Add(validationResult.ErrorMessage);
                }
                return ret;
            }

            var errorRefId = Guid.NewGuid().ToString();
            var sendEmail = userInfo.EmailPassword.GetValueOrDefault();
            var user = await GetUserByEmailAsync(userInfo.Email);
            if (user == null)
            {
                ret.ErrorMessages.Add("User Not Found.");
                ret.Success = false;
                return ret;
            }

            //var passwordValidation = await ValidateCredentialsAsync(userInfo.Email, userInfo.Password);
            //if (passwordValidation.Item1 != AuthorizationResultEnum.PasswordResetNeeded)
            //{
            //    _log.Error($"Invalid user status. User needs to be in RESET status. Ref Id: {errorRefId}");
            //    return ret;
            //}

            var userRecord = new Cms.Model.Models.UserManagement.User
            {
                Email = userInfo.Email,
                UserId = user.UserId
            };

            try
            {
                var pwdInfo = CreateSaltAndPassword(userInfo.NewPassword);
                var salt64 = Convert.ToBase64String(pwdInfo.Item1);
                var hash64 = Convert.ToBase64String(pwdInfo.Item2);
                userRecord.Password = hash64;
                userRecord.PasswordSalt = salt64;

            }
            catch (Exception e)
            {
                ret.ErrorMessages.Add($"Could not create hash password. Ref Id {errorRefId}");
                _log.Error($"Could not create hash password. Ref Id: {errorRefId}", e);
                return ret;
            }


            try
            {
                using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    using (var dbConnection = Connection)
                    {
                        await dbConnection.OpenAsync();
                        await dbConnection.UpdateUserPasswordAsync(userRecord);
                        await dbConnection.UpdateLastPasswordChangeDateAsync(userRecord.UserId, DateTime.UtcNow);
                        await dbConnection.UpdateUserStatusAsync(userRecord.UserId, "ACTIVE");
                        await dbConnection.ResetLoginAttemptCountAsync(userRecord.UserId);
                        scope.Complete();
                        scope.Dispose();
                        ret.Success = true;
                    }
                }
                try
                {
                    if (sendEmail)
                    {
                        _accountPasswordService.EmailPassword(userInfo.Email, userInfo.Password);
                    }
                }
                catch (Exception e)
                {
                    ret.WarningMessages.Add("Failed sending email to user.");
                }

            }
            catch (Exception e)
            {
                ret.ErrorMessages.Add($"An error occurred while saving password. Ref Id: {errorRefId}");
                _log.Error($"An error occurred while saving password. Ref Id: {errorRefId}");
                ret.Success = false;
            }
            return ret;
        }

        public async Task<ServiceResponseDto> ResetUserPasswordByEmailAsync(UserPasswordUpdateRequestDto userInfo)
        {
            var ret = new ServiceResponseDto();
            var validator = new ResetUserPasswordByEmailValidator(_serviceLocator) { CascadeMode = CascadeMode.Stop };
            var validationResults = await validator.ValidateAsync(userInfo);
            if (!validationResults.IsValid)
            {
                foreach (var validationResult in validationResults.Errors)
                {
                    ret.ErrorMessages.Add(validationResult.ErrorMessage);
                }
                return ret;
            }

            var errorRefId = Guid.NewGuid().ToString();
            var passwordString = RandomString(15);
            var user = await GetUserByEmailAsync(userInfo.Email);
            if (user == null)
            {
                ret.ErrorMessages.Add("User Not Found.");
                ret.Success = false;
                return ret;
            }


            var userRecord = new Cms.Model.Models.UserManagement.User
            {
                Email = userInfo.Email,
                UserId = user.UserId
            };

            try
            {
                var pwdInfo = CreateSaltAndPassword(passwordString);
                var salt64 = Convert.ToBase64String(pwdInfo.Item1);
                var hash64 = Convert.ToBase64String(pwdInfo.Item2);
                userRecord.Password = hash64;
                userRecord.PasswordSalt = salt64;

            }
            catch (Exception e)
            {
                ret.ErrorMessages.Add($"Could not create hash password. Ref Id {errorRefId}");
                _log.Error($"Could not create hash password. Ref Id: {errorRefId}");
                return ret;
            }

            try
            {
                using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    using (var dbConnection = Connection)
                    {
                        await dbConnection.OpenAsync();
                        await dbConnection.UpdateUserPasswordAsync(userRecord);
                        await dbConnection.UpdateLastPasswordChangeDateAsync(userRecord.UserId, DateTime.UtcNow);
                        await dbConnection.UpdateUserStatusAsync(userRecord.UserId, "RESET");
                        await dbConnection.ResetLoginAttemptCountAsync(userRecord.UserId);
                        scope.Complete();
                        scope.Dispose();
                        ret.Success = true;
                    }
                }
                try
                {
                    _accountPasswordService.EmailPassword(userInfo.Email, passwordString);
                }
                catch (Exception e)
                {
                    ret.WarningMessages.Add("Failed sending email to user.");
                }

            }
            catch (Exception e)
            {
                ret.ErrorMessages.Add($"An error occurred while saving password. Ref Id: {errorRefId}");
                _log.Error($"An error occurred while saving password. Ref Id: {errorRefId}");

                ret.Success = false;
            }
            return ret;
        }


        public async Task<ServiceResponseDto<AddUpdateUserRequestDto>> UpdateUserAsync(AddUpdateUserRequestDto addUpdateUserRequestInfo)
        {
            var ret = new ServiceResponseDto<AddUpdateUserRequestDto>();
            var validator = new UserValidator(_serviceLocator, true) { CascadeMode = CascadeMode.Stop };
            var validationResults = await validator.ValidateAsync(addUpdateUserRequestInfo);
            if (!validationResults.IsValid)
            {
                foreach (var validationResult in validationResults.Errors)
                {
                    ret.ErrorMessages.Add(validationResult.ErrorMessage);
                }
                return ret;
            }

            var errorRefId = Guid.NewGuid().ToString();

            var userRecord = new Cms.Model.Models.UserManagement.User
            {
                UserId = addUpdateUserRequestInfo.UserId.GetValueOrDefault(),
                UserName = addUpdateUserRequestInfo.UserName,
                Email = addUpdateUserRequestInfo.Email,
                FirstName = addUpdateUserRequestInfo.FirstName,
                LastName = addUpdateUserRequestInfo.LastName,
                BBBId = addUpdateUserRequestInfo.SiteId
            };


            try
            {
                using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    using (var dbConnection = Connection)
                    {
                        await dbConnection.OpenAsync();

                        //update user table
                        await dbConnection.UpdateUserAsync(userRecord);

                        //update membership 
                        await dbConnection.UpdateUserSiteIdAsync(userRecord.UserId,
                                                                userRecord.BBBId);

                        //update roles
                        await dbConnection.UpdateUserRolesAsync(addUpdateUserRequestInfo.UserId.GetValueOrDefault(),
                            addUpdateUserRequestInfo
                                .Roles
                                .Select(c => c.RoleId.GetValueOrDefault())
                                .Distinct()
                                .ToList());

                        //update userprofile
                        await dbConnection.UpdateUserProfileDefaultSiteIdAsync(userRecord.UserId,
                            userRecord.BBBId);


                        scope.Complete();
                        scope.Dispose();
                        ret.Items.Add(addUpdateUserRequestInfo);
                        ret.Success = true;
                    }
                }
            }
            catch (Exception e)
            {
                ret.ErrorMessages.Add($"Error updating user. Ref Id: {errorRefId}");
                _log.Error($"Error updating user. Ref Id: {errorRefId}", e);
                return ret;
            }

            return ret;
        }

        public async Task<ServiceResponseDto> DeleteUserAsync(string userId)
        {
            var ret = new ServiceResponseDto();

            var validator = new DeleteUserValidator(_serviceLocator) { CascadeMode = CascadeMode.Stop };
            var validationResults = await validator.ValidateAsync(userId);
            if (!validationResults.IsValid)
            {
                foreach (var validationResult in validationResults.Errors)
                {
                    ret.ErrorMessages.Add(validationResult.ErrorMessage);
                }
                return ret;
            }


            var errorRefId = Guid.NewGuid().ToString();

            try
            {
                using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    using (var dbConnection = Connection)
                    {
                        await dbConnection.OpenAsync();

                        await dbConnection.DeleteUserRolesAsync(userId);
                        await dbConnection.DeleteMembershipAsync(userId);
                        await dbConnection.DeleteUserProfileAsync(userId);
                        await dbConnection.DeleteUserAsync(userId);

                        scope.Complete();
                        scope.Dispose();
                        ret.Success = true;
                    }
                }
            }
            catch (Exception e)
            {
                _log.Error($"Error updating user. Ref Id: {errorRefId}", e);
                ret.ErrorMessages.Add($"Error updating user. Ref Id: {errorRefId}");
                return ret;
            }

            return ret;
        }


        public async Task<ServiceResponseDto> UpdateUserStatusAsync(UserStatusUpdateRequestDto userInfo)
        {
            var ret = new ServiceResponseDto();
            var validator = new UpdateUserStatusValidator(_serviceLocator) { CascadeMode = CascadeMode.Stop };
            var validationResults = await validator.ValidateAsync(userInfo);
            if (!validationResults.IsValid)
            {
                foreach (var validationResult in validationResults.Errors)
                {
                    ret.ErrorMessages.Add(validationResult.ErrorMessage);
                }
                return ret;
            }

            var errorRefId = Guid.NewGuid().ToString();
            var sendEmail = userInfo.EmailPassword.GetValueOrDefault();
            var passwordString = RandomString(15);
            var pwdInfo = CreateSaltAndPassword(passwordString);
            var salt64 = Convert.ToBase64String(pwdInfo.Item1);
            var hash64 = Convert.ToBase64String(pwdInfo.Item2);

            try
            {
                using (var dbConnection = Connection)
                {
                    await dbConnection.OpenAsync();
                    var rec = await dbConnection.UpdateUserStatusAsync(userInfo.UserId.GetValueOrDefault(), userInfo.Status);
                    await dbConnection.ResetLoginAttemptCountAsync(userInfo.UserId.GetValueOrDefault());

                    if (userInfo.Status.Equals("RESET", StringComparison.OrdinalIgnoreCase))
                    {
                        await dbConnection.UpdateUserPasswordAsync(new User
                        {
                            UserId = userInfo.UserId.GetValueOrDefault(),
                            Password = hash64,
                            PasswordSalt = salt64

                        });
                        try
                        {
                            if (sendEmail)
                            {
                                _accountPasswordService.EmailPassword(userInfo.Email, passwordString);
                            }
                        }
                        catch (Exception e)
                        {
                            ret.WarningMessages.Add("Failed sending email to user.");
                        }
                    }
                    ret.Success = true;
                }
            }
            catch (Exception e)
            {
                _log.Error($"An error occurred while updating user status. Ref Id: {errorRefId}", e);
                ret.ErrorMessages.Add($"An error occurred while updating user status. Ref Id: {errorRefId}");
                ret.Success = false;
            }
            return ret;
        }

        public async Task<IEnumerable<RolesDto>> GetRolesListAsync()
        {
            using (var dbConnection = Connection)
            {
                await dbConnection.OpenAsync();
                var records = await dbConnection.GetRolesAsync();
                var ret = _mapper.Map<IEnumerable<RolesDto>>(records);
                return ret;

            }
        }

        public async Task<ServiceResponseDto> UpdateUserPasswordAsync(UserPasswordUpdateRequestDto userInfo, bool fromReset = false)
        {
            var ret = new ServiceResponseDto();
            var errorRefId = Guid.NewGuid().ToString();
            var sendEmail = userInfo.EmailPassword.GetValueOrDefault();

            var validator = new UpdateUserPasswordValidator(_serviceLocator) { CascadeMode = CascadeMode.Stop };
            var validationResults = await validator.ValidateAsync(userInfo);
            if (!validationResults.IsValid)
            {
                foreach (var validationResult in validationResults.Errors)
                {
                    ret.ErrorMessages.Add(validationResult.ErrorMessage);
                }
                return ret;
            }

            var user = await GetUserByEmailAsync(userInfo.Email);
            if (user == null)
            {
                ret.ErrorMessages.Add("User Not Found.");
                ret.Success = false;
                return ret;

            }

            var userRecord = new Cms.Model.Models.UserManagement.User
            {
                UserId = user.UserId,
                Email = userInfo.Email,
            };

            try
            {
                var pwdInfo = CreateSaltAndPassword(userInfo.Password);
                var salt64 = Convert.ToBase64String(pwdInfo.Item1);
                var hash64 = Convert.ToBase64String(pwdInfo.Item2);
                userRecord.Password = hash64;
                userRecord.PasswordSalt = salt64;
            }
            catch (Exception e)
            {
                _log.Error($"Could not create hash password. Ref Id {errorRefId}", e);
                ret.ErrorMessages.Add($"Could not create hash password. Ref Id {errorRefId}");
                return ret;
            }


            try
            {
                using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    using (var dbConnection = Connection)
                    {
                        await dbConnection.OpenAsync();
                        await dbConnection.UpdateUserPasswordAsync(userRecord);
                        await dbConnection.UpdateLastPasswordChangeDateAsync(userRecord.UserId, DateTime.UtcNow);
                        await dbConnection.ResetLoginAttemptCountAsync(userRecord.UserId);
                        scope.Complete();
                        scope.Dispose();
                        ret.Success = true;
                    }
                }
                try
                {
                    if (sendEmail)
                    {
                        _accountPasswordService.EmailPassword(userInfo.Email, userInfo.Password);
                    }
                }
                catch (Exception e)
                {
                    ret.WarningMessages.Add("Failed sending email to user.");
                }

            }
            catch (Exception e)
            {
                _log.Error($"An error occurred while saving password. Ref Id: {errorRefId}", e);
                ret.ErrorMessages.Add($"An error occurred while saving password. Ref Id: {errorRefId}");
                ret.Success = false;
            }
            return ret;
        }

        private SqlConnection Connection =>
            new SqlConnection(
                _connectionStringService.GetVendorConnectionString(
                    Repository.CoreCmsDb));


        private (byte[], byte[]) CreateSaltAndPassword(string password)
        {
            var rfc2898DeriveBytes = new Rfc2898DeriveBytes(password, 32)
            {
                IterationCount = 10000
            };
            var hash = rfc2898DeriveBytes.GetBytes(20);
            var salt = rfc2898DeriveBytes.Salt;
            return (salt, hash);
        }
        private byte[] CreatePasswordWithSalt(byte[] salt, string password)
        {
            var rfc2898DeriveBytes = new Rfc2898DeriveBytes(password, salt, 10000);
            var hash = rfc2898DeriveBytes.GetBytes(20);
            return hash;
        }

        private bool IsValidHash(byte[] hash1, byte[] hash2)
        {
            for (int i = 0; i < 20; i++)
            {
                if (hash1[i] != hash2[i])
                {
                    return false;
                }
            }
            return true;
        }

    }
}
