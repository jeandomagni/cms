﻿using Cms.Domain.Mappers;
using Cms.Domain.Services.Solr;
using Cms.Model.Models.SecTerm;
using Cms.Model.Repositories.SecTerms;
using System.Threading.Tasks;

namespace Cms.Web.Services
{
    public interface ISecTermService
    {
        Task<SecTerm> Update(SecTerm secTerm);
    }

    public class SecTermService : ISecTermService
    {
        private ICategoryRepository _categoryRepository;
        private ICoreSecTermMappers _coreSecTermMappers;
        private ISecMetadataRepository _secMetadataRepository;
        private ISecTermRepository _secTermRepository;
        private ISolrSecTermService _solrMetadataService;
        private ISolrSearchTypeaheadService _solrSearchTypeaheadService;

        public SecTermService(ICategoryRepository categoryRepository,
            ICoreSecTermMappers coreSecTermMappers,
            ISecMetadataRepository secMetadataRepository,
            ISecTermRepository secTermRepository,
            ISolrSecTermService solrService,
            ISolrSearchTypeaheadService solrSearchTypeaheadService)
        {
            _categoryRepository = categoryRepository;
            _coreSecTermMappers = coreSecTermMappers;
            _secMetadataRepository = secMetadataRepository;
            _secTermRepository = secTermRepository;
            _solrMetadataService = solrService;
            _solrSearchTypeaheadService = solrSearchTypeaheadService;
        }

        public async Task<SecTerm> Update(SecTerm secTerm)
        {
            if (secTerm.IsEditable)
            {
                var coreCategory = secTerm.ToCoreCategory();
                await _categoryRepository.Update(coreCategory);
            }

            var metadata = secTerm.ToCoreSecMetadata();
            if (await _secMetadataRepository.GetByTobId(secTerm.TobId) == null)
            {
                await _secMetadataRepository.Add(metadata);
            }
            else
            {
                await _secMetadataRepository.Update(metadata);
            }
            
            // Fetching prior to pushing to SOLR ensures that we get the latest Spanish URL (if changed),
            // since that is fetched through a SOLR function.
            var coreSecTerm = await _secTermRepository.GetByTobId(secTerm.TobId);
            var updatedSecTerm = _coreSecTermMappers.MapToSecTerm(coreSecTerm);

            await _solrMetadataService.Update(updatedSecTerm);
            await _solrSearchTypeaheadService.UpdateCategory(updatedSecTerm);
            
            return updatedSecTerm;
        }
    }
}
