﻿using Cms.Model.Models.Config;
using Cms.Model.Models.Email;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using Cms.Domain.Contracts;


namespace Cms.Web.Services.Email
{
    public class EmailLoginService : EmailBaseService, IEmailLoginService
    {
        private readonly ContentReviewSettings _contentReviewSettings;
        public EmailLoginService(
            IOptions<ContentReviewSettings> contentReviewSettingsConfig,
            IOptions<EmailSettings> emailSettingsConfig)
            : base(emailSettingsConfig)
        {
            _contentReviewSettings = contentReviewSettingsConfig.Value;
        }

        public void SendVerificationCode(string email, string verificationCode)
        {
            var message = new EmailMessage()
            {
                ToAddresses = new List<EmailAddress> { new EmailAddress { Address = email } },
                FromAddresses = _contentReviewSettings.FromEmailAddresses,
                Subject = BuildVerificationNotificationSubject(),
                Content = BuildVerificationNotificationBody(verificationCode, email)
            };

            Send(message);         
        }

        private string BuildVerificationNotificationSubject()
        {
            return $"BBB CMS account email verification code";
        }

        private string BuildVerificationNotificationBody(string code, string email)
        {

            return $@"<!doctype html>
                <html>
                    <head>
                        <meta http-equiv=""Content - Type"" content=""text/html charset=UTF-8"" />
                    </head>
                    <body>
                        <div style=""font-family: Calibri, Tahoma, Helvetica, Arial, sans-serif; font-size: 11pt"">
                            <p>	
                            Thanks for verifying your {email} account!

                            Your code is: {code}.</p>
                        </div>
                    </body>
                </html>";
        }
    }
}
