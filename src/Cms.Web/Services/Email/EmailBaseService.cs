﻿using System.Linq;
using Cms.Domain.Contracts;
using Cms.Model.Models.Config;
using Cms.Model.Models.Email;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Options;
using MimeKit;
using MimeKit.Text;

namespace Cms.Web.Services.Email
{
    public class EmailBaseService : IEmailBaseService
    {
        private readonly EmailSettings _emailSettings;

        public EmailBaseService(IOptions<EmailSettings> emailSettingsConfig)
        {
            _emailSettings = emailSettingsConfig.Value;
        }

        public void Send(EmailMessage emailMessage)
        {
            var message = new MimeMessage();
            message.To.AddRange(emailMessage.ToAddresses.Select(x => new MailboxAddress(x.Name, x.Address)));
            message.From.AddRange(emailMessage.FromAddresses.Select(x => new MailboxAddress(x.Name, x.Address)));

            message.Subject = emailMessage.Subject;

            message.Body = new TextPart(TextFormat.Html)
            {
                Text = emailMessage.Content
            };

            using (var emailClient = new SmtpClient())
            {
                emailClient.ServerCertificateValidationCallback = (s, c, ch, e) => true;
                emailClient.Connect(_emailSettings.SmtpServer, _emailSettings.SmtpPort, SecureSocketOptions.Auto);
                emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                if (!string.IsNullOrWhiteSpace(_emailSettings.SmtpUsername) && !string.IsNullOrWhiteSpace(_emailSettings.SmtpPassword))
                {
                    emailClient.Authenticate(_emailSettings.SmtpUsername, _emailSettings.SmtpPassword);
                }

                emailClient.Send(message);

                emailClient.Disconnect(true);
            }
        }
    }
}
