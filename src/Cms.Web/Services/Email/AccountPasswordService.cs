﻿using Cms.Model.Models.Config;
using Cms.Model.Models.Email;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using Cms.Domain.Contracts;


namespace Cms.Web.Services.Email
{
    public class AccountPasswordService : EmailBaseService, IAccountPasswordService
    {
        private readonly ContentReviewSettings _contentReviewSettings;
        public AccountPasswordService(
            IOptions<ContentReviewSettings> contentReviewSettingsConfig,
            IOptions<EmailSettings> emailSettingsConfig)
            : base(emailSettingsConfig)
        {
            _contentReviewSettings = contentReviewSettingsConfig.Value;

        }

        public void EmailPassword(string email, string temporaryPassword)
        {
            var message = new EmailMessage()
            {
                ToAddresses = new List<EmailAddress> { new EmailAddress { Address = email } },
                FromAddresses = _contentReviewSettings.FromEmailAddresses,
                Subject = "Password Reset",
                Content = BuildEmailBody(email, temporaryPassword)
            };

            Send(message);         
        }

        private string BuildVerificationNotificationSubject()
        {
            return $"BBB CMS account email verification code";
        }

        private string BuildEmailBody(string email, string password)
        {

            return $@"<!doctype html>
                <html>
                    <head>
                        <meta http-equiv=""Content - Type"" content=""text/html charset=UTF-8"" />
                    </head>
                    <body>
                        <div style=""font-family: Calibri, Tahoma, Helvetica, Arial, sans-serif; font-size: 11pt"">
                            <p>	
                            Dear {email},
                            </p>
                            <p>
                            A request to reset the password of your account has been made. Your temporary assigned password is {password}
                            </p>
                        </div>
                    </body>
                </html>";
        }
    }
}
