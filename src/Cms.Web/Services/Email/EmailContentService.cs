﻿using Cms.Model.Models.Config;
using Cms.Model.Models.Content;
using Cms.Model.Models.Content.Db;
using Cms.Model.Models.Content.Entities;
using Cms.Model.Models.Email;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using Cms.Domain.Contracts;

namespace Cms.Web.Services.Email
{
    public class EmailContentService : EmailBaseService, IEmailContentService
    {
        private readonly ContentReviewSettings _contentReviewSettings;
        private readonly HttpRequest _request;
        public EmailContentService(
            IOptions<ContentReviewSettings> contentReviewSettingsConfig,
            IOptions<EmailSettings> emailSettingsConfig,
            IHttpContextAccessor httpContextAccessor)
            : base(emailSettingsConfig) {
            _contentReviewSettings = contentReviewSettingsConfig.Value;
            _request = httpContextAccessor.HttpContext?.Request;
        }

        public void SendReviewNotification(Content content)
        {
            // If no ToEmailAddresses are set (lower environments), do not attempt to send.
            if (_contentReviewSettings.ToEmailAddresses != null && _contentReviewSettings.ToEmailAddresses.Any())
            {
                var message = new EmailMessage()
                {
                    ToAddresses = _contentReviewSettings.ToEmailAddresses,
                    FromAddresses = _contentReviewSettings.FromEmailAddresses,
                    Subject = BuildReviewNotificationSubject(content.Type),
                    Content = BuildReviewNotificationBody(
                        content.Title,
                        content.Authors,
                        content.Code,
                        content.ModifiedBy)
                };

                Send(message);
            }
        }

        public void SendApprovalNotification(Content content, DbContent existingContent)
        {
            var message = new EmailMessage()
            {
                ToAddresses = BuildApprovalToAddresses(content.CreatedBy),
                FromAddresses = _contentReviewSettings.FromEmailAddresses,
                Subject = "Your CoreCMS article was approved",
                Content = BuildApprovalNotificationBody(content, existingContent)
            };

            Send(message);
        }

        #region Review email helpers
        private string BuildReviewNotificationSubject(string contentType)
        {
            return $"New {contentType.ToLower()} pending review";
        }

        private string BuildReviewNotificationBody(string title, List<Author> authors, string contentCode, string modifiedBy)
        {
            var authorsString = string.Join(", ",
                authors.Select(author => $@"<a href=""mailto:{author.Email}"">{author.Name}</a>"));

            var editContentUriBuilder = new UriBuilder()
            {
                Scheme = _request.Scheme,
                Host = _request.Host.Host,
                Path = "/#/content/create",
                Query = $"contentCode={contentCode}"
            };

            if (_request.Host.Port != null)
            {
                editContentUriBuilder.Port = (int)_request.Host.Port;
            }

            // URL decode is required to inject the hash back in the URL. 
            var editContentUrlString = WebUtility.UrlDecode(editContentUriBuilder.Uri.ToString());

            return $@"<!doctype html>
                <html>
                    <head>
                        <meta http-equiv=""Content - Type"" content=""text/html charset=UTF-8"" />
                    </head>
                    <body>
                        <div style=""font-family: Calibri, Tahoma, Helvetica, Arial, sans-serif; font-size: 11pt"">
                            <p>Hello! There is an article awaiting approval in the Pending Review tab of CoreCMS.</p>
                            <p>
                                Title: {title}<br />
                                Authors: {authorsString}<br />
                                Last modified by: {modifiedBy}
                            </p>
                            <p>To approve, please visit <a href=""{editContentUrlString}"">{editContentUrlString}</a></p>
                        </div>
                    </body>
                </html>";
        }
        #endregion

        #region Approval email helpers
        private List<EmailAddress> BuildApprovalToAddresses(string createdBy)
        {
            var createdByEmail = new EmailAddress();
            createdByEmail.Address = createdBy;
            return new List<EmailAddress> { createdByEmail };
        }

        private string BuildApprovalNotificationBody(Content content, DbContent existingContent)
        {
            var sbChangeNotice = new StringBuilder();

            var titleHasChanged = content.Title != existingContent.Title;
            var urlHasChanged = content.ArticleUrl.Full != existingContent.Canonical;

            if (titleHasChanged || urlHasChanged) {
                sbChangeNotice.Append("<p>Please note the following changes:</p>");
                sbChangeNotice.Append("<ul>");
            }

            if (titleHasChanged)
            {
                sbChangeNotice.Append("<li>");
                sbChangeNotice.Append("<strong>Title:</strong>");
                sbChangeNotice.Append(" ");
                sbChangeNotice.Append($"<strike>{existingContent.Title}</strike>");
                sbChangeNotice.Append(" ");
                sbChangeNotice.Append(content.Title);
                sbChangeNotice.Append("</li>");
            }

            if (urlHasChanged)
            {
                sbChangeNotice.Append("<li>");
                sbChangeNotice.Append("<strong>URL:</strong>");
                sbChangeNotice.Append(" ");
                sbChangeNotice.Append($"<strike>{existingContent.Canonical}</strike>");
                sbChangeNotice.Append(" ");
                sbChangeNotice.Append(content.ArticleUrl.Full);
                sbChangeNotice.Append("</li>");
            }

            if (titleHasChanged || urlHasChanged)
            {
                sbChangeNotice.Append("</ul>");
            }

            return $@"<!doctype html>
                <html>
                    <head>
                        <meta http-equiv=""Content - Type"" content=""text/html charset=UTF-8"" />
                    </head>
                    <body>
                        <div style=""font-family: Calibri, Tahoma, Helvetica, Arial, sans-serif; font-size: 11pt"">
                            <p>Your article ""{content.Title}"" was just approved for publication in CoreCMS.</p>
                            <p>The URL of the article is <a href=""{content.ArticleUrl.Full}"">{content.ArticleUrl.Full}</a> and you may share it.</p>
                            <p>Note: it may take a few minutes before it appears live. Be sure to share this URL and not the draft version.</p>
                            {sbChangeNotice.ToString()}
                        </div>
                    </body>
                </html>";
        }
        #endregion
    }
}
