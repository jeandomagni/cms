﻿using Ardalis.GuardClauses;
using Bbb.Core.Solr.Model;
using Bbb.Core.Utilities.Extensions;
using Cms.Domain.ContentGuardClauses;
using Cms.Domain.Enum;
using Cms.Domain.Helpers;
using Cms.Domain.Mappers;
using Cms.Domain.Model;
using Cms.Domain.Services.Solr;
using Cms.Domain.SolrGuardClauses;
using Cms.Model.Enum;
using Cms.Model.Models;
using Cms.Model.Models.BaselinePages;
using Cms.Model.Models.Content;
using Cms.Model.Models.Content.Db;
using Cms.Model.Repositories;
using Cms.Model.Repositories.Content;
using Cms.Web.Extensions;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Cms.Web.Services
{
    public interface IContentService
    {
        Task UpdateContentSites(string contentCode, string[] sites);
        Task<List<string>> SoftDelete(DbContent storedContent);
        Task<int> HardDelete(DbContent storedContent, AppUser user);
        Task<Cms.Model.Models.Content.Content> CreateAsync(Cms.Model.Models.Content.Content content, AppUser user);
        Task<Cms.Model.Models.Content.Content> UpdateAsync(Cms.Model.Models.Content.Content content, AppUser user);
        Task<Cms.Model.Models.Content.Content> GetContentByCodeAsync(string contentCode, AppUser currentUser);
        Task Delete(int id, AppUser currentUser, bool softDelete);
        Task DeleteByCode(string contentCode, AppUser currentUser, bool softDelete);
        Task<OrderedArticlesByLocation> GetOrderedArticles(Cms.Model.Models.Content.ArticleOrder articleOrder, int page, int skip, int pageSize);
    }

    public class ContentService : IContentService
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IContentAuditLogMappers _auditLogMappers;
        private readonly IAuditLogRepository _auditLogRepository;
        private readonly ISolrContentService _solrService;
        private readonly IContentRepository _contentRepository;
        private readonly IContentEventRecurrenceService _contentEventRecurrenceService;
        private readonly IContentReviewRepository _contentReviewRepository;
        private readonly IContentLocationRepository _contentLocationRepository;
        private readonly IContentMediaRepository _contentMediaRepository;
        private readonly ISiteContentRepository _siteContentRepository;
        private readonly IContentEventRepository _contentEventRepository;
        private readonly IContentNearMeRepository _contentNearMeRepository;
        private readonly ITagService _tagService;
        private readonly IContentMappers _contentMappers;
        private readonly IBaselinePagesRepository _baselinePagesRepository;
        private readonly IPinnedContentLocationRepository _pinnedContentLocationRepository;
        private readonly IArticleOrderService _articleOrderService;
        private readonly IUserProfileRepository _userProfileRepository;

        public ContentService(
            IContentAuditLogMappers auditLogMappers,
            IAuditLogRepository auditLogRepository,
            IContentLocationRepository contentLocationRepository,
            IContentRepository contentRepository,
            IContentEventRecurrenceService contentEventRecurrenceService,
            IContentReviewRepository contentReviewRepository,
            ISolrContentService solrService,
            IContentMediaRepository contentMediaRepository,
            ISiteContentRepository siteContentRepository,
            IContentEventRepository contentEventRepository,
            IContentNearMeRepository contentNearMeRepository,
            ITagService tagService,
            IContentMappers contentMappers,
            IBaselinePagesRepository baselinePagesRepository,
            IPinnedContentLocationRepository pinnedContentLocationRepository,
            IArticleOrderService articleOrderService,
            IUserProfileRepository userProfileRepository)
        {
            _auditLogMappers = auditLogMappers;
            _auditLogRepository = auditLogRepository;
            _solrService = solrService;
            _contentReviewRepository = contentReviewRepository;
            _contentLocationRepository = contentLocationRepository;
            _contentMediaRepository = contentMediaRepository;
            _contentEventRecurrenceService = contentEventRecurrenceService;
            _siteContentRepository = siteContentRepository;
            _contentRepository = contentRepository;
            _contentEventRepository = contentEventRepository;
            _contentNearMeRepository = contentNearMeRepository;
            _tagService = tagService;
            _contentMappers = contentMappers;
            _baselinePagesRepository = baselinePagesRepository;
            _pinnedContentLocationRepository = pinnedContentLocationRepository;
            _articleOrderService = articleOrderService;
            this._userProfileRepository = userProfileRepository;
        }

        public async Task UpdateContentSites(string contentCode, string[] sites)
        {
            await _siteContentRepository.DeleteForContent(contentCode);

            foreach (var site in sites)
            {
                if (!string.IsNullOrWhiteSpace(site))
                {
                    var siteContent = new SiteContent();
                    siteContent.ContentCode = contentCode;
                    siteContent.SiteId = site;
                    await _siteContentRepository.Add(siteContent);
                }
            }
        }

        public async Task<List<string>> SoftDelete(DbContent storedContent)
        {
            storedContent.ModifiedDate = DateTimeOffset.Now;
            storedContent.Status = ContentStatus.Deleted;
            await _contentRepository.DeleteSoft(storedContent.Id);
            return await _solrService.PostContent(storedContent);
        }

        public async Task<int> HardDelete(DbContent storedContent, AppUser user)
        {
            await _contentEventRecurrenceService.DeleteAllForContentCode(storedContent.Code, user);
            await _contentRepository.Delete(storedContent.Id);
            await _contentEventRepository.DeleteByContentCode(storedContent.Code);
            await _contentNearMeRepository.DeleteByContentCode(storedContent.Code);
            await _contentMediaRepository.DeleteByContentCode(storedContent.Code);
            await _contentReviewRepository.DeleteByContentCode(storedContent.Code);
            await _contentLocationRepository.DeleteForContent(storedContent.Id);
            return await _solrService.Delete(storedContent.Id.ToString());
        }

        public async Task<Cms.Model.Models.Content.Content> CreateAsync(Cms.Model.Models.Content.Content content, AppUser user)
        {
            Guard.Against.RecordNotFound(content);
            Guard.Against.MissingTitle(content);
            Guard.Against.MissingOrInvalidLayout(content);
            Guard.Against.MissingOrInvalidContentType(content);
            Guard.Against.InvalidHeadlineLength(content);

            content = await _contentMappers.InitializeContentAsync(content, user);
            if(!user.IsAdmin)
            {
                var defaultCountry = await _userProfileRepository.GetDefaultCountry(user.UserId);
                content.AddCountryGeotag(new Cms.Model.Models.Content.Entities.Geotag { LocationId = defaultCountry });
            }

            var dbContent = await _contentMappers.MapToDbContentAsync(content, user);
            var createdDbContent = await _contentRepository.Add(dbContent);

            await UpdateContentSites(createdDbContent.Code, new[] { createdDbContent.OwnerSiteId });

            if (content.Type == ContentType.Event)
            {
                await _contentEventRepository.Add(
                   new DbContentEvent { ContentCode = createdDbContent.Code });
            }

            if (content.Type == ContentType.NearMe)
            {
                await _contentNearMeRepository.Add(
                   new DbContentNearMe { ContentCode = createdDbContent.Code });
            }

            var solrResponse = await _solrService.PostContent(createdDbContent);
            Guard.Against.InvalidSolrResponse(solrResponse);

            return await _contentMappers.MapToContentAsync(createdDbContent, user);
        }

        public async Task<Cms.Model.Models.Content.Content> UpdateAsync(Cms.Model.Models.Content.Content content, AppUser user)
        {
            Guard.Against.RecordNotFound(content);

            var storedContent = await _contentRepository.GetById(content.Id, user.Email);

            Guard.Against.RecordNotFound(storedContent);
            Guard.Against.InvalidUpdates(content);
            Guard.Against.BaselinePageTypeChangeForbidden(storedContent, content);

            var dbContent = await _contentMappers.MapToDbContentAsync(content, user);

            var dbAuditLogs = _auditLogMappers.MapToDbAuditLogs(storedContent, dbContent, user.Email);
            await _auditLogRepository.AddMany(dbAuditLogs);

            var updatedDbContent = await _contentRepository.Update(dbContent);

            if (!content.Tags.IsNullOrEmpty())
            {
                await _tagService.UpdateTags(content.Tags, user.UserId);
            }

            if (content.Type == ContentType.BaselinePage)
            {
                BaselinePage baselinePage = await _baselinePagesRepository.GetByCode(content.Code);
                baselinePage.UrlSegment = content.UriSegment;
                await _baselinePagesRepository.UpdateBaselinePage(baselinePage);
            }

            var solrResult = await _solrService.PostContent(updatedDbContent);


            var result = await _contentMappers.MapToContentAsync(updatedDbContent, user);

            if (content.Type != ContentType.BaselinePage && content.Type != ContentType.NearMe)
            {
                if (result.Status == ContentStatus.Published)
                {
                    await _articleOrderService.AddContentToOrder(result, solrResult);
                }
                else
                {
                    await _articleOrderService.RemoveContentFromOrder(result, solrResult);
                }
            }

            return result;
        }

        public async Task<Cms.Model.Models.Content.Content> GetContentByCodeAsync(string contentCode, AppUser currentUser)
        {
            var dbContent = await _contentRepository.GetByCode(contentCode, currentUser.Email);
            return dbContent != null ? await _contentMappers.MapToContentAsync(dbContent, currentUser) : null;
        }

        public async Task Delete(int id, AppUser currentUser, bool softDelete)
        {
            var storedContent = await _contentRepository.GetById(
                    id, currentUser.Email);

            Guard.Against.RecordNotFound(storedContent);
            Guard.Against.InvalidRequestor(storedContent, currentUser);

            await _pinnedContentLocationRepository.DeleteByContentCode(storedContent.Code);

            if (storedContent.Type == ContentType.BaselinePage)
            {
                await _baselinePagesRepository.DeleteBaselinePageByCode(storedContent.Code);
            }

            await _articleOrderService.RemoveContentFromOrder(storedContent.Id.ToString());
            if (softDelete)
            {
                await SoftDelete(storedContent);
            }
            else
            {
                await HardDelete(storedContent, currentUser);
            }

            await _auditLogRepository.Add(
                currentUser.Email,
                id,
                ContentHelpers.MapContentTypeToEntity(storedContent.Type),
                EntityActions.DELETE);
        }

        public async Task DeleteByCode(string contentCode, AppUser currentUser, bool softDelete)
        {
            var contentId = await _contentRepository.GetIdByCode(contentCode);
            await Delete(contentId, currentUser, softDelete);
        }

        public async Task<OrderedArticlesByLocation> GetOrderedArticles(Cms.Model.Models.Content.ArticleOrder articleOrder, int page, int skip, int pageSize)
        {
            try
            {
                _log.Debug("GetOrderedArticles start");
                var result = new OrderedArticlesByLocation { ArticleOrderId = articleOrder.Id, OverrideFeedId = articleOrder.OverrideFeedId, TotalCount = articleOrder.OrderedArticleIdsAsArray.Count };
                if (articleOrder.OrderedArticleIdsAsArray.Count == 0) return result;

                List<string> futureArticleIds = await _solrService.GetFutureArticles();

                var articleResult = await GetArticles(articleOrder, skip, pageSize, futureArticleIds);
                List<Article> articles = articleResult.Item1;
                List<string> orderedArticleIds = articleResult.Item2;
                _log.Debug($"Get articles: {articles.Count}");

                var notNullArticles = articles.Where(x => x != null).ToList();
                while (notNullArticles.Count != orderedArticleIds.Count)
                {
                    _log.Debug("Found null articles");
                    foreach (var articleId in orderedArticleIds)
                    {
                        if (!notNullArticles.Any(x => x.Id == articleId))
                        {
                            _log.Debug($"null article: {articleId}");
                            articleOrder.OrderedArticleIdsAsArray = articleOrder.OrderedArticleIdsAsArray.Where(x => x != articleId).ToList();
                        }
                    }
                    await _articleOrderService.UpdateOrderMultiple(new List<Cms.Model.Models.Content.ArticleOrder> { articleOrder });

                    articleResult = await GetArticles(articleOrder, skip, pageSize, futureArticleIds);
                    articles = articleResult.Item1;
                    orderedArticleIds = articleResult.Item2;
                    notNullArticles = articles.Where(x => x != null).ToList();
                }

                string cityName = articleOrder.City;
                string stateCode = articleOrder.State;
                string countryCode = articleOrder.Country;
                string serviceArea = articleOrder.ServiceArea;

                _log.Debug($"before selecting data");
                var resultArticles = articles.Select(article => new ArticleOrderSummary
                {
                    Id = article.Id,
                    Code = article.Code,
                    Title = article.Title,
                    City = article.Cities != null && article.Cities.Contains($"{cityName}, {stateCode}") ? cityName : null,
                    State = article.States != null && article.States.Contains(stateCode) ? stateCode : null,
                    Country = article.Countries != null && article.Countries.Contains(countryCode) ? countryCode : null,
                    PinnedCity = article.PinnedCities != null && article.PinnedCities.Contains($"{cityName}, {stateCode}") ? cityName : null,
                    PinnedState = article.PinnedStates != null && article.PinnedStates.Contains(stateCode) ? stateCode : null,
                    PinnedCountry = article.PinnedCountries != null && article.PinnedCountries.Contains(countryCode) ? countryCode : null,
                    Priority = article.Priority,
                    PublishDate = article.PublishDate,
                    ImageUrl = !string.IsNullOrWhiteSpace(article.Image) ?
                        JsonConvert.DeserializeObject<List<ContentMedia>>(article.Image)?.FirstOrDefault()?.Url :
                        null
                }).ToList();

                result.Articles = resultArticles;

                _log.Debug("GetOrderedArticles finish");
                return result;
            }
            catch (Exception e)
            {
                _log.Error(e);
                throw;
            }
        }

        private async Task<Tuple<List<Article>, List<string>>> GetArticles(Cms.Model.Models.Content.ArticleOrder articleOrder, int skip, int pageSize, List<string> futureArticleIds)
        {
            var orderedArticleIds = articleOrder.OrderedArticleIdsAsArray
                .Where(x => !string.IsNullOrWhiteSpace(x)) // Filter empty strings
                .Where(x => futureArticleIds.All(y => y != x)) // Filter all articles from future
                .Skip(skip)
                .Take(pageSize)
                .ToList();
            var articles = await _solrService.GetArticlesByIds(orderedArticleIds);

            return new Tuple<List<Article>, List<string>>(articles, orderedArticleIds);
        }
    }
}
