﻿using Ardalis.GuardClauses;
using Cms.Domain.ContentGuardClauses;
using Cms.Domain.Extensions;
using Cms.Domain.Mappers;
using Cms.Domain.Model;
using Cms.Domain.Services.Solr;
using Cms.Model.Models.Content;
using Cms.Model.Models.Recurrence;
using Cms.Model.Repositories.Content;
using log4net;
using Newtonsoft.Json;
using RecurrenceCalculator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cms.Web.Services
{
    public interface IContentEventRecurrenceService
    {
        string Validate(EventRecurrence eventRecurrence);
        bool IsValid(EventRecurrence eventRecurrence, out string message);
        Task<int> CreateOccurrences(ContentEvent contentEvent);
        Task DeleteOccurrence(int occurenceId, AppUser user);
        Task DeleteAllForContentCode(string contentCode, AppUser user);
    }

    public class ContentEventRecurrenceService : IContentEventRecurrenceService
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IContentEventOccurenceRepository _contentEventOccurrenceRepository;
        private readonly IContentEventRepository _contentEventRepository;
        private readonly IContentRepository _contentRepository;
        private readonly ISolrContentService _solrContentService;
        private readonly IContentEventMappers _contentEventMappers;
        private readonly IArticleOrderService _articleOrderService;
        private readonly IContentMappers _contentMappers;

        public ContentEventRecurrenceService(
            IContentEventOccurenceRepository contentEventOccurrenceRepository,
            IContentEventRepository contentEventRepository,
            IContentRepository contentRepository,
            ISolrContentService solrContentService,
            IContentEventMappers contentEventMappers,
            IArticleOrderService articleOrderService,
            IContentMappers contentMappers)
        {
            _contentEventOccurrenceRepository = contentEventOccurrenceRepository;
            _contentEventRepository = contentEventRepository;
            _contentRepository = contentRepository;
            _solrContentService = solrContentService;
            _contentEventMappers = contentEventMappers;
            _articleOrderService = articleOrderService;
            this._contentMappers = contentMappers;
        }

        public async Task<int> CreateOccurrences(ContentEvent contentEvent)
        {
            await _contentEventOccurrenceRepository.DeleteForEventId(contentEvent.Id);

            if (contentEvent.Recurrence == null) return 0;

            contentEvent.Recurrence.Init();
            Guard.Against.InvalidEventRecurrence(contentEvent.Recurrence);

            // WEB-2740: Compile more useful information about ArgumentOutOfRange exceptions
            try
            {
                var occurrenceDates = new Calculator().CalculateOccurrences(contentEvent.Recurrence).ToList();
                var occurrences = occurrenceDates.Select(date =>
                EventMappers.MapToContentEventOccurrence(date, contentEvent.Recurrence, contentEvent));

                return await _contentEventOccurrenceRepository.SaveOccurrences(occurrences);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                _log.Error(
                    "Content event reccurrence calculation exception.\n"
                    + $"Event: {JsonConvert.SerializeObject(contentEvent.Recurrence)}",
                    ex);
            }

            return 0;
        }

        public async Task DeleteOccurrence(int occurrenceId, AppUser user)
        {
            var occurrence = await _contentEventOccurrenceRepository.GetByOccurrenceId(occurrenceId);
            var contentEvent = await _contentEventRepository.GetById(occurrence.EventId);
            var dbContent = await _contentRepository.GetByCode(contentEvent.ContentCode, user.Email);

            var solrOccurrence = _contentEventMappers.MapToSolrEventOccurrence(
                dbContent, contentEvent, occurrence);

            await _contentEventOccurrenceRepository.DeleteOccurrence(occurrenceId);
            await _solrContentService.Delete(solrOccurrence.Id);

            await _articleOrderService.RemoveContentFromOrder(await _contentMappers.MapToContentAsync(dbContent, user), new List<string> { $"{dbContent.Id}_{occurrenceId}" });
        }

        public async Task DeleteAllForContentCode(string contentCode, AppUser user)
        {
            var contentEvent = await _contentEventRepository.GetForContentByCode(contentCode);
            
            if (contentEvent == null) { return; }

            var dbContent = await _contentRepository.GetByCode(contentCode, user.Email);

            contentEvent.Recurrence = null;
            await _contentEventRepository.Update(contentEvent);

            await _contentEventOccurrenceRepository.DeleteForEventId(contentEvent.Id);
            await _solrContentService.PostContent(dbContent);
        }

        public bool IsValid(EventRecurrence eventRecurrence, out string message)
        {
            message = Validate(eventRecurrence);
            return string.IsNullOrEmpty(message);
        }

        public string Validate(EventRecurrence eventRecurrence)
        {
            eventRecurrence.Init();
            return new Calculator().ValidateRecurrence(eventRecurrence);
        }
    }
}
