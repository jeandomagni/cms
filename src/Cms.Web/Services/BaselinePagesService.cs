﻿using Ardalis.GuardClauses;
using Cms.Domain.BaselinePageGuardClauses;
using Cms.Domain.Model;
using Cms.Model.Models.BaselinePages;
using Cms.Model.Models.Query.KendoGrid;
using Cms.Model.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cms.Web.Services
{
    public interface IBaselinePagesService
    {
        Task<BaselinePage> CreateShortlink(BaselinePage model);
        Task<IEnumerable<BaselinePage>> PaginateBaselinePages(GridOptions options, bool getShortlinks);
        Task<BaselinePage> UpdateBaselinePage(BaselinePage model);
        Task<int> DeleteBaselinePage(int id);
        Task<BaselinePage> GetById(int id);
        Task<BaselinePage> UpdateShortlink(BaselinePage model);
        Task<BaselinePage> CreateBaselinePage(BaselinePage model);
        Task<bool> IsSegmentUrlExists(string urlSegment, int? baselinePageId);
        Task<List<BaselinePage>> GetAll();
    }
    public class BaselinePagesService : IBaselinePagesService
    {
        private readonly IBaselinePagesRepository _baselinePagesRepository;
        private readonly IContentService _contentService;

        public BaselinePagesService(IBaselinePagesRepository baselinePagesRepository, IContentService contentService)
        {
            _baselinePagesRepository = baselinePagesRepository;
            _contentService = contentService;
        }

        public Task<bool> IsSegmentUrlExists(string urlSegment, int? baselinePageId)
        {
            return _baselinePagesRepository.IsSegmentUrlExists(urlSegment, baselinePageId);
        }

        public async Task<BaselinePage> CreateBaselinePage(BaselinePage model)
        {
            Guard.Against.RecordNotFound(model);
            Guard.Against.SegmentUrlNotFound(model);

            var baselinePageSegmentUrlExist = await _baselinePagesRepository.IsSegmentUrlExists(model.UrlSegment, model.Id);
            Guard.Against.SegmentUrlDuplication(baselinePageSegmentUrlExist);            

            return await _baselinePagesRepository.CreateBaselinePage(model);
        }

        public async Task<BaselinePage> CreateShortlink(BaselinePage model)
        {
            Guard.Against.RecordNotFound(model);
            Guard.Against.SegmentUrlNotFound(model);
            Guard.Against.RedirectUrlNotFound(model);
            Guard.Against.SegmentUrlEqualToRedirectUrl(model);

            var baselinePageSegmentUrlExist = await _baselinePagesRepository.IsSegmentUrlExists(model.UrlSegment, model.Id);
            Guard.Against.SegmentUrlDuplication(baselinePageSegmentUrlExist);

            return await _baselinePagesRepository.CreateBaselinePage(model);
        }

        public async Task<int> DeleteBaselinePage(int id)
        {
            return await _baselinePagesRepository.DeleteBaselinePage(id);
        }

        public Task<BaselinePage> GetById(int id)
        {
            return _baselinePagesRepository.GetById(id);
        }

        public Task<IEnumerable<BaselinePage>> PaginateBaselinePages(GridOptions options, bool getShortlinks)
        {
            return _baselinePagesRepository.PaginateShortlinks(options, getShortlinks);
        }

        public async Task<BaselinePage> UpdateBaselinePage(BaselinePage model)
        {
            Guard.Against.RecordNotFound(model);
            Guard.Against.SegmentUrlNotFound(model);

            var baselinePageSegmentUrlExist = await _baselinePagesRepository.IsSegmentUrlExists(model.UrlSegment, model.Id);
            Guard.Against.SegmentUrlDuplication(baselinePageSegmentUrlExist);

            return await _baselinePagesRepository.UpdateBaselinePage(model);
        }

        public async Task<BaselinePage> UpdateShortlink(BaselinePage model)
        {
            Guard.Against.RecordNotFound(model);
            Guard.Against.SegmentUrlNotFound(model);
            Guard.Against.RedirectUrlNotFound(model);
            Guard.Against.SegmentUrlEqualToRedirectUrl(model);

            var baselinePageSegmentUrlExist = await _baselinePagesRepository.IsSegmentUrlExists(model.UrlSegment, model.Id);
            Guard.Against.SegmentUrlDuplication(baselinePageSegmentUrlExist);

            return await _baselinePagesRepository.UpdateBaselinePage(model);
        }

        public Task<List<BaselinePage>> GetAll()
        {
            return _baselinePagesRepository.GetAll();
        }
    }
}
