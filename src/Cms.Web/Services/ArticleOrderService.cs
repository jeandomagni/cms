﻿using Cms.Domain.Cache;
using Cms.Domain.Enum;
using Cms.Domain.Mappers;
using Cms.Domain.Services.Solr;
using Cms.Model.Models.Content;
using Cms.Model.Models.Content.Db;
using Cms.Model.Models.Content.Entities;
using Cms.Model.Models.Query;
using Cms.Model.Repositories;
using Cms.Model.Repositories.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ContentModel = Cms.Model.Models.Content.Content;

namespace Cms.Web.Services
{
    public interface IArticleOrderService
    {
        Task<ArticleOrder> Get(int id);
        Task<ArticleOrder> Get(ArticleOrderParams articleOrderParams);
        Task<ArticleOrder> GetOrCreate(ArticleOrderParams articleOrderParams);
        Task UpdateOrderMultiple(List<ArticleOrder> articleOrdersToUpdate);
        Task<ArticleOrder> Create(ArticleOrderParams articleOrderParams);
        Task AddContentToOrder(ContentModel content, List<string> solrIds);
        Task RemoveContentFromOrder(ContentModel result, List<string> solrIds);
        Task<List<ArticleOrder>> AddMultiple(List<ArticleOrder> articleOrders);
        Task DeleteFromLocation(ContentModel content, Geotag geotag);
        Task DeleteContent(ArticleOrder articleOrder, string id);
        Task<ArticleOrder> Get(ArticleOrder articleOrder);
        Task DeleteTopicFromContentOrders(ContentModel content, string topic);
        Task RemoveContentFromOrder(string id);
        Task SwitchToServiceAreaNewsfeed(ArticleOrderParams orderParams);
        Task SwitchToCountryNewsfeed(ArticleOrderParams orderParams);
        Task ClearNewsfeedOverride(ArticleOrderParams orderParams);
    }
    public class ArticleOrderService : IArticleOrderService
    {
        private readonly IArticleOrderRepository _articleOrderRepository;
        private readonly ISolrArticleOrderService _solrArticleOrderService;
        private readonly IArticleOrderMappers _articleOrderMappers;
        private readonly IListinngTypeToArticleTypeMappingService _listinngTypeToArticleTypeMappingService;
        private readonly IArticleOrderSummaryService _articleOrderSummaryService;
        private readonly ISolrContentService _solrContentService;
        private readonly ISolrLocationService _solrLocationService;
        private readonly ILocationService _locationService;
        private readonly IBbbCache _bbbCache;

        public ArticleOrderService(IArticleOrderRepository articleOrderRepository, ISolrArticleOrderService solrArticleOrderService, IArticleOrderMappers articleOrderMappers, IListinngTypeToArticleTypeMappingService listinngTypeToArticleTypeMappingService, IArticleOrderSummaryService articleOrderSummaryService, ISolrContentService solrContentService, ISolrLocationService solrLocationService, ILocationService locationService, IBbbCache bbbCache)
        {
            this._articleOrderRepository = articleOrderRepository;
            this._solrArticleOrderService = solrArticleOrderService;
            this._articleOrderMappers = articleOrderMappers;
            this._listinngTypeToArticleTypeMappingService = listinngTypeToArticleTypeMappingService;
            this._articleOrderSummaryService = articleOrderSummaryService;
            this._solrContentService = solrContentService;
            this._solrLocationService = solrLocationService;
            this._locationService = locationService;
            this._bbbCache = bbbCache;
        }

        public async Task<List<ArticleOrder>> AddMultiple(List<ArticleOrder> articleOrders)
        {
            if (articleOrders?.Count > 0)
            {

                List<ArticleOrder> result = await _articleOrderRepository.InsertMultiple(articleOrders);

                //var solrArticleOrder = result.Select(x => _articleOrderMappers.MapToSolrArticleOrder(x)).ToList();
                //await _solrArticleOrderService.AddOrUpdateMultiple(solrArticleOrder);
                return result;
            }

            return new List<ArticleOrder>();
        }

        public Task<ArticleOrder> Get(int id)
        {
            return _articleOrderRepository.Get(id);
        }

        public async Task<ArticleOrder> Get(ArticleOrder articleOrder)
        {
            return (await _articleOrderRepository.GetBySeveralFilters(new List<ArticleOrder> { articleOrder })).FirstOrDefault();
        }

        public async Task<ArticleOrder> Get(ArticleOrderParams articleOrderParams)
        {
            var result = await _articleOrderRepository.GetBySeveralFilters(new List<ArticleOrder> { _articleOrderMappers.MapToArticleOrder(articleOrderParams) });

            result = await GetResultIfThereIsOverride(result);

            if (result.Count == 0 && !string.IsNullOrWhiteSpace(articleOrderParams.City))
            {
                result = await GetServiceAreaNewsfeed(articleOrderParams);
            }

            return result.FirstOrDefault();
        }

        private async Task<List<ArticleOrder>> GetResultIfThereIsOverride (List<ArticleOrder> result)
        {
            var overrideFeedId = result.FirstOrDefault ()?.OverrideFeedId;
            bool stop = overrideFeedId == null;
            int count = 0;
            while (!stop && count < 10)
            {
                var overrideFeed = await _articleOrderRepository.Get (overrideFeedId.Value);
                stop = overrideFeed.OverrideFeedId == null;
                if (overrideFeed.OverrideFeedId != null) overrideFeedId = overrideFeed.OverrideFeedId;
                overrideFeed.OverrideFeedId = overrideFeedId;
                result = new List<ArticleOrder> { overrideFeed };

                count++;
            }

            return result;
        }

        private async Task<List<ArticleOrder>> GetServiceAreaNewsfeed(ArticleOrderParams articleOrderParams)
        {
            var country = (await _locationService.GetAllStates()).FirstOrDefault(x => x.State == articleOrderParams.State).CountryCode;
            if (_solrLocationService.TryGetBbbIdByCityState(articleOrderParams.City, articleOrderParams.State, country, out var cityBbbId))
            {
                return await _articleOrderRepository.GetBySeveralFilters(new List<ArticleOrder> { new ArticleOrder { ListingType = articleOrderParams.ListingType, ServiceArea = cityBbbId, Topic = articleOrderParams.Topic } });
            }

            return new List<ArticleOrder>();
        }

        public async Task<ArticleOrder> Create(ArticleOrderParams articleOrderParams)
        {
            var articleOrder = _articleOrderMappers.MapToArticleOrder(articleOrderParams);
            return (await AddMultiple(new List<ArticleOrder> { articleOrder })).FirstOrDefault();
        }

        public async Task AddContentToOrder(ContentModel content, List<string> solrIds)
        {
            var articleOrders = await GetArticleOrdersFromContent(content);
            string id = string.Join(",", solrIds);
            var isEvent = content.Type == ContentType.Event;

            if (articleOrders.Count() > 0)
            {
                var now = DateTime.UtcNow;
                foreach (var item in articleOrders)
                {
                    if (item.Id != 0)
                    {
                        if (solrIds.Any(x => !item.OrderedArticleIdsAsArray.Contains(x)))
                        {
                            item.ModifiedDate = now;
                            if (!string.IsNullOrWhiteSpace(item.OrderedArticleIds))
                            {
                                item.OrderedArticleIds = $"{id},{item.OrderedArticleIds}";

                                if (isEvent && item.ListingType == ContentType.Event)
                                {
                                    item.OrderedArticleIdsAsArray = await _solrContentService.GetFutureOreredEventIdsByIds(item.OrderedArticleIdsAsArray.Distinct().ToList());
                                }
                            }
                            else
                            {
                                item.OrderedArticleIds = id;
                            }
                        }
                    }
                    else
                    {
                        item.OrderedArticleIds = id;
                    }
                }

                await UpdateOrderMultiple(articleOrders.Where(x => x.Id != 0 && x.ModifiedDate == now).ToList());
                await AddMultiple(articleOrders.Where(x => x.Id == 0 && !x.NotTagged).ToList());
            }
        }

        public async Task RemoveContentFromOrder(ContentModel content, List<string> solrIds)
        {
            var articleOrders = await GetArticleOrdersFromContent(content, true);

            if (articleOrders.Count() > 0)
            {
                string contentId = content.Id.ToString();
                bool recurringEvent = content.Type == ContentType.Event && solrIds.All(x => x.Contains($"{contentId}_"));
                if (recurringEvent)
                {
                    contentId = $"{contentId}_";
                }
                var articleOrdersToUpdate = articleOrders.Where(x => x.Id != 0 && x.OrderedArticleIdsAsArray.Any(y => y.IndexOf(contentId) > -1)).ToList();

                foreach (var item in articleOrdersToUpdate)
                {
                    item.ModifiedDate = DateTime.UtcNow;
                    if (recurringEvent)
                    {
                        item.OrderedArticleIdsAsArray = item.OrderedArticleIdsAsArray.Where(x => !x.Contains(contentId)).ToList();
                    }
                    else
                    {
                        item.OrderedArticleIdsAsArray = item.OrderedArticleIdsAsArray.Where(x => x != contentId).ToList();
                    }
                }

                await UpdateOrderMultiple(articleOrdersToUpdate);
            }
        }

        public async Task UpdateOrderMultiple(List<ArticleOrder> articleOrdersToUpdate)
        {
            if (articleOrdersToUpdate?.Count > 0)
            {
                await _articleOrderRepository.UpdateOrderMultiple(articleOrdersToUpdate);
                //await _solrArticleOrderService.AddOrUpdateMultiple(articleOrdersToUpdate.Select(x => _articleOrderMappers.MapToSolrArticleOrder(x)).ToList());
            }
        }

        private async Task<List<ArticleOrder>> GetArticleOrdersFromContent(ContentModel content, bool all = false)
        {
            var mappings = await _listinngTypeToArticleTypeMappingService.Get();
            var listingTypes = mappings?.FirstOrDefault(x => x.ArticleType == content.Type.ToLower())?.ListingTypes;

            if (listingTypes?.Count() > 0)
            {
                var articleOrdersWithoutListingType = new List<ArticleOrder>();

                if (content?.States != null) articleOrdersWithoutListingType.AddRange(content.States.Select(x => new ArticleOrder { State = x.LocationId }));
                if (content?.PinnedStates != null) articleOrdersWithoutListingType.AddRange(content.PinnedStates.Select(x => new ArticleOrder { State = x.LocationId }));
                if (content?.BbbIds != null) articleOrdersWithoutListingType.AddRange(content.BbbIds.Select(x => new ArticleOrder { ServiceArea = x.LocationId }));
                if (content?.PinnedBbbIds != null) articleOrdersWithoutListingType.AddRange(content.PinnedBbbIds.Select(x => new ArticleOrder { ServiceArea = x }));
                if (content?.Cities != null) articleOrdersWithoutListingType.AddRange(content.Cities.Where(x => !string.IsNullOrWhiteSpace(x.City)).Select(x => new ArticleOrder { City = x.City.ToLower(), State = x.State, Country = x.CountryCode }));
                if (content?.PinnedCities != null) articleOrdersWithoutListingType.AddRange(content.PinnedCities.Where(x => !string.IsNullOrWhiteSpace(x.City)).Select(x => new ArticleOrder { City = x.City.ToLower(), State = x.State, Country = x.CountryCode }));
                if (content.EventBbb != null) articleOrdersWithoutListingType.Add(new ArticleOrder { ServiceArea = content.EventBbb.LegacyBBBID });

                if ((all || articleOrdersWithoutListingType.Count == 0) && content?.Countries != null) articleOrdersWithoutListingType.AddRange(content.Countries.Select(x => new ArticleOrder { Country = x.LocationId }));

                if (content.Type == ContentType.Event)
                {
                    if (content?.Sites != null) articleOrdersWithoutListingType.AddRange(content.Sites.Select(x => new ArticleOrder { ServiceArea = x.LegacyBBBID }));
                }
                if (content?.PinnedLocations != null && content?.PinnedLocations.Any(x => x.LocationType == LocationTypes.Country) == true) articleOrdersWithoutListingType.AddRange(content?.PinnedLocations.Where(x => x.LocationType == LocationTypes.Country).Select(x => new ArticleOrder { Country = x.PinnedLocation }));

                List<ArticleOrder> articleOrders = await ApplyListingTypesAndTopics(content, listingTypes, articleOrdersWithoutListingType);
                List<ArticleOrder> dbArticleOrders = await _articleOrderRepository.GetBySeveralFilters(articleOrders);

                foreach (var item in articleOrders)
                {
                    var ex = dbArticleOrders.FirstOrDefault(x => x.Topic == item.Topic && x.ListingType == item.ListingType && x.City == item.City && x.State == item.State && x.ServiceArea == item.ServiceArea &&
                       ((item.IsCountryLevel() && x.Country == item.Country) || !item.IsCountryLevel()));

                    if (ex != null)
                    {
                        item.Id = ex.Id;
                        item.Country = ex.Country;
                        item.OrderedArticleIds = ex.OrderedArticleIds;
                    }
                }

                return articleOrders;
            }
            return new List<ArticleOrder>();
        }

        private async Task DeleteFromLocation(ContentModel content, Func<ArticleOrder> map)
        {
            var mappings = await _listinngTypeToArticleTypeMappingService.Get();
            var listingTypes = mappings.FirstOrDefault(x => x.ArticleType == content.Type.ToLower())?.ListingTypes;

            if (listingTypes?.Count() > 0)
            {
                var articleOrdersWithoutListingType = new List<ArticleOrder> { map() };
                List<ArticleOrder> articleOrders = await ApplyListingTypesAndTopics(content, listingTypes, articleOrdersWithoutListingType);

                articleOrders = await _articleOrderRepository.GetBySeveralFilters(articleOrders);

                var contentId = content.Id.ToString();
                var update = articleOrders.Where(x => x.OrderedArticleIdsAsArray.Contains(contentId)).ToList();
                foreach (var item in update)
                {
                    item.OrderedArticleIdsAsArray = item.OrderedArticleIdsAsArray.Where(x => x != contentId).ToList();
                }

                await UpdateOrderMultiple(update);
            }
        }

        private async Task<List<ArticleOrder>> ApplyListingTypesAndTopics(ContentModel content, string[] listingTypes, List<ArticleOrder> articleOrdersWithoutListingType)
        {
            var bbbIds = articleOrdersWithoutListingType.Where(x => !string.IsNullOrWhiteSpace(x.ServiceArea)).Select(x => x.ServiceArea).ToList();

            if (bbbIds.Count > 0)
            {
                var locationsForBbbs = await _solrLocationService.GetCitiesForBbb(bbbIds);
                if (locationsForBbbs.Count > 0)
                {
                    var locationsFromBbb = locationsForBbbs.Select(x => new ArticleOrder { City = x.City, State = x.State.ToUpper(), Country = x.CountryCode.ToString() }).ToList();

                    foreach (var item in locationsFromBbb)
                    {
                        if (!articleOrdersWithoutListingType.Any(x => x.City == item.City && x.State == item.State))
                        {
                            item.NotTagged = true;
                            articleOrdersWithoutListingType.Add(item);
                        }
                    }

                }
            }

            var topics = new List<string> { null };
            topics.AddRange(content.Topics);
            topics = topics.Distinct().ToList();

            var articleOrders = new List<ArticleOrder>();
            foreach (var lt in listingTypes)
            {
                var list = articleOrdersWithoutListingType.Where(x => !string.IsNullOrEmpty(x.State) || !string.IsNullOrEmpty(x.City) || !string.IsNullOrEmpty(x.Country) || !string.IsNullOrEmpty(x.ServiceArea));
                foreach (var item in list)
                {
                    foreach (var topic in topics)
                    {
                        articleOrders.Add(new ArticleOrder { City = item.City, State = item.State, Country = item.Country, ServiceArea = item.ServiceArea, ListingType = lt, Topic = topic ?? "", NotTagged = item.NotTagged });
                    }
                }
            }

            return articleOrders;
        }

        public async Task DeleteFromLocation(ContentModel content, Geotag geotag)
        {
            await DeleteFromLocation(content, () => _articleOrderMappers.MapToArticleOrder(geotag));
        }

        public async Task DeleteContent(ArticleOrder articleOrder, string id)
        {
            if (articleOrder.OrderedArticleIdsAsArray.Contains(id))
            {
                articleOrder.OrderedArticleIdsAsArray = articleOrder.OrderedArticleIdsAsArray.Where(x => x != id).ToList();
                await UpdateOrderMultiple(new List<ArticleOrder> { articleOrder });
            }
        }

        public async Task DeleteTopicFromContentOrders(ContentModel content, string topic)
        {
            var articleOrders = await GetArticleOrdersFromContent(content, true);

            if (articleOrders.Count() > 0)
            {
                articleOrders = articleOrders.Where(x => x.Topic == topic).ToList(); // Filter article orders by topic that we want to remove
                var contentId = content.Id.ToString();
                var update = articleOrders.Where(x => x.OrderedArticleIdsAsArray.Contains(contentId)).ToList();
                foreach (var item in update)
                {
                    item.OrderedArticleIdsAsArray = item.OrderedArticleIdsAsArray.Where(x => x != contentId).ToList();
                }

                await UpdateOrderMultiple(update);
            }
        }

        public async Task<ArticleOrder> GetOrCreate(ArticleOrderParams articleOrder)
        {
            var storedArticleOrder = await Get(articleOrder);

            if (!string.IsNullOrWhiteSpace(articleOrder.City) && string.IsNullOrWhiteSpace(storedArticleOrder.City))
            {
                var order = storedArticleOrder.OrderedArticleIdsAsArray;
                storedArticleOrder = await Create(articleOrder);
                storedArticleOrder.OrderedArticleIdsAsArray = order;
            }

            return storedArticleOrder;
        }

        public async Task RemoveContentFromOrder(string id)
        {
            var articleOrders = await GetArticleOrdersByContentId(id);
            foreach (var item in articleOrders)
            {
                item.OrderedArticleIdsAsArray = item.OrderedArticleIdsAsArray.Where(x => x != id).ToList();
            }

            await UpdateOrderMultiple(articleOrders);
        }

        private Task<List<ArticleOrder>> GetArticleOrdersByContentId(string id)
        {
            return _articleOrderRepository.GetByContentId(id);
        }

        public async Task OverrideFeed(ArticleOrderParams orderParams, int? overrideFeedId)
        {
            var mappedArticleOrder = _articleOrderMappers.MapToArticleOrder(orderParams);

            var articleOrder = await Get(mappedArticleOrder);
            if (articleOrder != null)
            {
                await _articleOrderRepository.OverrideFeed(articleOrder.Id, overrideFeedId);
            }
            else
            {
                await AddMultiple(new List<ArticleOrder> { mappedArticleOrder });
            }
        }

        public async Task SwitchToServiceAreaNewsfeed(ArticleOrderParams orderParams)
        {
            var serviceAreaNewsfeed = (await GetServiceAreaNewsfeed(orderParams)).FirstOrDefault();
            await OverrideFeed(orderParams, serviceAreaNewsfeed.Id);
        }

        public async Task SwitchToCountryNewsfeed(ArticleOrderParams orderParams)
        {
            var allServiceAreas = await _bbbCache.GetAllServiceAreasAsync();
            var serviceArea = allServiceAreas.FirstOrDefault(x => x.BbbId == orderParams.ServiceArea);
            var countryArticleOrder = await Get(new ArticleOrderParams { Country = serviceArea.PrimaryCountry, ListingType = orderParams.ListingType, Topic = orderParams.Topic });
            await OverrideFeed(orderParams, countryArticleOrder.Id);
        }

        public async Task ClearNewsfeedOverride(ArticleOrderParams orderParams)
        {
            await OverrideFeed(orderParams, null);
        }
    }
}
