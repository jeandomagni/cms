﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Text;

namespace Cms.Web.Services
{
    public static class TokenValidationService
    {
        public static string SigningKey;

        public static TokenValidationParameters GetValidationParameters()
        {
            var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(SigningKey));
            return new TokenValidationParameters
            {
                // The signing key must match!
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = signingKey,

                // Validate the JWT Issuer (iss) claim
                ValidateIssuer = true,
                ValidIssuer = "BBBCouncil",

                // Validate the JWT Audience (aud) claim
                ValidateAudience = true,
                ValidAudience = "BBB",

                // Validate the token expiry
                ValidateLifetime = true,

                // If you want to allow a certain amount of clock drift, set that here:
                ClockSkew = TimeSpan.Zero
            };
        }
    }
}
