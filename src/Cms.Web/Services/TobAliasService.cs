﻿using Ardalis.GuardClauses;
using Cms.Domain.Mappers;
using Cms.Domain.SecTermGuardClauses;
using Cms.Domain.Services.Solr;
using Cms.Model.Models.SecTerm;
using Cms.Model.Repositories.SecTerms;
using System.Threading.Tasks;

namespace Cms.Web.Services
{
    public interface ITobAliasService
    {
        Task<int> Delete(AddEditAliasParams addEditAliasParams);
        Task<TobAlias> Add(AddEditAliasParams addEditAliasParams);
        Task<TobAlias> Update(AddEditAliasParams tobAlias);
    }

    public class TobAliasService : ITobAliasService
    {
        private ITobAliasRepository _tobAliasRepository;
        private ISolrSearchTypeaheadService _solrService;

        public TobAliasService(ITobAliasRepository tobAliasRepository, ISolrSearchTypeaheadService solrService)
        {
            _tobAliasRepository = tobAliasRepository;
            _solrService = solrService;
        }
        
        public async Task<int> Delete(AddEditAliasParams addEditAliasParams)
        {
            var aliasId = (int) addEditAliasParams.Alias.AliasId;
            await _solrService.DeleteAlias(addEditAliasParams);
            return await _tobAliasRepository.DeleteByAliasId(aliasId);
        }

        public async Task<TobAlias> Add(AddEditAliasParams addEditAliasParams)
        {
            Guard.Against.EmptyOrInvalidAliasName(addEditAliasParams.Alias);
            Guard.Against.DuplicateAlias(await _tobAliasRepository.GetByName(addEditAliasParams.Alias.Name, addEditAliasParams.Alias.Spanish));

            var coreTobAlias = addEditAliasParams.ToCoreTobAlias();
            addEditAliasParams.Alias = (await _tobAliasRepository.Add(coreTobAlias)).ToTobAlias();
            await _solrService.UpdateAlias(addEditAliasParams);
            return addEditAliasParams.Alias;
        }

        public async Task<TobAlias> Update(AddEditAliasParams addEditAliasParams)
        {
            Guard.Against.EmptyOrInvalidAliasName(addEditAliasParams.Alias);

            var coreTobAlias = addEditAliasParams.ToCoreTobAlias();
            addEditAliasParams.Alias = (await _tobAliasRepository.Update(coreTobAlias)).ToTobAlias();
            await _solrService.UpdateAlias(addEditAliasParams);
            return addEditAliasParams.Alias;
        }
    }
}
