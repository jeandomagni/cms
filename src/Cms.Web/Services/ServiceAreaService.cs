﻿using Bbb.Core.Utilities.Extensions;
using Cms.Domain.Cache;
using Cms.Domain.Enum;
using Cms.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SolrLocation = Bbb.Core.Solr.Model.Location;

namespace Cms.Web.Services
{
    public interface IServiceAreaService
    {
        Task<string> GetCountryForBranchState(string state);
        Task<IEnumerable<string>> GetServiceableCountriesForUser(AppUser currentUser);
        Task<IEnumerable<string>> GetServiceableStatesForUser(AppUser currentUser);
        Task<bool> IsServiceableCountryForUser(string country, AppUser currentUser);
        Task<bool> IsServiceableStateForUser(string stateCode, AppUser currentUser);
        Task<bool> IsServiceableCityForUser(SolrLocation city, AppUser currentUser);
        Task<bool> IsServiceableBbbIdForUser(string bbbId, AppUser currentUser);
    }

    public class ServiceAreaService : IServiceAreaService
    {
        private readonly ILegacyBbbSiteCache _bbbSiteCache;
        private readonly IBbbCache _bbbCache;
        private readonly IStateProvinceCache _stateProvinceCache;

        public ServiceAreaService(
            ILegacyBbbSiteCache bbbSiteCache,
            IBbbCache bbbCache,
            IStateProvinceCache stateProvinceCache)
        {
            _bbbSiteCache = bbbSiteCache;
            _bbbCache = bbbCache;
            _stateProvinceCache = stateProvinceCache;
        }

        public async Task<IEnumerable<string>> GetServiceableCountriesForUser(AppUser currentUser)
        {
            var allSites = await _bbbSiteCache.GetAllForUserLocationsAsync(currentUser);

            if (allSites.IsNullOrEmpty())
            {
                return new List<string>();
            }

            var userBbbIds = allSites.Select(userSite => userSite.LegacyBBBID).ToArray();

            return (await _bbbCache.GetAllServiceAreasAsync())
                .Where(x => userBbbIds.Contains(x.BbbId))
                .SelectMany(x => x.Countries)
                .Where(x => !x.Contains(',')) // goodnight bad data
                .Distinct()
                .OrderBy(x => x);
        }

        public async Task<IEnumerable<string>> GetServiceableStatesForUser(AppUser currentUser)
        {
            var allSites = await _bbbSiteCache.GetAllForUserLocationsAsync(currentUser);

            if (allSites.IsNullOrEmpty())
            {
                return new List<string>();
            }

            var userBbbIds = allSites.Select(userSite => userSite.LegacyBBBID).ToArray();

            return (await _bbbCache.GetAllServiceAreasAsync())
                .Where(x => userBbbIds.Contains(x.BbbId))
                .SelectMany(x => x.States ?? new List<string>())
                .Distinct()
                .OrderBy(x => x);
        }

        public async Task<bool> IsServiceableCountryForUser(string country, AppUser currentUser)
        {
            if (currentUser.GlobalOrLocalAdmin)
            {
                return true;
            }

            var supportedCountries = await GetServiceableCountriesForUser(currentUser);
            return supportedCountries.Contains(country);
        }

        public async Task<bool> IsServiceableStateForUser(string stateCode, AppUser currentUser)
        {
            if (currentUser.GlobalOrLocalAdmin)
            {
                return true;
            }

            var supportedStates = await GetServiceableStatesForUser(currentUser);
            return supportedStates.Contains(stateCode);
        }

        public async Task<bool> IsServiceableCityForUser(SolrLocation city, AppUser currentUser)
        {
            if (currentUser.GlobalOrLocalAdmin)
            {
                return true;
            }

            var allSites = await _bbbSiteCache.GetAllForUserLocationsAsync(currentUser);

            if (allSites.IsNullOrEmpty())
            {
                return false;
            }

            var userBbbIds = allSites.Select(site => site.LegacyBBBID);
            return userBbbIds.Any(bbbId => city.BbbIds.Contains(bbbId));
        }

        public async Task<string> GetCountryForBranchState(string state)
        {
            return await _stateProvinceCache.GetCountryCodeForStateCodeAsync(state);
        }

        public async Task<bool> IsServiceableBbbIdForUser(string bbbId, AppUser currentUser)
        {
            if (currentUser.GlobalOrLocalAdmin)
            {
                return true;
            }

            var allSites = await _bbbSiteCache.GetAllForUserLocationsAsync(currentUser);

            if (allSites.IsNullOrEmpty())
            {
                return false;
            }

            var userBbbIds = allSites.Select(userSite => userSite.LegacyBBBID);
            return userBbbIds.Contains(bbbId);
        }
    }
}
