﻿using Cms.Domain.Enum;
using Cms.Domain.Mappers;
using Cms.Domain.Services.Solr;
using Cms.Model.Models;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SolrLocation = Bbb.Core.Solr.Model.Location;

namespace Cms.Web.Services
{
    public interface ILocationService
    {
        Task<IEnumerable<Location>> GetAllStates();
    }

    public class LocationService : ILocationService
    {
        private readonly IMemoryCache _cache;
        private readonly ISolrLocationService _solrLocationService;

        public LocationService(IMemoryCache cache, ISolrLocationService solrLocationService)
        {
            _cache = cache;
            _solrLocationService = solrLocationService;
        }

        public async Task<IEnumerable<Location>> GetAllStates()
        {
            var cachedStates = _cache.Get<IEnumerable<Location>>(CacheKeys.StateProvinces);

            if (cachedStates == null)
            {
                var solrStates = await _solrLocationService.GetAllStateProvinces();
                _cache.Set(
                    CacheKeys.StateProvinces,
                    solrStates.Select(LocationMappers.MapToLocation),
                    TimeSpan.FromHours(12));

                cachedStates = _cache.Get<IEnumerable<Location>>(CacheKeys.StateProvinces);
            }

            return cachedStates;
        }
    }
}
