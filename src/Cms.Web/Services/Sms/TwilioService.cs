﻿using Cms.Domain.Services;
using Cms.Model.Models.Config;
using log4net;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Rest.Lookups.V1;

namespace Cms.Web.Services.Sms
{
    public interface ITwilioService
    {
        Task<PhoneNumberResource> LookupPhoneNumberAsync(string phoneNumber);
        Task SendSms(string phoneNumber, string code);
    }

    public class TwilioService : ITwilioService
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly TwilioSettings _twilioSettings;

        public TwilioService(IOptions<TwilioSettings> config)
        {
            _twilioSettings = config.Value;
            try
            {
                TwilioClient.Init(_twilioSettings.Sid, _twilioSettings.Token);
            }
            catch
            {
            }
        }

        public async Task<PhoneNumberResource> LookupPhoneNumberAsync(string phoneNumber)
        {
            return await PhoneNumberResource.FetchAsync(
                new Twilio.Types.PhoneNumber(phoneNumber));
        }

        public async Task SendSms(string phoneNumber, string code)
        {
            try
            {
                var message = await MessageResource.CreateAsync(
                    body: $"CoreCMS: Thanks for verifying your account. Your code is {code}.",
                    from: new Twilio.Types.PhoneNumber(_twilioSettings.Phone),
                    to: new Twilio.Types.PhoneNumber(phoneNumber)
                );
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                throw ex;
            }
        }
    }
}