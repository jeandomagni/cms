﻿using Cms.Domain.Exception.Models;
using log4net;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Cms.Web.Services
{
    public class ExceptionService
    {
        private readonly RequestDelegate next;
        protected readonly IHostingEnvironment _hostingEnvironment;
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public ExceptionService(
            RequestDelegate next,
            IHostingEnvironment hostingEnvironment) {
            this.next = next;
            _hostingEnvironment = hostingEnvironment;
        }

        #region Helper Methods
        private string FormatLogMessage(Exception ex, HttpContext context, ClaimsIdentity user)
        {
            var sbLogMsg = new StringBuilder();
            sbLogMsg.Append(ex.Message);
            sbLogMsg.AppendLine();
            sbLogMsg.Append($"URL: {context.Request.Path}");
            sbLogMsg.AppendLine();
            sbLogMsg.Append($"User: {user.Name}");
            return sbLogMsg.ToString();
        }

        private bool IsEnabled(LogLevel logLevel)
        {
            switch (logLevel)
            {
                case LogLevel.Critical:
                    return _log.IsFatalEnabled;
                case LogLevel.Debug:
                case LogLevel.Trace:
                    return _log.IsDebugEnabled;
                case LogLevel.Error:
                    return _log.IsErrorEnabled;
                case LogLevel.Information:
                    return _log.IsInfoEnabled;
                case LogLevel.Warning:
                    return _log.IsWarnEnabled;
                default:
                    throw new ArgumentOutOfRangeException(nameof(logLevel));
            }
        }
        #endregion

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                var user = context.User?.Identities.FirstOrDefault();

                var logLevel = ex is CmsBaseException cmsException
                    ? cmsException.LogLevel
                    : LogLevel.Error;

                if (user != null && IsEnabled(logLevel))
                {
                    var message = FormatLogMessage(ex, context, user);
                    switch (logLevel)
                    {
                        case LogLevel.Critical:
                            _log.Fatal(message, ex);
                            break;
                        case LogLevel.Debug:
                        case LogLevel.Trace:
                            _log.Debug(message, ex);
                            break;
                        case LogLevel.Error:
                            _log.Error(message, ex);
                            break;
                        case LogLevel.Information:
                            _log.Info(message, ex);
                            break;
                        case LogLevel.Warning:
                            _log.Warn(message, ex);
                            break;
                        default:
                            _log.Warn($"Encountered unknown log level {logLevel}, writing out as Info.");
                            _log.Info(message, ex);
                            break;

                    }
                }

                await HandleExceptionAsync(
                    context,
                    ex,
                    _hostingEnvironment.IsDevelopment());
            }
        }

        private static Task HandleExceptionAsync(
            HttpContext context, 
            Exception exception,
            bool isDev) {
            context.Response.ContentType = "application/json";

            if (exception is CmsBaseException cmsException) {
                context.Response.StatusCode = 
                    (int) cmsException.HttpStatusCode;
                    var result = JsonConvert.SerializeObject(new {
                        error = cmsException.ErrorMessage,
                        stackTrace =  isDev ? exception.ToString() : string.Empty,
                        message = isDev ? exception.Message : string.Empty
                });
                return 
                    context.Response.WriteAsync(result);
            }
            else {
                context.Response.StatusCode = 
                    (int)HttpStatusCode.InternalServerError;

                var result =
                    JsonConvert.SerializeObject(new {
                        message = "An internal server error has occurred",
                        stackTrace = isDev ? exception.ToString() : string.Empty
                    });

                return 
                    context.Response.WriteAsync(result);
            }
        }
    }
}
