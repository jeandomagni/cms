﻿using Cms.Model.Models.Config;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cms.Web.Filters
{
    internal class ApiAuthorizationHeader : AuthorizationHeader
    {
        private const string ApiKeyHeader = "cms-api-key";
        public IConfiguration Configuration { get; }
        public string CorecmsApiKey { get; }

        public ApiAuthorizationHeader(IOptions<AppSecurity> config)
        {
            Order = 1;
            CorecmsApiKey = config.Value.CorecmsApiKey;
        }

        public override void OnActionExecuting(ActionExecutingContext actionContext)
        {
            actionContext.HttpContext.Request.Headers.TryGetValue(ApiKeyHeader, out StringValues values);
            string apiHeaderValue = values.FirstOrDefault();
            if (!string.IsNullOrWhiteSpace(apiHeaderValue) && !string.IsNullOrWhiteSpace(CorecmsApiKey) && apiHeaderValue == CorecmsApiKey)
            {
                actionContext.RouteData.Values.TryAdd("CancelAuthorization", true);
            }
        }
    }
}
