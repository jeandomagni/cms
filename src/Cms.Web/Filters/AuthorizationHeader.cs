﻿using Cms.Web.Services;
using log4net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;

namespace Cms.Web.Filters
{
    internal class AuthorizationHeader : ActionFilterAttribute
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private const string TokenHeader = "cms-access-token";

        public AuthorizationHeader()
        {
            Order = 2;
        }

        public override void OnActionExecuting(ActionExecutingContext actionContext)
        {
            if (actionContext.RouteData.Values.ContainsKey("CancelAuthorization")) return;

            actionContext.HttpContext.Request.Headers.TryGetValue(
                TokenHeader, out StringValues values);

            var handler = new JwtSecurityTokenHandler();
            try
            {
                if (!values.Any()) {
                    throw new HttpRequestException("Missing auth header");
                }
                actionContext.HttpContext.User = 
                    handler.ValidateToken(
                        values.FirstOrDefault(),
                        TokenValidationService.GetValidationParameters(),
                        out SecurityToken validToken
                    );
            }
            catch (Exception exception) {
                _log.Warn(exception);
                actionContext.Result = new UnauthorizedResult();
            }       
        }
    }
}
