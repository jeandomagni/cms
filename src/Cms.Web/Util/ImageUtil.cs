﻿using System;
using System.IO;
using ImageMagick;

namespace Cms.Web.Util
{
    public static class ImageUtil
    {
        public static FileInfo SaveAsOptimizedImage(Stream inputStream, string outputPath)
        {
            var imageInfo = new FileInfo(outputPath);
            var ext = imageInfo.Extension.ToLower();
            var isJpeg = ext == ".jpeg" || ext == ".jpg";
            if (isJpeg)
            {
                SaveAsOptimizedJpeg(inputStream, outputPath);
            }
            else
            {
                SaveAsCompressedImage(inputStream, outputPath);
            }

            var webpOutputFilePath = Path.Combine(imageInfo.DirectoryName,
                Path.GetFileNameWithoutExtension(outputPath) + ".webp");
            SaveAsOptimizedWebP(inputStream, webpOutputFilePath);

            imageInfo.Refresh();
            return imageInfo;
        }
        private static void SaveAsOptimizedJpeg(Stream inputStream, string outputPath)
        {
            inputStream.Seek(0, SeekOrigin.Begin);
            using (var image = new MagickImage(inputStream))
            {
                image.Format = MagickFormat.Pjpeg;
                image.Quality = 60;
                using (var fs = new FileStream(outputPath, FileMode.Create))
                {
                    image.Write(fs);
                }
            }
        }

        private static void SaveAsOptimizedWebP(Stream inputStream, string outputPath)
        {
            inputStream.Seek(0, SeekOrigin.Begin);
            using (var image = new MagickImage(inputStream))
            {
                image.Format = MagickFormat.WebP;
                using (var fs = new FileStream(outputPath, FileMode.Create))
                {
                    image.Write(fs);
                }
            }
        }

        private static void SaveAsCompressedImage(Stream inputStream, string outputPath)
        {
            inputStream.Seek(0, SeekOrigin.Begin);
            using (var fileStream = File.Create(outputPath))
            {
                inputStream.Seek(0, SeekOrigin.Begin);
                inputStream.CopyTo(fileStream);
            }

            try
            {
                var optimizer = new ImageOptimizer();
                optimizer.LosslessCompress(outputPath);
            }
            catch (Exception)
            {
                File.Delete(outputPath);
                throw;
            }
            

        }
    }
}
