﻿using HtmlAgilityPack;

namespace Cms.Web.Util
{
    public sealed class HtmlUtil
    {
        public static string StripHtml(string html)
        {
            if (string.IsNullOrWhiteSpace(html)) return null;
            var document = new HtmlDocument();
            document.LoadHtml(html);
            return HtmlEntity.DeEntitize(document.DocumentNode.InnerText);
        }
    }
}
