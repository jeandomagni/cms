﻿
using Cms.Model.Models;
using Cms.Model.Models.Content;
using Cms.Model.Models.Content.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cms.Web.Extensions
{
    public static class ContentExtensions
    {
        public static void AddCity(this Content content, Location city)
        {
            if (string.IsNullOrWhiteSpace(city?.Id)) { return; }

            if (!content.Cities.Contains(city))
            {
                content.Cities.Add(city);
            }
        }

        public static bool RemoveCity(this Content content, Location city)
        {
            if (string.IsNullOrWhiteSpace(city?.Id)) { return false; }

            if (content.Cities.FirstOrDefault(x => string.Equals(x.Id, city.Id, StringComparison.InvariantCultureIgnoreCase)) != null)
            {
                content.Cities = content.Cities
                .Where(x => !string.Equals(x.Id, city.Id, StringComparison.InvariantCultureIgnoreCase))
                .ToList();
                return true;
            }

            return false;
        }

        public static bool RemoveCityPin(this Content content, Location city)
        {
            if (string.IsNullOrWhiteSpace(city?.Id)) { return false; }

            if (content.PinnedCities.FirstOrDefault(x => string.Equals(x.Id, city.Id, StringComparison.InvariantCultureIgnoreCase)) != null)
            {
                content.PinnedCities = content.PinnedCities
                .Where(x => !string.Equals(x.Id, city.Id, StringComparison.InvariantCultureIgnoreCase))
                .ToList();
                return true;
            }

            return false;
        }

        public static bool RemoveCityAndPin(this Content content, Location city)
        {
            return content.RemoveCity(city) || content.RemoveCityPin(city);
        }

        public static void AddCityPin(this Content content, Location city)
        {
            if (string.IsNullOrWhiteSpace(city?.Id)) { return; }

            if (!content.PinnedCities.Contains(city))
            {
                content.PinnedCities.Add(city);
            }
        }

        public static void AddBbbIdGeotag(this Content content, Geotag geotag)
        {
            if (geotag == null) { return; }
            
            if (content.BbbIds.FirstOrDefault(x => x.LocationId == geotag.LocationId) == null)
            {
                content.BbbIds.Add(geotag);
            }
            else
            {
                content.BbbIds = content.BbbIds.Select(x => x.LocationId == geotag.LocationId ? geotag : x).ToList();
            }
        }

        public static bool RemoveBbbIdGeotag(this Content content, Geotag geotag)
        {
            if (geotag == null) { return false; }

            if (content.BbbIds.FirstOrDefault(x => x.LocationId == geotag.LocationId) != null)
            {
                content.BbbIds = content.BbbIds.Where(x => x.LocationId != geotag.LocationId).ToList();
                return true;
            }

            return false;
        }

        public static void AddCountryGeotag(this Content content, Geotag geotag)
        {
            if (geotag == null) { return; }
            if (content.Countries == null) { content.Countries = new List<Geotag>(); }

            if (content.Countries.FirstOrDefault(x => x.LocationId == geotag.LocationId) == null)
            {
                content.Countries.Add(geotag);
            }
            else
            {
                content.Countries = content.Countries
                    .Select(x => x.LocationId == geotag.LocationId ? geotag : x)
                    .ToList();
            }
        }

        public static bool RemoveCountryGeotag(this Content content, Geotag geotag)
        {
            if (geotag == null) { return false; }

            if (content.Countries.FirstOrDefault(x => x.LocationId == geotag.LocationId) != null)
            {
                content.Countries = content.Countries
                    .Where(x => x.LocationId != geotag.LocationId)
                    .ToList();

                return true;
            }

            return false;
        }

        public static void AddStateGeotag(this Content content, Geotag geotag)
        {
            if (geotag == null) { return; }

            if (content.States.FirstOrDefault(x => x.LocationId == geotag.LocationId) == null)
            {
                content.States.Add(geotag);
            }
            else
            {
                content.States = content.States
                    .Select(x => x.LocationId == geotag.LocationId ? geotag : x)
                    .ToList();
            }
        }

        public static bool RemoveStateGeotag(this Content content, Geotag geotag)
        {
            if (geotag == null) { return false; }

            if (content.States.FirstOrDefault(x => x.LocationId == geotag.LocationId) != null)
            {
                content.States = content.States
                    .Where(x => x.LocationId != geotag.LocationId)
                    .ToList();
                return true;
            }
            return false;
        }
    }
}
