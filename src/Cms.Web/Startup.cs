﻿using Bbb.Core.Solr.Model;
using Bbb.Core.Utilities.Solr.Services;
using Cms.Domain.Cache;
using Cms.Domain.Handlers;
using Cms.Domain.Helpers;
using Cms.Domain.Mappers;
using Cms.Domain.Services;
using Cms.Domain.Services.Solr;
using Cms.Model.Enum;
using Cms.Model.Models.Config;
using Cms.Model.Repositories;
using Cms.Model.Repositories.BusinessProfileSeo;
using Cms.Model.Repositories.Content;
using Cms.Model.Repositories.SecTerms;
using Cms.Model.Services;
using Cms.Web.Filters;
using Cms.Web.Model.Security;
using Cms.Web.Security.Interfaces;
using Cms.Web.Security.Providers;
using Cms.Web.Services;
using Cms.Web.Services.Email;
using Cms.Web.Services.Sms;
using Flurl;
using log4net;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SolrNet;
using System;
using System.Linq;
using System.Reflection;
using Cms.Domain.Contracts;
using Cms.Web.Configuration;
using Cms.Web.Services.UserManagement;
using SolrArticleOrder = Bbb.Core.Solr.Model.ArticleOrder;

namespace Cms.Web
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", false, true)
                //.AddJsonFile($"appsettings.{env.EnvironmentName}.json", true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();

            LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType).Info("log4net Initialized");
        }

        public IConfigurationRoot Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAutoMapper(this.GetType().Assembly);

            // repositories
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IContentRepository, ContentRepository>();
            services.AddTransient<IContentQuoteRepository, ContentQuoteRepository>();
            services.AddTransient<IEntityRepository, EntityRepository>();
            services.AddTransient<ITagRepository, TagRepository>();
            services.AddTransient<ITagService, TagService>();
            services.AddTransient<IFileRepository, FileRepository>();
            services.AddTransient<IUserProfileRepository, UserProfileRepository>();
            services.AddTransient<ISiteContentRepository, SiteContentRepository>();
            services.AddTransient<IContentAbListRepository, ContentAbListRepository>();
            services.AddTransient<IContentMediaRepository, ContentMediaRepository>();
            services.AddTransient<IContentEventRepository, ContentEventRepository>();
            services.AddTransient<IContentNearMeRepository, ContentNearMeRepository>();
            services.AddTransient<IDomainContentRepository, DomainContentRepository>();
            services.AddTransient<IContentLocationRepository, ContentLocationRepository>();
            services.AddTransient<IUserLocationRepository, UserLocationRepository>();
            services.AddTransient<IContentReviewRepository, ContentReviewRepository>();
            services.AddTransient<IBbbInfoRepository, BbbInfoRepository>();
            services.AddTransient<IPinnedContentLocationRepository, PinnedContentLocationRepository>();
            services.AddTransient<IStateProvinceRepository, StateProvinceRepository>();
            services.AddTransient<IBusinessProfileSeoRepository, BusinessProfileSeoRepository>();
            services.AddTransient<IBaselinePagesRepository, BaselinePagesRepository>();
            services.AddTransient<IArticleOrderRepository, ArticleOrderRepository>();
            services.AddTransient<IEmailBaseService, EmailBaseService>();

            var generalConfiguration = Configuration.GetSection("General").Get<General>();

            // check config for v2 path and inject appriopriate instances
            if (generalConfiguration != null && generalConfiguration.UseV2Database)
            {
                services.AddTransient<ITobAliasRepository,
                    Cms.Model.Repositories.SecTerms.V2.CategoryAliasRepository>();
                services.AddTransient<ISecTermRepository,
                    Cms.Model.Repositories.SecTerms.V2.SecTermRepository>();
                services.AddTransient<ICategoryRepository,
                    Cms.Model.Repositories.SecTerms.V2.CategoryRepository>();
                services.AddTransient<ISecMetadataRepository,
                    Cms.Model.Repositories.SecTerms.V2.SecMetadataRepository>();
            }
            else
            {
                services.AddTransient<ITobAliasRepository, TobAliasRepository>();
                services.AddTransient<ISecTermRepository, SecTermRepository>();
                services.AddTransient<ICategoryRepository, CategoryRepository>();
                services.AddTransient<ISecMetadataRepository, SecMetadataRepository>();
            }

            // services
            services.AddTransient<ITokenProvider, TokenProvider>();
            services.AddTransient<ISolrCibrService, SolrCibrService>();
            services.AddTransient<ISolrContentService, SolrContentService>();
            services.AddTransient<ISolrSearchTypeaheadService, SolrSearchTypeaheadService>();
            services.AddTransient<ISolrBbbInfoService, SolrBbbInfoService>();
            services.AddTransient<IContentService, ContentService>();
            services.AddTransient<ITobAliasService, TobAliasService>();
            services.AddTransient<ISecTermService, SecTermService>();
            services.AddTransient<ILegacyBbbInfoService, LegacyBbbInfoService>();
            services.AddTransient<ILeadService, LeadService> ();            
            services.AddTransient<IConnectionStringService, ConnectionStringService>();
            services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTransient<IAuthenticationHandler, AuthenticationHandler>();
            services.AddTransient<ISolrLocationService, SolrLocationService>();
            services.AddTransient<IEmailContentService, EmailContentService>();
            services.AddTransient<IEmailLoginService, EmailLoginService>();
            services.AddTransient<IAccountPasswordService, AccountPasswordService>();
            services.AddTransient<IAuditLogRepository, AuditLogRepository>();
            services.AddTransient<IArticleOrderSummaryService, ArticleOrderSummaryService>();
            services.AddTransient<IContentEventRecurrenceService, ContentEventRecurrenceService>();
            services.AddTransient<IContentEventOccurenceRepository, ContentEventOccurenceRepository>();
            services.AddTransient<IServiceAreaService, ServiceAreaService>();
            services.AddTransient<ITwilioService, TwilioService>();
            services.AddTransient<IVerificationRequestRepository, VerificationRequestRepository>();
            services.AddTransient<IVerificationClientRepository, VerificationClientRepository>();
            services.AddTransient<IBaselinePagesService, BaselinePagesService>();
            services.AddTransient<IArticleOrderService, ArticleOrderService>();
            services.AddTransient<IListinngTypeToArticleTypeMappingService, ListinngTypeToArticleTypeMappingService>();
            services.AddTransient<ILocationService, LocationService>();
            services.AddTransient<IUserProfileService, UserProfileService>();

            services.AddTransient<IUserManagementService, UserManagementService>();
            services.AddMemoryCache();

            // caches
            services.AddTransient<IBbbCache, BbbCache>();
            services.AddTransient<ILegacyBbbSiteCache, LegacyBbbSiteCache>();
            services.AddTransient<IStateProvinceCache, StateProvinceCache>();
            services.AddTransient<ITobCache, TobCache>();
            services.AddTransient<IUserProfileCache, UserProfileCache>();

            // helpers
            services.AddTransient<ITobHelpers, TobHelpers>();
            services.AddScoped<ApiAuthorizationHeader>();
            services.AddSingleton(Configuration);

            // mappers
            services.AddTransient<IMetadataMappers, MetadataMappers>();
            services.AddTransient<IBusinessProfileMappers, BusinessProfileMappers>();
            services.AddTransient<ICoreSecTermMappers, CoreSecTermMappers>();
            services.AddTransient<IContentMappers, ContentMappers>();
            services.AddTransient<IContentEventMappers, ContentEventMappers>();
            services.AddTransient<IContentAbListMappers, ContentAbListMappers>();
            services.AddTransient<IContentNearMeMappers, ContentNearMeMappers>();
            services.AddTransient<IContentGeotagMappers, ContentGeotagMappers>();
            services.AddTransient<IContentReviewMappers, ContentReviewMappers>();
            services.AddTransient<IContentAuditLogMappers, ContentAuditLogMappers>();
            services.AddTransient<IBbbAuditLogMappers, BbbAuditLogMappers>();
            services.AddTransient<IBbbInfoMappers, BbbInfoMappers>();
            services.AddTransient<IArticleOrderMappers, ArticleOrderMappers>();
            services.AddTransient<IServiceLocator, ServiceLocator>(provider => new ServiceLocator(provider));

            services.Configure<ConnectionStrings>(
                options => Configuration.GetSection("ConnectionStrings").Bind(options));
            services.Configure<MediaSettings>(
                options => Configuration.GetSection("MediaSettings").Bind(options));
            services.Configure<AppSecurity>(
                options => Configuration.GetSection("AppSecurity").Bind(options));
            services.Configure<General>(
                options => Configuration.GetSection("General").Bind(options));
            services.Configure<EmailSettings>(
                options => Configuration.GetSection("EmailSettings").Bind(options));
            services.Configure<LeadsApiSettings> (
                options => Configuration.GetSection ("LeadsApiSettings").Bind (options));
            services.Configure<TwilioSettings>(
                options => Configuration.GetSection("TwilioSettings").Bind(options));
            services.Configure<ContentReviewSettings>(
                options => Configuration.GetSection("ContentReviewSettings").Bind(options));

            var solrBaseUrl = Configuration
                .GetSection("ConnectionStrings")
                .Get<ConnectionStrings>()
                .SolrDb;


            services.AddSolrNet<BbbInfo>(Url.Combine(solrBaseUrl, SolrCore.BbbInfo.ToRelativeName()));
            services.AddSolrNet<Cibr>(Url.Combine(solrBaseUrl, SolrCore.Cibr.ToRelativeName()));
            services.AddSolrNet<Article>(Url.Combine(solrBaseUrl, SolrCore.Article.ToRelativeName()));
            services.AddSolrNet<Bbb.Core.Solr.Model.Location>(Url.Combine(solrBaseUrl, SolrCore.Locations.ToRelativeName()));
            services.AddSolrNet<SearchTypeahead>(Url.Combine(solrBaseUrl, SolrCore.SearchTypeahead.ToRelativeName()));

            RegisterSolrSearch(solrBaseUrl);

            var solrCloudBaseUrls = Configuration
                .GetSection("ConnectionStrings")
                .Get<ConnectionStrings>()
                .SolrCloudDb
                .Split(new char[] { ',', ';' });

            services.AddSolrNet<SecTerm>(Url.Combine(solrCloudBaseUrls.FirstOrDefault(), SolrCore.SecTerms.ToRelativeName()));
            services.AddSolrNet<SolrArticleOrder>(Url.Combine(solrCloudBaseUrls.FirstOrDefault(), SolrCore.ArticleOrder.ToRelativeName()));
            services.AddTransient<ISolrArticleOrderService>(s => new SolrArticleOrderService(solrCloudBaseUrls));
            services.AddTransient<ISolrSecTermService>(s => new SolrSecTermService(solrCloudBaseUrls));

            var config = Configuration.GetSection("AppSecurity").Get<AppSecurity>();

            TokenValidationService.SigningKey = config.TokenSigningKey;
            var tokenValidationParameters = TokenValidationService.GetValidationParameters();
            services.AddCors();
            services.AddAuthentication(opts =>
            {
                opts.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                opts.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(o => { o.TokenValidationParameters = tokenValidationParameters; });

            services.AddLazyCache();

            services.AddMvc();

            services.AddSpaStaticFiles(configuration =>
            {
                if (Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development")
                {
                    configuration.RootPath = "app/dist/app";
                }
                else
                {
                    configuration.RootPath = "wwwroot/dist";
                }
            });

            JsonConvert.DefaultSettings = () =>
            {
                return new JsonSerializerSettings()
                {
                    NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore,
                    ContractResolver = new CamelCasePropertyNamesContractResolver(),
                };
            };

        }

        protected virtual void RegisterSolrSearch(string solrBaseUrl)
        {
            var solrSearch = new Bbb.Core.Solr.Search.SolrSearch();
            solrSearch.Init<Bbb.Core.Solr.Model.Location>(solrBaseUrl);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            var tokenValidationParameters = TokenValidationService.GetValidationParameters();
            var options = new TokenProviderOptions
            {
                Audience = tokenValidationParameters.ValidAudience,
                Issuer = tokenValidationParameters.ValidIssuer,
                SigningCredentials =
                    new SigningCredentials(
                        tokenValidationParameters.IssuerSigningKey,
                        SecurityAlgorithms.HmacSha256),
                Expiration = TimeSpan.FromDays(1)
            };

            app.UseMiddleware<TokenProvider>(Options.Create(options));
            app.UseMiddleware<ExceptionService>();
            app.UseCors(builder =>
                builder.AllowAnyOrigin());

            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    "default",
                    "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "app";

                if (env.IsDevelopment())
                {
                    // Configure the timeout to 5 minutes to avoid "The Angular CLI process did not
                    // start listening for requests within the timeout period of 50 seconds." issue
                    spa.Options.StartupTimeout = new TimeSpan(0, 5, 0);

                    spa.UseAngularCliServer(npmScript: "start");
                }
            });
        }
    }
}