import { HttpClient, HttpResponse } from "@angular/common/http";




export class LoginResponseBase{
  
}

export class LoginResponse extends HttpResponse<LoginResponseBase> {
  access_token: string;
  appUser: string;
  error: boolean;  
}
