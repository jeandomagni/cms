import { Injectable } from "@angular/core";
import { HttpClient, HttpResponse } from "@angular/common/http";
import { CookieService } from "ngx-cookie-service";
import { Observable } from "rxjs";

import { Authentication } from "src/app/auth/authentication.model";
import { LoginData } from "src/app/auth/login-data.model";
import { LoginResponse } from "src/app/auth/login-response.model";
import { LoginValidation } from "src/app/auth/login-validation.model";
import { User } from "src/app/auth/user.model";
import { LegacyBbbInfo } from "src/app/shared/models/bbb/legacy-bbb-info.model";
import { Location } from "src/app/shared/models/location/location.interface";

@Injectable()
export class AuthService {
  currentUser: User;
  isAdmin: boolean;
  isGlobalAdmin: boolean;
  isLocalAdminRole: boolean;
  authentication: Authentication = new Authentication();
  private serviceBase = "/";

  constructor(private http: HttpClient, private cookieService: CookieService) {}

  init(): void {
    try {
      this.currentUser = JSON.parse(window.localStorage.getItem("appUser"));
      this.isLocalAdminRole = this.currentUser.localAdmin;
      this.isGlobalAdmin = this.currentUser.globalAdmin;
      this.isAdmin = this.isLocalAdminRole || this.isGlobalAdmin;
      if (!this.currentUser) { this.logOut(); }
    } catch (ex) {
      this.logOut();
    }
  }

  setUserLocations(locations: string[]): void {
    this.currentUser.locations = locations;
    window.localStorage.setItem("appUser", JSON.stringify(this.currentUser));
  }

  login(loginData: LoginData): Observable<LoginResponse> {
    const data = `grant_type=password&username=${loginData.username}&password=${loginData.password}&clientId=${loginData.clientId}`;
    const headers = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      }
    };
    return this.http.post<LoginResponse>(
      `${this.serviceBase}token`,
      data,
      headers
    );
  }

  prelogin(preVerificationData: object): Observable<LoginValidation> {
    return this.http.put<LoginValidation>("/api/account/", preVerificationData);
  }

  verifyLogin(
    userLogin: string,
    verificationType: string,
    clientId: string
  ): Observable<object> {
    return this.http.get(
      `/api/account/${userLogin}/${verificationType}/${clientId}`
    );
  }

  verifyCode(verificationData: object): Observable<object> {
    return this.http.post("/api/account/", verificationData);
  }

  logOut(): void {
    window.localStorage.removeItem("authorizationData");
    window.localStorage.removeItem("appUser");

    this.cookieService.delete(
      "Cms.AuthCookie",
      "/",
      `.${AuthService.getBaseDomain(window.location.hostname)}`
    );

    this.authentication.isAuth = false;

    if (window.location.href.indexOf("login") === -1) {
      window.location.href = "/#/login";
    }
  }

  hasRole(role: string): boolean {
    if (
      !this.currentUser ||
      !this.currentUser.role ||
      this.currentUser.role.length === 0
    ) {
      return false;
    }

    const regex = new RegExp(  this.currentUser.role.join( "|" ), "i");
    const isAdmin = regex.test( role );
    return isAdmin;
  }

  getSites = (): LegacyBbbInfo[] =>
    this.currentUser.sites.sort((a, b) => (a.bbbName > b.bbbName ? 1 : -1));

  getAdminSites = (): string[] => this.currentUser.adminSites;

  fillAuthData(): void {
    const authData = window.localStorage.getItem("authorizationData");
    const currentUser = JSON.parse(window.localStorage.getItem("appUser"));
    if (authData) {
      this.authentication.isAuth = true;
      this.authentication.userName = currentUser.email;
    }
  }

  isLocalAdmin(site: string): boolean {
    return this.currentUser.adminSites.includes(site);
  }

  getDefaultSiteId(): string {
    return this.currentUser.profile.defaultSite;
  }

  getDefaultSiteName(): string {
    return this.currentUser.profile.defaultSiteName;
  }

  getDefaultSiteState(): string {
    return this.currentUser.profile.defaultSiteState;
  }

  getDefaultSiteCountry(): string {
    return this.currentUser.profile.defaultSiteCountry;
  }

  getDefaultSiteCity(): Location {
    return this.currentUser.profile.defaultSiteCity;
  }

  getTopics(): string[] {
    return this.currentUser.topics;
  }

  private static getBaseDomain(url: string): string {
    const firstDotIndex = url.indexOf(".");
    const domain = firstDotIndex ? url.substring(firstDotIndex + 1) : url;
    return domain.replace("/", "");
  }
}
