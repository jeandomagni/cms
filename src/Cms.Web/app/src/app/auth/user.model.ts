import { LegacyBbbInfo } from "src/app/shared/models/bbb/legacy-bbb-info.model";
import { UserProfile } from "src/app/shared/models/user/user-profile.model";

export class User {
  adminSites: string[];
  email: string;
  iat: string;
  locations: string[];
  profile: UserProfile;
  role: string[];
  sites: LegacyBbbInfo[];
  userId: string;
  topics: string[];
  localAdmin: boolean;
  globalAdmin: boolean;
}
