﻿import { HttpClientModule } from "@angular/common/http";
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { CookieService } from "ngx-cookie-service";

import { AuthService } from "src/app/auth/auth.service";

@NgModule({
  imports: [CommonModule, HttpClientModule, RouterModule],
  providers: [AuthService, CookieService]
})
export class AuthModule {
  constructor(authService: AuthService) {
    authService.init();
  }
}
