export class LoginData {
  clientId: string;
  password: string;
  username: string;
}
