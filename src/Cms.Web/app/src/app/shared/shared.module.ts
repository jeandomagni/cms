import { CommonModule } from "@angular/common";
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { FlexLayoutModule } from "@angular/flex-layout";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import {
  MatCardModule,
  MatButtonModule,
  MatInputModule,
  MatAutocompleteModule,
  MatIconModule,
  MatToolbarModule,
  MatDialogModule,
  MatChipsModule,
  MatSelectModule,
  MatMenuModule,
  MatDatepickerModule,
  MatGridListModule,
  MatDividerModule,
  MatCheckboxModule,
  MatNativeDateModule,
  MatSnackBarModule,
  MatTabsModule,
  MatListModule,
  MatFormFieldModule,
  MatRadioModule,
  MatProgressSpinnerModule
} from "@angular/material";
import { FileUploadModule } from "ng2-file-upload";

import { AuthService } from "src/app/auth/auth.service";
import { NotificationService } from "src/app/services/notification.service";
import { ServiceAreaService } from "src/app/services/service-area.service";
import { SolrService } from "src/app/services/solr.service";
import { ContentMediaService } from "src/app/services/content-media.service";
import { ConfirmDialogService } from "src/app/services/confirm-dialog.service";
import { PaginationService } from "src/app/services/pagination.service";
import { StateProvinceService } from "src/app/services/state-province.service";
import { FileService } from "src/app/services/file.service";
import { HighlightPipe } from "src/app/shared/pipes/highlight.pipe";
import { TruncatePipe } from "src/app/shared/pipes/truncate.pipe";
import { ErrorInterceptorService } from "src/app/shared/interceptors/error-interceptor.service";
import { LoaderInterceptorService } from "src/app/shared/interceptors/loader-interceptor.service";
import { TokenInterceptorService } from "src/app/shared/interceptors/token-interceptor.service";
import { SharedComponents } from "src/app/shared/shared.components";
import { BaselinePageService } from "src/app/services/baselinePage.service";
import { ShortlinksService } from "src/app/services/shortlinks.service";
import { AuthModule } from "src/app/auth/auth.module";
import { AppSettingsService } from "src/app/services/app-settings.service";

@NgModule({
  imports: [
    AuthModule,
    CommonModule,
    FileUploadModule,
    FlexLayoutModule,
    FormsModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatFormFieldModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatSelectModule,
    MatSnackBarModule,
    MatTabsModule,
    MatToolbarModule,
    ReactiveFormsModule,
    MatRadioModule,
    MatProgressSpinnerModule
  ],
  declarations: [...SharedComponents, HighlightPipe, TruncatePipe],
  entryComponents: [...SharedComponents],
  providers: [
    AuthService,
    AppSettingsService,
    ConfirmDialogService,
    ContentMediaService,
    FileService,
    NotificationService,
    PaginationService,
    ServiceAreaService,
    SolrService,
    BaselinePageService,
    ShortlinksService,
    StateProvinceService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptorService,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoaderInterceptorService,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    }
  ],
  exports: [...SharedComponents, HighlightPipe, TruncatePipe]
})
export class SharedModule {}
