import { BaseAutocompleteComponent } from "src/app/components/base/base-autocomplete/base-autocomplete.component";
import { BaseDragDropTreeComponent } from "src/app/components/base/base-drag-drop-tree/base-drag-drop-tree.component";
import { BbbPickerComponent } from "src/app/components/bbb-picker/bbb-picker.component";
import { CardActivityListComponent } from "src/app/components/card-activity-list/card-activity-list.component";
import { CardCityPickerComponent } from "src/app/components/card-city-picker/card-city-picker.component";
import { CardCountryPickerComponent } from "src/app/components/card-country-picker/card-country-picker.component";
import { CardServiceAreaPickerComponent } from "src/app/components/card-service-area-picker/card-service-area-picker.component";
import { CardSitePickerComponent } from "src/app/components/card-site-picker/card-site-picker.component";
import { CardStateProvincePickerComponent } from "src/app/components/card-state-province-picker/card-state-province-picker.component";
import { CardTopicPickerComponent } from "src/app/components/card-topic-picker/card-topic-picker.component";
import { CategoryPickerComponent } from "src/app/components/category-picker/category-picker.component";
import { FilterColumnComponent } from "src/app/components/filter-column/filter-column.component";
import { FileUploaderComponent } from "src/app/components/file-uploader/file-uploader.component";
import { FloatingFooterPaginationComponent } from "src/app/components/floating-footer-pagination/floating-footer-pagination.component";
import { CopyButtonComponent } from "src/app/components/copy-button/copy-button.component";
import { CardSearchComponent } from "src/app/components/card-search/card-search.component";
import { BbbCarouselComponent } from "src/app/components/bbb-carousel/bbb-carousel.component";
import { AddEditImageDialogComponent } from "src/app/components/bbb-carousel/add-edit-image-dialog.component";
import { ConfirmDialogComponent } from "src/app/components/confirm-dialog/confirm-dialog.component";
import { StateProvincePickerInlineComponent } from "src/app/components/state-province-picker-inline/state-province-picker-inline.component";
import { ProfileSearchComponent } from "src/app/components/profile-search/profile-search.component";
import { MediaSearchDialogComponent } from "src/app/components/media-search-dialog/media-search-dialog.component";
import { ImageSearchComponent } from "src/app/components/image-search/image-search.component";
import { CardImagePickerComponent } from "src/app/components/card-image-picker/card-image-picker.component";
import { ImagePickerComponent } from "src/app/components/image-picker/image-picker.component";
import { NavigationComponent } from "src/app/components/navigation/navigation.component";
import { LayoutViewSelectorComponent } from "src/app/components/layout-view-selector/layout-view-selector.component";
import { GroupedCitiesComponent } from "src/app/components/grouped-cities/grouped-cities.component";
import { DialogAddArticleToNewsfeedComponent } from "src/app/components/dialog-add-article-to-newsfeed/dialog-add-article-to-newsfeed.component";
import { RolesPickerComponent } from "src/app/components/roles-picker/roles-picker.component";
import { BbbSelectionComponent } from "src/app/components/bbb-selector/bbb-selection.component";


export const SharedComponents = [
  AddEditImageDialogComponent,
  BaseAutocompleteComponent,
  BaseDragDropTreeComponent,
  BbbCarouselComponent,
  BbbPickerComponent,
  CardActivityListComponent,
  CardCityPickerComponent,
  CardCountryPickerComponent,
  CardImagePickerComponent,
  CardSearchComponent,
  CardServiceAreaPickerComponent,
  CardSitePickerComponent,
  CardStateProvincePickerComponent,
  CardTopicPickerComponent,
  CategoryPickerComponent,
  ConfirmDialogComponent,
  CopyButtonComponent,
  FileUploaderComponent,
  FilterColumnComponent,
  FloatingFooterPaginationComponent,
  ImagePickerComponent,
  ImageSearchComponent,
  MediaSearchDialogComponent,
  NavigationComponent,
  ProfileSearchComponent,
  StateProvincePickerInlineComponent,
  LayoutViewSelectorComponent,
  GroupedCitiesComponent,
  DialogAddArticleToNewsfeedComponent,
  RolesPickerComponent,
  BbbSelectionComponent
];
