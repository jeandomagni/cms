import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'truncate',
})
export class TruncatePipe implements PipeTransform {
  transform(value: string, limit = 25, completeWords = false, ellipsis = '...'): string {
    if (!value) return value;

    const calculatedLimit = completeWords ? value.substr(0, limit).lastIndexOf(' ') : limit;
    return value.length > calculatedLimit ? value.substr(0, calculatedLimit) + ellipsis : value;
  }
}
