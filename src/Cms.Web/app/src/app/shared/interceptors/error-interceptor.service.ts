import {
  HttpResponse,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
} from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { NotificationService } from "src/app/services/notification.service";

@Injectable()
export class ErrorInterceptorService implements HttpInterceptor {
  constructor(private notificationService: NotificationService) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return Observable.create((observer) => {
      const subscription = next.handle(req).subscribe(
        (event) => {
          if (event instanceof HttpResponse) {
            observer.next(event);
          }
        },
        (err: HttpErrorResponse) => {
          if (err.status === 401) {
            window.localStorage.removeItem("authorizationData");
            window.location.href = "#/login/form";
          } else {
            const displayError = err.error.error || err.error.message;
            this.notificationService.error(displayError);
          }

          observer.error(err);
        },
        () => {
          observer.complete();
        }
      );
      return () => {
        subscription.unsubscribe();
      };
    });
  }
}
