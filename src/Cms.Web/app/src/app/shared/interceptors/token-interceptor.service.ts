import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class TokenInterceptorService implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = req.url.indexOf('solrproxy') > -1 || req.url.indexOf('api') > -1 ? window.localStorage.getItem('authorizationData') : null;
    let newHeaders = req.headers;

    if (token) {
      newHeaders = newHeaders.append('cms-access-token', token);
    }

    const authReq = req.clone({ headers: newHeaders });
    return next.handle(authReq);
  }
}
