import { Component, OnInit, EventEmitter } from "@angular/core";
import { MediaObserver, MediaChange } from "@angular/flex-layout";
import { Subscription } from "rxjs";
import { takeUntil } from "rxjs/operators";

import { AuthService } from "src/app/auth/auth.service";
import { User } from "src/app/auth/user.model";
import { MainMenuItem } from "src/app/shared/models/main-menu-item.interface";
import { AppSettingsService } from "src/app/services/app-settings.service";

@Component({
  selector: "common-layout",
  templateUrl: "./common-layout.component.html",
  styleUrls: ["./common-layout.component.scss"],
})
export class CommonLayoutComponent implements OnInit {
  isXs: boolean;
  menuItems: MainMenuItem[];
  title = "BBB Unified";
  user: User;
  watcher: Subscription;

  private onDestroy: EventEmitter<void> = new EventEmitter();

  constructor(
    private authService: AuthService,
    private appSettingsService: AppSettingsService,
    mediaObserver: MediaObserver
  ) {
    this.watcher = mediaObserver.media$.subscribe((change: MediaChange) => {
      this.isXs = change.mqAlias === "xs";
    });
  }

  ngOnInit(): void {
    this.menuItems = this.buildMenuItems();
    this.user = this.authService.currentUser || new User();
    this.appSettingsService
      .getConnectionString("TerminusBaseUrl")
      .pipe(takeUntil(this.onDestroy))
      .subscribe(({ connectionString }) => {
        window["terminusBaseUrl"] = connectionString;
      });

    this.appSettingsService
      .getGeneralSetting("ShowUnifiedComplaintFields")
      .pipe(takeUntil(this.onDestroy))
      .subscribe(({ setting }) => {
        window["showUnifiedComplaintFields"] = setting;
      });
  }

  ngOnDestroy(): void {
    this.watcher.unsubscribe();
  }

  profile(): void {
    window.location.href = "#/profile/manage";
  }

  logout(): void {
    this.authService.logOut();
  }

  private buildMenuItems(): MainMenuItem[] {
    const isGlobalAdmin  =
      !!this.authService.currentUser &&
      this.authService.isGlobalAdmin;

    const isLocalAdmin =
      !!this.authService.currentUser &&
      ((this.authService.currentUser.adminSites &&
        this.authService.currentUser.adminSites.length > 0) ||
        this.authService.isAdmin);
    return [
      {
        name: "Manage",
        icon: "dashboard",
        href: "#/content/manage",
      },
      {
        name: "Create",
        icon: "code",
        href: "#/content/create",
      },
      (isLocalAdmin || isGlobalAdmin ) && {
        name: "Bbb",
        icon: "business_center",
        href: "#/bbb/manage",
      },
      {
        name: "Profile",
        icon: "person",
        href: "#/profile/manage",
      },
      {
        name: "Admin",
        icon: "lock",
        href: "#/admin/manage",
      },
    ].filter(Boolean);
  }
}
