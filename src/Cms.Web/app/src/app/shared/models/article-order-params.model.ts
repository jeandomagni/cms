export class ArticleOrderParams {
  city: string;
  country: string;
  listingType: string;
  serviceArea: string;
  page: number;
  pageSize: number;
  state: string;
  topic: string;
  overrideFeedId?: number;

  constructor() {
    this.country = 'USA';
    this.listingType = 'news';
    this.page = 1;
    this.pageSize = 10;
  }
}
