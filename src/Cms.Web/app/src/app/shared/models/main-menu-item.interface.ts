export interface MainMenuItem {
  name: string;
  icon: string;
  href: string;
}
