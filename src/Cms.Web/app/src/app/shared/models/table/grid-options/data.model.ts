import { Filter } from "src/app/shared/models/table/grid-options/filter.model";
import { Sort } from "src/app/shared/models/table/grid-options/sort.model";

export class Data {
  checkedOutBy?: string;
  domainOnly?: boolean;
  filter: Filter;
  includeExpired?: boolean;
  locations?: string;
  page: number;
  pageSize: number;
  skip: number;
  sort?: Sort[];
  take: number;

  constructor(currentPage: number, pageSize: number) {
    this.page = currentPage;
    this.take = pageSize;
    this.skip = pageSize * (currentPage - 1);
    this.pageSize = pageSize;
  }
}
