import { Filter } from "src/app/shared/models/table/filter.interface";

export class TableOptions {
  filters: Filter[];
  limit: number;
  orderBy: string;
  page: number;

  constructor() {
    this.filters = [];
    this.limit = 20;
    this.page = 1;
  }
}
