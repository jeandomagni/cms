import { Data } from "src/app/shared/models/table/grid-options/data.model";

export class GridOptions {
  data: Data;

  constructor(currentPage = 1, pageSize = 20) {
    this.data = new Data(currentPage, pageSize);
  }
}
