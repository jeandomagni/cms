export interface Filter {
  column: string;
  keyword: string;
  type: string;
}
