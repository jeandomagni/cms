import { TableOptions } from './table-options.model';

export class BaselinePageTableOptions extends TableOptions {
    getShortlinks: boolean;
}
