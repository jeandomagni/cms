export interface Shortlink {
    id: number;
    contentCode: string;
    redirectUrl: string;
    urlSegment: string;
    totalCount: number;
  }
  