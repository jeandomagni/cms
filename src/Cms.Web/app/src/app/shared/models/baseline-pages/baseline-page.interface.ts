export interface BaselinePage {
    id: number;
    contentCode: string;
    redirectUrl: string;
    urlSegment: string;
    title: string;
    
    layout: string;
    modifiedDate: Date;
    totalCount: number;
  }
  