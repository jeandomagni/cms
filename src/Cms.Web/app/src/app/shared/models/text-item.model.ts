export class TextItem {
    constructor(id: string, text: string, city? : string) {
        this.id = id;
        this.text = text;
        this.city = city;
    }

    public id: string;
    public text: string;
    public city: string;
  }
  