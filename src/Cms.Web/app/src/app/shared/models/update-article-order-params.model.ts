import { ArticleOrderParams } from './article-order-params.model';
export class UpdateArticleOrderParams extends ArticleOrderParams {
  order: string[];
  id: number;
}
