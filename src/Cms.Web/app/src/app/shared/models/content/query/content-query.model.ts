import { QueryFilter } from "src/app/shared/models/content/query/query-filter.interface";

export class ContentQuery {
  checkedOutBy: string;
  domainOnly: boolean;
  includeExpired: boolean;
  limit: number;
  page: number;
  locations: string;
  orderBy: string;
  filters: QueryFilter[];

  constructor() {
    this.filters = [];
    this.limit = 20;
    this.page = 1;
  }
}
