import { Geotag } from "src/app/shared/models/location/geotag.interface";
import { Location } from "src/app/shared/models/location/location.interface";

export interface ContentPreview {
  canEdit: boolean;
  checkoutBy: string;
  cities: Location[];
  code: string;
  countries: Geotag[];
  createDate: Date;
  createdBy: string;
  expirationDate: Date;
  geoTags: Geotag[];
  id: number;
  isCheckedOut: boolean;
  publishDate?: Date;
  states: Geotag[];
  status: string;
  tags: string[];
  title: string;
  topicsAndTags: string;
  totalCount: number;
  type: string;
  serviceArea: Geotag[]
  isInMyNewsfeed: boolean;
}
