export class Video {
  caption: string;
  id: number;
  position: number;
  title: string;
  type?: number;
  url: string;
}
