import { Author } from "src/app/shared/models/content/entities/author.interface";
import { ArticleUrl } from "src/app/shared/models/content/entities/article-url.interface";
import { Category } from "src/app/shared/models/content/entities/category.interface";
import { Video } from "src/app/shared/models/content/entities/video.model";
import { Recurrence } from "src/app/shared/models/content/event/recurrence.interface";
import { LegacyBbbInfo } from "src/app/shared/models/bbb/legacy-bbb-info.model";
import { Geotag } from "src/app/shared/models/location/geotag.interface";
import { Location } from "src/app/shared/models/location/location.interface";
import { Pin } from "src/app/shared/models/location/pin.interface";

export class Content {
  articleUrl: ArticleUrl;
  articleUrlCustom: string;
  authors: Author[];
  bbbIds: Geotag[];
  canCheckOut: boolean;
  canEdit: boolean;
  categories: Category[];
  checkoutBy: string;
  checkoutDate: Date;
  createDate: Date;
  createdBy: string;
  cities: Location[];
  code: string;
  contentHtml: string;
  countries: Geotag[];
  eventBbb: LegacyBbbInfo;
  expirationDate: Date;
  hasRedirectUrl: boolean;
  hasSeoCanonical: boolean;
  id: number;
  isCheckedOut: boolean;
  layout: string;
  metaDescription: string;
  mobileHeadline: string;
  modifiedDate: Date;
  noIndex: boolean;
  occurrences: Recurrence[];
  pageTitle: string;
  pinnedCities: Location[];
  pinnedLocations: Pin[];
  pinnedTopic: string;
  priority: number;
  publishDate: Date;
  redirectUrl: string;
  seoCanonical: string;
  sites: LegacyBbbInfo[];
  states: Geotag[];
  status: string;
  summary: string;
  tags: string[];
  title: string;
  topics: string[];
  type: string;
  videos: Video[];
  isIndustryTip: boolean;
  bbbAuthor: string;
  availableAuthors: Author[];

  constructor() {
    this.canEdit = true;
  }
}
