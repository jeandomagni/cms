export class Media {
  id: number;
  url: string;
  fileCode: string;
  title: string;
  description: string;
  contentCode: string;
  mainImage: boolean;
  order: number;
  imageTitle: string;
  imageCaption: string;
  credit: string;
}
