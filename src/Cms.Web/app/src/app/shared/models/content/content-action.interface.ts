export interface ContentAction {
  label: string;
  raised: boolean;
  color?: string;
  desc: string;
}
