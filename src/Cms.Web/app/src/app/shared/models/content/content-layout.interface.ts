export interface ContentLayout {
  articleTypes: string[];
  description: string;
  id: string;
}
