import { BusinessLinkList } from "src/app/shared/models/content/ab-list/business-link-list.interface";

export interface AbList {
  businessLinkLists?: BusinessLinkList[];
  contentCode: string;
  id?: number;
}
