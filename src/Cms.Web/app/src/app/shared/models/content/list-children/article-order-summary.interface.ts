export interface ArticleOrderSummary {
  code: string;
  title: string;
  city: string;
  state: string;
  country: string;
  pinnedCity: string;
  pinnedState: string;
  pinnedCountry: string;
  publishDate?: Date;
  priority?: number;
  order: number;
  id: string;
  imageUrl: string;

  fromNextPage: boolean;
}
