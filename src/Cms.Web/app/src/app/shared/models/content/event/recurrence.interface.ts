export interface Recurrence {
  endDate: Date;
  eventId: number;
  id: number;
  startDate: Date;
}
