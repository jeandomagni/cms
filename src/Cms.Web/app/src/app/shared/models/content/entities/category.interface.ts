export interface Category {
  id: string;
  entityId: string;
  entityType: string;
  title: string;
  secondaryTitle: string;
  url: string;
  logo: string;
  location: string;
  cultureInfo: string[];
  score?: number;
  profileUrl: string;
  altTitles: string[];
  altTitleHighlight: string;
  metaTags: string[];
}
