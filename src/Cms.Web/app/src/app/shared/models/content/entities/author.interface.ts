export interface Author {
  email: string;
  id: string;
  name: string;
  url: string;
}
