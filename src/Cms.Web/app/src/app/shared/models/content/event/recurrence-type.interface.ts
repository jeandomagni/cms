export interface RecurrenceType {
  label: string;
  value: number;
}
