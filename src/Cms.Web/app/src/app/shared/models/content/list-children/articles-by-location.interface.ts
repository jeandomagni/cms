import { ArticleOrderSummary } from "src/app/shared/models/content/list-children/article-order-summary.interface";

export interface ArticlesByLocation {
  articles: ArticleOrderSummary[];
  totalCount: number;
  articleOrderId: number;
  overrideFeedId?: number;
}
