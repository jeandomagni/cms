export interface NearMe {
  aboveListContent?: string;
  contentCode: string;
  id?: number;
  relatedInformationHeadline?: string;
  relatedInformationText?: string;
}
