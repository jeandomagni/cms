import { Link } from "src/app/shared/models/link.interface";

export interface BusinessLinkList {
  businessLinks: Link[];
  title: string;
}
