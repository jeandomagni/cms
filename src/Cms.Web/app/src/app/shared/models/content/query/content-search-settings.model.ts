export class ContentSearchSettings {
  title: string;
  content: string;
  tags: string;
  id: string;
  createdBy: string;
  filterBy: string;
  isAdmin: boolean;
  page: number;
  serviceArea: string;

  constructor() {
    this.page = 1;
  }
}
