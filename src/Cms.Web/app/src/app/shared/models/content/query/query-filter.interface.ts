export interface QueryFilter {
  column: string;
  type: string;
  keyword: string;
}
