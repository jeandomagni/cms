export interface ArticleUrl {
  full: string;
  prefix: string;
  segment: string;
}
