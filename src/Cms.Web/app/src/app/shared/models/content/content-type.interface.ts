export interface ContentType {
  isAdminOnly: boolean;
  name: string;
  hideFromCreate: boolean;
}
