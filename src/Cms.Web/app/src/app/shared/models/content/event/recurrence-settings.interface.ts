export interface RecurrenceSettings {
  dailyType: number;
  dayOfMonth: number;
  dayOfWeek: string;
  endDate: Date;
  friday: boolean;
  instance: number;
  interval: number;
  monday: boolean;
  recurrenceType: number;
  saturday: boolean;
  startDate: Date;
  sunday: boolean;
  thursday: boolean;
  tuesday: boolean;
  wednesday: boolean;
  startTime?: string;
  endTime?: string;
}
