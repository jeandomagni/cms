import { RecurrenceSettings } from "src/app/shared/models/content/event/recurrence-settings.interface";

export class Event {
  addressLine: string;
  addressLine2: string;
  city: string;
  contentCode: string;
  country: string;
  endDate: Date;
  hasRecurrence: boolean;
  isVirtualEvent: boolean;
  locationName: string;
  postalCode: string;
  recurrence: RecurrenceSettings;
  registrationLink: string;
  startDate: Date;
  state: string;
}
