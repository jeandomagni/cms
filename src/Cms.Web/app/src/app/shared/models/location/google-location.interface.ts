export interface GoogleLocation {
  lat: number;
  lng: number;
}
