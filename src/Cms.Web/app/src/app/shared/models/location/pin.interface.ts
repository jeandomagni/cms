export interface Pin {
  id?: number;
  locationType: string;
  pinExpirationDate: Date;
  pinnedLocation: string;
}
