export interface Location {
  bbbIds: string[];
  city: string;
  citySeo: string;
  countryName: string;
  countryCode: string;
  id: string;
  state: string;
  stateFull: string;
}
