export interface ContentLocation {
  location: string;
  type: string;
}
