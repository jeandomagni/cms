export class Geotag {
  contentCode: string;
  isPinned: boolean;
  locationId: string;
  locationType: string;
  pinExpirationDate?: Date;
  pinId?: number;
}
