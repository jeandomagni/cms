export interface Location {
  addressLine1: string;
  addressLine2: string;
  city: string;
  contactEmail: string;
  country: string;
  excludeFromLocator: boolean;
  faxNumber: string;
  jurisdiction: string[];
  latitude: string;
  longitude: string;
  officeHours: string[];
  phoneHours: string[];
  phoneNumber: string;
  textNumber: string;
  state: string;
  type: string;
  zipCode: string;
  contactEmailPr: string;
  phoneNumberPr: string;
}
