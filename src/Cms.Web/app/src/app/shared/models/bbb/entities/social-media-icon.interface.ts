export interface SocialMediaIcon {
  icon: string;
  id: string;
  name: string;
}
