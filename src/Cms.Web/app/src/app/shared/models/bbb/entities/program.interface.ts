export interface Program {
  icon: string;
  linkUrl: string;
  priority: number;
  summaryText: string;
  title: string;
}
