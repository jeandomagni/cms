export interface ApplyForAccreditationInfo {
  sendEmail: boolean;
  terms: string;
  showTerms: boolean;
  applyEmail: string;
  sendEmail: boolean;
  applicationSubmission: string;
  questions : Array<ApplyForAccreditationInfoQuestion>;
  paymentProcessorUrl: string;
}

export interface ApplyForAccreditationInfoQuestion {
  editable : boolean;
  required : boolean;
  question : string;
  helptext : string;
}