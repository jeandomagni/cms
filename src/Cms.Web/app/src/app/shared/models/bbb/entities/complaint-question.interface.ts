export interface ComplaintQuestion {
  editable: boolean;
  qualificationHtml?: string;
  question: string;
}
