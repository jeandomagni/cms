import { SocialMedia } from "src/app/shared/models/bbb/entities/social-media.interface";
import { Media } from "src/app/shared/models/content/entities/media.model";

export interface Person {
  accreditedDate?: Date;
  bio: string;
  businessName: string;
  company: string;
  email: string;
  firstName: string;
  image: Media;
  imageUrl: string;
  lastName: string;
  name: string;
  phone: string;
  priority?: number;
  socialMedia: SocialMedia[];
  title: string;
  type: string;
  website: string;
}
