import { Person } from "src/app/shared/models/bbb/entities/person.interface";

export interface PersonGroup {
  name: string;
  persons: Person[];
}
