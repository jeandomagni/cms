export interface Resource {
  description: string;
  priority: number;
  title: string;
  url: string;
}
