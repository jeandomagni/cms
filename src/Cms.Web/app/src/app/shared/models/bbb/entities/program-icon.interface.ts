export interface ProgramIcon {
  faClass?: string;
  isImg: boolean;
  name: string;
  src?: string;
  value: string;
}
