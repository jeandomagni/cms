import {VerificationMethod} from '../../../login/verification/verification-method.interface';

export interface IPagingSettings {
  page: number;
  pageSize: number;
  sortColumn: string;
  direction: string;
}

export class UserMembership {
  userId: string;
  bbbId: number;
  firstName: string;
  lastName: string;
  userName: string;
  email: string;
  mId: number;
  status: string;
  siteId: string;
  lastLoginDate: Date;
  lastPasswordChange: Date;
  lastLockoutDate: Date;
  password: string;
  comment: string;
}

export class UserMembershipInfo extends UserMembership {
  roles: Roles;
  emailPassword: boolean;
}

export interface userDialogData {
  mode: string;
  user: addUpdateUserRequest;
}

export interface addUpdateUserRequest {
  userId: string;
  bbbId: number;
  siteId: string;
  firstName: string;
  lastName: string;
  userName: string;
  email: string;
  roles: Roles;
  emailPassword: boolean;
  password: string;
  newPassword: string;
  verifyPassword: string;
  status: string;
  mId: number;
  lastLoginDate: Date;
  lastPasswordChange: Date;
  lastLockoutDate: Date;
  comment: string;
}


export class UserMembershipVm {
  mode: string;
  password: string;
  confirmPwd:string;
  userId: string;
  bbbId: number;
  firstName: string;
  lastName: string;
  userName: string;
  email: string;
  mId: number;
  status: string;
  siteId: number;
  lastLoginDate: Date;
  lastPasswordChange: Date;
  lastLockoutDate: Date;
  comment: string
}

export class userUpdateRequest
{
    userId: string;
    email: string;
    status: string;
    password: string;
    newPassword: string;
    bbbId: number;
    firstName: string;
    lastName: string;
    userName: string;
}


export class UserMembershipListRequest implements IPagingSettings {
  constructor() {
    this.page = 1;
    this.pageSize = 10;
    this.direction = "ASC";
  }
  page: number;
  pageSize: number;
  sortColumn: string;
  direction: string;
  userId: string;
  bbbId: number;
  firstName: string;
  lastName: string;
  userName: string;
  email: string;
  mId: number;
  status: string;
  siteId: number;
  lastLoginDate: Date;
  lastPasswordChange: Date;
  lastLockoutDate: Date;
  comment: string;
  search: string;
}

export class PaginationResult<T> {
  items: T[];
  totalRecords: number;
  page: number;
  pageSize: number;
  totalPages: number;
  isFirst: boolean;
  isLast: boolean;
  hasNext: boolean;
  hasPrevious: boolean;
}


export class Roles {
  roleId: string;
  roleName: string;
  loweredRoleName: string;
  description: string;
}

export class PasswordChangeInfo {
  email: string;
}

export class ServiceResponse {
  success: boolean;
  errorMessages: string[];
  warningMessages: string[];
  items: any[];
}

