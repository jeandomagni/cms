export interface Alias {
  aliasId?: number;
  creatorUserId?: number;
  dateTimeCreated?: Date;
  name?: string;
  spanish?: string;
}
