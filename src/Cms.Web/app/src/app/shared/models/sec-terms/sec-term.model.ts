import { Metadata } from "src/app/shared/models/metadata/metadata.model";
import { Alias } from "src/app/shared/models/sec-terms/alias.interface";

export class SecTerm {
  aliases: Alias[];
  categoryId: number;
  categoryCode: string;
  isActive: boolean;
  isEditable: boolean;
  metadata: Metadata[];
  metadataBusiness: Metadata[];
  modifiedDate: Date;
  modifiedUser: string;
  name: string;
  spanishName: string;
  spanishUrlSegment: string;
  tobId: string;
  urlSegment: string;

  constructor() {
    this.aliases = [];
    this.metadata = [];
    this.metadataBusiness = [];
  }
}
