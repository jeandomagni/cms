import { SecTerm } from "src/app/shared/models/sec-terms/sec-term.model";

export interface SecTermResults {
  results: SecTerm[];
  totalCount: number;
}
