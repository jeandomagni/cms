import { Alias } from "src/app/shared/models/sec-terms/alias.interface";
import { SecTerm } from "src/app/shared/models/sec-terms/sec-term.model";

export interface EditAliasParams {
  alias: Alias;
  secTerm: SecTerm;
}
