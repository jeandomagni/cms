export class SecTermSearchSettings {
  currentPage: number;
  isActive: string;
  name: string;
  tobId: string;
  urlSegment: string;

  constructor() {
    this.currentPage = 1;
  }
}
