export interface MetadataPlaceholder {
  description: string;
  variable: string;
}
