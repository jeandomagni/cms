export class Metadata {
  cultureIds: string[];
  metaDesc: string;
  pageTitle: string;
  type: string;
}
