export interface AuditLog {
  createdDate: Date;
  summary: string;
}
