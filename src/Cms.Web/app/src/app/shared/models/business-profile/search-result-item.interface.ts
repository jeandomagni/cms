export interface SearchResultItem {
  bbbId: string;
  businessId: string;
  businessName: string;
  displayAddress: string;
  id: string;
  logoUrl: string;
  profileUrl: string;
}
