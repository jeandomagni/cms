import { ProfileSearchParams } from "src/app/shared/models/business-profile/profile-search-params.interface";
import { SearchResultItem } from "src/app/shared/models/business-profile/search-result-item.interface";

export class BusinessSearchResult {
  items: SearchResultItem[];
  pageCount: number;
  resultCount: number;
  searchParams: ProfileSearchParams;

  constructor() {
    this.items = [];
  }
}
