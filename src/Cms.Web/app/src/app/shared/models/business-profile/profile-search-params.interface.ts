export interface ProfileSearchParams {
  pageNumber?: number;
  pageSize?: number;
  searchText: string;
}
