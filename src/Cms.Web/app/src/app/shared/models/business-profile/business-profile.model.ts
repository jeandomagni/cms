import { Metadata } from "src/app/shared/models/metadata/metadata.model";

export class BusinessProfile {
  bbbId: string;
  businessId: string;
  businessName: string;
  displayAddress: string;
  id: string;
  isAccredited: boolean;
  isRaqActive: boolean;
  logoUrl: string;
  metadata: Metadata;
  profileSeoId?: number;
  profileUrl: string;

  constructor() {
    this.metadata = new Metadata();
  }
}
