export interface BusinessProfileTypeaheadSuggestion {
  id?: string;
  title: string;
  secondaryTitle?: string;
}
