export interface Link {
  children?: Link[];
  disabled?: boolean;
  linkText: string;
  linkUrl: string;
  priority?: number;
}
