export interface ApplicationSubmission {
  name: string;
  value: string;
}
