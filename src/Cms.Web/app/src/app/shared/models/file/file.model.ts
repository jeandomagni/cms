import { FileTag } from "src/app/shared/models/file/file-tag.interface";

export class File {
  caption?: string;
  code: string;
  contentType: string;
  createDate: Date;
  credit?: string;
  deleted: boolean;
  fileSizeKb: number;
  id: number;
  ownerSiteId: string;
  path: string;
  published: boolean;
  tagItems: FileTag[];
  tags: string;
  title: string;
  totalCount?: number;
  type: string;
  uri: string;
}
