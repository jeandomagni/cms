export class ImageSearchSettings {
  createdBy: string;
  currentPage: number;
  tags: string;
  title: string;

  constructor() {
    this.currentPage = 1;
  }
}
