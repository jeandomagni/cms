export interface FileTag {
  label: string;
  name: string;
  type: string;
  value?: string;
}
