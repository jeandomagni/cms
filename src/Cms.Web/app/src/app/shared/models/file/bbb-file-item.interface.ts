import { FileItem } from "ng2-file-upload";

import { FileTag } from "src/app/shared/models/file/file-tag.interface";

export interface BbbFileItem extends FileItem {
  errMsg?: string;
  // imageDescription: string;
  imageCaption: string;
  imageCredit: string;
  imageTitle: string;
  tags: FileTag[];
}
