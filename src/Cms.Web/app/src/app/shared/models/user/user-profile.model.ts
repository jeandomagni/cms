import { Location } from '../location/location.interface';

export class UserProfile {
  id: number;
  userId: string;
  baseUriCulture: string;
  baseUriCountry: string;
  defaultSite: string;
  openContentInNewTab?: boolean;
  pinnedTopic: string;
  topics: string;
  twoFactorAuthEnabled: boolean;
  createdDate: Date;
  createdBy: string;
  modifiedDate: Date;
  modifiedBy: string;
  phoneNumber: string;
  phoneNumberVerified: boolean;
  displayPhoneNumber: string;
  defaultSiteName: string;
  defaultSiteState: string;
  defaultSiteCity: Location;
  defaultSiteCountry: string;
}
