import { QuoteTag } from "./quote-tag.interface";

export interface Quote {
  code: string;
  createdDate: Date;
  createdBy: string;
  deleted: boolean;
  firstName: string;
  id: number;
  lastName: string;
  ownerSiteId: string;
  published: boolean;
  tagItems: QuoteTag[];
  tags: string;
  text: string;
  totalCount: number;
}
