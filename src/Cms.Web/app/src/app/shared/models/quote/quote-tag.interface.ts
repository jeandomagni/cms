export interface QuoteTag {
  associatedTags?: QuoteTag[];
  displayLocation?: string;
  name: string;
  type: string;
  value: string;
}
