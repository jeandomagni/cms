import { HttpClientModule } from "@angular/common/http";
import { FlexLayoutModule } from "@angular/flex-layout";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {
  MatSidenavModule,
  MatProgressSpinnerModule,
  MatToolbarModule,
  MatButtonModule,
  MatMenuModule,
  MatIconModule,
  MatTabsModule
} from "@angular/material";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ToastrModule } from "ngx-toastr";

import { AuthModule } from "src/app/auth/auth.module";
import { SharedModule } from "src/app/shared/shared.module";

import { AppRoutingModule } from "./app-routing.module";

export const imports = [
  AuthModule,
  BrowserAnimationsModule,
  BrowserModule,
  FlexLayoutModule,
  FormsModule,
  HttpClientModule,
  ReactiveFormsModule,
  ToastrModule.forRoot({
    autoDismiss: true,
    timeOut: 4000,
    maxOpened: 1,
    newestOnTop: true,
    preventDuplicates: false,
    closeButton: true
  }),

  MatButtonModule,
  MatIconModule,
  MatMenuModule,
  MatProgressSpinnerModule,
  MatSidenavModule,
  MatTabsModule,
  MatToolbarModule,

  SharedModule,

  AppRoutingModule
];
