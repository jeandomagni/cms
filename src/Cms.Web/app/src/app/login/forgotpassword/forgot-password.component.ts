import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { HttpResponse } from '@angular/common/http';

import { AuthService } from 'src/app/auth/auth.service';
import { PasswordMgmtService } from 'src/app/services/content-password-mgmt.service';
import * as userMgmt  from 'src/app/shared/models/user-management/user-membership.model';


@Component({
  selector: 'forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.styles.scss']
})
export class ForgotPasswordComponent implements OnInit {
  authMessage: string;
  passwordForm: FormGroup;
  userInfo: userMgmt.PasswordChangeInfo;

  constructor(private authService: AuthService,
              private passwordMgmtService: PasswordMgmtService
  ) {

  }

  ngOnDestroy(): void {
    // this.onDestroy.next();
  }

  ngOnInit(): void {
    const userInfo = window.localStorage.getItem(
      'passwordChange'
    );
    this.userInfo = userInfo
      ? JSON.parse(userInfo)
      : null;

    this.passwordForm = new FormGroup({
      email: new FormControl('', [Validators.required]),
    });

    try {
      this.passwordForm.get('email').setValue( this.userInfo.email);
    } catch (e) {
    }
  }

  onSubmit(): void {

    const userReq = {
      email : this.passwordForm.get('email').value,
    };

    this.passwordMgmtService
      .resetUserPasswordByEmail(userReq as userMgmt.addUpdateUserRequest)
      .pipe()
      .subscribe((serviceResponse) => {
        if (serviceResponse.success) {
          window.localStorage.clear();
          window.location.href = '/';
          return;
        }
        this.authMessage = serviceResponse.errorMessages.join(',');

      });

  }


}
