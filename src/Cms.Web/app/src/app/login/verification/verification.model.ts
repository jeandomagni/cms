import { VerificationMethod } from "./verification-method.interface";

export class Verification {
  clientId: string;
  verificationMethods: VerificationMethod[];
  userLogin: string;
}
