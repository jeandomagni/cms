﻿import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { HttpResponse } from "@angular/common/http";
import { NotificationService } from 'src/app/services/notification.service';

import { AuthService } from "src/app/auth/auth.service";
import { LoginResponse } from "src/app/auth/login-response.model";
import { Verification } from "src/app/login/verification/verification.model";

@Component({
  selector: "verification",
  templateUrl: "./verification.component.html",
  styleUrls: ["./verification.styles.scss"]
})
export class VerificationComponent implements OnInit {
  authMessage = "";
  canEnterCode = false;
  codeResentMessage = "";
  enterCodeForm: FormGroup;
  viewModel: Verification;
  verifyForm: FormGroup;

  constructor(
    private authService: AuthService,
    private notificationService: NotificationService
              ) {}

  ngOnInit(): void {
    const storedVerificationData = window.localStorage.getItem(
      "verificationData"
    );
    this.viewModel = storedVerificationData
      ? JSON.parse(storedVerificationData)
      : null;

    this.enterCodeForm = new FormGroup({
      code: new FormControl("", [Validators.required]),
      rememberMe: new FormControl(false)
    });

    this.verifyForm = new FormGroup({
      method: new FormControl("email", [Validators.required])
    });
  }

  onSubmitVerify(): void {
    this.authService
      .verifyLogin(
        this.viewModel.userLogin,
        this.verifyForm.get("method").value,
        null
      )
      .subscribe(
        (): void => {
          this.canEnterCode = true;
        },
        (): void => {
          this.authMessage =
            "An error occurred verifying your login, please try again or contact support.";
        }
      );
  }

  onResendCode(): void {
    this.authService
      .verifyLogin(
        this.viewModel.userLogin,
        this.verifyForm.get("method").value,
        null
      )
      .subscribe((): void => {
        this.codeResentMessage = "Your code has been re-sent.";
      });
  }

  onSubmitEnterCode(): void {
    const code = this.enterCodeForm.get("code").value;
    const rememberMe = this.enterCodeForm.get("rememberMe").value;
    this.authService
      .verifyCode({
        verificationCode: code,
        userLogin: this.viewModel.userLogin,
        rememberMe,
        clientId: this.viewModel.clientId
      })
      .subscribe(
        (): void => {
          if (rememberMe) {
            window.localStorage.setItem("clientId", this.viewModel.clientId);
          }
          this.authService
            .login({
              username: this.viewModel.userLogin,
              password: code,
              clientId: this.viewModel.clientId
            })
            .subscribe(
              (response: LoginResponse): void => {
                window.localStorage.setItem(
                  "authorizationData",
                  response.access_token
                );
                window.localStorage.setItem(
                  "appUser",
                  JSON.stringify(response.appUser)
                );
                window.location.href = "/";
              },
              (): void => {
                this.authMessage = "Invalid username or password";
              }
            );
        },
        (): void => {
          this.authMessage =
            "Invalid code. Please try again or click resend to send another code.";
        }
      );
  }
}
