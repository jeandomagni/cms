import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { HttpResponse } from '@angular/common/http';

import { AuthService } from 'src/app/auth/auth.service';
import { LoginResponse  } from 'src/app/auth/login-response.model';
import { LoginValidation } from 'src/app/auth/login-validation.model';
import { UserMgmtService } from 'src/app/services/content-user-mgmt.service';
import * as userMgmt  from 'src/app/shared/models/user-management/user-membership.model';

import { merge, of } from 'rxjs';
import {
  takeUntil,
  startWith,
  switchMap,
  map,
  catchError
} from 'rxjs/operators';


@Component({
  selector: 'login-app',
  templateUrl: './form.component.html',
  styleUrls: ['./form.styles.scss']
})
export class FormComponent implements OnInit {
  authMessage: string;
  loginForm: FormGroup;

  constructor(private authService: AuthService,
              private userMgmtService: UserMgmtService
  ) {

  }

  ngOnDestroy(): void {
    // this.onDestroy.next();
  }

  ngOnInit(): void {
    window.localStorage.removeItem('authorizationData');
    window.localStorage.removeItem('appUser');
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required])
    });
  }

  onSubmit(): void {
    const email = this.loginForm.get('email').value;
    const password = this.loginForm.get('password').value;
    const clientId = window.localStorage.getItem('clientId');
    this.authMessage = '';
    this.authService
      .prelogin({
        userLogin: email,
        password,
        clientId
      })
      .subscribe(
        (preloginResponse: LoginValidation): void => {
          if (!preloginResponse) {
            this.authMessage = 'Invalid username or password';
            return;
          }
          if (preloginResponse.verificationRequired) {
            window.localStorage.setItem(
              'verificationData',
              JSON.stringify({
                ...preloginResponse,
                userLogin: email
              })
            );
            window.location.href = '#/login/verification';
          } else {
            this.authService
              .login({
                username: email,
                password,
                clientId
              })
              .subscribe(
                (response: LoginResponse): void => {
                  window.localStorage.setItem(
                    'authorizationData',
                    response.access_token
                  );
                  window.localStorage.setItem(
                    'appUser',
                    JSON.stringify(response.appUser)
                  );
                  window.location.href = '/';
                },
                (response: LoginResponse): void => {
                  if (response.status !== 200 ) {
                    this.authMessage = 'Invalid username or password';
                    try {
                      if (response.status !== 200) {

                        switch (response.status) {
                          case 404:
                            this.authMessage = 'Invalid username or password';
                            break;
                          case 400:
                            this.authMessage = 'Invalid username or password';
                            break;
                          case 409:
                            this.authMessage = 'Account locked. Please contact system support.';
                            break;
                          case 307:
                            this.authMessage = 'Password change required.';
                            const userInfo = new userMgmt.PasswordChangeInfo();
                            userInfo.email = email;

                            window.localStorage.setItem(
                              'passwordChange',
                              JSON.stringify( userInfo));
                            window.location.href = '#/login/password-change';
                            break;
                          case 417:
                            this.authMessage = 'Account disabled. Please contact system support.';
                            break;
                          case 500:
                            this.authMessage = 'An internal server error occurred. Please contact system support.';
                            break;
                        }
                      }
                    } catch (o) {

                    }

                  }
                }
              );
          }
        },
        (): void => {
          this.authMessage = 'Invalid username or password';
        }
      );
  }
}
