import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { HttpResponse } from '@angular/common/http';

import { AuthService } from 'src/app/auth/auth.service';
import { PasswordMgmtService } from 'src/app/services/content-password-mgmt.service';
import * as userMgmt  from 'src/app/shared/models/user-management/user-membership.model';
import {ServiceResponse} from 'src/app/shared/models/user-management/user-membership.model';
import { NotificationService } from 'src/app/services/notification.service';


@Component({
  selector: 'password-change-app',
  templateUrl: './password-change.component.html',
  styleUrls: ['./password-change.styles.scss']
})
export class PasswordChangeComponent implements OnInit {
  authMessage: string;
  passwordForm: FormGroup;
  userInfo: userMgmt.PasswordChangeInfo;

  constructor(private authService: AuthService,
              private notificationService: NotificationService,
              private passwordMgmtService: PasswordMgmtService
  ) {

  }

  ngOnDestroy(): void {
    // this.onDestroy.next();
  }

  ngOnInit(): void {
    const userInfo = window.localStorage.getItem(
      'passwordChange'
    );
    this.userInfo = userInfo
      ? JSON.parse(userInfo)
      : null;

    this.passwordForm = new FormGroup({
      email: new FormControl('', [Validators.required]),
      newPassword: new FormControl('', [Validators.required]),
      verifyPassword: new FormControl('', [Validators.required])
    });
    this.passwordForm.get('email').setValue( this.userInfo.email);
  }

  onSubmit(): void {
    const newPassword = this.passwordForm.get('newPassword').value;
    const verifyPassword = this.passwordForm.get('verifyPassword').value;

    if (newPassword.length < 8) {
      this.authMessage = 'Password length must be at least 8 characters long.';
      return;
    }

    if (newPassword !== verifyPassword) {
      this.authMessage = 'New password and verification does not match.';
      return;
    }

    const userReq = {
      email : this.passwordForm.get('email').value,
      newPassword : this.passwordForm.get('newPassword').value,
      verifyPassword : this.passwordForm.get('verifyPassword').value
    };


    this.passwordMgmtService
      .updatePasswordByEmail(userReq as userMgmt.addUpdateUserRequest)
      .pipe()
      .subscribe((serviceResponse) => {
        if (serviceResponse.success) {
          this.notificationService.success('Password was updated successfully');
          window.localStorage.clear();
          window.location.href = '/';
          return;
        }
        this.authMessage = serviceResponse.errorMessages.join(',');
        return;
      });

  }


}
