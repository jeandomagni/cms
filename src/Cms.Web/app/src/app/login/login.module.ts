﻿import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatButtonModule } from "@angular/material/button";
import { MatCardModule } from "@angular/material/card";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { MatRadioModule } from "@angular/material/radio";
import { MatToolbarModule } from "@angular/material/toolbar";

import { AuthModule } from "src/app/auth/auth.module";
import { LoaderService } from "src/app/components/loading/loader.service";
import { FormComponent } from "src/app/login/form/form.component";
import { LoginComponent } from "src/app/login/login.component";
import { LoginRoutingModule } from "src/app/login/login-routing.module";
import { VerificationComponent } from "src/app/login/verification/verification.component";
import { PasswordChangeComponent } from "src/app/login/passwordchange/password-change.component";
import { ForgotPasswordComponent } from "src/app/login/forgotpassword/forgot-password.component";

import { UserMgmtService } from "src/app/services/content-user-mgmt.service";
import { PasswordMgmtService } from "src/app/services/content-password-mgmt.service";

@NgModule({
  imports: [
    AuthModule,
    CommonModule,
    FormsModule,
    HttpClientModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatToolbarModule,
    ReactiveFormsModule,
    LoginRoutingModule
  ],
  declarations: [LoginComponent, FormComponent, VerificationComponent, PasswordChangeComponent, ForgotPasswordComponent],
  providers: [LoaderService, UserMgmtService, PasswordMgmtService ],
  bootstrap: [LoginComponent]
})
export class LoginModule {}
