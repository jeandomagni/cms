import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { FormComponent } from "src/app/login/form/form.component";
import { VerificationComponent } from "src/app/login/verification/verification.component";
import {PasswordChangeComponent} from './passwordchange/password-change.component';
import { ForgotPasswordComponent } from "src/app/login/forgotpassword/forgot-password.component";

const routes: Routes = [
  { path: "form", component: FormComponent },
  { path: "verification", component: VerificationComponent },
  { path: "password-change", component: PasswordChangeComponent },
  { path: "forgot-password", component: ForgotPasswordComponent},
  { path: "**", redirectTo: "/login/form" }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule {}
