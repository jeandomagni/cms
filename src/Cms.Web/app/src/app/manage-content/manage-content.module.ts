import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FlexLayoutModule } from "@angular/flex-layout";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {
  MatTabsModule,
  MatIconModule,
  MatCardModule,
  MatAutocompleteModule,
  MatFormFieldModule,
  MatButtonModule,
  MatInputModule,
  MatSelectModule,
  MatTableModule,
  MatSortModule,
  MatChipsModule,
  MatPaginatorModule,
  MatListModule,
  MatRadioModule,
  MatDialogModule,
} from "@angular/material";

import { ManageContentComponent } from "src/app/manage-content/manage-content.component";
import { ManageContentRoutingModule } from "src/app/manage-content/manage-content-routing.module";
import { CustomTabComponent } from "src/app/manage-content/tabs/custom-tab/custom-tab.component";
import { GenericTabComponent } from "src/app/manage-content/tabs/generic-tab/generic-tab.component";
import { DialogAddToCountryNewsfeedComponent } from "src/app/manage-content/tabs/generic-tab/dialog-add-to-country-newsfeed.component";
import { ListChildrenTabComponent } from "src/app/manage-content/tabs/list-children-tab/list-children-tab.component";
import { ContentService } from "src/app/services/content.service";
import { ContentLocationService } from "src/app/services/content-location.service";
import { StateProvinceService } from "src/app/services/state-province.service";
import { SharedModule } from "src/app/shared/shared.module";
import { DragDropModule } from "@angular/cdk/drag-drop";

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    FormsModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatCardModule,
    MatChipsModule,
    MatDialogModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatPaginatorModule,
    MatSelectModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatRadioModule,
    DragDropModule,
    ReactiveFormsModule,
    SharedModule,

    ManageContentRoutingModule,
  ],
  declarations: [
    CustomTabComponent,
    GenericTabComponent,
    ListChildrenTabComponent,
    ManageContentComponent,
    DialogAddToCountryNewsfeedComponent,
  ],
  entryComponents: [
    CustomTabComponent,
    GenericTabComponent,
    ListChildrenTabComponent,
    DialogAddToCountryNewsfeedComponent,
  ],
  providers: [ContentService, StateProvinceService, ContentLocationService],
})
export class ManageContentModule {}
