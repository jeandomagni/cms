import { Component } from "@angular/core";

import { Links } from "src/app/manage-content/constants/links.constants";

@Component({
  selector: "manage-content",
  templateUrl: "./manage-content.component.html"
})
export class ManageContentComponent {
  activeLink = "All Drafts";
  links: string[] = Links;
}
