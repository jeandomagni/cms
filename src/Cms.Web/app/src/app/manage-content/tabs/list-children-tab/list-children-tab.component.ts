import { DOWN_ARROW, UP_ARROW } from "@angular/cdk/keycodes";
import { Component, EventEmitter, OnDestroy } from "@angular/core";
import { FormControl } from "@angular/forms";
import { Observable, of } from "rxjs";
import {
  takeUntil,
  startWith,
  debounceTime,
  tap,
  switchMap,
  catchError,
  map
} from "rxjs/operators";

import { AuthService } from "src/app/auth/auth.service";
import { ArticleOrderParams } from "src/app/shared/models/article-order-params.model";
import { ConfirmDialogService } from "src/app/services/confirm-dialog.service";
import { ContentService } from "src/app/services/content.service";
import { ContentLocationService } from "src/app/services/content-location.service";
import { NotificationService } from "src/app/services/notification.service";
import { StateProvinceService } from "src/app/services/state-province.service";
import { SolrService } from "src/app/services/solr.service";
import { ContentPreview } from "src/app/shared/models/content/content-preview.interface";
import { ArticlesByLocation } from "src/app/shared/models/content/list-children/articles-by-location.interface";
import { ArticleOrderSummary } from "src/app/shared/models/content/list-children/article-order-summary.interface";
import { Location } from "src/app/shared/models/location/location.interface";
import { UserProfile } from "src/app/shared/models/user/user-profile.model";
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { UpdateArticleOrderParams } from 'src/app/shared/models/update-article-order-params.model';
import { Params } from '@angular/router';

@Component({
  selector: "manage-list-children-tab",
  templateUrl: "./list-children-tab.component.html",
  styleUrls: ['./list-children-tab.component.scss']
})
export class ListChildrenTabComponent implements OnDestroy {
  cityCtrl: FormControl = new FormControl();
  serviceAreaCtrl: FormControl = new FormControl();
  cityToHighlight: string;
  cityValue: string;
  serviceAreaValue: string;
  disabledCardControls = false;
  filteredCities: Observable<Location[]>;
  filteredStates: Observable<string[]>;
  fnState: EventListener;
  fnCity: EventListener;
  isAdmin: boolean;
  itemsInPage: number;
  linkTarget: string;
  listChildren: ArticleOrderSummary[];
  horizontalGroupArticles: ArticleOrderSummary[];
  verticalGroupArticles: ArticleOrderSummary[];
  notFoundMessage = "No locations found.";
  params = new ArticleOrderParams();
  totalPages: number;
  totalRecords: number;
  stateToHighlight: string;
  stateProvinceCtrl: FormControl = new FormControl();
  stateValue: string;
  private onDestroy: EventEmitter<void> = new EventEmitter();
  articleOrderId: number;
  overrideFeedId?: number;


  constructor(
    authService: AuthService,
    private confirmDialogService: ConfirmDialogService,
    private contentService: ContentService,
    private notificationService: NotificationService,
    private solrService: SolrService,
    private stateProvinceService: StateProvinceService,
    private contentLocationService: ContentLocationService
  ) {
    this.isAdmin = authService.isAdmin;
    this.linkTarget = (authService.currentUser.profile || new UserProfile())
      .openContentInNewTab
      ? "_blank"
      : "_self";
  }  

  ngOnDestroy(): void {
    document.removeEventListener("keydown", this.fnCity);
    document.removeEventListener("keydown", this.fnState);
    this.onDestroy.next();
  }

  allowSelection(option: string): { [className: string]: boolean } {
    return {
      "no-data": option === this.notFoundMessage
    };
  }

  getPreviewUrl = (code: string): string =>
    `${window["terminusBaseUrl"]}/content/${code}`; //eslint-disable-line

  refresh(overrideParams?: ArticleOrderParams): void {
    if(overrideParams != null) {
      this.params.city = overrideParams.city;
      this.params.country = overrideParams.country;
      this.params.listingType = overrideParams.listingType;
      this.params.serviceArea = overrideParams.serviceArea;
      this.params.state = overrideParams.state;
      this.params.topic = overrideParams.topic;
      this.params.page = 1;
    }

    this.contentService
      .paginateListChildren(this.params)
      .pipe(takeUntil(this.onDestroy))
      .subscribe((data: ArticlesByLocation) => {
        this.listChildren = data.articles;

        this.articleOrderId = data.articleOrderId;
        this.overrideFeedId = data.overrideFeedId;

        this.horizontalGroupArticles = data.articles.slice(0, 3);
        this.verticalGroupArticles = data.articles.slice(3, 11);

        this.refreshNextPageItemFlag();

        this.totalRecords = data.totalCount;
        this.totalPages = Math.ceil(data.totalCount / 10) || 1;
        this.itemsInPage = data.articles.length > 10 ? 10 : data.articles.length;
      });
  }

  refreshNextPageItemFlag() {
    var base = (this.params.page - 1) * this.params.pageSize;
    for(var i = 0; i < this.horizontalGroupArticles.length; i++)
    {
      this.horizontalGroupArticles[i].order = base + i + 1;
      this.horizontalGroupArticles[i].fromNextPage = false;
    }
    for(var i = 0; i < this.verticalGroupArticles.length; i++)
    {
      this.verticalGroupArticles[i].order = base + i + 4;
      if(i == this.params.pageSize - 3) {
        this.verticalGroupArticles[i].fromNextPage = true;
      } else {
        this.verticalGroupArticles[i].fromNextPage = false;
      }
    }
  }

  showConfirmDelete(code: string): void {
    this.confirmDialogService
      .showConfirmDelete()
      .pipe(takeUntil(this.onDestroy))
      .subscribe((confirmed: boolean) => {
        if (confirmed) {
          this.contentLocationService
            .deleteFromFeed(code, this.articleOrderId)
            .pipe(takeUntil(this.onDestroy))
            .subscribe(() => {
              this.notificationService.success("Article was removed from feed");
              this.refresh();
            });
        }
      });
  }

  updatePage(newPage: number): void {
    this.params.page = newPage;
    this.refresh();
  }

  clearAllSearchFilters(): void {
    
  }

  drop = (event: CdkDragDrop<ArticleOrderSummary[]>) => {
    if(this.overrideFeedId != null) {
      this.notificationService.error("You cant update order of this feed because it is overriden by country newsfeed");
      return;
    }

    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
      if(event.previousContainer.data == this.horizontalGroupArticles){
        transferArrayItem(this.verticalGroupArticles,
          this.horizontalGroupArticles,
          0,
          2);
      }
      if(event.previousContainer.data == this.verticalGroupArticles) {
        transferArrayItem(this.horizontalGroupArticles,
          this.verticalGroupArticles,
          3,
          0);
      }
    }

    var newOrder = this.horizontalGroupArticles.map(x => x.id).concat(this.verticalGroupArticles.map(x => x.id));
    this.contentService.updateOrder(<UpdateArticleOrderParams> { 
      id: this.articleOrderId,
      order: newOrder, 
      city: this.params.city, 
      state: this.params.state, 
      country: this.params.country, 
      serviceArea: this.params.serviceArea, 
      listingType: this.params.listingType,
      page: this.params.page, 
      topic: this.params.topic,
      pageSize: this.params.pageSize }).subscribe(result => {
        if(result == null) {
          this.notificationService.error("Error occured during reordering. Please refresh page and try again.");
        } else {
          this.articleOrderId = result;
          this.refreshNextPageItemFlag();
        }
      });
  }

  hasNoData = () => {
    return this.horizontalGroupArticles != null && this.horizontalGroupArticles.length == 0;
  }

  getImageOrDefault = (imageUrl) => {
    if(imageUrl == null || imageUrl == "" || !this.validUrl(imageUrl)) {
      return "/images/image-placeholder.png";
    }

    return imageUrl;    
  }

  validUrl(str) {
    var res = str.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
    return (res !== null)
  }

  canDelete() {
    return this.isAdmin || (this.params.city != "" || this.params.serviceArea != "" || this.params.state != "");
  }
}
