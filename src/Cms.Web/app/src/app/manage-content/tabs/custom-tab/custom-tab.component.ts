import {
  Component,
  EventEmitter,
  OnDestroy,
  ViewChild,
  AfterViewInit
} from "@angular/core";
import { MatSort, MatPaginator, MatTableDataSource } from "@angular/material";
import { merge, of } from "rxjs";
import {
  takeUntil,
  switchMap,
  map,
  catchError,
  startWith
} from "rxjs/operators";

import { ContentService } from "src/app/services/content.service";
import { TableOptions } from "src/app/shared/models/table/table-options.model";
import { ContentPreview } from "src/app/shared/models/content/content-preview.interface";
import { ContentQuery } from "src/app/shared/models/content/query/content-query.model";

@Component({
  selector: "manage-custom-tab",
  templateUrl: "./custom-tab.component.html"
})
export class CustomTabComponent implements AfterViewInit, OnDestroy {
  dataSource: MatTableDataSource<ContentPreview> = new MatTableDataSource();
  displayedColumns: string[] = [
    "title",
    "tags",
    "createDate",
    "createdBy",
    "status",
    "geoTags"
  ];
  filter: EventEmitter<void> = new EventEmitter();
  resultsLength = 0;
  tableOptions: TableOptions = new TableOptions();

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  private onDestroy: EventEmitter<void> = new EventEmitter();

  constructor(private contentService: ContentService) {}

  ngAfterViewInit(): void {
    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.pipe(takeUntil(this.onDestroy)).subscribe(() => {
      this.paginator.pageIndex = 0;
    });

    // If the user updates a filter, reset back to the first page.
    this.filter.pipe(takeUntil(this.onDestroy)).subscribe(() => {
      this.paginator.pageIndex = 0;
    });

    merge(this.sort.sortChange, this.paginator.page, this.filter)
      .pipe(
        takeUntil(this.onDestroy),
        startWith({}),
        switchMap(() => this.contentService.paginate(this.getContentQuery())),
        map((data: ContentPreview[]) => {
          this.resultsLength =
            !!data && data.length > 0 ? data[0].totalCount : 0;
          return data;
        }),
        catchError(() => of([]))
      )
      .subscribe((data: ContentPreview[]) => {
        this.dataSource.data = data;
      });
  }

  ngOnDestroy(): void {
    this.onDestroy.next();
  }

  private getContentQuery = (): ContentQuery => {
    const query = new ContentQuery();
    query.filters = this.tableOptions.filters;
    query.limit = this.paginator.pageSize;
    query.orderBy = this.sort.active;
    query.page = this.paginator.pageIndex + 1;
    return query;
  };
}
