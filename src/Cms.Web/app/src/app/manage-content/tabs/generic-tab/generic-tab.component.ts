import {
  Component,
  EventEmitter,
  OnDestroy,
  Input,
  ViewChild,
} from "@angular/core";
import { takeUntil, catchError } from "rxjs/operators";

import { AuthService } from "src/app/auth/auth.service";
import { ConfirmDialogService } from "src/app/services/confirm-dialog.service";
import { ContentService } from "src/app/services/content.service";
import { NotificationService } from "src/app/services/notification.service";
import { ContentPreview } from "src/app/shared/models/content/content-preview.interface";
import { UserProfile } from "src/app/shared/models/user/user-profile.model";
import { CheckedOutLink } from "../../constants/links.constants";
import { CardSearchComponent } from "../../../components/card-search/card-search.component";
import { ContentLocationService } from "../../../services/content-location.service";
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from "@angular/material/dialog";
import { DialogAddToCountryNewsfeedComponent } from "./dialog-add-to-country-newsfeed.component";
import { countries } from "../../../components/card-city-picker/card-city-picker.constants";

@Component({
  selector: "manage-generic-tab",
  templateUrl: "./generic-tab.component.html",
  styleUrls: ["./generic-tab.component.scss"],
})
export class GenericTabComponent implements OnDestroy {
  @Input() activeLink: string;
  @ViewChild("cardSearch", { static: false }) cardSearch: CardSearchComponent;

  content: ContentPreview[];
  isAdmin = false;
  linkTarget: string;

  private onDestroy: EventEmitter<void> = new EventEmitter();

  constructor(
    authService: AuthService,
    private confirmDialogService: ConfirmDialogService,
    private contentService: ContentService,
    private contentLocationService: ContentLocationService,
    private notificationService: NotificationService,
    public dialog: MatDialog
  ) {
    this.isAdmin = authService.isAdmin;
    this.linkTarget = (authService.currentUser.profile || new UserProfile())
      .openContentInNewTab
      ? "_blank"
      : "_self";
  }

  ngOnDestroy(): void {
    this.onDestroy.next();
  }

  checkIn(content: ContentPreview): void {
    this.contentService
      .checkIn(content.code)
      .pipe(takeUntil(this.onDestroy))
      .subscribe(() => {
        this.notificationService.success("Checked in article");
        if (this.activeLink == CheckedOutLink) {
          this.cardSearch.refreshContent();
        }
      });
  }

  addingToMyNewsfeed: boolean;
  addToMyNewsfeed(content: ContentPreview): void {
    if (!this.addingToMyNewsfeed) {
      this.addingToMyNewsfeed = true;
      this.contentLocationService
        .addToMyNewsfeed(content.code)
        .pipe(takeUntil(this.onDestroy))
        .subscribe(
          () => {
            content.isInMyNewsfeed = true;
            this.notificationService.success("Article added to newsfeed");
            this.addingToMyNewsfeed = false;
          },
          (error) => {
            this.addingToMyNewsfeed = false;
          }
        );
    }
  }

  addingToCountryNewsfeed: boolean;
  addToCountryNewsfeed(content: ContentPreview): void {
    if (!this.addingToCountryNewsfeed) {
      this.addingToCountryNewsfeed = true;

      const dialogRef = this.dialog.open(DialogAddToCountryNewsfeedComponent, {
        width: "250px",
        data: { countries: [] },
      });

      dialogRef.afterClosed().subscribe((result) => {
        this.contentLocationService
          .addToCountriesNewsfeed(content.code, result)
          .subscribe(
            (x) => {
              this.addingToCountryNewsfeed = false;
              this.notificationService.success("Article added to newsfeed");
              this.cardSearch.refreshContent();
            },
            (error) => {
              this.addingToCountryNewsfeed = false;
            }
          );
      });
    }
  }

  deleteFromMyNewsfeed(content: ContentPreview): void {
    if (!this.addingToMyNewsfeed) {
      this.addingToMyNewsfeed = true;
      this.contentLocationService
        .deleteFromMyNewsfeed(content.code)
        .pipe(takeUntil(this.onDestroy))
        .subscribe(
          () => {
            content.isInMyNewsfeed = false;
            this.notificationService.success(
              "Article deleted from my newsfeed"
            );
            this.addingToMyNewsfeed = false;
          },
          (error) => {
            this.addingToMyNewsfeed = false;
          }
        );
    }
  }

  getColor = (status: string): string =>
    status === "draft" || status === "pending_review" ? "orange" : "green";

  getPreviewUrl = (code: string): string =>
    `${window["terminusBaseUrl"]}/content/${code}`;

  showConfirmDelete(id: number): void {
    this.confirmDialogService
      .showConfirmDelete()
      .pipe(takeUntil(this.onDestroy))
      .subscribe((confirmed: boolean) => {
        if (confirmed) {
          this.contentService
            .delete(id)
            .pipe(takeUntil(this.onDestroy))
            .subscribe(() => {
              this.notificationService.success("Deleted article");
              if (this.activeLink == CheckedOutLink) {
                this.cardSearch.refreshContent();
              }
            });
        }
      });
  }

  showConfirmUnpublish(content: ContentPreview): void {
    this.confirmDialogService
      .showConfirmUnpublish()
      .pipe(takeUntil(this.onDestroy))
      .subscribe((confirmed: boolean) => {
        if (confirmed) {
          this.contentService
            .unpublish(content.code)
            .pipe(takeUntil(this.onDestroy))
            .subscribe(() => {
              this.notificationService.success("Unpublished article");
            });
        }
      });
  }
}
