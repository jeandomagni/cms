import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";

import { ServiceAreaService } from "src/app/services/service-area.service";
import { DialogAddToCountryNewsfeedDataModel } from "./dialog-add-to-country-newsfeed-data.model";

@Component({
  selector: "dialog-add-to-country-newsfeed",
  templateUrl: "./dialog-add-to-country-newsfeed.component.html",
})
export class DialogAddToCountryNewsfeedComponent {
  countryList: string[];

  constructor(
    public dialogRef: MatDialogRef<DialogAddToCountryNewsfeedComponent>,
    private serviceAreaService: ServiceAreaService,
    @Inject(MAT_DIALOG_DATA) public data: DialogAddToCountryNewsfeedDataModel
  ) {
    if (this.data.countries.indexOf("USA") == -1) {
      this.data.countries.push("USA");
    }

    this.serviceAreaService.getCountriesForUser().subscribe((countries) => {
      this.countryList = countries;
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
