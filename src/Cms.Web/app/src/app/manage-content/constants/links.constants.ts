export const AllDraftsLink: string = 'All Drafts';
export const PendingReviewLink: string = 'Pending Review';
export const ExpiredContentLink: string = 'Expired Content';
export const DomainContentLink: string = 'Domain Content';
export const AllContentLink: string = 'All Content';
export const CustomLink: string = 'Custom';
export const NewsFeedLink: string = 'News Feed';
export const CheckedOutLink: string = 'Checked Out';

export const Links: string[] = [
  AllDraftsLink,
  PendingReviewLink,
  ExpiredContentLink,
  DomainContentLink,
  AllContentLink,
  CustomLink,
  NewsFeedLink,
  CheckedOutLink
];