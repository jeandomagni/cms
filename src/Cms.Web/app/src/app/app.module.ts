import { NgModule } from "@angular/core";

import { CommonLayoutComponent } from "src/app/shared/common-layout/common-layout.component";
import { LoaderComponent } from "src/app/components/loading/loader.component";
import { LoaderService } from "src/app/components/loading/loader.service";

import { AppComponent } from "./app.component";
import { imports } from "./app.imports";

@NgModule({
  declarations: [AppComponent, CommonLayoutComponent, LoaderComponent],
  entryComponents: [LoaderComponent],
  imports,
  providers: [LoaderService],
  bootstrap: [AppComponent]
})
export class AppModule {}
