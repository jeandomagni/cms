import { ContentAction } from "src/app/shared/models/content/content-action.interface";

export const CreateAction: ContentAction = {
  label: "Create",
  raised: true,
  color: "primary",
  desc: "create article"
};

export const CheckOutAction: ContentAction = {
  label: "Check Out",
  raised: false,
  color: "accent",
  desc: "check out article"
};

export const CheckInAction: ContentAction = {
  label: "Check In",
  raised: false,
  color: "accent",
  desc: "check in article"
};

export const SaveAction: ContentAction = {
  label: "Save",
  raised: false,
  color: "accent",
  desc: "save article"
};

export const ApproveAction: ContentAction = {
  label: "Approve",
  raised: false,
  color: "accent",
  desc: "approve article"
};

export const UnpublishAction: ContentAction = {
  label: "Unpublish",
  raised: false,
  color: "accent",
  desc: "unpublish article"
};

export const PublishAction: ContentAction = {
  label: "Publish",
  raised: false,
  color: "accent",
  desc: "publish article"
};

export const PreviewAction: ContentAction = {
  label: "Preview",
  raised: false,
  desc: "preview article"
};

export const DeleteAction: ContentAction = {
  label: "Delete",
  raised: false,
  color: "warn",
  desc: "delete article"
};
