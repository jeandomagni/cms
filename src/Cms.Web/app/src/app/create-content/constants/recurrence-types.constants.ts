import { RecurrenceType } from "src/app/shared/models/content/event/recurrence-type.interface";

export const RecurrenceTypes: RecurrenceType[] = [
  { value: 1, label: "Daily" },
  { value: 2, label: "Weekly" },
  { value: 3, label: "Monthly" },
  { value: 4, label: "MonthNth" }
];
