import { ContentLayout } from "src/app/shared/models/content/content-layout.interface";

export const ContentImageContentLayout: ContentLayout = {
  id: "Image-Content",
  description: "Article with image above the article body text",
  articleTypes: ["Article", "Event"]
};

export const ContentImageLayout: ContentLayout = {
  id: "Content-Image",
  description: "Article with image below the article body text",
  articleTypes: ["Article", "Event"]
};

export const ContentVideoLayout: ContentLayout = {
  id: "Content-Video",
  description: "Article with video below the article body text",
  articleTypes: ["Article", "Event"]
};

export const ContentVideoCarouselLayout: ContentLayout = {
  id: "Content-Video-Carousel",
  description: "Article with video carousel below the article body text",
  articleTypes: ["Article", "Event"]
};

export const ContentVideoTopLayout: ContentLayout = {
  id: "Content-Video-Top",
  description: "Article with video above the article body text",
  articleTypes: ["Article", "Event"]
};

export const ContentVideoTopImageBottomLayout: ContentLayout = {
  id: "Content-Video-Top-Image-Bottom",
  description: "Article with video above and image below the article body text",
  articleTypes: ["Article", "Event"]
};

export const ContentImageTopVideoBottomLayout: ContentLayout = {
  id: "Content-Image-Top-Video-Bottom",
  description: "Article with image above and video below the article body text",
  articleTypes: ["Article", "Event"]
};

export const ContentImageCarouselLayout: ContentLayout = {
  id: "Content-Image-Carousel",
  description: "Article with image and carousel below the article body text",
  articleTypes: ["Article", "Event"]
};

export const ContentImageCarouselVideoCarouselLayout: ContentLayout = {
  id: "Content-Image-Carousel-Video-Carousel",
  description:
    "Article with image carousel and video carousel below the article body text",
  articleTypes: ["Article", "Event"]
};

export const ContentImageTopImageCarouselBottomLayout: ContentLayout = {
  id: "Content-Image-Top-Image-Carousel-Bottom",
  description:
    "Article with image above and image carousel below the article body text",
  articleTypes: ["Article", "Event"]
};

export const NearMeLayout: ContentLayout = {
  id: "Near-Me",
  description: "Near Me layout",
  articleTypes: ["Near Me"]
};

export const AbListAutomaticLayout: ContentLayout = {
  id: "AB-List-Automatic",
  description: "Automatic Layout",
  articleTypes: ["AB List"]
};

export const AbListOneColumnLayout: ContentLayout = {
  id: "AB-List-One-Column",
  description: "One Column Layout",
  articleTypes: ["AB List"]
};

export const AbListTwoColumnLayout: ContentLayout = {
  id: "AB-List-Two-Column",
  description: "Two Column Layout",
  articleTypes: ["AB List"]
};

export const BaselinePageLayout: ContentLayout = {
  id: "Baseline-Page",
  description: "Baseline page without image",
  articleTypes: ["BaselinePage"]
};

export const BaselinePageWithImageLayout: ContentLayout = {
  id: "Baseline-Page-Image",
  description: "Baseline page with image",
  articleTypes: ["BaselinePage"]
};

export const AllContentLayouts: ContentLayout[] = [
  ContentImageContentLayout,
  ContentImageLayout,
  ContentVideoLayout,
  ContentVideoCarouselLayout,
  ContentVideoTopLayout,
  ContentVideoTopImageBottomLayout,
  ContentImageTopVideoBottomLayout,
  ContentImageCarouselLayout,
  ContentImageCarouselVideoCarouselLayout,
  ContentImageTopImageCarouselBottomLayout,
  NearMeLayout,
  AbListAutomaticLayout,
  AbListOneColumnLayout,
  AbListTwoColumnLayout,
  BaselinePageLayout,
  BaselinePageWithImageLayout
];
