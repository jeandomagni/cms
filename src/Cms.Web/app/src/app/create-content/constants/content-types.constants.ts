import { ContentType } from "src/app/shared/models/content/content-type.interface";

export const AbListContentType: ContentType = {
  name: "AB List",
  isAdminOnly: false,
  hideFromCreate: false
};

export const ArticleContentType: ContentType = {
  name: "Article",
  isAdminOnly: false,
  hideFromCreate: false
};

export const EventContentType: ContentType = {
  name: "Event",
  isAdminOnly: false,
  hideFromCreate: false
};

export const NearMeContentType: ContentType = {
  name: "Near Me",
  isAdminOnly: true,
  hideFromCreate: false
};

export const BaselinePageContentType: ContentType = {
  name: "BaselinePage",
  isAdminOnly: false,
  hideFromCreate: true
};

export const AllContentTypes: ContentType[] = [
  AbListContentType,
  ArticleContentType,
  EventContentType,
  NearMeContentType,
  BaselinePageContentType
];
