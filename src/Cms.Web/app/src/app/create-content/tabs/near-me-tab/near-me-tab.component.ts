import { Component, Input } from "@angular/core";
import { Settings } from "tinymce";

import { AuthService } from "src/app/auth/auth.service";
import { TinymceService } from "src/app/services/tinymce.service";
import { Content } from "src/app/shared/models/content/content.model";
import { NearMe } from "src/app/shared/models/content/near-me/near-me.interface";

@Component({
  selector: "near-me-tab",
  templateUrl: "./near-me-tab.component.html"
})
export class NearMeTabComponent {
  @Input() content: Content;
  @Input() nearMe: NearMe;

  tinymceInit: Settings;
  tinymceKey: string;

  constructor(authService: AuthService, tinyMceService: TinymceService) {
    this.tinymceInit = authService.isAdmin
      ? tinyMceService.adminSettings
      : tinyMceService.globalSettings;
    this.tinymceKey = tinyMceService.key;
  }
}
