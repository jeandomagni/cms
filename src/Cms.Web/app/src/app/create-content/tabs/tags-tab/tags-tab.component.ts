import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnInit,
  OnDestroy,
  ViewChild,
  ElementRef
} from "@angular/core";
import { MatSnackBar } from "@angular/material";
import { Subject, Observable, Subscription } from 'rxjs';
import { takeUntil } from "rxjs/operators";

import { ContentLocation } from "src/app/shared/models/location/content-location.interface";
import { Geotag } from "src/app/shared/models/location/geotag.interface";
import { Pin } from "src/app/shared/models/location/pin.interface";
import { AuthService } from "src/app/auth/auth.service";
import { NotificationService } from "src/app/services/notification.service";
import { ContentNearMeService } from "src/app/services/content-near-me.service";
import { ContentLocationService } from "src/app/services/content-location.service";
import { ContentService } from "src/app/services/content.service";
import { TagService } from "src/app/services/tag.service";
import { Content } from "src/app/shared/models/content/content.model";
import { BaseAutocompleteComponent } from "src/app/components/base/base-autocomplete/base-autocomplete.component";
import { AllContentTypes, NearMeContentType } from '../../constants/content-types.constants';
import { ENTER } from "@angular/cdk/keycodes";

@Component({
  selector: "tags-tab",
  templateUrl: "./tags-tab.component.html",
  styleUrls: ["./tags-tab.component.scss"]
})
export class TagsTabComponent extends BaseAutocompleteComponent<string>
  implements OnInit, OnDestroy {
  @Input() content: Content;
  @Output() contentChange: EventEmitter<Content> = new EventEmitter();
  @Output() updateContent: EventEmitter<void> = new EventEmitter();
  notFoundMessage = "No matching tags found. Press enter to add new.";
  separatorKeysCodes: number[] = [ENTER];

  categoryIdsToExclude: string[];
  isAdmin: boolean;
  showIndustryTip: boolean;

  @ViewChild("tagInput", { static: false }) tagInput: ElementRef;

  private readonly onDestroy = new Subject<void>();

  constructor(
    authService: AuthService,
    private contentNearMeService: ContentNearMeService,
    private notificationService: NotificationService,
    private snackBar: MatSnackBar,
    private tagService: TagService,
    private contentLocationService: ContentLocationService,
    private contentService: ContentService
  ) {
    super();

    this.isAdmin = authService.isAdmin;
  }

  ngOnInit(): void {
    this.contentNearMeService
      .getSelectedTobIds()
      .pipe(takeUntil(this.onDestroy))
      .subscribe((ids: string[]) => {
        this.categoryIdsToExclude = ids;
      });

      this.populateShowIndustryTip();
  }

  populateShowIndustryTip () {
    this.showIndustryTip = true;
    if(this.content.type == NearMeContentType.name) {
      this.showIndustryTip = false;
    }
  }

  ngOnDestroy(): void {
    this.onDestroy.next();
  }

  addTag(tag: string): void {
    this.tagInput.nativeElement.value = "";
    this.acCtrl.setValue(null);

    if (tag == null || tag == "") {
      return;
    }

    if (this.content.tags.find((x: string) => x === tag)) {
      this.notificationService.error(`You already added ${tag}.`);

      return;
    }

    this.content.tags.push(tag);
    this.contentChange.emit(this.content);

    this.updateTags();
  }

  removeTag(tag: string): void {
    this.content.tags = this.content.tags.filter((x: string) => x !== tag);
    this.contentChange.emit(this.content);

    this.updateTags();
  }

  pinTopic(topic: string): void {
    this.content.pinnedTopic = topic;
    this.contentChange.emit(this.content);

    this.updateContent.emit();
  }

  updateTopics(): void {
    if (this.content.topics.find(x => x === this.content.pinnedTopic)) {
      this.content.pinnedTopic = null;
    }

    this.contentChange.emit(this.content);
    this.updateContent.emit();
  }

  removeTopic(topic: string): void {
    this.contentService.removeTopic(this.content.code, topic).subscribe(x => {
      this.notificationService.success("Topic was successfully removed.");
    });
  }

  update(): void {
    this.updateContent.emit();
  }

  validateCategories(): boolean {
    let valid = true;
    const { categories, isIndustryTip } = this.content;
    const hasTobs = categories && categories.length;
    if (isIndustryTip && !hasTobs) {
      this.notificationService.error(
        "Industry Tip articles must have at least one TOB"
      );
      valid = false;
    }
    return valid;
  }

  updateIsIndustryTip(isIndustryTip: boolean): void {
    this.content.isIndustryTip = isIndustryTip;
  }

  updateCategories(): void {
    if (this.content.type === "Near Me") {
      if (this.content.categories && this.content.categories.length > 1) {
        this.notificationService.warningList([
          "Near Me pages can only have one TOB"
        ]);
        this.content.categories = [this.content.categories[0]];
      }
    }

    if (this.validateCategories()) {
      this.contentChange.emit(this.content);
      this.updateContent.emit();
    }
  }

  protected queryItems(value: string): Observable<string[]> {
    return this.tagService.query(value);
  }

  private updateTags(): void {
    this.tagService
      .updateContentTags(this.content.id, this.content.tags)
      .pipe(takeUntil(this.onDestroy))
      .subscribe(() => {
        this.content.tags = this.content.tags
          .map(value => value.toLowerCase())
          .filter((item, i, ar) => ar.indexOf(item) === i);
        this.contentChange.emit(this.content);
      });
  }

  addLocation(location: ContentLocation): void {
    this.contentLocationService
      .add(location, this.content)
      .pipe(takeUntil(this.onDestroy))
      .subscribe((data: Content) => {
        this.content = data;
        this.contentChange.emit(this.content);
        this.notificationService.success("Saved article");
      });
  }

  addPin(pin: Pin): void {
    this.contentLocationService
      .addPin(pin, this.content)
      .pipe(takeUntil(this.onDestroy))
      .subscribe((data: Content) => {
        this.content = data;
        this.contentChange.emit(this.content);
        this.notificationService.success("Saved article");
      });
  }

  removeGeotag(geotag: Geotag): void {
    this.contentLocationService
      .deleteGeotag(geotag)
      .pipe(takeUntil(this.onDestroy))
      .subscribe((data: Content) => {
        this.content = data;
        this.contentChange.emit(this.content);
        this.notificationService.success("Saved article");
      });
  }

  removeLocation(location: ContentLocation): void {
    this.contentLocationService
      .delete(location, this.content)
      .pipe(takeUntil(this.onDestroy))
      .subscribe((data: Content) => {
        this.content = data;
        this.contentChange.emit(this.content);
        this.notificationService.success("Saved article");
      });
  }

  removePin(pin: Pin): void {
    this.contentLocationService
      .deletePin(pin, this.content)
      .pipe(takeUntil(this.onDestroy))
      .subscribe((data: Content) => {
        this.content = data;
        this.contentChange.emit(this.content);
        this.notificationService.success("Saved article");
      });
  }

  updateGeotag(geotag: Geotag): void {
    this.contentLocationService
      .updateGeotag(geotag)
      .pipe(takeUntil(this.onDestroy))
      .subscribe((data: Content) => {
        this.content = data;
        this.contentChange.emit(this.content);
        this.notificationService.success("Saved article");
      });
  }

  updatePin(pin: Pin): void {
    this.contentLocationService
      .updatePin(pin, this.content)
      .pipe(takeUntil(this.onDestroy))
      .subscribe((data: Content) => {
        this.content = data;
        this.contentChange.emit(this.content);
        this.notificationService.success("Saved article");
      });
  }
}
