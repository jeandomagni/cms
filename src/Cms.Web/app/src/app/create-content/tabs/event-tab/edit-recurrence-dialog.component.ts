import { Component, Inject, OnDestroy } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

import { Days } from "src/app/constants/days.constants";
import { EditRecurrenceDialogData } from "src/app/create-content/tabs/event-tab/edit-recurrence-dialog-data.interface";
import { RecurrenceTypes } from "src/app/create-content/constants/recurrence-types.constants";
import { Instances } from "src/app/create-content/tabs/event-tab/instances.constants";
import { EventRecurrenceService } from "src/app/services/event-recurrence.service";
import { NotificationService } from "src/app/services/notification.service";
import { RecurrenceType } from "src/app/shared/models/content/event/recurrence-type.interface";

@Component({
  selector: "edit-recurrence-dialog",
  templateUrl: "./edit-recurrence-dialog.component.html"
})
export class EditRecurrenceDialogComponent implements OnDestroy {
  days: string[] = Days;
  instances = Instances;
  types: RecurrenceType[] = RecurrenceTypes;

  private readonly onDestroy = new Subject<void>();

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: EditRecurrenceDialogData,
    private dialogRef: MatDialogRef<EditRecurrenceDialogComponent>,
    private eventRecurrenceService: EventRecurrenceService,
    private notificationService: NotificationService
  ) {}

  ngOnDestroy(): void {
    this.onDestroy.next();
  }

  update(): void {
    this.data.eventRecurrence.startTime = this.getTimeString(
      this.data.eventRecurrence.startTime
    );
    this.data.eventRecurrence.endTime = this.getTimeString(
      this.data.eventRecurrence.endTime
    );
    this.eventRecurrenceService
      .validate(this.data.eventRecurrence)
      .pipe(takeUntil(this.onDestroy))
      .subscribe((error: string) => {
        if (!error) {
          this.dialogRef.close(this.data.eventRecurrence);
        } else {
          this.notificationService.error(error);
        }
      });
  }

  private getTimeString(dateString: string): string {
    if (!dateString) return null;

    const date = new Date(dateString);
    let hours = date.getHours();
    const minutes = date.getMinutes();
    const ampm = hours >= 12 ? "PM" : "AM";
    hours %= 12;
    hours = hours || 12; // the hour '0' should be '12'
    const minutesString = minutes < 10 ? `0${minutes}` : minutes;
    return `${hours}:${minutesString} ${ampm}`;
  }
}
