import { RecurrenceSettings } from "src/app/shared/models/content/event/recurrence-settings.interface";

export interface EditRecurrenceDialogData {
  eventRecurrence: RecurrenceSettings;
}
