import { Instance } from "src/app/create-content/tabs/event-tab/instance.interface";

export const Instances: Instance[] = [
  { value: 1, label: "first" },
  { value: 2, label: "second" },
  { value: 3, label: "third" },
  { value: 4, label: "fourth" },
  { value: 5, label: "last" }
];
