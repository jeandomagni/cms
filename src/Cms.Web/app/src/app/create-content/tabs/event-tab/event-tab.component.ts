﻿import { SelectionModel } from "@angular/cdk/collections";
import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnInit,
  ViewChild,
  OnChanges,
  OnDestroy
} from "@angular/core";
import {
  MatDialog,
  MatSort,
  MatTableDataSource,
  MatPaginator
} from "@angular/material";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

import { Countries } from "src/app/constants/countries.constants";
import { EditRecurrenceDialogComponent } from "src/app/create-content/tabs/event-tab/edit-recurrence-dialog.component";
import { ConfirmDialogService } from "src/app/services/confirm-dialog.service";
import { EventRecurrenceService } from "src/app/services/event-recurrence.service";
import { NotificationService } from "src/app/services/notification.service";
import { LegacyBbbInfo } from "src/app/shared/models/bbb/legacy-bbb-info.model";
import { Content } from "src/app/shared/models/content/content.model";
import { Event } from "src/app/shared/models/content/event/event.model";
import { RecurrenceSettings } from "src/app/shared/models/content/event/recurrence-settings.interface";
import { Recurrence } from "src/app/shared/models/content/event/recurrence.interface";

@Component({
  selector: "event-tab",
  templateUrl: "./event-tab.component.html"
})
export class EventTabComponent implements OnInit, OnChanges, OnDestroy {
  @Input() content: Content;
  @Input() event: Event;
  @Output() contentChange: EventEmitter<Content> = new EventEmitter();
  @Output() eventChange: EventEmitter<Event> = new EventEmitter();
  @Output() update: EventEmitter<void> = new EventEmitter();
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  countries = Countries;
  displayedOccurrenceColumns: string[] = ["select", "startDate", "endDate"];
  selectedOccurrence: SelectionModel<Recurrence> = new SelectionModel<
    Recurrence
  >(false, []);
  occurrencesDataSource: MatTableDataSource<Recurrence>;
  private readonly onDestroy = new Subject<void>();

  constructor(
    private confirmDialogService: ConfirmDialogService,
    private dialog: MatDialog,
    private eventRecurrenceService: EventRecurrenceService,
    private notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    this.occurrencesDataSource = new MatTableDataSource(
      this.content.occurrences
    );
    this.occurrencesDataSource.sort = this.sort;
    this.occurrencesDataSource.paginator = this.paginator;
  }

  ngOnChanges(): void {
    if (this.occurrencesDataSource) {
      this.occurrencesDataSource.data = this.content.occurrences;
    }
  }

  ngOnDestroy(): void {
    this.onDestroy.next();
  }

  addEditEventRecurrence(): void {
    const dialogData = { eventRecurrence: this.event.recurrence || {} };
    const dialogRef = this.dialog.open(EditRecurrenceDialogComponent, {
      data: dialogData
    });

    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.onDestroy))
      .subscribe((eventRecurrence: RecurrenceSettings) => {
        if (!eventRecurrence) {
          return;
        }

        this.event.startDate = this.combineDateAndTime(
          eventRecurrence.startDate,
          eventRecurrence.startTime || "12:00 AM"
        );
        this.event.endDate = this.combineDateAndTime(
          eventRecurrence.endDate,
          eventRecurrence.endTime || "11:59 PM"
        );

        this.event.recurrence = eventRecurrence;
        this.eventChange.emit(this.event);
        this.update.emit();
      });
  }

  deleteOccurrence(): void {
    this.confirmDialogService
      .showConfirmDelete()
      .pipe(takeUntil(this.onDestroy))
      .subscribe(confirmed => {
        if (confirmed) {
          const toDelete = this.selectedOccurrence.selected[0];

          this.content.occurrences = this.content.occurrences.filter(
            x => x !== toDelete
          );
          this.contentChange.emit(this.content);
          this.eventRecurrenceService.delete(toDelete.id).subscribe(() => {
            this.selectedOccurrence.clear();
            this.update.emit();
          });
        }
      });
  }

  deleteRecurrence(): void {
    this.confirmDialogService
      .showConfirmCustom(
        "Are you sure that you want to remove event recurrence?",
        "Remove Recurrence"
      )
      .pipe(takeUntil(this.onDestroy))
      .subscribe(confirmed => {
        if (confirmed) {
          this.eventRecurrenceService
            .deleteAllForContentCode(this.content.code)
            .pipe(takeUntil(this.onDestroy))
            .subscribe((res: Event) => {
              this.content.occurrences = [];
              this.event = res;

              this.contentChange.emit(this.content);
              this.eventChange.emit(this.event);

              this.notificationService.success("Saved article");
            });
        }
      });
  }

  updateContentEventBbb(location: LegacyBbbInfo): void {
    this.content.eventBbb = location;
    this.contentChange.emit(this.content);
  }

  private combineDateAndTime(date: Date, timeString: string): Date {
    return this.setHours(date, timeString);
  }

  private setHours(dt: Date, h: string): Date {
    const s = /(\d+):(\d+)(.+)/.exec(h);
    dt.setHours(s[3] === "PM" ? 12 + parseInt(s[1], 10) : parseInt(s[1], 10));
    dt.setMinutes(parseInt(s[2], 10));
    return dt;
  }
}
