﻿import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";

import { EditBusinessLinkDialogData } from "src/app/create-content/tabs/ab-list-tab/dialogs/edit-business-link-dialog-data.interface";

@Component({
  selector: "edit-business-link-dialog",
  templateUrl: "./edit-business-link-dialog.component.html"
})
export class EditBusinessLinkDialogComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: EditBusinessLinkDialogData,
    private dialogRef: MatDialogRef<EditBusinessLinkDialogComponent>
  ) {}

  update(): void {
    this.dialogRef.close(this.data.businessLink);
  }
}
