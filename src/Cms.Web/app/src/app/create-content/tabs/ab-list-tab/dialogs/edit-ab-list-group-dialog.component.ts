﻿import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";

import { EditAbListGroupDialogData } from "src/app/create-content/tabs/ab-list-tab/dialogs/edit-ab-list-group-dialog-data.interface";

@Component({
  selector: "edit-ab-list-group-dialog",
  templateUrl: "./edit-ab-list-group-dialog.component.html"
})
export class EditAbListGroupDialogComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: EditAbListGroupDialogData,
    private dialogRef: MatDialogRef<EditAbListGroupDialogComponent>
  ) {}

  update(): void {
    this.data.group.businessLinks = this.data.group.businessLinks || [];
    this.dialogRef.close(this.data.group);
  }
}
