import { LinkListGrid } from "src/app/create-content/tabs/ab-list-tab/models/link-list-grid.model";
import { BusinessLinkList } from "src/app/shared/models/content/ab-list/business-link-list.interface";

export class Group {
  List: BusinessLinkList;
  Grid: LinkListGrid;

  constructor(linkList: BusinessLinkList) {
    this.List = linkList;
    this.Grid = new LinkListGrid(linkList.businessLinks);
  }
}
