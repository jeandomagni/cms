import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnChanges
} from "@angular/core";
import { MatDialog } from "@angular/material";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

import { EditAbListGroupDialogComponent } from "src/app/create-content/tabs/ab-list-tab/dialogs/edit-ab-list-group-dialog.component";
import { EditAbListGroupDialogData } from "src/app/create-content/tabs/ab-list-tab/dialogs/edit-ab-list-group-dialog-data.interface";
import { EditBusinessLinkDialogComponent } from "src/app/create-content/tabs/ab-list-tab/dialogs/edit-business-link-dialog.component";
import { EditBusinessLinkDialogData } from "src/app/create-content/tabs/ab-list-tab/dialogs/edit-business-link-dialog-data.interface";
import { Group } from "src/app/create-content/tabs/ab-list-tab/models/group.model";
import { ConfirmDialogService } from "src/app/services/confirm-dialog.service";
import { AbList } from "src/app/shared/models/content/ab-list/ab-list.interface";
import { BusinessLinkList } from "src/app/shared/models/content/ab-list/business-link-list.interface";
import { Link } from "src/app/shared/models/link.interface";

@Component({
  selector: "ab-list-tab",
  templateUrl: "./ab-list-tab.component.html"
})
export class AbListTabComponent implements OnChanges {
  @Input() abList: AbList;
  @Output() abListChange: EventEmitter<AbList> = new EventEmitter();
  @Input() canEdit: boolean;
  @Output() update: EventEmitter<void> = new EventEmitter();

  displayedLinkListColumns = ["select", "linkText", "linkUrl"];
  groups: Group[];

  private readonly onDestroy = new Subject<void>();

  constructor(
    private confirmDialogService: ConfirmDialogService,
    private dialog: MatDialog
  ) {}

  ngOnChanges(): void {
    this.groups = this.abList.businessLinkLists.map(list =>
      this.getGroup(list)
    );
  }

  addBusinessLink(group: Group): void {
    const indexToUpdate = this.abList.businessLinkLists.findIndex(
      x => x.title === group.List.title
    );

    const dialogData: EditBusinessLinkDialogData = {
      businessLink: { linkText: null, linkUrl: null }
    };
    const dialogRef = this.dialog.open(EditBusinessLinkDialogComponent, {
      data: dialogData
    });

    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.onDestroy))
      .subscribe((link: Link) => {
        if (!link) {
          return;
        }

        this.abList.businessLinkLists[indexToUpdate].businessLinks.push(link);

        this.abListChange.emit(this.abList);
        this.update.emit();
      });
  }

  editBusinessLink(group: Group): void {
    const indexToUpdate = this.abList.businessLinkLists.findIndex(
      x => x.title === group.List.title
    );
    const linkToUpdate = group.Grid.selected.selected[0];

    const dialogData: EditBusinessLinkDialogData = {
      businessLink: { ...linkToUpdate }
    };
    const dialogRef = this.dialog.open(EditBusinessLinkDialogComponent, {
      data: dialogData
    });

    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.onDestroy))
      .subscribe((link: Link) => {
        if (!link) {
          return;
        }

        this.abList.businessLinkLists[
          indexToUpdate
        ].businessLinks = this.abList.businessLinkLists[
          indexToUpdate
        ].businessLinks.map((x: Link) => (x === linkToUpdate ? link : x));

        this.abListChange.emit(this.abList);
        this.update.emit();
      });
  }

  addGroup(): void {
    const dialogData: EditAbListGroupDialogData = {
      group: { businessLinks: [], title: "" }
    };
    const dialogRef = this.dialog.open(EditAbListGroupDialogComponent, {
      data: dialogData
    });

    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.onDestroy))
      .subscribe((updatedBusinessLinkList: BusinessLinkList) => {
        if (!updatedBusinessLinkList) {
          return;
        }

        this.abList.businessLinkLists.push(updatedBusinessLinkList);
        this.abListChange.emit(this.abList);
        this.update.emit();
      });
  }

  editGroup(group: BusinessLinkList): void {
    const indexToUpdate = this.abList.businessLinkLists.findIndex(
      x => x.title === group.title
    );

    const dialogData: EditAbListGroupDialogData = { group };
    const dialogRef = this.dialog.open(EditAbListGroupDialogComponent, {
      data: dialogData
    });

    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.onDestroy))
      .subscribe((updatedBusinessLinkList: BusinessLinkList) => {
        if (!updatedBusinessLinkList) {
          return;
        }

        this.abList.businessLinkLists[indexToUpdate] = updatedBusinessLinkList;
        this.abListChange.emit(this.abList);
        this.update.emit();
      });
  }

  deleteBusinessLink(group: Group): void {
    this.confirmDialogService
      .showConfirmDelete()
      .pipe(takeUntil(this.onDestroy))
      .subscribe(confirmed => {
        if (confirmed) {
          const indexToUpdate = this.abList.businessLinkLists.findIndex(
            x => x.title === group.List.title
          );
          const linkToRemove = group.Grid.selected.selected[0];
          this.abList.businessLinkLists[
            indexToUpdate
          ].businessLinks = this.abList.businessLinkLists[
            indexToUpdate
          ].businessLinks.filter((x: Link) => x !== linkToRemove);

          this.abListChange.emit(this.abList);
          this.update.emit();
        }
      });
  }

  deleteGroup(group: Group): void {
    this.confirmDialogService
      .showConfirmDelete()
      .pipe(takeUntil(this.onDestroy))
      .subscribe(confirmed => {
        if (confirmed) {
          this.abList.businessLinkLists = this.abList.businessLinkLists.filter(
            x => x !== group.List
          );
          this.abListChange.emit(this.abList);
          this.update.emit();
        }
      });
  }

  private getGroup(linkList: BusinessLinkList): Group {
    const group = new Group(linkList);
    return group;
  }
}
