import { MatTableDataSource } from "@angular/material";
import { SelectionModel } from "@angular/cdk/collections";

import { Link } from "src/app/shared/models/link.interface";

export class LinkListGrid {
  dataSource: MatTableDataSource<Link>;
  selected: SelectionModel<Link>;

  constructor(links: Link[]) {
    this.dataSource = new MatTableDataSource<Link>(links);
    this.selected = new SelectionModel<Link>(false, []);
  }
}
