import { Link } from "src/app/shared/models/link.interface";

export interface EditBusinessLinkDialogData {
  businessLink: Link;
}
