import { Component, Input } from "@angular/core";
import { Settings } from "tinymce";

import { AuthService } from "src/app/auth/auth.service";
import { TinymceService } from "src/app/services/tinymce.service";
import { Content } from "src/app/shared/models/content/content.model";

@Component({
  selector: "content-tab",
  templateUrl: "./content-tab.component.html"
})
export class ContentTabComponent {
  @Input() content: Content;

  tinymceInit: Settings;
  tinymceKey: string;

  constructor(authService: AuthService, tinyMceService: TinymceService) {
    this.tinymceInit = authService.isAdmin
      ? tinyMceService.adminSettings
      : tinyMceService.globalSettings;
    this.tinymceKey = tinyMceService.key;
  }
}
