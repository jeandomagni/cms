import { Component, Input } from "@angular/core";

import { AuthService } from "src/app/auth/auth.service";
import { Content } from "src/app/shared/models/content/content.model";

@Component({
  selector: "meta-tab",
  templateUrl: "./meta-tab.component.html"
})
export class MetaTabComponent {
  @Input() content: Content;
  isAdmin: boolean;

  constructor(authService: AuthService) {
    this.isAdmin = authService.isAdmin;
  }
}
