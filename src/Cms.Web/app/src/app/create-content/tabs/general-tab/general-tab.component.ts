import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
} from "@angular/core";
import { FormControl } from "@angular/forms";
import { Observable } from "rxjs";
import { map, startWith } from "rxjs/operators";

import { AuthService } from "src/app/auth/auth.service";
import {
  AbListAutomaticLayout,
  AllContentLayouts,
} from "src/app/create-content/constants/content-layouts.constants";
import { AllContentTypes } from "src/app/create-content/constants/content-types.constants";
import { ContentLayout } from "src/app/shared/models/content/content-layout.interface";
import { Content } from "src/app/shared/models/content/content.model";

@Component({
  selector: "general-tab",
  templateUrl: "./general-tab.component.html",
})
export class GeneralTabComponent implements OnChanges {
  @Input() content: Content;
  @Output() contentChange: EventEmitter<Content> = new EventEmitter();

  filteredLayouts: Observable<ContentLayout[]>;
  filteredTypes: Observable<string[]> = new Observable().pipe(
    map(() => AllContentTypes.map((x) => x.name))
  );
  layoutCtrl: FormControl = new FormControl();
  layoutToHighlight: string;
  layouts: ContentLayout[];
  typeCtrl: FormControl = new FormControl();
  types: string[];
  typeToHighlight: string;
  selectedLayout: string;

  constructor(authService: AuthService) {
    var allContentTypes = AllContentTypes.filter((x) => !x.hideFromCreate);

    const contentTypes = authService.isAdmin
      ? allContentTypes
      : allContentTypes.filter((x) => !x.isAdminOnly);
    this.types = contentTypes.map((x) => x.name);

    this.filteredLayouts = this.layoutCtrl.valueChanges.pipe(
      startWith(AllContentLayouts),
      map((value) => this.filterLayouts(value))
    );

    this.filteredTypes = this.typeCtrl.valueChanges.pipe(
      startWith(this.types),
      map((value) => this.filterTypes(value))
    );
  }

  // implemented in ngOnChanges to handle edit view to create view transition
  ngOnChanges(): void {
    if (!this.content.canEdit) {
      this.typeCtrl.disable();
    } else {
      this.typeCtrl.enable();
    }

    if (!this.content.canEdit || !this.content.type) {
      this.layoutCtrl.disable();
    } else {
      this.layoutCtrl.enable();
    }

    this.typeCtrl.setValue(this.content.type);
    this.layoutCtrl.setValue(
      this.content.layout
        ? AllContentLayouts.find((x) => x.id === this.content.layout)
        : null
    );

    this.layouts = this.content.type
      ? AllContentLayouts.filter((x) =>
          x.articleTypes.includes(this.content.type)
        )
      : AllContentLayouts;
  }

  displayLayout(layout: ContentLayout): string {
    if (!layout) return null;
    return layout.description;
  }

  selectLayout(layout: ContentLayout): void {
    this.content.layout = layout ? layout.id : null;
    this.contentChange.emit(this.content);
  }

  selectType(type: string): void {
    this.content.type = type;
    this.contentChange.emit(this.content);
    this.typeCtrl.setValue(this.content.type);

    // enable layoutCtrl if it isn't already
    if (this.layoutCtrl.disabled) {
      this.layoutCtrl.enable();
    }

    this.layouts = this.content.type
      ? AllContentLayouts.filter((x) =>
          x.articleTypes.includes(this.content.type)
        )
      : AllContentLayouts;

    if (this.layouts.find((x) => x === this.layoutCtrl.value)) {
      // If there's already a valid layout set, we won't override it.
      return;
    } else {
      // If it's invalid, we'll blank it out
      const layoutToSelect = type === "AB List" ? AbListAutomaticLayout : null;

      this.layoutCtrl.setValue(layoutToSelect);
      this.selectLayout(layoutToSelect);
    }
  }

  private filterLayouts(value: string): ContentLayout[] {
    if (!value || typeof value !== "string") {
      this.layoutToHighlight = "";
      return this.layouts;
    }

    this.layoutToHighlight = value;
    const filterValue = value.toLowerCase();
    return this.layouts.filter((layout: ContentLayout) =>
      layout.description.toLowerCase().includes(filterValue)
    );
  }

  private filterTypes(value: string): string[] {
    if (!value || typeof value !== "string") {
      this.typeToHighlight = "";
      return this.types;
    }

    this.typeToHighlight = value;
    const filterValue = value.toLowerCase();
    return this.types.filter((type: string) =>
      type.toLowerCase().includes(filterValue)
    );
  }
}
