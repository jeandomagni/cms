import {
  Component,
  Input,
  OnInit,
  OnDestroy,
  Output,
  EventEmitter,
  OnChanges,
  ViewChild
} from "@angular/core";
import { SelectionModel } from "@angular/cdk/collections";
import { MatDialog, MatTableDataSource, MatSort } from "@angular/material";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

import { EditVideoDialogComponent } from "src/app/create-content/tabs/media-tab/edit-video-dialog.component";
import { EditVideoDialogData } from "src/app/create-content/tabs/media-tab/edit-video-dialog-data.interface";
import { ConfirmDialogService } from "src/app/services/confirm-dialog.service";
import { Content } from "src/app/shared/models/content/content.model";
import { Media } from "src/app/shared/models/content/entities/media.model";
import { Video } from "src/app/shared/models/content/entities/video.model";

@Component({
  selector: "media-tab",
  templateUrl: "./media-tab.component.html"
})
export class MediaTabComponent implements OnInit, OnChanges, OnDestroy {
  @Input() carousel: Media[];
  @Input() content: Content;
  @Output() contentChange: EventEmitter<Content> = new EventEmitter();
  @Output() updateContent: EventEmitter<void> = new EventEmitter();
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  displayedVideoColumns: string[] = [
    "select",
    "url",
    "title",
    "caption",
    "position"
  ];
  mainImage: Media[] = [];
  selectedVideo: SelectionModel<Video> = new SelectionModel<Video>(false, []);
  videoDataSource: MatTableDataSource<Video> = new MatTableDataSource();

  private readonly onDestroy = new Subject<void>();

  constructor(
    private confirmDialogService: ConfirmDialogService,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.videoDataSource.sort = this.sort;
  }

  ngOnChanges(): void {
    const single = this.carousel.find((img: Media) => img.mainImage);
    this.mainImage = single ? [single] : [];

    this.videoDataSource.data = this.content.videos;
  }

  ngOnDestroy(): void {
    this.onDestroy.next();
  }

  addEditVideo(isNewVideo: boolean): void {
    const indexToUpdate = this.content.videos.findIndex(
      x => x === this.selectedVideo.selected[0]
    );

    const dialogData: EditVideoDialogData = {
      video: isNewVideo ? new Video() : this.selectedVideo.selected[0]
    };
    const dialogRef = this.dialog.open(EditVideoDialogComponent, {
      data: dialogData
    });

    dialogRef.afterClosed().subscribe((video: Video) => {
      if (!video) {
        return;
      }

      if (isNewVideo) {
        const newVideos = [...this.content.videos, video];
        this.content.videos = newVideos;
      } else {
        this.content.videos[indexToUpdate] = video;
      }
      this.contentChange.emit(this.content);
      this.updateContent.emit();
    });
  }

  deleteVideo(): void {
    this.confirmDialogService
      .showConfirmDelete()
      .pipe(takeUntil(this.onDestroy))
      .subscribe(confirmed => {
        if (confirmed) {
          this.content.videos = this.content.videos.filter(
            x => x !== this.selectedVideo.selected[0]
          );
          this.contentChange.emit(this.content);
          this.updateContent.emit();

          this.selectedVideo.clear();
        }
      });
  }

  update(): void {
    this.updateContent.emit();
  }
}
