export interface VideoPlatform {
  id: number;
  name: string;
  regex: RegExp;
  obsolete: boolean;
}
