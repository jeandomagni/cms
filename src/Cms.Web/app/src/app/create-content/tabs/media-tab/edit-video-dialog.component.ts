﻿import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";

import { AllVideoPlatforms } from "src/app/create-content/tabs/media-tab/video-platforms.constants";
import { VideoPlatform } from "src/app/create-content/tabs/media-tab/video-platform.interface";
import { NotificationService } from "src/app/services/notification.service";
import { Video } from "src/app/shared/models/content/entities/video.model";
import { EditVideoDialogData } from "src/app/create-content/tabs/media-tab/edit-video-dialog-data.interface";

@Component({
  selector: "edit-video-dialog",
  templateUrl: "./edit-video-dialog.component.html"
})
export class EditVideoDialogComponent {
  videoPlatforms: VideoPlatform[];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: EditVideoDialogData,
    private dialogRef: MatDialogRef<EditVideoDialogComponent>,
    private notificationService: NotificationService
  ) {
    this.videoPlatforms = AllVideoPlatforms.filter(x => x.obsolete != true);

    if(this.data.video.type != null && this.videoPlatforms.filter(x => x.id == this.data.video.type).length == 0) {
      var videos = AllVideoPlatforms.filter(x => x.id == this.data.video.type);
      if(videos.length > 0) {
        var obsoleteVideoPlatform = videos[0];
        this.videoPlatforms.push(obsoleteVideoPlatform);
      }
    }
  }

  closeDialog(): void {
    this.dialogRef.close();
  }

  update(): void {
    const platform = this.videoPlatforms.find(
      (vp: VideoPlatform) => vp.id === this.data.video.type
    );

    if (!platform.regex.test(this.data.video.url)) {
      this.notificationService.error(
        "The provided URL is invalid for this video type."
      );
    } else {
      this.data.video.position = this.data.video.position || 0;
      this.dialogRef.close(this.data.video);
    }
  }
}
