import { Video } from "src/app/shared/models/content/entities/video.model";

export interface EditVideoDialogData {
  video: Video;
}
