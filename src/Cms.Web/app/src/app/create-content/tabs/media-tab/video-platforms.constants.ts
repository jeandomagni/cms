import { VideoPlatform } from "src/app/create-content/tabs/media-tab/video-platform.interface";

export const OoyalaVideoPlatform: VideoPlatform = <VideoPlatform>{
  id: 8200,
  name: "Ooyala",
  regex: new RegExp("ooyala.com"),
  obsolete: true
};

export const YouTubeVideoPlatform: VideoPlatform = <VideoPlatform>{
  id: 8201,
  name: "YouTube",
  regex: new RegExp("youtube.com")
};

export const VimeoVideoPlatform: VideoPlatform = <VideoPlatform>{
  id: 8202,
  name: "Vimeo",
  regex: new RegExp("vimeo.com")
};

export const BizzCamVideoPlatform: VideoPlatform = <VideoPlatform>{
  id: 8203,
  name: "BizzCam",
  regex: new RegExp("bizzcam.com")
};

export const BrightcoveVideoPlatform: VideoPlatform = <VideoPlatform>{
  id: 8204,
  name: "Brightcove",
  regex: new RegExp("brightcove")
};

export const AllVideoPlatforms: VideoPlatform[] = [
  OoyalaVideoPlatform,
  YouTubeVideoPlatform,
  VimeoVideoPlatform,
  BizzCamVideoPlatform,
  BrightcoveVideoPlatform
];
