import { Component, Input } from "@angular/core";
import { Observable } from "rxjs";

import { ContentService } from "src/app/services/content.service";
import { AuditLog } from "src/app/shared/models/audit-log.interface";

@Component({
  selector: "activity-tab",
  templateUrl: "./activity-tab.component.html"
})
export class ActivityTabComponent {
  @Input() id: number;

  displayLoadMore = true;
  itemsObs: Observable<AuditLog[]>;

  constructor(private contentService: ContentService) {}

  getItems = (page: number, pageSize: number): Observable<AuditLog[]> =>
    this.contentService.getAuditLogs(this.id, page, pageSize);
}
