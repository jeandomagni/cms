import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FlexLayoutModule } from "@angular/flex-layout";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {
  MatTabsModule,
  MatIconModule,
  MatCardModule,
  MatAutocompleteModule,
  MatFormFieldModule,
  MatButtonModule,
  MatInputModule,
  MatCheckboxModule,
  MatToolbarModule,
  MatDialogModule,
  MatSelectModule,
  MatTableModule,
  MatSortModule,
  MatChipsModule,
  MatRadioModule,
  MatDatepickerModule,
  MatPaginatorModule,
  MatListModule
} from "@angular/material";
import { EditorModule } from "@tinymce/tinymce-angular";
import {
  OwlDateTimeModule,
  OWL_DATE_TIME_FORMATS,
  OwlNativeDateTimeModule
} from "ng-pick-datetime";


import { CreateContentComponent } from "src/app/create-content/create-content.component";
import { CreateContentRoutingModule } from "src/app/create-content/create-content-routing.module";
import { TinymceService } from "src/app/services/tinymce.service";
import { NavigationService } from "src/app/create-content/services/navigation.service";
import { ValidationService } from "src/app/create-content/services/validation.service";
import { ActionsComponent } from "src/app/create-content/shared/actions.component";
import { NoticesComponent } from "src/app/create-content/shared/notices.component";
import { AbListTabComponent } from "src/app/create-content/tabs/ab-list-tab/ab-list-tab.component";
import { EditAbListGroupDialogComponent } from "src/app/create-content/tabs/ab-list-tab/dialogs/edit-ab-list-group-dialog.component";
import { EditBusinessLinkDialogComponent } from "src/app/create-content/tabs/ab-list-tab/dialogs/edit-business-link-dialog.component";
import { ActivityTabComponent } from "src/app/create-content/tabs/activity-tab/activity-tab.component";
import { ContentTabComponent } from "src/app/create-content/tabs/content-tab/content-tab.component";
import { EditRecurrenceDialogComponent } from "src/app/create-content/tabs/event-tab/edit-recurrence-dialog.component";
import { EventTabComponent } from "src/app/create-content/tabs/event-tab/event-tab.component";
import { GeneralTabComponent } from "src/app/create-content/tabs/general-tab/general-tab.component";
import { EditVideoDialogComponent } from "src/app/create-content/tabs/media-tab/edit-video-dialog.component";
import { MediaTabComponent } from "src/app/create-content/tabs/media-tab/media-tab.component";
import { MetaTabComponent } from "src/app/create-content/tabs/meta-tab/meta-tab.component";
import { NearMeTabComponent } from "src/app/create-content/tabs/near-me-tab/near-me-tab.component";
import { TagsTabComponent } from "src/app/create-content/tabs/tags-tab/tags-tab.component";
import { ContentService } from "src/app/services/content.service";
import { ContentLocationService } from "src/app/services/content-location.service";
import { ContentNearMeService } from "src/app/services/content-near-me.service";
import { ContentAbListService } from "src/app/services/content-ab-list.service";
import { ContentEventService } from "src/app/services/content-event.service";
import { EventRecurrenceService } from "src/app/services/event-recurrence.service";
import { TagService } from "src/app/services/tag.service";
import { SharedModule } from "src/app/shared/shared.module";

const CMS_NATIVE_FORMATS = {
  fullPickerInput: {
    year: "numeric",
    month: "numeric",
    day: "numeric",
    hour: "numeric",
    minute: "numeric"
  },
  datePickerInput: { year: "numeric", month: "numeric", day: "numeric" },
  timePickerInput: { hour: "numeric", minute: "numeric" },
  monthYearLabel: { year: "numeric", month: "short" },
  dateA11yLabel: { year: "numeric", month: "long", day: "numeric" },
  monthYearA11yLabel: { year: "numeric", month: "long" }
};

@NgModule({
  imports: [
    CommonModule,
    EditorModule,
    FlexLayoutModule,
    FormsModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatPaginatorModule,
    MatRadioModule,
    MatSelectModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    ReactiveFormsModule,
    SharedModule,

    CreateContentRoutingModule
  ],
  declarations: [
    AbListTabComponent,
    ActionsComponent,
    ActivityTabComponent,
    ContentTabComponent,
    CreateContentComponent,
    EditAbListGroupDialogComponent,
    EditBusinessLinkDialogComponent,
    EditRecurrenceDialogComponent,
    EditVideoDialogComponent,
    EventTabComponent,
    GeneralTabComponent,
    MediaTabComponent,
    MetaTabComponent,
    NearMeTabComponent,
    NoticesComponent,
    TagsTabComponent
  ],
  entryComponents: [
    AbListTabComponent,
    ActionsComponent,
    ActivityTabComponent,
    ContentTabComponent,
    EditAbListGroupDialogComponent,
    EditBusinessLinkDialogComponent,
    EditRecurrenceDialogComponent,
    EditVideoDialogComponent,
    EventTabComponent,
    GeneralTabComponent,
    MediaTabComponent,
    MetaTabComponent,
    NearMeTabComponent,
    NoticesComponent,
    TagsTabComponent
  ],
  providers: [
    ContentAbListService,
    ContentEventService,
    ContentLocationService,
    ContentNearMeService,
    ContentService,
    EventRecurrenceService,
    NavigationService,
    TagService,
    TinymceService,
    ValidationService,
    { provide: OWL_DATE_TIME_FORMATS, useValue: CMS_NATIVE_FORMATS }
  ]
})
export class CreateContentModule {}
