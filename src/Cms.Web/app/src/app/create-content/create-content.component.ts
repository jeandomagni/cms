import { Component, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute, Params } from "@angular/router";
import { Subject, Observable, merge } from "rxjs";
import { takeUntil, map } from "rxjs/operators";

import { AuthService } from "src/app/auth/auth.service";
import { NavigationService } from "src/app/create-content/services/navigation.service";
import { ValidationService } from "src/app/create-content/services/validation.service";
import { ContentService } from "src/app/services/content.service";
import { ContentAbListService } from "src/app/services/content-ab-list.service";
import { ContentEventService } from "src/app/services/content-event.service";
import { ContentMediaService } from "src/app/services/content-media.service";
import { ContentNearMeService } from "src/app/services/content-near-me.service";
import { NotificationService } from "src/app/services/notification.service";
import { AbList } from "src/app/shared/models/content/ab-list/ab-list.interface";
import { Content } from "src/app/shared/models/content/content.model";
import { Media } from "src/app/shared/models/content/entities/media.model";
import { Event } from "src/app/shared/models/content/event/event.model";
import { NearMe } from "src/app/shared/models/content/near-me/near-me.interface";

@Component({
  selector: "create-content",
  templateUrl: "./create-content.component.html",
})
export class CreateContentComponent implements OnInit, OnDestroy {
  abList: AbList;
  activeLink = "General";
  carousel: Media[];
  content: Content;
  dbContent: Content;
  event: Event;
  isAdmin: boolean;
  links: string[];
  nearMe: NearMe;
  userEmail: string;
  private readonly onDestroy = new Subject<void>();

  constructor(
    authService: AuthService,
    private route: ActivatedRoute,
    private abListService: ContentAbListService,
    private contentService: ContentService,
    private eventService: ContentEventService,
    private mediaService: ContentMediaService,
    private navigationService: NavigationService,
    private nearMeService: ContentNearMeService,
    private notificationService: NotificationService,
    private validationService: ValidationService
  ) {
    this.isAdmin = authService.isAdmin;
    this.links = navigationService.createNavigation;
  }

  ngOnInit(): void {
    this.route.queryParams
      .pipe(takeUntil(this.onDestroy))
      .subscribe((queryParams: Params) => {
        const { contentCode } = queryParams;
        if (contentCode) {
          this.refreshContent(contentCode);
        } else {
          this.content = new Content();

          // 2 years from now
          const twoYearsLater = new Date(
            new Date().setFullYear(new Date().getFullYear() + 2)
          );
          this.content.expirationDate = twoYearsLater;
        }
      });
  }

  ngOnDestroy(): void {
    this.onDestroy.next();
  }

  refreshContent(contentCode: string): void {
    this.contentService
      .getByCode(contentCode)
      .pipe(takeUntil(this.onDestroy))
      .subscribe((data: Content) => {
        this.setModel(data);
      });
  }

  update(): void {
    const warnings = this.validationService.validateForCreate(
      this.dbContent,
      this.content,
      this.event
    );

    if (warnings.length > 0) {
      this.notificationService.warningList(warnings);
      return;
    }

    merge(this.updateTypeSpecificContent(), this.updateContent())
      .pipe(takeUntil(this.onDestroy))
      .subscribe();
  }

  private setModel(content: Content): void {
    this.dbContent = content;
    this.content = content;

    this.links = this.navigationService.getNavigationByType(this.content.type);

    this.mediaService
      .getByContentCode(this.content.code)
      .pipe(takeUntil(this.onDestroy))
      .subscribe((data: Media[]) => {
        this.carousel = data.sort((a, b) =>
          b.mainImage > a.mainImage ? 1 : 0 || a.order - b.order
        );
      });

    switch (content.type) {
      case "Event":
        this.eventService
          .getByContentCode(content.code)
          .pipe(takeUntil(this.onDestroy))
          .subscribe((data: Event) => {
            this.event = data;
          });
        break;
      case "AB List":
        this.abListService
          .getByContentCode(content.code)
          .pipe(takeUntil(this.onDestroy))
          .subscribe((data: AbList) => {
            this.abList = data;
          });
        break;
      case "Near Me":
        this.nearMeService
          .getByContentCode(content.code)
          .pipe(takeUntil(this.onDestroy))
          .subscribe((data: NearMe) => {
            this.nearMe = data;
          });
        break;
      default:
        break;
    }
  }

  private updateContent = (): Observable<void> =>
    this.contentService.update(this.content).pipe(
      map((data: Content) => {
        this.setModel(data);
        this.notificationService.success("Saved article");
      })
    );

  private updateTypeSpecificContent(): Observable<void> {
    switch (this.content.type) {
      case "Event":
        if (this.event) {
          return this.eventService.update(this.event).pipe(
            map((data: Event) => {
              this.event = data;
            })
          );
        }

        return this.eventService
          .create({ ...new Event(), contentCode: this.content.code })
          .pipe(
            map((data: Event) => {
              this.event = data;
            })
          );

      case "Near Me":
        if (this.nearMe) {
          return this.nearMeService.update(this.nearMe).pipe(
            map((data: NearMe) => {
              this.nearMe = data;
            })
          );
        }
        return this.nearMeService
          .create({ contentCode: this.content.code })
          .pipe(
            map((data: NearMe) => {
              this.nearMe = data;
            })
          );

      case "AB List":
        if (this.abList) {
          return this.abListService.update(this.abList).pipe(
            map((data: AbList) => {
              this.abList = data;
            })
          );
        }
        return this.abListService
          .create({ contentCode: this.content.code })
          .pipe(
            map((data: AbList) => {
              this.abList = data;
            })
          );

      default:
        return new Observable();
    }
  }
}
