import { Injectable } from '@angular/core';

@Injectable()
export class NavigationService {
  createNavigation: string[] = ['General'];

  getNavigationByType(type: string): string[] {
    switch (type) {
      case 'Event':
        return this.getEventNavigation();
      case 'Article':
      case 'AB List':
      case 'Near Me':
      default:
        return this.getDefaultNavigation();
    }
  }

  private getDefaultNavigation = (): string[] => [
    'General',
    'Content',
    'Media',
    'Tags',
    'Meta',
    'Activity',
  ];

  private getEventNavigation = (): string[] => [
    'General',
    'Event',
    'Content',
    'Media',
    'Tags',
    'Meta',
    'Activity',
  ];
}
