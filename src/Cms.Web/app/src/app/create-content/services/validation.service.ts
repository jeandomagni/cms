import { Injectable } from "@angular/core";

import { ContentImageCarouselLayout } from "src/app/create-content/constants/content-layouts.constants";
import {
  AllContentTypes,
  EventContentType,
  AbListContentType,
  NearMeContentType,
  BaselinePageContentType
} from "src/app/create-content/constants/content-types.constants";
import { Content } from "src/app/shared/models/content/content.model";
import { AbList } from "src/app/shared/models/content/ab-list/ab-list.interface";
import { Event } from "src/app/shared/models/content/event/event.model";
import { NearMe } from "src/app/shared/models/content/near-me/near-me.interface";
import { Media } from "src/app/shared/models/content/entities/media.model";

@Injectable()
export class ValidationService {
  validateForCreate(content: Content, dbContent: Content = null, event: Event = null): string[] {
    const errors = [];

    if (dbContent != null && (content.type == BaselinePageContentType.name && content.type != dbContent.type)) {
      errors.push("BaselinePage type could not be changed from or to. BaselinePage type should be managed from admin page");
    }


    if (!content.title) {
      errors.push("An article headline is required");
    }

    if (!content.layout) {
      errors.push("An article layout is required");
    }

    if (!content.type) {
      errors.push("An article type is required");
    }

    switch (content.type) {
      case EventContentType.name:
        // If not a virtual event, should include full location info
        if (
          event &&
          !event.isVirtualEvent &&
          (!event.addressLine ||
            !event.city ||
            !event.state ||
            !event.country ||
            !event.postalCode)
        ) {
          errors.push("Event location info is required for non-virtual events");
        }
        break;
      case BaselinePageContentType.name:
        if(content.articleUrl.segment.indexOf("/") > -1)
        {
            errors.push("No nested directories allowed in URL field for Baseline Pages");
        }
        break;
      default:
        break;
    }

    return errors;
  }

  validateForPublish(
    content: Content,
    carousel: Media[],
    event: Event,
    nearMe: NearMe,
    abList: AbList
  ): string[] {
    const errors = [];

    if (!content.title) {
      errors.push("An article headline is required");
    }

    if (!content.layout) {
      errors.push("An article layout is required");
    }

    if (!content.type) {
      errors.push("An article type is required");
    }

    if (!content.pageTitle) {
      errors.push("An article page title is required");
    }

    if (!content.mobileHeadline) {
      errors.push("An article mobile headline is required");
    }

    if (
      content.layout === ContentImageCarouselLayout.id &&
      (carousel || []).length === 0
    ) {
      errors.push("At least one carousel image is required");
    }

    if (content.hasRedirectUrl && !content.redirectUrl) {
      errors.push("A redirect URL is required.");
    }

    if (content.hasSeoCanonical && !content.seoCanonical) {
      errors.push("An SEO canonical is required.");
    }

    if (
      content.type !== BaselinePageContentType.name &&
      content.type !== EventContentType.name &&
      content.type !== NearMeContentType.name &&
      (content.cities || []).length === 0 &&
      (content.states || []).length === 0 &&
      (content.countries || []).length === 0
    ) {
      errors.push("At least one geo-location tag is required");
    }

    if (!AllContentTypes.find(type => content.type === type.name)) {
      errors.push("Invalid content type selected");
    }

    switch (content.type) {
      case AbListContentType.name:
        if ((abList.businessLinkLists || []).length === 0) {
          errors.push("At least one location with links is required");
        } else if (
          (abList.businessLinkLists[0].businessLinks || []).length === 0
        ) {
          errors.push("At least one link is required for every location");
        }

        break;

      case NearMeContentType.name:
        if (!nearMe.aboveListContent) {
          errors.push("Above list content is required");
        }

        if (!nearMe.relatedInformationHeadline) {
          errors.push("Related information headline is required");
        }

        if (!nearMe.relatedInformationText) {
          errors.push("Related information text is required");
        }

        break;

      case EventContentType.name:
        if ((content.authors || []).length === 0) {
          errors.push("An article author is required");
        }

        if (!content.contentHtml) {
          errors.push("Article main text is required");
        }

        if (!content.summary) {
          errors.push("Article summary is required");
        }

        if (!event.startDate) {
          errors.push("An event start date is required");
        }

        if (!event.endDate) {
          errors.push("An event end date is required");
        }

        // If not a virtual event, should include full location info
        if (
          !event.isVirtualEvent &&
          (!event.addressLine ||
            !event.city ||
            !event.state ||
            !event.country ||
            !event.postalCode)
        ) {
          errors.push("Event location info is required for non-virtual events");
        }

        break;
      
      case BaselinePageContentType.name:
          if(content.articleUrl.segment.indexOf("/") > -1)
          {
              errors.push("No nested directories allowed in URL field for Baseline Pages");
          }

      default:
        if ((content.authors || []).length === 0) {
          errors.push("An article author is required");
        }

        if (!content.contentHtml) {
          errors.push("Article main text is required");
        }

        if (!content.summary) {
          errors.push("Article summary is required");
        }

        break;
    }

    return errors;
  }
}
