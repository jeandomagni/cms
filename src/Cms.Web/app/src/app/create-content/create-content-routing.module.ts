import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { CreateContentComponent } from "src/app/create-content/create-content.component";

const routes: Routes = [
  {
    path: "",
    component: CreateContentComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreateContentRoutingModule {}
