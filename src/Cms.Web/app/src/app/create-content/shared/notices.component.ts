import { Component, Input } from "@angular/core";

import { Content } from "src/app/shared/models/content/content.model";

@Component({
  selector: "notices",
  templateUrl: "./notices.component.html"
})
export class NoticesComponent {
  @Input() content: Content;
}
