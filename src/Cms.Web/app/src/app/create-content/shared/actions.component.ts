import {
  Component,
  OnDestroy,
  Input,
  Output,
  EventEmitter,
  OnChanges,
} from "@angular/core";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

import { AuthService } from "src/app/auth/auth.service";
import {
  ApproveAction,
  CheckOutAction,
  CheckInAction,
  CreateAction,
  DeleteAction,
  PreviewAction,
  PublishAction,
  SaveAction,
  UnpublishAction,
} from "src/app/create-content/constants/actions.constants";
import { ValidationService } from "src/app/create-content/services/validation.service";
import { ConfirmDialogService } from "src/app/services/confirm-dialog.service";
import { ContentService } from "src/app/services/content.service";
import { NotificationService } from "src/app/services/notification.service";
import { Content } from "src/app/shared/models/content/content.model";
import { AbList } from "src/app/shared/models/content/ab-list/ab-list.interface";
import { ContentAction } from "src/app/shared/models/content/content-action.interface";
import { NearMe } from "src/app/shared/models/content/near-me/near-me.interface";
import { Event } from "src/app/shared/models/content/event/event.model";
import { Media } from "src/app/shared/models/content/entities/media.model";

@Component({
  selector: "actions",
  templateUrl: "./actions.component.html",
})
export class ActionsComponent implements OnChanges, OnDestroy {
  @Input() abList: AbList;
  @Input() carousel: Media[];
  @Input() content: Content;
  @Input() event: Event;
  @Input() nearMe: NearMe;
  @Output() refresh: EventEmitter<string> = new EventEmitter();
  @Output() update: EventEmitter<void> = new EventEmitter();

  actions: ContentAction[] = [CreateAction];
  isAdmin: boolean;
  userEmail: string;
  private readonly onDestroy = new Subject<void>();

  constructor(
    authService: AuthService,
    private confirmDialogService: ConfirmDialogService,
    private contentService: ContentService,
    private notificationService: NotificationService,
    private validationService: ValidationService
  ) {
    this.isAdmin = authService.isAdmin;
    this.userEmail = authService.currentUser.email;
  }

  ngOnChanges(): void {
    if (this.content && this.content.code) {
      this.actions = [
        !this.content.isCheckedOut &&
          this.content.canCheckOut &&
          CheckOutAction,
        this.content.checkoutDate &&
          (this.isAdmin || this.userEmail === this.content.checkoutBy) &&
          CheckInAction,
        this.content.canEdit && SaveAction,
        this.content.status === "pending_review" &&
          this.content.canEdit &&
          this.isAdmin &&
          ApproveAction,
        this.content.status === "pending_review" &&
          this.content.canEdit &&
          this.isAdmin &&
          UnpublishAction,
        this.content.status === "draft" &&
          this.content.canEdit &&
          PublishAction,
        this.content.status === "published" &&
          this.content.canEdit &&
          UnpublishAction,
        PreviewAction,
        (this.content.canEdit || this.isAdmin) && DeleteAction,
      ].filter(Boolean);
    } else {
      this.actions = [CreateAction];
    }
  }

  ngOnDestroy(): void {
    this.onDestroy.next();
  }

  clickAction(actionLabel: string): void {
    switch (actionLabel) {
      case ApproveAction.label:
        this.confirmPublish();
        break;
      case CheckInAction.label:
        this.checkIn();
        break;
      case CheckOutAction.label:
        this.checkOut();
        break;
      case CreateAction.label:
        this.create();
        break;
      case DeleteAction.label:
        this.confirmDelete();
        break;
      case PreviewAction.label:
        this.preview();
        break;
      case PublishAction.label:
        this.confirmPublish();
        break;
      case SaveAction.label:
        this.update.emit();
        break;
      case UnpublishAction.label:
        this.confirmUnpublish();
        break;
      default:
        break;
    }
  }

  private checkIn(): void {
    this.contentService
      .checkIn(this.content.code)
      .pipe(takeUntil(this.onDestroy))
      .subscribe(() => {
        this.notificationService.success("Checked in article");
        this.refresh.emit(this.content.code);
      });
  }

  private checkOut(): void {
    this.contentService
      .checkOut(this.content.code)
      .pipe(takeUntil(this.onDestroy))
      .subscribe(() => {
        this.notificationService.success("Checked out article");
        this.refresh.emit(this.content.code);
      });
  }

  private confirmDelete(): void {
    this.confirmDialogService
      .showConfirmDelete()
      .pipe(takeUntil(this.onDestroy))
      .subscribe((confirmed) => {
        if (confirmed) {
          this.contentService
            .delete(this.content.id)
            .pipe(takeUntil(this.onDestroy))
            .subscribe(() => {
              window.location.href = "#/";
              this.notificationService.success("Deleted article");
            });
        }
      });
  }

  private confirmPublish(): void {
    const disclaimer =
      "Are you sure you want to publish this content?<br><br>" +
      "By clicking Publish and submitting written content to CoreCMS, " +
      "you hereby agree to license such written content to other BBBs " +
      "for use on their respective bbb.org web pages. You also acknowledge " +
      "that the written content you submitted has been reviewed and " +
      "pre-approved by a BBB CEO or designee of a BBB CEO prior to submission. " +
      "You are responsible for all written content you submit to CoreCMS. " +
      "CBBB and BBBs that republish written content on their respective bbb.org " +
      "web pages are responsible for such republished content. " +
      "If you don't agree to these terms, please click Cancel.";

    this.confirmDialogService
      .showConfirmCustom(disclaimer, "Publish")
      .pipe(takeUntil(this.onDestroy))
      .subscribe((confirmed) => {
        if (confirmed) {
          const errors = this.validationService.validateForPublish(
            this.content,
            this.carousel,
            this.event,
            this.nearMe,
            this.abList
          );
          if (errors.length > 0) {
            this.notificationService.warningList(errors);
            return;
          }

          this.contentService
            .publish(this.content)
            .pipe(takeUntil(this.onDestroy))
            .subscribe((data: Content) => {
              if (data.status === "pending_review") {
                this.notificationService.success(
                  "Your article has been submitted for approval"
                );
              } else {
                this.notificationService.success(
                  "Your article has been published"
                );
              }
              this.refresh.emit(this.content.code);
            });
        }
      });
  }

  private confirmUnpublish(): void {
    this.confirmDialogService
      .showConfirmUnpublish()
      .pipe(takeUntil(this.onDestroy))
      .subscribe((confirmed) => {
        if (confirmed) {
          this.contentService
            .unpublish(this.content.code)
            .pipe(takeUntil(this.onDestroy))
            .subscribe(() => {
              this.notificationService.success(
                "Your article has been unpublished"
              );
              this.refresh.emit(this.content.code);
            });
        }
      });
  }

  private create(): void {
    const errors = this.validationService.validateForCreate(this.content);
    if (errors.length > 0) {
      this.notificationService.warningList(errors);
      return;
    }

    this.contentService
      .create(this.content)
      .pipe(takeUntil(this.onDestroy))
      .subscribe((contentCode: string) => {
        this.notificationService.success("Created article");
        window.location.href = `#/content/create?contentCode=${contentCode}`;
      });
  }

  private preview(): void {
    const contentCode = new URL(
      window.location.href.replace("/#", "")
    ).searchParams.get("contentCode");
    const url = `${window["terminusBaseUrl"]}/content/${contentCode}`; //eslint-disable-line
    window.open(url, "_blank");
  }
}
