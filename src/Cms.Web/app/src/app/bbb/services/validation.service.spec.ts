import { TestBed } from "@angular/core/testing";

import { Bbb } from "src/app/shared/models/bbb/bbb.model";

import { ValidationService } from "./validation.service";

describe("ValidationService", () => {
  let perfectBbb = new Bbb();
  let service: ValidationService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ValidationService]
    });
    service = TestBed.get(ValidationService);

    perfectBbb.popularCategories = new Array(10).map(i => ({
      name: `category-${i}`,
      tobId: `category-${i}`
    }));
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should not return errors for perfect BBB", () => {
    expect(service.validateForSave(perfectBbb).length > 0);
  });

  it("should return errors if there are not 10 categories", () => {
    var bbb = {
      ...perfectBbb,
      popularCategories: new Array(11).map(i => ({
        name: `category-${i}`,
        tobId: `category-${i}`
      }))
    };

    expect(service.validateForSave(bbb).length > 0);
  });
});
