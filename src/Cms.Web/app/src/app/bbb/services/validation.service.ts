import { Injectable } from "@angular/core";

import { Bbb } from "src/app/shared/models/bbb/bbb.model";

@Injectable()
export class ValidationService {
  validateForSave(bbb: Bbb): string[] {
    let errors = [];

    if (bbb.popularCategories.length !== 10) {
      errors.push("10 popular categories are required.");
    }

    if(bbb.applyForAccreditationInfo != null ){
      if (bbb.applyForAccreditationInfo.terms == '') {
        errors.push("Application Term are required.");
      }
      
      if (bbb.applyForAccreditationInfo.applicationSubmission == '2' && bbb.applyForAccreditationInfo.paymentProcessorUrl == '') {
        errors.push("Payment processor URL are required.");
      }
    }

    return errors;
  }
}
