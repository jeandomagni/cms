import { ComplaintQuestion } from "src/app/shared/models/bbb/entities/complaint-question.interface";

export interface EditQuestionDialogData {
  question: ComplaintQuestion;
}
