import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnInit,
  OnChanges
} from "@angular/core";
import { MatDialog } from "@angular/material";
import { Observable } from "rxjs";

import { ComplaintQuestion } from "src/app/shared/models/bbb/entities/complaint-question.interface";
import { ConfirmDialogService } from "src/app/services/confirm-dialog.service";
import { BaseDragDropTreeComponent } from "src/app/components/base/base-drag-drop-tree/base-drag-drop-tree.component";
import { TreeNode } from "src/app/components/base/base-drag-drop-tree/tree-node.interface";

import { EditQuestionDialogComponent } from "./dialogs/edit-question-dialog.component";

@Component({
  selector: "complaint-question-drag-drop-tree",
  templateUrl: "./complaint-question-drag-drop-tree.html",
  styleUrls: ["./complaint-question-drag-drop-tree.styles.scss"]
})
export class ComplaintQuestionDragDropTreeComponent
  extends BaseDragDropTreeComponent<ComplaintQuestion>
  implements OnInit, OnChanges {
  @Input() questions: ComplaintQuestion[];
  @Output() questionsChange: EventEmitter<
    ComplaintQuestion[]
  > = new EventEmitter();

  allowItemEdit = true;
  generateLabel = (question: ComplaintQuestion): string => question.question;

  constructor(
    confirmDialogService: ConfirmDialogService,
    private dialog: MatDialog
  ) {
    super(confirmDialogService);
  }

  ngOnInit() {
    this.refreshItems();

    this.update = (items: ComplaintQuestion[]) => {
      this.questionsChange.emit(items);
      this.refreshItems();
    };
  }

  ngOnChanges() {
    this.refreshItems();
  }

  addEditItem = (
    question: ComplaintQuestion
  ): Observable<ComplaintQuestion> => {
    const dialogRef = this.dialog.open(EditQuestionDialogComponent, {
      data: {
        question
      }
    });

    return dialogRef.afterClosed();
  };

  getIsEditable = (node: TreeNode) =>
    this.questions.find(x => this.generateLabel(x) === node.label).editable;

  refreshItems() {
    this.items = this.questions;
    if (this.dataSource) {
      this.dataSource.data = this.questions || [];
    }
  }
}
