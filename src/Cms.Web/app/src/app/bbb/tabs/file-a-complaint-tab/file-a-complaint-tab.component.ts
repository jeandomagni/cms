import { Component, Input, EventEmitter, Output } from "@angular/core";

import { Bbb } from "src/app/shared/models/bbb/bbb.model";
import { MatDialog } from "@angular/material";
import { EditQuestionDialogComponent } from "./dialogs/edit-question-dialog.component";
import { ComplaintQuestion } from "src/app/shared/models/bbb/entities/complaint-question.interface";
import { takeUntil } from "rxjs/operators";

@Component({
  selector: "bbb-file-a-complaint-tab",
  templateUrl: "./file-a-complaint-tab.component.html"
})
export class FileAComplaintTabComponent {
  @Input() bbb: Bbb;
  @Output() bbbChange: EventEmitter<Bbb> = new EventEmitter();
  @Output() update: EventEmitter<void> = new EventEmitter();

  readonly MAX_QUESTIONS = 10; // TODO: move to config

  private onDestroy: EventEmitter<void> = new EventEmitter();

  constructor(private dialog: MatDialog) {}

  addQuestion(): void {
    const dialogRef = this.dialog.open(EditQuestionDialogComponent, {
      data: {
        question: <ComplaintQuestion>{
          editable: true
        }
      }
    });

    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.onDestroy))
      .subscribe((question: ComplaintQuestion) => {
        if (question) {
          this.bbb.complaintQualifyingQuestions.push(question);
          this.bbbChange.emit(this.bbb);
          this.update.emit();
        }
      });
  }
}
