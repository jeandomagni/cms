import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";
import { Settings } from "tinymce";

import { AuthService } from "src/app/auth/auth.service";
import { NotificationService } from "src/app/services/notification.service";
import { TinymceService } from "src/app/services/tinymce.service";

import { EditQuestionDialogData } from "./edit-question-dialog-data.interface";

@Component({
  selector: "bbb-edit-question-dialog",
  templateUrl: "./edit-question-dialog.component.html",
  styleUrls: ["./edit-question-dialog.styles.scss"]
})
export class EditQuestionDialogComponent {
  tinymceInit: Settings;
  tinymceKey: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: EditQuestionDialogData,
    authService: AuthService,
    private dialogRef: MatDialogRef<EditQuestionDialogComponent>,
    private notificationService: NotificationService,
    tinyMceService: TinymceService
  ) {
    const baseSettings = authService.isAdmin
      ? tinyMceService.adminSettings
      : tinyMceService.globalSettings;

    this.tinymceInit = {
      ...baseSettings,
      height: 250
    };
    this.tinymceKey = tinyMceService.key;
  }

  update(): void {
    if (!this.data.question.question) {
      this.notificationService.error(
        "You must enter a question before saving."
      );
      return;
    }

    this.dialogRef.close(this.data.question);
  }
}
