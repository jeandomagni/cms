import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnInit
} from "@angular/core";

import { Bbb } from "src/app/shared/models/bbb/bbb.model";
import { Media } from "src/app/shared/models/content/entities/media.model";

@Component({
  selector: "bbb-media-tab",
  templateUrl: "./media-tab.component.html"
})
export class MediaTabComponent implements OnInit {
  @Input() bbb: Bbb;
  @Input() canEdit: boolean;
  @Output() bbbChange: EventEmitter<Bbb> = new EventEmitter();
  @Output() update: EventEmitter<void> = new EventEmitter();

  image = [];
  private defaultImageUrl =
    "https://www.bbb.org/TerminusContent/_shared/images/logo_square.png";

  ngOnInit () {
    if(this.bbb.informationPageImageUrl != null) {
      this.image.push(<Media>{ url: this.bbb.informationPageImageUrl }) ;
    }
    else {
      this.image.splice(0, 1);
    }
  }

  imageChange(media: Media): void {
    this.bbb.informationPageImageUrl = media ? media.url : null;
    this.bbbChange.emit(this.bbb);
    this.update.emit();
  }

  imageDelete(id: number): void {
    this.imageChange(null);
  }
}
