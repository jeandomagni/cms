import { Component, Input, Output, EventEmitter } from "@angular/core";

import { AuthService } from "src/app/auth/auth.service";
import { EventLayouts } from "src/app/bbb/constants/event-layouts.constants";
import { Vendors } from "src/app/bbb/constants/vendors.constants";
import { Countries } from "src/app/constants/countries.constants";
import { Languages } from "src/app/constants/languages.constants";
import { Bbb } from "src/app/shared/models/bbb/bbb.model";

@Component({
  selector: "bbb-general-tab",
  templateUrl: "./general-tab.component.html"
})
export class GeneralTabComponent {
  @Input() bbb: Bbb;
  @Input() canEdit: boolean;
  @Output() update: EventEmitter<void> = new EventEmitter();

  countries = Countries;
  eventLayouts = EventLayouts;
  languages = Languages;
  isSystemAdmin: boolean;
  vendors: string[] = Vendors;

  constructor(authService: AuthService) {
    this.isSystemAdmin = authService.isGlobalAdmin;
  }
}
