import {
  Component,
  OnInit,
  Input,
  EventEmitter,
  Output,
  ViewChild,
  ElementRef
} from "@angular/core";
import { MatAutocompleteSelectedEvent } from "@angular/material";

import { NotificationService } from "src/app/services/notification.service";
import { SolrService } from "src/app/services/solr.service";
import { BaseAutocompleteComponent } from "src/app/components/base/base-autocomplete/base-autocomplete.component";
import { BbbCategory } from "src/app/shared/models/bbb/entities/category.interface";
import { Category } from "src/app/shared/models/content/entities/category.interface";

@Component({
  selector: "bbb-category-autocomplete",
  templateUrl: "./category-autocomplete.component.html"
})
export class CategoryAutocompleteComponent
  extends BaseAutocompleteComponent<Category>
  implements OnInit {
  @Input() primaryCountry: string;
  @Input() selected: BbbCategory[];
  @Output() selectedChange: EventEmitter<BbbCategory[]> = new EventEmitter();
  @ViewChild("categoryInput", { static: false }) categoryInput: ElementRef;

  constructor(
    private notificationService: NotificationService,
    private solrService: SolrService
  ) {
    super();
  }

  ngOnInit() {
    this.selected = this.selected || [];
    this.notFoundMessage = "No categories found";
  }

  select(event: MatAutocompleteSelectedEvent): void {
    this.categoryInput.nativeElement.value = "";
    this.acCtrl.setValue(null);

    if (this.selected.length === 10) {
      this.notificationService.error("You can only add up to 10 categories.");
      return;
    }

    const category: Category = event.option.value;
    this.selectedChange.emit([
      ...this.selected,
      {
        name: category.title,
        tobId: category.entityId
      }
    ]);
  }

  protected queryItems(searchValue: string) {
    const countriesToQuery = [this.primaryCountry];

    return this.solrService.suggestPrimaryCategories(
      searchValue,
      countriesToQuery,
      this.selected.map(x => x.tobId)
    );
  }
}
