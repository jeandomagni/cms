import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnInit,
  OnChanges
} from "@angular/core";

import { BbbCategory } from "src/app/shared/models/bbb/entities/category.interface";
import { ConfirmDialogService } from "src/app/services/confirm-dialog.service";
import { BaseDragDropTreeComponent } from "src/app/components/base/base-drag-drop-tree/base-drag-drop-tree.component";

@Component({
  selector: "category-drag-drop-tree",
  templateUrl: "./category-drag-drop-tree.html"
})
export class CategoryDragDropTreeComponent
  extends BaseDragDropTreeComponent<BbbCategory>
  implements OnInit, OnChanges {
  @Input() categories: BbbCategory[];
  @Output() categoriesChange: EventEmitter<BbbCategory[]> = new EventEmitter();

  generateLabel = (category: BbbCategory): string => category.name;

  constructor(confirmDialogService: ConfirmDialogService) {
    super(confirmDialogService);
  }

  ngOnInit() {
    this.refreshItems();
    this.update = (items: BbbCategory[]) => {
      this.categoriesChange.emit(items);
    };
  }

  ngOnChanges() {
    this.refreshItems();
  }

  refreshItems() {
    this.items = this.categories;
    if (this.dataSource) {
      this.dataSource.data = this.categories || [];
    }
  }
}
