import { Component, Input } from "@angular/core";
import * as fileSaver from 'file-saver';
import { BbbInfoService } from "src/app/services/bbb-info.service";
import formatDate from "src/app/shared/utils/formatDate";

@Component({
  selector: "bbb-leads-reports-tab",
  templateUrl: "./leads-reports-tab.component.html"
})
export class LeadsReportsTabComponent {
  @Input() id: number;

  listedRangeStart = null;
  listedRangeEnd = null;
  accredRangeStart = null;
  accredRangeEnd = null;

  constructor(private bbbInfoService: BbbInfoService) {  }

  getListedLeadsReport = () =>
     this.bbbInfoService.getListedLeads(this.id, this.listedRangeStart, this.listedRangeEnd).subscribe((data) => {      
      const blob = new Blob([data], {type: "text/csv"});    
      var filename =`LLR_bbbId=${this.id}_from=${formatDate(this.listedRangeStart)}`;
      if(this.listedRangeEnd)
        filename+= `_to=${formatDate(this.listedRangeEnd)}`;
      fileSaver.saveAs(blob, `${filename}.csv`);
    });

    getAccreditationLeadsReport = () =>
        this.bbbInfoService.getAccreditationLeads(this.id, this.accredRangeStart, this.accredRangeEnd).subscribe((data) => {
            const blob = new Blob([data], { type: "text/csv" });
            var filename = `ALR_bbbId=${this.id}_from=${formatDate(this.accredRangeStart)}`;
            if (this.accredRangeEnd)
                filename += `_to=${formatDate(this.accredRangeEnd)}`;
            fileSaver.saveAs(blob, `${filename}.csv`);
        });
}
