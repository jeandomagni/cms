import {
  Component,
  Input,
  ViewChild,
  OnChanges,
  Output,
  EventEmitter
} from "@angular/core";
import { MatSort, MatTableDataSource, MatDialog } from "@angular/material";
import { SelectionModel } from "@angular/cdk/collections";
import { takeUntil } from "rxjs/operators";

import { EditLinkDialogComponent } from "src/app/bbb/tabs/header-footer-tab/dialogs/edit-link-dialog.component";
import { ConfirmDialogService } from "src/app/services/confirm-dialog.service";
import { Bbb } from "src/app/shared/models/bbb/bbb.model";
import { Link } from "src/app/shared/models/link.interface";
import { Observable, of } from "rxjs";

@Component({
  selector: "bbb-header-footer-tab",
  templateUrl: "./header-footer-tab.component.html"
})
export class HeaderFooterTabComponent implements OnChanges {
  @Input() bbb: Bbb;
  @Output() bbbChange: EventEmitter<Bbb> = new EventEmitter();
  @Output() update: EventEmitter<void> = new EventEmitter();

  @ViewChild(MatSort, { static: true }) footerLinkSort: MatSort;
  @ViewChild(MatSort, { static: true }) headerLinkSort: MatSort;

  displayedFooterLinkColumns: string[] = [
    "select",
    "linkText",
    "linkUrl",
    "priority",
    "disabled"
  ];
  displayedHeaderLinkColumns: string[] = [
    "select",
    "linkText",
    "linkUrl",
    "priority",
    "disabled"
  ];
  footerLinksDataSource: MatTableDataSource<Link> = new MatTableDataSource();
  headerLinksDataSource: MatTableDataSource<Link> = new MatTableDataSource();
  selectedFooterLink: SelectionModel<Link> = new SelectionModel<Link>(
    false,
    []
  );
  selectedHeaderLink: SelectionModel<Link> = new SelectionModel<Link>(
    false,
    []
  );
  private onDestroy: EventEmitter<void> = new EventEmitter();

  constructor(
    private confirmDialogService: ConfirmDialogService,
    private dialog: MatDialog
  ) {
    this.footerLinksDataSource.sort = this.footerLinkSort;
    this.headerLinksDataSource.sort = this.headerLinkSort;
  }

  ngOnChanges(): void {
    this.footerLinksDataSource.data = this.bbb.footerLinks;
    this.headerLinksDataSource.data = this.bbb.headerLinks;
  }

  ngOnDestroy(): void {
    this.onDestroy.next();
  }

  addChildToMenuLink(linkToAssignChild: Link): void {
    const dialogData = { link: {}, hidePriorityField: true };
    const dialogRef = this.dialog.open(EditLinkDialogComponent, {
      data: dialogData
    });

    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.onDestroy))
      .subscribe((link: Link) => {
        if (link) {
          const indexToGiveChild = this.bbb.menuLinks.findIndex(
            x => x.linkUrl === linkToAssignChild.linkUrl
          );
          this.bbb.menuLinks[indexToGiveChild].children =
            this.bbb.menuLinks[indexToGiveChild].children || [];
          this.bbb.menuLinks[indexToGiveChild].children.push(link);
          this.bbbChange.emit(this.bbb);
          this.update.emit();
        }
      });
  }

  addFooterLink(): void {
    const dialogData = { link: {} };
    const dialogRef = this.dialog.open(EditLinkDialogComponent, {
      data: dialogData
    });

    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.onDestroy))
      .subscribe((link: Link) => {
        if (link) {
          this.bbb.footerLinks.push(link);
          this.bbbChange.emit(this.bbb);
          this.update.emit();
        }
      });
  }

  addHeaderLink(): void {
    const dialogData = { link: {} };
    const dialogRef = this.dialog.open(EditLinkDialogComponent, {
      data: dialogData
    });

    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.onDestroy))
      .subscribe((link: Link) => {
        if (link) {
          this.bbb.headerLinks.push(link);
          this.bbbChange.emit(this.bbb);
          this.update.emit();
        }
      });
  }

  deleteFooterLink(): void {
    this.confirmDialogService
      .showConfirmDelete()
      .pipe(takeUntil(this.onDestroy))
      .subscribe(confirmed => {
        if (confirmed) {
          const link = this.selectedFooterLink.selected[0];

          this.bbb.footerLinks = this.bbb.footerLinks.filter(
            (x: Link) => x.linkUrl !== link.linkUrl
          );
          this.bbbChange.emit(this.bbb);
          this.update.emit();

          this.selectedFooterLink.clear();
        }
      });
  }

  deleteHeaderLink(): void {
    this.confirmDialogService
      .showConfirmDelete()
      .pipe(takeUntil(this.onDestroy))
      .subscribe(confirmed => {
        if (confirmed) {
          const link = this.selectedHeaderLink.selected[0];

          this.bbb.headerLinks = this.bbb.headerLinks.filter(
            (x: Link) => x.linkUrl !== link.linkUrl
          );
          this.bbbChange.emit(this.bbb);
          this.update.emit();

          this.selectedHeaderLink.clear();
        }
      });
  }

  editFooterLink(): void {
    const link = this.selectedFooterLink.selected[0];
    const dialogData = { link };
    const dialogRef = this.dialog.open(EditLinkDialogComponent, {
      data: dialogData
    });

    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.onDestroy))
      .subscribe((updatedLink: Link) => {
        if (updatedLink) {
          this.bbb.footerLinks = this.bbb.footerLinks.map((x: Link) =>
            x.linkUrl === link.linkUrl ? updatedLink : x
          );
          this.bbbChange.emit(this.bbb);
          this.update.emit();

          this.selectedFooterLink.clear();
        }
      });
  }

  editHeaderLink(): void {
    const link = this.selectedHeaderLink.selected[0];
    const dialogData = { link };
    const dialogRef = this.dialog.open(EditLinkDialogComponent, {
      data: dialogData
    });

    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.onDestroy))
      .subscribe((updatedLink: Link) => {
        if (updatedLink) {
          this.bbb.headerLinks = this.bbb.headerLinks.map((x: Link) =>
            x.linkUrl === link.linkUrl ? updatedLink : x
          );
          this.bbbChange.emit(this.bbb);
          this.update.emit();

          this.selectedHeaderLink.clear();
        }
      });
  }

  generateMenuLinkLabel = (link: Link): string =>
    `${link.linkText} (${link.linkUrl})`;

  getMenuLinkChildren = (link: Link): Observable<Link[]> => of(link.children);

  addEditMenuLink = (link: Link): Observable<Link> => {
    const dialogRef = this.dialog.open(EditLinkDialogComponent, {
      data: {
        link,
        hidePriorityField: true
      }
    });

    return dialogRef.afterClosed();
  };

  addMenuLink(): void {
    this.addEditMenuLink({ linkText: null, linkUrl: null })
      .pipe(takeUntil(this.onDestroy))
      .subscribe((link: Link) => {
        if (link) {
          this.bbb.menuLinks.push(link);
          this.bbbChange.emit(this.bbb);
          this.update.emit();
        }
      });
  }
}
