import { Link } from "src/app/shared/models/link.interface";

export interface EditLinkDialogData {
  hidePriorityField: boolean;
  link: Link;
}
