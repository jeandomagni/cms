import {
  Component,
  Input,
  OnChanges,
  Output,
  EventEmitter,
  OnInit
} from "@angular/core";
import { MatDialog } from "@angular/material";

import { EditLinkDialogComponent } from "src/app/bbb/tabs/header-footer-tab/dialogs/edit-link-dialog.component";
import { ConfirmDialogService } from "src/app/services/confirm-dialog.service";
import { Link } from "src/app/shared/models/link.interface";
import { Observable } from "rxjs";
import { BaseDragDropTreeComponent } from "src/app/components/base/base-drag-drop-tree/base-drag-drop-tree.component";

@Component({
  selector: "menu-links-drag-drop-tree",
  templateUrl: "./menu-links-drag-drop-tree.html"
})
export class MenuLinksDragDropTreeComponent
  extends BaseDragDropTreeComponent<Link>
  implements OnInit, OnChanges {
  @Input() menuLinks: Link[];
  @Output() menuLinksChange: EventEmitter<Link[]> = new EventEmitter();
  @Output() updateBbb: EventEmitter<void> = new EventEmitter();

  allowChildren = true;
  allowItemEdit = true;
  generateLabel = (link: Link): string => `${link.linkText} (${link.linkUrl})`;

  constructor(
    confirmDialogService: ConfirmDialogService,
    private dialog: MatDialog
  ) {
    super(confirmDialogService);
  }

  ngOnInit() {
    this.refreshItems();
    this.update = (items: Link[]) => {
      this.menuLinksChange.emit(items);
      this.updateBbb.emit();
    };
  }

  ngOnChanges() {
    this.refreshItems();
  }

  addEditItem = (link: Link): Observable<Link> => {
    const dialogRef = this.dialog.open(EditLinkDialogComponent, {
      data: {
        link,
        hidePriorityField: true
      }
    });

    return dialogRef.afterClosed();
  };

  refreshItems() {
    this.items = this.menuLinks;
    if (this.dataSource) {
      this.dataSource.data = this.menuLinks || [];
    }
  }
}
