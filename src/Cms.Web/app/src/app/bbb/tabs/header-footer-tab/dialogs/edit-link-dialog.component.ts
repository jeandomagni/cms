import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";

import { EditLinkDialogData } from "src/app/bbb/tabs/header-footer-tab/dialogs/edit-link-dialog-data.interface";

@Component({
  selector: "bbb-edit-link-dialog",
  templateUrl: "./edit-link-dialog.component.html"
})
export class EditLinkDialogComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: EditLinkDialogData,
    private dialogRef: MatDialogRef<EditLinkDialogComponent>
  ) {}

  update(): void {
    this.dialogRef.close(this.data.link);
  }
}
