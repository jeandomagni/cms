import { Component, Input } from "@angular/core";
import { Observable } from "rxjs";

import { BbbInfoService } from "src/app/services/bbb-info.service";
import { AuditLog } from "src/app/shared/models/audit-log.interface";

@Component({
  selector: "bbb-activity-tab",
  templateUrl: "./activity-tab.component.html"
})
export class ActivityTabComponent {
  @Input() id: number;

  displayLoadMore = true;
  itemsObs: Observable<AuditLog[]>;

  constructor(private bbbInfoService: BbbInfoService) {}

  getItems = (page: number, pageSize: number): Observable<AuditLog[]> =>
    this.bbbInfoService.getAuditLogs(this.id, page, pageSize);
}
