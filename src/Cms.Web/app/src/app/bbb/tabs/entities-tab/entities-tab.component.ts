import { SelectionModel } from "@angular/cdk/collections";
import {
  Component,
  OnInit,
  ViewChild,
  OnChanges,
  Input,
  Output,
  EventEmitter
} from "@angular/core";
import { MatSort, MatTableDataSource, MatDialog } from "@angular/material";
import { takeUntil } from "rxjs/operators";

import { SocialMediaIcons } from "src/app/bbb/constants/social-media-icons.constants";
import { EditLocationDialogComponent } from "src/app/bbb/tabs/entities-tab/dialogs/edit-location-dialog.component";
import { EditProgramDialogComponent } from "src/app/bbb/tabs/entities-tab/dialogs/edit-program-dialog.component";
import { EditSocialMediaDialogComponent } from "src/app/bbb/tabs/entities-tab/dialogs/edit-social-media-dialog.component";
import { EditResourceDialogComponent } from "src/app/bbb/tabs/entities-tab/dialogs/edit-resource-dialog.component";
import { ConfirmDialogService } from "src/app/services/confirm-dialog.service";
import { Bbb } from "src/app/shared/models/bbb/bbb.model";
import { Location } from "src/app/shared/models/bbb/entities/location.interface";
import { Program } from "src/app/shared/models/bbb/entities/program.interface";
import { Resource } from "src/app/shared/models/bbb/entities/resource.interface";
import { SocialMedia } from "src/app/shared/models/bbb/entities/social-media.interface";

@Component({
  selector: "bbb-entities-tab",
  templateUrl: "./entities-tab.component.html"
})
export class EntitiesTabComponent implements OnInit, OnChanges {
  @Input() bbb: Bbb;
  @Output() bbbChange: EventEmitter<Bbb> = new EventEmitter();
  @Output() update: EventEmitter<void> = new EventEmitter();

  @ViewChild(MatSort, { static: true }) locationSort: MatSort;
  @ViewChild(MatSort, { static: true }) programSort: MatSort;
  @ViewChild(MatSort, { static: true }) resourceSort: MatSort;
  @ViewChild(MatSort, { static: true }) socialMediaSort: MatSort;

  displayedLocationColumns: string[] = [
    "select",
    "type",
    "contactEmail",
    "phoneNumber",
    "address",
    "city",
    "state",
    "zipCode",
    "country"
  ];
  displayedProgramColumns: string[] = [
    "select",
    "title",
    "summaryText",
    "linkUrl",
    "icon",
    "priority"
  ];
  displayedResourceColumns: string[] = [
    "select",
    "title",
    "description",
    "url",
    "priority"
  ];
  displayedSocialMediaColumns: string[] = ["select", "name", "icon", "url"];
  locationsDataSource: MatTableDataSource<Location> = new MatTableDataSource();
  programsDataSource: MatTableDataSource<Program> = new MatTableDataSource();
  resourcesDataSource: MatTableDataSource<Resource> = new MatTableDataSource();
  socialMediaDataSource: MatTableDataSource<
    SocialMedia
  > = new MatTableDataSource();
  selectedLocation: SelectionModel<Location> = new SelectionModel<Location>(
    false,
    []
  );
  selectedProgram: SelectionModel<Program> = new SelectionModel<Program>(
    false,
    []
  );
  selectedResource: SelectionModel<Resource> = new SelectionModel<Resource>(
    false,
    []
  );
  selectedSocialMedia: SelectionModel<SocialMedia> = new SelectionModel<
    SocialMedia
  >(false, []);
  private readonly onDestroy: EventEmitter<void> = new EventEmitter();

  constructor(
    private confirmDialogService: ConfirmDialogService,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.locationsDataSource.sort = this.locationSort;
    this.programsDataSource.sort = this.programSort;
    this.resourcesDataSource.sort = this.resourceSort;
    this.socialMediaDataSource.sort = this.socialMediaSort;
  }

  ngOnChanges(): void {
    this.locationsDataSource.data = this.bbb.locations;
    this.programsDataSource.data = this.bbb.programs;
    this.resourcesDataSource.data = this.bbb.additionalResources;
    this.socialMediaDataSource.data = this.bbb.socialMedia;
  }

  addLocation(): void {
    const dialogData = { location: {} };
    const dialogRef = this.dialog.open(EditLocationDialogComponent, {
      data: dialogData
    });

    dialogRef.afterClosed().subscribe((location: Location) => {
      if (location) {
        this.bbb.locations.push(location);
        this.bbbChange.emit(this.bbb);
        this.update.emit();
      }
    });
  }

  addProgram(): void {
    const dialogData = { program: {} };
    const dialogRef = this.dialog.open(EditProgramDialogComponent, {
      data: dialogData
    });

    dialogRef.afterClosed().subscribe((program: Program) => {
      if (program) {
        this.bbb.programs.push(program);
        this.bbbChange.emit(this.bbb);
        this.update.emit();
      }
    });
  }

  addResource(): void {
    const dialogData = { resource: {} };
    const dialogRef = this.dialog.open(EditResourceDialogComponent, {
      data: dialogData
    });

    dialogRef.afterClosed().subscribe((resource: Resource) => {
      if (resource) {
        this.bbb.additionalResources.push(resource);
        this.bbbChange.emit(this.bbb);
        this.update.emit();
      }
    });
  }

  addSocialMedia(): void {
    const dialogData = { socialMedia: {} };
    const dialogRef = this.dialog.open(EditSocialMediaDialogComponent, {
      data: dialogData
    });

    dialogRef.afterClosed().subscribe((socialMedia: SocialMedia) => {
      if (socialMedia) {
        this.bbb.socialMedia.push(socialMedia);
        this.bbbChange.emit(this.bbb);
        this.update.emit();
      }
    });
  }

  deleteLocation(): void {
    this.confirmDialogService
      .showConfirmDelete()
      .pipe(takeUntil(this.onDestroy))
      .subscribe(confirmed => {
        if (confirmed) {
          const location = this.selectedLocation.selected[0];

          this.bbb.locations = this.bbb.locations.filter(
            (x: Location) => x.addressLine1 !== location.addressLine1
          );
          this.bbbChange.emit(this.bbb);
          this.update.emit();

          this.selectedLocation.clear();
        }
      });
  }

  deleteProgram(): void {
    this.confirmDialogService
      .showConfirmDelete()
      .pipe(takeUntil(this.onDestroy))
      .subscribe(confirmed => {
        if (confirmed) {
          const program = this.selectedProgram.selected[0];

          this.bbb.programs = this.bbb.programs.filter(
            (x: Program) => x.title !== program.title
          );
          this.bbbChange.emit(this.bbb);
          this.update.emit();

          this.selectedProgram.clear();
        }
      });
  }

  deleteResource(): void {
    this.confirmDialogService
      .showConfirmDelete()
      .pipe(takeUntil(this.onDestroy))
      .subscribe(confirmed => {
        if (confirmed) {
          const resource = this.selectedResource.selected[0];

          this.bbb.additionalResources = this.bbb.additionalResources.filter(
            (x: Resource) => x.title !== resource.title
          );
          this.bbbChange.emit(this.bbb);
          this.update.emit();

          this.selectedResource.clear();
        }
      });
  }

  deleteSocialMedia(): void {
    this.confirmDialogService
      .showConfirmDelete()
      .pipe(takeUntil(this.onDestroy))
      .subscribe(confirmed => {
        if (confirmed) {
          const socialMedia = this.selectedSocialMedia.selected[0];

          this.bbb.socialMedia = this.bbb.socialMedia.filter(
            (x: SocialMedia) => x.name !== socialMedia.name
          );
          this.bbbChange.emit(this.bbb);
          this.update.emit();

          this.selectedSocialMedia.clear();
        }
      });
  }

  editLocation(): void {
    const location = this.selectedLocation.selected[0];
    const dialogData = { location };
    const dialogRef = this.dialog.open(EditLocationDialogComponent, {
      data: dialogData
    });

    dialogRef.afterClosed().subscribe((updatedLocation: Location) => {
      if (updatedLocation) {
        this.bbb.locations = this.bbb.locations.map((x: Location) =>
          x.addressLine1 === location.addressLine1 ? updatedLocation : x
        );
        this.bbbChange.emit(this.bbb);
        this.update.emit();

        this.selectedLocation.clear();
      }
    });
  }

  editProgram(): void {
    const program = this.selectedProgram.selected[0];
    const dialogData = { program };
    const dialogRef = this.dialog.open(EditProgramDialogComponent, {
      data: dialogData
    });

    dialogRef.afterClosed().subscribe((updatedProgram: Program) => {
      if (updatedProgram) {
        this.bbb.programs = this.bbb.programs.map((x: Program) =>
          x.title === program.title ? updatedProgram : x
        );
        this.bbbChange.emit(this.bbb);
        this.update.emit();

        this.selectedProgram.clear();
      }
    });
  }

  editResource(): void {
    const resource = this.selectedResource.selected[0];
    const dialogData = { resource };
    const dialogRef = this.dialog.open(EditResourceDialogComponent, {
      data: dialogData
    });

    dialogRef.afterClosed().subscribe((updatedResource: Resource) => {
      if (updatedResource) {
        this.bbb.additionalResources = this.bbb.additionalResources.map(
          (x: Resource) => (x.title === resource.title ? updatedResource : x)
        );
        this.bbbChange.emit(this.bbb);
        this.update.emit();

        this.selectedProgram.clear();
      }
    });
  }

  editSocialMedia(): void {
    const socialMedia = this.selectedSocialMedia.selected[0];
    const dialogData = { socialMedia };
    const dialogRef = this.dialog.open(EditSocialMediaDialogComponent, {
      data: dialogData
    });

    dialogRef.afterClosed().subscribe((updatedSocialMedia: SocialMedia) => {
      if (updatedSocialMedia) {
        this.bbb.socialMedia = this.bbb.socialMedia.map((x: SocialMedia) =>
          x.name === socialMedia.name ? updatedSocialMedia : x
        );
        this.bbbChange.emit(this.bbb);
        this.update.emit();

        this.selectedSocialMedia.clear();
      }
    });
  }

  getSocialMediaIconById = (id: string): string =>
    SocialMediaIcons.find(x => x.id === id)
      ? SocialMediaIcons.find(x => x.id === id).icon
      : null;
}
