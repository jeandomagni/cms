import { Component, Inject, EventEmitter, OnInit } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";
import { takeUntil } from "rxjs/operators";

import { Countries } from "src/app/constants/countries.constants";
import { GeocodeService } from "src/app/services/geocode.service";
import { GoogleLocation } from "src/app/shared/models/location/google-location.interface";
import { EditLocationDialogData } from "src/app/bbb/tabs/entities-tab/dialogs/edit-location-dialog-data.interface";

@Component({
  selector: "bbb-edit-location-dialog",
  templateUrl: "./edit-location-dialog.component.html"
})
export class EditLocationDialogComponent implements OnInit {
  cannotGeocode: boolean;
  countries = Countries;
  locationTypes: string[] = ["Primary", "Branch"];
  officeHours: string;
  phoneHours: string;
  private onDestroy: EventEmitter<void> = new EventEmitter();

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: EditLocationDialogData,
    private dialogRef: MatDialogRef<EditLocationDialogComponent>,
    private geocodeService: GeocodeService
  ) {}

  ngOnInit(): void {
    this.officeHours = this.data.location.officeHours
      ? this.data.location.officeHours.join(",")
      : null;
    this.phoneHours = this.data.location.phoneHours
      ? this.data.location.phoneHours.join(",")
      : null;
    this.cannotGeocode = !(
      this.data.location.addressLine1 &&
      this.data.location.city &&
      this.data.location.state &&
      this.data.location.zipCode &&
      this.data.location.country
    );
  }

  update(): void {
    this.dialogRef.close({
      ...this.data.location,
      officeHours: this.officeHours ? this.officeHours.split(",") : null,
      phoneHours: this.phoneHours ? this.phoneHours.split(",") : null
    });
  }

  geocode(): void {
    const { addressLine1, city, state, zipCode, country } = this.data.location;

    this.geocodeService
      .geocode(addressLine1, city, state, zipCode, country)
      .pipe(takeUntil(this.onDestroy))
      .subscribe((data: GoogleLocation) => {
        this.data.location.latitude = data.lat.toString();
        this.data.location.longitude = data.lng.toString();
      });
  }
}
