import { Resource } from "src/app/shared/models/bbb/entities/resource.interface";

export interface EditResourceDialogData {
  resource: Resource;
}
