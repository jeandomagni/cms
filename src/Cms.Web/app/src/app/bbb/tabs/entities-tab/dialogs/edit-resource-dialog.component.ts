import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";
import { EditResourceDialogData } from "src/app/bbb/tabs/entities-tab/dialogs/edit-resource-dialog-data.interface";

@Component({
  selector: "bbb-edit-resource-dialog",
  templateUrl: "./edit-resource-dialog.component.html"
})
export class EditResourceDialogComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: EditResourceDialogData,
    private dialogRef: MatDialogRef<EditResourceDialogComponent>
  ) {}

  update(): void {
    this.dialogRef.close(this.data.resource);
  }
}
