import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";

import { ProgramIcons } from "src/app/bbb/constants/program-icons.constants";
import { EditProgramDialogData } from "src/app/bbb/tabs/entities-tab/dialogs/edit-program-dialog-data.interface";
import { ProgramIcon } from "src/app/shared/models/bbb/entities/program-icon.interface";

@Component({
  selector: "bbb-edit-program-dialog",
  templateUrl: "./edit-program-dialog.component.html",
  styleUrls: ['./edit-program-dialog.component.scss']
})
export class EditProgramDialogComponent {
  programIcons: ProgramIcon[] = ProgramIcons;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: EditProgramDialogData,
    private dialogRef: MatDialogRef<EditProgramDialogComponent>
  ) {}

  update(): void {
    this.dialogRef.close(this.data.program);
  }
}
