import { Location } from "src/app/shared/models/bbb/entities/location.interface";

export interface EditLocationDialogData {
  location: Location;
}
