import { SocialMedia } from "src/app/shared/models/bbb/entities/social-media.interface";

export interface EditSocialMediaDialogData {
  socialMedia: SocialMedia;
}
