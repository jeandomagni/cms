import { Program } from "src/app/shared/models/bbb/entities/program.interface";

export interface EditProgramDialogData {
  program: Program;
}
