import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";

import { SocialMediaIcons } from "src/app/bbb/constants/social-media-icons.constants";
import { EditSocialMediaDialogData } from "src/app/bbb/tabs/entities-tab/dialogs/edit-social-media-dialog-data.interface";
import { SocialMediaIcon } from "src/app/shared/models/bbb/entities/social-media-icon.interface";

@Component({
  selector: "bbb-edit-social-media-dialog",
  templateUrl: "./edit-social-media-dialog.component.html"
})
export class EditSocialMediaDialogComponent {
  icons: SocialMediaIcon[] = SocialMediaIcons;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: EditSocialMediaDialogData,
    private dialogRef: MatDialogRef<EditSocialMediaDialogComponent>
  ) {}

  update(): void {
    this.dialogRef.close(this.data.socialMedia);
  }
}
