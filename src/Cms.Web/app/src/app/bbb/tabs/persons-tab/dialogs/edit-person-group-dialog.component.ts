﻿import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";

import { EditPersonGroupDialogData } from "src/app/bbb/tabs/persons-tab/dialogs/edit-person-group-dialog-data.interface";

@Component({
  selector: "bbb-edit-person-group-dialog",
  templateUrl: "./edit-person-group-dialog.component.html"
})
export class EditPersonGroupDialogComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: EditPersonGroupDialogData,
    private dialogRef: MatDialogRef<EditPersonGroupDialogComponent>
  ) {}

  update(): void {
    this.dialogRef.close(this.data.group);
  }
}
