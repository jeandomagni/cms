import {
  Component,
  Input,
  OnChanges,
  Output,
  EventEmitter,
  OnDestroy,
} from "@angular/core";
import { MatDialog } from "@angular/material";
import { takeUntil } from "rxjs/operators";
import urljoin from "url-join";

import { Group } from "src/app/bbb/tabs/persons-tab/models/group.model";
import { EditPersonDialogComponent } from "src/app/bbb/tabs/persons-tab/dialogs/edit-person-dialog.component";
import { EditPersonGroupDialogComponent } from "src/app/bbb/tabs/persons-tab/dialogs/edit-person-group-dialog.component";
import { ConfirmDialogService } from "src/app/services/confirm-dialog.service";
import { Bbb } from "src/app/shared/models/bbb/bbb.model";
import { Person } from "src/app/shared/models/bbb/entities/person.interface";
import { PersonGroup } from "src/app/shared/models/bbb/entities/person-group.interface";
import slugify from "src/app/shared/utils/slugify";

@Component({
  selector: "bbb-persons-tab",
  templateUrl: "./persons-tab.component.html",
})
export class PersonsTabComponent implements OnChanges, OnDestroy {
  @Input() bbb: Bbb;
  @Output() bbbChange: EventEmitter<Bbb> = new EventEmitter();
  @Output() update: EventEmitter<void> = new EventEmitter();

  displayedPersonsColumns: string[] = [
    "select",
    "title",
    "firstName",
    "lastName",
    "email",
    "phone",
  ];
  groups: Group[];
  private onDestroy: EventEmitter<void> = new EventEmitter();

  constructor(
    private confirmDialogService: ConfirmDialogService,
    private dialog: MatDialog
  ) {}

  ngOnChanges(): void {
    this.groups = this.bbb.persons.map((personGroup) =>
      this.getGroup(personGroup)
    );
  }

  ngOnDestroy(): void {
    this.onDestroy.next();
  }

  addGroup(): void {
    const dialogData = { group: { name: "", persons: [] } };
    const dialogRef = this.dialog.open(EditPersonGroupDialogComponent, {
      data: dialogData,
    });

    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.onDestroy))
      .subscribe((updatedGroup: PersonGroup) => {
        if (!updatedGroup) {
          return;
        }

        this.bbb.persons.push(updatedGroup);
        this.bbbChange.emit(this.bbb);
        this.update.emit();
      });
  }

  addPerson(group: Group): void {
    const indexToUpdate = this.bbb.persons.findIndex(
      (x) => x.name === group.Group.name
    );

    const dialogData = { person: { image: {} } };
    const dialogRef = this.dialog.open(EditPersonDialogComponent, {
      data: dialogData,
    });

    dialogRef.afterClosed().subscribe((person: Person) => {
      if (!person) {
        return;
      }

      this.bbb.persons[indexToUpdate].persons.push(person);

      this.bbbChange.emit(this.bbb);
      this.update.emit();
    });
  }

  deleteGroup(group: PersonGroup): void {
    this.confirmDialogService
      .showConfirmDelete()
      .pipe(takeUntil(this.onDestroy))
      .subscribe((confirmed) => {
        if (confirmed) {
          this.bbb.persons = this.bbb.persons.filter((x) => x !== group);
          this.bbbChange.emit(this.bbb);
          this.update.emit();
        }
      });
  }

  deletePerson(group: Group): void {
    this.confirmDialogService
      .showConfirmDelete()
      .pipe(takeUntil(this.onDestroy))
      .subscribe((confirmed) => {
        if (confirmed) {
          const indexToUpdate = this.bbb.persons.findIndex(
            (x) => x.name === group.Group.name
          );
          const personToRemove = group.Grid.selected.selected[0];
          this.bbb.persons[indexToUpdate].persons = this.bbb.persons[
            indexToUpdate
          ].persons.filter((x: Person) => x !== personToRemove);

          this.bbbChange.emit(this.bbb);
          this.update.emit();
        }
      });
  }

  editGroup(group: PersonGroup): void {
    const indexToUpdate = this.bbb.persons.findIndex(
      (x) => x.name === group.name
    );

    const dialogData = { group: { ...group } };
    const dialogRef = this.dialog.open(EditPersonGroupDialogComponent, {
      data: dialogData,
    });

    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.onDestroy))
      .subscribe((updatedGroup: PersonGroup) => {
        if (!updatedGroup) {
          return;
        }

        this.bbb.persons[indexToUpdate] = updatedGroup;
        this.bbbChange.emit(this.bbb);
        this.update.emit();
      });
  }

  editPerson(group: Group): void {
    const indexToUpdate = this.bbb.persons.findIndex(
      (x) => x.name === group.Group.name
    );
    const personToUpdate = group.Grid.selected.selected[0];

    const dialogData = { person: { ...personToUpdate, image: {} } };
    const dialogRef = this.dialog.open(EditPersonDialogComponent, {
      data: dialogData,
    });

    dialogRef.afterClosed().subscribe((person: Person) => {
      if (!person) {
        return;
      }

      this.bbb.persons[indexToUpdate].persons = this.bbb.persons[
        indexToUpdate
      ].persons.map((x: Person) => (x === personToUpdate ? person : x));

      this.bbbChange.emit(this.bbb);
      this.update.emit();
    });
  }

  generateGroupUrl = (groupName: string): string => {
    return urljoin(
      window["terminusBaseUrl"],
      "local-bbb",
      this.bbb.bbbUrlSegment,
      "people",
      slugify(groupName)
    );
  };

  private getGroup = (personGroup: PersonGroup): Group =>
    new Group(personGroup);
}
