﻿import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";

import { Media } from "src/app/shared/models/content/entities/media.model";
import { EditPersonDialogData } from "src/app/bbb/tabs/persons-tab/dialogs/edit-person-dialog-data.interface";

@Component({
  selector: "bbb-edit-person-group-dialog",
  templateUrl: "./edit-person-dialog.component.html",
  styleUrls: ['./edit-person-dialog.component.scss']
})
export class EditPersonDialogComponent {
  image = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: EditPersonDialogData,
    private dialogRef: MatDialogRef<EditPersonDialogComponent>
  ) {
    if(data.person.imageUrl != null) {
      this.image.push(<Media> { url: data.person.imageUrl });
    }

  }

  onImageChange(media: Media): void {
    this.data.person.image = media || new Media();

    if (media) {
      this.data.person.imageUrl = media.url;
    }
  }

  update(): void {
    this.dialogRef.close(this.data.person);
  }

  imageChange(media: Media): void {
    this.data.person.image = media || new Media();
    if (media) {
      this.data.person.imageUrl = media.url;
    }
  }

  imageDelete(id: number): void {
    this.data.person.image = null;
    this.data.person.imageUrl = null;
    if(this.image.length > 0) {
      this.image.splice(0, 1);
    }
  }
}
