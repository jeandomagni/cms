import { PersonGroup } from "src/app/shared/models/bbb/entities/person-group.interface";

export interface EditPersonGroupDialogData {
  group: PersonGroup;
}
