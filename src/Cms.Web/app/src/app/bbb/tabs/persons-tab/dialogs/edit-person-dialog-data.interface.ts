import { Person } from "src/app/shared/models/bbb/entities/person.interface";

export interface EditPersonDialogData {
  person: Person;
}
