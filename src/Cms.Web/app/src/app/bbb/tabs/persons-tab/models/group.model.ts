import { PersonsGrid } from "src/app/bbb/tabs/persons-tab/models/persons-grid.model";
import { PersonGroup } from "src/app/shared/models/bbb/entities/person-group.interface";

export class Group {
  Group: PersonGroup;
  Grid: PersonsGrid;

  constructor(group: PersonGroup) {
    this.Group = group;
    this.Grid = new PersonsGrid(group.persons);
  }
}
