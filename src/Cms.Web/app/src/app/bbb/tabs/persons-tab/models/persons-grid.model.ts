import { MatTableDataSource } from "@angular/material";
import { SelectionModel } from "@angular/cdk/collections";

import { Person } from "src/app/shared/models/bbb/entities/person.interface";

export class PersonsGrid {
  dataSource: MatTableDataSource<Person>;
  selected: SelectionModel<Person>;

  constructor(persons: Person[]) {
    this.dataSource = new MatTableDataSource<Person>(persons);
    this.selected = new SelectionModel<Person>(false, []);
  }
}
