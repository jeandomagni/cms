import { Component, Input } from "@angular/core";

import { Bbb } from "src/app/shared/models/bbb/bbb.model";

@Component({
  selector: "bbb-lists-tab",
  templateUrl: "./lists-tab.component.html"
})
export class ListsTabComponent {
  @Input() bbb: Bbb;
  @Input() canEdit: boolean;
  ngOnChanges(): void {
    this.bbb.phoneLeadsEmails
      ? this.bbb.phoneLeadsEmails.replace(/\s/g, "")
      : undefined;
  }
}
