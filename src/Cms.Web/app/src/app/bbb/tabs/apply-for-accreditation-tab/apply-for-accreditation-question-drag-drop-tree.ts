import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnInit,
  OnChanges
} from "@angular/core";
import { MatDialog } from "@angular/material";
import { Observable } from "rxjs";

import { ApplyForAccreditationInfoQuestion } from "src/app/shared/models/bbb/entities/apply-for-accreditation.interface";
import { ConfirmDialogService } from "src/app/services/confirm-dialog.service";
import { BaseDragDropTreeComponent } from "src/app/components/base/base-drag-drop-tree/base-drag-drop-tree.component";
import { TreeNode } from "src/app/components/base/base-drag-drop-tree/tree-node.interface";

import { EditAccreditationQuestionDialogComponent } from "./dialogs/edit-accreditation-question-dialog.component";

@Component({
  selector: "apply-for-accreditation-question-drag-drop-tree",
  templateUrl: "./apply-for-accreditation-question-drag-drop-tree.html",
  styleUrls: ["./apply-for-accreditation-question-drag-drop-tree.styles.scss"]
})
export class ApplyForAccreditationQuestionDragDropTreeComponent
  extends BaseDragDropTreeComponent<ApplyForAccreditationInfoQuestion>
  implements OnInit, OnChanges {
  @Input() questions: ApplyForAccreditationInfoQuestion[];
  @Output() questionsChange: EventEmitter<
  ApplyForAccreditationInfoQuestion[]
  > = new EventEmitter();

  allowItemEdit = true;
  generateLabel = (question: ApplyForAccreditationInfoQuestion): string => question.question;

  constructor(
    confirmDialogService: ConfirmDialogService,
    private dialog: MatDialog
  ) {
    super(confirmDialogService);
  }

  ngOnInit() {
    this.refreshItems();

    this.update = (items: ApplyForAccreditationInfoQuestion[]) => {
      this.questionsChange.emit(items);
      this.refreshItems();
    };
  }

  ngOnChanges() {
    this.refreshItems();
  }

  addEditItem = (
    question: ApplyForAccreditationInfoQuestion
  ): Observable<ApplyForAccreditationInfoQuestion> => {
    const dialogRef = this.dialog.open(EditAccreditationQuestionDialogComponent, {
      data: {
        question
      }
    });

    return dialogRef.afterClosed();
  };

  getIsEditable = (node: TreeNode) =>
    this.questions.find(x => this.generateLabel(x) === node.label).editable;

  refreshItems() {
    this.items = this.questions;
    if (this.dataSource) {
      this.dataSource.data = this.questions || [];
    }
  }
}
