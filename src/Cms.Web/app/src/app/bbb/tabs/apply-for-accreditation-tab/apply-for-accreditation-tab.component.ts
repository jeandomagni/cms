import { Component, Input, Output, EventEmitter } from "@angular/core";

import { AuthService } from "src/app/auth/auth.service";
import { Bbb } from "src/app/shared/models/bbb/bbb.model";
import { ApplicationSubmissions } from "src/app/constants/ApplicationSubmission.constants";
import { MatDialog } from "@angular/material";
import { EditAccreditationQuestionDialogComponent } from "./dialogs/edit-accreditation-question-dialog.component";
import { ApplyForAccreditationInfoQuestion } from "src/app/shared/models/bbb/entities/apply-for-accreditation.interface";
import { takeUntil } from "rxjs/operators";
@Component({
  selector: "bbb-apply-for-accreditation-tab",
  templateUrl: "./apply-for-accreditation-tab.component.html",
})
export class ApplyForAccreditationTabComponent {
  @Input() bbb: Bbb;

  @Input() canEdit: boolean;
  @Output() bbbChange: EventEmitter<Bbb> = new EventEmitter();
  @Output() update: EventEmitter<void> = new EventEmitter();

  readonly MAX_QUESTIONS = 10; // TODO: move to config

  private onDestroy: EventEmitter<void> = new EventEmitter();

  isSystemAdmin: boolean;
  applicationSubmissions = ApplicationSubmissions;

  constructor(authService: AuthService, private dialog: MatDialog) {
    this.isSystemAdmin = authService.isAdmin;
  }

  ngOnChanges(): void {
      this.bbb.applyForAccreditationInfo.applyEmail = this.bbb
          .applyForAccreditationInfo.applyEmail
          ? this.bbb.applyForAccreditationInfo.applyEmail.replace(/\s/g, "")
          : this.bbb.phoneLeadsEmails.replace(/\s/g, "");
  }

  addQuestion(): void {
    const dialogRef = this.dialog.open(EditAccreditationQuestionDialogComponent, {
      data: {
        question: <ApplyForAccreditationInfoQuestion>{
          editable: true
        }
      }
    });

    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.onDestroy))
      .subscribe((question: ApplyForAccreditationInfoQuestion) => {
        if (question) {
          this.bbb.applyForAccreditationInfo.questions.push(question);
          this.bbbChange.emit(this.bbb);
          this.update.emit();
        }
      });
  }
}
