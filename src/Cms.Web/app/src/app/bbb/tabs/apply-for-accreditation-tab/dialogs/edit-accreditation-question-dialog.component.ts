import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";
import { Settings } from "tinymce";

import { AuthService } from "src/app/auth/auth.service";
import { NotificationService } from "src/app/services/notification.service";
import { TinymceService } from "src/app/services/tinymce.service";

import { EditQuestionDialogData } from "./edit-accreditation-question-dialog-data.interface";

@Component({
  selector: "bbb-edit-accreditation-question-dialog",
  templateUrl: "./edit-accreditation-question-dialog.component.html",
  styleUrls: ["./edit-accreditation-question-dialog.styles.scss"]
})
export class EditAccreditationQuestionDialogComponent {
  tinymceInit: Settings;
  tinymceKey: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: EditQuestionDialogData,
    authService: AuthService,
    private dialogRef: MatDialogRef<EditAccreditationQuestionDialogComponent>,
    private notificationService: NotificationService,
    tinyMceService: TinymceService
  ) {
    const baseSettings = authService.isAdmin
      ? tinyMceService.adminSettings
      : tinyMceService.globalSettings;

    this.tinymceInit = {
      ...baseSettings,
      height: 250
    };
    this.tinymceKey = tinyMceService.key;
  }

  update(): void {
    if (!this.data.question.question) {
      this.notificationService.error(
        "You must enter a question before saving."
      );
      return;
    }
    if (this.data.question.required == null) {
      this.notificationService.error(
        "You must enter a required before saving."
      );
      return;
    } 
    this.dialogRef.close(this.data.question);
  }

}
