import { ApplyForAccreditationInfoQuestion } from "src/app/shared/models/bbb/entities/apply-for-accreditation.interface";

export interface EditQuestionDialogData {
  question: ApplyForAccreditationInfoQuestion;
}
