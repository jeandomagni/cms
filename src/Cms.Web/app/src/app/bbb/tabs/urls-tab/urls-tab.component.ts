import { Component, Input } from "@angular/core";

import { AuthService } from "src/app/auth/auth.service";
import { Bbb } from "src/app/shared/models/bbb/bbb.model";

@Component({
  selector: "bbb-urls-tab",
  templateUrl: "./urls-tab.component.html"
})
export class UrlsTabComponent {
  @Input() bbb: Bbb;
  @Input() canEdit: boolean;

  isSystemAdmin: boolean;

  constructor(authService: AuthService) {
    this.isSystemAdmin = authService.isAdmin;
  }
}
