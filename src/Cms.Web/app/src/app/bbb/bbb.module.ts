import { DragDropModule } from "@angular/cdk/drag-drop";
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FlexLayoutModule } from "@angular/flex-layout";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {
  MatTabsModule,
  MatIconModule,
  MatCardModule,
  MatAutocompleteModule,
  MatFormFieldModule,
  MatButtonModule,
  MatInputModule,
  MatCheckboxModule,
  MatToolbarModule,
  MatDialogModule,
  MatSelectModule,
  MatSortModule,
  MatChipsModule,
  MatTableModule,
  MatTreeModule,
  MatDatepickerModule,
  MatMenuModule,
  MatRadioModule
} from "@angular/material";
import { EditorModule } from "@tinymce/tinymce-angular";

import { BbbComponent } from "src/app/bbb/bbb.component";
import { BbbRoutingModule } from "src/app/bbb/bbb.routing-module";
import { ActivityTabComponent } from "src/app/bbb/tabs/activity-tab/activity-tab.component";
import { EntitiesTabComponent } from "src/app/bbb/tabs/entities-tab/entities-tab.component";
import { EditLocationDialogComponent } from "src/app/bbb/tabs/entities-tab/dialogs/edit-location-dialog.component";
import { EditProgramDialogComponent } from "src/app/bbb/tabs/entities-tab/dialogs/edit-program-dialog.component";
import { EditResourceDialogComponent } from "src/app/bbb/tabs/entities-tab/dialogs/edit-resource-dialog.component";
import { EditSocialMediaDialogComponent } from "src/app/bbb/tabs/entities-tab/dialogs/edit-social-media-dialog.component";
import { EditQuestionDialogComponent } from "src/app/bbb/tabs/file-a-complaint-tab/dialogs/edit-question-dialog.component";
import { EditAccreditationQuestionDialogComponent } from "src/app/bbb/tabs/apply-for-accreditation-tab/dialogs/edit-accreditation-question-dialog.component";
import { ApplyForAccreditationQuestionDragDropTreeComponent } from "src/app/bbb/tabs/apply-for-accreditation-tab/apply-for-accreditation-question-drag-drop-tree";
import { FileAComplaintTabComponent } from "src/app/bbb/tabs/file-a-complaint-tab/file-a-complaint-tab.component";
import { ComplaintQuestionDragDropTreeComponent } from "src/app/bbb/tabs/file-a-complaint-tab/complaint-question-drag-drop-tree";
import { GeneralTabComponent } from "src/app/bbb/tabs/general-tab/general-tab.component";
import { CategoryDragDropTreeComponent } from "src/app/bbb/tabs/general-tab/category-drag-drop-tree";
import { EditLinkDialogComponent } from "src/app/bbb/tabs/header-footer-tab/dialogs/edit-link-dialog.component";
import { HeaderFooterTabComponent } from "src/app/bbb/tabs/header-footer-tab/header-footer-tab.component";
import { ListsTabComponent } from "src/app/bbb/tabs/lists-tab/lists-tab.component";
import { MediaTabComponent } from "src/app/bbb/tabs/media-tab/media-tab.component";
import { PersonsTabComponent } from "src/app/bbb/tabs/persons-tab/persons-tab.component";
import { UrlsTabComponent } from "src/app/bbb/tabs/urls-tab/urls-tab.component";
import { ApplyForAccreditationTabComponent } from "src/app/bbb/tabs/apply-for-accreditation-tab/apply-for-accreditation-tab.component";
import { LeadsReportsTabComponent } from "src/app/bbb/tabs/leads-reports-tab/leads-reports-tab.component";
import { BbbInfoService } from "src/app/services/bbb-info.service";
import { NotificationService } from "src/app/services/notification.service";
import { SharedModule } from "src/app/shared/shared.module";
import { GeocodeService } from "src/app/services/geocode.service";
import { CategoryAutocompleteComponent } from "src/app/bbb/tabs/general-tab/category-autocomplete/category-autocomplete.component";
import { EditPersonDialogComponent } from "src/app/bbb/tabs/persons-tab/dialogs/edit-person-dialog.component";
import { EditPersonGroupDialogComponent } from "src/app/bbb/tabs/persons-tab/dialogs/edit-person-group-dialog.component";
import { MenuLinksDragDropTreeComponent } from "src/app/bbb/tabs/header-footer-tab/menu-links-drag-drop-tree";
import { ValidationService } from "src/app/bbb/services/validation.service";
import { TinymceService } from "src/app/services/tinymce.service";
import {
  OwlDateTimeModule,
  OWL_DATE_TIME_FORMATS,
  OwlNativeDateTimeModule
} from "ng-pick-datetime";

const CMS_NATIVE_FORMATS = {
  fullPickerInput: { year: "numeric", month: "numeric", day: "numeric" },
  datePickerInput: { year: "numeric", month: "numeric", day: "numeric" },
  timePickerInput: { hour: "numeric", minute: "numeric" },
  monthYearLabel: { year: "numeric", month: "short" },
  dateA11yLabel: { year: "numeric", month: "long", day: "numeric" },
  monthYearA11yLabel: { year: "numeric", month: "long" }
};

const Components = [
  ActivityTabComponent,
  BbbComponent,
  CategoryAutocompleteComponent,
  CategoryDragDropTreeComponent,
  ComplaintQuestionDragDropTreeComponent,
  EditLinkDialogComponent,
  EditLocationDialogComponent,
  EditPersonDialogComponent,
  EditPersonGroupDialogComponent,
  EditProgramDialogComponent,
  EditQuestionDialogComponent,
  EditAccreditationQuestionDialogComponent,
  ApplyForAccreditationQuestionDragDropTreeComponent,
  EditResourceDialogComponent,
  EditSocialMediaDialogComponent,
  EntitiesTabComponent,
  FileAComplaintTabComponent,
  GeneralTabComponent,
  HeaderFooterTabComponent,
  ListsTabComponent,
  MediaTabComponent,
  MenuLinksDragDropTreeComponent,
  PersonsTabComponent,
  UrlsTabComponent,
  ApplyForAccreditationTabComponent,
  LeadsReportsTabComponent
];

@NgModule({
  imports: [
    CommonModule,
    DragDropModule,
    EditorModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,

    MatAutocompleteModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatFormFieldModule,
    MatRadioModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTreeModule,
    MatMenuModule,
    SharedModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,

    BbbRoutingModule
  ],
  declarations: [...Components],
  entryComponents: [...Components],
  providers: [
    BbbInfoService,
    GeocodeService,
    NotificationService,
    TinymceService,
    ValidationService,
    { provide: OWL_DATE_TIME_FORMATS, useValue: CMS_NATIVE_FORMATS }
  ],
})
export class BbbModule {}

