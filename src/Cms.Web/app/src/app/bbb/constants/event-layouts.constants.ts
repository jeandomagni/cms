import { EventLayout } from "src/app/shared/models/bbb/entities/event-layout.interface";

export const EventLayouts: EventLayout[] = [
  {
    value: "standard",
    name: "Standard Layout"
  },
  {
    value: "agenda",
    name: "Agenda Layout"
  }
];
