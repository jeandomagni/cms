import { SocialMediaIcon } from "src/app/shared/models/bbb/entities/social-media-icon.interface";

export const SocialMediaIcons: SocialMediaIcon[] = [
  {
    id: "facebook",
    name: "Facebook",
    icon: "fa-facebook-square"
  },
  {
    id: "flickr",
    name: "Flickr",
    icon: "fa-flickr"
  },
  {
    id: "foursquare",
    name: "Foursquare",
    icon: "fa-foursquare"
  },
  {
    id: "instagram",
    name: "Instagram",
    icon: "fa-instagram"
  },
  {
    id: "linkedin",
    name: "LinkedIn",
    icon: "fa-linkedin-square"
  },
  {
    id: "pinterest",
    name: "Pinterest",
    icon: "fa-pinterest-square"
  },
  {
    id: "twitter",
    name: "Twitter",
    icon: "fa-twitter-square"
  },
  {
    id: "youtube",
    name: "YouTube",
    icon: "fa-youtube-square"
  },
  {
    id: "vimeo",
    name: "Vimeo",
    icon: "fa-vimeo"
  }
];
