import { ProgramIcon } from "src/app/shared/models/bbb/entities/program-icon.interface";

export const ProgramIcons: ProgramIcon[] = [
  {
    value: "basketball",
    name: "Basketball",
    isImg: true,
    src: "/images/programs/basketball.png",
  },
  {
    value: "columned-building",
    name: "Building",
    isImg: true,
    src: "/images/programs/columned-building.png",
  },
  {
    value: "charity",
    name: "Charity",
    isImg: true,
    src: "/images/programs/charity.png",
  },
  { value: "fa-check", name: "Check", isImg: false, faClass: "fa-check" },
  {
    value: "fa-comment",
    name: "Commenting",
    isImg: false,
    faClass: "fa-comment",
  },
  { value: "fa-desktop", name: "Desktop", isImg: false, faClass: "fa-desktop" },
  {
    value: "money",
    name: "Dollar Sign",
    isImg: true,
    src: "/images/programs/money.png",
  },
  {
    value: "generic-event",
    name: "Event",
    isImg: true,
    src: "/images/programs/generic-event.png",
  },
  { value: "fa-file", name: "File", isImg: false, faClass: "fa-file" },
  {
    value: "graduation-cap",
    name: "Graduation Cap",
    isImg: true,
    src: "/images/programs/graduation-cap.png",
  },
  { value: "fa-key", name: "Key", isImg: false, faClass: "fa-key" },
  { value: "fa-lock", name: "Lock", isImg: false, faClass: "fa-lock" },
  {
    value: "programs-generic",
    name: "Programs",
    isImg: true,
    src: "/images/programs/programs-generic.png",
  },
  {
    value: "speaker",
    name: "Speaker",
    isImg: true,
    src: "/images/programs/speaker.png",
  },
  { value: "fa-star", name: "Star", isImg: false, faClass: "fa-star" },
  {
    value: "torch",
    name: "Torch",
    isImg: true,
    src: "/images/programs/torch.png",
  },
  { value: "fa-user", name: "User", isImg: false, faClass: "fa-user" },
  { value: "fa-users", name: "Users", isImg: false, faClass: "fa-users" },
];
