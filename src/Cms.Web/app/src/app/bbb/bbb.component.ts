﻿import { Component, EventEmitter } from "@angular/core";
import { takeUntil } from "rxjs/operators";

import { AuthService } from "src/app/auth/auth.service";
import { Links } from "src/app/bbb/constants/navigation.constants";
import { BbbInfoService } from "src/app/services/bbb-info.service";
import { NotificationService } from "src/app/services/notification.service";
import { Bbb } from "src/app/shared/models/bbb/bbb.model";
import { LegacyBbbInfo } from "src/app/shared/models/bbb/legacy-bbb-info.model";

import { ValidationService } from "./services/validation.service";

@Component({
  selector: "bbb",
  templateUrl: "./bbb.component.html",
})
export class BbbComponent {
  activeLink = "General";
  bbb: Bbb = new Bbb();
  bbbId: string;
  canEdit: boolean;
  heading: string;
  isLocalAdmin: boolean;
  links: string[] = Links;
  private onDestroy: EventEmitter<void> = new EventEmitter();

  constructor(
    private authService: AuthService,
    private bbbInfoService: BbbInfoService,
    private notificationService: NotificationService,
    private validationService: ValidationService
  ) {
    this.bbbId = this.authService.currentUser.profile.defaultSite;
    this.refreshBbb();
  }

  ngOnDestroy(): void {
    this.onDestroy.next();
  }

  setCurrentBbb(bbb: LegacyBbbInfo): void {
    this.bbbId = bbb.legacyBBBID;
    this.refreshBbb();
  }

  update(): void {
    const errors = this.validationService.validateForSave(this.bbb);
    if (errors.length > 0) {
      this.notificationService.warningList(errors);
      return;
    }

    this.bbbInfoService
      .update(this.bbb)
      .pipe(takeUntil(this.onDestroy))
      .subscribe((data: Bbb) => {
        this.notificationService.success("Saved BBB Information");
        this.setBbb(data);
      });
  }

  private refreshBbb(): void {
    this.bbbInfoService
      .getByLegacyId(this.bbbId)
      .pipe(takeUntil(this.onDestroy))
      .subscribe((data: Bbb) => {
        this.setBbb(data);
      });

    this.isLocalAdmin = this.authService.currentUser.adminSites.includes(
      this.bbbId
    );
    this.canEdit =  this.authService.isAdmin;
  }

  private setBbb(bbb: Bbb): void {
    this.bbb = bbb;
    this.heading = this.authService.isGlobalAdmin ? `${bbb.name} [Admin]` : bbb.name;
  }
}
