﻿import { Injectable } from "@angular/core";

import { AuthService } from "src/app/auth/auth.service";
import { GridOptions } from "src/app/shared/models/table/grid-options/grid-options.model";
import { ContentQuery } from "src/app/shared/models/content/query/content-query.model";
import { ContentSearchSettings } from "src/app/shared/models/content/query/content-search-settings.model";
import { ImageSearchSettings } from "src/app/shared/models/file/image-search-settings.model";
import { SecTermSearchSettings } from "src/app/shared/models/sec-terms/sec-term-search-settings.model";

@Injectable()
export class PaginationService {
  private cardSearch = false;

  constructor(private authService: AuthService) {}

  setCardSearch(isOpen: boolean): void {
    this.cardSearch = isOpen;
  }

  getCardSearch(): boolean {
    return this.cardSearch;
  }

  getOptionsForCards(searchSettings: ContentSearchSettings): ContentQuery {
    const {
      title,
      content,
      tags,
      id,
      createdBy,
      filterBy,
      isAdmin,
      page,
      serviceArea
    } = searchSettings;

    const query = new ContentQuery();
    query.domainOnly = searchSettings.filterBy === "Domain Content";
    query.includeExpired = searchSettings.filterBy === "Expired Content";
    query.page = page;

    query.filters = [
      title && { column: "title", type: "contains", keyword: title },
      content && { column: "contentHtml", type: "contains", keyword: content },
      tags && { column: "tags", type: "contains", keyword: tags },
      serviceArea && { column: "bbbIds", type: "contains", keyword: serviceArea },
      id && { column: "id", type: "eq", keyword: id },
      createdBy && {
        column: "createdBy",
        type: "contains",
        keyword: createdBy
      },
      (filterBy === "Domain Content" || filterBy === "All Content") && {
        column: "status",
        type: "eq",
        keyword: "published"
      },
      filterBy === "All Drafts" && {
        column: "status",
        type: "eq",
        keyword: "draft"
      },
      filterBy === "Pending Review" && {
        column: "status",
        type: "eq",
        keyword: "pending_review"
      },
      !isAdmin &&
        (filterBy === "Pending Review" || filterBy === "Expired Content") && {
          column: "createdBy",
          type: "eq",
          keyword: this.authService.authentication.userName
        },
      filterBy === "Expired Content" && {
        column: "expirationDate",
        type: "lte",
        keyword: this.getFilterDateString(new Date())
      }
    ].filter(Boolean);

    return query;
  }

  getOptionsForImages(settings: ImageSearchSettings): GridOptions {
    const { currentPage, title, tags, createdBy } = settings;

    const gridOptions = new GridOptions(currentPage);
    gridOptions.data.filter = {
      logic: "and",
      filters: [
        title && { field: "title", operator: "contains", value: title },
        tags && { field: "tags", operator: "contains", value: tags },
        createdBy && {
          field: "createdBy",
          operator: "contains",
          value: createdBy
        }
      ].filter(Boolean)
    };
    return gridOptions;
  }

  getOptionsForSecTerms(settings: SecTermSearchSettings): GridOptions {
    const { tobId, isActive, urlSegment, name, currentPage } = settings;

    const gridOptions = new GridOptions(currentPage);
    gridOptions.data.filter = {
      logic: "and",
      filters: [
        tobId && { field: "tobId", operator: "contains", value: tobId },
        isActive && {
          field: "isActive",
          operator: "contains",
          value: isActive
        },
        urlSegment && {
          field: "urlSegment",
          operator: "contains",
          value: urlSegment
        },
        name && { field: "name", operator: "contains", value: name }
      ].filter(Boolean)
    };
    return gridOptions;
  }

  private getFilterDateString(date: Date): string {
    const year = date.getFullYear();
    const month = date.getMonth() + 1;
    const day = date.getDate();
    const hour = date.getHours();
    const minutes = date.getMinutes();
    const seconds = date.getSeconds();

    const toDatePart = (part: number): string =>
      part < 10 ? `0${part}` : `${part}`;
    const dateString = `${year}-${toDatePart(month)}-${toDatePart(
      day
    )} ${toDatePart(hour)}:${toDatePart(minutes)}:${toDatePart(seconds)}`;

    return dateString;
  }
}
