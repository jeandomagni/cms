import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { Event } from "src/app/shared/models/content/event/event.model";
import { RecurrenceSettings } from "src/app/shared/models/content/event/recurrence-settings.interface";

@Injectable()
export class EventRecurrenceService {
  private serviceBase = "/api/contentEventRecurrence";

  constructor(private http: HttpClient) {}

  delete = (id: number): Observable<void> =>
    this.http.delete<void>(`${this.serviceBase}/${id}`);

  validate = (settings: RecurrenceSettings): Observable<string> =>
    this.http.post<string>(`${this.serviceBase}/validate/`, settings);

  deleteAllForContentCode = (code: string): Observable<Event> =>
    this.http.delete<Event>(`${this.serviceBase}/byCode/${code}`);
}
