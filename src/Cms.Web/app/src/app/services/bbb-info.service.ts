import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { AuditLog } from "src/app/shared/models/audit-log.interface";
import { Bbb } from "src/app/shared/models/bbb/bbb.model";
import formatDate from "src/app/shared/utils/formatDate";

@Injectable()
export class BbbInfoService {
  private serviceBase = "/api/bbbinfo";

  constructor(private http: HttpClient) {}

  getById = (id: number): Observable<Bbb> =>
    this.http.get<Bbb>(`${this.serviceBase}/${id}`);

  getByLegacyId = (legacyBbbId: string): Observable<Bbb> =>
    this.http.get<Bbb>(`${this.serviceBase}/legacy/${legacyBbbId}`);

  update = (bbbInfo: Bbb): Observable<Bbb> =>
    this.http.post<Bbb>(this.serviceBase, bbbInfo);

  getAuditLogs = (
    id: number,
    page: number,
    pageSize: number
  ): Observable<AuditLog[]> =>
    this.http.get<AuditLog[]>(
      `${this.serviceBase}/audit/${id}/${page || 1}/${pageSize || 10}`
    );

  getListedLeads = (
    bbbId: number,
    rangeStart: Date,
    rangeEnd: Date
  ) =>
    this.http.get( rangeEnd ?
      `${this.serviceBase}/listedLeads/${bbbId}/${formatDate(rangeStart)}/${formatDate(rangeEnd)}` :
      `${this.serviceBase}/listedLeads/${bbbId}/${formatDate(rangeStart)}`
      , { responseType: 'blob'});

    getAccreditationLeads = (
        bbbId: number,
        rangeStart: Date,
        rangeEnd: Date
    ) =>
        this.http.get(rangeEnd ?
            `${this.serviceBase}/accreditationLeads/${bbbId}/${formatDate(rangeStart)}/${formatDate(rangeEnd)}` :
            `${this.serviceBase}/accreditationLeads/${bbbId}/${formatDate(rangeStart)}`
            , { responseType: 'blob' });

}
