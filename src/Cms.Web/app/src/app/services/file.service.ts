﻿import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { AuthService } from "src/app/auth/auth.service";
import { LegacyBbbInfo } from "src/app/shared/models/bbb/legacy-bbb-info.model";
import { File } from "src/app/shared/models/file/file.model";
import { GridOptions } from "src/app/shared/models/table/grid-options/grid-options.model";
import { FileTag } from "src/app/shared/models/file/file-tag.interface";

@Injectable()
export class FileService {
  private serviceBase = "/api/files";

  constructor(private http: HttpClient, private authService: AuthService) {}

  getByCode(code: string): Observable<File[]> {
    return this.http.get<File[]>(`${this.serviceBase}/${code}`);
  }

  paginate(options: GridOptions): Observable<File[]> {
    const locations = this.authService.currentUser.sites.map(
      (loc: LegacyBbbInfo): string => loc.legacyBBBID
    );

    return this.http.post<File[]>(`${this.serviceBase}/paginate`, {
      ...options,
      data: {
        ...options.data,
        locations:
          locations !== null ? locations.join() : options.data.locations
      }
    });
  }

  delete = (code: string): Observable<void> =>
    this.http.delete<void>(`${this.serviceBase}/${code}`);

  update = (file: File): Observable<void> =>
    this.http.post<void>(`${this.serviceBase}/update`, file);

  suggestTags = (
    searchValue: string,
    tagFilter: string
  ): Observable<FileTag[]> =>
    this.http.get<FileTag[]>(
      `/api/imagetag/suggest?input=${searchValue}&filter=${tagFilter}`
    );
}
