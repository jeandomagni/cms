import { TinymceService } from "./tinymce.service";

describe("TinymceService", () => {
  const NOFOLLOW_REL_VALUE = "nofollow";
  const SPONSORED_REL_VALUE = "sponsored";

  let service: TinymceService;
  beforeEach(() => {
    service = new TinymceService();
  });

  describe("adminSettings", () => {
    let linkTypes: any[];
    beforeEach(() => {
      linkTypes = service.adminSettings.rel_list || [];
    });

    it("should contain nofollow rel type", () => {
      expect(
        linkTypes.findIndex((x: any) => x.value === NOFOLLOW_REL_VALUE)
      ).toBeGreaterThan(-1);
    });

    it("should contain sponsored rel type", () => {
      expect(
        linkTypes.findIndex((x: any) => x.value === SPONSORED_REL_VALUE)
      ).toBeGreaterThan(-1);
    });
  });

  describe("globalSettings", () => {
    let linkTypes: any[];
    beforeEach(() => {
      linkTypes = service.globalSettings.rel_list || [];
    });

    it("should not contain nofollow rel type", () => {
      expect(
        linkTypes.findIndex((x: any) => x.value === NOFOLLOW_REL_VALUE)
      ).toEqual(-1);
    });

    it("should not contain sponsored rel type", () => {
      expect(
        linkTypes.findIndex((x: any) => x.value === SPONSORED_REL_VALUE)
      ).toEqual(-1);
    });
  });
});
