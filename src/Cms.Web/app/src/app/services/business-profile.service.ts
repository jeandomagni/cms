﻿import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { BusinessProfile } from "src/app/shared/models/business-profile/business-profile.model";

@Injectable()
export class BusinessProfileService {
  private serviceBase = "/api/businessProfile";

  constructor(private http: HttpClient) {}

  getById = (id: string): Observable<BusinessProfile> =>
    this.http.get<BusinessProfile>(`${this.serviceBase}/${id}`);

  update = (businessProfile: BusinessProfile): Observable<void> =>
    this.http.post<void>(`${this.serviceBase}/update`, businessProfile);
}
