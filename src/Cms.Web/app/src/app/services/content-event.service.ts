﻿import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { Event } from "src/app/shared/models/content/event/event.model";

@Injectable()
export class ContentEventService {
  private serviceBase = "/api/contentEvent";

  constructor(private http: HttpClient) {}

  getByContentCode = (code: string): Observable<Event> =>
    this.http.get<Event>(`${this.serviceBase}/content/${code}`);

  create = (event: Event): Observable<Event> =>
    this.http.put<Event>(this.serviceBase, event);

  update = (event: Event): Observable<Event> =>
    this.http.post<Event>(this.serviceBase, event);

  delete = (contentCode: string, EventContentId: number): Observable<void> =>
    this.http.delete<void>(
      `${this.serviceBase}/${contentCode}/${EventContentId}`
    );
}
