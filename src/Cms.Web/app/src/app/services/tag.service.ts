﻿import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class TagService {
  private serviceBase = '/api/tag';

  constructor(private http: HttpClient) {}

  query = (tag: string): Observable<string[]> => this.http.get<string[]>(`${this.serviceBase}/query/${tag}`);

  updateContentTags = (contentId: number, tags: string[]): Observable<void> =>
    this.http.post<void>(`${this.serviceBase}/content/${contentId}`, tags);
}
