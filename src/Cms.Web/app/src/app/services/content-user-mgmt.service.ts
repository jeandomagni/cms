﻿/*
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { AuthService } from "src/app/auth/auth.service";
import { Quote } from "src/app/shared/models/quote/quote.interface";
import { TableOptions } from "src/app/shared/models/table/table-options.model";
import { LegacyBbbInfo } from "src/app/shared/models/bbb/legacy-bbb-info.model";

@Injectable()
export class ContentQuoteService {
  private serviceBase = "/api/contentQuote";

  constructor(private http: HttpClient, private authService: AuthService) {}

  create(quote: Quote): Observable<void> {
    return this.http.put<void>(this.serviceBase, quote);
  }

  delete = (code: string): Observable<void> =>
    this.http.delete<void>(`${this.serviceBase}/${code}`);

  paginate(options: TableOptions): Observable<Quote[]> {
    const locations = this.authService.currentUser.sites.map(
      (loc: LegacyBbbInfo) => loc.legacyBBBID
    );

    return this.http.post<Quote[]>(`${this.serviceBase}/paginate`, {
      ...options,
      locations: locations ? locations.join() : null
    });
  }
}
*/
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { AuthService } from "src/app/auth/auth.service";
import * as userMgmt  from "src/app/shared/models/user-management/user-membership.model";
import { TableOptions } from "src/app/shared/models/table/table-options.model";
import { LegacyBbbInfo } from "src/app/shared/models/bbb/legacy-bbb-info.model";
import {UserMembershipInfo} from 'src/app/shared/models/user-management/user-membership.model';

@Injectable()
export class UserMgmtService {
  private serviceBase = "/api/userManagement";

  constructor(private http: HttpClient, private authService: AuthService) {}

  addUser(user: UserMembershipInfo): Observable<userMgmt.ServiceResponse> {
    return this.http.post<userMgmt.ServiceResponse>(`${this.serviceBase}/add`, user);
  }

  updateStatus(user: userMgmt.addUpdateUserRequest): Observable<userMgmt.ServiceResponse> {
    return this.http.put<userMgmt.ServiceResponse>(`${this.serviceBase}/updateStatus`, user);
  }

  getInfo(userId:string): Observable<userMgmt.UserMembershipInfo> {
    return this.http.get<userMgmt.UserMembershipInfo>(`${this.serviceBase}/info?userId=${userId}`);
  }

  getUser(email:string): Observable<userMgmt.UserMembership> {
    return this.http.get<userMgmt.UserMembership>(`${this.serviceBase}/userByEmail?email=${email}`);
  }

  updatePassword(user: userMgmt.addUpdateUserRequest): Observable<userMgmt.ServiceResponse> {
    return this.http.put<userMgmt.ServiceResponse>(`${this.serviceBase}/changePassword`, user);
  }

  updateUser(user: userMgmt.addUpdateUserRequest): Observable<userMgmt.ServiceResponse> {
    return this.http.put<userMgmt.ServiceResponse>(`${this.serviceBase}/update`, user);
  }

  delete = (user: string): Observable<userMgmt.ServiceResponse> =>
    this.http.delete<userMgmt.ServiceResponse>(`${this.serviceBase}/${user}`);

  paginate(options: userMgmt.UserMembershipListRequest): Observable<userMgmt.PaginationResult<userMgmt.UserMembership>> {
    const locations = this.authService.currentUser.sites.map(
      (loc: LegacyBbbInfo) => loc.legacyBBBID
    );

    return this.http.post<userMgmt.PaginationResult<userMgmt.UserMembership>>(`${this.serviceBase}/paginate`, {
      ...options,
      locations: locations ? locations.join() : null
    });
  }
}
