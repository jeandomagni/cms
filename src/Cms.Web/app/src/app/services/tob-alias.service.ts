﻿import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { EditAliasParams } from "src/app/shared/models/sec-terms/edit-alias-params.interface";

@Injectable()
export class TobAliasService {
  private serviceBase = "/api/tobAlias";

  constructor(private http: HttpClient) {}

  delete = (addEditAliasParams: EditAliasParams): Observable<void> =>
    this.http.post<void>(`${this.serviceBase}/delete`, addEditAliasParams);

  add = (addEditAliasParams: EditAliasParams): Observable<void> =>
    this.http.post<void>(`${this.serviceBase}/add`, addEditAliasParams);

  update = (addEditAliasParams: EditAliasParams): Observable<void> =>
    this.http.post<void>(`${this.serviceBase}/update`, addEditAliasParams);
}
