import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { AuthService } from "src/app/auth/auth.service";
import { BaselinePage } from "src/app/shared/models/baseline-pages/baseline-page.interface"
import { BaselinePageTableOptions } from "src/app/shared/models/table/baseline-page-table-options.model";

@Injectable()
export class ShortlinksService {
  private serviceBase = "/api/shortlinks";

  constructor(private http: HttpClient, private authService: AuthService) {}

  create(quote: BaselinePage): Observable<void> {
    return this.http.put<void>(this.serviceBase, quote);
  }

  delete = (id: number): Observable<void> =>
    this.http.delete<void>(`${this.serviceBase}/${id}`);

  paginate(options: BaselinePageTableOptions): Observable<BaselinePage[]> {
    return this.http.post<BaselinePage[]>(`${this.serviceBase}/paginate`, {
      ...options
    });
  }
}
