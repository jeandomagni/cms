﻿import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Roles } from 'src/app/shared/models/user-management/user-membership.model';

@Injectable()
export class UserManagementRolesService {
  private serviceBase = '/api/usermanagement';

  constructor(private http: HttpClient) {}

  get = (): Observable<Roles[]> =>
    this.http.get<Roles[]>(`${this.serviceBase}/roles`)

}
