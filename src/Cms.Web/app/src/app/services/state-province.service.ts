﻿import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class StateProvinceService {
  private serviceBase = '/api/stateProvince';

  constructor(private http: HttpClient) {}

  getByCountryCode = (countryCode: string): Observable<string[]> =>
    this.http.get<string[]>(`${this.serviceBase}/${countryCode}`);
}
