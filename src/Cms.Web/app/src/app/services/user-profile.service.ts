﻿import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { UserProfile } from "src/app/shared/models/user/user-profile.model";

@Injectable()
export class UserProfileService {
  private serviceBase = "/api/userprofile";

  constructor(private http: HttpClient) {}

  get = (): Observable<UserProfile> =>
    this.http.get<UserProfile>(`${this.serviceBase}/user`);

  update = (userProfile: UserProfile): Observable<UserProfile> =>
    this.http.post<UserProfile>(this.serviceBase, userProfile);

  verifyPhone = (phone: string): Observable<void> =>
    this.http.get<void>(`${this.serviceBase}/verify/${phone}`);

  savePhone = (verificationCode: string): Observable<void> =>
    this.http.get<void>(`${this.serviceBase}/save/${verificationCode}`);
}
