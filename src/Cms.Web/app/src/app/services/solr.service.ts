﻿import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { Category } from "src/app/shared/models/content/entities/category.interface";
import { Location } from "src/app/shared/models/location/location.interface";
import { BusinessSearchResult } from "src/app/shared/models/business-profile/business-search-result.model";
import { BusinessProfileTypeaheadSuggestion } from "src/app/shared/models/business-profile/business-profile-typeahead-suggestion.interface";
import { ProfileSearchParams } from "src/app/shared/models/business-profile/profile-search-params.interface";

@Injectable()
export class SolrService {
  private serviceBase = "/solrproxy";

  constructor(private http: HttpClient) {}

  paginateCategories = (
    searchText: string,
    excludedCategories: string[],
    countries: string[]
  ): Observable<Category[]> =>
    this.http.post<Category[]>(`${this.serviceBase}/category/paginate`, {
      filterValue: searchText,
      pageSize: 10,
      excludedCategories,
      countries,
      queryPrimaryOnly: false,
      showHighRiskCategories: true
    });

  paginateCities = (
    query: string,
    countryCodes: string[]
  ): Observable<Location[]> =>
    this.http.post<Location[]>(`${this.serviceBase}/locations/query`, {
      query,
      countryCodes,
      context: "*",
      locationType: "CityState"
    });

  queryStateProvinces = (): Observable<Location[]> =>
    this.http.get<Location[]>(`${this.serviceBase}/locations/statesprovinces`);

  suggestBusinesses = (
    searchText: string
  ): Observable<BusinessProfileTypeaheadSuggestion[]> =>
    this.http.get<BusinessProfileTypeaheadSuggestion[]>(
      `${this.serviceBase}/businesses/suggest/${searchText}`
    );

  searchBusinesses = (
    profileSearchParams: ProfileSearchParams
  ): Observable<BusinessSearchResult> =>
    this.http.post<BusinessSearchResult>(
      `${this.serviceBase}/businesses/search/`,
      profileSearchParams
    );

  suggestPrimaryCategories = (
    searchText: string,
    countries: string[],
    excludedCategories: string[]
  ): Observable<Category[]> =>
    this.http.post<Category[]>(`${this.serviceBase}/category/paginate`, {
      filterValue: searchText,
      pageSize: 10,
      excludedCategories,
      countries,
      queryPrimaryOnly: true
    });
}
