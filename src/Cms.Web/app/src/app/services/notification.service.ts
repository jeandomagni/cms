import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class NotificationService {
  constructor(private toastr: ToastrService) {}

  error(message: string): void {
    this.toastr.error(message, 'Error');
  }

  success(message: string): void {
    this.toastr.success(message, 'Success');
  }

  warningList(messages: string[]): void {
    this.toastr.warning(`<ul><li>${messages.join('</li><li>')}</li></ul>`, 'Warning', {
      enableHtml: true,
      timeOut: 0,
    });
  }
}
