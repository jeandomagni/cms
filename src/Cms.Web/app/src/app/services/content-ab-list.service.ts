﻿import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { AbList } from "src/app/shared/models/content/ab-list/ab-list.interface";

@Injectable()
export class ContentAbListService {
  private serviceBase = "/api/contentAbList";

  constructor(private http: HttpClient) {}

  create = (contentAbList: AbList): Observable<AbList> =>
    this.http.put<AbList>(this.serviceBase, contentAbList);

  getByContentCode = (code: string): Observable<AbList> =>
    this.http.get<AbList>(`${this.serviceBase}/content/${code}`);

  update = (contentAbList: AbList): Observable<AbList> =>
    this.http.post<AbList>(`${this.serviceBase}/update`, contentAbList);
}
