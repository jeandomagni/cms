import { Injectable } from "@angular/core";
import { MatDialog } from "@angular/material";
import { Observable } from "rxjs";

import { ConfirmDialogComponent } from "src/app/components/confirm-dialog/confirm-dialog.component";

@Injectable()
export class ConfirmDialogService {
  constructor(private dialog: MatDialog) {}

  showConfirmCustom(label: string, confirmLabel: string): Observable<boolean> {
    return this.show(label, confirmLabel);
  }

  showConfirmDelete(): Observable<boolean> {
    return this.show("Are you sure you want to delete this item?", "Delete");
  }

  showConfirmUnpublish(): Observable<boolean> {
    return this.show(
      "Are you sure you want to unpublish this content?",
      "Unpublish"
    );
  }

  private show(label: string, confirmLabel: string): Observable<boolean> {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { label, confirmLabel }
    });

    return dialogRef.afterClosed();
  }
}
