﻿import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { Content } from "src/app/shared/models/content/content.model";
import { ContentLocation } from "src/app/shared/models/location/content-location.interface";
import { Pin } from "src/app/shared/models/location/pin.interface";
import { Geotag } from "src/app/shared/models/location/geotag.interface";
import { ArticleOrderParams } from '../shared/models/article-order-params.model';

@Injectable()
export class ContentLocationService {
  private serviceBase = "/api/contentLocation";

  constructor(private http: HttpClient) {}

  add(location: ContentLocation, content: Content): Observable<Content> {
    return this.http.post<Content>(`${this.serviceBase}/add/`, {
      location,
      content,
    });
  }

  delete(location: ContentLocation, content: Content): Observable<Content> {
    return this.http.post<Content>(`${this.serviceBase}/delete/`, {
      location,
      content,
    });
  }

  addPin(pin: Pin, content: Content): Observable<Content> {
    return this.http.post<Content>(`${this.serviceBase}/addPin/`, {
      pin,
      content,
    });
  }

  deletePin(pin: Pin, content: Content): Observable<Content> {
    return this.http.post<Content>(`${this.serviceBase}/deletePin/`, {
      pin,
      content,
    });
  }

  updatePin(pin: Pin, content: Content): Observable<Content> {
    return this.http.post<Content>(`${this.serviceBase}/updatePin/`, {
      pin,
      content,
    });
  }

  updateGeotag(geotag: Geotag): Observable<Content> {
    return this.http.post<Content>(`${this.serviceBase}/updateGeotag/`, geotag);
  }

  deleteGeotag(geotag: Geotag): Observable<Content> {
    return this.http.post<Content>(`${this.serviceBase}/deleteGeotag/`, geotag);
  }

  deleteFromFeed = (code: string, articleOrderId: number): Observable<void> =>
    this.http.delete<void>(
      `${this.serviceBase}/${code}/deleteFromFeed/${articleOrderId}`
    );

  addToMyNewsfeed = (code: string): Observable<Content> =>
    this.http.post<Content>(
      `${this.serviceBase}/addToMyNewsfeed/${code}`,
      null
    );
  addToCountriesNewsfeed = (
    code: string,
    countries: string[]
  ): Observable<Content> =>
    this.http.post<Content>(
      `${this.serviceBase}/addToCountriesNewsfeed/${code}/${countries.join(
        ","
      )}`,
      null
    );
  deleteFromMyNewsfeed = (code: string): Observable<Content> =>
    this.http.post<Content>(
      `${this.serviceBase}/deleteFromMyNewsfeed/${code}`,
      null
    );

  addToTop10 = (data: ArticleOrderParams, articleId: string): Observable<Content> =>
      this.http.post<Content>(
        `${this.serviceBase}/addToTop/${articleId}`,
        data
      );
}
