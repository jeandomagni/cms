﻿import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { NearMe } from "src/app/shared/models/content/near-me/near-me.interface";

@Injectable()
export class ContentNearMeService {
  private serviceBase = "/api/contentNearMe";

  constructor(private http: HttpClient) {}

  getByContentCode = (code: string): Observable<NearMe> =>
    this.http.get<NearMe>(`${this.serviceBase}/content/${code}`);

  create = (nearMe: NearMe): Observable<NearMe> =>
    this.http.put<NearMe>(this.serviceBase, nearMe);

  update = (nearMe: NearMe): Observable<NearMe> =>
    this.http.post<NearMe>(this.serviceBase, nearMe);

  delete = (contentCode: string, id: number): Observable<void> =>
    this.http.delete<void>(`${this.serviceBase}/${contentCode}/${id}`);

  getSelectedTobIds = (): Observable<string[]> =>
    this.http.get<string[]>(`${this.serviceBase}/selectedTobIds`);
}
