import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

import { GoogleLocation } from "src/app/shared/models/location/google-location.interface";

@Injectable()
export class GeocodeService {
  private serviceBase = "/api/geocode";
  constructor(private httpClient: HttpClient) {}

  geocode(
    addressLine1: string,
    city: string,
    state: string,
    zipCode: string,
    country: string
  ): Observable<GoogleLocation> {
    const formattedAddress = `${addressLine1}, ${city}, ${state} ${zipCode}, ${country}`;
    return this.httpClient.get<GoogleLocation>(
      `${this.serviceBase}?address=${formattedAddress}`
    );
  }
}
