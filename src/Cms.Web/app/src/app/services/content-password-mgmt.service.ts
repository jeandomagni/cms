﻿/*
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { AuthService } from "src/app/auth/auth.service";
import { Quote } from "src/app/shared/models/quote/quote.interface";
import { TableOptions } from "src/app/shared/models/table/table-options.model";
import { LegacyBbbInfo } from "src/app/shared/models/bbb/legacy-bbb-info.model";

@Injectable()
export class ContentQuoteService {
  private serviceBase = "/api/contentQuote";

  constructor(private http: HttpClient, private authService: AuthService) {}

  create(quote: Quote): Observable<void> {
    return this.http.put<void>(this.serviceBase, quote);
  }

  delete = (code: string): Observable<void> =>
    this.http.delete<void>(`${this.serviceBase}/${code}`);

  paginate(options: TableOptions): Observable<Quote[]> {
    const locations = this.authService.currentUser.sites.map(
      (loc: LegacyBbbInfo) => loc.legacyBBBID
    );

    return this.http.post<Quote[]>(`${this.serviceBase}/paginate`, {
      ...options,
      locations: locations ? locations.join() : null
    });
  }
}
*/
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { AuthService } from "src/app/auth/auth.service";
import * as userMgmt  from "src/app/shared/models/user-management/user-membership.model";
import {ServiceResponse} from 'src/app/shared/models/user-management/user-membership.model';


@Injectable()
export class PasswordMgmtService {
  private serviceBase = "/api/PasswordManagement";

  constructor(private http: HttpClient, private authService: AuthService) {}

  updatePasswordByEmail(user: userMgmt.addUpdateUserRequest): Observable<ServiceResponse> {
    return this.http.put<ServiceResponse>(`${this.serviceBase}/changePassword`, user);
  }


  resetUserPasswordByEmail(user: userMgmt.addUpdateUserRequest): Observable<ServiceResponse> {
    return this.http.put<ServiceResponse>(`${this.serviceBase}/resetPassword`, user);
  }

}
