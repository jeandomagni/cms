import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { AuthService } from "src/app/auth/auth.service";
import { BaselinePage } from "src/app/shared/models/baseline-pages/baseline-page.interface"
import { BaselinePageTableOptions } from "src/app/shared/models/table/baseline-page-table-options.model";

@Injectable()
export class BaselinePageService {
  private serviceBase = "/api/baselinepages";

  constructor(private http: HttpClient, private authService: AuthService) {}

  create(quote: BaselinePage): Observable<BaselinePage> {
    return this.http.put<BaselinePage>(this.serviceBase, quote);
  }

  delete = (id: number): Observable<void> =>
    this.http.delete<void>(`${this.serviceBase}/${id}`);

  paginate(options: BaselinePageTableOptions): Observable<BaselinePage[]> {
    return this.http.post<BaselinePage[]>(`${this.serviceBase}/paginate`, {
      ...options
    });
  }

  isSegmentUrlExists(urlSegment: string, id: number): Observable<boolean> {
    return this.http.post<boolean>(`${this.serviceBase}/${id == null ? 0 : id}/isSegmentUrlExists`, { urlSegment: urlSegment });
  }
}
