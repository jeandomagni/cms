import { Injectable } from "@angular/core";

import { MetadataPlaceholder } from "src/app/shared/models/metadata/metadata-placeholder.interface";

@Injectable()
export class MetadataService {
  getLabel(type: string): string {
    switch (type) {
      case "business":
        return "Business Profile";
      case "accreditedBusiness":
        return "Accredited Business Profile";
      case "raqAccreditedBusiness":
        return "Accredited Business Profile with RAQ";
      case "accreditedCountry":
        return "Accredited Country Level Metadata";
      case "country":
        return "Country Level Metadata";
      case "accreditedState":
        return "Accredited State Level Metadata";
      case "state":
        return "State Level Metadata";
      case "accreditedCity":
        return "Accredited City Level Metadata";
      case "city":
        return "City Level Metadata";
      default:
        return "";
    }
  }

  getSupportedVariables(
    type: string,
    cultureIds: string[]
  ): MetadataPlaceholder[] {
    const language = cultureIds.includes("es-MX") ? "es" : "en";
    const id = `${type}_${language}`;

    switch (id) {
      case "business_en":
      case "business_es":
        return [
          { variable: "{name}", description: "Business name" },
          { variable: "{category}", description: "Name of primary category" },
          { variable: "{city}", description: "City of business" },
          { variable: "{state}", description: "State of business" }
        ];
      case "accreditedBusiness_en":
      case "raqAccreditedBusiness_en":
      case "accreditedBusiness_es":
      case "raqAccreditedBusiness_es":
        return [
          { variable: "{name}", description: "Business name" },
          { variable: "{category}", description: "Name of primary category" },
          { variable: "{city}", description: "City of business" },
          { variable: "{state}", description: "State of business" },
          { variable: "{date}", description: "Date of accreditation" }
        ];
      case "accreditedCountry_en":
      case "country_en":
        return [
          { variable: "{category}", description: "Name of category" },
          { variable: "{country}", description: "Name of country" }
        ];
      case "accreditedState_en":
      case "state_en":
        return [
          { variable: "{category}", description: "Name of category" },
          { variable: "{country}", description: "Name of country" },
          { variable: "{state}", description: "Name of state or province" }
        ];
      case "accreditedCity_en":
      case "city_en":
        return [
          { variable: "{category}", description: "Name of category" },
          { variable: "{country}", description: "Name of country" },
          { variable: "{state}", description: "Name of state or province" },
          { variable: "{city}", description: "Name of city" }
        ];
      case "accreditedCountry_es":
      case "country_es":
        return [
          { variable: "{categoría}", description: "Name of category" },
          { variable: "{país}", description: "Name of country" }
        ];
      case "accreditedState_es":
      case "state_es":
        return [
          { variable: "{categoría}", description: "Name of category" },
          { variable: "{país}", description: "Name of country" },
          { variable: "{estado}", description: "Name of state or province" }
        ];
      case "accreditedCity_es":
      case "city_es":
        return [
          { variable: "{categoría}", description: "Name of category" },
          { variable: "{país}", description: "Name of country" },
          { variable: "{estado}", description: "Name of state or province" },
          { variable: "{ciudad}", description: "Name of city" }
        ];
      default:
        return null;
    }
  }
}
