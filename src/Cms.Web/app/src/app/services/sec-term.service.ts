﻿import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { SecTerm } from "src/app/shared/models/sec-terms/sec-term.model";
import { GridOptions } from "src/app/shared/models/table/grid-options/grid-options.model";
import { SecTermResults } from "src/app/shared/models/sec-terms/sec-term-results.interface";

@Injectable()
export class SecTermService {
  private serviceBase = "/api/secTerm";

  constructor(private http: HttpClient) {}

  paginate = (options: GridOptions): Observable<SecTermResults> =>
    this.http.post<SecTermResults>(`${this.serviceBase}/paginate`, options);

  getByTobId = (tobId: string): Observable<SecTerm> =>
    this.http.get<SecTerm>(`${this.serviceBase}/${tobId}`);

  update = (secTerm: SecTerm): Observable<SecTerm> =>
    this.http.post<SecTerm>(`${this.serviceBase}/update`, secTerm);
}
