/* eslint-disable @typescript-eslint/camelcase */
import { Injectable } from "@angular/core";
import { Settings } from "tinymce";

// Existing Settings type is missing this... bastards.
// Source: https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/types/tinymce/index.d.ts
// Didn't want to edit the package file located at \Cms.Web\app\node_modules\@types\tinymce\index.d.ts
// since it could get overwritten.
interface ExtendedSettings extends Settings {
  rel_list?: Array<{}>;
  default_link_target?: string;
}

@Injectable()
export class TinymceService {
  adminSettings: ExtendedSettings = {
    ...this.getSharedSettings(),
    allow_unsafe_link_target: true, // WEB-3262 stops 'noopener noreferrer' from being appended to nofollow links
    rel_list: [
      { title: "Default", value: "" },
      { title: "No Follow", value: "nofollow" },
      { title: "Sponsored", value: "sponsored" },
      { title: "User Generated Content", value: "ugc" }
    ],
    default_link_target: "_blank"
  };

  globalSettings: ExtendedSettings = {
    ...this.getSharedSettings(),
    default_link_target: "_blank"
  };

  key = "8go91yuj78rvq9zg7m7749matj77c09xi1m1jkcdbxik9yv8";

  private getSharedSettings(): Settings {
    return {
      inline: false,
      plugins: "lists table link wordcount paste",
      skin: "oxide",
      theme: "silver",
      height: 800,
      toolbar:
        "bold italic underline" +
        " | " +
        "alignleft aligncenter alignright alignjustify" +
        " | " +
        "bullist numlist indent outdent link table" +
        " | " +
        "formatselect removeformat" +
        " | " +
        "undo redo",
      content_style: "body { font-size: 12px !important; }",
      menubar: ""
    };
  }
}
