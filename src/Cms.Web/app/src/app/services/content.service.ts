﻿import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { ArticleOrderParams } from "src/app/shared/models/article-order-params.model";
import { UpdateArticleOrderParams } from "src/app/shared/models/update-article-order-params.model";
import { AuditLog } from "src/app/shared/models/audit-log.interface";
import { Content } from "src/app/shared/models/content/content.model";
import { ContentPreview } from "src/app/shared/models/content/content-preview.interface";
import { ArticlesByLocation } from "src/app/shared/models/content/list-children/articles-by-location.interface";
import { ContentQuery } from "src/app/shared/models/content/query/content-query.model";

@Injectable()
export class ContentService {
  private serviceBase = "/api/content";

  constructor(private http: HttpClient) {}

  getByCode = (code: string): Observable<Content> =>
    this.http.get<Content>(`${this.serviceBase}/${code}`);

  getAuditLogs = (
    id: number,
    page: number,
    pageSize: number
  ): Observable<AuditLog[]> =>
    this.http.get<AuditLog[]>(
      `${this.serviceBase}/audit/${id}/${page || 1}/${pageSize || 10}`
    );

  create(content: Content): Observable<string> {
    const options: Record<string, any> = { responseType: "text" };
    return this.http.put<string>(this.serviceBase, content, options);
  }

  update = (content: Content): Observable<Content> =>
    this.http.post<Content>(this.serviceBase, content);

  delete = (id: number): Observable<void> =>
    this.http.delete<void>(`${this.serviceBase}/${id}`);

  removeTopic = (code: string, topic: string): Observable<void> =>
    this.http.delete<void>(`${this.serviceBase}/${code}/removeTopic/${topic}`);

  paginate = (options: ContentQuery): Observable<ContentPreview[]> =>
    this.http.post<ContentPreview[]>(`${this.serviceBase}/paginate`, options);

  publish = (content: Content): Observable<Content> =>
    this.http.post<Content>(`${this.serviceBase}/publish`, content);

  unpublish = (code: string): Observable<Content> =>
    this.http.post<Content>(`${this.serviceBase}/unpublish/${code}`, null);

  paginateListChildren = (
    orderParams: ArticleOrderParams
  ): Observable<ArticlesByLocation> =>
    this.http.post<ArticlesByLocation>(
      `${this.serviceBase}/paginateListChildren`,
      orderParams
    );

  checkOut = (code: string): Observable<Content> =>
    this.http.post<Content>(`${this.serviceBase}/checkOut/${code}`, null);

  checkIn = (code: string): Observable<Content> =>
    this.http.post<Content>(`${this.serviceBase}/checkIn/${code}`, null);

  updateOrder = (updateParams: UpdateArticleOrderParams): Observable<number> =>
    this.http.post<number>(
      `${this.serviceBase}/updateArticleOrder`,
      updateParams);

  switchToServiceAreaNewsfeed = (updateParams: ArticleOrderParams) =>
    this.http.post<boolean>(`${this.serviceBase}/switchToServiceAreaNewsfeed`, updateParams);


  switchToCountryNewsfeed = (updateParams: ArticleOrderParams) =>
    this.http.post<boolean>(`${this.serviceBase}/switchToCountryNewsfeed`, updateParams);

  clearNewsfeedOverride = (updateParams: ArticleOrderParams) =>
    this.http.post<boolean>(`${this.serviceBase}/clearNewsfeedOverride`, updateParams);
}
