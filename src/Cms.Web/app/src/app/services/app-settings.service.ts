import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

@Injectable()
export class AppSettingsService {
  private serviceBase = "/api/appSettings";

  constructor(private http: HttpClient) {}

  getConnectionString = (key: any): Observable<any> =>
    this.http.get(`${this.serviceBase}/connectionString/${key}`);

  getGeneralSetting = (key: any): Observable<any> =>
    this.http.get(`${this.serviceBase}/generalSetting/${key}`);
}
