﻿import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { Media } from "src/app/shared/models/content/entities/media.model";

@Injectable()
export class ContentMediaService {
  private serviceBase = "/api/contentmedia";

  constructor(private http: HttpClient) {}

  getByContentCode = (code: string): Observable<Media[]> =>
    this.http.get<Media[]>(`${this.serviceBase}/contentcode/${code}`);

  create = (media: Media): Observable<Media> =>
    this.http.put<Media>(this.serviceBase, media);

  delete = (contentCode: string, mediaContentId: number): Observable<void> =>
    this.http.delete<void>(
      `${this.serviceBase}/${contentCode}/${mediaContentId}`
    );

  reorder = (contentCode: string, mediaList: Media[]): Observable<void> =>
    this.http.post<void>(`${this.serviceBase}/${contentCode}`, mediaList);

  update = (media: Media): Observable<void> =>
    this.http.post<void>(`${this.serviceBase}/update`, media);
}
