import { MetadataService } from "./metadata.service";

describe("MetadataService", () => {
  let service: MetadataService;
  beforeEach(() => {
    service = new MetadataService();
  });

  it("#getLabel should return correct business label", () => {
    expect(service.getLabel("business")).toBe("Business Profile");
  });

  it("#getLabel should return empty string for invalid type", () => {
    expect(service.getLabel("super-invalid-type")).toBe("");
  });
});
