﻿import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class ServiceAreaService {
  private serviceBase = '/api/serviceArea';

  constructor(private http: HttpClient) {}

  getCountriesForUser(): Observable<string[]> {
    return this.http.get<string[]>(`${this.serviceBase}/countries`);
  }

  getStatesForUser(): Observable<string[]> {
    return this.http.get<string[]>(`${this.serviceBase}/states`);
  }

  getFilteredStatesForUser(countries: string[]) {
    return this.http.get<string[]>(`${this.serviceBase}/states/${countries.join()}`);
  }
}
