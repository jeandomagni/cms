import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { FileTag } from "src/app/shared/models/file/file-tag.interface";
import { BaselinePage } from '../../shared/models/baseline-pages/baseline-page.interface';

@Injectable()
export class BaselinePageValidationService
{
    validateBaselinePage(baselinePage: BaselinePage): string[] {
        const errors = [];
    
        if(baselinePage.urlSegment.indexOf("/") > -1)
        {
            errors.push("No nested directories allowed in URL field");
        }
    
        return errors;
      }
}