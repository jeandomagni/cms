﻿import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { QuoteTag } from "src/app/shared/models/quote/quote-tag.interface";

@Injectable()
export class QuoteTagService {
  private serviceBase = "/api/quoteTag";

  constructor(private http: HttpClient) {}

  suggest = (query: string, filter: string): Observable<QuoteTag[]> =>
    this.http.get<QuoteTag[]>(
      `${this.serviceBase}/suggest?input=${query}&filter=${filter}`
    );
}
