import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { FileTag } from "src/app/shared/models/file/file-tag.interface";
import { Shortlink } from '../../shared/models/baseline-pages/shortlink.interface';

@Injectable()
export class ShortlinkValidationService
{
    validateShortlink(shortlink: Shortlink): string[] {
        const errors = [];
    
        if(shortlink.urlSegment.indexOf("/") > -1)
        {
            errors.push("No nested directories allowed in URL field");
        }

        if(shortlink.urlSegment == shortlink.redirectUrl) {
            errors.push("Url Segment should be different from Redirect Url");
        }
    
        return errors;
      }
}