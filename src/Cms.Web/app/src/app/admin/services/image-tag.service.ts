﻿import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { FileTag } from "src/app/shared/models/file/file-tag.interface";

@Injectable()
export class ImageTagService {
  private serviceBase = "/api/imageTag";

  constructor(private http: HttpClient) {}

  suggest = (query: string, filter: string): Observable<FileTag[]> =>
    this.http.get<FileTag[]>(
      `${this.serviceBase}/suggest?input=${query}&filter=${filter}`
    );
}
