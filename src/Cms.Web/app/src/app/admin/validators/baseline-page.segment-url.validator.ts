import { Directive, forwardRef, Injectable, Input } from '@angular/core';
import {
  AsyncValidator,
  AbstractControl,
  NG_ASYNC_VALIDATORS,
  ValidationErrors
} from '@angular/forms';
import { catchError, map } from 'rxjs/operators';
import { BaselinePageService } from '../../services/baselinePage.service';
import { Observable, of } from 'rxjs';
import { BaselinePage } from '../../shared/models/baseline-pages/baseline-page.interface';
import { timer } from 'rxjs';
import { mapTo, switchMap } from 'rxjs/operators';  

@Injectable({ providedIn: 'root' })
export class SegmentUrlExistsValidator implements AsyncValidator {
  constructor(private baselinePageService: BaselinePageService) {}

  validate(
    ctrl: AbstractControl
  ): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> {
        var id = ctrl.parent.get("id").value;
    return timer(500).pipe(
      switchMap(() => this.baselinePageService.isSegmentUrlExists(ctrl.value, id).pipe(
        map(isExist => (isExist ? { uniqueBaselinePageSegmentUrl: true } : null)),
        catchError(() => of(null))
      )
      )
    );
  }
}

@Directive({
    selector: '[uniqueBaselinePageSegmentUrl]',
    providers: [
      {
        provide: NG_ASYNC_VALIDATORS,
        useExisting: forwardRef(() => SegmentUrlExistsValidator),
        multi: true
      }
    ]
  })
  export class UniqueBaselinePageSegmentUrlDirective {
    constructor(private validator: SegmentUrlExistsValidator) {}
  
    validate(control: AbstractControl) {
      this.validator.validate(control);
    }
  }