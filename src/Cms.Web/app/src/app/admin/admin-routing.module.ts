import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { AdminComponent } from "src/app/admin/admin.component";
import { EditProfileSeoComponent } from "src/app/admin/edit-profile-seo/edit-profile-seo.component";
import { EditSecTermComponent } from "src/app/admin/edit-sec-term/edit-sec-term.component";

const routes: Routes = [
  {
    path: "manage",
    component: AdminComponent
  },
  {
    path: "editProfileSeo",
    component: EditProfileSeoComponent
  },
  {
    path: "editSecTerm",
    component: EditSecTermComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {}
