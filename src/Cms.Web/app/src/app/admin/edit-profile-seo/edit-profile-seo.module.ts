import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FlexLayoutModule } from "@angular/flex-layout";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {
  MatTabsModule,
  MatIconModule,
  MatCardModule,
  MatAutocompleteModule,
  MatFormFieldModule,
  MatButtonModule,
  MatInputModule,
  MatSelectModule,
  MatTableModule,
  MatSortModule,
  MatChipsModule,
  MatPaginatorModule,
  MatListModule,
  MatToolbarModule,
  MatDialogModule,
  MatRadioModule,
  MatCheckboxModule
} from "@angular/material";

import { EditProfileSeoComponent } from "src/app/admin/edit-profile-seo/edit-profile-seo.component";
import { BusinessProfileService } from "src/app/services/business-profile.service";
import { SharedModule } from "src/app/shared/shared.module";
import { MetadataService } from "src/app/services/metadata.service";

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    FormsModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDialogModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatPaginatorModule,
    MatRadioModule,
    MatSelectModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    ReactiveFormsModule,
    SharedModule
  ],
  declarations: [EditProfileSeoComponent],
  entryComponents: [],
  providers: [BusinessProfileService, MetadataService]
})
export class EditProfileSeoModule {}
