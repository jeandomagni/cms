import { Component, EventEmitter, OnInit, OnDestroy } from "@angular/core";
import { ActivatedRoute, Params } from "@angular/router";
import { takeUntil } from "rxjs/operators";

import { BusinessProfileService } from "src/app/services/business-profile.service";
import { BusinessProfile } from "src/app/shared/models/business-profile/business-profile.model";
import { MetadataService } from "src/app/services/metadata.service";
import { MetadataPlaceholder } from "src/app/shared/models/metadata/metadata-placeholder.interface";
import { NotificationService } from "src/app/services/notification.service";

@Component({
  selector: "edit-profile-seo",
  templateUrl: "./edit-profile-seo.component.html"
})
export class EditProfileSeoComponent implements OnInit, OnDestroy {
  activeLink = "General";
  links = ["General"];
  metadataLabel: string;
  profile: BusinessProfile = new BusinessProfile();
  showInfoPanel: boolean;
  supportedVariables: MetadataPlaceholder[];
  private onDestroy: EventEmitter<void> = new EventEmitter();

  constructor(
    private businessProfileService: BusinessProfileService,
    private metadataService: MetadataService,
    private notificationService: NotificationService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.queryParams
      .pipe(takeUntil(this.onDestroy))
      .subscribe((queryParams: Params) => {
        const { id } = queryParams;
        this.businessProfileService
          .getById(id)
          .pipe(takeUntil(this.onDestroy))
          .subscribe((data: BusinessProfile) => {
            this.profile = data;
            this.supportedVariables = this.metadataService.getSupportedVariables(
              data.metadata.type,
              data.metadata.cultureIds
            );
            this.metadataLabel = this.metadataService.getLabel(
              data.metadata.type
            );
          });
      });
  }

  ngOnDestroy(): void {
    this.onDestroy.next();
  }

  save(): void {
    const validations = this.validateForPublish();

    if (validations.length > 0) {
      this.notificationService.warningList(validations);
      return;
    }

    this.businessProfileService
      .update(this.profile)
      .pipe(takeUntil(this.onDestroy))
      .subscribe(() => {
        this.notificationService.success("Updated profile SEO");
      });
  }

  togglePlaceholderInfo(): void {
    this.showInfoPanel = !this.showInfoPanel;
  }

  private validateForPublish(): string[] {
    const ret = [];

    if (!this.profile.metadata.pageTitle) {
      ret.push("A page title is required");
    }

    if (!this.profile.metadata.metaDesc) {
      ret.push("A meta description is required");
    }

    return ret;
  }
}
