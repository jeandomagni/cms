import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FlexLayoutModule } from "@angular/flex-layout";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {
  MatTabsModule,
  MatIconModule,
  MatCardModule,
  MatAutocompleteModule,
  MatFormFieldModule,
  MatButtonModule,
  MatInputModule,
  MatSelectModule,
  MatTableModule,
  MatSortModule,
  MatChipsModule,
  MatPaginatorModule,
  MatListModule,
  MatToolbarModule,
  MatDialogModule,
  MatRadioModule,
  MatCheckboxModule
} from "@angular/material";

import { EditSecTermComponent } from "src/app/admin/edit-sec-term/edit-sec-term.component";
import { SecTermInfoComponent } from "src/app/admin/edit-sec-term/sections/sec-term-info.component";
import { EditAliasDialogComponent } from "src/app/admin/edit-sec-term/tabs/general-tab/edit-alias-dialog.component";
import { GeneralTabComponent } from "src/app/admin/edit-sec-term/tabs/general-tab/general-tab.component";
import { MetadataTabComponent } from "src/app/admin/edit-sec-term/tabs/metadata-tab/metadata-tab.component";
import { MetadataBusinessTabComponent } from "src/app/admin/edit-sec-term/tabs/metadata-business-tab/metadata-business-tab.component";
import { TobAliasService } from "src/app/services/tob-alias.service";
import { SharedModule } from "src/app/shared/shared.module";

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    FormsModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDialogModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatPaginatorModule,
    MatRadioModule,
    MatSelectModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    ReactiveFormsModule,
    SharedModule
  ],
  declarations: [
    EditAliasDialogComponent,
    EditSecTermComponent,
    GeneralTabComponent,
    MetadataTabComponent,
    MetadataBusinessTabComponent,
    SecTermInfoComponent
  ],
  entryComponents: [
    EditAliasDialogComponent,
    GeneralTabComponent,
    MetadataTabComponent,
    MetadataBusinessTabComponent,
    SecTermInfoComponent
  ],
  providers: [TobAliasService]
})
export class EditSecTermModule {}
