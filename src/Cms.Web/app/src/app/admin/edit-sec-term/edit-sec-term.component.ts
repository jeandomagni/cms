import { Component, EventEmitter, OnInit, OnDestroy } from "@angular/core";
import { ActivatedRoute, Params } from "@angular/router";
import { takeUntil } from "rxjs/operators";

import {
  DefaultNavigationItem,
  Links
} from "src/app/admin/edit-sec-term/constants/navigation.constants";
import { SecTermService } from "src/app/services/sec-term.service";
import { SecTerm } from "src/app/shared/models/sec-terms/sec-term.model";

@Component({
  selector: "edit-sec-term",
  templateUrl: "./edit-sec-term.component.html"
})
export class EditSecTermComponent implements OnInit, OnDestroy {
  activeLink = DefaultNavigationItem;
  links = Links;
  secTerm: SecTerm = new SecTerm();
  private onDestroy: EventEmitter<void> = new EventEmitter();

  constructor(
    private route: ActivatedRoute,
    private secTermService: SecTermService
  ) {}

  ngOnInit(): void {
    this.refresh();
  }

  ngOnDestroy(): void {
    this.onDestroy.next();
  }

  refresh(): void {
    this.route.queryParams
      .pipe(takeUntil(this.onDestroy))
      .subscribe((queryParams: Params) => {
        const { tobId } = queryParams;
        return this.secTermService
          .getByTobId(tobId)
          .pipe(takeUntil(this.onDestroy))
          .subscribe((data: SecTerm) => {
            this.secTerm = data;
          });
      });
  }
}
