import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnDestroy
} from "@angular/core";
import { takeUntil } from "rxjs/operators";

import { SecTerm } from "src/app/shared/models/sec-terms/sec-term.model";
import { NotificationService } from "src/app/services/notification.service";
import { SecTermService } from "src/app/services/sec-term.service";

@Component({
  selector: "est-sec-term-info",
  templateUrl: "./sec-term-info.component.html"
})
export class SecTermInfoComponent implements OnDestroy {
  @Input() secTerm: SecTerm;
  @Output() secTermChange: EventEmitter<SecTerm> = new EventEmitter();

  private onDestroy: EventEmitter<void> = new EventEmitter();

  constructor(
    private notificationService: NotificationService,
    private secTermService: SecTermService
  ) {}

  ngOnDestroy(): void {
    this.onDestroy.next();
  }

  update(): void {
    const warnings = this.validate();
    if (warnings.length > 0) {
      this.notificationService.warningList(warnings);
      return;
    }

    this.secTermService
      .update(this.secTerm)
      .pipe(takeUntil(this.onDestroy))
      .subscribe((data: SecTerm) => {
        this.secTermChange.emit(data);
        this.notificationService.success("Updated SEC term");
      });
  }

  private validate(): string[] {
    const warnings = [];

    this.secTerm.metadata.forEach(metadata => {
      if (!metadata.pageTitle || !metadata.metaDesc) {
        warnings.push("All metadata fields are required");
      }
    });

    return warnings;
  }
}
