import { ENTER } from "@angular/cdk/keycodes";
import {
  Component,
  Input,
  OnChanges,
  EventEmitter,
  OnDestroy,
  Output
} from "@angular/core";
import { FormControl } from "@angular/forms";
import { MatDialog, MatChipInputEvent } from "@angular/material";
import { takeUntil } from "rxjs/operators";

import { EditAliasDialogComponent } from "src/app/admin/edit-sec-term/tabs/general-tab/edit-alias-dialog.component";
import { ConfirmDialogService } from "src/app/services/confirm-dialog.service";
import { NotificationService } from "src/app/services/notification.service";
import { TobAliasService } from "src/app/services/tob-alias.service";
import { SecTerm } from "src/app/shared/models/sec-terms/sec-term.model";
import { Alias } from "src/app/shared/models/sec-terms/alias.interface";

@Component({
  selector: "est-general-tab",
  templateUrl: "./general-tab.component.html"
})
export class GeneralTabComponent implements OnChanges, OnDestroy {
  @Output() refresh: EventEmitter<void> = new EventEmitter();
  @Input() secTerm: SecTerm;

  addOnBlur = false;
  aliasesEn: Alias[];
  aliasesEs: Alias[];
  aliasEnCtrl = new FormControl();
  aliasEsCtrl = new FormControl();
  separatorKeysCodes: number[] = [ENTER];
  private onDestroy: EventEmitter<void> = new EventEmitter();

  constructor(
    private confirmDialogService: ConfirmDialogService,
    private dialog: MatDialog,
    private notificationService: NotificationService,
    private tobAliasService: TobAliasService
  ) {}

  ngOnChanges(): void {
    this.aliasesEn = this.secTerm.aliases.filter(x => x.name);
    this.aliasesEs = this.secTerm.aliases.filter(x => x.spanish);
  }

  ngOnDestroy(): void {
    this.onDestroy.next();
  }

  addAliasEn(event: MatChipInputEvent): void {
    const { input } = event;
    const { value } = event;

    if (!value) {
      return;
    }

    const alias: Alias = {
      name: value.trim(),
      spanish: null
    };

    const dialogData = { alias };
    const dialogRef = this.dialog.open(EditAliasDialogComponent, {
      data: dialogData
    });

    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.onDestroy))
      .subscribe((updatedAlias: Alias) => {
        if (updatedAlias) {
          this.tobAliasService
            .add({ alias: updatedAlias, secTerm: this.secTerm })
            .pipe(takeUntil(this.onDestroy))
            .subscribe(() => {
              this.refresh.emit();
              this.notificationService.success("Added alias");
            });
        }
      });

    if (input) {
      input.value = "";
    }

    this.aliasEnCtrl.setValue(null);
  }

  addAliasEs(event: MatChipInputEvent): void {
    const { input } = event;
    const { value } = event;

    if (!value) {
      return;
    }

    const alias: Alias = {
      name: null,
      spanish: value.trim()
    };

    const dialogData = { alias };
    const dialogRef = this.dialog.open(EditAliasDialogComponent, {
      data: dialogData
    });

    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.onDestroy))
      .subscribe((updatedAlias: Alias) => {
        if (updatedAlias) {
          this.tobAliasService
            .add({ alias: updatedAlias, secTerm: this.secTerm })
            .pipe(takeUntil(this.onDestroy))
            .subscribe(() => {
              this.refresh.emit();
              this.notificationService.success("Added alias");
            });
        }
      });

    if (input) {
      input.value = "";
    }

    this.aliasEsCtrl.setValue(null);
  }

  editAlias(alias: Alias): void {
    const dialogData = { alias };
    const dialogRef = this.dialog.open(EditAliasDialogComponent, {
      data: dialogData
    });

    dialogRef.afterClosed().subscribe((updatedAlias: Alias) => {
      if (updatedAlias) {
        this.tobAliasService
          .update({ alias: updatedAlias, secTerm: this.secTerm })
          .pipe(takeUntil(this.onDestroy))
          .subscribe(() => {
            this.refresh.emit();
            this.notificationService.success("Saved alias");
          });
      }
    });
  }

  removeAlias(alias: Alias): void {
    this.confirmDialogService
      .showConfirmCustom(
        "Are you sure you want to remove this alias?",
        "Remove alias"
      )
      .pipe(takeUntil(this.onDestroy))
      .subscribe(confirmed => {
        if (confirmed) {
          this.tobAliasService
            .delete({ alias, secTerm: this.secTerm })
            .pipe(takeUntil(this.onDestroy))
            .subscribe(() => {
              this.refresh.emit();
              this.notificationService.success("Alias removed");
            });
        }
      });
  }
}
