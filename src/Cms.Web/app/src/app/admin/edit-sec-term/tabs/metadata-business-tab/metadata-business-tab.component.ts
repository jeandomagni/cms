import {
  Component,
  Input,
  OnChanges,
  EventEmitter,
  OnDestroy
} from "@angular/core";

import { MetadataService } from "src/app/services/metadata.service";
import { Metadata } from "src/app/shared/models/metadata/metadata.model";
import { MetadataPlaceholder } from "src/app/shared/models/metadata/metadata-placeholder.interface";
import { SecTerm } from "src/app/shared/models/sec-terms/sec-term.model";

@Component({
  selector: "est-metadata-business-tab",
  templateUrl: "./metadata-business-tab.component.html"
})
export class MetadataBusinessTabComponent implements OnChanges, OnDestroy {
  @Input() secTerm: SecTerm;

  metadataEn: Metadata[];
  metadataEs: Metadata[];
  showInfoPanel = new Set();

  private onDestroy: EventEmitter<void> = new EventEmitter();

  constructor(private metadataService: MetadataService) {}

  ngOnChanges(): void {
    this.metadataEn = this.secTerm.metadataBusiness.filter(metadata =>
      metadata.cultureIds.includes("en-US")
    );
    this.metadataEs = this.secTerm.metadataBusiness.filter(metadata =>
      metadata.cultureIds.includes("es-MX")
    );
  }

  ngOnDestroy(): void {
    this.onDestroy.next();
  }

  getLabel = (type: string): string => this.metadataService.getLabel(type);

  getShowInfoPanel = (type: string): boolean => this.showInfoPanel[type];

  getSupportedVariables = (metadata: Metadata): MetadataPlaceholder[] =>
    this.metadataService.getSupportedVariables(
      metadata.type,
      metadata.cultureIds
    );

  togglePlaceholderInfo(type: string): void {
    this.showInfoPanel[type] = !this.showInfoPanel[type];
  }
}
