import { Alias } from "src/app/shared/models/sec-terms/alias.interface";

export interface EditAliasDialogData {
  alias: Alias;
}
