import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";

import { EditAliasDialogData } from "src/app/admin/edit-sec-term/tabs/general-tab/edit-alias-dialog-data.interface";

@Component({
  selector: "edit-alias-dialog",
  templateUrl: "./edit-alias-dialog.component.html"
})
export class EditAliasDialogComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: EditAliasDialogData,
    private dialogRef: MatDialogRef<EditAliasDialogComponent>
  ) {}

  update(): void {
    this.data.alias.name = this.data.alias.name
      ? this.data.alias.name.toLowerCase()
      : null;
    this.data.alias.spanish = this.data.alias.spanish
      ? this.data.alias.spanish.toLowerCase()
      : null;
    this.dialogRef.close(this.data.alias);
  }
}
