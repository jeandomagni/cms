﻿import { Component } from '@angular/core';

import {
  Links,
  DefaultNavigationItem
} from 'src/app/admin/constants/navigation.constants';
import {AuthService} from '../auth/auth.service';

@Component({
  selector: 'admin',
  templateUrl: './admin.component.html'
})
export class AdminComponent {
  activeLink = DefaultNavigationItem;
  links = [];
  isAdmin = false;
  constructor(authService: AuthService) {

    for (let index = 0; index < Links.length; index++) {
      const label = Links[index];
      if ((label === 'Users' && !authService.isGlobalAdmin) || (label === 'Baseline' && !authService.isGlobalAdmin)) {
        continue;
      }
      this.links.push(label);
    }
  }

}
