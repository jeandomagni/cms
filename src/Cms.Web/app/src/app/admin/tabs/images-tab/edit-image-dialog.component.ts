import { ENTER } from "@angular/cdk/keycodes";
import { Component, Inject, ElementRef, ViewChild } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material";
import { Observable } from "rxjs";

import { ImageTagService } from "src/app/admin/services/image-tag.service";
import {
  TagFilters,
  DefaultTagFilter
} from "src/app/constants/tag-filters.constants";
import { FileTag } from "src/app/shared/models/file/file-tag.interface";
import { NotificationService } from "src/app/services/notification.service";
import { EditImageDialogData } from "src/app/admin/tabs/images-tab/edit-image-dialog-data.interface";
import { BaseAutocompleteComponent } from "src/app/components/base/base-autocomplete/base-autocomplete.component";

@Component({
  selector: "edit-image-dialog",
  templateUrl: "./edit-image-dialog.component.html"
})
export class EditImageDialogComponent extends BaseAutocompleteComponent<
  FileTag
> {
  addOnBlur = false;
  notFoundMessage = "No matching tags found. Press enter to add new.";
  removable = true;
  selectable = true;
  separatorKeysCodes: number[] = [ENTER];
  tagFilter = DefaultTagFilter;
  tagFilters = TagFilters;
  @ViewChild('tagsInput', {static: false}) tagsInput:ElementRef<HTMLInputElement>; 

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: EditImageDialogData,
    private dialogRef: MatDialogRef<EditImageDialogComponent>,
    private imageTagService: ImageTagService,
    private notificationService: NotificationService
  ) {
    super();
  }

  addTag(value: string): void {
    this.clearTag();  
    if (!value) {
      return;
    }

    var lowercaseValue = value.toLowerCase();

    if (
      this.data.image.tagItems &&
      this.data.image.tagItems.find((x: FileTag) => x.name === lowercaseValue)
    ) {
      this.notificationService.error(
        `${lowercaseValue} is already tagged on this image`
      );
      return;
    }

    this.data.image.tagItems = this.data.image.tagItems || [];
    this.data.image.tagItems.push({
      name: lowercaseValue,
      type: "generic",
      label: `${lowercaseValue} :: generic`
    });  
  }

  clearTag() {
    this.acCtrl.setValue("");
    this.tagsInput.nativeElement.value = "";
  }

  removeTag(tag: FileTag): void {
    this.data.image.tagItems = this.data.image.tagItems.filter(
      (x: FileTag) => x !== tag
    );
  }

  selectedTag(tag: FileTag): void {
    this.clearTag();

    if (this.data.image.tagItems && this.data.image.tagItems.filter(x => x.value == tag.value).length  > 0) {
      this.notificationService.error(
        `${tag.name} is already tagged on this image`
      );
      return;
    }

    tag.label = tag.label;
    this.data.image.tagItems.push(tag);
  }

  update(): void {
    this.data.image.tags = JSON.stringify(this.data.image.tagItems);

    this.dialogRef.close(this.data.image);
  }

  protected queryItems(value: string): Observable<FileTag[]> {
    return this.imageTagService.suggest(value, this.tagFilter);
  }
}
