import { Component, EventEmitter, ViewChild } from "@angular/core";
import { MatDialog } from "@angular/material";
import { takeUntil } from "rxjs/operators";

import { EditImageDialogComponent } from "src/app/admin/tabs/images-tab/edit-image-dialog.component";
import { ImageSearchComponent } from "src/app/components/image-search/image-search.component";
import { ConfirmDialogService } from "src/app/services/confirm-dialog.service";
import { FileService } from "src/app/services/file.service";
import { NotificationService } from "src/app/services/notification.service";
import { File } from "src/app/shared/models/file/file.model";

@Component({
  selector: "admin-images-tab",
  templateUrl: "./images-tab.component.html"
})
export class ImagesTabComponent {
  @ViewChild(ImageSearchComponent, { static: false })
  imageSearchComponent: ImageSearchComponent;

  images: File[] = [];
  private onDestroy: EventEmitter<void> = new EventEmitter();

  constructor(
    private confirmDialogService: ConfirmDialogService,
    private fileService: FileService,
    private dialog: MatDialog,
    private notificationService: NotificationService
  ) {}

  ngOnDestroy(): void {
    this.onDestroy.next();
  }

  deleteImageByCode(code: string): void {
    this.confirmDialogService
      .showConfirmDelete()
      .pipe(takeUntil(this.onDestroy))
      .subscribe(confirmed => {
        if (confirmed) {
          this.fileService.delete(code).subscribe(() => {
            this.imageSearchComponent.refreshContent();
            this.notificationService.success("Deleted image");
          });
        }
      });
  }

  editImage(image: File): void {
    const dialogData = { image };
    const dialogRef = this.dialog.open(EditImageDialogComponent, {
      data: dialogData
    });

    dialogRef.afterClosed().subscribe((updatedImage: File) => {
      if (!updatedImage) {
        return;
      }

      this.fileService.update(updatedImage).subscribe(() => {
        this.imageSearchComponent.refreshContent();
        this.notificationService.success("Saved image");
      });
    });
  }
}
