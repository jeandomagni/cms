import { File } from "src/app/shared/models/file/file.model";

export interface EditImageDialogData {
  image: File;
}
