import { Quote } from "src/app/shared/models/quote/quote.interface";

export interface EditQuoteDialogData {
  quote: Quote;
}
