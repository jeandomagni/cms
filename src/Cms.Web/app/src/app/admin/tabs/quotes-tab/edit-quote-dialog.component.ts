import { ENTER } from "@angular/cdk/keycodes";
import {
  Component,
  Inject,
  ViewChild,
  ElementRef,
  OnInit,
} from "@angular/core";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
  MatChipInputEvent,
} from "@angular/material";
import { Observable } from "rxjs";

import { QuoteTagService } from "src/app/admin/services/quote-tag.service";
import {
  TagFilters,
  DefaultTagFilter,
} from "src/app/constants/tag-filters.constants";
import { QuoteTag } from "src/app/shared/models/quote/quote-tag.interface";
import { NotificationService } from "src/app/services/notification.service";
import { EditQuoteDialogData } from "src/app/admin/tabs/quotes-tab/edit-quote-dialog-data.interface";
import { BaseAutocompleteComponent } from "src/app/components/base/base-autocomplete/base-autocomplete.component";

@Component({
  selector: "edit-quote-dialog",
  templateUrl: "./edit-quote-dialog.component.html",
})
export class EditQuoteDialogComponent
  extends BaseAutocompleteComponent<QuoteTag>
  implements OnInit {
  addOnBlur = false;
  notFoundMessage = "No matching tags found. Press enter to add new.";
  removable = true;
  selectable = true;
  separatorKeysCodes: number[] = [ENTER];
  tagFilter = DefaultTagFilter;
  tagFilters = TagFilters;

  @ViewChild("tagInput", { static: false }) tagInput: ElementRef<
    HTMLInputElement
  >;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: EditQuoteDialogData,
    private dialogRef: MatDialogRef<EditQuoteDialogComponent>,
    private notificationService: NotificationService,
    private quoteTagService: QuoteTagService
  ) {
    super();
  }

  ngOnInit(): void {
    this.data.quote.tagItems = this.data.quote.tagItems || [];
  }

  addTag(event: MatChipInputEvent): void {
    const { input, value } = event;

    if (!value) {
      return;
    }

    if (
      this.data.quote.tagItems &&
      this.data.quote.tagItems.find((x: QuoteTag) => x.name === value)
    ) {
      this.notificationService.error(
        `${value} is already tagged on this quote`
      );
      return;
    }

    this.data.quote.tagItems.push({
      name: value,
      value: value.toLowerCase(),
      type: "generic",
    });

    this.acCtrl.setValue(null);

    // Reset the input value
    if (input) {
      input.value = "";
    }
  }

  removeTag(tag: QuoteTag): void {
    this.data.quote.tagItems = this.data.quote.tagItems.filter(
      (x: QuoteTag) => x !== tag
    );
  }

  selectedTag(tag: QuoteTag): void {
    if (this.data.quote.tagItems.find(item => item.value === tag.value)) {
      return;
    }

    this.data.quote.tagItems.push(tag);

    if (tag.associatedTags) {
      tag.associatedTags.forEach((x) => {
        if (this.data.quote.tagItems.find(item => item.value === x.value)) {
          return;
        }
        this.data.quote.tagItems.push(x);
      });
    }

    this.tagInput.nativeElement.value = "";
    this.acCtrl.setValue(null);
  }

  update(): void {
    this.data.quote.tags = JSON.stringify(this.data.quote.tagItems);
    this.dialogRef.close(this.data.quote);
  }

  protected queryItems(value: string): Observable<QuoteTag[]> {
    const suggestions = this.quoteTagService.suggest(value, this.tagFilter);
    return suggestions;
  }
}
