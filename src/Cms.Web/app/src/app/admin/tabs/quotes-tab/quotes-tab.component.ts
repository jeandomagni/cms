import { SelectionModel } from "@angular/cdk/collections";
import {
  Component,
  EventEmitter,
  ViewChild,
  AfterViewInit,
  OnDestroy
} from "@angular/core";
import {
  MatDialog,
  MatSort,
  MatTableDataSource,
  MatPaginator
} from "@angular/material";
import { merge, of } from "rxjs";
import {
  takeUntil,
  startWith,
  switchMap,
  map,
  catchError
} from "rxjs/operators";

import { EditQuoteDialogComponent } from "src/app/admin/tabs/quotes-tab/edit-quote-dialog.component";
import { ContentQuoteService } from "src/app/services/content-quote.service";
import { ConfirmDialogService } from "src/app/services/confirm-dialog.service";
import { NotificationService } from "src/app/services/notification.service";
import { Quote } from "src/app/shared/models/quote/quote.interface";
import { TableOptions } from "src/app/shared/models/table/table-options.model";

@Component({
  selector: "admin-quotes-tab",
  templateUrl: "./quotes-tab.component.html"
})
export class QuotesTabComponent implements AfterViewInit, OnDestroy {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  displayedColumns: string[] = [
    "select",
    "firstName",
    "lastName",
    "text",
    "tags",
    "published"
  ];
  dataSource: MatTableDataSource<Quote> = new MatTableDataSource();
  filter: EventEmitter<void> = new EventEmitter();
  options = new TableOptions();
  resultsLength: number;
  selected: SelectionModel<Quote> = new SelectionModel<Quote>(false, []);
  updateQuotes: EventEmitter<void> = new EventEmitter();
  private onDestroy: EventEmitter<void> = new EventEmitter();

  constructor(
    private confirmDialogService: ConfirmDialogService,
    private contentQuoteService: ContentQuoteService,
    private dialog: MatDialog,
    private notificationService: NotificationService
  ) {
    this.dataSource.sort = this.sort;
  }

  ngAfterViewInit(): void {
    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.pipe(takeUntil(this.onDestroy)).subscribe(() => {
      this.paginator.pageIndex = 0;
    });

    // If the user updates a filter, reset back to the first page.
    this.filter.pipe(takeUntil(this.onDestroy)).subscribe(() => {
      this.paginator.pageIndex = 0;
    });

    merge(
      this.sort.sortChange,
      this.filter,
      this.paginator.page,
      this.updateQuotes
    )
      .pipe(
        takeUntil(this.onDestroy),
        startWith({}),
        switchMap(() => {
          this.options.page = this.paginator.pageIndex + 1;
          this.options.limit = this.paginator.pageSize;
          if(this.sort.direction != "")
          {
            this.options.orderBy = this.sort.active;
            if(this.sort.direction == 'desc')
            {
              this.options.orderBy += '-';
            }
          }
          return this.contentQuoteService.paginate(this.options);
        }),
        map((data: Quote[]) => {
          this.resultsLength =
            !!data && data.length > 0 ? data[0].totalCount : 0;
          return data;
        }),
        catchError(() => of([]))
      )
      .subscribe((data: Quote[]) => {
        this.dataSource.data = data;
      });
  }

  ngOnDestroy(): void {
    this.onDestroy.next();
  }

  add(): void {
    const dialogData = { quote: {} };
    const dialogRef = this.dialog.open(EditQuoteDialogComponent, {
      data: dialogData
    });

    dialogRef.afterClosed().subscribe((quote: Quote) => {
      if (quote) {
        this.contentQuoteService
          .create(quote)
          .pipe(takeUntil(this.onDestroy))
          .subscribe(() => {
            this.updateQuotes.emit();
            this.notificationService.success("Saved quote");
          });
      }
    });
  }

  delete(): void {
    this.confirmDialogService
      .showConfirmDelete()
      .pipe(takeUntil(this.onDestroy))
      .subscribe(confirmed => {
        if (confirmed) {
          this.contentQuoteService
            .delete(this.selected.selected[0].code)
            .subscribe(() => {
              this.updateQuotes.emit();
              this.notificationService.success("Deleted quote");
            });
        }
      });
  }

  edit(): void {
    const dialogData = { quote: this.selected.selected[0] };
    const dialogRef = this.dialog.open(EditQuoteDialogComponent, {
      data: dialogData
    });

    dialogRef.afterClosed().subscribe((quote: Quote) => {
      if (quote) {
        this.contentQuoteService
          .create(quote)
          .pipe(takeUntil(this.onDestroy))
          .subscribe(() => {
            this.notificationService.success("Saved quote");
          });
      }
    });
  }
}
