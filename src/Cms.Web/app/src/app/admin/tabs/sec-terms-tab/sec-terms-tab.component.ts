import { Component, EventEmitter, OnInit } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import { takeUntil } from "rxjs/operators";

import { ConfirmDialogService } from "src/app/services/confirm-dialog.service";
import { PaginationService } from "src/app/services/pagination.service";
import { SecTermService } from "src/app/services/sec-term.service";
import { SecTerm } from "src/app/shared/models/sec-terms/sec-term.model";
import { SecTermSearchSettings } from "src/app/shared/models/sec-terms/sec-term-search-settings.model";

@Component({
  selector: "admin-sec-terms-tab",
  templateUrl: "./sec-terms-tab.component.html"
})
export class SecTermsTabComponent implements OnInit {
  cardSearchObs: Observable<boolean>;
  cardSearchSubject: BehaviorSubject<boolean>;
  filters = new SecTermSearchSettings();
  isSearchExpanded = true;
  resultsOnPage: number;
  secTerms: SecTerm[];
  totalPages: number;
  totalResults: number;
  private onDestroy: EventEmitter<void> = new EventEmitter();

  constructor(
    private confirmDialogService: ConfirmDialogService,
    private paginationService: PaginationService,
    private secTermService: SecTermService
  ) {
    this.cardSearchSubject = new BehaviorSubject<boolean>(false);
    this.cardSearchObs = this.cardSearchSubject.asObservable();
  }

  ngOnInit(): void {
    this.refreshTerms();
  }

  publish(secTerm: SecTerm): void {
    this.confirmDialogService
      .showConfirmCustom(
        "Are you sure you want to publish this category?",
        "Publish"
      )
      .pipe(takeUntil(this.onDestroy))
      .subscribe(confirmed => {
        if (confirmed) {
          const updatedSecTerm = { ...secTerm, isActive: true };
          this.secTermService.update(updatedSecTerm);
          this.refreshTerms();
        }
      });
  }

  refreshTerms(): void {
    this.secTermService
      .paginate(this.paginationService.getOptionsForSecTerms(this.filters))
      .pipe(takeUntil(this.onDestroy))
      .subscribe(data => {
        this.secTerms = data.results;
        this.resultsOnPage = data.results.length;
        this.totalResults = data.totalCount;
        this.totalPages = Math.ceil(data.totalCount / 20) || 1;
      });
  }

  toggleSearch(): void {
    const toggled = !this.cardSearchSubject.getValue();
    this.cardSearchSubject.next(toggled);
  }

  unpublish(secTerm: SecTerm): void {
    this.confirmDialogService
      .showConfirmCustom(
        "Are you sure you want to unpublish this category?",
        "Unpublish"
      )
      .pipe(takeUntil(this.onDestroy))
      .subscribe(confirmed => {
        if (confirmed) {
          const updatedSecTerm = { ...secTerm, isActive: false };
          this.secTermService.update(updatedSecTerm);
          this.refreshTerms();
        }
      });
  }

  updatePage(newPage: number): void {
    this.filters.currentPage = newPage;
    this.refreshTerms();
  }
}
