import { SelectionModel } from "@angular/cdk/collections";
import {
  Component,
  EventEmitter,
  ViewChild,
  AfterViewInit,
  OnDestroy
} from "@angular/core";
import {
  MatDialog,
  MatSort,
  MatTableDataSource,
  MatPaginator
} from "@angular/material";
import { merge, of } from "rxjs";
import {
  takeUntil,
  startWith,
  switchMap,
  map,
  catchError
} from "rxjs/operators";

import { EditShortlinkDialogComponent } from "src/app/admin/tabs/baseline-pages-tab/shortlinks/edit-shortlink-page-dialog.component";
import { ShortlinksService } from "src/app/services/shortlinks.service";
import { ConfirmDialogService } from "src/app/services/confirm-dialog.service";
import { NotificationService } from "src/app/services/notification.service";
import { BaselinePage } from "src/app/shared/models/baseline-pages/baseline-page.interface";
import { BaselinePageTableOptions } from "src/app/shared/models/table/baseline-page-table-options.model";

@Component({
  selector: "shortlinks",
  templateUrl: "./shortlinks.component.html"
})
export class ShortlinksComponent implements AfterViewInit, OnDestroy {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  displayedColumns: string[] = [
    "urlSegment",
    "redirectUrl"
  ];
  dataSource: MatTableDataSource<BaselinePage> = new MatTableDataSource();
  filter: EventEmitter<void> = new EventEmitter();
  options = new BaselinePageTableOptions();
  resultsLength: number;
  selected: SelectionModel<BaselinePage> = new SelectionModel<BaselinePage>(false, []);
  updateShortlinks: EventEmitter<void> = new EventEmitter();
  private onDestroy: EventEmitter<void> = new EventEmitter();

  constructor(
    private confirmDialogService: ConfirmDialogService,
    private shortlinksService: ShortlinksService,
    private dialog: MatDialog,
    private notificationService: NotificationService
  ) {
    this.dataSource.sort = this.sort;
    this.options.getShortlinks = true;
  }

  ngAfterViewInit(): void {
    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.pipe(takeUntil(this.onDestroy)).subscribe(() => {
      this.paginator.pageIndex = 0;
    });

    // If the user updates a filter, reset back to the first page.
    this.filter.pipe(takeUntil(this.onDestroy)).subscribe(() => {
      this.paginator.pageIndex = 0;
    });

    merge(
      this.sort.sortChange,
      this.filter,
      this.paginator.page,
      this.updateShortlinks
    )
      .pipe(
        takeUntil(this.onDestroy),
        startWith({}),
        switchMap(() => {
          this.options.page = this.paginator.pageIndex + 1;
          this.options.limit = this.paginator.pageSize;
          if(this.sort.direction != "")
          {
            this.options.orderBy = this.sort.active;
            if(this.sort.direction == 'desc')
            {
              this.options.orderBy += '-';
            }
          }
          return this.shortlinksService.paginate(this.options);
        }),
        map((data: BaselinePage[]) => {
          this.resultsLength =
            !!data && data.length > 0 ? data[0].totalCount : 0;
          return data;
        }),
        catchError(() => of([]))
      )
      .subscribe((data: BaselinePage[]) => {
        this.dataSource.data = data;
      });
  }

  ngOnDestroy(): void {
    this.onDestroy.next();
  }

  add(): void {
    const dialogData = { shortlink: {} };
    const dialogRef = this.dialog.open(EditShortlinkDialogComponent, {
      data: dialogData
    });

    dialogRef.afterClosed().subscribe((shortlink: BaselinePage) => {
      if (shortlink) {
        this.shortlinksService
          .create(shortlink)
          .pipe(takeUntil(this.onDestroy))
          .subscribe(() => {
            this.updateShortlinks.emit();
            this.notificationService.success("Saved shortlink");
          });
      }
    });
  }

  delete(): void {
    this.confirmDialogService
      .showConfirmDelete()
      .pipe(takeUntil(this.onDestroy))
      .subscribe(confirmed => {
        if (confirmed) {
          this.shortlinksService
            .delete(this.selected.selected[0].id)
            .subscribe(() => {
              this.updateShortlinks.emit();
              this.notificationService.success("Deleted shortlink");
            });
        }
      });
  }

  edit(): void {
    var selectedBP = this.selected.selected[0];
    const dialogData = { shortlink: { redirectUrl: selectedBP.redirectUrl, urlSegment: selectedBP.urlSegment, id: selectedBP.id  }};
    const dialogRef = this.dialog.open(EditShortlinkDialogComponent, {
      data: dialogData
    });

    dialogRef.afterClosed().subscribe((shortlink: BaselinePage) => {
      if (shortlink) {
        this.shortlinksService
          .create(shortlink)
          .pipe(takeUntil(this.onDestroy))
          .subscribe(() => {
            selectedBP.urlSegment = shortlink.urlSegment;
            selectedBP.redirectUrl = shortlink.redirectUrl;
            this.notificationService.success("Saved shortlink");
          });
      }
    });
  }
}
