import { Shortlink } from "src/app/shared/models/baseline-pages/shortlink.interface";

export interface EditShortlinkPageDialogData {
  shortlink: Shortlink;
}
