import {
    Component,
    Inject,
    OnDestroy,
    ViewChild,
    ElementRef,
    OnInit
  } from "@angular/core";
  import {
    MAT_DIALOG_DATA,
    MatDialogRef,
    MatChipInputEvent
  } from "@angular/material";
  import { ENTER, DOWN_ARROW, UP_ARROW } from "@angular/cdk/keycodes";
  import { FormControl } from "@angular/forms";
  import { Observable, of } from "rxjs";
  import {
    startWith,
    debounceTime,
    tap,
    switchMap,
    catchError,
    map
  } from "rxjs/operators";
  
  import { NotificationService } from "src/app/services/notification.service";
  import { EditShortlinkPageDialogData } from "src/app/admin/tabs/baseline-pages-tab/shortlinks/edit-shortlink-page-dialog-data.interface";
  import { ShortlinkValidationService } from "src/app/admin/services/shortlink-validation.service";
  
  @Component({
    selector: "edit-shortlink-page-dialog",
    templateUrl: "./edit-shortlink-page-dialog.component.html"
  })
  export class EditShortlinkDialogComponent implements OnInit, OnDestroy {
    addOnBlur = false;
    removable = true;
    selectable = true;
    separatorKeysCodes: number[] = [ENTER];

    toHighlight: string;
    value: string;
  
  
    constructor(
      @Inject(MAT_DIALOG_DATA) public data: EditShortlinkPageDialogData,
      private dialogRef: MatDialogRef<EditShortlinkDialogComponent>,
      private notificationService: NotificationService,
      private shortlinkValidationService: ShortlinkValidationService
    ) {  
    }
  
    ngOnInit(): void {
      
    }
  
    ngOnDestroy(): void {
    }
  
   
  
    update(): void {
      var errors = this.shortlinkValidationService.validateShortlink(this.data.shortlink);
      if(errors.length > 0)
      {
        errors.forEach(err => {
          this.notificationService.error(err);
        });
        return;
      }

      this.dialogRef.close(this.data.shortlink);
    }
  }
  