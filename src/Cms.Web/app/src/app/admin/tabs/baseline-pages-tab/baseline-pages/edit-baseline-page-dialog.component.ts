import {
    Component,
    Inject,
    OnDestroy,
    OnChanges,
    OnInit
  } from "@angular/core";
  import {
    MAT_DIALOG_DATA,
    MatDialogRef
  } from "@angular/material";

  import { FormControl } from "@angular/forms";
  import { Observable } from "rxjs";
  import { map, startWith } from "rxjs/operators";
  import { ENTER } from "@angular/cdk/keycodes";
  import { AllContentLayouts } from "src/app/create-content/constants/content-layouts.constants";
  import { ContentLayout } from "src/app/shared/models/content/content-layout.interface";
  
  import { NotificationService } from "src/app/services/notification.service";
  import { EditBaselinePageDialogData } from "src/app/admin/tabs/baseline-pages-tab/baseline-pages/edit-baseline-page-dialog-data.interface";
  import { BaselinePageValidationService } from "src/app/admin/services/baseline-page-validation.service";
  
  @Component({
    selector: "edit-baselinePage-dialog",
    templateUrl: "./edit-baseline-page-dialog.component.html"
  })
  export class EditBaselinePageDialogComponent implements OnInit, OnDestroy {
    addOnBlur = false;
    removable = true;
    selectable = true;
    separatorKeysCodes: number[] = [ENTER];

    toHighlight: string;
    value: string;

    filteredLayouts: Observable<ContentLayout[]>;
    layoutCtrl: FormControl = new FormControl();
    layoutToHighlight: string;
    layouts: ContentLayout[];  
  
    constructor(
      @Inject(MAT_DIALOG_DATA) public data: EditBaselinePageDialogData,
      private dialogRef: MatDialogRef<EditBaselinePageDialogComponent>,
      private notificationService: NotificationService,
      private baselinePageValidationService: BaselinePageValidationService
    ) {  
      this.filteredLayouts = this.layoutCtrl.valueChanges.pipe(
        startWith(AllContentLayouts),
        map(value => this.filterLayouts(value))
      );

      this.layouts = AllContentLayouts.filter(x =>
            x.articleTypes.includes("BaselinePage")
      );

      this.layoutCtrl.setValue(
        AllContentLayouts.find(x => x.id === this.data.baselinePage.layout)
      );
    }

  displayLayout(layout: ContentLayout): string {
    if (!layout) return null;
    return layout.description;
  }

  selectLayout(layout: ContentLayout): void {
    this.data.baselinePage.layout = layout.id;
  }

  private filterLayouts(value: string): ContentLayout[] {
    if (!value || typeof value !== "string") {
      this.layoutToHighlight = "";
      return this.layouts;
    }

    this.layoutToHighlight = value;
    const filterValue = value.toLowerCase();
    return this.layouts.filter((layout: ContentLayout) =>
      layout.description.toLowerCase().includes(filterValue)
    );
  }
  
    ngOnInit(): void {
      
    }
  
    ngOnDestroy(): void {
    }   
  
    update(): void {
      var errors = this.baselinePageValidationService.validateBaselinePage(this.data.baselinePage);
      if(errors.length > 0)
      {
        errors.forEach(err => {
          this.notificationService.error(err);
        });
        return;
      }
      this.dialogRef.close(this.data.baselinePage);
    }
  }
  