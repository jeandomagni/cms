import { BaselinePage } from "src/app/shared/models/baseline-pages/baseline-page.interface";

export interface EditBaselinePageDialogData {
    baselinePage: BaselinePage;
}
