import { ENTER } from '@angular/cdk/keycodes';
import {
  Component,
  Inject,
  OnInit,
  EventEmitter
} from '@angular/core';
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
  MatChipInputEvent,
} from '@angular/material';

import { Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AuthService } from 'src/app/auth/auth.service';
import { NotificationService } from 'src/app/services/notification.service';
import { Bbb } from 'src/app/shared/models/bbb/bbb.model';
import { UserManagementRolesService } from 'src/app/services/user-management-roles.service';
import { BbbInfoService } from 'src/app/services/bbb-info.service';
import {LegacyBbbInfo} from '../../../shared/models/bbb/legacy-bbb-info.model';
import {Roles, UserMembership} from '../../../shared/models/user-management/user-membership.model';
import { UserMgmtService } from 'src/app/services/content-user-mgmt.service';

@Component({
  selector: 'edit-user-dialog',
  templateUrl: './edit-user-dialog.component.html',
})
export class EditUserDialogComponent
  implements OnInit {

  addOnBlur = false;
  notFoundMessage = '';
  removable = true;
  selectable = true;
  separatorKeysCodes: number[] = [ENTER];
  bbb: Bbb = new Bbb();
  bbbId: string;
  canEdit: boolean;
  heading: string;
  isLocalAdmin: boolean;
  roles: Roles[];
  title: string;
  selectedBBB: LegacyBbbInfo;
  selectedRoles: Roles[];
  resultsLength: number;
  allBbbs: LegacyBbbInfo[];
  isAdmin: boolean;
  validationError: boolean;
  errorMessage: string;

  private onDestroy: EventEmitter<void> = new EventEmitter();
  validBBBSelection: boolean;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private authService: AuthService,
    private rolesService: UserManagementRolesService,
    private dialogRef: MatDialogRef<EditUserDialogComponent>,
    private notificationService: NotificationService,
    private bbbInfoService: BbbInfoService,
    private userMgmtService: UserMgmtService,
  ) {
    this.bbbId = this.authService.currentUser.profile.defaultSite;
    this.refreshBbb();
    this.rolesService
      .get()
      .pipe(takeUntil(this.onDestroy))
      .subscribe( (data: Roles[]) => {
        this.roles = data;
      });
    this.isAdmin = authService.isGlobalAdmin;
    this.allBbbs = authService.getSites();
    this.validBBBSelection = true;
  }


  ngOnInit(): void {
    switch (this.data.mode) {
      case 'adduser':
        this.title = 'Add User';
        this.selectedBBB = null;
        break;
      case 'edituser':
        this.title = 'Edit User';
        const selectedBbb = this.allBbbs.find(
          bbb => bbb.legacyBBBID.toLowerCase() === this.data.user.siteId.toLowerCase()
        );
        this.selectedRoles = this.data.user.roles.map(x => x.roleName);
        this.selectedBBB = selectedBbb;
        break;
      case 'statuschange':
        this.title = 'Update User Status';
        break;
      case 'passwordchange':
        this.title = 'Update User Password';
        break;
    }
  }

  update(): void {
    this.validationError = false;
    switch (this.data.mode) {
      case 'statuschange':
        this.statusChange();
        break;
      case 'passwordchange':
        this.passwordChange();
        break;
      case 'adduser':
        this.addUser();
        break;
      case 'edituser':
        this.editUser();
        break;
    }
  }

  private addUser(): void {
    const selectedBbb = this.selectedBBB;
    const selectedRoles = this.selectedRoles;
    if (this.selectedRoles !== null && this.selectedRoles.length > 0) {
      if (selectedBbb === null) {
        this.validationError = true;
        this.errorMessage = 'You need to specify a BBB in order to set roles.';
        return;
      }
    }

    this.data.user.siteId = selectedBbb.legacyBBBID;
    this.data.user.roles = [];
    selectedRoles.forEach(r =>  {
      // @ts-ignore
      const role = this.roles.find(ro => ro.roleName === r );
      this.data.user.roles.push(role);
    });

    this.userMgmtService
      .getUser(this.data.user.email)
      .pipe(takeUntil(this.onDestroy))
      .subscribe((user: UserMembership ) => {
        if (user == null) {
          this.userMgmtService
            .addUser(this.data.user)
            .pipe(takeUntil(this.onDestroy))
            .subscribe((serviceResponse) => {
              if (serviceResponse.success) {
                this.dialogRef.close(this.data);
              }
              this.validationError = true;
              this.errorMessage = serviceResponse.errorMessages.join(',');
            });
        } else {
          this.validationError = true;
          this.errorMessage = 'User already exists.';
        }
      });
  }


  private editUser(): void {
    const selectedBbb = this.selectedBBB;
    const selectedRoles = this.selectedRoles;
    if (this.selectedRoles !== null && this.selectedRoles.length > 0) {
      if (selectedBbb === null) {
        this.validationError = true;
        this.errorMessage = 'You need to specify a BBB in order to set roles.';
        return;
      }
    }

    this.data.user.siteId = selectedBbb.legacyBBBID;
    this.data.user.roles = [];
    selectedRoles.forEach(r =>  {
      // @ts-ignore
      const role = this.roles.find(ro => ro.roleName === r );
      this.data.user.roles.push(role);
    });

    this.userMgmtService
      .getUser(this.data.user.email)
      .pipe(takeUntil(this.onDestroy))
      .subscribe((user: UserMembership ) => {
        if (user == null) {
          this.validationError = true;
          this.errorMessage = "User does not exists.";
        }
        if (this.data.user.userId !== user.userId) {
          this.validationError = true;
          this.errorMessage = 'Email already in use.';
          return;
        } else {

          this.userMgmtService
            .updateUser(this.data.user)
            .pipe(takeUntil(this.onDestroy))
            .subscribe((serviceResponse) => {
              if (serviceResponse.success) {
                this.dialogRef.close(this.data);
              }
              this.validationError = true;
              this.errorMessage = serviceResponse.errorMessages.join(',');
            });
        }
      });
  }

  private statusChange(): void {
    this.userMgmtService
      .updateStatus(this.data.user)
      .pipe(takeUntil(this.onDestroy))
      .subscribe((serviceResponse) => {
        if (serviceResponse.success) {
          this.dialogRef.close(this.data);
        }
        this.validationError = true;
        this.errorMessage = serviceResponse.errorMessages.join(',');
      });
  }

  private passwordChange(): void {
    this.data.user.newPassword = this.data.user.password;
    this.data.user.verifyPassword = this.data.user.confirmPwd;
    this.userMgmtService
      .updatePassword(this.data.user)
      .pipe(takeUntil(this.onDestroy))
      .subscribe((serviceResponse) => {
        if (serviceResponse.success) {
          this.dialogRef.close(this.data);
        }
        this.validationError = true;
        this.errorMessage = serviceResponse.errorMessages.join(',');
      });
  }



  private refreshBbb(): void {
    this.isLocalAdmin = this.authService.currentUser.adminSites.includes(
      this.bbbId
    );
    this.canEdit = this.isLocalAdmin || this.authService.isAdmin;
  }

  private setBbb(bbb: Bbb): void {
    this.bbb = bbb;
    this.heading = this.isLocalAdmin ? `${bbb.name} [Admin]` : bbb.name;
  }

  bbbSelectionChange($event: LegacyBbbInfo) {
    if ($event === null) {
      // this.selectedRoles = [];
    }
  }




}
