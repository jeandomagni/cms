import { SelectionModel } from '@angular/cdk/collections';
import {
  Component,
  EventEmitter,
  ViewChild,
  AfterViewInit,
  OnDestroy,
  ElementRef
} from '@angular/core';
import {
  MatDialog,
  MatSort,
  MatTableDataSource,
  MatPaginator,
  MatInput
} from '@angular/material';
import {merge, Observable, of, Subject, fromEvent} from 'rxjs';
import {
  takeUntil,
  startWith,
  switchMap,
  map,
  catchError,
  debounceTime,
  distinctUntilChanged,
  filter,
  debounce
} from 'rxjs/operators';


import { EditUserDialogComponent } from 'src/app/admin/tabs/users-tab/edit-user-dialog.component';
import { ConfirmDialogService } from 'src/app/services/confirm-dialog.service';
import { NotificationService } from 'src/app/services/notification.service';
import * as userMgmt from 'src/app/shared/models/user-management/user-membership.model';
import { UserMgmtService } from 'src/app/services/content-user-mgmt.service';
import { AuthService } from 'src/app/auth/auth.service';
import {File} from '../../../shared/models/file/file.model';


@Component({
  selector: 'admin-users-tab',
  templateUrl: './users-tab.component.html'
})
export class UsersTabComponent implements AfterViewInit, OnDestroy {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild('searchCtrl', { static: true }) searchCtrl: ElementRef;

  displayedColumns: string[] = [
    'select',
    'firstName',
    'lastName',
    'email',
    'status'
  ];
  dataSource: MatTableDataSource<userMgmt.UserMembership> = new MatTableDataSource();
  filter: EventEmitter<string> = new EventEmitter();
  options = new userMgmt.UserMembershipListRequest();
  resultsLength: number;
  selected: SelectionModel<userMgmt.UserMembership> = new SelectionModel<userMgmt.UserMembership>(false, []);
  updateUsers: EventEmitter<void> = new EventEmitter();
  private onDestroy: EventEmitter<void> = new EventEmitter();
  isAdmin: boolean;
  search: string;
  userResults: Observable<Observable<any[]>>;
  userSubject = new Subject();

  constructor(
    authService: AuthService,
    private confirmDialogService: ConfirmDialogService,
    private userMgmtService: UserMgmtService,
    private dialog: MatDialog,
    private notificationService: NotificationService
  ) {
    this.dataSource.sort = this.sort;
    this.isAdmin = authService.isGlobalAdmin;

  }

  ngAfterViewInit(): void {

    fromEvent(this.searchCtrl.nativeElement, 'keyup').pipe(
      map((event: any) => {
        return event.target.value;
      })
      , debounceTime(1000)
      , distinctUntilChanged()
    ).subscribe((text: string) => {
      this.filter.emit(text);
    });

    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.pipe(takeUntil(this.onDestroy)).subscribe(() => {
      this.selected.clear();
      this.paginator.pageIndex = 0;
    });

    // If the user updates a filter, reset back to the first page.
    this.filter.pipe(takeUntil(this.onDestroy)).subscribe(() => {
      this.paginator.pageIndex = 0;
    });

    merge(
      this.sort.sortChange,
      this.filter,
      this.paginator.page,
      this.updateUsers
    )
      .pipe(
        takeUntil(this.onDestroy),
        startWith({}),
        switchMap(() => {
          this.options.search = this.search ;
          this.options.page = this.paginator.pageIndex + 1;
          this.options.pageSize = this.paginator.pageSize;
          this.options.sortColumn = 'email';
          if (this.sort.direction !== '') {
            this.options.direction = 'asc';
            if (this.sort.direction === 'desc') {
              this.options.direction = 'desc';
            }
          }
          return this.userMgmtService.paginate(this.options);
        }),
        map((data: userMgmt.PaginationResult<userMgmt.UserMembership>) => {
          this.resultsLength =
            !!data && data.items.length > 0 ? data.totalRecords : 0;
          return data.items;
        }),
        catchError(() => of([]))
      )
      .subscribe((data: userMgmt.UserMembership[]) => {
        this.dataSource.data = data;
        this.selected.clear();
      });
  }

  ngOnDestroy(): void {
    this.onDestroy.next();
  }

  add(): void {
    const dialogRef = this.dialog.open(EditUserDialogComponent, {
      data: {
        mode : 'adduser',
        user: {}
      }
    });

    dialogRef.afterClosed().subscribe((data: userMgmt.userDialogData) => {
      if (data) {
        if (data.user.userName === undefined) {
          data.user.userName = data.user.email;
        }
        this.updateUsers.emit();
        this.notificationService.success('User created.');
      }
    });
  }

  edit(): void {
    const userId = this.selected.selected[0].userId;
    this.userMgmtService
      .getInfo(userId)
      .pipe(takeUntil(this.onDestroy))
      .subscribe( (response: userMgmt.UserMembershipInfo) => {

        const dialogData = {
          user: Object.assign({}, response),
          mode: 'edituser'
        };
        const dialogRef = this.dialog.open(EditUserDialogComponent, {
          data: dialogData
        });

        dialogRef.afterClosed().subscribe((dialogData: userMgmt.userDialogData) => {
          if (dialogData) {
            this.updateUsers.emit();
            this.notificationService.success('User Updated');
          }
        });
      });
  }

  changeStatus(): void {
    const dialogData = { user: Object.assign({}, this.selected.selected[0]), mode: 'statuschange' };
    console.log(dialogData);
    const dialogRef = this.dialog.open(EditUserDialogComponent, {
      data: dialogData
    });

    dialogRef.afterClosed().subscribe((data: userMgmt.userDialogData) => {
      if (data) {
        this.updateUsers.emit();
        this.notificationService.success('Status updated.');
      }
    });
  }


  setPassword(): void {
    const dialogData = { user: Object.assign({password : null}, this.selected.selected[0]), mode: 'passwordchange' };
    const dialogRef = this.dialog.open(EditUserDialogComponent, {
      data: dialogData
    });

    dialogRef.afterClosed().subscribe((data: userMgmt.userDialogData) => {
      if (data) {
        this.updateUsers.emit();
        this.notificationService.success('Password Changed.');
      }
    });
  }

  delete(): void {
    this.confirmDialogService
      .showConfirmDelete()
      .pipe(takeUntil(this.onDestroy))
      .subscribe(confirmed => {
        if (confirmed) {
          this.userMgmtService
            .delete(this.selected.selected[0].userId)
            .subscribe((serviceResponse) => {
              this.updateUsers.emit();
              this.notificationService.success('Deleted user');
            });
        }
      });
  }
}
