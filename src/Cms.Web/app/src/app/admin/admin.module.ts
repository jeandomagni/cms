import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FlexLayoutModule } from "@angular/flex-layout";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {
  MatTabsModule,
  MatIconModule,
  MatCardModule,
  MatAutocompleteModule,
  MatFormFieldModule,
  MatButtonModule,
  MatInputModule,
  MatSelectModule,
  MatTableModule,
  MatSortModule,
  MatChipsModule,
  MatPaginatorModule,
  MatListModule,
  MatToolbarModule,
  MatDialogModule,
  MatRadioModule,
  MatCheckboxModule
} from "@angular/material";

import { AdminComponent } from "src/app/admin/admin.component";
import { AdminRoutingModule } from "src/app/admin/admin-routing.module";
import { EditProfileSeoModule } from "src/app/admin/edit-profile-seo/edit-profile-seo.module";
import { EditSecTermModule } from "src/app/admin/edit-sec-term/edit-sec-term.module";
import { ImageTagService } from "src/app/admin/services/image-tag.service";
import { QuoteTagService } from "src/app/admin/services/quote-tag.service";
import { EditImageDialogComponent } from "src/app/admin/tabs/images-tab/edit-image-dialog.component";
import { ImagesTabComponent } from "src/app/admin/tabs/images-tab/images-tab.component";
import { ProfileSeoTabComponent } from "src/app/admin/tabs/profile-seo-tab/profile-seo-tab.component";
import { EditQuoteDialogComponent } from "src/app/admin/tabs/quotes-tab/edit-quote-dialog.component";
import { EditUserDialogComponent } from "src/app/admin/tabs/users-tab/edit-user-dialog.component";
import { QuotesTabComponent } from "src/app/admin/tabs/quotes-tab/quotes-tab.component";
import { UsersTabComponent } from "src/app/admin/tabs/users-tab/users-tab.component";
import { SecTermsTabComponent } from "src/app/admin/tabs/sec-terms-tab/sec-terms-tab.component";
import { UploadTabComponent } from "src/app/admin/tabs/upload-tab/upload-tab.component";
import { FileService } from "src/app/services/file.service";
import { ContentQuoteService } from "src/app/services/content-quote.service";
import { UserMgmtService } from "src/app/services/content-user-mgmt.service";
import { PasswordMgmtService } from "src/app/services/content-password-mgmt.service";
import { SecTermService } from "src/app/services/sec-term.service";
import { SharedModule } from "src/app/shared/shared.module";
import { EditBaselinePageDialogComponent } from "src/app/admin/tabs/baseline-pages-tab/baseline-pages/edit-baseline-page-dialog.component";
import { BaselinePagesComponent } from "src/app/admin/tabs/baseline-pages-tab/baseline-pages/baseline-pages.component";
import { EditShortlinkDialogComponent } from "src/app/admin/tabs/baseline-pages-tab/shortlinks/edit-shortlink-page-dialog.component";
import { ShortlinksComponent } from "src/app/admin/tabs/baseline-pages-tab/shortlinks/shortlinks.component";
import { BaselinePageTabComponent } from "src/app/admin/tabs/baseline-pages-tab/baselinePageTab.component";
import { BaselinePageValidationService } from "src/app/admin/services/baseline-page-validation.service";
import { ShortlinkValidationService } from "src/app/admin/services/shortlink-validation.service";
import { UniqueBaselinePageSegmentUrlDirective } from 'src/app/admin/validators/baseline-page.segment-url.validator';
import { BbbInfoService } from "src/app/services/bbb-info.service";
import { UserManagementRolesService } from 'src/app/services/user-management-roles.service';


import {
  OwlDateTimeModule,
  OWL_DATE_TIME_FORMATS,
  OwlNativeDateTimeModule
} from "ng-pick-datetime";

const CMS_NATIVE_FORMATS = {
  fullPickerInput: {
    year: "numeric",
    month: "numeric",
    day: "numeric",
    hour: "numeric",
    minute: "numeric"
  },
  datePickerInput: { year: "numeric", month: "numeric", day: "numeric" },
  timePickerInput: { hour: "numeric", minute: "numeric" },
  monthYearLabel: { year: "numeric", month: "short" },
  dateA11yLabel: { year: "numeric", month: "long", day: "numeric" },
  monthYearA11yLabel: { year: "numeric", month: "long" }
};

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    FormsModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDialogModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatPaginatorModule,
    MatRadioModule,
    MatSelectModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    ReactiveFormsModule,
    SharedModule,

    OwlDateTimeModule,
    OwlNativeDateTimeModule,

    EditProfileSeoModule,
    EditSecTermModule,

    AdminRoutingModule
  ],
  declarations: [
    AdminComponent,
    EditImageDialogComponent,
    EditQuoteDialogComponent,
    EditUserDialogComponent,
    ImagesTabComponent,
    ProfileSeoTabComponent,
    QuotesTabComponent,
    UsersTabComponent,
    SecTermsTabComponent,
    UploadTabComponent,
    EditBaselinePageDialogComponent,
    BaselinePageTabComponent,
    BaselinePagesComponent,
    EditShortlinkDialogComponent,
    ShortlinksComponent,
    UniqueBaselinePageSegmentUrlDirective
  ],
  entryComponents: [
    EditImageDialogComponent,
    EditQuoteDialogComponent,
    EditUserDialogComponent,
    ImagesTabComponent,
    ProfileSeoTabComponent,
    QuotesTabComponent,
    UsersTabComponent,
    SecTermsTabComponent,
    UploadTabComponent,
    EditBaselinePageDialogComponent,
    BaselinePageTabComponent,
    EditShortlinkDialogComponent,
    ShortlinksComponent
  ],
  providers: [
    UserMgmtService,
    PasswordMgmtService,
    ContentQuoteService,
    FileService,
    ImageTagService,
    QuoteTagService,
    SecTermService,
    BaselinePageValidationService,
    ShortlinkValidationService,
    BbbInfoService,
    UserManagementRolesService,
    { provide: OWL_DATE_TIME_FORMATS, useValue: CMS_NATIVE_FORMATS }
  ]
})
export class AdminModule {}
