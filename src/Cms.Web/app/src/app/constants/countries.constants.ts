import { Country } from "src/app/shared/models/location/country.interface";

export const Countries: Country[] = [
  {
    value: "USA",
    name: "United States"
  },
  {
    value: "CAN",
    name: "Canada"
  },
  {
    value: "MEX",
    name: "Mexico"
  }
];
