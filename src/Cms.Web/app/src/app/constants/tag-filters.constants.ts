import { TagFilter } from "src/app/shared/models/file/tag-filter.interface";

export const TagFilters: TagFilter[] = [
  { value: "*", name: "All" },
  { value: "generic", name: "Generic" },
  { value: "city", name: "City" },
  { value: "state", name: "State" },
  { value: "country", name: "Country" }
];

export const DefaultTagFilter = TagFilters[0].value;
