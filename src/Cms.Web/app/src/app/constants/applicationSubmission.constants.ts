import { ApplicationSubmission } from "src/app/shared/models/ApplicationSubmission.interface";

export const ApplicationSubmissions: ApplicationSubmission[] = [
  {
    value: "0",
    name: "Submit application without payment",
  },
  {
    value: "1",
    name: "Submit application with payment",
  },
  {
    value: "2",
    name: "Submit application with user choice to pay now or pay later ",
  },
];
