import { Language } from "src/app/shared/models/language.interface";

export const Languages: Language[] = [
  {
    value: "EN",
    name: "English"
  },
  {
    value: "ES",
    name: "Spanish"
  },
  {
    value: "FR",
    name: "French"
  }
];
