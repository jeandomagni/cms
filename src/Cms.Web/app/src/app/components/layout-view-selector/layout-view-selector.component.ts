import {
    Component,
    Input,
    Output,
    EventEmitter,
    ViewChild,
    ElementRef,
    OnInit
  } from "@angular/core";
  import { MatAutocompleteSelectedEvent } from "@angular/material/autocomplete";
  import { MatSnackBar } from "@angular/material/snack-bar";
  import { Observable, of } from "rxjs";
  import { map, catchError } from "rxjs/operators";
  
  import { Location } from "src/app/shared/models/location/location.interface";
  import { SolrService } from "src/app/services/solr.service";
  import { ServiceAreaService } from "src/app/services/service-area.service";
  import { AuthService } from "src/app/auth/auth.service";
  import { BaseAutocompleteComponent } from "src/app/components/base/base-autocomplete/base-autocomplete.component";
  import { TextItem } from "src/app/shared/models/text-item.model";
  import { ArticleOrderParams } from "src/app/shared/models/article-order-params.model";
  import { Countries } from "src/app/constants/countries.constants";
  import { ListingTypes } from "src/app/manage-content/tabs/list-children-tab/listing-types.constants";
  import { MatDialog } from "@angular/material/dialog";
  import { DialogAddArticleToNewsfeedComponent } from "../dialog-add-article-to-newsfeed/dialog-add-article-to-newsfeed.component";
  import { ContentService } from '../../services/content.service';
  
  @Component({
    selector: "layout-view-selector",
    templateUrl: "./layout-view-selector.component.html",
    styleUrls: ["./layout-view-selector.component.scss"]
  })
  export class LayoutViewSelectorComponent extends BaseAutocompleteComponent<TextItem> implements OnInit {
    selectedItem: TextItem;
    @Input() articleOrderId: string;
    @Input() overrideFeedId: string;
    @Output() selectedItemChange: EventEmitter<ArticleOrderParams> = new EventEmitter();
    @Output() update: EventEmitter<void> = new EventEmitter();
    params = new ArticleOrderParams();
    listingTypes = ListingTypes;
    countries = Countries;

    serviceAreaLayoutView: string = "Service Area";
    stateLayoutView: string = "State/Province";
    cityLayoutView: string = "City";

    layoutViews: string[] = [];
    selectedLayoutView: string;

    allItems: TextItem[];
    notFoundMessage = "No matching sites found.";
    selectable = true;
    allTopics: string[];
  
    @ViewChild("siteInput", { static: false }) siteInput: ElementRef;
  
    constructor(private authService: AuthService, 
                private serviceAreaService: ServiceAreaService,
                private contentService: ContentService,
                private snackBar: MatSnackBar,
                private solrService: SolrService,
                public dialog: MatDialog) {
      super();
      
      this.layoutViews = [];
      this.layoutViews.push(this.serviceAreaLayoutView);
      this.layoutViews.push(this.stateLayoutView);
      this.layoutViews.push(this.cityLayoutView);

      this.selectedLayoutView = this.serviceAreaLayoutView;

      this.allItems = [];
      this.allTopics = this.authService.getTopics();
    }

    ngOnInit() {
      this.layoutViewChanged();
    }

    clearAllSearchFilters () {
        this.clear();
        this.params.listingType = this.listingTypes[0].toLowerCase();
        this.params.country = this.countries[0].value;
        this.selectedLayoutView = this.serviceAreaLayoutView;
        this.countrySelectionChanged();
    }

    clear () {
      this.params.state = '';
      this.params.city = '';
      this.params.serviceArea = '';
    }

    listingTypeSelectionChanged () {
      this.refresh();
    }

    countrySelectionChanged () {
      this.selectedItem = null;
      this.refresh();
    }

    refresh() {
      var value = '';
      if(this.selectedItem != null) {
        value = this.selectedItem.id;
      }

      this.clear();

      if(this.isState()) {
        var stateCountryArray = value.split(" ");
        this.params.state = stateCountryArray[0];
      }
      if(this.isCity() && this.selectedItem != null) {
        var array = value.split(" ");
        this.params.city = this.selectedItem.city;
        this.params.state = array[array.length - 2];
      }
      if(this.isServiceArea()) {
          this.params.serviceArea = value;        
      }

      this.selectedItemChange.emit(this.params);
      this.update.emit();
    }

    showSwitchToServiceArea = () => {
      return this.overrideFeedId == null;
    }
  
    protected queryItems(value: string): Observable<TextItem[]> {
      var func = this.populateFromServiceAreaa;
      if(this.selectedLayoutView == this.cityLayoutView) {
        func = this.populateFromCities;
      }
      else if (this.selectedLayoutView == this.stateLayoutView) {
        func = this.populateFromStates
      }

      return func(value).pipe(
        map((items: TextItem[]) => {
          const suggestions = items.filter(x =>
            x.text.toLowerCase().includes(value.toLowerCase())
          );
          return suggestions.length > 0 ? suggestions : null;
        }),
        catchError(_ => of(null))
      );
    }

    displayFn(item: TextItem): string {
      return item && item.text ? item.text : '';
    }

    layoutViewChanged() {
      var defaultItem :TextItem = null;
      var defaultCountry = this.authService.getDefaultSiteCountry();
      this.params.country = defaultCountry;
      if(this.isState()) {
        var defaultState = this.authService.getDefaultSiteState();
        if(defaultState != null && !this.authService.isAdmin)
        {
          defaultItem = new TextItem(defaultState, defaultState);
        }
      }
      if(this.isServiceArea()) {
        var defaultSiteId = this.authService.getDefaultSiteId();
        var defaultSiteName = this.authService.getDefaultSiteName();
        if(defaultSiteId != null && defaultSiteName != null && !this.authService.isGlobalAdmin)
        {
          defaultItem = new TextItem(defaultSiteId, defaultSiteName);
        }
      }
      if(this.isCity()) {
        var defaultSiteCity = this.authService.getDefaultSiteCity();
        if(defaultSiteCity != null)
        {
          defaultItem = new TextItem(defaultSiteCity.id, defaultSiteCity.city);
        }
      }

      if(defaultItem != null) {
        this.selectedItem = defaultItem;
      } else {
        this.selectedItem = null;
        this.acCtrl.setValue("");
      }
      this.refresh();
    }

    populateFromStates = (value: string): Observable<TextItem[]> => {
      return this.serviceAreaService.getFilteredStatesForUser([this.params.country]).pipe(
          map((states: string[]) => {
            return states.map(x => {
              var state = x.split(" ")[0];
              return new TextItem(state, state);
          });
        }),
        catchError(_ => of(null))
      );
    }

    populateFromCities = (value: string): Observable<TextItem[]> => {
      return this.solrService.paginateCities(value, [this.params.country]).pipe(
        map((locations: Location[]) => {
          return locations.map(x => new TextItem(x.id, x.id, x.city));
        }),
        catchError(_ => of(null))
      );
    }

    populateFromServiceAreaa = (value: string): Observable<TextItem[]> => {
      var sites = this.authService.getSites();
      sites = sites.filter(x => x.countryCode == this.params.country);
      return of(sites.map(x => new TextItem(x.legacyBBBID, x.bbbName)));
    }

    showAddArticleToNewsfeed = () => {
      const dialogRef = this.dialog.open(DialogAddArticleToNewsfeedComponent, {
        width: "80%",
        data: this.params
      });

      dialogRef.afterClosed().subscribe(() => {
        this.params.page = 1;
        this.refresh();
      });
    }

    switchToServiceAreaNewsfeed = () => {
      if(this.params.city != null) {
        this.contentService.switchToServiceAreaNewsfeed(this.params).subscribe(response => {
          this.params.page = 1;
          this.refresh();
        });
      }
    }

    switchToCountryNewsfeed = () => {
      if(this.params.city != null) {
        this.contentService.switchToCountryNewsfeed(this.params).subscribe(response => {
          this.params.page = 1;
          this.refresh();
        });
      }
    }

    clearNewsfeedOverride = () => {
      if(this.params.city != null) {
        this.contentService.clearNewsfeedOverride(this.params).subscribe(response => {
          this.params.page = 1;
          this.refresh();
        });
      }
    }

    showSwitchToCountryNewsfeed = () => {
      return this.selectedLayoutView == this.serviceAreaLayoutView && this.params.serviceArea != null && this.params.serviceArea != "";
    }

    isState = () => this.selectedLayoutView == this.stateLayoutView;
    isCity = () => this.selectedLayoutView == this.cityLayoutView;
    isServiceArea = () => this.selectedLayoutView == this.serviceAreaLayoutView;
  }
  