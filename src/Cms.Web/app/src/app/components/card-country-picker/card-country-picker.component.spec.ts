import { TestBed, ComponentFixture } from "@angular/core/testing";
import { By } from "@angular/platform-browser";
import { of } from "rxjs";

import { AuthService } from "src/app/auth/auth.service";
import { SharedModule } from "src/app/shared/shared.module";
import { ServiceAreaService } from "src/app/services/service-area.service";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { Geotag } from "src/app/shared/models/location/geotag.interface";

import { CardCountryPickerComponent } from "./card-country-picker.component";

describe("CardCountryPickerComponent", () => {
  let authServiceStub: Partial<AuthService> = { init: () => {} };
  let component: CardCountryPickerComponent;
  let fixture: ComponentFixture<CardCountryPickerComponent>;
  let input: HTMLInputElement;
  let serviceAreaServiceStub: Partial<ServiceAreaService> = {
    getCountriesForUser: () => of(["USA", "CAN", "MEX"])
  };

  const initComponentAndVariables = () => {
    TestBed.configureTestingModule({
      providers: [
        { provide: AuthService, useValue: authServiceStub },
        { provide: ServiceAreaService, useValue: serviceAreaServiceStub }
      ],
      imports: [BrowserAnimationsModule, SharedModule]
    });

    fixture = TestBed.createComponent(CardCountryPickerComponent);
    component = fixture.componentInstance;
    component.editable = true;
    fixture.detectChanges();
    input = fixture.debugElement.query(By.css("input"))!.nativeElement;

    spyOn(component.remove, "emit").and.callThrough();
    spyOn(component.update, "emit").and.callThrough();
  };

  const initSelectedItems = () => {
    var usaGeotag: Geotag = {
      contentCode: "test-content-code",
      isPinned: false,
      locationId: "USA",
      locationType: "country"
    };

    component.selected = [usaGeotag];

    fixture.detectChanges();
  };

  describe("for BBB admins", () => {
    beforeEach(() => {
      authServiceStub.isAdmin = false;
      initComponentAndVariables();
    });

    describe("with selected items", () => {
      beforeEach(() => {
        initSelectedItems();
      });

      it("should show up as a chip with ID as label", () => {
        const chipElement = fixture.debugElement.query(By.css("mat-chip"))
          .nativeElement;
        expect(chipElement.innerText).toContain("USA");
      });

      it("should not surface a menu when chip is clicked", () => {
        const chipElement = fixture.debugElement.query(By.css("mat-chip span"))
          .nativeElement;
        chipElement.click();
        fixture.detectChanges();

        const menuItem = fixture.debugElement.query(By.css(".mat-menu-item"));
        expect(menuItem).toBeNull();
      });

      it("should remove a chip when the X icon is clicked", () => {
        const cancelIconElement = fixture.debugElement.query(
          By.css("mat-chip mat-icon")
        ).nativeElement;
        cancelIconElement.click();
        fixture.detectChanges();

        expect(component.remove.emit).toHaveBeenCalledWith(
          jasmine.objectContaining({ locationId: "USA" })
        );
      });
    });
  });

  describe("for global admins", () => {
    beforeEach(() => {
      authServiceStub.isAdmin = true;
      initComponentAndVariables();
    });

    describe("with selected items", () => {
      beforeEach(() => {
        initSelectedItems();
      });

      it("should show up as a chip with ID as label", () => {
        const chipElement = fixture.debugElement.query(By.css("mat-chip"))
          .nativeElement;
        expect(chipElement.innerText).toContain("USA");
      });

      it("should surface an option to pin when chip is clicked", () => {
        const chipElement = fixture.debugElement.query(By.css("mat-chip span"))
          .nativeElement;
        chipElement.click();
        fixture.detectChanges();

        const menuItemElement = fixture.debugElement.query(
          By.css(".mat-menu-item")
        ).nativeElement;
        expect(menuItemElement.innerText).toContain("Add Pin");
      });

      it("should surface country suggestions when user types", async () => {
        input.dispatchEvent(new Event("focusin"));
        input.value = "U";
        input.dispatchEvent(new Event("input"));

        fixture.detectChanges();
        await fixture.whenStable();
        fixture.detectChanges();

        const matOptions = document.querySelectorAll("mat-option");
        expect(matOptions.length).toBe(1);
      });

      it("should pin a chip when that menu item is selected", () => {
        const chipElement = fixture.debugElement.query(By.css("mat-chip span"))
          .nativeElement;
        chipElement.click();
        fixture.detectChanges();

        const menuItemElement = fixture.debugElement.query(
          By.css(".mat-menu-item")
        ).nativeElement;
        menuItemElement.click();
        fixture.detectChanges();

        expect(component.update.emit).toHaveBeenCalledWith(
          jasmine.objectContaining({ locationId: "USA", isPinned: true })
        );
      });
    });
  });
});
