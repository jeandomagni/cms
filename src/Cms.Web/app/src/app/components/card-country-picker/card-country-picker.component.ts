﻿﻿import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnInit,
  ViewChild,
  ElementRef,
  OnChanges,
  OnDestroy
} from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { MatAutocompleteSelectedEvent } from "@angular/material/autocomplete";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

import { AuthService } from "src/app/auth/auth.service";
import { ServiceAreaService } from "src/app/services/service-area.service";
import { Geotag } from "src/app/shared/models/location/geotag.interface";
import { BaseAutocompleteComponent } from "src/app/components/base/base-autocomplete/base-autocomplete.component";

@Component({
  selector: "card-country-picker",
  templateUrl: "./card-country-picker.component.html"
})
export class CardCountryPickerComponent
  extends BaseAutocompleteComponent<string>
  implements OnInit, OnChanges, OnDestroy {
  @Input() contentCode: string;
  @Input() editable: boolean;
  @Input() selected: Geotag[];
  @Output() remove: EventEmitter<Geotag> = new EventEmitter();
  @Output() update: EventEmitter<Geotag> = new EventEmitter();

  allowPin: boolean;
  notFoundMessage = "No countries found.";
  selectable = true;

  @ViewChild("countryInput", { static: false }) countryInput: ElementRef;

  constructor(
    authService: AuthService,
    private serviceAreaService: ServiceAreaService,
    private snackBar: MatSnackBar
  ) {
    super();

    this.allowPin = authService.isAdmin;
  }

  ngOnInit(): void {
    this.selected = this.selected || [];
  }

  ngOnChanges(): void {
    if (!this.editable) {
      this.acCtrl.disable();
    }
  }

  removeCountry(country: Geotag): void {
    this.remove.emit(country);
  }

  selectCountry(event: MatAutocompleteSelectedEvent): void {
    const country = event.option.value;

    if (this.selected.find(item => item.locationId === country)) {
      this.snackBar.open(`You already added ${country}.`, "", {
        duration: 1000
      });

      return;
    }

    const geotag: Geotag = {
      contentCode: this.contentCode,
      isPinned: false,
      locationId: country,
      locationType: "country",
      pinExpirationDate: null,
      pinId: null
    };
    this.update.emit(geotag);

    this.countryInput.nativeElement.value = "";
    this.acCtrl.setValue(null);
  }

  setExpiration(geotag: Geotag): void {
    this.update.emit(geotag);
  }

  togglePin(geotag: Geotag): void {
    const twoWeeksLater = new Date(
      new Date().setDate(new Date().getDate() + 7)
    );

    const updatedGeotag: Geotag = {
      ...geotag,
      pinExpirationDate: !geotag.isPinned ? twoWeeksLater : null,
      isPinned: !geotag.isPinned
    };

    this.update.emit(updatedGeotag);
  }

  protected queryItems(searchValue: string): Observable<string[]> {
    return this.serviceAreaService
      .getCountriesForUser()
      .pipe(
        map((countries: string[]) =>
          countries.filter((x: string) => x.toLowerCase().includes(searchValue))
        )
      );
  }
}
