﻿import {
  Component,
  Input,
  OnInit,
  Output,
  EventEmitter,
  OnDestroy
} from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { Observable, BehaviorSubject } from "rxjs";
import { takeUntil } from "rxjs/operators";

import { FileService } from "src/app/services/file.service";
import { PaginationService } from "src/app/services/pagination.service";
import { File } from "src/app/shared/models/file/file.model";
import { ImageSearchSettings } from "src/app/shared/models/file/image-search-settings.model";

@Component({
  selector: "image-search",
  templateUrl: "./image-search.component.html"
})
export class ImageSearchComponent implements OnInit, OnDestroy {
  @Input() filterBy: string;
  @Input() searchResults: File[];
  @Output() searchResultsChange: EventEmitter<File[]> = new EventEmitter();

  cardOptions = new ImageSearchSettings();
  cardSearchObs: Observable<boolean>;
  disabledCardControls = true;
  form: FormGroup;
  resultCount = 0;
  totalPages = 0;
  totalResultCount = 0;
  totalResultCountLabel: string;

  private cardSearchSubject: BehaviorSubject<boolean>;
  private onDestroy: EventEmitter<void> = new EventEmitter();

  constructor(
    private fileService: FileService,
    private paginationService: PaginationService
  ) {
    this.cardSearchSubject = new BehaviorSubject<boolean>(
      this.paginationService.getCardSearch()
    );
    this.cardSearchObs = this.cardSearchSubject.asObservable();

    this.form = new FormGroup({
      createdByCtrl: new FormControl(),
      headlineCtrl: new FormControl(),
      tagsCtrl: new FormControl()
    });
  }

  ngOnInit(): void {
    this.refreshContent();
  }

  ngOnDestroy(): void {
    this.onDestroy.next();
  }

  nextPage(): void {
    if (this.cardOptions.currentPage < this.totalPages) {
      this.updatePage(this.cardOptions.currentPage + 1);
    }
  }

  previousPage(): void {
    if (this.cardOptions.currentPage > 1) {
      this.updatePage(this.cardOptions.currentPage - 1);
    }
  }

  refreshContent(): void {
    const options = this.paginationService.getOptionsForImages(
      this.cardOptions
    );
    this.fileService
      .paginate(options)
      .pipe(takeUntil(this.onDestroy))
      .subscribe((data: File[]) => {
        if (data) {
          this.searchResultsChange.emit(data);
          this.totalResultCount =
            !!data && data.length > 0 ? data[0].totalCount : 0;
          this.totalResultCountLabel =
            !!data && data.length > 0
              ? data[0].totalCount
                  .toString()
                  .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
              : "";
          this.totalPages = Math.ceil(this.totalResultCount / 20);
          this.resultCount = !!data && data.length > 0 ? data.length : 0;
          this.disabledCardControls = false;
        }
      });
  }

  updatePage(newPage: number): void {
    this.disabledCardControls = true;
    this.cardOptions.currentPage = newPage;
    this.refreshContent();
  }

  search(): void {
    this.disabledCardControls = true;

    this.cardOptions = {
      currentPage: 1,
      createdBy: this.form.get("createdByCtrl").value,
      tags: this.form.get("tagsCtrl").value,
      title: this.form.get("headlineCtrl").value
    };

    this.refreshContent();
  }

  toggleSearch(): void {
    this.form.get("createdByCtrl").setValue("");
    this.form.get("tagsCtrl").setValue("");
    this.form.get("headlineCtrl").setValue("");
    this.search();

    const toggled = !this.cardSearchSubject.getValue();
    this.cardSearchSubject.next(toggled);
    this.paginationService.setCardSearch(toggled);
  }
}
