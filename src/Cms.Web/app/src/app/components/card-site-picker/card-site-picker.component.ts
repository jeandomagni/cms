﻿import {
  Component,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef
} from "@angular/core";
import { MatAutocompleteSelectedEvent } from "@angular/material/autocomplete";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Observable, of } from "rxjs";

import { AuthService } from "src/app/auth/auth.service";
import { BaseAutocompleteComponent } from "src/app/components/base/base-autocomplete/base-autocomplete.component";
import { LegacyBbbInfo } from "src/app/shared/models/bbb/legacy-bbb-info.model";

@Component({
  selector: "card-site-picker",
  templateUrl: "./card-site-picker.component.html"
})
export class CardSitePickerComponent extends BaseAutocompleteComponent<
  LegacyBbbInfo
> {
  @Input() canRemove: boolean;
  @Input() selectedSites: LegacyBbbInfo[];
  @Output() selectedSitesChange: EventEmitter<
    LegacyBbbInfo[]
  > = new EventEmitter();
  @Output() update: EventEmitter<void> = new EventEmitter();

  allBbbs: LegacyBbbInfo[];
  notFoundMessage = "No matching sites found.";
  selectable = true;

  @ViewChild("siteInput", { static: false }) siteInput: ElementRef;

  constructor(private authService: AuthService, private snackBar: MatSnackBar) {
    super();

    this.allBbbs = this.authService.getSites();
  }

  select(event: MatAutocompleteSelectedEvent): void {
    const bbb = event.option.value;

    if (this.selectedSites.find(item => item.legacyBBBID === bbb.legacyBBBID)) {
      this.snackBar.open(`You already added ${bbb.bbbName}.`, "", {
        duration: 1000
      });

      return;
    }

    this.selectedSites.push(bbb);
    this.selectedSitesChange.emit(this.selectedSites);
    this.update.emit();

    this.siteInput.nativeElement.value = "";
    this.acCtrl.setValue(null);
  }

  remove(bbbName: string): void {
    const sites = this.selectedSites.filter(x => x.bbbName !== bbbName);
    this.selectedSitesChange.emit(sites);

    this.update.emit();
  }

  protected queryItems(value: string): Observable<LegacyBbbInfo[]> {
    return of(
      this.allBbbs.filter(x => x.bbbName.toLowerCase().indexOf(value) >= 0)
    );
  }
}
