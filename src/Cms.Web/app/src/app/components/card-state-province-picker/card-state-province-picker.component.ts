﻿import {
  Component,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
  OnChanges
} from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { MatAutocompleteSelectedEvent } from "@angular/material/autocomplete";
import { Observable, of } from "rxjs";
import { map, catchError } from "rxjs/operators";

import { ServiceAreaService } from "src/app/services/service-area.service";
import { Geotag } from "src/app/shared/models/location/geotag.interface";
import { BaseAutocompleteComponent } from "src/app/components/base/base-autocomplete/base-autocomplete.component";

@Component({
  selector: "card-state-province-picker",
  templateUrl: "./card-state-province-picker.component.html"
})
export class CardStateProvincePickerComponent
  extends BaseAutocompleteComponent<string>
  implements OnChanges {
  @Input() contentCode: string;
  @Input() editable: boolean;
  @Input() selected: Geotag[];
  @Output() remove: EventEmitter<Geotag> = new EventEmitter();
  @Output() update: EventEmitter<Geotag> = new EventEmitter();

  allowPin: boolean = true;
  notFoundMessage = "No states found.";
  selectable = true;

  @ViewChild("stateInput", { static: false }) stateInput: ElementRef;

  constructor(
    private serviceAreaService: ServiceAreaService,
    private snackBar: MatSnackBar
  ) {
    super();
  }

  ngOnChanges(): void {
    if (!this.editable && this.acCtrl) {
      this.acCtrl.disable();
    }
  }

  removeState(geotag: Geotag): void {
    this.remove.emit(geotag);
  }

  selectState(event: MatAutocompleteSelectedEvent): void {
    const state = event.option.value;

    if (this.selected.find(item => item.locationId === state)) {
      this.snackBar.open(`You already added ${state}.`, "", {
        duration: 1000
      });

      return;
    }

    this.update.emit({
      contentCode: this.contentCode,
      isPinned: false,
      locationId: state,
      locationType: "state"
    });

    this.stateInput.nativeElement.value = "";
    this.acCtrl.setValue(null);
  }

  setExpiration(geotag: Geotag): void {
    this.update.emit(geotag);
  }

  togglePin(geotag: Geotag): void {
    const twoWeeksLater = new Date(
      new Date().setDate(new Date().getDate() + 7)
    );
    this.update.emit({
      ...geotag,
      pinExpirationDate: !geotag.isPinned ? twoWeeksLater : null,
      isPinned: !geotag.isPinned
    });
  }

  protected queryItems(value: string): Observable<string[]> {
    return this.serviceAreaService.getFilteredStatesForUser(this.filterByCountries.map(x => x.locationId)).pipe(
      map((states: string[]) => {
        const suggestions = states.map(x => x.split(' ')[0]).filter(x => x.toLowerCase().includes(value.toLowerCase()));
        return suggestions.length > 0 ? suggestions : null;
      }),
      catchError(_ => of(null))
    );
  }
}
