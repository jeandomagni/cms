﻿import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  ElementRef,
  ViewChild
} from "@angular/core";
import { Observable, of } from "rxjs";

import { AuthService } from "src/app/auth/auth.service";
import { NotificationService } from "src/app/services/notification.service";
import { Geotag } from "src/app/shared/models/location/geotag.interface";
import { LegacyBbbInfo } from "src/app/shared/models/bbb/legacy-bbb-info.model";
import { BaseAutocompleteComponent } from "src/app/components/base/base-autocomplete/base-autocomplete.component";

@Component({
  selector: "card-service-area-picker",
  templateUrl: "./card-service-area-picker.component.html"
})
export class CardServiceAreaPickerComponent
  extends BaseAutocompleteComponent<LegacyBbbInfo>
  implements OnChanges {
  @Input() contentCode: string;
  @Input() selected: Geotag[];
  @Output() remove: EventEmitter<Geotag> = new EventEmitter();
  @Output() update: EventEmitter<Geotag> = new EventEmitter();

  allBbbs: LegacyBbbInfo[];
  expirationDate: Date;
  isAdmin: boolean;
  isPinned: boolean;
  isTagged: boolean;
  notFoundBbb = new LegacyBbbInfo();
  notFoundMessage = "No BBBs found.";
  removable = true;
  selectable = true;
  userBbbGeotag: Geotag;
  userBbbId: string;

  @ViewChild("serviceAreaInput", { static: false })
  serviceAreaInput: ElementRef;

  constructor(
    private authService: AuthService,
    private notificationService: NotificationService
  ) {
    super();

    this.isAdmin = this.authService.isAdmin;
    this.allBbbs = this.authService.getSites();
  }

  ngOnChanges(): void {
    if (!this.isAdmin && this.selected !== null) {
      this.userBbbId = this.authService.currentUser.profile.defaultSite;
      this.userBbbGeotag =
        this.selected.find(x => x.locationId === this.userBbbId) ||
        new Geotag();
      this.isTagged = !!this.userBbbGeotag.locationId;
      this.isPinned = this.userBbbGeotag.isPinned;
    }
  }

  removeLocation(geotag: Geotag): void {
    this.remove.emit(geotag);
  }

  selectLocation(entity: LegacyBbbInfo): void {
    if (!entity) {
      return;
    }

    if (this.selected.find(item => item.locationId === entity.legacyBBBID)) {
      this.notificationService.error(`You already added ${entity.legacyBBBID}`);
      return;
    }

    const geotag: Geotag = {
      contentCode: this.contentCode,
      isPinned: false,
      locationId: entity.legacyBBBID,
      locationType: "bbbId"
    };
    this.update.emit(geotag);

    this.serviceAreaInput.nativeElement.value = "";
    this.acCtrl.setValue(null);
  }

  setExpiration(geotag: Geotag): void {
    this.update.emit(geotag);
  }

  togglePin(geotag: Geotag): void {
    const twoWeeksLater = new Date(
      new Date().setDate(new Date().getDate() + 7)
    );

    this.update.emit({
      ...geotag,
      pinExpirationDate: !geotag.isPinned ? twoWeeksLater : null,
      isPinned: !geotag.isPinned
    });
  }

  toggleUserTag(): void {
    if (this.isTagged) {
      this.update.emit(this.buildUserGeotag(false));
    } else {
      this.remove.emit(this.buildUserGeotag(false));
    }
  }

  toggleUserPin(): void {
    if (this.isPinned) {
      this.update.emit(this.buildUserGeotag(true));
    } else {
      this.update.emit(this.buildUserGeotag(false));
    }
  }

  private buildUserGeotag(withPin: boolean): Geotag {
    const geotag = {
      contentCode: this.contentCode,
      isPinned: withPin,
      locationId: this.userBbbId,
      locationType: "bbbId",
      pinExpirationDate:
        this.userBbbGeotag.pinExpirationDate ||
        new Date(new Date().setDate(new Date().getDate() + 7)),
      pinId: this.userBbbGeotag.pinId
    };

    return geotag;
  }

  protected queryItems(value: string): Observable<LegacyBbbInfo[]> {
    return of(
      this.allBbbs
        .filter(bbb => this.filterByCountries.length == 0 || this.filterByCountries.filter(c => c.locationId == bbb.countryCode).length > 0)
        .filter(bbb => bbb.bbbName.toLowerCase().includes(value))
    );
  }
}
