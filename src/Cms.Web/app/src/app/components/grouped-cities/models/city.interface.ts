import { Location } from "src/app/shared/models/location/location.interface";

export interface City extends Location {
  expirationDate: Date;
}
