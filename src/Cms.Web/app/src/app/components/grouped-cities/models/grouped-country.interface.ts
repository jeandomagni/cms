import { GroupedState } from "./grouped-state.interface";

export interface GroupedCountry {
  name: string;
  states: GroupedState[];
}
