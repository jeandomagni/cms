import { City } from "./city.interface";

export interface GroupedState {
  cities: City[];
  name: string;
}
