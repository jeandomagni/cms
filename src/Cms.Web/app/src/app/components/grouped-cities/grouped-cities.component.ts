import {
    Component,
    Input,
    Output,
    EventEmitter,
    OnChanges,
    OnDestroy
  } from "@angular/core";
  import { FormControl } from "@angular/forms";
  import { Observable } from "rxjs";
  
  import { NotificationService } from "src/app/services/notification.service";
  import { SolrService } from "src/app/services/solr.service";
  import { Location } from "src/app/shared/models/location/location.interface";
  import { Pin } from "src/app/shared/models/location/pin.interface";
  import { ContentLocation } from "src/app/shared/models/location/content-location.interface";
  
  import { City } from "./models/city.interface";
  import { GroupedCountry } from "./models/grouped-country.interface";
  
  @Component({
    selector: "grouped-cities",
    templateUrl: "./grouped-cities.component.html",
    styleUrls: ["./grouped-cities.component.scss"]
  })
  export class GroupedCitiesComponent implements OnChanges {

    @Input() pinned: Location[];
    @Input() pinnedData: Pin[];
    @Input() selected: Location[];
    @Output() add: EventEmitter<ContentLocation> = new EventEmitter();
    @Output() addPin: EventEmitter<Pin> = new EventEmitter();
    @Output() remove: EventEmitter<ContentLocation> = new EventEmitter();
    @Output() removePin: EventEmitter<Pin> = new EventEmitter();
    @Output() updatePin: EventEmitter<Pin> = new EventEmitter();
  
    groupedLocations: GroupedCountry[] = [];
    notFoundMessage = "No city found.";
    selectable = true;
    countryCtrl: FormControl = new FormControl("USA");
  
    constructor(
      private notificationService: NotificationService,
      private solrService: SolrService
    ) {
    }
  
    ngOnChanges(): void {
      this.groupedLocations = this.groupLocations();
      this.selected = this.selected || [];
    }
  
    isPinned = (entity: Location): boolean =>
      !!this.pinned.find(e => e.id === entity.id);
  
    pinLocation(city: Location): void {
      const oneWeekLater = new Date(new Date().setDate(new Date().getDate() + 7));
  
      this.addPin.emit({
        pinnedLocation: JSON.stringify(city),
        locationType: "city",
        pinExpirationDate: oneWeekLater
      });
    }
  
    removeLocation(city: Location): void {
      const locationToRemove = { location: JSON.stringify(city), type: "city" };
      this.remove.emit(locationToRemove);
    }
  
    setExpiration(city: City): void {
      const index = this.pinnedData.findIndex(
        (pin: Pin) =>
          pin.locationType === "city" && pin.pinnedLocation === city.id
      );
  
      this.updatePin.emit({
        id: index !== -1 ? this.pinnedData[index].id : 0,
        pinnedLocation: JSON.stringify(city),
        locationType: "city",
        pinExpirationDate: city.expirationDate
      });
    }
  
    unpinLocation(city: City): void {
      const pinIndex = this.pinnedData.findIndex(
        (pin: Pin) =>
          pin.locationType === "city" && pin.pinnedLocation === city.id
      );
      if (pinIndex !== -1) {
        this.removePin.emit({
          ...this.pinnedData[pinIndex],
          pinnedLocation: JSON.stringify(city)
        });
      }
    }
  
    private groupLocations(): GroupedCountry[] {
      const locations = this.getLocationsWithExpirationDates();
      const countriesArray: GroupedCountry[] = [];
      locations.forEach(obj => {
        const indexOfCountry = countriesArray.findIndex(
          x => x.name === obj.countryName
        );
  
        if (indexOfCountry === -1) {
          countriesArray.push({
            name: obj.countryName,
            states: [{ name: obj.stateFull, cities: [obj] }]
          });
          return;
        }
  
        const indexOfState = countriesArray[indexOfCountry].states.findIndex(
          x => x.name === obj.stateFull
        );
        if (indexOfState === -1) {
          countriesArray[indexOfCountry].states.push({
            name: obj.stateFull,
            cities: [obj]
          });
          return;
        }
  
        if (
          !countriesArray[indexOfCountry].states[indexOfState].cities.find(
            x => obj.city === x.city
          )
        ) {
          countriesArray[indexOfCountry].states[indexOfState].cities.push(obj);
        }
      });
  
      return countriesArray;
    }
  
    private getLocationsWithExpirationDates = (): City[] => {
      return this.selected.map(loc => {
        const pinIndex = this.isPinned(loc)
          ? this.pinnedData.findIndex(
              pin => loc.id === pin.pinnedLocation && pin.locationType === "city"
            )
          : -1;
  
        return {
          ...loc,
          expirationDate:
            pinIndex !== -1
              ? new Date(this.pinnedData[pinIndex].pinExpirationDate)
              : null
        };
      });
    };
  }
  