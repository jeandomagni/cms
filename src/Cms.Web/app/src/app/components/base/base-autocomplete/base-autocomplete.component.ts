import { Component, OnDestroy, Input } from '@angular/core';
import { Observable, of } from "rxjs";
import { FormControl } from "@angular/forms";
import {
  startWith,
  debounceTime,
  tap,
  switchMap,
  map,
  catchError
} from "rxjs/operators";
import { DOWN_ARROW, UP_ARROW } from "@angular/cdk/keycodes";
import { Geotag } from 'src/app/shared/models/location/geotag.interface';

@Component({
  selector: "cms-base-autocomplete",
  template: ""
})
export class BaseAutocompleteComponent<T> implements OnDestroy {
  acCtrl: FormControl = new FormControl();
  filtered: Observable<T[]>;
  keydownFn: EventListener;
  highlightedInputValue: string;
  inputValue: string = "";
  isNotFound = false;
  notFoundMessage: string = "No items found.";
  @Input() showOnFocus: boolean;
  private _filterByCountries: Geotag[]; 
  get filterByCountries(): Geotag[] { 
    return this._filterByCountries;
  }
  @Input()
  set filterByCountries(val: Geotag[]) {
    this._filterByCountries = val;
    this.populateFilter();
  }

  constructor() {
    this.populateFilter();

    this.keydownFn = (evt: KeyboardEvent): void => {
      if (evt.keyCode === DOWN_ARROW || evt.keyCode === UP_ARROW) {
        if (this.inputValue === this.notFoundMessage) {
          evt.stopPropagation();
        }
      }
    };

    document.addEventListener("keydown", this.keydownFn, true);
  }

  populateFilter () {
    this.filtered = this.acCtrl.valueChanges.pipe(
      startWith(""),
      debounceTime(300),
      tap(value => {
        this.inputValue = value || "";
      }),
      switchMap(() => this.filter())
    );
  }

  ngOnDestroy() {
    document.removeEventListener("keydown", this.keydownFn, true);
  }

  protected queryItems(_searchValue: string): Observable<T[]> {
    return of([]);
  }

  private filter(): Observable<T[]> {
    if (!this.showOnFocus && (!this.inputValue || typeof this.inputValue !== "string")) {
      return of(null);
    }

    this.highlightedInputValue = this.inputValue;
    const searchValue = this.inputValue.toLowerCase();

    return this.queryItems(searchValue).pipe(
      map((suggestions: T[]) => {
        if (suggestions != null && suggestions.length > 0) {
          this.isNotFound = false;
          return suggestions;
        }

        this.isNotFound = true;
        this.highlightedInputValue = "";
        return [];
      }),
      catchError(_ => of(null))
    );
  }
}
