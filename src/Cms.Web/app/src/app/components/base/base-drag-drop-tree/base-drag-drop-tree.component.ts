import { CdkDragDrop } from "@angular/cdk/drag-drop";
import { FlatTreeControl } from "@angular/cdk/tree";
import { Component, EventEmitter } from "@angular/core";
import { MatTreeFlatDataSource, MatTreeFlattener } from "@angular/material";
import { Observable, of } from "rxjs";
import { takeUntil } from "rxjs/operators";

import { ConfirmDialogService } from "src/app/services/confirm-dialog.service";

import { TreeNode } from "./tree-node.interface";

@Component({
  selector: "base-drag-drop-tree",
  template: ""
})
export class BaseDragDropTreeComponent<T> {
  addEditItem = (item: T): Observable<T> => of(item);
  allowChildren: boolean;
  allowItemEdit: boolean;
  childrenPropertyName = "children";
  dragging = false;
  dataSource: MatTreeFlatDataSource<T, TreeNode>;
  expandTimeout: any;
  expandDelay = 1000;
  expandedNodeSet = new Set<string>();
  generateLabel = (_node: T) => "";
  items: T[] = [];
  treeControl: FlatTreeControl<TreeNode>;
  treeFlattener: MatTreeFlattener<T, TreeNode>;
  update: (items: T[]) => void;
  protected onDestroy: EventEmitter<void> = new EventEmitter();

  constructor(protected confirmDialogService: ConfirmDialogService) {
    this.treeFlattener = new MatTreeFlattener(
      this.transformer,
      this.getLevel,
      this.isExpandable,
      node => of(this.getChildren(node))
    );
    this.treeControl = new FlatTreeControl<TreeNode>(
      this.getLevel,
      this.isExpandable
    );
    this.dataSource = new MatTreeFlatDataSource(
      this.treeControl,
      this.treeFlattener
    );
  }

  ngOnDestroy(): void {
    this.onDestroy.next();
  }

  addChild(nodeToAssignChild: TreeNode): void {
    this.addEditItem(null)
      .pipe(takeUntil(this.onDestroy))
      .subscribe(item => {
        if (item) {
          const childIndex = this.items.findIndex(
            x => this.generateLabel(x) === nodeToAssignChild.label
          );

          this.items[childIndex] = this.setChildren(this.items[childIndex], [
            ...this.getChildren(this.items[childIndex]),
            item
          ]);
          this.update(this.items);
        }
      });
  }

  deleteItem(node: TreeNode): void {
    this.confirmDialogService
      .showConfirmDelete()
      .pipe(takeUntil(this.onDestroy))
      .subscribe(confirmed => {
        if (confirmed) {
          if (node.level === 0) {
            this.items = this.items.filter(
              x => this.generateLabel(x) !== node.label
            );
          } else {
            const parentIndex = this.items.findIndex(x =>
              this.getChildren(x).find(
                y => this.generateLabel(y) === node.label
              )
            );

            this.items[parentIndex] = this.setChildren(
              this.items[parentIndex],
              this.getChildren(this.items[parentIndex]).filter(
                x => this.generateLabel(x) !== node.label
              )
            );
          }

          this.update(this.items);
        }
      });
  }

  dragStart(): void {
    this.dragging = true;
  }

  dragEnd(): void {
    this.dragging = false;
  }

  dragHover(node: TreeNode): void {
    if (this.dragging) {
      clearTimeout(this.expandTimeout);
      this.expandTimeout = setTimeout(() => {
        this.treeControl.expand(node);
      }, this.expandDelay);
    }
  }

  dragHoverEnd(): void {
    if (this.dragging) {
      clearTimeout(this.expandTimeout);
    }
  }

  drop(event: CdkDragDrop<string[]>): void {
    // ignore drops outside of the tree
    if (!event.isPointerOverContainer) return;

    const visibleNodes = this.getVisibleNodes();

    // deep clone the data source so we can mutate it
    const changedData = JSON.parse(JSON.stringify(this.dataSource.data));

    // recursive find function to find siblings of node
    const findNodeSiblings = (arr: Array<T>, id: string): Array<T> => {
      let result: T[];
      let subResult: T[];
      arr.forEach(item => {
        const children = this.getChildren(item);
        if (this.generateLabel(item) === id) {
          result = arr;
        } else if (!!children && children.length > 0) {
          subResult = findNodeSiblings(children, id);
          if (subResult) result = subResult;
        }
      });

      return result;
    };

    // remove the node from its old place
    const node: TreeNode = event.item.data;
    const siblings = findNodeSiblings(changedData, node.label);
    const siblingIndex = siblings.findIndex(
      n => this.generateLabel(n) === node.label
    );
    const nodeToInsert = siblings.splice(siblingIndex, 1)[0];

    // determine where to insert the node
    const nodeAtDest = visibleNodes[event.currentIndex];

    if (this.generateLabel(nodeAtDest) === this.generateLabel(nodeToInsert))
      return;

    // determine drop index relative to destination array
    let relativeIndex = event.currentIndex; // default if no parent
    const nodeAtDestFlatNode = this.treeControl.dataNodes.find(
      n => this.generateLabel(nodeAtDest) === n.label
    );
    const parent = this.getParentNode(nodeAtDestFlatNode);
    if (parent) {
      const parentIndex =
        visibleNodes.findIndex(n => this.generateLabel(n) === parent.label) + 1;
      relativeIndex = event.currentIndex - parentIndex;
    }
    // insert node
    const newSiblings = findNodeSiblings(
      changedData,
      this.generateLabel(nodeAtDest)
    );
    if (!newSiblings) return;
    newSiblings.splice(relativeIndex, 0, nodeToInsert);

    // rebuild tree with mutated data
    this.rebuildTreeForData(changedData);
  }

  editItem(node: TreeNode): void {
    let item: T;
    let parentIndex: number;
    let index: number;

    if (node.level === 0) {
      index = this.items.findIndex(x => this.generateLabel(x) === node.label);
      item = this.items[index];
    } else {
      parentIndex = this.items.findIndex(x =>
        this.getChildren(x).find(y => this.generateLabel(y) === node.label)
      );
      index = this.getChildren(this.items[parentIndex]).findIndex(
        x => this.generateLabel(x) === node.label
      );
      item = this.getChildren(this.items[parentIndex])[index];
    }

    this.addEditItem({ ...item })
      .pipe(takeUntil(this.onDestroy))
      .subscribe(updatedItem => {
        if (updatedItem) {
          if (node.level === 0) {
            this.items[index] = updatedItem;
          } else {
            const children = this.getChildren(this.items[parentIndex]);
            children[index] = updatedItem;
            this.items[parentIndex] = this.setChildren(
              this.items[parentIndex],
              children
            );
          }
          this.update(this.items);
          return;
        }
      });
  }

  getChildren = (item: T): T[] => {
    console.log("gettin");
    if (!this.allowChildren) {
      return [];
    }

    if (item.hasOwnProperty(this.childrenPropertyName)) {
      console.log("has", item);
      return item[this.childrenPropertyName] || [];
    }
  };

  setChildren(item: T, childrenToSet: T[]) {
    if (!this.allowChildren || item === null) {
      return item;
    }

    item[this.childrenPropertyName] = childrenToSet;
    return item;
  }

  private expandNodesById(): void {
    if (!this.treeControl.dataNodes || this.treeControl.dataNodes.length === 0)
      return;
    this.treeControl.dataNodes.forEach(node => {
      if (this.expandedNodeSet.has(node.label)) {
        this.treeControl.expand(node);
        let parent = this.getParentNode(node);
        while (parent) {
          this.treeControl.expand(parent);
          parent = this.getParentNode(parent);
        }
      }
    });
  }

  private forgetMissingExpandedNodes(): void {
    const expandedNodes = [...this.expandedNodeSet].filter(nodeId =>
      this.treeControl.dataNodes.find(treeNode => treeNode.label === nodeId)
    );
    this.expandedNodeSet = new Set(expandedNodes);
  }

  private getLevel = (node: TreeNode): number => node.level;

  private getParentNode(node: TreeNode): TreeNode | null {
    if (node.level === 0) {
      return null;
    }

    return this.treeControl.dataNodes
      .slice(0, this.treeControl.dataNodes.indexOf(node))
      .find(x => x.level < node.level);
  }

  private getVisibleNodes(): T[] {
    this.rememberExpandedTreeNodes();
    const result = [];

    const addExpandedChildren = (node: T, expanded: Set<string>): void => {
      result.push(node);
      if (expanded.has(this.generateLabel(node))) {
        this.getChildren(node).map(child =>
          addExpandedChildren(child, expanded)
        );
      }
    };
    this.dataSource.data.forEach(node => {
      addExpandedChildren(node, this.expandedNodeSet);
    });
    return result;
  }

  private isExpandable = (node: TreeNode): boolean => node.expandable;

  private rebuildTreeForData(data: T[]): void {
    this.dataSource.data = data;

    this.rememberExpandedTreeNodes();

    this.update(data);

    this.forgetMissingExpandedNodes();
    this.expandNodesById();
  }

  private rememberExpandedTreeNodes(): void {
    if (this.treeControl.dataNodes) {
      this.treeControl.dataNodes.forEach(node => {
        if (
          this.treeControl.isExpandable(node) &&
          this.treeControl.isExpanded(node)
        ) {
          // capture latest expanded state
          this.expandedNodeSet.add(node.label);
        }
      });
    }
  }

  private transformer = (node: T, level: number): TreeNode => {
    return {
      expandable: this.allowChildren && this.getChildren(node).length > 0,
      label: this.generateLabel(node),
      level
    };
  };
}
