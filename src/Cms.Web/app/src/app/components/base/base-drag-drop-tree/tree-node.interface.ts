export interface TreeNode {
  expandable: boolean;
  label: string;
  level: number;
}
