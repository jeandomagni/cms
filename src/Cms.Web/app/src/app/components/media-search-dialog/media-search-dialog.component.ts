﻿import { Component } from "@angular/core";
import { FormControl } from "@angular/forms";
import { MatDialogRef } from "@angular/material";
import {
  map,
  catchError,
  startWith,
  debounceTime,
  switchMap
} from "rxjs/operators";
import { of, Observable } from "rxjs";

import { FileService } from "src/app/services/file.service";
import { File } from "src/app/shared/models/file/file.model";
import { GridOptions } from "src/app/shared/models/table/grid-options/grid-options.model";

@Component({
  selector: "media-search",
  templateUrl: "./media-search-dialog.component.html"
})
export class MediaSearchDialogComponent {
  ctrl = new FormControl();
  queryOptions: GridOptions;
  images: Observable<File[]>;

  constructor(
    private fileService: FileService,
    private dialogRef: MatDialogRef<MediaSearchDialogComponent>
  ) {
    this.queryOptions = new GridOptions();

    this.images = this.ctrl.valueChanges.pipe(
      startWith(""),
      debounceTime(300),
      switchMap(value => this.filter(value))
    );
  }

  add(item: File): void {
    this.dialogRef.close(item);
  }

  closeDialog(): void {
    this.dialogRef.close();
  }

  private filter(searchValue: string): Observable<File[]> {
    const filters = {
      logic: "and",
      filters: [
        {
          field: "title",
          operator: "contains",
          value: searchValue
        }
      ]
    };

    this.queryOptions.data.filter = searchValue ? filters : null;
    return this.fileService.paginate(this.queryOptions).pipe(
      map((files: File[]) => files),
      catchError(_ => of(null))
    );
  }
}
