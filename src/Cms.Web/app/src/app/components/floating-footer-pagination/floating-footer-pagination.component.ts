import {
  Component,
  Input,
  EventEmitter,
  Output,
  OnChanges
} from "@angular/core";

@Component({
  selector: "floating-footer-pagination",
  templateUrl: "./floating-footer-pagination.component.html"
})
export class FloatingFooterPaginationComponent implements OnChanges {
  @Input() currentPage: number;
  @Input() disabled: boolean;
  @Input() hideSpacer: boolean;
  @Input() resultCount: number;
  @Input() resultsOnPage: number;
  @Input() totalPages: number;
  @Output() updatePage: EventEmitter<number> = new EventEmitter();

  label: string;

  ngOnChanges(): void {
    this.label = `${this.resultsOnPage} results in page ${this.currentPage}/${this.totalPages} for ${this.resultCount} records`;
  }

  nextPage(): void {
    if (this.currentPage < this.totalPages) {
      this.updatePage.emit(this.currentPage + 1);
    }
  }

  previousPage(): void {
    if (this.currentPage > 1) {
      this.updatePage.emit(this.currentPage - 1);
    }
  }
}
