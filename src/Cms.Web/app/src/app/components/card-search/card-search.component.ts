﻿import {
  Component,
  EventEmitter,
  Output,
  Input,
  OnChanges
} from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Observable, BehaviorSubject } from "rxjs";
import { takeUntil } from "rxjs/operators";

import { AuthService } from "src/app/auth/auth.service";
import { ContentService } from "src/app/services/content.service";
import { PaginationService } from "src/app/services/pagination.service";
import { ContentPreview } from "src/app/shared/models/content/content-preview.interface";
import { ContentQuery } from "src/app/shared/models/content/query/content-query.model";
import { ContentSearchSettings } from "src/app/shared/models/content/query/content-search-settings.model";

@Component({
  selector: "card-search",
  templateUrl: "./card-search.component.html",
  styleUrls: ["./card-search.component.scss"]
})
export class CardSearchComponent implements OnChanges {
  @Input() hideContentField: boolean;
  @Input() hideTagsField: boolean;
  @Input() hideServiceAreaField: boolean;
  @Input() hideCreatedByField: boolean;
  @Input() showSearchButton: boolean;

  @Input() filterBy: string;
  @Input() results: ContentPreview[];
  @Output() resultsChange: EventEmitter<ContentPreview[]> = new EventEmitter();

  @Input() loading: boolean;
  @Output() loadingChange: EventEmitter<boolean> = new EventEmitter();

  cardOptions: ContentQuery = new ContentQuery();
  cardSearch: boolean;
  cardSearchObs: Observable<boolean>;
  disabledCardControls = true;
  fetchedForType = "";
  form: FormGroup;
  resultCount = 0;
  searchSettings = new ContentSearchSettings();
  totalPages = 1;
  totalResultCount = 0;
  totalResultCountLabel: string;
  private readonly onDestroy: EventEmitter<void> = new EventEmitter();

  private cardSearchSubject: BehaviorSubject<boolean>;

  constructor(
    private authService: AuthService,
    private contentService: ContentService,
    private paginationService: PaginationService
  ) {
    this.cardSearchSubject = new BehaviorSubject<boolean>(
      this.paginationService.getCardSearch()
    );
    this.cardSearchObs = this.cardSearchSubject.asObservable();

    this.form = new FormGroup({
      articleNumberCtrl: new FormControl(null, [
        Validators.pattern("^[0-9]*$")
      ]),
      contentCtrl: new FormControl("", [Validators.maxLength(50)]),
      createdByCtrl: new FormControl(),
      headlineCtrl: new FormControl(),
      tagsCtrl: new FormControl(),
      serviceAreaCtrl: new FormControl()
    });

    this.cardOptions = paginationService.getOptionsForCards(
      this.getSearchSettings()
    );
  }

  ngOnChanges(): void {
    if (this.filterBy !== this.fetchedForType) {
      this.refreshContent();
    }
  }

  search(): void {
    this.disabledCardControls = true;
    this.cardOptions.page = 1;
    this.refreshContent();
  }

  nextPage(): void {
    if (this.cardOptions.page < this.totalPages) {
      this.updatePage(this.cardOptions.page + 1);
    }
  }

  previousPage(): void {
    if (this.cardOptions.page > 1) {
      this.updatePage(this.cardOptions.page - 1);
    }
  }

  updatePage(newPage: number): void {
    this.disabledCardControls = true;
    this.cardOptions.page = newPage;
    this.refreshContent();
  }

  private getSearchSettings(): ContentSearchSettings {
    return {
      filterBy: this.filterBy || "All Drafts",
      id: this.form.get("articleNumberCtrl").value,
      content: this.form.get("contentCtrl").value,
      createdBy: this.form.get("createdByCtrl").value,
      tags: this.form.get("tagsCtrl").value,
      title: this.form.get("headlineCtrl").value,
      serviceArea: this.form.get("serviceAreaCtrl").value,
      isAdmin: this.authService.hasRole("episerveradmin"),
      page: this.cardOptions.page
    };
  }

  public refreshContent(): void {
    this.loadingChange.emit(true);

    const locations = this.authService.currentUser.sites.map(
      loc => loc.legacyBBBID
    );

    this.cardOptions = {
      ...this.cardOptions,
      ...this.paginationService.getOptionsForCards(this.getSearchSettings()),
      checkedOutBy:
        this.filterBy === "Checked Out"
          ? this.authService.currentUser.email
          : null,
        locations: (this.filterBy === "All Content")
          ? null :
          !this.authService.isGlobalAdmin ? locations.join() : null
    };

    this.contentService
      .paginate(this.cardOptions)
      .pipe(takeUntil(this.onDestroy))
      .subscribe((data: ContentPreview[]) => {
        if (!data) {
          return;
        }

        this.resultsChange.emit(data);
        this.totalResultCount =
          !!data && data.length > 0 ? data[0].totalCount : 0;
        this.totalResultCountLabel =
          !!data && data.length > 0
            ? data[0].totalCount
                .toString()
                .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
            : "";
        this.totalPages = Math.ceil(this.totalResultCount / 20);
        this.resultCount = !!data && data.length > 0 ? data.length : 0;
        this.disabledCardControls = false;

        this.fetchedForType = this.filterBy;

        this.loadingChange.emit(false);
      }, error => { this.loadingChange.emit(false); });
  }

  onFormSubmit(event) {
    if ( event.keyCode === 13) {
      this.search();
    }
  }
}
