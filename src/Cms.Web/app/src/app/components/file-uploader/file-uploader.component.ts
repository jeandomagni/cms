import { COMMA, ENTER } from "@angular/cdk/keycodes";
import {
  Component,
  ElementRef,
  ViewChild,
  OnDestroy,
  OnInit,
  EventEmitter
} from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { MatAutocomplete } from "@angular/material/autocomplete";
import { MatChipInputEvent } from "@angular/material/chips";
import { ActivatedRoute, Params } from "@angular/router";
import { FileUploader } from "ng2-file-upload";
import { Observable } from "rxjs";
import { takeUntil } from "rxjs/operators";

import { BaseAutocompleteComponent } from "src/app/components/base/base-autocomplete/base-autocomplete.component";
import { NotificationService } from "src/app/services/notification.service";
import { FileTag } from "src/app/shared/models/file/file-tag.interface";
import { BbbFileItem } from "src/app/shared/models/file/bbb-file-item.interface";
import { FileService } from "src/app/services/file.service";

@Component({
  selector: "file-uploader",
  templateUrl: "./file-uploader.component.html",
  styleUrls: ["./file-uploader.styles.scss"]
})
export class FileUploaderComponent extends BaseAutocompleteComponent<FileTag>
  implements OnInit, OnDestroy {
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  addOnBlur = true;
  contentCode: string;
  notFoundMessage = "No matches found. Press enter to add.";
  removable = true;
  selectable = true;
  tagsForm: FormGroup;
  uploader: FileUploader = new FileUploader({
    url: "/api/files",
    headers: [
      {
        name: "cms-access-token",
        value: window.localStorage.getItem("authorizationData")
      }
    ]
  });

  @ViewChild("tagInput", { static: false }) tagInput: ElementRef<
    HTMLInputElement
  >;
  @ViewChild("auto", { static: false }) matAutocomplete: MatAutocomplete;

  private onDestroy: EventEmitter<void> = new EventEmitter();

  constructor(
    private fileService: FileService,
    private notificationService: NotificationService,
    private route: ActivatedRoute
  ) {
    super();

    this.tagsForm = new FormGroup({
      filterCtrl: new FormControl("*"),
      tagsCtrl: this.acCtrl
    });
    this.uploader.onBeforeUploadItem = this.onBeforeUploadItem;
    this.uploader.onAfterAddingFile = this.onAfterAddingFile;
    this.uploader.onBuildItemForm = this.onBuildItemForm;
    this.uploader.onErrorItem = this.onErrorItem;
    this.uploader.onSuccessItem = this.onSuccessItem;
  }

  ngOnInit(): void {
    this.route.queryParams
      .pipe(takeUntil(this.onDestroy))
      .subscribe((queryParams: Params) => {
        const { contentCode } = queryParams;
        this.contentCode = contentCode;
      });
  }

  ngOnDestroy(): void {
    this.onDestroy.next();
  }

  addTag(event: MatChipInputEvent, tags: FileTag[]): void {
    if (this.matAutocomplete.isOpen && this.isNotFound) {
      return; // to not conflict with select event
    }

    const { input } = event;
    const { value } = event;

    if ((value || "").trim()) {
      tags.push({
        label: `${value} :: generic`,
        type: "generic",
        value,
        name: value
      });
    }

    if (input) {
      input.value = "";
    }

    this.tagsForm.get("tagsCtrl").setValue(null);
  }

  removeTag(tagToRemove: FileTag, tags: FileTag[]): void {
    const index = tags.findIndex(x => tagToRemove.label === x.label);

    if (index >= 0) {
      tags.splice(index, 1);
    }
  }

  selectTag(tag: FileTag, tags: FileTag[]): void {
    tags.push(tag);
    this.tagsForm.get("tagsCtrl").setValue(null);
  }

  private onAfterAddingFile = (fileItem: BbbFileItem): void => {
    fileItem.tags = []; // eslint-disable-line no-param-reassign
    fileItem.imageTitle = fileItem.file.name.replace(/\.[^/.]+$/, "");
  };

  private onBuildItemForm = (fileItem: BbbFileItem, form: FormData): void => {
    form.append("tags", JSON.stringify(fileItem.tags));
    // form.append("imageDescription", fileItem.imageDescription || "");
    form.append("imageCaption", fileItem.imageCaption || "");
    form.append("imageCredit", fileItem.imageCredit || "");
    form.append("contentCode", this.contentCode);
    form.append("imageTitle", fileItem.imageTitle || "");
  };

  private onErrorItem = (fileItem: BbbFileItem, response: string): void => {
    fileItem.isError = true; // eslint-disable-line no-param-reassign
    fileItem.isUploaded = false; // assuming that if an error has been raised then file hasn't been uploaded.  Have to be set for Upload All disabled check to work
    let errMsg = fileItem.errMsg || response;
    try {
      ({ message: errMsg } = JSON.parse(response));
    } catch (e) {
      // swallow
    }
    this.notificationService.error(errMsg);
  };

  private onSuccessItem = (fileItem: BbbFileItem): void => {
    fileItem.isSuccess = true; // eslint-disable-line no-param-reassign
  };
  private onBeforeUploadItem = (fileItem: BbbFileItem): void => {
    if (!fileItem.imageTitle){
      fileItem.errMsg = 'Image title is required';
      throw Error(fileItem.errMsg);
    }
    if (!fileItem.imageCredit){
      fileItem.errMsg = 'Error: Need to add photo credit. For Getty Images, enter "Getty."';
      throw Error(fileItem.errMsg);
    }
  };

  protected queryItems(value: string): Observable<FileTag[]> {
    return this.fileService.suggestTags(
      value.toLowerCase(),
      this.tagsForm.get("filterCtrl").value
    );
  }
}
