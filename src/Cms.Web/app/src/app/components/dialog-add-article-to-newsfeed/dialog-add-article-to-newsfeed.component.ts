import {
    Component,
    Inject,
    ViewChild
  } from "@angular/core";
  import { MatAutocompleteSelectedEvent } from "@angular/material/autocomplete";
  import { MatSnackBar } from "@angular/material/snack-bar";
  import { Observable, of } from "rxjs";
  import { map, catchError } from "rxjs/operators";
  
  import { CardSearchComponent } from "../../components/card-search/card-search.component";
  import { ContentPreview } from "src/app/shared/models/content/content-preview.interface";
  import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
  import { ContentLocationService } from '../../services/content-location.service';
import { NotificationService } from 'src/app/services/notification.service';
import { ArticleOrderParams } from '../../shared/models/article-order-params.model';
  
  @Component({
    selector: "dialog-add-article-to-newsfeed",
    templateUrl: "./dialog-add-article-to-newsfeed.component.html",
    styleUrls: ["./dialog-add-article-to-newsfeed.component.scss"]
  })
  export class DialogAddArticleToNewsfeedComponent {
    filter: string = "All Content";
    content: ContentPreview[];
    articleOrder: ArticleOrderParams;
    loading: boolean;
    
    @ViewChild("cardSearch", { static: false }) cardSearch: CardSearchComponent;

    constructor(
      public dialogRef: MatDialogRef<DialogAddArticleToNewsfeedComponent>,
      private contentLocationService: ContentLocationService,
      private notificationService: NotificationService,
      @Inject( MAT_DIALOG_DATA ) public data?: ArticleOrderParams
    ) {
        this.articleOrder = data;
        
    }
  
    onNoClick(): void {
      this.dialogRef.close();
    }

    addToTop10(code: string) {
      this.contentLocationService.addToTop10(this.articleOrder, code).subscribe(response => {
        this.notificationService.success("Article was successfully added to top 10");
      });
    }

    getImageOrDefault = (imageUrl) => {
      if(imageUrl == null || imageUrl == "" || !this.validUrl(imageUrl)) {
        return "/images/image-placeholder.png";
      }
  
      return imageUrl;    
    }

    validUrl(str) {
      var res = str.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
      return (res !== null)
    }
  
  }
  