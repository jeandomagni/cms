﻿import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnInit,
  ViewChild,
  ElementRef
} from "@angular/core";
import { MatAutocompleteSelectedEvent } from "@angular/material/autocomplete";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Observable } from "rxjs";

import { AuthService } from "src/app/auth/auth.service";
import { BaseAutocompleteComponent } from "src/app/components/base/base-autocomplete/base-autocomplete.component";
import { SolrService } from "src/app/services/solr.service";
import { Category } from "src/app/shared/models/content/entities/category.interface";
import { Geotag } from "src/app/shared/models/location/geotag.interface";

@Component({
  selector: "category-picker",
  templateUrl: "./category-picker.component.html"
})
export class CategoryPickerComponent extends BaseAutocompleteComponent<Category>
  implements OnInit {
  @Input() countries: Geotag[];
  @Input() editable: boolean;
  @Input() excluded: string[];
  @Input() selected: Category[];
  @Output() excludedChange: EventEmitter<string[]> = new EventEmitter();
  @Output() selectedChange: EventEmitter<Category[]> = new EventEmitter();
  @Output() update: EventEmitter<void> = new EventEmitter();

  isAdmin: boolean;
  notFoundMessage = "No category found.";
  selectable = true;

  @ViewChild("categoryInput", { static: false }) categoryInput: ElementRef;

  constructor(
    private snackBar: MatSnackBar,
    private solrService: SolrService,
    authService: AuthService
  ) {
    super();

    this.isAdmin = authService.isAdmin;
  }

  ngOnInit(): void {
    this.excluded = this.excluded || [];
    this.selected = this.selected || [];
  }

  select(event: MatAutocompleteSelectedEvent): void {
    const category = event.option.value;

    if (this.selected.find(item => item.title === category.title)) {
      this.snackBar.open(`You already added ${category.title}.`, "", {
        duration: 1000
      });

      return;
    }

    this.selected.push(category);
    this.selectedChange.emit(this.selected);

    this.excluded.push(category.id);
    this.excludedChange.emit(this.excluded);

    this.update.emit();

    this.categoryInput.nativeElement.value = "";
    this.acCtrl.setValue(null);
  }

  remove(category: Category): void {
    const categories = this.selected.filter(x => x.title !== category.title);
    this.selectedChange.emit(categories);

    const excludedCategories = this.excluded.filter(x => x !== category.id);
    this.excludedChange.emit(excludedCategories);

    this.update.emit();
  }

  protected queryItems(value: string): Observable<Category[]> {
    const countriesToQuery =
      this.countries && this.countries.length > 0
        ? this.countries.map(x => x.locationId)
        : null;

    return this.solrService.paginateCategories(
      value,
      this.excluded,
      countriesToQuery
    );
  }
}
