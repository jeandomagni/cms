﻿import {
  Component,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
  OnInit
} from "@angular/core";
import { Observable, of } from "rxjs";
import { map, catchError } from "rxjs/operators";

import { BaseAutocompleteComponent } from "src/app/components/base/base-autocomplete/base-autocomplete.component";
import { StateProvinceService } from "src/app/services/state-province.service";

@Component({
  selector: "state-province-picker-inline",
  templateUrl: "./state-province-picker-inline.component.html"
})
export class StateProvincePickerInlineComponent
  extends BaseAutocompleteComponent<string>
  implements OnInit {
  @Input() countryCode: string;
  @Input() selectedItems: string[];
  @Output() selectedItemsChange: EventEmitter<string[]> = new EventEmitter();
  @ViewChild("stateInput", { static: false }) stateInput: ElementRef;

  notFoundMessage = "No matching locations found.";
  placeholder = "Select a state or province";
  removable = true;
  selectable = true;

  constructor(private stateProvinceService: StateProvinceService) {
    super();
  }

  ngOnInit(): void {
    this.selectedItems = this.selectedItems || [];
  }

  select(state: string): void {
    this.selectedItems.push(state);
    this.selectedItemsChange.emit(this.selectedItems);

    this.stateInput.nativeElement.value = "";
    this.acCtrl.setValue("");
  }

  remove(state: string): void {
    const states = this.selectedItems.filter((x: string) => x !== state);
    this.selectedItemsChange.emit(states);
  }

  protected queryItems(value: string): Observable<string[]> {
    return this.stateProvinceService
      .getByCountryCode(this.countryCode || "USA")
      .pipe(
        map((states: string[]) => {
          const suggestions = states.filter(x =>
            x.toLowerCase().includes(value.toLowerCase())
          );
          return suggestions.length > 0 ? suggestions : [this.notFoundMessage];
        }),
        catchError(_ => of(null))
      );
  }
}
