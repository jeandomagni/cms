﻿import { Component, Input, Output, EventEmitter, OnInit } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Observable, of } from "rxjs";

import { AuthService } from "src/app/auth/auth.service";
import { BaseAutocompleteComponent } from "src/app/components/base/base-autocomplete/base-autocomplete.component";

import { topics } from "./card-topic-picker.constants";

@Component({
  selector: "card-topic-picker",
  templateUrl: "./card-topic-picker.component.html"
})
export class CardTopicPickerComponent extends BaseAutocompleteComponent<string>
  implements OnInit {
  @Input() canRemove: boolean;
  @Input() selectedTopics: string[];
  @Input() pinnedTopic: string;
  @Input() topicsLabel: string;
  @Output() onAdded: EventEmitter<string> = new EventEmitter();
  @Output() onPinAdd: EventEmitter<string> = new EventEmitter();
  @Output() onRemoved: EventEmitter<string> = new EventEmitter();
  @Output() pinnedTopicChange: EventEmitter<string> = new EventEmitter();
  @Output() selectedTopicsChange: EventEmitter<string[]> = new EventEmitter();

  allTopics: string[] = topics;
  cardTitle: string;
  isAdmin: boolean;
  notFoundMessage = "No topics found.";

  constructor(private authService: AuthService, private snackBar: MatSnackBar) {
    super();
  }

  ngOnInit(): void {
    this.cardTitle =
      this.topicsLabel || "Type of Article (Formerly Called Topics)*";
    this.selectedTopics = this.selectedTopics || [];
    this.isAdmin = this.authService.isAdmin;
  }

  onTopicRemoved(topic: string): void {
    const updatedSelectedTopics = this.selectedTopics.filter(
      (value: string) => value !== topic
    );
    this.selectedTopicsChange.emit(updatedSelectedTopics);

    if (this.pinnedTopic === topic) {
      this.pinnedTopicChange.emit(null);
    }

    this.onRemoved.emit(topic);
  }

  isPinned = (topic: string): boolean => this.pinnedTopic === topic;

  pinTopic(topic: string): void {
    this.onPinAdd.emit(topic);
  }

  selectTopic(topic: string): void {
    if (!topic) return;

    if (this.selectedTopics.find((item: string) => item === topic)) {
      this.snackBar.open(`You already added ${topic}`, "", {
        duration: 1000
      });
    }

    this.selectedTopics.push(topic);
    this.selectedTopicsChange.emit(this.selectedTopics);
    if (this.selectedTopics.length === 1) {
      this.pinnedTopicChange.emit(topic);
      this.onPinAdd.emit(topic);
      return;
    }
    this.onAdded.emit(topic);
  }

  protected queryItems(value: string): Observable<string[]> {
    return of(
      this.allTopics.filter((topic: string) =>
        topic.toLowerCase().includes(value)
      )
    );
  }
}
