export const topics: string[] = [
  'Auto',
  'Blog',
  'Business',
  'Espanol',
  'Events',
  'Family',
  'Health and Safety',
  'Home',
  'Investigations',
  'Money',
  'Opinion',
  'News Releases',
  'Scams',
  'Shopping',
  'Technology',
  'Travel and Transportation',
  'Warnings'
];
