﻿import {
  Component,
  EventEmitter,
  Input,
  Output,
  OnChanges
} from "@angular/core";
import { Observable, of } from "rxjs";

import { AuthService } from "src/app/auth/auth.service";
import { BaseAutocompleteComponent } from "src/app/components/base/base-autocomplete/base-autocomplete.component";
import { LegacyBbbInfo } from "src/app/shared/models/bbb/legacy-bbb-info.model";

@Component({
  selector: "bbb-picker",
  templateUrl: "./bbb-picker.component.html"
})
export class BbbPickerComponent extends BaseAutocompleteComponent<LegacyBbbInfo>
  implements OnChanges {
  @Input() enabled: boolean;
  @Input() placeholder: string;
  @Input() title: string;
  @Input() selectedBbbName: string;
  @Output() onAdded: EventEmitter<LegacyBbbInfo> = new EventEmitter();

  allBbbs: LegacyBbbInfo[];
  isAdmin: boolean;
  notFoundMessage = "No BBBs found.";

  constructor(authService: AuthService) {
    super();

    this.isAdmin = authService.isAdmin;
    this.allBbbs = authService.getSites();
  }

  ngOnChanges(): void {
    this.acCtrl.setValue(this.selectedBbbName);
  }

  selectLocation(bbbName: string): void {
    const selectedBbb = this.allBbbs.find(
      bbb => bbb.bbbName.toLowerCase() === bbbName.toLowerCase()
    );
    this.onAdded.emit(selectedBbb);
  }

  protected queryItems(value: string): Observable<LegacyBbbInfo[]> {
    return of(
      this.allBbbs.filter(bbb => bbb.bbbName.toLowerCase().includes(value))
    );
  }
}
