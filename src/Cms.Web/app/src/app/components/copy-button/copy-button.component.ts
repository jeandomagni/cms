﻿import { Component, Input } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";

@Component({
  selector: "copy-button",
  templateUrl: "./copy-button.component.html",
  styleUrls: ["./copy-button.styles.scss"]
})
export class CopyButtonComponent {
  @Input() text: string;

  constructor(private snackBar: MatSnackBar) {}

  copyText(): void {
    const selBox = document.createElement("textarea");
    selBox.style.position = "fixed";
    selBox.style.left = "0";
    selBox.style.top = "0";
    selBox.style.opacity = "0";
    selBox.value = this.text;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand("copy");
    document.body.removeChild(selBox);

    this.snackBar.open("Text copied!", "", {
      duration: 1000,
      horizontalPosition: "right",
      verticalPosition: "bottom"
    });
  }
}
