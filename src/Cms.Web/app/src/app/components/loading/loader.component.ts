import { Component } from "@angular/core";

import { LoaderService } from "src/app/components/loading/loader.service";

@Component({
  selector: "cms-loading",
  templateUrl: "./loader.component.html",
  styles: [
    `
      .loader-container {
        position: fixed;
        top: 50%;
        left: 50%;
        display: block;
        z-index: 999;
      }
    `
  ]
})
export class LoaderComponent {
  loading: boolean;
  constructor(private loaderService: LoaderService) {
    this.loaderService.isLoading.subscribe(v => {
      this.loading = v;
    });
  }
}
