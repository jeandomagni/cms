﻿import {
  Component,
} from "@angular/core";

import {FormControl} from '@angular/forms';
import { AuthService } from "src/app/auth/auth.service";

@Component({
  selector: "roles-picker",
  templateUrl: "./roles-picker.component.html"
})
export class RolesPickerComponent  {
  roles = new FormControl();
  roleList = ['Extra cheese', 'Mushroom', 'Onion', 'Pepperoni', 'Sausage', 'Tomato'];
  selectedRoles;
}
