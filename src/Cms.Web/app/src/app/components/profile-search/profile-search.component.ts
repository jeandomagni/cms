﻿import { Component } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { Observable, BehaviorSubject } from "rxjs";

import { BaseAutocompleteComponent } from "src/app/components/base/base-autocomplete/base-autocomplete.component";
import { SolrService } from "src/app/services/solr.service";
import { BusinessSearchResult } from "src/app/shared/models/business-profile/business-search-result.model";
import { BusinessProfileTypeaheadSuggestion } from "src/app/shared/models/business-profile/business-profile-typeahead-suggestion.interface";

@Component({
  selector: "profile-search",
  templateUrl: "./profile-search.component.html",
  styleUrls: ["./profile-search.styles.scss"]
})
export class ProfileSearchComponent extends BaseAutocompleteComponent<
  BusinessProfileTypeaheadSuggestion
> {
  form: FormGroup;
  notFoundMessage = "No businesses found.";
  resultsHeading: string;
  searchResult = new BusinessSearchResult();
  showResultsObs: Observable<boolean>;
  private showResultsSubject: BehaviorSubject<boolean>;

  constructor(private solrService: SolrService) {
    super();

    this.form = new FormGroup({
      ctrl: this.acCtrl
    });

    this.showResultsSubject = new BehaviorSubject<boolean>(false);
    this.showResultsObs = this.showResultsSubject.asObservable();
  }

  selectBusiness(business: BusinessProfileTypeaheadSuggestion): void {
    if (!business) return;

    window.location.href = `/#/admin/editProfileSeo?id=${business.id}`;
  }

  submit(): void {
    const profileSearchParams = {
      searchText: this.form.get("ctrl").value
    };

    this.solrService
      .searchBusinesses(profileSearchParams)
      .subscribe((data: BusinessSearchResult) => {
        this.searchResult = data;
        this.showResultsSubject.next(data.items.length > 0);
        this.resultsHeading = `Showing ${
          data.resultCount > data.searchParams.pageSize
            ? data.searchParams.pageSize
            : data.resultCount
        } of ${data.resultCount} results for "${data.searchParams.searchText}"`;
      });
  }

  updatePage(pageNumber: number): void {
    const profileSearchParams = JSON.parse(
      JSON.stringify(this.searchResult.searchParams)
    );
    profileSearchParams.pageNumber = pageNumber;

    this.solrService
      .searchBusinesses(profileSearchParams)
      .subscribe((data: BusinessSearchResult) => {
        this.searchResult = data;
      });
  }

  protected queryItems(
    value: string
  ): Observable<BusinessProfileTypeaheadSuggestion[]> {
    return this.solrService.suggestBusinesses(value);
  }
}
