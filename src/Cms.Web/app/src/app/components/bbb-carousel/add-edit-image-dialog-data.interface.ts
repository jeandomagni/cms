import { Media } from "src/app/shared/models/content/entities/media.model";

export interface AddEditImageDialogData {
  image: Media;
}
