import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnDestroy
} from "@angular/core";
import { MatDialog } from "@angular/material";
import { takeUntil } from "rxjs/operators";

import { AddEditImageDialogComponent } from "src/app/components/bbb-carousel/add-edit-image-dialog.component";
import { MediaSearchDialogComponent } from "src/app/components/media-search-dialog/media-search-dialog.component";
import { ContentMediaService } from "src/app/services/content-media.service";
import { NotificationService } from "src/app/services/notification.service";
import { Media } from "src/app/shared/models/content/entities/media.model";
import { File } from "src/app/shared/models/file/file.model";

@Component({
  selector: "bbb-carousel",
  templateUrl: "./bbb-carousel.component.html"
})
export class BbbCarouselComponent implements OnDestroy {
  @Input() hideEdit: boolean;
  @Input() canEdit: boolean;
  @Input() contentCode: string;
  @Input() datasource: Media[];
  @Input() single: boolean;
  @Output() overrideUpdate: EventEmitter<Media> = new EventEmitter<Media>();
  @Output() overrideRemove: EventEmitter<number> = new EventEmitter<number>();
  @Output() datasourceChange: EventEmitter<Media[]> = new EventEmitter();
  @Output() update: EventEmitter<void> = new EventEmitter();

  isSaving = false;
  private onDestroy: EventEmitter<void> = new EventEmitter();

  constructor(
    private contentMediaService: ContentMediaService,
    private dialog: MatDialog,
    private notificationService: NotificationService
  ) {}

  ngOnDestroy(): void {
    this.onDestroy.next();
  }

  addItem(): void {
    this.isSaving = true;
    const dialogRef = this.dialog.open(MediaSearchDialogComponent);

    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.onDestroy))
      .subscribe((file: File) => {
        if (!file) return;

        const media = this.createMedia(file);

        if(this.overrideUpdate.observers.length == 0) {
          this.contentMediaService
            .create(media)
            .pipe(takeUntil(this.onDestroy))
            .subscribe(() => {
              this.isSaving = false;

              this.notificationService.success("Image added");
              this.update.emit();
            });
        }
        else {
          media.title = null;
          media.imageTitle = null;
          this.datasource.push(media);
          this.overrideUpdate.emit(media);
          this.isSaving = false;
        }
      });
  }

  editItem(image: Media): void {
    this.isSaving = true;

    const dialogRef = this.dialog.open(AddEditImageDialogComponent, {
      data: { image }
    });

    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.onDestroy))
      .subscribe((updatedImage: Media) => {
        this.isSaving = false;

        if (!image) return;

        this.contentMediaService
          .update(updatedImage)
          .pipe(takeUntil(this.onDestroy))
          .subscribe(() => {
            this.isSaving = false;

            this.notificationService.success("Image saved");
            this.update.emit();
          });
      });
  }

  removeItem(id: number): void {
    this.isSaving = true;

    if(this.overrideRemove.observers.length == 0) {
      this.contentMediaService
        .delete(this.contentCode, id)
        .pipe(takeUntil(this.onDestroy))
        .subscribe(() => {
          this.isSaving = false;

          this.notificationService.success("Image removed");
          this.update.emit();
        });
    } else {
      this.datasource.splice(0, 1);
      this.overrideRemove.emit(id);
      this.isSaving = false;
    }
  }

  moveLeft(order: number): void {
    if (order <= 1) return;

    this.isSaving = true;
    this.datasource[order - 1].order = order - 1;
    this.datasource[order - 2].order = order;

    this.contentMediaService
      .reorder(this.contentCode, this.datasource)
      .pipe(takeUntil(this.onDestroy))
      .subscribe(() => {
        this.isSaving = false;

        this.notificationService.success("Order updated");
        this.update.emit();
      });
  }

  moveRight(order: number): void {
    if (order === this.datasource.length) return;

    this.isSaving = true;
    this.datasource[order - 1].order = order + 1;
    this.datasource[order].order = order;

    this.contentMediaService
      .reorder(this.contentCode, this.datasource)
      .pipe(takeUntil(this.onDestroy))
      .subscribe(() => {
        this.isSaving = false;

        this.notificationService.success("Order updated");
        this.update.emit();
      });
  }

  truncate(s: string, n: number): string {
    if (!s) return s;
    return s.length > n ? `${s.substr(0, n - 1)}...` : s.toString();
  }

  private createMedia(file: File): Media {
    const media = new Media();
    media.contentCode = this.contentCode;
    media.credit = file.credit;
    media.fileCode = file.code;
    media.description = file.title;
    media.id = file.id;
    media.imageCaption = file.caption;
    media.imageTitle = file.title;
    media.mainImage = this.single;
    media.title = file.title;
    media.url = file.uri;
    return media;
  }
}
