﻿import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";

import { AddEditImageDialogData } from "./add-edit-image-dialog-data.interface";

@Component({
  selector: "add-edit-image-dialog",
  templateUrl: "./add-edit-image-dialog.component.html"
})
export class AddEditImageDialogComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: AddEditImageDialogData,
    private dialogRef: MatDialogRef<AddEditImageDialogComponent>
  ) {}

  update(): void {
    this.dialogRef.close(this.data.image);
  }
}
