﻿import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnChanges
} from "@angular/core";
import { MatDialog } from "@angular/material";

import { MediaSearchDialogComponent } from "src/app/components/media-search-dialog/media-search-dialog.component";
import { Media } from "src/app/shared/models/content/entities/media.model";
import { File } from "src/app/shared/models/file/file.model";

@Component({
  selector: "card-image-picker",
  templateUrl: "./card-image-picker.component.html"
})
export class CardImagePickerComponent implements OnChanges {
  @Input() editable: boolean;
  @Input() file: File;
  @Input() title: string;
  @Output() change: EventEmitter<Media> = new EventEmitter();
  @Output() fileChange: EventEmitter<Media> = new EventEmitter();

  constructor(private dialog: MatDialog) {}

  ngOnChanges(): void {
    this.file = this.file || new File();
  }

  addItem(): void {
    const dialogRef = this.dialog.open(MediaSearchDialogComponent);

    dialogRef.afterClosed().subscribe((entity: File) => {
      if (!entity) return;

      const file = this.createMediaFromFile(entity);

      this.fileChange.emit(file);
      this.change.emit(file);
    });
  }

  removeItem(): void {
    const file = null;

    this.fileChange.emit(file);
    this.change.emit(file);
  }

  private createMediaFromFile(file: File): Media {
    const media = new Media();
    media.title = file.title;
    media.fileCode = file.code;
    media.url = file.uri;
    media.imageCaption = file.caption;
    media.imageTitle = file.title;
    return media;
  }
}
