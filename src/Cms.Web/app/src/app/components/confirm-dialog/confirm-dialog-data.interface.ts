export interface ConfirmDialogData {
  confirmLabel: string;
  label: string;
}
