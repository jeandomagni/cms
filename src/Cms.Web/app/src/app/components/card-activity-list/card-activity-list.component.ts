﻿import {
  Component,
  Input,
  EventEmitter,
  OnInit,
  OnDestroy
} from "@angular/core";
import { Observable } from "rxjs";
import { takeUntil } from "rxjs/operators";

import { AuditLog } from "src/app/shared/models/audit-log.interface";

@Component({
  selector: "card-activity-list",
  templateUrl: "./card-activity-list.component.html"
})
export class CardActivityListComponent implements OnInit, OnDestroy {
  @Input() getItems: (page: number, pageSize: number) => Observable<AuditLog[]>;

  displayLoadMore = true;
  items: AuditLog[] = [];
  private onDestroy: EventEmitter<void> = new EventEmitter();
  private page = 1;
  private pageSize = 20;

  ngOnInit(): void {
    this.refreshItems();
  }

  ngOnDestroy(): void {
    this.onDestroy.next();
  }

  getMoreActivity(): void {
    this.page += 1;
    this.refreshItems();
  }

  private refreshItems(): void {
    this.getItems(this.page, this.pageSize)
      .pipe(takeUntil(this.onDestroy))
      .subscribe((data: AuditLog[]) => {
        if (!data) {
          return;
        }

        this.displayLoadMore = data.length === this.pageSize;
        this.items = this.items.concat(data);
      });
  }
}
