export const countries = [
  { name: 'The United States', code: 'USA' },
  { name: 'Canada', code: 'CAN' },
  { name: 'Mexico', code: 'MEX' },
]