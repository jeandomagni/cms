﻿import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  OnDestroy
} from "@angular/core";
import { FormControl } from "@angular/forms";
import { Observable } from "rxjs";

import { NotificationService } from "src/app/services/notification.service";
import { SolrService } from "src/app/services/solr.service";
import { Location } from "src/app/shared/models/location/location.interface";
import { ContentLocation } from "src/app/shared/models/location/content-location.interface";
import { BaseAutocompleteComponent } from "src/app/components/base/base-autocomplete/base-autocomplete.component";

import { countries } from "./card-city-picker.constants";

@Component({
  selector: "card-city-picker",
  templateUrl: "./card-city-picker.component.html",
  styleUrls: ["./card-city-picker.styles.scss"]
})
export class CardCityPickerComponent extends BaseAutocompleteComponent<Location>
  implements OnChanges, OnDestroy {
  @Input() canEdit: boolean;
  @Input() selected: Location[];
  @Output() add: EventEmitter<ContentLocation> = new EventEmitter();
  

  countries = countries;
  notFoundMessage = "No city found.";
  selectable = true;
  countryCtrl: FormControl = new FormControl("USA");

  constructor(
    private notificationService: NotificationService,
    private solrService: SolrService
  ) {
    super();
  }

  ngOnChanges(): void {

    if (!this.canEdit) {
      this.countryCtrl.disable();
      this.acCtrl.disable();
    }
  }

  selectLocation(city: Location): void {
    if (!this.selected.find(item => item.id === city.id)) {
      this.acCtrl.setValue(null);

      const locationToAdd = { location: JSON.stringify(city), type: "city" };
      this.add.emit(locationToAdd);
      return;
    }

    this.notificationService.error(`You already added ${city.id}`);
  }  

  protected queryItems(value: string): Observable<Location[]> {
    return this.solrService.paginateCities(value, this.filterByCountries.map(x => x.locationId));
  }
}
