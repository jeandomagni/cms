import {
  Component,
  OnInit,
  Input,
  EventEmitter,
  Output, OnChanges
} from '@angular/core';
import { MatAutocompleteSelectedEvent } from '@angular/material';
import {Observable, of} from 'rxjs';
import { NotificationService } from 'src/app/services/notification.service';
import { BaseAutocompleteComponent } from 'src/app/components/base/base-autocomplete/base-autocomplete.component';

import { LegacyBbbInfo } from 'src/app/shared/models/bbb/legacy-bbb-info.model';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'bbb-selector',
  templateUrl: './bbb-selection.component.html'
})
export class BbbSelectionComponent
  extends BaseAutocompleteComponent<LegacyBbbInfo>
  implements OnChanges {
  @Input() selected: LegacyBbbInfo;
  @Input() selectedBbbName: string;
  @Input() title: string;
  @Output() selectedChange: EventEmitter<LegacyBbbInfo> = new EventEmitter();

  allBbbs: LegacyBbbInfo[];
  isAdmin: boolean;
  notFoundMessage = 'No BBBs found.';
  placeholder: string;

  constructor(
    authService: AuthService,
    private notificationService: NotificationService,

  ) {
    super();
    this.isAdmin = authService.isAdmin;
    this.allBbbs = authService.getSites();
  }

  ngOnInit(): void {
    this.placeholder = this.title;
    if (this.title === null || this.title === undefined) {
      this.placeholder = 'Search BBB';
    }
  }

  ngOnChanges(): void {
    this.acCtrl.setValue(this.selectedBbbName);
  }

  select(event: MatAutocompleteSelectedEvent): void {
    // this.categoryInput.nativeElement.value = "";
    const bbb: LegacyBbbInfo = event.option.value;
    this.acCtrl.setValue(bbb.bbbName);
    this.selectedChange.emit(bbb);
  }

  clear(): void {
    this.acCtrl.setValue(null);
    this.selectedChange.emit(null);
  }

  protected queryItems(value: string): Observable<LegacyBbbInfo[]> {
    return of(
      this.allBbbs.filter(bbb => bbb.bbbName.toLowerCase().includes(value))
    );
  }

}
