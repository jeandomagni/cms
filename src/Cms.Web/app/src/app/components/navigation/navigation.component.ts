import { Component, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "navigation",
  templateUrl: "./navigation.component.html"
})
export class NavigationComponent {
  @Input() activeLink: string;
  @Output() activeLinkChange: EventEmitter<string> = new EventEmitter();
  @Input() links: string[];

  setActiveLink(link: string) {
    this.activeLinkChange.emit(link);
  }
}
