﻿import { Component, Input, Output, EventEmitter, OnInit } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";

import { DefaultFilterTypes } from "src/app/components/filter-column/filter-column.constants";
import { FilterType } from "src/app/components/filter-column/filter-type.model";
import { Filter } from "src/app/shared/models/table/filter.interface";

@Component({
  selector: "filter-column",
  templateUrl: "./filter-column.component.html",
  styleUrls: ["./filter-column.styles.scss"]
})
export class FilterColumnComponent implements OnInit {
  @Input() columnName: string;
  @Input() filters: Filter[];
  @Output() filter: EventEmitter<void> = new EventEmitter();
  @Output() filtersChange: EventEmitter<Filter[]> = new EventEmitter();

  filterForm: FormGroup;
  filterTypes: FilterType[] = DefaultFilterTypes;

  ngOnInit(): void {
    const initialFilter = this.getInitialFilter();
    this.filterForm = new FormGroup({
      keywordCtrl: new FormControl(initialFilter.keyword),
      typeCtrl: new FormControl(initialFilter.type)
    });
  }

  filterClick(): void {
    this.removeExistingElement();
    this.filters.push({
      column: this.columnName,
      keyword: this.filterForm.get("keywordCtrl").value,
      type: this.filterForm.get("typeCtrl").value
    });

    this.filtersChange.emit(this.filters);
    this.filter.emit();
  }

  clearClick(): void {
    this.filterForm.get("keywordCtrl").setValue("");
    this.removeExistingElement();
    this.filter.emit();
  }

  private removeExistingElement(): void {
    const matchIndex = this.filters.findIndex(
      x => x.column === this.columnName
    );

    if (matchIndex > -1) {
      this.filters.splice(matchIndex);
      this.filtersChange.emit(this.filters);
    }
  }

  private getInitialFilter(): Filter {
    const matchIndex = this.filters.findIndex(
      x => x.column === this.columnName
    );

    return {
      keyword: matchIndex > -1 ? this.filters[matchIndex].keyword : "",
      type: matchIndex > -1 ? this.filters[matchIndex].type : "contains",
      column: this.columnName
    };
  }
}
