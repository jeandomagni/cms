import { FilterType } from "src/app/components/filter-column/filter-type.model";

export const DefaultFilterTypes: FilterType[] = [
  { key: "contains", label: "Contains" },
  { key: "eq", label: "Is equal to" },
  { key: "startswith", label: "Starts with" }
];
