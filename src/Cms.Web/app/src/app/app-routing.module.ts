import { NgModule } from "@angular/core";
import { Routes, RouterModule, ExtraOptions } from "@angular/router";

import { AdminModule } from "src/app/admin/admin.module";
import { BbbModule } from "src/app/bbb/bbb.module";
import { CreateContentModule } from "src/app/create-content/create-content.module";
import { ManageContentModule } from "src/app/manage-content/manage-content.module";
import { ProfileModule } from "src/app/profile/profile.module";
import { LoginModule } from "src/app/login/login.module";
import { CommonLayoutComponent } from "src/app/shared/common-layout/common-layout.component";

const routes: Routes = [
  {
    path: "login",
    loadChildren: (): Promise<LoginModule> =>
      import("src/app/login/login.module").then(mod => mod.LoginModule)
  },

  {
    path: "",
    component: CommonLayoutComponent,
    children: [
      {
        path: "",
        loadChildren: (): Promise<ManageContentModule> =>
          import("src/app/manage-content/manage-content.module").then(
            mod => mod.ManageContentModule
          )
      },
      {
        path: "content/manage",
        loadChildren: (): Promise<ManageContentModule> =>
          import("src/app/manage-content/manage-content.module").then(
            mod => mod.ManageContentModule
          )
      },
      {
        path: "bbb/manage",
        loadChildren: (): Promise<BbbModule> =>
          import("src/app/bbb/bbb.module").then(mod => mod.BbbModule)
      },
      {
        path: "content/create",
        loadChildren: (): Promise<CreateContentModule> =>
          import("src/app/create-content/create-content.module").then(
            mod => mod.CreateContentModule
          )
      },
      {
        path: "profile/manage",
        loadChildren: (): Promise<ProfileModule> =>
          import("src/app/profile/profile.module").then(
            mod => mod.ProfileModule
          )
      },
      {
        path: "admin",
        loadChildren: (): Promise<AdminModule> =>
          import("src/app/admin/admin.module").then(mod => mod.AdminModule)
      }
    ]
  }
];

const options: ExtraOptions = {
  useHash: true,
  initialNavigation: true
  // enableTracing: true
};

@NgModule({
  imports: [RouterModule.forRoot(routes, options)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
