import { Component, OnDestroy, OnInit } from "@angular/core";
import { FormControl } from "@angular/forms";
import { MatDialog } from "@angular/material";
import { Subject, Observable } from "rxjs";
import { takeUntil, startWith, map } from "rxjs/operators";

import { AuthService } from "src/app/auth/auth.service";
import { VerifyAddPhoneNumberDialog } from "src/app/profile/verify-add-phone-number-dialog.component";
import { UserProfileService } from "src/app/services/user-profile.service";
import { NotificationService } from "src/app/services/notification.service";
import { ConfirmDialogService } from "src/app/services/confirm-dialog.service";
import { LegacyBbbInfo } from "src/app/shared/models/bbb/legacy-bbb-info.model";
import { UserProfile } from "src/app/shared/models/user/user-profile.model";

@Component({
  selector: "profile",
  templateUrl: "./profile.component.html"
})
export class ProfileComponent implements OnInit, OnDestroy {
  links: string[] = ["Basic Settings", "Security"];
  activeLink: string = this.links[0];
  countryCtrl: FormControl = new FormControl();
  cultureCtrl: FormControl = new FormControl();
  defaultSite = new LegacyBbbInfo();
  filteredCountries: Observable<string[]>;
  filteredCultures: Observable<string[]>;
  selectedTopics: string[];
  toHighlightCountry: string;
  toHighlightCulture: string;
  userProfile = new UserProfile();
  private readonly allCountries: string[] = ["us", "ca", "mx"];
  private readonly allCultures: string[] = ["en", "es", "fr"];
  private readonly onDestroy = new Subject<void>();

  constructor(
    private authService: AuthService,
    private confirmDialogService: ConfirmDialogService,
    private dialog: MatDialog,
    private notificationService: NotificationService,
    private userProfileService: UserProfileService
  ) {
    this.refresh();
  }

  ngOnInit(): void {
    this.filteredCountries = this.countryCtrl.valueChanges.pipe(
      startWith(""),
      map(value => this.filterCountries(value))
    );

    this.filteredCultures = this.cultureCtrl.valueChanges.pipe(
      startWith(""),
      map(value => this.filterCultures(value))
    );
  }

  ngOnDestroy(): void {
    this.onDestroy.next();
  }

  addTopic(): void {
    this.userProfile.topics = JSON.stringify(this.selectedTopics);
  }

  deletePhoneNumber(): void {
    this.confirmDialogService
      .showConfirmDelete()
      .pipe(takeUntil(this.onDestroy))
      .subscribe(confirmed => {
        if (confirmed) {
          this.userProfile.phoneNumber = null;
          this.userProfile.phoneNumberVerified = false;
          this.saveProfile();
        }
      });
  }

  pinTopic(topic: string): void {
    this.userProfile.pinnedTopic = topic;
    this.userProfile.topics = JSON.stringify(this.selectedTopics);
  }

  removeTopic(topic: string): void {
    if (this.userProfile.pinnedTopic === topic) {
      this.userProfile.pinnedTopic = null;
    }

    this.userProfile.topics = JSON.stringify(this.selectedTopics);
  }

  saveProfile(): void {
    this.userProfileService
      .update(this.userProfile)
      .pipe(takeUntil(this.onDestroy))
      .subscribe(() => {
        const { currentUser } = this.authService;
        currentUser.profile = this.userProfile;
        window.localStorage.setItem("appUser", JSON.stringify(currentUser));
        this.notificationService.success("Saved user profile");
        this.refresh();
      });
  }

  selectCountry(value: string): void {
    this.userProfile.baseUriCountry = value;
  }

  selectCulture(value: string): void {
    this.userProfile.baseUriCulture = value;
  }

  setDefaultSite(location: LegacyBbbInfo): void {
    this.userProfile.defaultSite = location.legacyBBBID;
  }

  verifyAddPhoneNumber(): void {
    const dialogRef = this.dialog.open(VerifyAddPhoneNumberDialog);

    dialogRef.afterClosed().subscribe((success: boolean) => {
      if (success) {
        this.notificationService.success("SMS authentication enabled");
        this.refresh();
      }
    });
  }

  private filterCountries(value: string): string[] {
    if (!value) {
      return this.allCountries;
    }

    this.toHighlightCountry = value;

    const filterValue = value.toLowerCase();
    const suggestions = this.allCountries.filter((country: string) =>
      country.toLowerCase().includes(filterValue)
    );
    return suggestions.length > 0 ? suggestions : [];
  }

  private filterCultures(value: string): string[] {
    if (!value) {
      return this.allCultures;
    }

    this.toHighlightCulture = value;

    const filterValue = value.toLowerCase();
    const suggestions = this.allCultures.filter((culture: string) =>
      culture.toLowerCase().includes(filterValue)
    );
    return suggestions.length > 0 ? suggestions : [];
  }

  private refresh(): void {
    this.userProfileService
      .get()
      .pipe(takeUntil(this.onDestroy))
      .subscribe((data: UserProfile) => {
        this.userProfile = data;
        if (typeof data.defaultSite === "string") {
          this.defaultSite = this.authService.currentUser.sites.find(
            (site: LegacyBbbInfo) =>
              site.legacyBBBID === this.userProfile.defaultSite
          );
        }
        if (this.userProfile.topics) {
          this.selectedTopics = JSON.parse(this.userProfile.topics);
        }

        this.countryCtrl.setValue(this.userProfile.baseUriCountry);
        this.cultureCtrl.setValue(this.userProfile.baseUriCulture);
      });
  }
}
