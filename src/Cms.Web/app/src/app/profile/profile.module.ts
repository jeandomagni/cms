import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FlexLayoutModule } from "@angular/flex-layout";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {
  MatTabsModule,
  MatIconModule,
  MatCardModule,
  MatAutocompleteModule,
  MatFormFieldModule,
  MatButtonModule,
  MatInputModule,
  MatCheckboxModule,
  MatToolbarModule,
  MatDialogModule,
  MatSelectModule,
  MatSortModule,
  MatChipsModule
} from "@angular/material";

import { ProfileComponent } from "src/app/profile/profile.component";
import { ProfileRoutingModule } from "src/app/profile/profile-routing.module";
import { VerifyAddPhoneNumberDialog } from "src/app/profile/verify-add-phone-number-dialog.component";
import { UserProfileService } from "src/app/services/user-profile.service";
import { SharedModule } from "src/app/shared/shared.module";

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    FormsModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDialogModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatSortModule,
    MatTabsModule,
    MatToolbarModule,
    ReactiveFormsModule,
    SharedModule,

    ProfileRoutingModule
  ],
  declarations: [ProfileComponent, VerifyAddPhoneNumberDialog],
  entryComponents: [VerifyAddPhoneNumberDialog],
  providers: [UserProfileService]
})
export class ProfileModule {}
