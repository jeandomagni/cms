﻿import { Component, OnDestroy } from "@angular/core";
import { FormControl } from "@angular/forms";
import { MatDialogRef } from "@angular/material";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

import { UserProfileService } from "src/app/services/user-profile.service";

@Component({
  selector: "verify-add-phone-number",
  templateUrl: "./verify-add-phone-number-dialog.component.html"
})
export class VerifyAddPhoneNumberDialog implements OnDestroy {
  ctrl: FormControl = new FormControl();
  phoneNumber: string;
  verificationCode: string;
  verificationError: string;
  verifying: boolean;
  private readonly onDestroy = new Subject<void>();

  constructor(
    private dialogRef: MatDialogRef<VerifyAddPhoneNumberDialog>,
    private userProfileService: UserProfileService
  ) {}

  ngOnDestroy(): void {
    this.onDestroy.next();
  }

  verify(): void {
    this.verificationError = null;

    this.userProfileService
      .verifyPhone(this.phoneNumber)
      .pipe(takeUntil(this.onDestroy))
      .subscribe(
        () => {
          this.verifying = true;
        },
        () => {
          this.verificationError = "Please enter a valid phone number.";
        }
      );
  }

  update(): void {
    this.verificationError = null;

    this.userProfileService
      .savePhone(this.verificationCode)
      .pipe(takeUntil(this.onDestroy))
      .subscribe(
        () => {
          this.dialogRef.close(true);
        },
        () => {
          this.verificationError =
            'Invalid code entered. Please try again or click "Resend" to get a new code.';
        }
      );
  }
}
