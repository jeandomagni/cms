﻿using Bbb.Core.Solr.Model;
using Cms.Model.Models.Content.Entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Cms.Domain.Helpers
{
    public static class SolrHelpers
    {
        public static List<string> FormatCities(string serialized) {
            if (string.IsNullOrWhiteSpace(serialized))
                return null;

            var locations = 
                JsonConvert.DeserializeObject<IEnumerable<Location>>(serialized);

            return locations.Select(location => $"{location.City}, {location.State}").ToList();
        }

        public static List<string> FormatCategories(string serialized)
        {
            if (string.IsNullOrWhiteSpace(serialized))
                return null;

            try
            {
                var locations = JsonConvert.DeserializeObject<IEnumerable<SearchTypeahead>>(serialized);
                return locations.Select(category => $"{category.EntityId}|{category.Title}").ToList();
            }
            catch
            {
                return null;
            }
        }

        public static List<string> FormatSites(string serialized)
        {
            if (string.IsNullOrWhiteSpace(serialized))
                return null;

            try
            {
                var sites = JsonConvert.DeserializeObject<IEnumerable<LegacyBbbInfo>>(serialized);
                return sites.Select(site => site.LegacyBBBID).ToList();
            }
            catch
            {
                return null;
            }
        }

        public static string FormatEventBbb(string serialized)
        {
            if (string.IsNullOrWhiteSpace(serialized))
                return null;

            try
            {
                var info = JsonConvert.DeserializeObject<LegacyBbbInfo>(serialized);
                return info?.LegacyBBBID;
            }
            catch 
            {
                return null;
            }
        }

        public static string FormatArticleVideos(string serialized)
        {
            if (string.IsNullOrWhiteSpace(serialized))
                return null;

            var model =
                JsonConvert.DeserializeObject<IEnumerable<Video>>(serialized);

            return JsonConvert.SerializeObject(model, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            });
        }

        public static int GetCursorStart(int pageNumber, int pageSize)
        {
            if (pageNumber == 1)
            {
                return 0;
            }

            return (pageSize * pageNumber);
        }
    }
}
