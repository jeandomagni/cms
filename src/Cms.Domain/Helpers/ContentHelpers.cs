﻿using Cms.Domain.Enum;

namespace Cms.Domain.Helpers
{
    public static class ContentHelpers
    {
        public static string MapContentTypeToEntity(string type)
        {
            switch (type)
            {
                case ContentType.Article:
                    return EntityCodes.ARTICLE;
                case ContentType.Event:
                    return EntityCodes.EVENT;
                case ContentType.NearMe:
                    return EntityCodes.NEARME;
                case ContentType.AccreditedBusinessList:
                    return EntityCodes.ABLIST;
                case ContentType.BaselinePage:
                    return EntityCodes.BLPAGE;
            }
            return null;
        }
    }
}
