﻿using Cms.Domain.Extensions;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace Cms.Domain.Helpers
{
    public static class StringHelpers
    {
        public static List<string> MapCommaSeparatedStringToList(string commaSeparatedList)
        {
            if (!string.IsNullOrWhiteSpace(commaSeparatedList) && commaSeparatedList.Contains(",")) {
                return commaSeparatedList
                    .Split(",")
                    .Select(x => x.Trim())
                    .ToList();
            }

            return new List<string>() {commaSeparatedList }
                .Where(x => !string.IsNullOrWhiteSpace(x))
                .ToList();
        }

        public static string MapSerializedJsonListToCommaSeparatedString(string serializedJsonList)
        {
            return string.Join(",", serializedJsonList.DeserializeOrDefault(new List<string>()));
        }

        public static string MapCommaSeparatedToSerializedJsonList(string commaSeparatedList)
        {
            var list = MapCommaSeparatedStringToList(commaSeparatedList);
            return JsonConvert.SerializeObject(list);
        }

        public static List<string> DeserializeTagsString(string tagsString)
        {
            if (string.IsNullOrWhiteSpace(tagsString))
            {
                return new List<string>();
            }

            try
            {
                var tags = tagsString.DeserializeOrDefault<List<string>>(null) ?? new List<string>();
                if (tags.Any())
                {
                    return tags;
                }

                return StringHelpers.MapCommaSeparatedStringToList(tagsString);
            }
            catch
            {
                return StringHelpers.MapCommaSeparatedStringToList(tagsString);
            }
        }

        /// <summary>
        /// Retrieves a base domain name from a full domain name.
        /// For example: www.west-wind.com produces west-wind.com
        /// </summary>
        /// <param name="domainName">Dns Domain name as a string</param>
        /// <returns></returns>
        public static string GetBaseDomain(string domainName)
        {
            var tokens = domainName.Split('.');

            // only split 3 segments like www.west-wind.com
            if (tokens == null || tokens.Length != 3)
                return domainName;

            var tok = new List<string>(tokens);
            var remove = tokens.Length - 2;
            tok.RemoveRange(0, remove);

            return tok[0] + "." + tok[1]; ;
        }
    }
}
