﻿using Bbb.Core.Solr.Model;
using Cms.Domain.Enum;
using Cms.Domain.Extensions;
using Cms.Model.Models.Content.Entities;
using System;
using System.Linq;
using CmsContent = Cms.Model.Models.Content.Content;

namespace Cms.Domain.Helpers
{
    public static class ArticleUrlHelpers
    {
        public const string ArticleUrlSegment = "article";
        public const string NearMeUrlSegment = "near-me";

        public static string ExtractTopicUrlSegment(string url)
        {
            if (string.IsNullOrWhiteSpace(url))
            {
                return null;
            }

            if (!url.Contains(ArticleUrlSegment)) {
                return null;
            }

            var urlSegments = url.Split('/').ToList();
            var articleUrlSegmentIndex = urlSegments.FindIndex(x => x == ArticleUrlSegment);
            var topicIndex = articleUrlSegmentIndex + 1;
            return urlSegments[topicIndex];
        }

        public static string ExtractEditableUrlSegment(string url, int id)
        {
            if (string.IsNullOrWhiteSpace(url))
            {
                return null;
            }

            if (!url.Contains(ArticleUrlSegment))
            {
                return null;
            }

            var uneditableIdUrlSegment = $"{id.ToString()}-";
            var urlSegment = url.Split('/').ToList().Last();

            return urlSegment.Substring(
                uneditableIdUrlSegment.Length,
                urlSegment.Length - uneditableIdUrlSegment.Length);
        }

        public static string CreateUrlSegment(CmsContent content)
        {
            return content.Type == ContentType.NearMe
                ? content.Categories?.FirstOrDefault()?.Title?.ToUrlSlug()
                : content.Type == ContentType.BaselinePage
                ? $"{content.ArticleUrl.Segment}"
                : $"{content.Id}-{content.Title.ToUrlSlug()}";
        }

        public static ArticleUrl CreateGenericUrl(
            string baseUrl,
            int id,
            string title,
            string pinnedTopic,
            string currentUrl,
            string status)
        {
            var defaultTopicUrlSegment = (pinnedTopic ?? Topics.Default).ToUrlSlug();
            var topicUrlSegment = status == ContentStatus.Published
                ? ExtractTopicUrlSegment(currentUrl) ?? defaultTopicUrlSegment
                : defaultTopicUrlSegment;

            var url = new ArticleUrl();
            url.Prefix = $"{baseUrl}/{ArticleUrlSegment}/{topicUrlSegment}/{id}-";
            url.Segment = ExtractEditableUrlSegment(currentUrl, id) ?? title.ToUrlSlug();
            return url;
        }

        public static ArticleUrl CreateNearMeUrl(string baseUrl, SearchTypeahead category)
        {
            var url = new ArticleUrl();
            url.Prefix = $"{baseUrl}/{NearMeUrlSegment}/";
            url.Segment = $"{category?.Title?.ToUrlSlug()}";
            return url;
        }

        internal static ArticleUrl CreateBaselinePageUrl(string baseUrl, string uriSegment)
        {
            var url = new ArticleUrl();
            url.Prefix = $"{baseUrl}/";
            url.Segment = $"{uriSegment.ToUrlSlug()}";
            return url;
        }
    }
}
