﻿using Cms.Model.Constants;

namespace Cms.Domain.Helpers
{
    public static class CultureHelpers
    {
        public static string GetCultureInfo(string language, string country)
        {
            switch (country)
            {
                case "MEX" when language == "ES":
                    return CultureConstants.EsMxCultureId;
                case "CAN" when language == "EN":
                    return CultureConstants.EnCaCultureId;
                case "USA" when language == "EN":
                    return CultureConstants.EnUsCultureId;
                default:
                    return CultureConstants.EnUsCultureId;
            }
        }
    }
}
