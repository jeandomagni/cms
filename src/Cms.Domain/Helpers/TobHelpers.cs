﻿using Cms.Model.Models;
using System;

namespace Cms.Domain.Helpers
{
    public interface ITobHelpers
    {
        TobAndCultureId ParseTobAndCultureIdStr(string tobCultureId);
    }
    
    public class TobHelpers : ITobHelpers
    {
        /// <summary>
        /// Parses a custom tob and culture id with a | delimiter such as 90040-000|en-US or Accounting|en-CA, etc
        /// </summary>
        /// <param name="tobCultureId">Tob culture id</param>
        /// <returns></returns>
        public TobAndCultureId ParseTobAndCultureIdStr(string tobCultureId)
        {
            if (string.IsNullOrWhiteSpace(tobCultureId))
            {
                throw new ArgumentException(@"Must not be empty", nameof(tobCultureId));
            }

            var delimiter = '|';
            var arr = tobCultureId.Split(delimiter);
            var cultureInfo = arr[1].Trim();
            return new TobAndCultureId {
                Tob = arr[0].Trim(),
                CultureInfo = cultureInfo
            };
        }
    }
}
