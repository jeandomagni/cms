﻿using SolrNet;
using System;
using System.Linq;
using System.Text;

namespace Cms.Domain.Util
{
    public static class QueryUtil
    {
        public static string[] Tokenize(string search, bool phrase = false)
        {
            var tokens = search.Split(new[] { '-', ',', ' ', '\'', '.', '(', ')', '@', ':' },
                StringSplitOptions.RemoveEmptyEntries);
            return phrase ? tokens.Select(s => $"\"{s.Trim()}\"").ToArray() : tokens.Select(s => s.Trim()).ToArray();
        }

        private static readonly string[] EscapeStrings = { "\\", "/", "+", "-", "&&", "||", "!", "(", ")", "{", "}", "[", "]", "^", "\"", "~", "*", "?", ":" };

        public static string EscapeQueryValue(string value)
        {
            return EscapeStrings.Aggregate(value.Replace("-", " "), (current, special) => current.Replace(special,
                $"\\{special}"));
        }

        public static AbstractSolrQuery ExplodedTokenizedQuery(string searchText, string op = "AND", bool phrased = true)
        {
            var tokens = Tokenize(searchText)
                .Select(EscapeQueryValue)
                .Select(x => phrased ? $"\"{x}\"" : x);

            if (tokens.Count() > 1)
            {
                return new SolrMultipleCriteriaQuery(tokens.Select(x => new SolrQuery(x)), op);
            }

            return new SolrQuery(tokens.FirstOrDefault());
        }

        public static string CleanAccentCharacters(string value)
        {
            var tempBytes = Encoding.GetEncoding("ISO-8859-8").GetBytes(value);
            return Encoding.UTF8.GetString(tempBytes);
        }

        public static string ToSolrQuerySafeString(this string value)
        {
            return EscapeQueryValue(CleanAccentCharacters(value));
        }
    }
}
