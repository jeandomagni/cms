﻿using Cms.Domain.Exception.Models;

namespace Cms.Domain.Exception
{
    public class InvalidEventRecurrenceException : CmsUserInputException
    {
        public InvalidEventRecurrenceException(string msg) : base(msg)
        {
        }
    }
}
