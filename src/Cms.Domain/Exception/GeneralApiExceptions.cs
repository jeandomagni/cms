﻿using Cms.Domain.Exception.Models;
using Microsoft.Extensions.Logging;
using System.Net;

namespace Cms.Domain.Exception
{
    public class GeneralApiException: CmsGeneralException
    {      
        public GeneralApiException() : base("An error has occurred. Please try again or contact system support")
        {
        }
    }

    public class NullParameterException : CmsGeneralException
    {
        public NullParameterException() : base("Missing request parameter")
        {
        }
    }

    public class TitleRequiredException : CmsUserInputException {
        public TitleRequiredException() : base("A title is needed to create content")
        {
        }
    }

    public class CreditRequiredException : CmsUserInputException
    {
        public CreditRequiredException() : base("Error: Need to add photo credit. For Getty Images, enter \"Getty.\"")
        {
        }
    }

    public class LayoutRequiredException : CmsUserInputException
    {
        public LayoutRequiredException() : base("A layout is required for this content")
        {
        }
    }

    public class InvalidFileExtException : CmsUserInputException
    {
        public InvalidFileExtException(string extension) : base($"Invalid file extension: {extension}")
        {
        }
    }

    public class StringLengthLimitException : CmsUserInputException
    {
        public StringLengthLimitException(string field, string limit) : base($"{field} character limit of {limit} exceeded")
        {
        }
    }

    public class FieldRequiredException : CmsUserInputException
    {
        public FieldRequiredException(string field) : base($"{field} is a required field")
        {
        }
    }

    public class InvalidFieldException : CmsUserInputException
    {
        public InvalidFieldException(string field) : base($"{field} value is invalid")
        {
        }
    }

    public class ContentTypeRequiredException : CmsUserInputException
    {
        public ContentTypeRequiredException() : base("A type is needed to create content")
        {
        }
    }

    public class InvalidContentTypeException : CmsUserInputException
    {
        public InvalidContentTypeException() : base("Invalid content type")
        {
        }
    }

    public class InvalidContentLayoutException : CmsUserInputException
    {
        public InvalidContentLayoutException() : base("Invalid content layout")
        {
        }
    }

    public class CheckedOutContentException : CmsUserInputException
    {
        public CheckedOutContentException() : base("Content is checked out by another user")
        {
        }
    }

    public class RecordNotFoundException : CmsGeneralException
    {
        public RecordNotFoundException() : base("Record not found")
        {
            HttpStatusCode = HttpStatusCode.NotFound;
            LogLevel = LogLevel.Warning;
        }
    }

    public class InvalidContentStatusException : CmsGeneralException
    {
        public InvalidContentStatusException() : base("Invalid content status for requested action")
        {
            HttpStatusCode = HttpStatusCode.BadRequest;
        }
    }

    public class SolrRecordUpdateException : CmsGeneralException
    {
        public SolrRecordUpdateException() : base("Error updating record in Solr")
        {
        }
    }

    public class MissingUserDefaultSiteException : CmsUserInputException
    {
        public MissingUserDefaultSiteException() : base("A default site is required. Please configure your user profile.")
        {
        }
    }

    public class InvalidRequestorException : CmsUserInputException
    {
        public InvalidRequestorException() : base("User not authorized for this request")
        {
            HttpStatusCode = HttpStatusCode.Forbidden;
        }
    }

    public class UserProfileNotConfiguredException : CmsUserInputException
    {
        public UserProfileNotConfiguredException() : base("User profile not configured")
        {
            HttpStatusCode = HttpStatusCode.NotFound;
        }
    }

    public class InvalidDomainRequestorException : CmsGeneralException
    {
        public InvalidDomainRequestorException() : base("Only users in a record's domain may modify it")
        {
            HttpStatusCode = HttpStatusCode.Forbidden;
        }
    }

    public class InvalidPublishDateException : CmsUserInputException
    {
        public InvalidPublishDateException() : base("Invalid publish date provided. Date must be in the future")
        {
            HttpStatusCode = HttpStatusCode.BadRequest;
        }
    }

    public class InvalidExpirationDateException : CmsUserInputException
    {
        public InvalidExpirationDateException() : base("Invalid expiration date provided. Date must be in the future and must occur after the publish date")
        {
        }
    }

    public class InvalidUrlException : CmsUserInputException
    {
        public InvalidUrlException(string msg) : base(msg)
        {
        }
    }

    public class ExternalUrlException : CmsUserInputException
    {
        public ExternalUrlException(string msg) : base(msg)
        {
        }
    }

    public class MissingAuthorException : CmsUserInputException
    {
        public MissingAuthorException() : base("A content author is required.")
        {
        }
    }

    public class MissingLocationTagException : CmsUserInputException
    {
        public MissingLocationTagException() : base("Content requires at least one location tag")
        {
        }
    }

    public class MissingCarouselException : CmsUserInputException
    {
        public MissingCarouselException() : base("Content type requires at least one carousel media item")
        {
        }
    }

    public class MaxFileSizeException : CmsUserInputException
    {
        public MaxFileSizeException(string fileName, int maxUploadSize) : base($"{fileName} must be smaller than {maxUploadSize} KB")
        {
        }
    }

    public class InvalidAreaCodeException : CmsUserInputException
    {
        public InvalidAreaCodeException(string areaCode) : base($"Invalid area code provided: {areaCode}. Please enter either 3 digits (area code) or 6 digits (area code and prefix).")
        {
        }
    }

    public class DuplicateSegmentUrlException : CmsUserInputException
    {
        public DuplicateSegmentUrlException() : base("A baseline page with this URL segment already exists.")
        {
        }
    }

    public class SegmentUrlEqualToRedirectUrlException : CmsUserInputException
    {
        public SegmentUrlEqualToRedirectUrlException() : base("URL Segment should be different from Redirect URL to create shortlink")
        {
        }
    }

    public class SegmentUrlNotFoundException : CmsUserInputException
    {
        public SegmentUrlNotFoundException() : base("Segment URL is needed to create baseline page")
        {
        }
    }

    public class RedirectUrlNotFoundException : CmsUserInputException
    {
        public RedirectUrlNotFoundException() : base("A redirect URL is needed to create a shortlink")
        {
        }
    }
}
