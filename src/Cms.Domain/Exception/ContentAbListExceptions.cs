﻿using Cms.Domain.Exception.Models;

namespace Cms.Domain.Exception
{
    public class EmptyAbLinkListException : CmsUserInputException
    {
        public EmptyAbLinkListException() : base("Please provide links for all locations, or delete any locations without links.")
        {
        }
    }

    public class InvalidTitleForAbLinkListException : CmsUserInputException
    {
        public InvalidTitleForAbLinkListException() : base("Please provide a valid title for all locations.")
        {
        }
    }
}
