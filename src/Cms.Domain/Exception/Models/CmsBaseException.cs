﻿using Microsoft.Extensions.Logging;
using System.Net;

namespace Cms.Domain.Exception.Models
{
    public class CmsBaseException : System.Exception
    {
        public CmsBaseException(string errorMessage) : base(errorMessage)
        {
            ErrorMessage = errorMessage;
        }

        public string ErrorMessage { get; set; }
        public HttpStatusCode HttpStatusCode { get; set; }
        public LogLevel LogLevel { get; set; }
    }
}
