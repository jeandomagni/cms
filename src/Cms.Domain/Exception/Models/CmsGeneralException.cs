﻿using Microsoft.Extensions.Logging;
using System.Net;

namespace Cms.Domain.Exception.Models
{
    public class CmsGeneralException : CmsBaseException
    {
        public CmsGeneralException(string errorMessage) : base(errorMessage)
        {
            HttpStatusCode = HttpStatusCode.InternalServerError;
            LogLevel = LogLevel.Error;
        }
    }
}
