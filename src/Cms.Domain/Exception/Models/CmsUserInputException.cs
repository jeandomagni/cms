﻿using Microsoft.Extensions.Logging;
using System.Net;

namespace Cms.Domain.Exception.Models
{
    public class CmsUserInputException : CmsBaseException
    {
        public CmsUserInputException(string errorMessage) : base(errorMessage)
        {
            HttpStatusCode = HttpStatusCode.BadRequest;
            LogLevel = LogLevel.Information;
        }
    }
}
