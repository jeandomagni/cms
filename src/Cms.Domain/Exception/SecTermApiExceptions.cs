﻿using Cms.Domain.Exception.Models;

namespace Cms.Domain.Exception
{
    public class EmptyAliasNameException : CmsUserInputException
    {      
        public EmptyAliasNameException() : base("An alias name is required")
        {
        }
    }

    public class DuplicateAliasException : CmsUserInputException
    {
        public DuplicateAliasException() : base("An alias with this name already exists")
        {
        }
    }

    public class InvalidSecTermMetadataException : CmsUserInputException
    {
        public InvalidSecTermMetadataException() : base("All metadata values are required.")
        {
        }
    }

    public class AliasHasSpecialCharactersException : CmsUserInputException
    {
        public AliasHasSpecialCharactersException() : base("No special characters or punctuation allowed")
        {
        }
    }
}
