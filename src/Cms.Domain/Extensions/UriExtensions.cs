﻿using Cms.Domain.Helpers;
using System;

namespace Cms.Domain.Extensions
{
    public static class UriExtensions
    {
        /// <summary>
        /// Returns the base domain from a domain name
        /// Example: http://www.west-wind.com returns west-wind.com
        /// </summary>
        /// <param name="uri"></param>
        /// <returns></returns>
        public static string GetBaseDomain(this Uri uri)
        {
            if (uri.HostNameType == UriHostNameType.Dns)
                return StringHelpers.GetBaseDomain(uri.DnsSafeHost);

            return uri.Host;
        }
    }
}
