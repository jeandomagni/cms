﻿using HtmlAgilityPack;
using Netko.Common.Util.Seo;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Cms.Domain.Extensions
{
    public static class StringExtensions
    {
        public static T DeserializeOrDefault<T>(this string serializedJson, T defaultObject)
        {
            try
            {
                var deserializedObject = !string.IsNullOrEmpty(serializedJson)
                    ? JsonConvert.DeserializeObject<T>(
                        serializedJson,
                        new JsonSerializerSettings {
                            ContractResolver = new CamelCasePropertyNamesContractResolver()
                        })
                    : defaultObject;
                return deserializedObject;
            }
            catch
            {
                return defaultObject;
            }
        }

        public static string ToUrlSlug(this string phrase)
        {
            return SeoFriendlyStringSanitizer.Sanitize(phrase);
        }

        public static string StripHtml(this string html)
        {
            if (string.IsNullOrWhiteSpace(html)) return null;
            var document = new HtmlDocument();
            document.LoadHtml(html);
            return HtmlEntity.DeEntitize(document.DocumentNode.InnerText);
        }

        public static string ToSubstring(this string value, int maxLength, string defaultValue = null)
        {
            var substring = value ?? defaultValue;

            if (!string.IsNullOrWhiteSpace(substring))
            {
                substring = substring.Trim();
                substring = substring.Length > maxLength ? substring.Substring(0, maxLength) : substring;
            }

            return substring;
        }
    }
}
