﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cms.Domain.Extensions
{
    /// <summary>
    /// Extensions for DateTime
    /// </summary>
    public static class DateTimeExtensions
    {
        /// <summary>
        /// Sets time to start of day
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static DateTime ToStartOfDay(this DateTime dateTime)
        {
            return new DateTime(
               dateTime.Year,
               dateTime.Month,
               dateTime.Day,
               0, 0, 0, 0);
        }

        public static DateTime ToTimeOfDateOrStartOfDay (this DateTime dateTime, DateTime? timeOfDate)
        {
            return timeOfDate.HasValue ? dateTime.ToStartOfDay() + timeOfDate.Value.TimeOfDay : dateTime.ToStartOfDay();
        }

        public static DateTime ToTimeOfDateOrEndOfDay(this DateTime dateTime, DateTime? timeOfDate)
        {
            return timeOfDate.HasValue ? dateTime.ToStartOfDay() + timeOfDate.Value.TimeOfDay : dateTime.ToEndOfDay();
        }

        /// <summary>
        /// Sets time to end of day
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static DateTime ToEndOfDay(this DateTime dateTime)
        {
            return new DateTime(
               dateTime.Year,
               dateTime.Month,
               dateTime.Day,
               23, 59, 59, 0);
        }
        
        public static string ToSolrDate(this DateTimeOffset date)
        {
            if (date == null || date == DateTimeOffset.MinValue)
            {
                return null;
            }

            return date.ToString("s") + "Z";
        }
    }
}
