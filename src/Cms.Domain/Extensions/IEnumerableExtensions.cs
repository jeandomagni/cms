﻿using Bbb.Core.Utilities.Extensions;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Cms.Domain.Extensions
{
    public static class IEnuemrableExtensions
    {
        public static string SerializeOrDefault<T>(this IEnumerable<T> list, string defaultSerialization = null)
        {
            if (list.IsNullOrEmpty())
            {
                return defaultSerialization;
            }

            return JsonConvert.SerializeObject(list);
        }
    }
}
