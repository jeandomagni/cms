﻿using Cms.Model.Attributes;
using Cms.Model.Models.General;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Reflection;

namespace Cms.Domain.Extensions
{
    public static class ObjectExtensions
    {
        public static string SerializeObject(this object objectToSerialize)
        {
            return JsonConvert.SerializeObject(objectToSerialize);
        }

        public static IList<Variance> DetailedCompare<T>(this T val1, T val2)
        {
            var variances = new List<Variance>();

            var properties = val1.GetType().GetProperties();
            foreach (var property in properties)
            {
                var v = new Variance();
                v.ValueA = property.GetValue(val1);
                v.ValueB = property.GetValue(val2);

                if (!Equals(v.ValueA, v.ValueB))
                {
                    v.PropertyName = property.Name;
                    v.Action = property.GetCustomAttribute<ActionAttribute>()?.Value;
                    variances.Add(v);
                }
            }

            return variances;
        }
    }
}
