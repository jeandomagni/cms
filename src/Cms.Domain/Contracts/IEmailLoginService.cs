﻿namespace Cms.Domain.Contracts
{
    public interface IEmailLoginService
    {
        void SendVerificationCode(string email, string verificationCode);
    }
}