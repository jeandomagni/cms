﻿using System;
using System.Collections.Generic;

namespace Cms.Domain.Contracts
{
    public interface IServiceLocator
    {
        object Create(Type serviceType);
        object TryToCreate(Type requestedType);
        T Create<T>();
        T TryToCreate<T>();
        IEnumerable<T> CreateMany<T>();
    }
}