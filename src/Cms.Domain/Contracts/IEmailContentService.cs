﻿using Cms.Model.Models.Content;
using Cms.Model.Models.Content.Db;

namespace Cms.Domain.Contracts
{
    public interface IEmailContentService
    {
        void SendReviewNotification(Content content);
        void SendApprovalNotification(Content content, DbContent existingContent);
    }
}