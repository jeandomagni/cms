﻿namespace Cms.Domain.Contracts
{
    public interface IAccountPasswordService
    {
        void EmailPassword(string email, string temporaryPassword);
    }
}