﻿using Cms.Model.Models.Email;

namespace Cms.Domain.Contracts
{
    public interface IEmailBaseService
    {
        void Send(EmailMessage emailMessage);
    }
}