﻿using Cms.Domain.Services;
using Cms.Model.Enum;
using Cms.Model.Services;
using log4net;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Cms.Domain.Handlers
{
    public interface IAuthenticationHandler
    {
        Task<bool> AuthenticateExternalUser(string user, string pass);
    }

    public class AuthenticationHandler : IAuthenticationHandler
    {
        private readonly IConnectionStringService _connectionStringService;
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public AuthenticationHandler(
            IConnectionStringService connectionStringService)
        {
            _connectionStringService = connectionStringService;
        }

        public async Task<bool> AuthenticateExternalUser(string username, string password)
        {
            return true;

            using (var client = new HttpClient())
            {
                try
                {
                    var serviceEndpoint =
                        _connectionStringService.GetVendorConnectionString(
                            Repository.PheonixApi);
                    var response =
                        await client.PostAsync(
                            serviceEndpoint + "/authentication",
                            new StringContent(
                                JsonConvert.SerializeObject(
                                    new
                                    {
                                        username = username,
                                        password = password
                                    }), Encoding.UTF8, "application/json"));

                    bool.TryParse(
                        await response.Content.ReadAsStringAsync(), out var authenticated);

                    return authenticated;
                }
                catch (System.Exception exception)
                {
                    _log.Error(exception);
                    return false;
                }
            }
        }
    }
}