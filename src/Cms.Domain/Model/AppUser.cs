﻿using System;
using Cms.Domain.Enum;
using Cms.Model.Models.User;
using System.Collections.Generic;
using System.Linq;

namespace Cms.Domain.Model
{
    public class AppUser
    {
        public string UserId;
        public string Email;
        public string DefaultSite;
        public string Iat;
        public string Domain => GetDomain(Email);
        public string[] AdminSites;
        public IEnumerable<UserRole> Roles;

        public bool HasAccessToRecord(string createdBy, string site)
        {
            if (HasRole(UserRoles.GlobalAdmin))
                return true;

            return
                AdminSites.Contains(site) || createdBy == Email;
        }

        public bool HasRole(string roleName)
        {
            return Roles == null ?
                false : Roles.Any(r => r.RoleName.Equals(roleName, StringComparison.OrdinalIgnoreCase));
        }

        public bool IsAdminForSite(string siteId)
        {
            return
                AdminSites.Contains(siteId);
        }

        private string GetDomain(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return null;

            var domain = Email.Split('@');
            return domain.Length > 1 ? domain[1] : domain[0];
        }

        public bool IsAdmin => HasRole("episerveradmin");
        public bool IsGlobalAdmin { get; set; }
        public bool IsLocalAdmin { get; set; }

        public bool GlobalOrLocalAdmin => IsGlobalAdmin || IsLocalAdmin;
    }
}