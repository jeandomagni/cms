﻿using System.Linq;
using System.Reflection;

namespace Cms.Domain.Enum
{
    public static class ContentLayout
    {
        public const string ImageContent = "Image-Content";
        public const string ContentImage = "Content-Image";
        public const string ContentImageCarousel = "Content-Image-Carousel";
        public const string NearMe = "Near-Me";
        public const string ContentVideo = "Content-Video";
        public const string ContentImageTopVideoBottom = "Content-Image-Top-Video-Bottom";
        public const string ContentVideoTopImageBottom = "Content-Video-Top-Image-Bottom";
        public const string ContentVideoTop = "Content-Video-Top";
        public const string ContentVideoCarousel = "Content-Video-Carousel";
        public const string ContentImageCarouselVideoCarousel = "Content-Image-Carousel-Video-Carousel";
        public const string ContentImageTopImageCarouselBottom = "Content-Image-Top-Image-Carousel-Bottom";
        public const string ABListAutomatic = "AB-List-Automatic";
        public const string ABListOneColumn = "AB-List-One-Column";
        public const string ABListTwoColumn = "AB-List-Two-Column";
        public const string BaselinePage = "Baseline-Page";
        public const string BaselinePageImage = "Baseline-Page-Image";

        public static bool IsValidLayout(string layoutName, string contentType)
        {
            if (string.Equals(ContentType.NearMe, contentType))
            {
                return string.Equals(layoutName, NearMe);
            }

            if (string.Equals(ContentType.AccreditedBusinessList, contentType))
            {
                return string.Equals(layoutName, ABListAutomatic)
                    || string.Equals(layoutName, ABListOneColumn)
                    || string.Equals(layoutName, ABListTwoColumn);
            }

            return typeof(ContentLayout)
                .GetFields(BindingFlags.Public | BindingFlags.Static)
                .Select(x => x.GetRawConstantValue().ToString())
                .Where(x => !string.Equals(NearMe, x))
                .Contains(layoutName);
        }
    }
}
