﻿namespace Cms.Domain.Enum
{
    public static class UserRoles
    {
        public const string GlobalAdmin = "episerveradmin";
        public const string SiteAdmin = "siteadmin";
        public const string LocalAuthor = "episerveradmin";
        public const string SystemAuthor = "episerveradmin";
    }
}
