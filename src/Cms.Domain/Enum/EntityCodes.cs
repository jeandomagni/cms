﻿namespace Cms.Domain.Enum
{
    public class EntityCodes
    {
        public static string ARTICLE = "ARTICLE";
        public static string FILE = "FILE";
        public static string NEARME = "NEARME";
        public static string EVENT = "EVENT";
        public static string BBB = "BBB";
        public static string USER = "USER";
        public static string ABLIST = "ABLIST";
        public static string BLPAGE = "BLPAGE";
    }
}
