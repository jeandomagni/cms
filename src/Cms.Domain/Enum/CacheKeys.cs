﻿namespace Cms.Domain.Enum
{
    public static class CacheKeys
    {
        public const string BbbServiceAreas = "bbbserviceareas";
        public const string BbbSites = "bbbsites";
        public const string StateProvinces = "stateprovinces";
    }
}
