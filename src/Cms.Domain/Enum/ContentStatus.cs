﻿namespace Cms.Domain.Enum
{
    public static class ContentStatus
    {
        public const string Published = "published";
        public const string Draft = "draft";
        public const string PendingReview = "pending_review";
        public const string Deleted = "deleted";
        public const string Rejected = "rejected";
    }
}
