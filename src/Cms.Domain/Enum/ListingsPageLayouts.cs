﻿namespace Cms.Domain.Enum
{
    public static class ListingsPageLayouts
    {
        public const string Standard = "standard";
        public const string Agenda = "agenda";
    }
}
