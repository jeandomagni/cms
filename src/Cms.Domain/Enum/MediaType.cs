﻿using System.Linq;
using System.Reflection;

namespace Cms.Domain.Enum
{
    public static class MediaType
    {
        public const string ContentImage = "ContentImage";
        public const string SiteImage = "SiteImage";

        public static bool IsValidMediaType(string type) {
            return typeof(ContentType).GetFields(BindingFlags.Public | BindingFlags.Static).
                Select(x => x.GetRawConstantValue().ToString()).Contains(type);
        }
    }
}
