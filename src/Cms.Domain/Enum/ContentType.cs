﻿using System.Linq;
using System.Reflection;

namespace Cms.Domain.Enum
{
    public static class ContentType
    {
        public const string NewsRelease = "News Release";
        public const string Article = "Article";
        public const string Event = "Event";
        public const string ScanAlert = "Scam Alert";
        public const string Warning = "Warning";
        public const string Investigation = "Investigation";
        public const string Tips = "Tips";
        public const string Opinion = "Opinion";
        public const string Blog = "Blog";
        public const string Espanol = "Espanol";
        public const string Image = "Image";
        public const string NearMe = "Near Me";
        public const string Quote = "Quote";
        public const string BaselinePage = "BaselinePage";
        public const string AccreditedBusinessList = "AB List";

        public static bool IsValidContentType(string type) {
            return typeof(ContentType).GetFields(BindingFlags.Public | BindingFlags.Static).
                Select(x => x.GetRawConstantValue().ToString()).Contains(type);
        }
    }
}
