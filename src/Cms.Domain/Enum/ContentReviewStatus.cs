﻿namespace Cms.Domain.Enum
{
    public static class ContentReviewStatus
    {
        public const string Approved = "approved";
        public const string Rejected = "rejected";
    }
}
