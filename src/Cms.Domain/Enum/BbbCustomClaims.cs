﻿namespace Cms.Domain.Enum
{
    public static class BbbCustomClaims
    {
        public const string UserId = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name";
        public const string Email = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress";
        public const string Roles = "http://schemas.xmlsoap.org/ws/2009/09/identity/claims/actor";
        public const string AdminSites = "http://schemas.xmlsoap.org/ws/2009/09/identity/claims/adminsites";
        public const string GlobalAdmin= "http://schemas.xmlsoap.org/ws/2009/09/identity/claims/globalAdmin";
        public const string LocalAdmin = "http://schemas.xmlsoap.org/ws/2009/09/identity/claims/localAdmin";
    }
}
