﻿using System.Linq;
using System.Reflection;

namespace Cms.Domain.Enum
{
    public static class LocationTypes
    {
        public const string Country = "country";
        public const string State = "state";
        public const string City = "city";
        public const string BbbId = "bbbId";
    }
}
