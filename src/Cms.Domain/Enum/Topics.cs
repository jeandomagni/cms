﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Cms.Domain.Enum
{
    public class Topics
    {
        public const string Default ="News Releases";
        public const string Auto = "Auto";
        public const string Blog = "Blog";
        public const string Business = "Business";
        public const string Espanol = "Espanol";
        public const string Events = "Events";
        public const string Family = "Family";
        public const string HealthSafety = "Health and Safety";
        public const string Home = "Home";
        public const string Investigations = "Investigations";
        public const string Money = "Money";
        public const string NewsReleases = "News Releases";
        public const string Opinion = "Opinion";
        public const string Scams = "Scams";
        public const string Shopping = "Shopping";
        public const string Technology = "Technology";
        public const string Tips = "Tips";
        public const string TravelTransportation = "Travel and Transportation";
        public const string Warnings = "Warnings";
        

        public static bool IsValidTopic(string topic)
        {
            if (string.IsNullOrWhiteSpace(topic))
            {
                return false;
            }

            return typeof(Topics).GetFields(BindingFlags.Public | BindingFlags.Static).
                Select(x => x.GetRawConstantValue().ToString()).Contains(topic);
        }

        public static List<string> GetAll()
        {
            return typeof(Topics).GetFields(BindingFlags.Public | BindingFlags.Static).
                Select(x => x.GetRawConstantValue().ToString()).ToList();
        }
    }
}
