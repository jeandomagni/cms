﻿using Bbb.Core.Solr.Model;
using Cms.Model.Models.Content;
using Cms.Model.Models.Content.Db;
using Cms.Model.Models.Recurrence;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace Cms.Domain.Mappers
{
    public interface IContentEventMappers
    {
        ContentEvent MapToContentEvent(DbContentEvent dbContentEvent);
        DbContentEvent MapToDbContentEvent(ContentEvent contentEvent);
        Article MapToSolrEventOccurrence(DbContent dbContent, DbContentEvent contentEvent, ContentEventOccurrence occurrence);
        IList<Article> MapToSolrEventOccurrences(DbContent dbContent, DbContentEvent contentEvent, IEnumerable<ContentEventOccurrence> occurrences);
    }

    public class ContentEventMappers : IContentEventMappers
    {
        private readonly IContentMappers _contentMappers;
        public ContentEventMappers(IContentMappers contentMappers)
        {
            _contentMappers = contentMappers;
        }

        public Article MapToSolrEventOccurrence(
            DbContent dbContent,
            DbContentEvent contentEvent,
            ContentEventOccurrence occurrence)
        {
            var doc = _contentMappers.MapToSolrArticle(dbContent, contentEvent, null, null);
            doc.Id = $"{dbContent.Id}_{occurrence.Id}";
            doc.EventStartDate = occurrence.StartDate.DateTime;
            doc.EventEndDate = occurrence.EndDate.DateTime;
            doc.CanonicalUrl = doc.CanonicalUrl?.Replace($"/{dbContent.Id}-", $"/{doc.Id}-");
            return doc;
        }

        public IList<Article> MapToSolrEventOccurrences(
            DbContent dbContent,
            DbContentEvent contentEvent,
            IEnumerable<ContentEventOccurrence> occurrences)
        {
            return occurrences
                .Select(x => MapToSolrEventOccurrence(dbContent, contentEvent, x))
                .ToList();
        }

        public DbContentEvent MapToDbContentEvent(ContentEvent contentEvent)
        {
            var dbContentEvent = new DbContentEvent();
            dbContentEvent.AddressLine = contentEvent.AddressLine;
            dbContentEvent.AddressLine2 = contentEvent.AddressLine2;
            dbContentEvent.City = contentEvent.City;
            dbContentEvent.ContentCode = contentEvent.ContentCode;
            dbContentEvent.Country = contentEvent.Country;
            dbContentEvent.EndDate = contentEvent.EndDate;
            dbContentEvent.Id = contentEvent.Id;
            dbContentEvent.IsVirtualEvent = contentEvent.IsVirtualEvent;
            dbContentEvent.LocationName = contentEvent.LocationName;
            dbContentEvent.PostalCode = contentEvent.PostalCode;
            dbContentEvent.Recurrence = contentEvent.Recurrence != null
                ? JsonConvert.SerializeObject(contentEvent.Recurrence)
                : null;
            dbContentEvent.RegistrationLink = contentEvent.RegistrationLink;
            dbContentEvent.StartDate = contentEvent.StartDate;
            dbContentEvent.State = contentEvent.State;
            return dbContentEvent;
        }

        public ContentEvent MapToContentEvent(DbContentEvent dbContentEvent)
        {
            var contentEvent = new ContentEvent();
            contentEvent.AddressLine = dbContentEvent.AddressLine;
            contentEvent.AddressLine2 = dbContentEvent.AddressLine2;
            contentEvent.City = dbContentEvent.City;
            contentEvent.ContentCode = dbContentEvent.ContentCode;
            contentEvent.Country = dbContentEvent.Country;
            contentEvent.EndDate = dbContentEvent.EndDate;
            contentEvent.HasRecurrence = EventRecurrence.TryParse(
                dbContentEvent.Recurrence,
                out EventRecurrence eventRecurrence);
            contentEvent.Id = dbContentEvent.Id;
            contentEvent.IsVirtualEvent = dbContentEvent.IsVirtualEvent;
            contentEvent.LocationName = dbContentEvent.LocationName;
            contentEvent.PostalCode = dbContentEvent.PostalCode;
            contentEvent.Recurrence = eventRecurrence;
            contentEvent.RegistrationLink = dbContentEvent.RegistrationLink;
            contentEvent.StartDate = dbContentEvent.StartDate;
            contentEvent.State = dbContentEvent.State;
            return contentEvent;
        }
    }
}
