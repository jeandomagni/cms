﻿using Cms.Model.Models.Content.Entities;
using Cms.Model.Models.ContentLocation;
using Cms.Model.Models.ContentLocation.Db;
using Newtonsoft.Json;
using System;
using SolrLocation = Bbb.Core.Solr.Model.Location;

namespace Cms.Domain.Mappers
{
    public static class PinnedContentLocationMappers
    {
        public static DbPinnedContentLocation MapToDbPinnedContentLocation(PinnedContentLocation pinnedContentLocation)
        {
            var coreCmsPinnedContentLocation = new DbPinnedContentLocation();
            coreCmsPinnedContentLocation.Id = pinnedContentLocation.Id;
            coreCmsPinnedContentLocation.PinExpirationDate = pinnedContentLocation.PinExpirationDate.DateTime.Date;
            coreCmsPinnedContentLocation.PinnedLocationId = pinnedContentLocation.PinnedLocation;
            coreCmsPinnedContentLocation.PinnedLocationType = pinnedContentLocation.LocationType;
            return coreCmsPinnedContentLocation;
        }

        public static DbPinnedContentLocation MapToDbPinnedContentLocation(PinnedContentLocation pinnedContentLocation, string contentCode, string currentUser)
        {
            var coreCmsPinnedContentLocation = new DbPinnedContentLocation();
            coreCmsPinnedContentLocation.ContentCode = contentCode;
            coreCmsPinnedContentLocation.CreatedDate = DateTime.Now;
            coreCmsPinnedContentLocation.CreatedBy = currentUser;
            coreCmsPinnedContentLocation.Id = pinnedContentLocation.Id;
            coreCmsPinnedContentLocation.PinExpirationDate = pinnedContentLocation.PinExpirationDate;
            coreCmsPinnedContentLocation.PinnedLocationId = string.Equals(pinnedContentLocation.LocationType, "city", StringComparison.InvariantCultureIgnoreCase) ?
                JsonConvert.DeserializeObject<SolrLocation>(pinnedContentLocation.PinnedLocation).Id :
                pinnedContentLocation.PinnedLocation;
            coreCmsPinnedContentLocation.PinnedLocationType = pinnedContentLocation.LocationType;
            return coreCmsPinnedContentLocation;
        }
        
        public static PinnedContentLocation MapToPinnedContentLocation(DbPinnedContentLocation coreCmsPinnedContentLocation)
        {
            var pinnedContentLocation = new PinnedContentLocation();
            pinnedContentLocation.Id = coreCmsPinnedContentLocation.Id;
            pinnedContentLocation.PinnedLocation = coreCmsPinnedContentLocation.PinnedLocationId;
            pinnedContentLocation.LocationType = coreCmsPinnedContentLocation.PinnedLocationType;
            pinnedContentLocation.PinExpirationDate = coreCmsPinnedContentLocation.PinExpirationDate.DateTime.Date;
            return pinnedContentLocation;
        }

        public static PinnedContentLocation MapToPinnedContentLocation(ContentLocation contentLocation)
        {
            var pinnedContentLocation = new PinnedContentLocation();
            pinnedContentLocation.LocationType = contentLocation.Type;
            pinnedContentLocation.PinnedLocation = contentLocation.Location;
            return pinnedContentLocation;
        }

        public static DbPinnedContentLocation MapToDbPinnedContentLocation(Geotag geotag, string currentUser)
        {
            var dbPin = new DbPinnedContentLocation();
            dbPin.ContentCode = geotag.ContentCode;
            dbPin.CreatedDate = DateTime.Now;
            dbPin.CreatedBy = currentUser;
            dbPin.Id = geotag.PinId ?? 0;
            dbPin.PinExpirationDate = geotag.PinExpirationDate.Value;
            dbPin.PinnedLocationId = geotag.LocationId;
            dbPin.PinnedLocationType = geotag.LocationType;
            return dbPin;
        }
    }
}
