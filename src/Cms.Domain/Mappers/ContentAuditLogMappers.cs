﻿using Cms.Domain.Enum;
using Cms.Domain.Extensions;
using Cms.Domain.Helpers;
using Cms.Model.Enum;
using Cms.Model.Models.Content.Db;
using Cms.Model.Models.General;
using log4net;
using System.Collections.Generic;
using System.Linq;

namespace Cms.Domain.Mappers
{
    public interface IContentAuditLogMappers
    {
        IList<DbAuditLog> MapToDbAuditLogs(DbContent existingContent, DbContent updatedContent, string userId);
        IList<DbAuditLog> MapToDbAuditLogs(DbContentEvent existingContent, DbContentEvent updatedContent, int contentId, string userId);
        AuditLog MapToAuditLog(DbAuditLog dbAuditLog);
    }

    public class ContentAuditLogMappers : IContentAuditLogMappers
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public IList<DbAuditLog> MapToDbAuditLogs(DbContent existingContent, DbContent updatedContent, string userId)
        {
            var entityCode = ContentHelpers.MapContentTypeToEntity(existingContent.Type);
            return existingContent.DetailedCompare(updatedContent)
                .Where(x => !string.IsNullOrWhiteSpace(x.Action))
                .Select(x => MapToDbAuditLog(x.Action, entityCode, existingContent.Id, userId))
                .ToList();
        }

        public IList<DbAuditLog> MapToDbAuditLogs(DbContentEvent existingContent, DbContentEvent updatedContent, int contentId, string userId)
        {
            return existingContent.DetailedCompare(updatedContent)
                 .Where(x => !string.IsNullOrWhiteSpace(x.Action))
                 .Select(x => MapToDbAuditLog(x.Action, EntityCodes.EVENT, contentId, userId))
                 .ToList();
        }

        public AuditLog MapToAuditLog(DbAuditLog dbLog)
        {
            var log = new AuditLog();
            log.CreatedDate = dbLog.CreatedDate;
            log.Summary = FormatActivitySummary(dbLog.UserId, dbLog.ActionCode);
            return log;
        }

        private DbAuditLog MapToDbAuditLog(string action, string entityCode, int id, string userId)
        {
            var dbLog = new DbAuditLog();
            dbLog.ActionCode = action;
            dbLog.EntityCode = entityCode;
            dbLog.EntityId = id;
            dbLog.UserId = userId;
            return dbLog;
        }

        private string FormatActivitySummary(string userEmail, string actionCode) =>
            $"{userEmail} {GetDescription(actionCode)} this article";

        private string GetDescription(string actionCode)
        {
            switch (actionCode)
            {
                case EntityActions.CREATE:
                    return "created";
                case EntityActions.UPDATE:
                    return "updated";
                case EntityActions.PUBLISH:
                    return "published";
                case EntityActions.UNPUBLISH:
                    return "unpublished";
                case EntityActions.APPROVE:
                    return "approved";
                case EntityActions.UPDATE_IMAGE:
                    return "updated the images on";
                case EntityActions.UPDATE_CAROUSEL:
                    return "updated the carousel on";
                case EntityActions.UPDATE_TEXT:
                    return "updated the main text on";
                case EntityActions.UPDATE_SUMMARY:
                    return "updated the summary on";
                case EntityActions.UPDATE_PUBLISH_DATE:
                    return "updated the publish date on";
                case EntityActions.UPDATE_HEADLINE:
                    return "updated the headline on";
                case EntityActions.UPDATE_CITIES:
                    return "updated the cities on";
                case EntityActions.UPDATE_STATES:
                    return "updated the states on";
                case EntityActions.UPDATE_COUNTRIES:
                    return "updated the countries on";
                case EntityActions.UPDATE_AUTHOR:
                    return "updated the author on";
                case EntityActions.UPDATE_PRIORITY:
                    return "updated the priority on";
                case EntityActions.UPDATE_PAGE_TITLE:
                    return "updated the page title on";
                case EntityActions.UPDATE_PINNED_CITIES:
                    return "updated the pinned cities on";
                case EntityActions.UPDATE_PINNED_COUNTRIES:
                    return "updated the pinned countries on";
                case EntityActions.UPDATE_PINNED_STATES:
                    return "updated the pinned states on";
                case EntityActions.UPDATE_TOPICS:
                    return "updated the topics on";
                case EntityActions.UPDATE_SITES:
                    return "updated the sites on";
                case EntityActions.UPDATE_CATEGORIES:
                    return "updated the types of business on";
                case EntityActions.UPDATE_TYPE:
                    return "updated the type on";
                case EntityActions.UPDATE_META_DESCRIPTION:
                    return "updated the meta description on";
                case EntityActions.UPDATE_MOBILE_HEADLINE:
                    return "updated the mobile headline on";
                case EntityActions.UPDATE_NO_INDEX:
                    return "updated the no index field on";
                case EntityActions.UPDATE_EXPIRATION_DATE:
                    return "updated the expiration date on";
                case EntityActions.UPDATE_START_DATE:
                    return "updated the event start date on";
                case EntityActions.UPDATE_END_DATE:
                    return "updated the event end date on";
                case EntityActions.UPDATE_REGISTATION_LINK:
                    return "updated the event registration link date on";
                case EntityActions.UPDATE_TAGS:
                    return "updated the tags on";
                case EntityActions.CHECKOUT:
                    return "checked out";
                case EntityActions.CHECKIN:
                    return "checked in";
                case EntityActions.UPDATE_LAYOUT:
                    return "updated the layout on";
                case EntityActions.UPDATE_PINNED_TOPIC:
                    return "updated the pinned topic on";
                case EntityActions.UPDATE_VIDEOS:
                    return "updated the videos on";
                case EntityActions.UPDATE_URL:
                    return "updated the URL on";
                case EntityActions.UPDATE_SEO_CANONICAL:
                    return "updated the SEO cannonical on";
                case EntityActions.UPDATE_SERVICE_AREA:
                    return "updated the service area on";
                case EntityActions.UPDATE_PINNED_SERVICE_AREA:
                    return "updated the pinned service area on";
                case EntityActions.UPDATE_REDIRECT_URL:
                    return "updated the redirect URL on";
                default:
                    _log.Warn($"{actionCode} missing from ContentAuditLogMappers.GetDescription()");
                    return actionCode;
            }
        }
    }
}
