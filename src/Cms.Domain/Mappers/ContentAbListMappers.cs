﻿using Cms.Domain.Extensions;
using Cms.Model.Models.Content;
using Cms.Model.Models.Content.Db;
using Cms.Model.Models.Content.Entities;
using System.Collections.Generic;

namespace Cms.Domain.Mappers
{
    public interface IContentAbListMappers
    {
        ContentAbList MapToContentAbList(DbContentAbList dbContentAbList);
        DbContentAbList MapToDbContentAbList(ContentAbList contentAbList);
    }

    public class ContentAbListMappers : IContentAbListMappers
    {
        public ContentAbList MapToContentAbList(DbContentAbList dbContentAbList)
        {
            var contentAbList = new ContentAbList();
            contentAbList.Id = dbContentAbList.Id;
            contentAbList.ContentCode = dbContentAbList.ContentCode;
            contentAbList.BusinessLinkLists = dbContentAbList.BusinessLinkLists.DeserializeOrDefault(new List<BusinessLinkList>());
            return contentAbList;
        }

        public DbContentAbList MapToDbContentAbList(ContentAbList contentAbList)
        {
            var dbContentAbList = new DbContentAbList();
            dbContentAbList.Id = contentAbList.Id;
            dbContentAbList.ContentCode = contentAbList.ContentCode;
            dbContentAbList.BusinessLinkLists = contentAbList.BusinessLinkLists.SerializeObject();
            return dbContentAbList;
        }
    }
}
