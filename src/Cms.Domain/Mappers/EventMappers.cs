﻿using Cms.Domain.Extensions;
using Cms.Model.Models.Content;
using Cms.Model.Models.Recurrence;
using System;

namespace Cms.Domain.Mappers
{
    public static class EventMappers
    {
        public static ContentEventOccurrence MapToContentEventOccurrence(DateTime date, EventRecurrence eventRecurrence, ContentEvent contentEvent)
        {
            var contentEventOccurrence = new ContentEventOccurrence();
            contentEventOccurrence.EventId = contentEvent.Id;
            contentEventOccurrence.StartDate = date.ToTimeOfDateOrStartOfDay(eventRecurrence.StartTime);
            contentEventOccurrence.EndDate = date.ToTimeOfDateOrEndOfDay(eventRecurrence.EndTime);
            return contentEventOccurrence;
        }
    }
}
