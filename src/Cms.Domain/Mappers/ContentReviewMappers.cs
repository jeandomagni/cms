﻿using Cms.Domain.Model;
using Cms.Model.Models.Content;

namespace Cms.Domain.Mappers
{
    public interface IContentReviewMappers
    {
        ContentReview MapToContentReview(Content content, AppUser user);
    }

    public class ContentReviewMappers : IContentReviewMappers
    {
        public ContentReview MapToContentReview(Content content, AppUser user)
        {
            var review = new ContentReview();
            review.CreatedBy = user.Email;
            review.ContentCode = content.Code;
            return review;
        }
    }
}
