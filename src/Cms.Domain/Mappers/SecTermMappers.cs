
using Bbb.Core.Solr.Enums;
using Cms.Domain.Extensions;
using Cms.Model.Models.SecTerm;
using Cms.Model.Models.SecTerm.Db;
using Newtonsoft.Json;
using System.Collections.Generic;
using SolrSearchTypeahead = Bbb.Core.Solr.Model.SearchTypeahead;
using SolrSecTerm = Bbb.Core.Solr.Model.SecTerm;

namespace Cms.Domain.Mappers
{
    public static class SecTermMappers
    {
        public static DbSecMetadata ToCoreSecMetadata(this SecTerm secTerm)
        {
            var secMetadata = new DbSecMetadata();
            secMetadata.TobId = secTerm.TobId;
            secMetadata.IsActive = secTerm.IsActive;
            secMetadata.ModifiedDate = secTerm.ModifiedDate;
            secMetadata.ModifiedUser = secTerm.ModifiedUser;
            secMetadata.MetadataJson = secTerm.Metadata.SerializeObject();
            secMetadata.MetadataBusinessJson = secTerm.MetadataBusiness.SerializeObject();
            return secMetadata;
        }

        public static DbCategory ToCoreCategory(this SecTerm secTerm)
        {
            var category = new DbCategory();
            category.CategoryName = secTerm.Name;
            category.CategoryURL = secTerm.UrlSegment;
            category.Spanish = secTerm.SpanishName;
            category.TOBId = secTerm.TobId;
            return category;
        }

        public static SolrSecTerm ToSecTermDocument(this SecTerm secTerm)
        {
            var content = new SolrSecTerm();
            content.TobId = secTerm.TobId;
            content.MetadataJson = JsonConvert.SerializeObject(secTerm.Metadata);
            content.MetadataBusinessJson = JsonConvert.SerializeObject(secTerm.MetadataBusiness);
            return content;
        }

        public static IList<SolrSearchTypeahead> ToSearchTypeaheadList(this SecTerm secTerm)
        {
            var searchTypeaheadList = new List<SolrSearchTypeahead>();

            var id = $"{secTerm.CategoryId}_{secTerm.CategoryCode}";

            if (!string.IsNullOrWhiteSpace(secTerm.Name))
            {
                var english = new SolrSearchTypeahead();
                english.Id = id;
                english.EntityId = secTerm.TobId;
                english.Title = secTerm.Name;
                english.Url = secTerm.UrlSegment;
                english.EntityType = EntityType.Category;
                english.CultureInfo = new List<string> { "en-US", "en-CA" };
                english.MetaTags = new List<string> { "primary", secTerm.Name.Substring(0, 1).ToUpper() };
                searchTypeaheadList.Add(english);
            }

            if (!string.IsNullOrWhiteSpace(secTerm.SpanishName))
            {
                var spanish = new SolrSearchTypeahead();
                spanish.Id = $"{id}_es";
                spanish.EntityId = secTerm.TobId;
                spanish.Title = secTerm.SpanishName;
                spanish.Url = secTerm.SpanishUrlSegment;
                spanish.EntityType = EntityType.Category;
                spanish.CultureInfo = new List<string> { "es-MX" };
                spanish.MetaTags = new List<string> { "primary", secTerm.SpanishName.Substring(0, 1).ToUpper() };
                searchTypeaheadList.Add(spanish);
            }

            return searchTypeaheadList;
        }
    }
}
