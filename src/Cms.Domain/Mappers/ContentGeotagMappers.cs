﻿using Cms.Model.Models.Content.Entities;

namespace Cms.Domain.Mappers
{
    public interface IContentGeotagMappers
    {
        Geotag MapToGeotag(string location, string contentCode, string locationType, PinnedLocationModel pin);
    }

    public class ContentGeotagMappers : IContentGeotagMappers
    {
        public Geotag MapToGeotag(
            string location,
            string contentCode,
            string locationType,
            PinnedLocationModel pin)
        {
            var geotag = new Geotag();
            geotag.ContentCode = contentCode;
            geotag.IsPinned = pin != null;
            geotag.LocationId = location;
            geotag.LocationType = locationType;
            geotag.PinExpirationDate = pin?.PinExpirationDate;
            geotag.PinId = pin?.Id;
            return geotag;
        }
    }
}
