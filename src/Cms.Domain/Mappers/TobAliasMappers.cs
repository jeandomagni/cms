﻿using Bbb.Core.Solr.Enums;
using Bbb.Core.Solr.Model;
using Cms.Model.Models.SecTerm;
using Cms.Model.Models.SecTerm.Db;
using System;
using System.Collections.Generic;

namespace Cms.Domain.Mappers
{
    public static class TobAliasMappers
    {
        public static TobAlias ToTobAlias(this DbTobAlias coreTobAlias)
        {
            var tobAlias = new TobAlias();
            tobAlias.AliasId = coreTobAlias.AliasId;
            tobAlias.CreatorUserId = coreTobAlias.CreatorUserId;
            tobAlias.DateTimeCreated = coreTobAlias.DateTimeCreated;
            tobAlias.Name = coreTobAlias.TobAlias;
            tobAlias.Spanish = coreTobAlias.Spanish;
            return tobAlias;
        }

        public static DbTobAlias ToCoreTobAlias(this AddEditAliasParams addEditAliasParams)
        {
            var coreTobAlias = new DbTobAlias();
            coreTobAlias.Description = addEditAliasParams.SecTerm.Name;
            coreTobAlias.AliasId = addEditAliasParams.Alias.AliasId;
            coreTobAlias.CreatorUserId = addEditAliasParams.Alias.CreatorUserId ?? 100;
            coreTobAlias.DateTimeCreated = addEditAliasParams.Alias.DateTimeCreated ?? DateTime.Now;
            coreTobAlias.Spanish = addEditAliasParams.Alias.Spanish;
            coreTobAlias.TobAlias = addEditAliasParams.Alias.Name;
            coreTobAlias.TobId = addEditAliasParams.SecTerm.TobId;
            coreTobAlias.UrlSegment = null;
            return coreTobAlias;
        }

        public static List<SearchTypeahead> ToSearchTypeaheadList(this AddEditAliasParams addEditAliasParams)
        {
            var searchTypeaheadList = new List<SearchTypeahead>();

            var id = $"{addEditAliasParams.Alias.AliasId}_{addEditAliasParams.SecTerm.TobId}_alias";

            if (!string.IsNullOrWhiteSpace(addEditAliasParams.Alias.Name))
            {
                var english = new SearchTypeahead();
                english.Id = id;
                english.EntityId = addEditAliasParams.SecTerm.TobId;
                english.Title = addEditAliasParams.Alias.Name;
                english.Url = addEditAliasParams.SecTerm.UrlSegment;
                english.EntityType = EntityType.Category;
                english.CultureInfo = new List<string> { "en-US", "en-CA" };
                english.MetaTags = new List<string> { "alias", addEditAliasParams.Alias.Name.Substring(0, 1).ToUpper() };
                searchTypeaheadList.Add(english);
            }

            if (!string.IsNullOrWhiteSpace(addEditAliasParams.Alias.Spanish))
            {
                var spanish = new SearchTypeahead();
                spanish.Id = $"{id}_es";
                spanish.EntityId = addEditAliasParams.SecTerm.TobId;
                spanish.Title = addEditAliasParams.Alias.Spanish;
                spanish.Url = addEditAliasParams.SecTerm.SpanishUrlSegment;
                spanish.EntityType = EntityType.Category;
                spanish.CultureInfo = new List<string> { "es-MX" };
                spanish.MetaTags = new List<string> { "alias", addEditAliasParams.Alias.Spanish.Substring(0, 1).ToUpper() };
                searchTypeaheadList.Add(spanish);
            }

            return searchTypeaheadList;
        }
    }
}
