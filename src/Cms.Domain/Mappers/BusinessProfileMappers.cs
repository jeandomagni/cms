﻿using Bbb.Core.Solr.Model;
using Cms.Model.Models.BusinessProfile;
using Cms.Model.Models.BusinessProfile.Db;
using Newtonsoft.Json;
using SolrNet;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Cms.Domain.Mappers
{
    public interface IBusinessProfileMappers
    {
        TypeaheadSuggestion MapToTypeaheadSuggestion(SearchTypeahead searchTypeahead);
        TypeaheadSuggestion MapToTypeaheadSuggestion(Cibr cibrDoc);
        SearchResultItem MapToSearchResultItem(Cibr cibrDoc);
        BusinessSearchResult MapToBusinessSearchResult(SolrQueryResults<Cibr> solrResult, ProfileSearchParams searchParams);
        Task<BusinessProfile> MapToBusinessProfileAsync(Cibr cibrDoc, DbBusinessProfileSeo dbBusinessProfileSeo);
    }

    public class BusinessProfileMappers : IBusinessProfileMappers
    {
        private IMetadataMappers _metadataMappers;
        public BusinessProfileMappers(IMetadataMappers metadataMappers)
        {
            _metadataMappers = metadataMappers;
        }

        public TypeaheadSuggestion MapToTypeaheadSuggestion(SearchTypeahead searchTypeahead)
        {
            var typeaheadSuggestion = new TypeaheadSuggestion();
            typeaheadSuggestion.Id = searchTypeahead.Id;
            typeaheadSuggestion.SecondaryTitle = searchTypeahead.SecondaryTitle;
            typeaheadSuggestion.Title = searchTypeahead.Title;
            return typeaheadSuggestion;
        }

        public TypeaheadSuggestion MapToTypeaheadSuggestion(Cibr cibrDoc)
        {
            var typeaheadSuggestion = new TypeaheadSuggestion();
            typeaheadSuggestion.Id = $"{cibrDoc.BbbId}-{cibrDoc.BusinessId}";
            typeaheadSuggestion.SecondaryTitle = cibrDoc.FullAddress;
            typeaheadSuggestion.Title = cibrDoc.BusinessName;
            return typeaheadSuggestion;
        }

        public SearchResultItem MapToSearchResultItem(Cibr cibrDoc)
        {
            var searchResultItem = new SearchResultItem();
            searchResultItem.BbbId = cibrDoc.BbbId;
            searchResultItem.BusinessId = cibrDoc.BusinessId;
            searchResultItem.BusinessName = cibrDoc.BusinessName;
            searchResultItem.DisplayAddress = cibrDoc.FullAddress;
            searchResultItem.Id = $"{cibrDoc.BbbId}-{cibrDoc.BusinessId}";
            searchResultItem.LogoUrl = cibrDoc.LogoUri;
            searchResultItem.ProfileUrl = cibrDoc.ProfileUrl;
            return searchResultItem;
        }

        public BusinessSearchResult MapToBusinessSearchResult(
            SolrQueryResults<Cibr> solrResult,
            ProfileSearchParams searchParams)
        {
            var businessSearchResult = new BusinessSearchResult();
            businessSearchResult.Items = solrResult?.Select(MapToSearchResultItem);
            businessSearchResult.PageCount = solrResult.NumFound > 0
                ? (int) Math.Ceiling((double) (solrResult.NumFound / searchParams.PageSize))
                : 1;
            businessSearchResult.ResultCount = solrResult.NumFound;
            businessSearchResult.SearchParams = searchParams;
            return businessSearchResult;
        }

        public async Task<BusinessProfile> MapToBusinessProfileAsync(Cibr cibrDoc, DbBusinessProfileSeo dbBusinessProfileSeo)
        {
            var businessProfile = new BusinessProfile();
            businessProfile.BbbId = cibrDoc.BbbId;
            businessProfile.BusinessId = cibrDoc.BusinessId;
            businessProfile.BusinessName = cibrDoc.BusinessName;
            businessProfile.DisplayAddress = cibrDoc.FullAddress;
            businessProfile.Id = $"{cibrDoc.BbbId}-{cibrDoc.BusinessId}";
            businessProfile.IsAccredited = cibrDoc.BbbMember;
            businessProfile.IsRaqActive = cibrDoc.IsRequestQuoteActive;
            businessProfile.LogoUrl = cibrDoc.LogoUri;
            businessProfile.ProfileUrl = cibrDoc.ProfileUrl;

            businessProfile.Metadata = await _metadataMappers.MapToMetadataBusinessAsync(
                dbBusinessProfileSeo?.MetadataJson,
                businessProfile);

            return businessProfile;
        }

        public static DbBusinessProfileSeo MapToDbBusinessProfileSeo(BusinessProfile businessProfile)
        {
            var dbBusinessProfileSeo = new DbBusinessProfileSeo();

            if (businessProfile.ProfileSeoId.HasValue)
            {
                dbBusinessProfileSeo.Id = businessProfile.ProfileSeoId.Value;
            }

            dbBusinessProfileSeo.BbbId = businessProfile.BbbId;
            dbBusinessProfileSeo.BusinessId = businessProfile.BusinessId;
            dbBusinessProfileSeo.MetadataJson = JsonConvert.SerializeObject(businessProfile.Metadata);

            return dbBusinessProfileSeo;
        }
    }
}
