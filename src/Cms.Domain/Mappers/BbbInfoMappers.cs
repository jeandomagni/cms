﻿using Ardalis.GuardClauses;
using Cms.Domain.BbbInfoGuardClauses;
using Cms.Domain.Cache;
using Cms.Domain.Extensions;
using Cms.Domain.Helpers;
using Cms.Model.Enum;
using Cms.Model.Models.BbbInfo;
using Cms.Model.Models.BbbInfo.Db;
using Cms.Model.Models.BbbInfo.Entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BbbInfoDocument = Bbb.Core.Solr.Model.BbbInfo;

namespace Cms.Domain.Mappers
{
    public interface IBbbInfoMappers
    {
        Task<BbbInfo> MapToBbbInfoAsync(DbBbbInfo dbBbbInfo);
        DbBbbInfo MapToDbBbbInfo(BbbInfo bbbInfo);
        BbbInfoDocument MapToSolrDoc(BbbInfo bbbInfo);
        BbbServiceArea MapToBbbServiceArea(BbbInfoDocument bbbInfoSolrDoc);
    }

    public class BbbInfoMappers : IBbbInfoMappers
    {
        private ITobCache _tobCache;
        public BbbInfoMappers(ITobCache tobCache)
        {
            _tobCache = tobCache;
        }

        public async Task<BbbInfo> MapToBbbInfoAsync(DbBbbInfo dbBbbInfo)
        {
            var bbbInfo = new BbbInfo();
            bbbInfo.AboutUsText = dbBbbInfo.AboutUsText;
            bbbInfo.Active = dbBbbInfo.Active;
            bbbInfo.AdditionalResources = dbBbbInfo.AdditionalResources.DeserializeOrDefault(new List<BbbInfoAdditionalResources>());
            bbbInfo.AllowVendorUriAliases = dbBbbInfo.AllowVendorUriAliases;
            bbbInfo.AnnualReportsUrl = dbBbbInfo.AnnualReportsUrl;
            bbbInfo.ApplyForAccreditationUrl = dbBbbInfo.ApplyForAccreditationUrl;
            bbbInfo.JoinApplyUrl = dbBbbInfo.JoinApplyUrl;
            bbbInfo.AreaCodes = StringHelpers.MapSerializedJsonListToCommaSeparatedString(dbBbbInfo.AreaCodes);
            bbbInfo.BaseUriAliases = StringHelpers.MapSerializedJsonListToCommaSeparatedString(dbBbbInfo.BaseUriAliases);
            bbbInfo.BbbUrlSegment = dbBbbInfo.BbbUrlSegment;
            bbbInfo.BusinessLoginUrl = dbBbbInfo.BusinessLoginUrl;
            bbbInfo.CharityProfileSource = dbBbbInfo.CharityProfileSource;
            bbbInfo.ClaimListingUrl = dbBbbInfo.ClaimListingUrl;
            bbbInfo.CloseBodyScript = dbBbbInfo.CloseBodyScript;
            bbbInfo.ConsumerLoginUrl = dbBbbInfo.ConsumerLoginUrl;
            bbbInfo.CreatedBy = dbBbbInfo.CreatedBy;
            bbbInfo.CreatedDate = dbBbbInfo.CreatedDate;
            bbbInfo.EmploymentOpportunitiesUrl = dbBbbInfo.EmploymentOpportunitiesUrl;
            bbbInfo.EventsPageLayout = dbBbbInfo.EventsPageLayout;
            bbbInfo.FileComplaintUrl = dbBbbInfo.FileComplaintUrl;
            bbbInfo.FooterLinks = dbBbbInfo.FooterLinks.DeserializeOrDefault(new List<BbbLink>());
            bbbInfo.GatewayEnabled = dbBbbInfo.GatewayEnabled;
            bbbInfo.RaqEnabled = dbBbbInfo.RaqEnabled;
            bbbInfo.GoogleAnalyticsId = dbBbbInfo.GoogleAnalyticsId;
            bbbInfo.HeaderFooterFlag = dbBbbInfo.HeaderFooterFlag;
            bbbInfo.HeaderLinks = dbBbbInfo.HeaderLinks.DeserializeOrDefault(new List<BbbLink>());
            bbbInfo.HeadScript = dbBbbInfo.HeadScript;
            bbbInfo.Id = dbBbbInfo.Id;
            bbbInfo.InformationPageImageUrl = dbBbbInfo.InformationPageImageUrl;
            bbbInfo.LeadsgenLinkEnabled = dbBbbInfo.LeadsgenLinkEnabled;
            bbbInfo.LegacyId = dbBbbInfo.LegacyId;
            bbbInfo.LegacySiteId = dbBbbInfo.LegacySiteId;
            bbbInfo.Locations = dbBbbInfo.Locations.DeserializeOrDefault(new List<BbbInfoLocation>());
            bbbInfo.MenuLinks = dbBbbInfo.MenuLinks.DeserializeOrDefault(new List<BbbLink>());
            bbbInfo.ModifiedBy = dbBbbInfo.ModifiedBy;
            bbbInfo.ModifiedDate = dbBbbInfo.ModifiedDate;
            bbbInfo.Name = dbBbbInfo.Name;
            bbbInfo.NewsLetterSignupUrl = dbBbbInfo.NewsLetterSignupUrl;
            bbbInfo.OpenBodyScript = dbBbbInfo.OpenBodyScript;
            bbbInfo.Persons = ParseBbbInfoPersons(dbBbbInfo.Persons);
            bbbInfo.PhoneLeadsEmails = StringHelpers.MapSerializedJsonListToCommaSeparatedString(dbBbbInfo.PhoneLeadsEmails);
            bbbInfo.PrimaryCountry = dbBbbInfo.PrimaryCountry;
            bbbInfo.PrimaryLanguage = dbBbbInfo.PrimaryLanguage;
            bbbInfo.Programs = dbBbbInfo.Programs.DeserializeOrDefault(new List<BbbInfoProgram>());
            bbbInfo.SocialMedia = dbBbbInfo.SocialMedia.DeserializeOrDefault(new List<BbbInfoSocialMedium>());
            bbbInfo.Subdomains = StringHelpers.MapSerializedJsonListToCommaSeparatedString(dbBbbInfo.Subdomains);
            bbbInfo.SubmitReviewUrl = dbBbbInfo.SubmitReviewUrl;
            bbbInfo.Tagline = dbBbbInfo.Tagline;
            bbbInfo.Vendor = dbBbbInfo.Vendor;
            bbbInfo.VendorHostHeader = dbBbbInfo.VendorHostHeader;
            bbbInfo.Languages = StringHelpers.MapSerializedJsonListToCommaSeparatedString(dbBbbInfo.Languages);
            bbbInfo.PopularCategories = await Task.WhenAll(dbBbbInfo.PopularCategoriesJson
                .DeserializeOrDefault(new List<string>())
                .Select(tobId => MapToBbbInfoCategoryAsync(tobId, dbBbbInfo.PrimaryCountry))
                .Where(x=> x != null)
                .ToList());
            bbbInfo.ComplaintQualifyingQuestions = dbBbbInfo.ComplaintQualifyingQuestionsJson.DeserializeOrDefault(new List<BbbInfoFileComplaintQuestion>());
            bbbInfo.PrimaryUrlAlias = dbBbbInfo.PrimaryUrlAlias;
            bbbInfo.ApplyForAccreditationInfo = dbBbbInfo.ApplyForAccreditationInfoJson.DeserializeOrDefault(new BbbInfoApplyForAccreditationInfo());
            bbbInfo.BaseUrl= dbBbbInfo.BaseUrl;
            bbbInfo.BusinessLeadEmailNotficationEnabled = dbBbbInfo.BusinessLeadEmailNotficationEnabled;
           
            return bbbInfo;
        }

        public DbBbbInfo MapToDbBbbInfo(BbbInfo bbbInfo)
        {
            var dbBbbInfo = new DbBbbInfo();
            dbBbbInfo.AboutUsText = bbbInfo.AboutUsText;
            dbBbbInfo.Active = bbbInfo.Active;
            dbBbbInfo.AdditionalResources = bbbInfo.AdditionalResources.SerializeObject();
            dbBbbInfo.AllowVendorUriAliases = bbbInfo.AllowVendorUriAliases;
            dbBbbInfo.AnnualReportsUrl = bbbInfo.AnnualReportsUrl;
            dbBbbInfo.ApplyForAccreditationUrl = bbbInfo.ApplyForAccreditationUrl;
            dbBbbInfo.JoinApplyUrl = bbbInfo.JoinApplyUrl;
            dbBbbInfo.AreaCodes = MapToAreaCodesJson(bbbInfo.AreaCodes);
            dbBbbInfo.BaseUriAliases = StringHelpers.MapCommaSeparatedToSerializedJsonList(bbbInfo.BaseUriAliases);
            dbBbbInfo.BbbUrlSegment = bbbInfo.BbbUrlSegment;
            dbBbbInfo.BusinessLoginUrl = bbbInfo.BusinessLoginUrl;
            dbBbbInfo.CharityProfileSource = bbbInfo.CharityProfileSource;
            dbBbbInfo.ClaimListingUrl = bbbInfo.ClaimListingUrl;
            dbBbbInfo.CloseBodyScript = bbbInfo.CloseBodyScript;
            dbBbbInfo.ConsumerLoginUrl = bbbInfo.ConsumerLoginUrl;
            dbBbbInfo.CreatedBy = bbbInfo.CreatedBy;
            dbBbbInfo.CreatedDate = bbbInfo.CreatedDate;
            dbBbbInfo.EmploymentOpportunitiesUrl = bbbInfo.EmploymentOpportunitiesUrl;
            dbBbbInfo.EventsPageLayout = bbbInfo.EventsPageLayout;
            dbBbbInfo.FileComplaintUrl = bbbInfo.FileComplaintUrl;
            dbBbbInfo.FooterLinks = bbbInfo.FooterLinks.SerializeObject();
            dbBbbInfo.GatewayEnabled = bbbInfo.GatewayEnabled;
            dbBbbInfo.RaqEnabled = bbbInfo.RaqEnabled;
            dbBbbInfo.GoogleAnalyticsId = bbbInfo.GoogleAnalyticsId;
            dbBbbInfo.HeaderFooterFlag = bbbInfo.HeaderFooterFlag;
            dbBbbInfo.HeaderLinks = bbbInfo.HeaderLinks.SerializeObject();
            dbBbbInfo.HeadScript = bbbInfo.HeadScript;
            dbBbbInfo.Id = bbbInfo.Id;
            dbBbbInfo.InformationPageImageUrl = bbbInfo.InformationPageImageUrl;
            dbBbbInfo.LeadsgenLinkEnabled = bbbInfo.LeadsgenLinkEnabled;
            dbBbbInfo.LegacyId = bbbInfo.LegacyId;
            dbBbbInfo.LegacySiteId = bbbInfo.LegacySiteId;
            dbBbbInfo.Locations = bbbInfo.Locations.SerializeObject();
            dbBbbInfo.MenuLinks = bbbInfo.MenuLinks.SerializeObject();
            dbBbbInfo.ModifiedBy = bbbInfo.ModifiedBy;
            dbBbbInfo.ModifiedDate = bbbInfo.ModifiedDate;
            dbBbbInfo.Name = bbbInfo.Name;
            dbBbbInfo.NewsLetterSignupUrl = bbbInfo.NewsLetterSignupUrl;
            dbBbbInfo.OpenBodyScript = bbbInfo.OpenBodyScript;
            dbBbbInfo.Persons = bbbInfo.Persons.SerializeObject();
            dbBbbInfo.PhoneLeadsEmails = StringHelpers.MapCommaSeparatedToSerializedJsonList(bbbInfo.PhoneLeadsEmails);
            dbBbbInfo.PrimaryCountry = bbbInfo.PrimaryCountry;
            dbBbbInfo.PrimaryLanguage = bbbInfo.PrimaryLanguage;
            dbBbbInfo.Programs = bbbInfo.Programs.SerializeObject();
            dbBbbInfo.SocialMedia = bbbInfo.SocialMedia.SerializeObject();
            dbBbbInfo.Subdomains = StringHelpers.MapCommaSeparatedToSerializedJsonList(bbbInfo.Subdomains);
            dbBbbInfo.SubmitReviewUrl = bbbInfo.SubmitReviewUrl;
            dbBbbInfo.Tagline = bbbInfo.Tagline;
            dbBbbInfo.Vendor = bbbInfo.Vendor;
            dbBbbInfo.VendorHostHeader = bbbInfo.VendorHostHeader;
            dbBbbInfo.Languages = StringHelpers.MapCommaSeparatedToSerializedJsonList(bbbInfo.Languages);
            dbBbbInfo.PopularCategoriesJson = bbbInfo.PopularCategories.Where(x => x != null).Select(x => x.TobId).SerializeObject();
            dbBbbInfo.ComplaintQualifyingQuestionsJson = bbbInfo.ComplaintQualifyingQuestions.SerializeObject();
            dbBbbInfo.ApplyForAccreditationInfoJson = bbbInfo.ApplyForAccreditationInfo.SerializeObject();
            dbBbbInfo.PrimaryUrlAlias = bbbInfo.PrimaryUrlAlias;
            dbBbbInfo.BaseUrl = bbbInfo.BaseUrl;
            dbBbbInfo.BusinessLeadEmailNotficationEnabled = bbbInfo.BusinessLeadEmailNotficationEnabled;
            return dbBbbInfo;
        }

        public BbbInfoDocument MapToSolrDoc(BbbInfo bbbInfo)
        {
            var bbbInfoSolrDoc = new BbbInfoDocument();
            bbbInfoSolrDoc.Id = bbbInfo.Id;
            bbbInfoSolrDoc.AboutUsText = bbbInfo.AboutUsText;
            bbbInfoSolrDoc.AnnualReportsUrl = bbbInfo.AnnualReportsUrl;
            bbbInfoSolrDoc.ApplyForAccreditationUrl = bbbInfo.ApplyForAccreditationUrl;
            bbbInfoSolrDoc.JoinApplyUrl = bbbInfo.JoinApplyUrl;
            bbbInfoSolrDoc.AreaCodes = StringHelpers.MapCommaSeparatedStringToList(bbbInfo.AreaCodes);
            bbbInfoSolrDoc.BaseUriAliases = StringHelpers.MapCommaSeparatedStringToList(bbbInfo.BaseUriAliases);
            bbbInfoSolrDoc.BusinessLoginUrl = bbbInfo.BusinessLoginUrl;
            bbbInfoSolrDoc.CharityProfileSource = bbbInfo.CharityProfileSource;
            bbbInfoSolrDoc.ClaimListingUrl = bbbInfo.ClaimListingUrl;
            bbbInfoSolrDoc.CloseBodyScript = bbbInfo.CloseBodyScript;
            bbbInfoSolrDoc.ConsumerLoginUrl = bbbInfo.ConsumerLoginUrl;
            bbbInfoSolrDoc.CreatedBy = bbbInfo.CreatedBy;
            bbbInfoSolrDoc.CreatedDate = bbbInfo.CreatedDate.DateTime;
            bbbInfoSolrDoc.EmploymentOpportunitiesUrl = bbbInfo.EmploymentOpportunitiesUrl;
            bbbInfoSolrDoc.BbbUrlSegment = bbbInfo.BbbUrlSegment;
            bbbInfoSolrDoc.FileComplaintUrl = bbbInfo.FileComplaintUrl;
            bbbInfoSolrDoc.GatewayEnabled = bbbInfo.GatewayEnabled;
            bbbInfoSolrDoc.RaqEnabled = bbbInfo.RaqEnabled;
            bbbInfoSolrDoc.GoogleAnalyticsId = bbbInfo.GoogleAnalyticsId;
            bbbInfoSolrDoc.HeadScript = bbbInfo.HeadScript;
            bbbInfoSolrDoc.InformationPageImageUrl = bbbInfo.InformationPageImageUrl;
            bbbInfoSolrDoc.LeadsgenLinkEnabled = bbbInfo.LeadsgenLinkEnabled;
            bbbInfoSolrDoc.LegacyId = bbbInfo.LegacyId;
            bbbInfoSolrDoc.LegacySiteId = bbbInfo.LegacySiteId;
            bbbInfoSolrDoc.ModifiedBy = bbbInfo.ModifiedBy;
            bbbInfoSolrDoc.ModifiedDate = bbbInfo.ModifiedDate.DateTime;
            bbbInfoSolrDoc.Name = bbbInfo.Name;
            bbbInfoSolrDoc.NewsLetterSignupUrl = bbbInfo.NewsLetterSignupUrl;
            bbbInfoSolrDoc.OpenBodyScript = bbbInfo.OpenBodyScript;
            bbbInfoSolrDoc.PhoneLeadsEmails = StringHelpers.MapCommaSeparatedStringToList(bbbInfo.PhoneLeadsEmails);
            bbbInfoSolrDoc.PrimaryCountry = bbbInfo.PrimaryCountry;
            bbbInfoSolrDoc.PrimaryLanguage = bbbInfo.PrimaryLanguage;
            bbbInfoSolrDoc.Subdomains = StringHelpers.MapCommaSeparatedStringToList(bbbInfo.Subdomains);
            bbbInfoSolrDoc.SubmitReviewUrl = bbbInfo.SubmitReviewUrl;
            bbbInfoSolrDoc.Tagline = bbbInfo.Tagline;
            bbbInfoSolrDoc.Vendor = bbbInfo.Vendor;
            bbbInfoSolrDoc.AllowVendorUriAliases = bbbInfo.AllowVendorUriAliases;
            bbbInfoSolrDoc.Programs = bbbInfo.Programs.SerializeObject();
            bbbInfoSolrDoc.Persons = bbbInfo.Persons.SerializeObject();
            bbbInfoSolrDoc.Locations = bbbInfo.Locations.SerializeObject();
            bbbInfoSolrDoc.SocialMedia = bbbInfo.SocialMedia.SerializeObject();
            bbbInfoSolrDoc.VendorHostHeader = bbbInfo.VendorHostHeader;
            bbbInfoSolrDoc.HeaderFooterFlag = bbbInfo.HeaderFooterFlag;
            bbbInfoSolrDoc.AdditionalResources = bbbInfo.AdditionalResources.SerializeObject();
            bbbInfoSolrDoc.EventsPageLayout = bbbInfo.EventsPageLayout;
            bbbInfoSolrDoc.HeaderLinks = bbbInfo.HeaderLinks.SerializeObject();
            bbbInfoSolrDoc.FooterLinks = bbbInfo.FooterLinks.SerializeObject();
            bbbInfoSolrDoc.MenuLinks = bbbInfo.MenuLinks.SerializeObject();
            bbbInfoSolrDoc.Languages = StringHelpers.MapCommaSeparatedStringToList(bbbInfo.Languages);
            bbbInfoSolrDoc.PopularCategoriesJson = bbbInfo.PopularCategories.Select(x => x.TobId).SerializeObject();
            bbbInfoSolrDoc.ComplaintQualifyingQuestionsJson = bbbInfo.ComplaintQualifyingQuestions.SerializeObject();
            bbbInfoSolrDoc.PrimaryUrlAlias = bbbInfo.PrimaryUrlAlias;
            bbbInfoSolrDoc.ApplyForAccreditationInfoJson = bbbInfo.ApplyForAccreditationInfo.SerializeObject();
            bbbInfoSolrDoc.BaseUrl = bbbInfo.BaseUrl;
            bbbInfoSolrDoc.BusinessLeadEmailNotficationEnabled = bbbInfo.BusinessLeadEmailNotficationEnabled;
            return bbbInfoSolrDoc;
        }

        public BbbServiceArea MapToBbbServiceArea(BbbInfoDocument bbbInfoSolrDoc)
        {
            var locations = bbbInfoSolrDoc.Locations.DeserializeOrDefault(new List<BbbInfoLocation>());
            var primaryLocation = locations.FirstOrDefault(x => string.Equals(x.Type?.ToLower(), "primary"));

            if (primaryLocation == null || string.IsNullOrWhiteSpace(bbbInfoSolrDoc.LegacyId))
            {
                return null;
            }

            var serviceArea = new BbbServiceArea();
            serviceArea.BbbId = bbbInfoSolrDoc.LegacyId;
            serviceArea.Countries = new List<string> {
                primaryLocation.Country
            };
            serviceArea.States = primaryLocation.Jurisdiction;
            serviceArea.PrimaryCountry = bbbInfoSolrDoc.PrimaryCountry;
            return serviceArea;
        }

        #region Helper methods
        /// <summary>
        /// Parses the JSON in Persons property accounting for changes
        /// in its schema since its initial version 
        /// </summary>
        private IList<BbbInfoPersonGroup> ParseBbbInfoPersons(string json)
        {
            if (string.IsNullOrWhiteSpace(json))
            {
                return new List<BbbInfoPersonGroup>();
            }
            var items = JArray.Parse(json);
            if (items.Count == 0)
            {
                return new List<BbbInfoPersonGroup>();
            }

            var sample = items.First;
            var sampleIsPerson = sample["FirstName"] != null;

            // JSON is an array of persons
            if (sampleIsPerson)
            {
                var staffGroup = new BbbInfoPersonGroup
                {
                    Name = BbbPersonGroupDefaults.Name,
                    Persons = items.ToObject<List<BbbInfoPerson>>()
                };

                return new List<BbbInfoPersonGroup> { staffGroup };
            }

            // JSON is an array of groups
            return items.ToObject<List<BbbInfoPersonGroup>>();
        }

        private string MapToAreaCodesJson(string commaSeparatedAreaCodes)
        {
            var areaCodes = StringHelpers.MapCommaSeparatedStringToList(commaSeparatedAreaCodes)
                .Select(CleanAreaCode)
                .ToList();
            return JsonConvert.SerializeObject(areaCodes);
        }

        private string CleanAreaCode(string areaCode)
        {
            var cleanAreaCode = new string(areaCode.Where(char.IsDigit).ToArray());
            Guard.Against.InvalidAreaCode(cleanAreaCode);
            return cleanAreaCode;
        }

        private async Task<BbbInfoCategory> MapToBbbInfoCategoryAsync(string tobId, string primaryCountry)
        {
            var category = await _tobCache.GetByIdAsync(tobId, primaryCountry);
            if (category != null) {
                var bbbInfoCategory = new BbbInfoCategory ();
                bbbInfoCategory.Name = category.Title;
                bbbInfoCategory.TobId = tobId;
                return bbbInfoCategory;
            }
            return null;
        }
        #endregion
    }
}
