﻿using Cms.Domain.Cache;
using Cms.Domain.Extensions;
using Cms.Model.Constants;
using Cms.Model.Models.BusinessProfile;
using Cms.Model.Models.SecTerm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cms.Domain.Mappers
{
    public interface IMetadataMappers
    {
        IList<Metadata> MapToMetadataList(string metadataJson);
        IList<Metadata> MapToMetadataBusinessList(string metadataJson);
        Task<Metadata> MapToMetadataBusinessAsync(string metadataJson, BusinessProfile profile);
    }
    
    public class MetadataMappers : IMetadataMappers
    {
        private readonly IBbbCache _bbbCache;
        public MetadataMappers(IBbbCache bbbCache)
        {
            _bbbCache = bbbCache;
        }

        public IList<Metadata> MapToMetadataList(string metadataJson)
        {
            var metadataList = metadataJson.DeserializeOrDefault(MetadataConstants.DefaultMetadata);

            return new List<Metadata>() {
                metadataList.FirstOrDefault(x => x.Type == MetadataConstants.AccreditedCountryKey && x.CultureIds.SequenceEqual(CultureConstants.SupportedEnCultures))
                    ?? MetadataConstants.AccreditedCountryMetadataEn,
                metadataList.FirstOrDefault(x => x.Type == MetadataConstants.AccreditedCountryKey && x.CultureIds.SequenceEqual(CultureConstants.SupportedEsCultures))
                    ?? MetadataConstants.AccreditedCountryMetadataEs,
                metadataList.FirstOrDefault(x => x.Type == MetadataConstants.CountryKey && x.CultureIds.SequenceEqual(CultureConstants.SupportedEnCultures))
                    ?? MetadataConstants.CountryMetadataEn,
                metadataList.FirstOrDefault(x => x.Type == MetadataConstants.CountryKey && x.CultureIds.SequenceEqual(CultureConstants.SupportedEsCultures))
                    ?? MetadataConstants.CountryMetadataEs,
                metadataList.FirstOrDefault(x => x.Type == MetadataConstants.AccreditedStateKey && x.CultureIds.SequenceEqual(CultureConstants.SupportedEnCultures))
                    ?? MetadataConstants.AccreditedStateMetadataEn,
                metadataList.FirstOrDefault(x => x.Type == MetadataConstants.AccreditedStateKey && x.CultureIds.SequenceEqual(CultureConstants.SupportedEsCultures))
                    ?? MetadataConstants.AccreditedStateMetadataEs,
                metadataList.FirstOrDefault(x => x.Type == MetadataConstants.StateKey && x.CultureIds.SequenceEqual(CultureConstants.SupportedEnCultures))
                    ?? MetadataConstants.StateMetadataEn,
                metadataList.FirstOrDefault(x => x.Type == MetadataConstants.StateKey && x.CultureIds.SequenceEqual(CultureConstants.SupportedEsCultures))
                    ?? MetadataConstants.StateMetadataEs,
                metadataList.FirstOrDefault(x => x.Type == MetadataConstants.AccreditedCityKey && x.CultureIds.SequenceEqual(CultureConstants.SupportedEnCultures))
                    ?? MetadataConstants.AccreditedCityMetadataEn,
                metadataList.FirstOrDefault(x => x.Type == MetadataConstants.AccreditedCityKey && x.CultureIds.SequenceEqual(CultureConstants.SupportedEsCultures))
                    ?? MetadataConstants.AccreditedCityMetadataEs,
                metadataList.FirstOrDefault(x => x.Type == MetadataConstants.CityKey && x.CultureIds.SequenceEqual(CultureConstants.SupportedEnCultures))
                    ?? MetadataConstants.CityMetadataEn,
                metadataList.FirstOrDefault(x => x.Type == MetadataConstants.CityKey && x.CultureIds.SequenceEqual(CultureConstants.SupportedEsCultures))
                    ?? MetadataConstants.CityMetadataEs,
            };
        }

        public IList<Metadata> MapToMetadataBusinessList(string metadataBusinessJson)
        {
            var savedMetadataBusinessList = metadataBusinessJson.DeserializeOrDefault(MetadataBusinessConstants.DefaultBusinessMetadata);

            var savedOrDefaultAccreditedEn = savedMetadataBusinessList.FirstOrDefault(x =>
                    x.Type == MetadataBusinessConstants.MetadataAccreditedBusinessKey
                    && x.CultureIds.SequenceEqual(CultureConstants.SupportedEnCultures))
                ?? MetadataBusinessConstants.AccreditedBusinessMetadataEn;
            var savedOrDefaultAccreditedEs = savedMetadataBusinessList.FirstOrDefault(x =>
                    x.Type == MetadataBusinessConstants.MetadataAccreditedBusinessKey
                    && x.CultureIds.SequenceEqual(CultureConstants.SupportedEsCultures))
                ?? MetadataBusinessConstants.AccreditedBusinessMetadataEs;
            var savedOrDefaultNonAccreditedEn = savedMetadataBusinessList.FirstOrDefault(x =>
                    x.Type == MetadataBusinessConstants.MetadataBusinessKey
                    && x.CultureIds.SequenceEqual(CultureConstants.SupportedEnCultures))
                ?? MetadataBusinessConstants.BusinessMetadataEn;
            var savedOrDefaultNonAccreditedEs = savedMetadataBusinessList.FirstOrDefault(x =>
                    x.Type == MetadataBusinessConstants.MetadataBusinessKey
                    && x.CultureIds.SequenceEqual(CultureConstants.SupportedEsCultures))
                ?? MetadataBusinessConstants.BusinessMetadataEs;
            var savedOrDefaultAccreditedRaqEn = savedMetadataBusinessList.FirstOrDefault(x =>
                    x.Type == MetadataBusinessConstants.MetadataRaqAccreditedBusinessKey
                    && x.CultureIds.SequenceEqual(CultureConstants.SupportedEnCultures))
                ?? MetadataBusinessConstants.RaqAccreditedBusinessMetadataEn;
            var savedOrDefaultAccreditedRaqEs = savedMetadataBusinessList.FirstOrDefault(x =>
                    x.Type == MetadataBusinessConstants.MetadataRaqAccreditedBusinessKey
                    && x.CultureIds.SequenceEqual(CultureConstants.SupportedEsCultures))
                ?? MetadataBusinessConstants.RaqAccreditedBusinessMetadataEs;


            IEnumerable<Metadata> metadataBusinessList = new List<Metadata>();
            metadataBusinessList = metadataBusinessList.Append(savedOrDefaultNonAccreditedEn);
            metadataBusinessList = metadataBusinessList.Append(savedOrDefaultNonAccreditedEs);
            metadataBusinessList = metadataBusinessList.Append(savedOrDefaultAccreditedEn);
            metadataBusinessList = metadataBusinessList.Append(savedOrDefaultAccreditedEs);
            metadataBusinessList = metadataBusinessList.Append(savedOrDefaultAccreditedRaqEn);
            metadataBusinessList = metadataBusinessList.Append(savedOrDefaultAccreditedRaqEs);
            return metadataBusinessList.ToList();
        }

        public async Task<Metadata> MapToMetadataBusinessAsync(string metadataBusinessJson, BusinessProfile profile)
        {
            var profileCulture = await _bbbCache.GetCultureInfoByBbbIdAsync(profile.BbbId);
            var isEnglish = profileCulture == CultureConstants.EnUsCultureId || profileCulture == CultureConstants.EnCaCultureId;

            var defaultMetadata = MetadataBusinessConstants.BusinessMetadataEn;

            if (!profile.IsAccredited && isEnglish)
            {
                defaultMetadata = MetadataBusinessConstants.BusinessMetadataEn;
            }
            else if (!profile.IsAccredited && !isEnglish)
            {
                defaultMetadata = MetadataBusinessConstants.BusinessMetadataEs;
            }
            else if (profile.IsAccredited && !profile.IsRaqActive && isEnglish)
            {
                defaultMetadata = MetadataBusinessConstants.AccreditedBusinessMetadataEn;
            }
            else if (profile.IsAccredited && !profile.IsRaqActive && !isEnglish)
            {
                defaultMetadata = MetadataBusinessConstants.AccreditedBusinessMetadataEs;
            }
            else if (profile.IsAccredited && profile.IsRaqActive && isEnglish)
            {
                defaultMetadata = MetadataBusinessConstants.RaqAccreditedBusinessMetadataEn;
            }
            else if (profile.IsAccredited && profile.IsRaqActive && !isEnglish)
            {
                defaultMetadata = MetadataBusinessConstants.RaqAccreditedBusinessMetadataEs;
            }

            return metadataBusinessJson.DeserializeOrDefault(defaultMetadata);
        }
    }
}
