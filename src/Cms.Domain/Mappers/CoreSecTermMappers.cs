﻿using Cms.Domain.Extensions;
using Cms.Model.Models.SecTerm;
using Cms.Model.Models.SecTerm.Db;
using System.Collections.Generic;
using System.Linq;

namespace Cms.Domain.Mappers
{
    public interface ICoreSecTermMappers
    {
        SecTerm MapToSecTerm(DbSecTerm coreSecTerm);
        SecTermResults MapToSecTermResults(IEnumerable<DbSecTerm> coreSecTerms);
    }

    public class CoreSecTermMappers : ICoreSecTermMappers
    {
        private IMetadataMappers _metadataMappers;
        public CoreSecTermMappers(IMetadataMappers metadataMappers)
        {
            _metadataMappers = metadataMappers;
        }

        public SecTerm MapToSecTerm(DbSecTerm coreSecTerm)
        {
            var secTerm = new SecTerm();
            secTerm.CategoryId = coreSecTerm.CategoryId;
            secTerm.CategoryCode = coreSecTerm.CategoryCode;
            secTerm.TobId = coreSecTerm.TobId;
            secTerm.Name = coreSecTerm.Name;
            secTerm.IsActive = coreSecTerm.IsActive;
            secTerm.IsEditable = coreSecTerm.IsEditable;
            secTerm.Metadata = _metadataMappers.MapToMetadataList(coreSecTerm.MetadataJson);
            secTerm.MetadataBusiness = _metadataMappers.MapToMetadataBusinessList(coreSecTerm.MetadataBusinessJson);
            secTerm.Aliases = coreSecTerm.AliasesJson.DeserializeOrDefault(new List<TobAlias>());
            secTerm.ModifiedDate = coreSecTerm.ModifiedDate;
            secTerm.ModifiedUser = coreSecTerm.ModifiedUser;
            secTerm.UrlSegment = coreSecTerm.UrlSegment;
            secTerm.SpanishName = coreSecTerm.SpanishName;
            secTerm.SpanishUrlSegment = coreSecTerm.SpanishUrlSegment;
            return secTerm;
        }

        public SecTermResults MapToSecTermResults(IEnumerable<DbSecTerm> coreSecTerms)
        {
            var secTerms = new SecTermResults();
            secTerms.TotalCount = coreSecTerms?.FirstOrDefault()?.TotalCount ?? 0;
            secTerms.Results = coreSecTerms.Select(MapToSecTerm);
            return secTerms;
        }
    }
}
