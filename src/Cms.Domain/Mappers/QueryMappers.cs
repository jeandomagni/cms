﻿using Cms.Model.Models.Query.DataTable;
using Cms.Model.Models.Query.KendoGrid;
using System.Collections.Generic;
using System.Linq;

namespace Cms.Domain.Mappers
{
    public static class QueryMappers
    {
        public static GridOptions MapToGridOptions(Query query)
        {
            var options = new GridOptions();
            options.data = MapToData(query);
            return options;
        }

        private static Data MapToData(Query query) {
            var data = new Data();
            data.CheckedOutBy = query.CheckedOutBy;
            data.Domain = query.DomainOnly;
            data.includeExpired = query.IncludeExpired;
            data.filter = query.Filters != null
                ? MapToFilter(query.Filters)
                : null;
            data.locations = query.Locations;
            data.take = query.Limit;
            data.skip = query.Limit * (query.Page - 1);
            data.page = query.Page;
            data.pageSize = query.Limit;
            data.sort = MapToSort(query.OrderBy);
            return data;
        }

        private static KendoFilter MapToFilter(IList<Filter> filters)
        {
            var filter = new KendoFilter();
            filter.filters = filters
                .Select(MapToLogic)
                .ToList();
            filter.logic = "AND";
            return filter;
        }

        private static Logic MapToLogic(Filter filter)
        {
            var logic = new Logic();
            logic.field = filter.Column;
            logic.@operator = filter.Type;
            logic.value = filter.Keyword;
            return logic;
        }

        private static List<Sort> MapToSort(string orderBy)
        {
            if (string.IsNullOrWhiteSpace(orderBy))
            {
                return null;
            }

            var sort = new Sort();
            sort.field = orderBy.Replace("-", "");
            sort.dir = orderBy.Contains("-") ? "desc" : "asc";
            return new List<Sort> { sort };
        }
    }
}
