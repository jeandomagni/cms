﻿using Cms.Domain.Enum;
using Cms.Domain.Extensions;
using Cms.Model.Enum;
using Cms.Model.Models.BbbInfo.Db;
using Cms.Model.Models.General;
using log4net;
using System.Collections.Generic;
using System.Linq;

namespace Cms.Domain.Mappers
{
    public interface IBbbAuditLogMappers
    {
        IList<DbAuditLog> MapToDbAuditLogs(DbBbbInfo existingBbbInfo, DbBbbInfo updatedBbbInfo, string userId);
        AuditLog MapToAuditLog(DbAuditLog dbLog);
    }

    public class BbbAuditLogMappers : IBbbAuditLogMappers
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public IList<DbAuditLog> MapToDbAuditLogs(DbBbbInfo existingBbbInfo, DbBbbInfo updatedBbbInfo, string userId)
        {
            return existingBbbInfo.DetailedCompare(updatedBbbInfo)
                 .Where(x => !string.IsNullOrWhiteSpace(x.Action))
                 .Select(x => MapToDbAuditLog(x.Action, existingBbbInfo.Id, userId))
                 .ToList();
        }

        public AuditLog MapToAuditLog(DbAuditLog dbLog)
        {
            var log = new AuditLog();
            log.CreatedDate = dbLog.CreatedDate;
            log.Summary = FormatActivitySummary(dbLog.UserId, dbLog.ActionCode);
            return log;
        }

        private string FormatActivitySummary(string userEmail, string actionCode) =>
            $"{userEmail} updated {GetDescription(actionCode)} for this BBB";

        private string GetDescription(string actionCode)
        {
            switch (actionCode)
            {
                case EntityActions.ADDITIONAL_RESOURCES:
                    return "the additional resources";
                case EntityActions.UPDATE_VENDOR_HOST_HEADER:
                    return "the vendor host header";
                case EntityActions.UPDATE_VENDOR:
                    return "the vendor";
                case EntityActions.UPDATE_TAGLINE:
                    return "the tagline";
                case EntityActions.UPDATE_SUBMIT_REVIEW_URL:
                    return "the submit review URL";
                case EntityActions.UPDATE_SUBDOMAINS:
                    return "the subdomains";
                case EntityActions.UPDATE_SOCIAL_MEDIA:
                    return "the social media";
                case EntityActions.UPDATE_PROGRAMS:
                    return "the programs";
                case EntityActions.UPDATE_PRIMARY_COUNTRY:
                    return "the primary country";
                case EntityActions.UPDATE_PHONE_LEADS_EMAILS:
                    return "the phone leads emails";
                case EntityActions.UPDATE_PERSONS:
                    return "the contact persons";
                case EntityActions.UPDATE_OPEN_BODY_SCRIPT:
                    return "the open body script";
                case EntityActions.UPDATE_NEWS_LETTER_SIGNUP_URL:
                    return "the newsletter signup URL";
                case EntityActions.UPDATE_LEADSGEN_LINK_ENABLED:
                    return "the leadsgen link enabled";
                case EntityActions.UPDATE_LOCATIONS:
                    return "the locations";
                case EntityActions.UPDATE_INFORMATION_PAGE_IMAGE_URL:
                    return "the information page image URL";
                case EntityActions.UPDATE_HEAD_SCRIPT:
                    return "the head script";
                case EntityActions.UPDATE_HEADER_LINKS:
                    return "the header links";
                case EntityActions.UPDATE_HEADER_FOOTER_FLAG:
                    return "the header footer flag";
                case EntityActions.UPDATE_GOOGLE_ANALYTICS_ID:
                    return "the google analytics ID";
                case EntityActions.UPDATE_GATEWAY_ENABLED_FLAG:
                    return "the gateway enabled flag";
                case EntityActions.UPDATE_RAQ_ENABLED_FLAG:
                    return "the RAQ enabled flag";
                case EntityActions.UPDATE_FOOTER_LINKS:
                    return "the footer link";
                case EntityActions.UPDATE_FILE_COMPLAINT_URL:
                    return "the file complaint url";
                case EntityActions.UPDATE_EVENTS_PAGE_LAYOUT:
                    return "the events page layout";
                case EntityActions.UPDATE_EMPLOYMENT_OPPORTUNITIES_URL:
                    return "the employment opportunities URL";
                case EntityActions.UPDATE_CONSUMER_LOGIN_URL:
                    return "the consumer login URL";
                case EntityActions.UPDATE_CLOSE_BODY_SCRIPT:
                    return "the close body script";
                case EntityActions.UPDATE_CLAIMS_LISTING_URL:
                    return "the claims listing URL";
                case EntityActions.UPDATE_CHARITY_PROFILE_SOURCE:
                    return "the charity profile source";
                case EntityActions.UPDATE_BUSINESS_LOGIN_URL:
                    return "the business login URL";
                case EntityActions.UPDATE_BBB_URL_SEGMENT:
                    return "the bbb URL segment";
                case EntityActions.UPDATE_BASE_URI_ALIASES:
                    return "the base URI aliases";
                case EntityActions.UPDATE_AREA_CODES:
                    return "the area codes";
                case EntityActions.UPDATE_ACCREDITATION_APPLY_URL:
                    return "the accreditation application URL";
                case EntityActions.UPDATE_ANNUAL_REPORTS_URL:
                    return "the annual reports URL";
                case EntityActions.UPDATE_VENDOR_ALIASES:
                    return "the vendor aliases";
                case EntityActions.UPDATE_ADDITIONAL_RESOURCES:
                    return "the additional resources";
                case EntityActions.UPDATE_ABOUT_US_TEXT:
                    return "the about us text";
                case EntityActions.UPDATE_LANGUAGES:
                    return "the supported languages";
                case EntityActions.UPDATE_COMPLAINT_QUALIFYING_QUESTIONS:
                    return "the file a complaint qualifying questions";
                case EntityActions.UPDATE_APPLY_FOR_ACCREDITATION_INFO:
                    return "the apply for accreditation application";
                default:
                    _log.Warn($"{actionCode} missing from BbbAuditLogMappers.GetDescription()");
                    return actionCode;
            }
        }

        private DbAuditLog MapToDbAuditLog(string action, int bbbId, string userId)
        {
            var log = new DbAuditLog();
            log.ActionCode = action;
            log.EntityCode = EntityCodes.BBB;
            log.EntityId = bbbId;
            log.UserId = userId;
            return log;
        }
    }
}
