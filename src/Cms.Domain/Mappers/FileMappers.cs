﻿using Cms.Domain.Enum;
using Cms.Domain.Model;
using Cms.Model.Models;
using Cms.Model.Models.Content.Db;
using System;
using System.Linq;

namespace Cms.Domain.Mappers
{
    public static class FileMappers
    {
        public static DbContent MapToDbContent(File file, AppUser currentUser)
        {
            var dbContent = new DbContent();
            dbContent.Title = file.Title;
            dbContent.Code = file.Code;
            dbContent.Type = ContentType.Image;
            dbContent.Canonical = file.Uri;
            dbContent.Tags = file.TagItems != null ? string.Join(",", file.TagItems.Select(x => x.Value)) : null;
            dbContent.Status = ContentStatus.Published;
            dbContent.CreatedBy = currentUser.Email;
            dbContent.CreateDate = DateTime.UtcNow;
            dbContent.PublishDate = DateTime.UtcNow;
            return dbContent;
        }
    }
}
