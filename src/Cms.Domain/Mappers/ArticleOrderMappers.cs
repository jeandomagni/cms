﻿using Cms.Domain.Enum;
using Cms.Model.Models;
using Cms.Model.Models.Content;
using Cms.Model.Models.Content.Entities;
using Cms.Model.Models.Query;
using SolrArticleOrder = Bbb.Core.Solr.Model.ArticleOrder;

namespace Cms.Domain.Mappers
{
    public interface IArticleOrderMappers
    {
        SolrArticleOrder MapToSolrArticleOrder(ArticleOrder articleOrder);
        ArticleOrderParams MapToArticleOrderParams(ArticleOrder articleOrder);
        ArticleOrder MapToArticleOrder(ArticleOrderParams articleOrderParams);
        ArticleOrder MapToArticleOrder(Location location);
        ArticleOrder MapToArticleOrder(Geotag location);
        Geotag MapToGeotag(ArticleOrder articleOrder);
    }
    public class ArticleOrderMappers : IArticleOrderMappers
    {
        public ArticleOrder MapToArticleOrder(ArticleOrderParams articleOrderParams)
        {
            return new ArticleOrder
            {
                City = articleOrderParams.City,
                Country = articleOrderParams.Country,
                ListingType = articleOrderParams.ListingType,
                ServiceArea = articleOrderParams.ServiceArea,
                State = articleOrderParams.State,
                Topic = articleOrderParams.Topic
            };
        }

        public ArticleOrder MapToArticleOrder(Location location)
        {
            return new ArticleOrder()
            {
                Country = location.CountryCode,
                City = location.City,
                State = location.State
            };
        }

        public ArticleOrder MapToArticleOrder(Geotag location)
        {
            if (location.LocationType == LocationTypes.Country)
            {
                return new ArticleOrder
                {
                    Country = location.LocationId
                };
            }
            if (location.LocationType == LocationTypes.State)
            {
                return new ArticleOrder
                {
                    State = location.LocationId
                };
            }
            if (location.LocationType == LocationTypes.BbbId)
            {
                return new ArticleOrder
                {
                    ServiceArea = location.LocationId
                };
            }
            if (location.LocationType == LocationTypes.City)
            {
                var array = location.LocationId.Split(" ");
                if (array.Length < 3)
                {
                    throw new System.Exception($"Wrong location id for city passed: {location.LocationId}");
                }

                var state = array[array.Length - 2];
                var country = array[array.Length - 1];
                var city = location.LocationId.Replace(state, "").Replace(country, "").Trim();

                return new ArticleOrder
                {
                    City = city,
                    State = state,
                    Country = country
                };
            }

            throw new System.Exception("Wrong location type passed");
        }

        public ArticleOrderParams MapToArticleOrderParams(ArticleOrder articleOrder)
        {
            return new ArticleOrderParams
            {
                City = articleOrder.City,
                Country = articleOrder.Country,
                ListingType = articleOrder.ListingType,
                ServiceArea = articleOrder.ServiceArea,
                State = articleOrder.State,
                Topic = articleOrder.Topic,
            };
        }

        public Geotag MapToGeotag(ArticleOrder articleOrder)
        {
            Geotag geoTag = new Geotag();
            if (articleOrder.IsCityLevel())
            {
                geoTag.LocationId = $"{articleOrder.City} {articleOrder.State} {articleOrder.Country}";
                geoTag.LocationType = LocationTypes.City;
            }
            else if (articleOrder.IsStateLevel())
            {
                geoTag.LocationId = articleOrder.State;
                geoTag.LocationType = LocationTypes.State;
            }
            else if (articleOrder.IsBbbLevel())
            {
                geoTag.LocationId = articleOrder.ServiceArea;
                geoTag.LocationType = LocationTypes.BbbId;
            }
            else if (articleOrder.IsCountryLevel())
            {
                geoTag.LocationId = articleOrder.Country;
                geoTag.LocationType = LocationTypes.Country;
            }

            return geoTag;
        }

        public SolrArticleOrder MapToSolrArticleOrder(ArticleOrder articleOrder)
        {
            return new SolrArticleOrder
            {
                ArticleOrderId = articleOrder.Id.ToString(),
                City = articleOrder.City,
                Country = articleOrder.Country,
                ListingType = articleOrder.ListingType,
                OrderedArticleIds = articleOrder.OrderedArticleIdsAsArray,
                BbbId = articleOrder.ServiceArea,
                State = articleOrder.State,
                Topic = articleOrder.Topic
            };
        }
    }
}
