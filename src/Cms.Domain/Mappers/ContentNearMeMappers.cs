﻿using Cms.Model.Models.Content;
using Cms.Model.Models.Content.Db;

namespace Cms.Domain.Mappers
{
    public interface IContentNearMeMappers
    {
        ContentNearMe MapToContentNearMe(DbContentNearMe dbContentNearMe);
        DbContentNearMe MapToDbContentNearMe(ContentNearMe contentNearMe);
    }

    public class ContentNearMeMappers : IContentNearMeMappers
    {
        public ContentNearMe MapToContentNearMe(DbContentNearMe dbContentNearMe)
        {
            var contentNearMe = new ContentNearMe();
            contentNearMe.AboveListContent = dbContentNearMe.AboveListContent;
            contentNearMe.ContentCode = dbContentNearMe.ContentCode;
            contentNearMe.Id = dbContentNearMe.Id;
            contentNearMe.RelatedInformationHeadline = dbContentNearMe.RelatedInformationHeadline;
            contentNearMe.RelatedInformationText = dbContentNearMe.RelatedInformationText;
            return contentNearMe;
        }

        public DbContentNearMe MapToDbContentNearMe(ContentNearMe contentNearMe)
        {
            var dbContentNearMe = new DbContentNearMe();
            dbContentNearMe.AboveListContent = contentNearMe.AboveListContent;
            dbContentNearMe.ContentCode = contentNearMe.ContentCode;
            dbContentNearMe.Id = contentNearMe.Id;
            dbContentNearMe.RelatedInformationHeadline = contentNearMe.RelatedInformationHeadline;
            dbContentNearMe.RelatedInformationText = contentNearMe.RelatedInformationText;
            return dbContentNearMe;
        }
    }
}
