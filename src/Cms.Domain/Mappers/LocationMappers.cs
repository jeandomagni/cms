﻿using Bbb.Core.Solr.Enums;
using Bbb.Core.Solr.Enums;
using Cms.Model.Models;
using SolrLocation = Bbb.Core.Solr.Model.Location;

namespace Cms.Domain.Mappers
{
    public static class LocationMappers
    {
        public static Location MapToLocation(SolrLocation solrLocationDocument)
        {
            var location = new Location();
            location.Id = solrLocationDocument.Id;
            location.BbbIds = solrLocationDocument.BbbIds;
            location.City = solrLocationDocument.City;
            location.CitySeo = solrLocationDocument.CitySeo;
            location.CountryCode = System.Enum.IsDefined(typeof(CountryCode), solrLocationDocument.CountryCode)
                ? System.Enum.Parse(typeof(CountryCode), solrLocationDocument.CountryCode.ToString()).ToString()
                : "USA";
            location.State = solrLocationDocument.State;
            location.StateFull = solrLocationDocument.StateFull;
            return location;
        }
    }
}
