﻿using Cms.Domain.Enum;
using Cms.Domain.Extensions;
using Cms.Model.Models.Content;
using Cms.Model.Models.Content.Db;
using System;
using System.Linq;

namespace Cms.Domain.Mappers
{
    public static class ContentQuoteMappers
    {
        public static DbContent MapToDbContent(ContentQuote quote)
        {
            var dbContent = new DbContent();

            if (quote.TagItems.Where(x => string.Equals(x.Type, "city"))?.ToList()?.Count > 0)
            {
                dbContent.Summary = quote.TagItems.FirstOrDefault(x => string.Equals(x.Type, "city")).DisplayLocation;
            }
            else if (quote.TagItems.Where(x => string.Equals(x.Type, "state"))?.ToList()?.Count > 0)
            {
                dbContent.Summary = quote.TagItems.FirstOrDefault(x => string.Equals(x.Type, "state")).DisplayLocation;
            }
            else if (quote.TagItems.Where(x => string.Equals(x.Type, "country"))?.ToList()?.Count > 0)
            {
                dbContent.Summary = quote.TagItems.FirstOrDefault(x => string.Equals(x.Type, "country")).DisplayLocation;
            }
            else
            {
                dbContent.Summary = "United States";
            }

            dbContent.Title = $"{quote.FirstName} {quote.LastName}";
            dbContent.ContentHtml = quote.Text;
            dbContent.Code = quote.Code;
            dbContent.Type = ContentType.Quote;
            dbContent.Tags = quote.TagItems != null ? string.Join(",", quote.TagItems.Select(x => x.Value)) : null;
            dbContent.Status = ContentStatus.Published;
            dbContent.CreateDate = DateTime.UtcNow;
            dbContent.PublishDate = DateTime.UtcNow;
            return dbContent;
        }
    }
}
