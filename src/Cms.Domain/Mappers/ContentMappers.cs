﻿using Ardalis.GuardClauses;
using Bbb.Core.Utilities.Extensions;
using Cms.Domain.Cache;
using Cms.Domain.Enum;
using Cms.Domain.Extensions;
using Cms.Domain.Helpers;
using Cms.Domain.Model;
using Cms.Domain.UserGuardClauses;
using Cms.Model.Constants;
using Cms.Model.Enum;
using Cms.Model.Models;
using Cms.Model.Models.Config;
using Cms.Model.Models.Content;
using Cms.Model.Models.Content.Db;
using Cms.Model.Models.Content.Entities;
using Cms.Model.Models.ContentLocation;
using Cms.Model.Models.User;
using Cms.Model.Repositories;
using Cms.Model.Repositories.Content;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Article = Bbb.Core.Solr.Model.Article;
using SearchTypeahead = Bbb.Core.Solr.Model.SearchTypeahead;

namespace Cms.Domain.Mappers
{
    public interface IContentMappers
    {
        Task<Content> MapToContentAsync(DbContent dbContent, AppUser user);
        Task<DbContent> MapToDbContentAsync(Content content, AppUser user);
        Article MapToSolrArticle(DbContent dbContent, DbContentEvent dbContentEvent, DbContentNearMe dbContentNearMe, DbContentAbList dbContentAbList);
        Task<Content> InitializeContentAsync(Content content, AppUser user);
        ContentPreview MapToContentPreview(DbContent content, AppUser user, string defaultSite);
    }

    public class ContentMappers : IContentMappers
    {
        private readonly IContentAuditLogMappers _auditLogMappers;
        private readonly IBbbInfoRepository _bbbInfoRepository;
        private readonly IContentRepository _repository;
        private readonly IContentEventOccurenceRepository _eventOccurrenceRepository;
        private readonly IContentGeotagMappers _geotagMappers;
        private readonly IContentMediaRepository _mediaRepository;
        private readonly ILegacyBbbSiteCache _legacyBbbSiteCache;
        private readonly IUserRepository _userRepository;
        private readonly IUserProfileRepository _userProfileRepository;
        private readonly ConnectionStrings _config;
        private readonly General _generalConfig;
        public ContentMappers(
            IContentAuditLogMappers auditLogMappers,
            IBbbInfoRepository bbbInfoRepository,
            IContentRepository repository,
            IContentEventOccurenceRepository eventOccurrenceRepository,
            IContentGeotagMappers geotagMappers,
            IContentMediaRepository mediaRepository,
            ILegacyBbbSiteCache legacyBbbSiteCache,
            IUserRepository userRepository,
            IUserProfileRepository userProfileRepository,
            IOptions<ConnectionStrings> config,
            IOptions<General> generalConfig)
        {
            _auditLogMappers = auditLogMappers;
            _bbbInfoRepository = bbbInfoRepository;
            _legacyBbbSiteCache = legacyBbbSiteCache;
            _repository = repository;
            _eventOccurrenceRepository = eventOccurrenceRepository;
            _geotagMappers = geotagMappers;
            _mediaRepository = mediaRepository;
            _userRepository = userRepository;
            _userProfileRepository = userProfileRepository;

            _config = config.Value;
            _generalConfig = generalConfig.Value;
        }

        public async Task<Content> MapToContentAsync(DbContent dbContent, AppUser user)
        {
            var content = new Content();

            content.Authors = dbContent.Authors.DeserializeOrDefault(new List<Author>());
            content.BbbAuthor = dbContent.BbbAuthor;
            content.BbbIds = MapFlatJsonListToGeotagList(
                dbContent.BbbIds,
                dbContent.PinnedLocations,
                dbContent.Code,
                LocationTypes.BbbId);
            content.Categories = dbContent.Categories.DeserializeOrDefault(new List<SearchTypeahead>());
            content.CheckoutBy = dbContent.CheckoutBy;
            content.CheckoutDate = dbContent.CheckoutDate;
            content.Cities = dbContent.Cities.DeserializeOrDefault(new List<Location>());
            content.PinnedCities = dbContent.PinnedCities.DeserializeOrDefault(new List<Location>());
            content.Code = dbContent.Code;
            content.ContentHtml = dbContent.ContentHtml;
            content.Countries = MapFlatJsonListToGeotagList(
                dbContent.Countries,
                dbContent.PinnedLocations,
                dbContent.Code,
                LocationTypes.Country);
            content.PinnedCountries = MapFlatJsonListToGeotagList(
                dbContent.PinnedCountries,
                dbContent.PinnedLocations,
                dbContent.Code,
                LocationTypes.Country);
            content.CreateDate = dbContent.CreateDate;
            content.CreatedBy = dbContent.CreatedBy;
            content.Deleted = dbContent.Deleted;
            content.EventBbb = dbContent.EventBbb.DeserializeOrDefault<LegacyBbbInfo>(null);
            content.ExpirationDate = dbContent.ExpirationDate;
            content.Id = dbContent.Id;
            content.Image = dbContent.Image;
            content.Layout = dbContent.Layout;
            content.Locations = dbContent.Locations;
            content.MetaDescription = dbContent.MetaDescription;
            content.MobileHeadline = dbContent.MobileHeadline;
            content.ModifiedBy = dbContent.ModifiedBy;
            content.ModifiedDate = dbContent.ModifiedDate;
            content.NoIndex = dbContent.NoIndex;
            content.OwnerSiteId = dbContent.OwnerSiteId;
            content.PageTitle = dbContent.PageTitle;
            content.PinnedLocations = dbContent.PinnedLocations.DeserializeOrDefault(new List<PinnedContentLocation>());
            content.PinnedTopic = dbContent.PinnedTopic;
            content.Priority = dbContent.Priority;
            content.PublishDate = dbContent.PublishDate;
            content.Sites = dbContent.Sites.DeserializeOrDefault(new List<LegacyBbbInfo>());
            content.PinnedBbbIds = dbContent.PinnedBbbIds.DeserializeOrDefault<List<string>>(null);
            content.States = MapFlatJsonListToGeotagList(
                dbContent.States,
                dbContent.PinnedLocations,
                dbContent.Code,
                LocationTypes.State);
            content.PinnedStates = MapFlatJsonListToGeotagList(
                dbContent.PinnedStates,
                dbContent.PinnedLocations,
                dbContent.Code,
                LocationTypes.State);
            content.Status = dbContent.Status;
            content.Tags = StringHelpers.DeserializeTagsString(dbContent.Tags);
            content.Title = dbContent.Title;
            var topicList = dbContent.Topics.DeserializeOrDefault(new List<string>());
            content.IsIndustryTip = topicList.Any(t => t == Topics.Tips);
            topicList.RemoveAll(t => t == Topics.Tips);
            content.Topics = topicList;
            content.Type = dbContent.Type;
            content.UriSegment = dbContent.UriSegment;
            content.Videos = dbContent.Videos.DeserializeOrDefault(new List<Video>());
            content.HasRedirectUrl = !string.IsNullOrWhiteSpace(dbContent.RedirectUrl);
            content.RedirectUrl = dbContent.RedirectUrl;
            content.HasSeoCanonical = !string.IsNullOrWhiteSpace(dbContent.SeoCanonical);
            content.SeoCanonical = dbContent.SeoCanonical;

            content.AvailableAuthors = await MapToAvailableAuthorsAsync(dbContent.CreatedBy, dbContent.BbbAuthor, user);
            content.CanCheckOut = user.HasAccessToRecord(dbContent.CreatedBy, dbContent.OwnerSiteId);
            content.CanEdit = dbContent.CheckoutDate != null
                && string.Equals(dbContent.CheckoutBy, user.Email, StringComparison.InvariantCultureIgnoreCase);
            content.IsCheckedOut = dbContent.CheckoutDate != null;
            content.Occurrences = dbContent.Type == ContentType.Event
                ? await _eventOccurrenceRepository.SelectForContentCode(dbContent.Code)
                : null;
            content.Summary = dbContent.Summary.StripHtml();

            switch (dbContent.Type)
            {
                case ContentType.NearMe:
                    content.ArticleUrl = ArticleUrlHelpers.CreateNearMeUrl(_config.TerminusBaseUrl, content.Categories.FirstOrDefault());
                    break;
                case ContentType.BaselinePage:
                    content.ArticleUrl = ArticleUrlHelpers.CreateBaselinePageUrl(_config.TerminusBaseUrl, content.UriSegment);
                    break;
                default:
                    content.ArticleUrl = ArticleUrlHelpers.CreateGenericUrl(
                    _config.TerminusBaseUrl,
                    dbContent.Id,
                    dbContent.Title,
                    dbContent.PinnedTopic,
                    dbContent.Canonical,
                    dbContent.Status);
                    break;
            }
            

            return content;
        }

        public Article MapToSolrArticle(
            DbContent dbContent,
            DbContentEvent dbContentEvent,
            DbContentNearMe dbContentNearMe,
            DbContentAbList dbContentAbList)
        {
            var solrArticle = new Article();

            solrArticle.Id = dbContent.Id.ToString();
            solrArticle.Code = dbContent.Code;
            solrArticle.Title = dbContent.Title;
            solrArticle.ContentHtml = dbContent.ContentHtml;
            solrArticle.Type = dbContent.Type;
            solrArticle.Summary = dbContent.Summary;
            solrArticle.Categories = SolrHelpers.FormatCategories(dbContent.Categories);
            if (dbContent.PublishDate.HasValue)
            {
                solrArticle.PublishDate = dbContent.PublishDate.Value.UtcDateTime;
            }
            if (dbContent.ExpirationDate.HasValue)
            {
                solrArticle.ExpirationDate = dbContent.ExpirationDate.Value.UtcDateTime;
            }
            solrArticle.Authors = !string.IsNullOrWhiteSpace(dbContent.Authors) ?
                new List<string>() { dbContent.Authors } :
                null;
            solrArticle.CreateDate = dbContent.CreateDate.HasValue
                ? dbContent.CreateDate.Value.UtcDateTime
                : new DateTime();
            solrArticle.CreatedBy = dbContent.CreatedBy;
            solrArticle.ModifiedBy = dbContent.ModifiedBy;
            solrArticle.ModifiedDate = dbContent.ModifiedDate.DateTime;
            solrArticle.Image = dbContent.Image;
            solrArticle.Status = dbContent.Status;
            solrArticle.MobileHeadline = dbContent.MobileHeadline;
            solrArticle.PageTitle = dbContent.PageTitle;
            solrArticle.Cities = SolrHelpers.FormatCities(dbContent.Cities);
            solrArticle.PinnedCities = SolrHelpers.FormatCities(dbContent.PinnedCities);
            solrArticle.States = dbContent.States.DeserializeOrDefault<List<string>>(null);
            solrArticle.PinnedStates = dbContent.PinnedStates.DeserializeOrDefault<List<string>>(null);
            solrArticle.Countries = dbContent.Countries.DeserializeOrDefault<List<string>>(null);
            solrArticle.PinnedCountries = dbContent.PinnedCountries.DeserializeOrDefault<List<string>>(null);
            solrArticle.Keywords = StringHelpers.MapCommaSeparatedStringToList(dbContent.Tags);
            solrArticle.Tags = StringHelpers.MapCommaSeparatedStringToList(dbContent.Tags);
            solrArticle.Topics = dbContent.Topics.DeserializeOrDefault<List<string>>(null);
            solrArticle.PinnedTopic = dbContent.PinnedTopic;
            solrArticle.BbbIds = SolrHelpers.FormatSites(dbContent.Sites);
            solrArticle.CanonicalUrl = dbContent.Canonical;
            solrArticle.Priority = dbContent.Priority;
            solrArticle.MetaDescription = dbContent.MetaDescription;
            solrArticle.UriSegment = dbContent.UriSegment;
            solrArticle.Layout = dbContent.Layout;
            solrArticle.NoIndex = dbContent.NoIndex;
            solrArticle.Videos = SolrHelpers.FormatArticleVideos(dbContent.Videos);
            solrArticle.TaggedBbbIds = dbContent.BbbIds.DeserializeOrDefault<List<string>>(null);
            solrArticle.PinnedTaggedBbbIds = dbContent.PinnedBbbIds.DeserializeOrDefault<List<string>>(null);
            solrArticle.RedirectUrl = dbContent.RedirectUrl;
            solrArticle.SeoCanonical = dbContent.SeoCanonical;

            if (dbContentEvent != null && dbContent.Type == ContentType.Event)
            {
                solrArticle.EventLocationName = dbContentEvent.LocationName;
                solrArticle.EventStartDate = dbContentEvent.StartDate.HasValue
                    ? dbContentEvent.StartDate.Value.UtcDateTime
                    : new DateTime();
                solrArticle.EventEndDate = dbContentEvent.EndDate.HasValue
                    ? dbContentEvent.EndDate.Value.UtcDateTime
                    : new DateTime();
                solrArticle.EventAddress1 = dbContentEvent.AddressLine;
                solrArticle.EventAddress2 = dbContentEvent.AddressLine2;
                solrArticle.EventCountry = dbContentEvent.Country;
                solrArticle.EventCity = dbContentEvent.City;
                solrArticle.EventState = dbContentEvent.State;
                solrArticle.EventPostalCode = dbContentEvent.PostalCode;
                solrArticle.EventUrl = dbContentEvent.RegistrationLink;
                solrArticle.EventBbb = SolrHelpers.FormatEventBbb(dbContent.EventBbb);
                solrArticle.IsVirtualEvent = dbContentEvent.IsVirtualEvent;
            }

            if (dbContentNearMe != null && dbContent.Type == ContentType.NearMe)
            {
                solrArticle.NearMeAboveListContent = dbContentNearMe.AboveListContent;
                solrArticle.NearMeRelatedHeadline = dbContentNearMe.RelatedInformationHeadline;
                solrArticle.NearMeRelatedText = dbContentNearMe.RelatedInformationText;
            }

            if (dbContentAbList != null && dbContent.Type == ContentType.AccreditedBusinessList)
            {
                solrArticle.AbListBusinessLinkLists = dbContentAbList.BusinessLinkLists;
            }

            return solrArticle;
        }

        public async Task<DbContent> MapToDbContentAsync(Content content, AppUser user)
        {
            var storedContent = await _repository.GetById(content.Id, user.Email);
            var contentMedia = await _mediaRepository.GetByContentCode(content.Code);

            var dbContent = new DbContent();

            dbContent.Id = content.Id;
            dbContent.Title = content.Title;
            dbContent.ContentHtml = content.ContentHtml;
            dbContent.Code = content.Code;
            dbContent.PublishDate = content.PublishDate;
            dbContent.ExpirationDate = content.ExpirationDate;
            dbContent.Tags = content.Tags.SerializeOrDefault();
            dbContent.Status = content.Status;
            dbContent.Type = content.Type;
            dbContent.Layout = content.Layout;
            dbContent.CreatedBy = content.CreatedBy;
            dbContent.CreateDate = content.CreateDate;
            dbContent.Sites = content.Sites.SerializeOrDefault();
            dbContent.OwnerSiteId = content.OwnerSiteId;
            dbContent.NoIndex = content.NoIndex;
            dbContent.BbbAuthor = content.BbbAuthor;
            dbContent.CheckoutBy = content.CheckoutBy;
            dbContent.CheckoutDate = content.CheckoutDate;
            dbContent.Deleted = content.Deleted;
            dbContent.PinnedCountries = content.Countries?
                .Where(x => x.IsPinned)?
                .Select(x => x.LocationId)?
                .SerializeOrDefault();
            dbContent.PinnedStates = content.States?
                .Where(x => x.IsPinned)?
                .Select(x => x.LocationId)?
                .SerializeOrDefault();
            dbContent.PinnedCities = content.PinnedCities.SerializeOrDefault();
            dbContent.PinnedBbbIds = content.BbbIds?
                .Where(x => x.IsPinned)?
                .Select(x => x.LocationId)?
                .SerializeOrDefault();
            dbContent.Countries = content.Countries?.Select(x => x.LocationId)?.SerializeOrDefault();
            dbContent.States = content.States?.Select(x => x.LocationId)?.SerializeOrDefault();
            dbContent.Cities = content.Cities.SerializeOrDefault();
            dbContent.BbbIds = content.BbbIds?.Select(x => x.LocationId)?.SerializeOrDefault();
            dbContent.Priority = content.Priority;
            dbContent.PinnedLocations = content.PinnedLocations.SerializeOrDefault();
            dbContent.Videos = content.Videos.SerializeOrDefault();
            dbContent.PinnedTopic = Topics.IsValidTopic(content.PinnedTopic)
                ? content.PinnedTopic
                : Topics.Default;
            if (content.IsIndustryTip)
            {
                content.Topics.Add(Topics.Tips);
            }
            dbContent.Topics = content.Topics.SerializeOrDefault(
                new List<string>() { Topics.Default }.SerializeObject());
            dbContent.MobileHeadline = content.MobileHeadline.ToSubstring(100, content.Title);
            dbContent.UriSegment = ArticleUrlHelpers.CreateUrlSegment(content);
            dbContent.Summary = content.Summary.StripHtml();
            dbContent.GeoTags = MapToGeotags(content.Countries, content.States, content.Cities);
            dbContent.TopicsAndTags = MapToTopicsAndTags(content.Topics, content.Tags);
            dbContent.EventBbb = content.EventBbb != null
                ? content.EventBbb.SerializeObject()
                : null;
            dbContent.RedirectUrl = content.HasRedirectUrl
                ? content.RedirectUrl
                : null;
            dbContent.SeoCanonical = content.HasSeoCanonical
                ? content.SeoCanonical
                : null;

            if (content.Type == ContentType.AccreditedBusinessList)
            {
                var bbbName = content.Authors?.FirstOrDefault()?.Name;
                var dateString = dbContent.PublishDate.HasValue
                    ? $"{dbContent.PublishDate.Value.ToString("MMM")} {dbContent.PublishDate.Value.Year}"
                    : $"{dbContent.CreateDate.Value.ToString("MMM")} {dbContent.CreateDate.Value.Year}";

                dbContent.PageTitle = !string.IsNullOrWhiteSpace(bbbName)
                    ? $"Newly Accredited Businesses | {bbbName} | {dateString}"
                    : $"Newly Accredited Businesses | {dateString}";
                dbContent.MetaDescription = !string.IsNullOrWhiteSpace(bbbName)
                    ? $"Here are the newest businesses to become BBB Accredited with {bbbName}"
                    : "Here are the newest businesses to become BBB Accredited.";
            }
            else
            {
                dbContent.PageTitle = content.PageTitle.ToSubstring(100, content.Title);
                dbContent.MetaDescription = content.MetaDescription.ToSubstring(255, content.Summary);
            }

            dbContent.Image = contentMedia != null && contentMedia.Any()
                ? JsonConvert.SerializeObject(contentMedia)
                : content.Image;
            dbContent.ModifiedBy = user.Email;
            dbContent.Authors = JsonConvert.SerializeObject(
                new[] { MapToAuthor(content.BbbAuthor) });
            dbContent.Categories = content.Categories.SerializeOrDefault();

            if (content.ArticleUrl != null)
            {
                ArticleUrl url;
                switch (dbContent.Type)
                {
                    case ContentType.NearMe:
                        url = ArticleUrlHelpers.CreateNearMeUrl(_config.TerminusBaseUrl, content.Categories.FirstOrDefault());
                        break;
                    case ContentType.BaselinePage:
                        url = ArticleUrlHelpers.CreateBaselinePageUrl(_config.TerminusBaseUrl, content.ArticleUrl.Segment);
                        break;
                    default:
                        url = ArticleUrlHelpers.CreateGenericUrl(
                        _config.TerminusBaseUrl,
                        dbContent.Id,
                        dbContent.Title,
                        dbContent.PinnedTopic,
                        dbContent.Canonical,
                        dbContent.Status);
                        break;
                }

                url.Segment = content.ArticleUrl.Segment.ToUrlSlug();
                dbContent.Canonical = url.Full;
            }

            // We'll want to check this after doing most of the mapping
            // so that we can ensure the audit summary we are looking at is accurate.
            var currentModifiedDate = content.ModifiedDate;
            dbContent.ModifiedDate = MapToHasVisibleUpdates(storedContent, dbContent) && currentModifiedDate == storedContent?.ModifiedDate
                ? DateTimeOffset.Now
                : currentModifiedDate ?? DateTimeOffset.Now;

            return dbContent;
        }

        public async Task<Content> InitializeContentAsync(Content content, AppUser user)
        {
            var userProfile = await _userProfileRepository.GetForUser(user.UserId) ?? new UserProfile();
            Guard.Against.MissingDefaultSite(userProfile);

            var contentHeadline = content.Title.Trim();

            var defaultTopic = (content.Type == ContentType.Event) ? Topics.Events : Topics.Default;
            var defaultSite = await _legacyBbbSiteCache.GetByBbbIdAsync(userProfile.DefaultSite);

            content.Id = 0;
            content.CreatedBy = user.Email;
            content.Status = ContentStatus.Draft;
            content.Code = Guid.NewGuid().ToString();
            content.CreateDate = DateTimeOffset.Now;
            content.ModifiedBy = user.Email;
            content.ModifiedDate = DateTimeOffset.Now;
            content.CheckoutBy = user.Email;
            content.CheckoutDate = DateTimeOffset.Now;
            content.PinnedTopic = userProfile.PinnedTopic ?? defaultTopic;
            var topicList = userProfile.Topics.DeserializeOrDefault(
                new List<string>() { defaultTopic });
            content.IsIndustryTip = topicList.Any(t => t == Topics.Tips);
            content.Topics = topicList;
            content.Title = contentHeadline;
            content.PageTitle = contentHeadline.ToSubstring(100);
            content.MobileHeadline = contentHeadline.ToSubstring(100);
            content.Sites = new List<LegacyBbbInfo> { defaultSite };
            content.BbbAuthor = defaultSite.LegacyBBBID == _generalConfig.CouncilBbbId
                ? "bbb"
                : defaultSite.LegacyBBBID;
            content.Authors = new List<Author> {
                MapToAuthor(content.BbbAuthor)
            };
            content.UriSegment = ArticleUrlHelpers.CreateUrlSegment(content);
            content.OwnerSiteId = userProfile.DefaultSite;
            content.Priority = 10;
            content.NoIndex = content.Type == ContentType.BaselinePage ? false : true; 

            return content;
        }

        public ContentPreview MapToContentPreview(DbContent dbContent, AppUser user, string defaultSite)
        {
            var preview = new ContentPreview();
            preview.Cities = dbContent.Cities.DeserializeOrDefault(new List<Location>());
            preview.Code = dbContent.Code;
            preview.Countries = MapFlatJsonListToGeotagList(
                dbContent.Countries,
                dbContent.PinnedLocations,
                dbContent.Code,
                LocationTypes.Country);
            preview.CreatedBy = dbContent.CreatedBy;
            preview.ExpirationDate = dbContent.ExpirationDate;
            preview.Id = dbContent.Id;
            preview.CreateDate = dbContent.CreateDate;
            preview.PublishDate = dbContent.PublishDate;
            preview.States = MapFlatJsonListToGeotagList(
                dbContent.States,
                dbContent.PinnedLocations,
                dbContent.Code,
                LocationTypes.State);
            preview.Status = dbContent.Status;
            preview.Tags = StringHelpers.DeserializeTagsString(dbContent.Tags);
            preview.Title = dbContent.Title;
            preview.TotalCount = dbContent.TotalCount;
            preview.Type = dbContent.Type;
            preview.CanEdit = dbContent.CheckoutDate != null
                && string.Equals(dbContent.CheckoutBy, user.Email, StringComparison.InvariantCultureIgnoreCase);
            preview.IsCheckedOut = dbContent.CheckoutDate != null;
            preview.TopicsAndTags = dbContent.TopicsAndTags;
            preview.CheckoutBy = dbContent.CheckoutBy;
            preview.GeoTags = dbContent.GeoTags;
            preview.ServiceArea = MapFlatJsonListToGeotagList(
                dbContent.BbbIds,
                dbContent.PinnedLocations,
                dbContent.Code,
                LocationTypes.BbbId);
            var bbbIds = dbContent.BbbIds.DeserializeOrDefault<List<string>>(null) ?? new List<string>();
            bbbIds.AddRange(dbContent.PinnedBbbIds.DeserializeOrDefault<List<string>>(null) ?? new List<string>());

            if (!string.IsNullOrWhiteSpace(defaultSite))
            {
                preview.IsInMyNewsfeed = bbbIds.Contains(defaultSite);
            }
            return preview;
        }

        #region Helper methods
        private Author MapToAuthor(string bbbId)
        {
            if (string.IsNullOrWhiteSpace(bbbId) || string.Equals("bbb", bbbId))
            {
                return ContentConstants.DefaultAuthor;
            }

            var bbbInfo = _bbbInfoRepository.GetByLegacyId(bbbId).Result;

            var author = new Author();
            author.Id = bbbInfo.LegacyId;
            author.Name = bbbInfo.Name;
            author.Url = $"/local-bbb/{bbbInfo.BbbUrlSegment}";
            return author;
        }

        private string MapToTopicsAndTags(IEnumerable<string> topics, IEnumerable<string> tags)
        {
            var topicsAndTags = !topics.IsNullOrEmpty()
                ? !tags.IsNullOrEmpty()
                    ? topics.Concat(tags)
                    : topics
                : tags;

            return !topicsAndTags.IsNullOrEmpty()
                ? string.Join(",", topicsAndTags)
                : string.Empty;
        }

        private string MapToGeotags(List<Geotag> countries, List<Geotag> states, List<Location> cities)
        {
            var geoTagsList = new List<string>();
    
            if (!countries.IsNullOrEmpty())
            {
                geoTagsList = geoTagsList.Concat(countries.Select(x => x.LocationId)).ToList();
            }
            if (!states.IsNullOrEmpty())
            {
                geoTagsList = geoTagsList.Concat(countries.Select(x => x.LocationId)).ToList();
            }
            if (!cities.IsNullOrEmpty())
            {
                geoTagsList = geoTagsList.Concat(cities.Select(x => x.City)).ToList();
            }

            return !geoTagsList.IsNullOrEmpty() ?
                string.Join(",", geoTagsList) :
                null;
        }

        private List<Geotag> MapFlatJsonListToGeotagList(
            string locationsJson,
            string pinnedLocationsJson,
            string contentCode,
            string locationType)
        {
            if (string.IsNullOrWhiteSpace(locationsJson)) return new List<Geotag>();

            var locations = locationsJson.DeserializeOrDefault(new List<string>());

            if (locations.IsNullOrEmpty()) return new List<Geotag>();

            var pins = pinnedLocationsJson
                .DeserializeOrDefault(new List<PinnedLocationModel>())
                .Where(x => string.Equals(
                            locationType,
                            x.LocationType,
                            StringComparison.InvariantCultureIgnoreCase));
            return locations
                .Select(loc => _geotagMappers.MapToGeotag(
                    loc,
                    contentCode,
                    locationType,
                    pins.FirstOrDefault(
                        pin => string.Equals(
                            loc,
                            pin.PinnedLocation,
                            StringComparison.InvariantCultureIgnoreCase))))
                .ToList();
        }

        private async Task<List<Author>> MapToAvailableAuthorsAsync(string articleCreatedBy, string articleBbbAuthor, AppUser currentUser)
        {
            var availableAuthors = new HashSet<Author>();

            var councilBbbAuthor = ContentConstants.DefaultAuthor;

            if (string.Equals(articleCreatedBy, "system"))
            {
                availableAuthors.Add(councilBbbAuthor);
                return availableAuthors.ToList();
            }

            // 1) Lookup site author info for createdBy
            var creatorUser = await _userRepository.GetByEmail(articleCreatedBy);
            if (creatorUser != null)
            {
                var creatorProfile = await _userProfileRepository.GetForUser(creatorUser.UserId.ToString());
                var creatorSite = await _legacyBbbSiteCache.GetByBbbIdAsync(creatorProfile.DefaultSite);
                var creatorAuthor = MapToAuthor(creatorProfile.DefaultSite);
                availableAuthors.Add(creatorAuthor);
                if (string.IsNullOrWhiteSpace(articleBbbAuthor))
                {
                    articleBbbAuthor = creatorSite.LegacyBBBID;
                }
            }

            //2) Lookup site author info for content.BbbAuthor
            if (!string.Equals("bbb", articleBbbAuthor))
            {
                var contentAuthor = MapToAuthor(articleBbbAuthor);
                availableAuthors.Add(contentAuthor);
            }
            else
            {
                availableAuthors.Add(councilBbbAuthor);
            }

            if (currentUser.IsAdmin)
            {
                var userProfile = await _userProfileRepository.GetForUser(currentUser.UserId) ?? new UserProfile();
                var adminAuthor = MapToAuthor(userProfile.DefaultSite);
                availableAuthors.Add(adminAuthor);

                if (userProfile.DefaultSite == _generalConfig.CouncilBbbId)
                {
                    availableAuthors.Add(councilBbbAuthor);
                }
            }

            return availableAuthors.ToList();
        }

        private bool MapToHasVisibleUpdates(DbContent existingContent, DbContent updatedContent)
        {
            if (existingContent == null)
            {
                return true;
            }

            return _auditLogMappers.MapToDbAuditLogs(existingContent, updatedContent, string.Empty)
                .Select(x => x.ActionCode)
                .Intersect(EntityActions.VISIBLE_UPDATES)
                .Any();
        }
        #endregion
    }
}
