﻿using Cms.Domain.Exception;
using System;

namespace Cms.Domain.Services
{
    public interface IContentValidationService
    {
        void ValidatePublishDates(DateTimeOffset publishDate, 
            DateTimeOffset expirationDate);
    }

    public class ContentValidationService : IContentValidationService
    {
        public void ValidatePublishDates(DateTimeOffset publishDate, 
            DateTimeOffset expirationDate) {

            if (publishDate == null ||
                publishDate < DateTimeOffset.UtcNow) {
                throw new InvalidPublishDateException();
            }

            if (publishDate >= expirationDate) {
                throw new InvalidExpirationDateException();
            }
        }
    }
}
