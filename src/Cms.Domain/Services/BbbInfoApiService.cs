﻿using Cms.Model.Enum;
using Cms.Model.Services;
using log4net;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Cms.Domain.Services
{
    public interface ILegacyBbbInfoService
    {
        Task<IEnumerable<LegacyBbbInfo>> GetAll();
    }

    public class LegacyBbbInfoService : ILegacyBbbInfoService
    {
        private readonly IConnectionStringService _connectionStringService;
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public LegacyBbbInfoService(
            IConnectionStringService connectionStringService) {
            _connectionStringService = connectionStringService;
        }

        public async Task<IEnumerable<LegacyBbbInfo>> GetAll()
        {
            try
            {
                var baseUrl =
                        _connectionStringService.GetVendorConnectionString(
                            Repository.BbbInfoApi);

                using (var client = new HttpClient()) {
                    return 
                        JsonConvert.DeserializeObject<IEnumerable<LegacyBbbInfo>>(
                            await (await client.GetAsync(baseUrl)).Content.ReadAsStringAsync());
                }
            }
            catch (System.Exception exception)
            {
                _log.Error(exception);
                return null;
            }
        }
    }
}
