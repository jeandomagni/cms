﻿using Cms.Model.Enum;
using Cms.Model.Models.General;
using Cms.Model.Services;
using CsvHelper;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Cms.Model.Models.Config;

namespace Cms.Domain.Services
{
    public interface ILeadService
    {
        Task<byte[]> GetListed (string bbbId, DateTime rangeStart, DateTime? rangeEnd = null);
        Task<byte[]> GetAccreditation (string bbbId, DateTime rangeStart, DateTime? rangeEnd = null);
    }

    public class LeadService : ILeadService
    {
        private readonly IConnectionStringService _connectionStringService;
        private static readonly ILog _log = LogManager.GetLogger (System.Reflection.MethodBase.GetCurrentMethod ().DeclaringType);
        private readonly string _accessToken;


        public LeadService (IConnectionStringService connectionStringService, IOptions<LeadsApiSettings> leadsApiSettingsConfig)
        {
            _connectionStringService = connectionStringService;
            _accessToken = leadsApiSettingsConfig.Value.Token;
        }

        public Task<byte[]> GetListed (string bbbId, DateTime rangeStart, DateTime? rangeEnd = null)
        {
            return GetAll (bbbId, rangeStart, rangeEnd, null, "business");
        }

        public Task<byte[]> GetAccreditation (string bbbId, DateTime rangeStart, DateTime? rangeEnd = null)
        {
            return GetAll (bbbId, rangeStart, rangeEnd, "accreditation");
        }

        public async Task<byte[]> GetAll (string bbbId, DateTime rangeStart, DateTime? rangeEnd = null, string leadType = null, string reportertype = null)
        {
            try {
                var baseUrl = _connectionStringService.GetVendorConnectionString (Repository.LeadsApiUrl);
                var urlWithQueries = $"{baseUrl}?rangestart={rangeStart:yyyy-MM-dd}&bbbid={bbbId}&pageSize={int.MaxValue}";
                if (rangeEnd.HasValue)
                    urlWithQueries += $"&rangeend={rangeEnd:yyyy-MM-dd}";
                if (!string.IsNullOrWhiteSpace (leadType))
                    urlWithQueries += $"&leadtype={leadType}";
                if (!string.IsNullOrWhiteSpace (reportertype))
                    urlWithQueries += $"&reportertype={reportertype}";

                using (var client = new HttpClient ()) {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue ("Bearer", _accessToken);
                    var responce = await client.GetAsync (urlWithQueries);
                    var content = await responce.Content.ReadAsStringAsync ();
                    var data = JsonConvert.DeserializeObject<LeadsResultsModel> (content);
                    return MakeCsvFile (data.LeadsResults);
                }
            } catch (System.Exception exception) {
                _log.Error (exception);
                return null;
            }
        }

        public byte[] MakeCsvFile (List<LeadVM> records)
        {

            using (var memoryStream = new MemoryStream ())
            using (var streamWriter = new StreamWriter (memoryStream)) {
                using (var csv = new CsvWriter (streamWriter, CultureInfo.InvariantCulture)) {
                    csv.WriteRecords (records);
                }
                return memoryStream.ToArray ();
            }
        }
    }
}
