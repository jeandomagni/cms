﻿using Cms.Domain.Mappers;
using Cms.Model.Models.BbbInfo;
using SolrNet;
using SolrNet.Commands.Parameters;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SolrBbbInfo = Bbb.Core.Solr.Model.BbbInfo;

namespace Cms.Domain.Services.Solr
{
    public interface ISolrBbbInfoService
    {
        Task Update(BbbInfo bbbInfo);
        Task<IList<SolrBbbInfo>> GetAllAsync();
        Task<SolrBbbInfo> GetByBbbIdAsync(string bbbId);
    }

    public class SolrBbbInfoService : ISolrBbbInfoService
    {
        private readonly IBbbInfoMappers _bbbInfoMappers;
        private readonly ISolrOperations<SolrBbbInfo> _solr;
        public SolrBbbInfoService(ISolrOperations<SolrBbbInfo> solr, IBbbInfoMappers bbbInfoMappers) {
            _bbbInfoMappers = bbbInfoMappers;
            _solr = solr;
        }

        public async Task<IList<SolrBbbInfo>> GetAllAsync()
        {
            var bbbInfoSolrResult = await _solr.QueryAsync(
                new SolrQuery($"{SolrBbbInfo.LegacyIdField}:*"),
                new QueryOptions
                {
                    Fields = new[] { SolrBbbInfo.LegacyIdField, SolrBbbInfo.LocationsField, SolrBbbInfo.PrimaryCountryField },
                    Rows = 1000, // Just keeping this high enough to ensure that we fetch all BBBs.
                });

            return bbbInfoSolrResult
                    .Where(x => !string.IsNullOrWhiteSpace(x?.LegacyId))
                    .ToList();
        }

        public async Task<SolrBbbInfo> GetByBbbIdAsync(string bbbId)
        {
            var bbbInfoSolrResult = await _solr.QueryAsync(
                new SolrQuery($"{SolrBbbInfo.LegacyIdField}:{bbbId}"),
                new QueryOptions
                {
                    Fields = new[] {
                        SolrBbbInfo.LegacyIdField,
                        SolrBbbInfo.PrimaryCountryField,
                        SolrBbbInfo.PrimaryLanguageField,
                    },
                    Rows = 1,
                });

            return bbbInfoSolrResult.FirstOrDefault();
        }

        public async Task Update(BbbInfo bbbInfo)
        {
            await _solr.AddAsync(_bbbInfoMappers.MapToSolrDoc(bbbInfo));
            await _solr.CommitAsync();
        }
    }
}
