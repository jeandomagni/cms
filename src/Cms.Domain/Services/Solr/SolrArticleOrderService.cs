﻿using Bbb.Core.Solr.Model;
using Bbb.Core.Utilities.Solr.Atomic;
using Bbb.Core.Utilities.Solr.Services;
using Cms.Model.Enum;
using Cms.Model.Models.Config;
using Flurl;
using SolrNet;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using SolrArticleOrder = Bbb.Core.Solr.Model.ArticleOrder;

namespace Cms.Domain.Services.Solr
{
    public interface ISolrArticleOrderService
    {
        Task AddOrUpdate(SolrArticleOrder articleOrder);
        Task AddOrUpdateMultiple(List<SolrArticleOrder> articleOrders);
        Task DeleteAll();
        Task Delete(int id);
    }
    public class SolrArticleOrderService : ISolrArticleOrderService
    {
        private readonly SolrUpdateService[] _solrUpdateServices;
        private readonly string _core;

        public SolrArticleOrderService(string[] connectionStrings)
        {
            _solrUpdateServices = connectionStrings.Select(cs => new SolrUpdateService(cs.Replace("/solr/", ""))).ToArray();
            _core = SolrCore.ArticleOrder.ToRelativeName();
        }
        
        public async Task Run(Func<SolrUpdateService, Task> action)
        {
            foreach (var updateService in _solrUpdateServices)
            {
                await action(updateService);
            }
        }

        public async Task DeleteAll()
        {
            await Run(async (solrUpdateService) =>
            {
                await solrUpdateService.DeleteQueryAsync(_core, SolrQuery.All.ToString());
                await solrUpdateService.CommitAsync(_core);
            });
        }

        public async Task AddOrUpdate(SolrArticleOrder articleOrder)
        {
            await AddOrUpdateMultiple(new List<SolrArticleOrder> { articleOrder });
        }

        public async Task AddOrUpdateMultiple(List<SolrArticleOrder> articleOrders)
        {
            await Run(async (solrUpdateService) =>
            {
                await solrUpdateService.UpdateAsync(_core, articleOrders.Select(articleOrder => GetSolrArticleOrder(articleOrder)).ToArray());
                await solrUpdateService.CommitAsync(_core);
            });
        }

        private dynamic GetSolrArticleOrder(SolrArticleOrder articleOrder)
        {
            dynamic doc = new ExpandoObject();
            doc.articleOrderId = articleOrder.ArticleOrderId;
            doc.orderedArticleIds = articleOrder.OrderedArticleIds;
            doc.country = articleOrder.Country;
            doc.state = articleOrder.State;
            doc.city = articleOrder.City;
            doc.listingType = articleOrder.ListingType;
            doc.bbbId = articleOrder.BbbId;
            doc.topic = articleOrder.Topic;
            return doc;
        }

        public async Task Delete(int id)
        {
            await Run(async (solrUpdateService) =>
            {
                await solrUpdateService.DeleteIdAsync(_core, id);
                await solrUpdateService.CommitAsync(_core);
            });
        }
    }
}
