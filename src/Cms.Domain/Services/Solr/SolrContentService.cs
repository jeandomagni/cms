﻿using Bbb.Core.Solr.Model;
using Cms.Domain.Enum;
using Cms.Domain.Mappers;
using Cms.Model.Models;
using Cms.Model.Models.Content.Db;
using Cms.Model.Repositories.Content;
using SolrNet;
using SolrNet.Commands.Parameters;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cms.Domain.Services.Solr
{
    public interface ISolrContentService
    {
        Task<Article> GetByIdAsync(int id);
        Task<List<string>> PostContent(DbContent dbContent);
        Task<int> Delete(string id);
        Task<int> Delete(IEnumerable<string> ids);
        Task<List<Article>> GetArticlesByIds(List<string> orderedArticleIds);
        Task<List<string>> GetFutureOreredEventIdsByIds(List<string> orderedArticleIds);
        Task<List<string>> GetFutureArticles();
    }

    public class SolrContentService : ISolrContentService
    {
        private readonly IContentAbListRepository _contentAbListRepository;
        private readonly IContentEventRepository _contentEventRepository;
        private readonly IContentNearMeRepository _contentNearMeRepository;
        private readonly IContentEventOccurenceRepository _contentEventOccurenceRepository;
        private readonly IContentMappers _contentMappers;
        private readonly IContentEventMappers _contentEventMappers;
        private readonly ISolrOperations<Article> _solr;

        public SolrContentService(
            IContentAbListRepository contentAbListRepository,
            IContentEventRepository contentEventRepository,
            IContentNearMeRepository contentNearMeRepository,
            IContentEventOccurenceRepository contentEventOccurenceRepository,
            IContentMappers contentMappers,
            IContentEventMappers contentEventMappers,
            ISolrOperations<Article> solr) {
            _contentAbListRepository = contentAbListRepository;
            _contentEventRepository = contentEventRepository;
            _contentNearMeRepository = contentNearMeRepository;
            _contentEventOccurenceRepository = contentEventOccurenceRepository;
            _contentMappers = contentMappers;
            _contentEventMappers = contentEventMappers;
            _solr = solr;
        }

        public async Task<Article> GetByIdAsync(int id)
        {
            var q = new SolrQuery($"{Article.IdField}:{id.ToString()}");
            var opts = new QueryOptions();

            var solrResult = await _solr.QueryAsync(q, opts);
            return solrResult.FirstOrDefault();
        }

        public async Task<List<string>> PostContent(DbContent dbContent)
        {
            var contentEvent = dbContent.Type == ContentType.Event
                ? await _contentEventRepository.GetForContentByCode(dbContent.Code)
                : null;
            var contentNearMe = dbContent.Type == ContentType.NearMe
                ? await _contentNearMeRepository.GetForContentByCode(dbContent.Code)
                : null;
            var contentAbList = dbContent.Type == ContentType.AccreditedBusinessList
                ? await _contentAbListRepository.GetByContentCode(dbContent.Code)
                : null;

            if (!string.IsNullOrWhiteSpace(contentEvent?.ContentCode))
            {
                // Delete main event and any old recurrences -- we only want to store the current occurrences
                var queryByContentCode = new SolrQueryByField(Article.CodeField, contentEvent.ContentCode);
                await _solr.DeleteAsync(queryByContentCode);
            }

            if (!string.IsNullOrWhiteSpace(contentEvent?.Recurrence))
            {
                var occurrences = await _contentEventOccurenceRepository.SelectForEventId(contentEvent.Id);
                var solrOccurrences = _contentEventMappers.MapToSolrEventOccurrences(dbContent, contentEvent, occurrences);

                await _solr.AddRangeAsync(solrOccurrences);
                await _solr.CommitAsync();
                return solrOccurrences.OrderBy(x => x.EventStartDate).Select(x => x.Id).ToList();
            }

            var solrArticle = _contentMappers.MapToSolrArticle(dbContent, contentEvent, contentNearMe, contentAbList);

            await _solr.AddAsync(solrArticle);
            await _solr.CommitAsync();
            return new List<string> { solrArticle.Id };
        }

        public async Task<int> Delete(string id)
        {
            await _solr.DeleteAsync(id);
            await _solr.CommitAsync();
            return 1;
        }

        public async Task<int> Delete(IEnumerable<string> ids)
        {
            await _solr.DeleteAsync(ids);
            await _solr.CommitAsync();
            return 1;
        }

        public async Task<List<Article>> GetArticlesByIds(List<string> orderedArticleIds)
        {
            string q = $"{Article.IdField}:({string.Join(" OR ", orderedArticleIds)})";
            var opts = new QueryOptions();
            var solrResult = await _solr.QueryAsync(q, opts);

            var result = new List<Article>();
            foreach (var id in orderedArticleIds)
            {
                var article = solrResult.FirstOrDefault(x => x.Id == id);
                result.Add(article);
            }
            return result;

        }

        public async Task<List<string>> GetFutureOreredEventIdsByIds(List<string> orderedArticleIds)
        {
            string q = $"{Article.IdField}:({string.Join(" OR ", orderedArticleIds)})";
            var opts = new QueryOptions();
            opts.Rows = 1000000;
            opts.FilterQueries = new ISolrQuery[] {
                new SolrQueryByField(Article.EventStartDateField, "[NOW TO *]") { Quoted = false } 
            };
            opts.Fields = new List<string> { Article.EventStartDateField, Article.IdField };
            var solrResult = await _solr.QueryAsync(q, opts);

            return solrResult.OrderBy(x => x.EventStartDate).Select(x => x.Id).ToList();
        }

        public async Task<List<string>> GetFutureArticles()
        {
            var q = SolrQuery.All;
            var opts = new QueryOptions();
            opts.Rows = 1000000;
            opts.FilterQueries = new ISolrQuery[] {
                new SolrQueryByField(Article.PublishDateField, "[NOW TO *]") { Quoted = false },
                new SolrQueryByField(Article.StatusField, "published") { Quoted = false }
            };
            opts.Fields = new List<string> { Article.IdField };
            var solrResult = await _solr.QueryAsync(q, opts);

            return solrResult.Select(x => x.Id).ToList();
        }
    }
}
