﻿using Bbb.Core.Solr.Model;
using Cms.Domain.Helpers;
using Cms.Domain.Util;
using Cms.Model.Models.BusinessProfile;
using SolrNet;
using SolrNet.Commands.Parameters;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cms.Domain.Services.Solr
{
    public interface ISolrCibrService
    {
        Task<Cibr> GetPrimaryRecordByBbbIdAndBusinessIdAsync(string bbbId, string businessId);
        Task<Cibr> GetByIdAsync(string id);
        Task<SolrQueryResults<Cibr>> SearchAsync(ProfileSearchParams profileSearchParams);
    }

    public class SolrCibrService : ISolrCibrService
    {
        /// <summary>
        /// List which specifies what default fields are returned from SOLR
        /// </summary>
        public static readonly List<string> DefaultCibrFields = new List<string> {
            Cibr.IdField,
            Cibr.BusinesNameField,
            Cibr.AddressField,
            Cibr.CityField,
            Cibr.StateField,
            Cibr.PostalcodeField,
            Cibr.TobTextField,
            Cibr.RatingField,
            Cibr.BbbMemberField,
            Cibr.CharitySealField,
            Cibr.IsCharityField,
            Cibr.AccreditedCharityField,
            Cibr.ReportUrlField,
            Cibr.PhoneField,
            Cibr.LocationField,
            Cibr.BusinessIdField,
            Cibr.RatingScoreField,
            Cibr.SingleReportUrlField,
            Cibr.LocalReportUrlField,
            Cibr.ScoreField,
            Cibr.LogoUriField,
            Cibr.TobIdField,
            Cibr.BbbIdField,
            Cibr.ProfileUrlField,
            Cibr.LocalProfileUrlField,
            Cibr.SingleProfileUrlField,
            Cibr.CountryCodeField,
            Cibr.IsRequestQuoteActiveField,
            Cibr.RequestAQuoteUrlField
        };

        ISolrOperations<Cibr> _solr;
        public SolrCibrService(ISolrOperations<Cibr> solr)
        {
            _solr = solr;
        }

        public async Task<Cibr> GetByIdAsync(string id)
        {
            var q = new SolrQuery($"{Cibr.IdField}:{id}");
            var opts = new QueryOptions();

            var solrResult = await _solr.QueryAsync(q, opts);
            return solrResult.FirstOrDefault();
        }

        public async Task<Cibr> GetPrimaryRecordByBbbIdAndBusinessIdAsync(string bbbId, string businessId)
        {
            var q = new SolrQuery($"{Cibr.BbbIdField}:{bbbId}")
                && new SolrQuery($"{Cibr.BusinessIdField}:{businessId}");

            var opts = new QueryOptions();
            opts.FilterQueries = new ISolrQuery[]
            {
                new SolrQueryByField(Cibr.IsPrimaryAddressField, "true")
            };

            var solrResult = await _solr.QueryAsync(q, opts);
            return solrResult.FirstOrDefault();
        }

        public async Task<SolrQueryResults<Cibr>> SearchAsync(ProfileSearchParams profileSearchParams)
        {
            var searchFields = "businessname_txt^15.0 businessname_no_case^15.0 altbusinessnames^10.0 altbusinessnames_no_case^10.0 namesearchclean^6.0 namesearchnoexp^10 tobtext_txt tobtextid_txt tobtextall";

            var cleanInput = profileSearchParams.SearchText.Trim().ToSolrQuerySafeString();
            var q = new SolrMultipleCriteriaQuery(new[] {
                new SolrQuery($"(\"{cleanInput}\")").Boost(5),
                QueryUtil.ExplodedTokenizedQuery(cleanInput),
                new SolrQuery($"\"{profileSearchParams.SearchText}\""),
            }, "OR");

            var opts = new QueryOptions();
            opts.Fields = DefaultCibrFields;
            opts.Rows = profileSearchParams.PageSize;
            opts.StartOrCursor = new StartOrCursor.Start(
                SolrHelpers.GetCursorStart(
                    profileSearchParams.PageNumber,
                    profileSearchParams.PageSize));
            opts.ExtraParams = new Dictionary<string, string>()
            {
                { "defType", "edismax" },
                { "pf", searchFields },
                { "qf", searchFields },
                { "tie", "1" }
            };
            opts.FilterQueries = new ISolrQuery[]
            {
                new SolrQueryByField(Cibr.IsPrimaryAddressField, "true")
            };
            opts.OrderBy = new[]
            {
                new SortOrder("score", Order.DESC),
            };

            return await _solr.QueryAsync(q, opts);
        }
    }
}
