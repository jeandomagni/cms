﻿using Bbb.Core.Utilities.Extensions;
using Bbb.Core.Utilities.Solr.Atomic;
using Bbb.Core.Utilities.Solr.Services;
using Cms.Domain.Mappers;
using Cms.Model.Enum;
using Cms.Model.Models.SecTerm;
using SolrNet;
using System;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using SolrSecTerm = Bbb.Core.Solr.Model.SecTerm;

namespace Cms.Domain.Services.Solr
{
    public interface ISolrSecTermService
    {
        Task Update(Cms.Model.Models.SecTerm.SecTerm secTerm);
        Task Delete(Cms.Model.Models.SecTerm.SecTerm secTerm);
    }

    public class SolrSecTermService : ISolrSecTermService
    {
        private readonly SolrUpdateService[] _solrUpdateServices;
        private readonly string _core;

        public SolrSecTermService(string[] connectionStrings)
        {
            _solrUpdateServices = connectionStrings.Select(cs => new SolrUpdateService(cs.Replace("/solr/", ""))).ToArray();
            _core = SolrCore.SecTerms.ToRelativeName();
        }

        public async Task Run(Func<SolrUpdateService, Task> action)
        {
            foreach (var updateService in _solrUpdateServices)
            {
                await action(updateService);
            }
        }

        public async Task Update(Cms.Model.Models.SecTerm.SecTerm secTerm)
        {
            var secContent = secTerm.ToSecTermDocument();
            if (secTerm.IsActive)
            {
                await Post(secContent);
            }
            else
            {
                await Remove(secContent);
            }
        }

        public async Task Delete(Cms.Model.Models.SecTerm.SecTerm secTerm)
        {
            var secContent = secTerm.ToSecTermDocument();
            await Remove(secContent);
        }

        #region Helper Methods
        private async Task Post(SolrSecTerm secContent)
        {
            await Run(async (solrUpdateService) =>
            {
                await solrUpdateService.UpdateAsync(_core, GetSecContent(secContent));
                await solrUpdateService.CommitAsync(_core);
            });
            
        }

        private dynamic GetSecContent(SolrSecTerm secContent)
        {
            var doc = new ExpandoObject();
            doc.Add(new System.Collections.Generic.KeyValuePair<string, object>(SolrSecTerm.IdField, secContent.TobId));
            doc.Add(new System.Collections.Generic.KeyValuePair<string, object>(SolrSecTerm.MetadataBusinessJsonField, secContent.MetadataBusinessJson));
            doc.Add(new System.Collections.Generic.KeyValuePair<string, object>(SolrSecTerm.MetadataJsonField, secContent.MetadataJson));

            return doc;
        }

        private async Task Remove(SolrSecTerm secContent)
        {
            await Run(async (solrUpdateService) =>
            {
                await solrUpdateService.DeleteIdAsync(_core, secContent.TobId);
                await solrUpdateService.CommitAsync(_core);
            });
        }
        #endregion
    }
}
