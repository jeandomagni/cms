﻿using Bbb.Core.Utilities.Extensions;
using Cms.Domain.Mappers;
using Cms.Domain.Util;
using Cms.Model.Models.Params;
using Cms.Model.Models.SecTerm;
using SolrNet;
using SolrNet.Commands.Parameters;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SolrSearchTypeahead = Bbb.Core.Solr.Model.SearchTypeahead;

namespace Cms.Domain.Services.Solr
{
    public interface ISolrSearchTypeaheadService
    {
        Task UpdateAlias(AddEditAliasParams addEditAliasParams);
        Task DeleteAlias(AddEditAliasParams addEditAliasParams);
        Task UpdateCategory(SecTerm secTerm);
        Task<IList<SolrSearchTypeahead>> PaginateCategories(PaginateCategoryParams paginateCategoryParams);

        Task<IList<SolrSearchTypeahead>> SuggestBusinesses(string searchText);
        Task<SolrQueryResults<SolrSearchTypeahead>> GetPrimaryCategoryTobByTobIdAsync(string tobId, string cultureInfo);
    }

    public class SolrSearchTypeaheadService : ISolrSearchTypeaheadService
    {
        private static IReadOnlyDictionary<string, string> _solrCulturesByCountry = new Dictionary<string, string>
        {
            { "USA", "en-US" },
            { "CAN", "en-CA" },
            { "MEX", "es-MX" }
        };

        ISolrOperations<SolrSearchTypeahead> _solr;
        public SolrSearchTypeaheadService(ISolrOperations<SolrSearchTypeahead> solr)
        {
            _solr = solr;
        }

        public async Task UpdateAlias(AddEditAliasParams addEditAliasParams)
        {
            var staList = addEditAliasParams.ToSearchTypeaheadList();
            if (staList == null || staList.Count == 0) { return; }
            await PostSearchTypeaheadList(staList);
        }

        public async Task DeleteAlias(AddEditAliasParams addEditAliasParams)
        {
            var staList = addEditAliasParams.ToSearchTypeaheadList();
            if (staList == null || staList.Count == 0) { return; }
            await RemoveSearchTypeaheadList(staList);
        }

        public async Task UpdateCategory(SecTerm secTerm)
        {
            var staList = secTerm.ToSearchTypeaheadList();
            if (staList == null || staList.Count == 0) { return; }
            await PostSearchTypeaheadList(staList);
        }

        private IList<string> GetCulturesToQuery(IList<string> countries)
        {
            var countriesToQuery = !countries.IsNullOrEmpty()
                ? countries
                : new List<string>() { "USA" };

            var culturesToQuery = new List<string>();

            foreach (var country in countriesToQuery)
            {
                if (_solrCulturesByCountry.TryGetValue(country, out var culture) && !culturesToQuery.Contains(culture))
                {
                    culturesToQuery.Add(culture);
                }
            }

            return !culturesToQuery.IsNullOrEmpty()
                ? culturesToQuery
                : new List<string>() { "en-US" };
        }

        public async Task<IList<SolrSearchTypeahead>> PaginateCategories(PaginateCategoryParams paginateCategoryParams)
        {
            var cultures = GetCulturesToQuery(paginateCategoryParams.Countries);

            var q = new SolrQuery($"\"{paginateCategoryParams.FilterValue.ToSolrQuerySafeString()}\"") || QueryUtil.ExplodedTokenizedQuery(paginateCategoryParams.FilterValue).Boost(10);

            var opts = new QueryOptions();
            opts.Rows = paginateCategoryParams.PageSize;
            opts.RequestHandler = new RequestHandlerParameters("/suggest");
            opts.FilterQueries = new ISolrQuery[] {
                new SolrQueryInList(SolrSearchTypeahead.CultureInfoField, cultures),
                new SolrQuery($"{SolrSearchTypeahead.EntityTypeField}:Category"),
            };

            if (!paginateCategoryParams.ExcludedCategories.IsNullOrEmpty())
            {
                opts.AddFilterQueries(
                    !new SolrQueryInList(SolrSearchTypeahead.EntityIdField, paginateCategoryParams.ExcludedCategories));
            }

            if (paginateCategoryParams.QueryPrimaryOnly)
            {
                opts.AddFilterQueries(new SolrQuery($"{SolrSearchTypeahead.MetaTagsField}:primary"));
            }

            opts.ExtraParams = new Dictionary<string, string>
            {
                {  "tie", "1" }
            };

            return await _solr.QueryAsync(q, opts);
        }

        public async Task<IList<SolrSearchTypeahead>> SuggestBusinesses(string searchText)
        {
            var q = new SolrQuery($"\"{searchText.ToSolrQuerySafeString()}\"")
                || QueryUtil.ExplodedTokenizedQuery(searchText).Boost(10);

            var opts = new QueryOptions();
            opts.Rows = 10;
            opts.RequestHandler = new RequestHandlerParameters("/suggest");
            opts.FilterQueries = new ISolrQuery[] {
                new SolrQuery($"{SolrSearchTypeahead.EntityTypeField}:Organization")
            };
            opts.ExtraParams = new Dictionary<string, string>
            {
                {  "tie", "1" }
            };

            return await _solr.QueryAsync(q, opts);
        }

        public async Task<SolrQueryResults<SolrSearchTypeahead>> GetPrimaryCategoryTobByTobIdAsync(string tobId, string primaryCountry)
        {
            var cultureInfo = _solrCulturesByCountry.TryGetValue(primaryCountry, out var culture)
                ? culture
                : "en-US";
            var q = new SolrQuery($"{SolrSearchTypeahead.EntityIdField}:{tobId}");

            var opts = new QueryOptions();
            opts.Rows = 1;
            opts.FilterQueries = new ISolrQuery[]
            {
                new SolrQuery($"{SolrSearchTypeahead.EntityTypeField}:Category"),
                new SolrQuery($"{SolrSearchTypeahead.CultureInfoField}:{cultureInfo}"),
                new SolrQuery($"{SolrSearchTypeahead.MetaTagsField}:primary")
            };

            return await _solr.QueryAsync(q, opts);
        }

        #region Helper Methods
        private async Task PostSearchTypeaheadList(IList<SolrSearchTypeahead> searchTypeaheadList)
        {
            await _solr.AddRangeAsync(searchTypeaheadList);
            await _solr.CommitAsync();
        }

        private async Task RemoveSearchTypeaheadList(IList<SolrSearchTypeahead> searchTypeaheadList)
        {
            await _solr.DeleteAsync(searchTypeaheadList);
            await _solr.CommitAsync();
        }
        #endregion
    }
}
