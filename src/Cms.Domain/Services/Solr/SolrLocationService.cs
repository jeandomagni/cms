﻿using Bbb.Core.Solr.Enums;
using Bbb.Core.Solr.Search.Cbbb;
using Bbb.Core.Utilities.Extensions;
using SolrNet;
using SolrNet.Commands.Parameters;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SolrLocation = Bbb.Core.Solr.Model.Location;

namespace Cms.Domain.Services.Solr
{
    public interface ISolrLocationService
    {
        Task<IList<SolrLocation>> GetAllStateProvinces();
        Task<IList<SolrLocation>> PaginateLocations(string phrase, string context, string locationType, string[] bbbIds, string[] countryCodes, string userDefaultBbbId);
        Task<IList<SolrLocation>> SuggestCityStates(string input);
        Task<IList<SolrLocation>> GetSuggestedStateProvinces(string input);
        Task<IList<SolrLocation>> GetCitiesForBbb(string bbbId);
        Task<IList<string>> GetStatesForBbb(string bbbId);
        bool TryGetBbbIdByCityState(string city, string state, string country, out string bbbId);
        Task<List<SolrLocation>> GetCitiesForBbb(List<string> bbbIds);
        Task<SolrLocation> GetCity(string city, string state, object countryCode);
    }
    public class SolrLocationService : ISolrLocationService
    {
        private readonly ISolrOperations<SolrLocation> _solr;
        public SolrLocationService(
            ISolrOperations<SolrLocation> solr)
        {
            _solr = solr;
        }

        public async Task<IList<SolrLocation>> PaginateLocations(string phrase, string context, string locationType, string[] bbbIds, string[] countryCodes, string userDefaultBbbId)
        {
            var bbbIdsToQuery = !bbbIds.IsNullOrEmpty() 
                ? bbbIds.Where(bbbId => !string.IsNullOrEmpty(bbbId)) 
                : new[] { "*" };

            var extraParams = new Dictionary<string, string> {
                {"defType", "edismax"},
            };

            if (!string.IsNullOrWhiteSpace(userDefaultBbbId))
            {
                extraParams.Add("bq", $"{SolrLocation.BbbIdField}:{userDefaultBbbId}^2");
            }

            var queryOptions = new QueryOptions();
            queryOptions.Fields = new[] {
                SolrLocation.BbbIdField,
                SolrLocation.CityField,
                SolrLocation.CitySeoField,
                SolrLocation.CountryCodeField,
                SolrLocation.IdField,
                SolrLocation.StateField,
                SolrLocation.StateFullField,
                "score"
            };
            queryOptions.OrderBy = new[]
            {
                new SortOrder("score", Order.DESC),
                new SortOrder(SolrLocation.BusinessCountField, Order.DESC)
            };
            queryOptions.FilterQueries = new ISolrQuery[] {
                new SolrQueryByField(SolrLocation.LocationTypeField, locationType) &&
                new SolrQuery($"{{!parent which=\"suggest_phrase:*{phrase}*\"}}") &&
                new SolrQuery($"{{!parent which=\"suggest_phrase_context:{context}\"}}") &&
                new SolrMultipleCriteriaQuery(bbbIdsToQuery
                    .Select(bbbId => new SolrQuery($"{{!parent which=\"bbbId:({bbbId})\"}}"))
                    .ToArray(), "OR")
            };
            queryOptions.ExtraParams = extraParams;
            queryOptions.Rows = 10;

            var q = countryCodes.Length > 0
                    ? new SolrMultipleCriteriaQuery(countryCodes.Select(countryCode => new SolrQuery($"{SolrLocation.CountryCodeField}:{countryCode}")), "OR")
                    : SolrQuery.All;
            return await _solr.QueryAsync(q, queryOptions);
        }

        public async Task<IList<SolrLocation>> SuggestCityStates(string input)
        {
            var queryOptions = new QueryOptions();
            queryOptions.Fields = new []
            {
                SolrLocation.CityField,
                SolrLocation.CitySeoField,
                SolrLocation.StateField,
                SolrLocation.StateFullField,
                SolrLocation.CountryCodeField
            };
            queryOptions.FilterQueries = new ISolrQuery[] {
                new SolrQueryByField(SolrLocation.LocationTypeField, "CityState")
            };
            queryOptions.ExtraParams = new Dictionary<string, string> {
                {"defType", "edismax"},
                {"qf", $"{SolrLocation.DisplayTextField}^15 suggest_ngram suggest_phrase"}
            };
            queryOptions.Rows = 10;

            return await _solr.QueryAsync(
                new SolrQuery(input),
                queryOptions);
        }

        public async Task<IList<SolrLocation>> GetAllStateProvinces()
        {
            var q = SolrQuery.All;

            var queryOptions = new QueryOptions();
            queryOptions.Rows = 10000;
            queryOptions.OrderBy = new[]
            {
                new SortOrder(SolrLocation.StateField, Order.ASC)
            };
            queryOptions.Fields = new[]
            {
                SolrLocation.StateField,
                SolrLocation.StateFullField,
                SolrLocation.CountryCodeField,
            };
            queryOptions.Grouping = new GroupingParameters()
            {
                Fields = new[] { SolrLocation.StateField },
                Format = GroupingFormat.Grouped,
            };

            var solrResult = await _solr.QueryAsync(q, queryOptions);
            return solrResult?.Grouping?
                .Select(x => x.Value)?
                .SelectMany(x => x.Groups)?
                .SelectMany(x => x.Documents)?
                .ToList();
        }

        public async Task<IList<SolrLocation>> GetSuggestedStateProvinces(string input)
        {
            var q = new SolrQuery($"{input} OR {input}*");

            var opts = new QueryOptions();
            opts.OrderBy = new[]
            {
                new SortOrder("score", Order.DESC),
                new SortOrder(SolrLocation.StateField, Order.ASC)
            };
            opts.Fields = new[]
            {
                SolrLocation.StateField,
                SolrLocation.StateFullField,
                SolrLocation.CountryCodeField,
            };
            opts.ExtraParams = new Dictionary<string, string> {
                {"defType", "edismax"},
                {"qf", $"{SolrLocation.StateField}^100 {SolrLocation.StateFullField}"}
            };
            opts.Rows = 4;
            opts.Grouping = new GroupingParameters()
            {
                Fields = new[] { SolrLocation.StateField },
                Format = GroupingFormat.Grouped,
            };

            var solrResult = await _solr.QueryAsync(q, opts);
            return solrResult?.Grouping?
                .Select(x => x.Value)?
                .SelectMany(x => x.Groups)?
                .SelectMany(x => x.Documents)?
                .ToList();
        }

        public async Task<IList<SolrLocation>> GetCitiesForBbb(string bbbId)
        {
            var q = new SolrQuery($"{SolrLocation.BbbIdField}:{bbbId}");

            var queryOptions = new QueryOptions();
            queryOptions.Rows = 2; // We need to know we have only one location or >1 location related to bbb
            queryOptions.Fields = new[]
            {
                SolrLocation.CityField,
                SolrLocation.StateField,
                SolrLocation.IdField,
                SolrLocation.BbbIdField,
                SolrLocation.CountryCodeField
            };

            var solrResult = await _solr.QueryAsync(q, queryOptions);
            return solrResult.ToList();
        }

        public async Task<IList<string>> GetStatesForBbb(string bbbId)
        {
            var q = new SolrQuery($"{SolrLocation.BbbIdField}:{bbbId}");

            var queryOptions = new QueryOptions();
            queryOptions.Rows = int.MaxValue; 
            queryOptions.Fields = new[]
            {
                SolrLocation.StateField,
            };
            queryOptions.Grouping = new GroupingParameters()
            {
                Fields = new[] { SolrLocation.StateField },
                Format = GroupingFormat.Grouped
            };

            var solrResult = await _solr.QueryAsync(q, queryOptions);
            return solrResult?.Grouping?
                .Select(x => x.Value)?
                .SelectMany(x => x.Groups)?
                .SelectMany(x => x.Documents)?
                .Select(x => x.State)
                .Distinct()
                .ToList();
        }

        public bool TryGetBbbIdByCityState(string city, string state, string country, out string bbbId)
        {
            var builder = new LocationSearch();
            var location = builder.ExactOrBestPartialMatch($"{city}, {state}", System.Enum.Parse<CountryCode>(country));

            if (!string.IsNullOrWhiteSpace(location?.BbbIds?.FirstOrDefault()))
            {
                bbbId = location.BbbIds.FirstOrDefault();
                return true;
            }

            bbbId = null;
            return false;
        }

        public async Task<List<SolrLocation>> GetCitiesForBbb(List<string> bbbIds)
        {
            var q = new SolrQuery($"{SolrLocation.BbbIdField}:({string.Join(" OR ", bbbIds)})");

            var queryOptions = new QueryOptions();
            queryOptions.Rows = 1000000;
            queryOptions.Fields = new[]
            {
                SolrLocation.CityField,
                SolrLocation.StateField,
                SolrLocation.CountryCodeField
            };

            var solrResult = await _solr.QueryAsync(q, queryOptions);
            return solrResult.ToList();
        }

        public async Task<SolrLocation> GetCity(string city, string state, object countryCode)
        {
            var q = SolrQuery.All;

            var queryOptions = new QueryOptions();
            queryOptions.AddFilterQueries(new SolrQuery($"{SolrLocation.CityField}:{city}"));
            queryOptions.AddFilterQueries(new SolrQuery($"{SolrLocation.StateField}:{state}"));
            queryOptions.AddFilterQueries(new SolrQuery($"{SolrLocation.CountryCodeField}:{countryCode}"));
            queryOptions.Rows = 10;
            queryOptions.Fields = new[]
            {
                SolrLocation.CityField,
                SolrLocation.StateField,
                SolrLocation.CountryCodeField,
                SolrLocation.BbbIdField
            };

            var solrResult = await _solr.QueryAsync(q, queryOptions);
            return solrResult.FirstOrDefault(x => x.City.ToLower() == city.ToLower());
        }
    }
}
