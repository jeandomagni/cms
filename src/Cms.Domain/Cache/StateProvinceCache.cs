﻿using Cms.Model.Repositories;
using LazyCache;
using log4net;
using System;
using System.Reflection;
using System.Threading.Tasks;

namespace Cms.Domain.Cache
{
    public interface IStateProvinceCache
    {
        Task<string> GetCountryCodeForStateCodeAsync(string stateCode);
    }

    public class StateProvinceCache : IStateProvinceCache
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private IStateProvinceRepository _stateProvinceRepository;
        private IAppCache _cache;

        public StateProvinceCache(IAppCache cache, IStateProvinceRepository stateProvinceRepository)
        {
            _stateProvinceRepository = stateProvinceRepository;
            ItemExpiration = TimeSpan.FromHours(72).TotalMinutes;

            _cache = cache;
        }

        private string GenerateKey(string uniqueId, string methodName) => $"{MethodBase.GetCurrentMethod().DeclaringType.Name}-{methodName}-{uniqueId}".ToLowerInvariant();
        private DateTimeOffset GenerateExpiration() => DateTime.Now.AddMinutes(ItemExpiration);

        public async Task<string> GetCountryCodeForStateCodeAsync(string stateCode)
        {
            var key = GenerateKey(stateCode, MethodBase.GetCurrentMethod().Name);

            return await _cache.GetOrAddAsync(key, async () =>
            {
                try
                {
                    var countryCode = await _stateProvinceRepository.GetCountryCodeForStateCodeAsync(stateCode);

                    if (countryCode == null)
                    {
                        _log.Warn($"Invalid state code {stateCode}. Unable to get country code for {stateCode}.");
                        return "USA";
                    }

                    return countryCode;
                }
                catch (System.Exception ex)
                {
                    _log.Error($"Error while fetching stateCode for {nameof(stateCode)}={stateCode}", ex);
                }

                return "USA";
            }, GenerateExpiration());
        }

        public double ItemExpiration { get; }
    }
}
