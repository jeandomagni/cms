﻿using Bbb.Core.Utilities.Extensions;
using Cms.Domain.Enum;
using Cms.Domain.Model;
using Cms.Domain.Services;
using LazyCache;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Cms.Domain.Cache
{
    public interface ILegacyBbbSiteCache
    {
        Task<IList<LegacyBbbInfo>> GetAllAsync();
        Task<IList<LegacyBbbInfo>> GetAllForUserAsync(AppUser user);
        Task<IList<LegacyBbbInfo>> GetAllForUserLocationsAsync(AppUser user);
        Task<LegacyBbbInfo> GetByBbbIdAsync(string bbbId);
    }

    public class LegacyBbbSiteCache : ILegacyBbbSiteCache
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private ILegacyBbbInfoService _bbbInfoApiService;
        private IAppCache _cache;
        public LegacyBbbSiteCache(IAppCache cache, ILegacyBbbInfoService bbbInfoApiService)
        {
            _bbbInfoApiService = bbbInfoApiService;
            ItemExpiration = TimeSpan.FromHours(12).TotalMinutes;
            _cache = cache;
        }

        public double ItemExpiration { get; }
        private string GenerateKey(string uniqueId, string methodName) => $"{MethodBase.GetCurrentMethod().DeclaringType.Name}-{methodName}-{uniqueId}".ToLowerInvariant();
        private DateTimeOffset GenerateExpiration() => DateTime.Now.AddMinutes(ItemExpiration);

        public async Task<IList<LegacyBbbInfo>> GetAllAsync()
        {
            var key = GenerateKey("all-legacy-bbbs", MethodBase.GetCurrentMethod().Name);
            return await _cache.GetOrAddAsync(key, async () =>
            {
                return (await _bbbInfoApiService.GetAll()).ToList();
            }, GenerateExpiration());
        }

        public async Task<IList<LegacyBbbInfo>> GetAllForUserAsync(AppUser user)
        {
            var cachedBbbs = await GetAllAsync();

            var roles = user.Roles;
            if (roles.IsNullOrEmpty())
            {
                return null;
            }

            if (user.HasRole(UserRoles.GlobalAdmin))
            {
                return cachedBbbs;
            }

            var parsed = 0;

            return cachedBbbs
                .Where(site => roles.Select(role => role.SiteId)
                .Where(name => int.TryParse(name, out parsed))
                .Select(integer => parsed)
                .Contains(site.LegacySiteID))
                .OrderBy(site => site.BbbName)
                .ToList();
        }

        public async Task<IList<LegacyBbbInfo>> GetAllForUserLocationsAsync(AppUser user)
        {
            var cachedBbbs = await GetAllAsync();

            var roles = user.Roles;
            if (roles == null || !roles.Any())
            {
                return null;
            }

            if (user.HasRole(UserRoles.GlobalAdmin))
            {
                return cachedBbbs;
            }

            var parsedRoles = roles.Select(role =>
                int.TryParse(role.RoleName, out int parsedRole)
                    ? parsedRole
                    : 0);

           return cachedBbbs
                .Where(bbb => parsedRoles.Contains(bbb.LegacySiteID))
                .ToList();
        }

        public async Task<LegacyBbbInfo> GetByBbbIdAsync(string bbbId)
        {

            var cachedBbbs = await GetAllAsync();
            return cachedBbbs.FirstOrDefault(site => site.LegacyBBBID == bbbId);
        }
    }
}
