﻿using Cms.Domain.Helpers;
using Cms.Domain.Mappers;
using Cms.Domain.Services.Solr;
using Cms.Model.Constants;
using Cms.Model.Models.BbbInfo;
using LazyCache;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Cms.Domain.Cache
{
    public interface IBbbCache
    {
        Task<IList<BbbServiceArea>> GetAllServiceAreasAsync();
        Task<string> GetCultureInfoByBbbIdAsync(string bbbId);
    }

    public class BbbCache : IBbbCache
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        IBbbInfoMappers _bbbInfoMappers;
        private ISolrBbbInfoService _bbbInfoSolrService;
        private IAppCache _cache;
        public BbbCache(IAppCache cache, IBbbInfoMappers bbbInfoMappers, ISolrBbbInfoService bbbInfoSolrService)
        {
            _bbbInfoMappers = bbbInfoMappers;
            _bbbInfoSolrService = bbbInfoSolrService;
            ItemExpiration = TimeSpan.FromHours(12).TotalMinutes;

            _cache = cache;
        }

        public double ItemExpiration { get; }
        private string GenerateKey(string uniqueId, string methodName) => $"{MethodBase.GetCurrentMethod().DeclaringType.Name}-{methodName}-{uniqueId}".ToLowerInvariant();
        private DateTimeOffset GenerateExpiration() => DateTime.Now.AddMinutes(ItemExpiration);

        public async Task<IList<BbbServiceArea>> GetAllServiceAreasAsync()
        {
            var key = GenerateKey("all-bbbs", MethodBase.GetCurrentMethod().Name);
            return await _cache.GetOrAddAsync(key, async () =>
            {
                var solrBbbs = await _bbbInfoSolrService.GetAllAsync();
                return solrBbbs
                    .Select(_bbbInfoMappers.MapToBbbServiceArea)
                    .Where(x => x != null)
                    .ToList();
            }, GenerateExpiration());
        }

        public async Task<string> GetCultureInfoByBbbIdAsync(string bbbId)
        {
            var key = GenerateKey(bbbId, MethodBase.GetCurrentMethod().Name);

            return await _cache.GetOrAddAsync(key, async () =>
            {
                try
                {
                    var bbbInfo = await _bbbInfoSolrService.GetByBbbIdAsync(bbbId);

                    if (bbbInfo == null)
                    {
                        _log.Warn($"Invalid BBBID {bbbId}. Unable to get BbbInfoData for {bbbId}.");
                    }

                    return CultureHelpers.GetCultureInfo(bbbInfo.PrimaryCountry, bbbInfo.PrimaryLanguage);
                }
                catch (System.Exception ex)
                {
                    _log.Error($"Error while fetching BBB Info for {nameof(bbbId)}={bbbId}", ex);
                }

                return CultureConstants.EnUsCultureId;
            }, GenerateExpiration());
        }
    }
}
