﻿using Cms.Domain.Helpers;
using Cms.Domain.Services.Solr;
using Cms.Model.Repositories.SecTerms;
using LazyCache;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using SolrSearchTypeahead = Bbb.Core.Solr.Model.SearchTypeahead;

namespace Cms.Domain.Cache
{
    public interface ITobCache
    {
        Task<SolrSearchTypeahead> GetByIdAsync(string id, string primaryCountry);
        Task<IList<string>> GetHighRiskTobIdsAsync();
    }

    public class TobCache : ITobCache
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private ICategoryRepository _categoryRepository;
        private ISolrSearchTypeaheadService _solrService;
        private IAppCache _cache;
        public TobCache(IAppCache cache, ICategoryRepository categoryRepository, ISolrSearchTypeaheadService solrService)
        {
            _categoryRepository = categoryRepository;
            _solrService = solrService;
            ItemExpiration = TimeSpan.FromHours(24).TotalMinutes;

            _cache = cache;
        }

        public double ItemExpiration { get; }
        private string GenerateKey(string uniqueId, string methodName) => $"{MethodBase.GetCurrentMethod().DeclaringType.Name}-{methodName}-{uniqueId}".ToLowerInvariant();
        private DateTimeOffset GenerateExpiration() => DateTime.Now.AddMinutes(ItemExpiration);

        /// <summary>
        /// Using the tobId in the form of 89400-000|en-US, create category using secTerm and primaryCategory
        /// </summary>
        /// <param name="id">tob id</param>
        /// <param name="primaryCountry">primary country</param>
        /// <returns></returns>
        public async Task<SolrSearchTypeahead> GetByIdAsync(string id, string primaryCountry)
        {
            var key = GenerateKey(id, MethodBase.GetCurrentMethod().Name);
            return await _cache.GetOrAddAsync(key, async () =>
            {
                var result = await _solrService.GetPrimaryCategoryTobByTobIdAsync(id, primaryCountry);
                return result.FirstOrDefault();
            }, GenerateExpiration());
        }

        public async Task<IList<string>> GetHighRiskTobIdsAsync()
        {
            var key = GenerateKey("all", MethodBase.GetCurrentMethod().Name);
            return await _cache.GetOrAddAsync(key, async () =>
            {
                var tobIds = await _categoryRepository.GetAllHighRiskAndMonitoredTobIdsAsync();
                return tobIds.ToList();
            }, GenerateExpiration());
        }
    }
}
