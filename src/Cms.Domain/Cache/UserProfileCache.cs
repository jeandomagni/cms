﻿using Cms.Model.Models.User;
using LazyCache;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Cms.Domain.Cache
{
    public interface IUserProfileCache
    {
        void RemoveProfile(string userId);
        void AddProfile(UserProfile profile);
        UserProfile GetProfile(string userId);

    }
    public class UserProfileCache : IUserProfileCache
    {
        private readonly IAppCache _cache;

        public UserProfileCache(IAppCache cache)
        {
            _cache = cache;
            ItemExpiration = TimeSpan.FromHours(12).TotalMinutes;
        }
        public double ItemExpiration { get; }
        private string GenerateProfileKey(string uniqueId, string methodName) => $"{MethodBase.GetCurrentMethod().DeclaringType.Name}-{methodName}-{uniqueId}".ToLowerInvariant();
        private DateTimeOffset GenerateExpiration() => DateTime.Now.AddMinutes(ItemExpiration);

        public UserProfile GetProfile(string userId)
        {
            return _cache.Get<UserProfile>(GenerateProfileKey(userId, nameof(GetProfile)));
        }

        public void AddProfile(UserProfile profile)
        {
            _cache.Add<UserProfile>(GenerateProfileKey(profile.UserId, nameof(GetProfile)), profile, GenerateExpiration());
        }

        public void RemoveProfile(string userId)
        {
            _cache.Remove(GenerateProfileKey(userId, nameof(GetProfile)));
        }
    }
}
