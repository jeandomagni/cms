﻿using Ardalis.GuardClauses;
using Cms.Domain.Exception;
using Cms.Model.Models.User;

namespace Cms.Domain.UserGuardClauses
{
    public static class UserGuardClauseExtensions
    {
        /// <summary>
        /// Throws a <see cref="MissingUserDefaultSiteException" /> if <see cref="userProfile.DefaultSite" /> is null.
        /// </summary>
        /// <param name="guardClause"></param>
        /// <param name="userProfile"></param>
        /// <exception cref="MissingUserDefaultSiteException"></exception>
        public static void MissingDefaultSite(this IGuardClause guardClause, UserProfile userProfile)
        {
            if (string.IsNullOrWhiteSpace(userProfile.DefaultSite))
            {
                throw new MissingUserDefaultSiteException();
            }
        }
    }
}
