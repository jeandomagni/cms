﻿using Ardalis.GuardClauses;
using Bbb.Core.Solr.Model;
using Cms.Domain.Exception;
using System.Collections.Generic;

namespace Cms.Domain.SolrGuardClauses
{
    public static class SolrGuardClauseExtensions
    {
        /// <summary>
        /// Throws a <see cref="SolrRecordUpdateException" /> if <see cref="solrResponse" /> is invalid.
        /// </summary>
        /// <param name="guardClause"></param>
        /// <param name="solrResponse"></param>
        /// <exception cref="SolrRecordUpdateException"></exception>
        public static void InvalidSolrResponse(this IGuardClause guardClause, List<string> solrResponse)
        {
            if (solrResponse?.Count == 0)
            {
                throw new SolrRecordUpdateException();
            }
        }
    }
}
