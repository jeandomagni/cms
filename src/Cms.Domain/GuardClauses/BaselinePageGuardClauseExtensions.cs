﻿using Ardalis.GuardClauses;
using Cms.Domain.Exception;
using Cms.Model.Models.BaselinePages;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cms.Domain.BaselinePageGuardClauses
{
    public static class BaselinePageGuardClauseExtensions
    {
        /// <summary>
        /// Throws a <see cref="RecordNotFoundException" /> if <see cref="baselinePage" /> is null.
        /// </summary>
        /// <param name="guardClause"></param>
        /// <param name="baselinePage"></param>
        /// <exception cref="RecordNotFoundException"></exception>
        public static void RecordNotFound<T>(this IGuardClause guardClause, T baselinePage)
        {
            if (baselinePage == null)
            {
                throw new RecordNotFoundException();
            }
        }

        public static void SegmentUrlDuplication(this IGuardClause guardClause, bool checkResult)
        {
            if(checkResult)
            {
                throw new DuplicateSegmentUrlException();
            }
        }

        public static void SegmentUrlNotFound(this IGuardClause guardClause, BaselinePage baselinePage)
        {
            if(string.IsNullOrWhiteSpace(baselinePage.UrlSegment))
            {
                throw new SegmentUrlNotFoundException();
            }
        }

        public static void RedirectUrlNotFound(this IGuardClause guardClause, BaselinePage baselinePage)
        {
            if (string.IsNullOrWhiteSpace(baselinePage.RedirectUrl))
            {
                throw new RedirectUrlNotFoundException();
            }
        }


        public static void SegmentUrlEqualToRedirectUrl(this IGuardClause guardClause, BaselinePage baselinePage)
        {
            if (baselinePage.RedirectUrl == baselinePage.UrlSegment)
            {
                throw new SegmentUrlEqualToRedirectUrlException();
            }
        }
    }
}
