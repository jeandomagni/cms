﻿using Ardalis.GuardClauses;
using Cms.Domain.Exception;
using Cms.Model.Models.SecTerm;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Cms.Domain.SecTermGuardClauses
{
    public static class SecTermGuardClauseExtensions
    {
        /// <summary>
        /// Throws a <see cref="EmptyAliasNameException" /> if <see cref="alias.Name" /> or <see cref="alias.Spanish"/> is null or empty,
        /// or a <see cref="AliasHasSpecialCharactersException" /> if <see cref="alias.Name" /> or <see cref="alias.Spanish"/> has special characters.
        /// </summary>
        /// <param name="guardClause"></param>
        /// <param name="alias"></param>
        /// <exception cref="EmptyAliasNameException"></exception>
        /// <exception cref="AliasHasSpecialCharactersException"></exception>
        public static void EmptyOrInvalidAliasName(this IGuardClause guardClause, TobAlias alias)
        {
            // We don't need to have both a Spanish name and and English name,
            // but we need at least one.
            if (string.IsNullOrWhiteSpace(alias.Name) && string.IsNullOrWhiteSpace(alias.Spanish))
            {
                throw new EmptyAliasNameException();
            }

            if (!string.IsNullOrWhiteSpace(alias.Name) && !Regex.IsMatch(alias.Name, @"^[a-zA-Z0-9 ]+$"))
            {
                throw new AliasHasSpecialCharactersException();
            }

            if (!string.IsNullOrWhiteSpace(alias.Spanish) && !Regex.IsMatch(alias.Spanish, @"^[a-zA-Z0-9 ]+$"))
            {
                throw new AliasHasSpecialCharactersException();
            }
        }

        /// <summary>
        /// Throws a <see cref="DuplicateAliasException" /> if <see cref="alias" /> is not null.
        /// </summary>
        /// <param name="guardClause"></param>
        /// <param name="alias"></param>
        /// <exception cref="DuplicateAliasException"></exception>
        public static void DuplicateAlias(this IGuardClause guardClause, TobAlias alias)
        {
            if (alias != null)
            {
                throw new DuplicateAliasException();
            }
        }

        /// <summary>
        /// Throws a <see cref="InvalidSecTermMetadataException" /> if a page title or meta description in the provided list is empty.
        /// </summary>
        /// <param name="metadataList"></param>
        /// <exception cref="InvalidSecTermMetadataException"></exception>
        public static void EmptyMetadata(this IGuardClause guardClause, IEnumerable<Metadata> metadataList)
        {
            foreach (var metadata in metadataList)
            {
                if (string.IsNullOrWhiteSpace(metadata.PageTitle))
                {
                    throw new InvalidSecTermMetadataException();
                }

                if (string.IsNullOrWhiteSpace(metadata.MetaDesc))
                {
                    throw new InvalidSecTermMetadataException();
                }
            }
        }
    }
}
