﻿using Ardalis.GuardClauses;
using Cms.Domain.Exception;
using Cms.Domain.GenericGuardClauses;
using Cms.Model.Models.Content;
using System.Linq;

namespace Cms.Domain.ContentAbListGuardClauses
{
    public static class ContentAbListGuardClauseExtensions
    {
        /// <summary>
        /// Throws a <see cref="EmptyAbLinkListException" /> if any link lists are empty,
        /// or a <see cref="InvalidTitleForAbLinkListException" /> if any link lists have invalid titles.
        /// </summary>
        /// <param name="guardClause"></param>
        /// <param name="contentAbList"></param>
        /// <exception cref="EmptyAbLinkListException"></exception>
        /// <exception cref="InvalidTitleForAbLinkListException"></exception>
        public static void InvalidLinkLists(this IGuardClause guardClause, ContentAbList contentAbList)
        {
            foreach (var linkList in contentAbList.BusinessLinkLists)
            {
                if (string.IsNullOrWhiteSpace(linkList.Title))
                {
                    throw new InvalidTitleForAbLinkListException();
                }

                //if (linkList.BusinessLinks == null || linkList.BusinessLinks.Count() == 0)
                //{
                //    throw new EmptyAbLinkListException();
                //}

                foreach (var link in linkList.BusinessLinks)
                {
                    Guard.Against.InvalidUrl(link.LinkUrl);
                }
            }
        }
    }
}
