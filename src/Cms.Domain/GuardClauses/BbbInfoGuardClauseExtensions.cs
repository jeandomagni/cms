﻿using Ardalis.GuardClauses;
using Cms.Domain.Enum;
using Cms.Domain.Exception;
using Cms.Domain.Model;
using Cms.Model.Models.BbbInfo;
using Cms.Model.Models.BbbInfo.Db;
using System.Linq;

namespace Cms.Domain.BbbInfoGuardClauses
{
    public static class BbbInfoGuardClauseExtensions
    {
        /// <summary>
        /// Throws a <see cref="InvalidRequestorException" /> if <see cref="user" /> is invalid.
        /// </summary>
        /// <param name="guardClause"></param>
        /// <param name="bbbInfo"></param>
        /// <param name="user"></param>
        /// <exception cref="InvalidRequestorException"></exception>
        public static void InvalidRequestor(this IGuardClause guardClause, BbbInfo bbbInfo, AppUser user)
        {
            if (!user.GlobalOrLocalAdmin  && !(user.AdminSites.Contains(bbbInfo.LegacyId) || bbbInfo.CreatedBy == user.Email))
            {
                throw new InvalidRequestorException();
            }
        }

        /// <summary>
        /// Throws a <see cref="InvalidRequestorException" /> if <see cref="user" /> is invalid.
        /// </summary>
        /// <param name="guardClause"></param>
        /// <param name="dbBbbInfo"></param>
        /// <param name="user"></param>
        /// <exception cref="InvalidRequestorException"></exception>
        public static void InvalidRequestor(this IGuardClause guardClause, DbBbbInfo dbBbbInfo, AppUser user)
        {
            if (!user.GlobalOrLocalAdmin && !(user.AdminSites.Contains(dbBbbInfo.LegacyId) || dbBbbInfo.CreatedBy == user.Email))
            {
                throw new InvalidRequestorException();
            }
        }

        /// <summary>
        /// Throws a <see cref="RecordNotFoundException" /> if <see cref="bbbInfo" /> is invalid.
        /// </summary>
        /// <param name="guardClause"></param>
        /// <param name="bbbInfo"></param>
        /// <exception cref="RecordNotFoundException"></exception>
        public static void RecordNotFound(this IGuardClause guardClause, BbbInfo bbbInfo)
        {
            if (bbbInfo == null)
            {
                throw new RecordNotFoundException();
            }
        }

        /// <summary>
        /// Throws a <see cref="InvalidAreaCodeException" /> if <see cref="areaCode" /> is invalid.
        /// </summary>
        /// <param name="guardClause"></param>
        /// <param name="areaCode"></param>
        /// <exception cref="InvalidAreaCodeException"></exception>
        public static void InvalidAreaCode(this IGuardClause guardClause, string areaCode)
        {
            const int areaCodeLength = 3;
            const int areaCodeAndPrefixLength = 6;

            if (areaCode == null)
            {
                throw new InvalidAreaCodeException(areaCode);
            }

            if (areaCode.Length != areaCodeLength && areaCode.Length != areaCodeAndPrefixLength)
            {
                throw new InvalidAreaCodeException(areaCode);
            }
        }
    }
}
