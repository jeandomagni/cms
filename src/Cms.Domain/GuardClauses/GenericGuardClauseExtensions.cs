﻿using Ardalis.GuardClauses;
using Cms.Domain.Exception;
using System;
using System.Collections.Generic;

namespace Cms.Domain.GenericGuardClauses
{
    public static class GenericGuardClauseExtensions
    {
        /// <summary>
        /// Throws an <see cref="InvalidUrlException" /> if <see cref="url" /> is invalid.
        /// </summary>
        /// <param name="guardClause"></param>
        /// <param name="url"></param>
        /// <param name="errorMessage"></param>
        /// <exception cref="InvalidUrlException"></exception>
        public static void InvalidUrl(this IGuardClause guardClause, string url, string errorMessage = null)
        {
            Uri.TryCreate(url, UriKind.Absolute, out var uriResult);
            if (uriResult == null || (uriResult.Scheme != "http" && uriResult.Scheme != "https"))
            {
                throw new InvalidUrlException(errorMessage ?? $"{url} is not a valid URL.");
            }
        }

        /// <summary>
        /// Throws an <see cref="ExternalUrlException" /> if <see cref="url" /> does not belong to a BBB domain.
        /// </summary>
        /// <param name="guardClause"></param>
        /// <param name="url"></param>
        /// <param name="errorMessage"></param>
        /// <exception cref="ExternalUrlException"></exception>
        public static void ExternalUrl(this IGuardClause guardClause, string url, string errorMessage = null)
        {
            var bbbDomains = new List<string>() {
                "bbb.org",
                "cbbbstage.com",
                "cbbbtest.com",
                "cbbbdev.com"
            };

            Uri.TryCreate(url, UriKind.Absolute, out var uriResult);
            if (uriResult != null && !bbbDomains.Contains(uriResult.Host.Replace("www.", "")))
            {
                throw new ExternalUrlException(errorMessage ?? $"External URLs are not allowed.");
            }
        }
    }
}
