﻿using Ardalis.GuardClauses;
using Bbb.Core.Utilities.Extensions;
using Cms.Domain.Enum;
using Cms.Domain.Exception;
using Cms.Domain.GenericGuardClauses;
using Cms.Domain.Model;
using Cms.Model.Models.Content;
using Cms.Model.Models.Content.Db;
using Cms.Model.Models.Recurrence;
using RecurrenceCalculator;
using System.Collections.Generic;
using System.Linq;

namespace Cms.Domain.ContentGuardClauses
{
    public static class ContentGuardClauseExtensions
    {
        /// <summary>
        /// Throws a <see cref="RecordNotFoundException" /> if <see cref="content" /> is null.
        /// </summary>
        /// <param name="guardClause"></param>
        /// <param name="content"></param>
        /// <exception cref="RecordNotFoundException"></exception>
        public static void RecordNotFound<T>(this IGuardClause guardClause, T content)
        {
            if (content == null)
            {
                throw new RecordNotFoundException();
            }
        }

        /// <summary>
        /// Throws a <see cref="TitleRequiredException" /> if <see cref="content.Title" /> is null or whitespace.
        /// </summary>
        /// <param name="guardClause"></param>
        /// <param name="content"></param>
        /// <exception cref="TitleRequiredException"></exception>
        public static void MissingTitle(this IGuardClause guardClause, Content content)
        {
            if (string.IsNullOrWhiteSpace(content.Title))
            {
                throw new TitleRequiredException();
            }
        }

        /// <summary>
        /// Throws a <see cref="LayoutRequiredException" /> if <see cref="content.Layout" /> is null or whitespace.
        /// </summary>
        /// <param name="guardClause"></param>
        /// <param name="content"></param>
        /// <exception cref="LayoutRequiredException"></exception>
        public static void MissingLayout(this IGuardClause guardClause, Content content)
        {
            if (string.IsNullOrWhiteSpace(content.Layout))
            {
                throw new LayoutRequiredException();
            }
        }

        /// <summary>
        /// Throws a <see cref="InvalidContentLayoutException" /> if <see cref="content.Layout" /> is invalid.
        /// </summary>
        /// <param name="guardClause"></param>
        /// <param name="content"></param>
        /// <exception cref="InvalidContentLayoutException"></exception>
        public static void InvalidLayout(this IGuardClause guardClause, Content content)
        {
            if (!ContentLayout.IsValidLayout(content.Layout, content.Type))
            {
                throw new InvalidContentLayoutException();
            }
        }

        /// <summary>
        /// Throws a <see cref="LayoutRequiredException" /> if <see cref="content.Layout" /> is null or whitespace,
        /// or a <see cref="InvalidContentLayoutException" /> if <see cref="content.Layout" /> is invalid.
        /// </summary>
        /// <param name="guardClause"></param>
        /// <param name="content"></param>
        /// <exception cref="LayoutRequiredException"></exception>
        /// <exception cref="InvalidContentLayoutException"></exception>
        public static void MissingOrInvalidLayout(this IGuardClause guardClause, Content content)
        {
            Guard.Against.MissingLayout(content);
            Guard.Against.InvalidLayout(content);
        }

        /// <summary>
        /// Throws a <see cref="ContentTypeRequiredException" /> if <see cref="content.Type" /> is null or whitespace.
        /// </summary>
        /// <param name="guardClause"></param>
        /// <param name="content"></param>
        /// <exception cref="ContentTypeRequiredException"></exception>
        public static void MissingContentType(this IGuardClause guardClause, Content content)
        {
            if (string.IsNullOrWhiteSpace(content.Type))
            {
                throw new ContentTypeRequiredException();
            }
        }

        /// <summary>
        /// Throws a <see cref="InvalidContentTypeException" /> if <see cref="content.Type" /> is invalid.
        /// </summary>
        /// <param name="guardClause"></param>
        /// <param name="content"></param>
        /// <exception cref="InvalidContentTypeException"></exception>
        public static void InvalidContentType(this IGuardClause guardClause, Content content)
        {
            if (!ContentType.IsValidContentType(content.Type))
            {
                throw new InvalidContentTypeException();
            }
        }

        /// <summary>
        /// Throws a <see cref="ContentTypeRequiredException" /> if <see cref="content.Type" /> is null or whitespace,
        /// or a <see cref="InvalidContentTypeException" /> if <see cref="content.Type" /> is invalid.
        /// </summary>
        /// <param name="guardClause"></param>
        /// <param name="content"></param>
        /// <exception cref="ContentTypeRequiredException"></exception>
        /// <exception cref="InvalidContentTypeException"></exception>
        public static void MissingOrInvalidContentType(this IGuardClause guardClause, Content content)
        {
            Guard.Against.MissingContentType(content);
            Guard.Against.InvalidContentType(content);
        }

        /// <summary>
        /// Throws a <see cref="InvalidFieldException" /> if <see cref="content.Priority" /> is invalid.
        /// </summary>
        /// <param name="guardClause"></param>
        /// <param name="content"></param>
        /// <exception cref="InvalidFieldException"></exception>
        public static void InvalidPriority(this IGuardClause guardClause, Content content)
        {
            if (content.Priority < 1 || content.Priority > 99)
            {
                throw new InvalidFieldException("Priority");
            }
        }

        /// <summary>
        /// Throws an exception if there are invalid fields.
        /// </summary>
        /// <param name="guardClause"></param>
        /// <param name="content"></param>
        /// <exception cref="TitleRequiredException"></exception>
        /// <exception cref="LayoutRequiredException"></exception>
        /// <exception cref="InvalidContentLayoutException"></exception>
        /// <exception cref="ContentTypeRequiredException"></exception>
        /// <exception cref="InvalidContentTypeException"></exception>
        /// <exception cref="InvalidFieldException"></exception>
        /// <exception cref="InvalidUrlException"></exception>
        /// <exception cref="ExternalUrlException"></exception>
        public static void InvalidUpdates(this IGuardClause guardClause, Content content)
        {
            Guard.Against.MissingTitle(content);
            Guard.Against.MissingOrInvalidLayout(content);
            Guard.Against.MissingOrInvalidContentType(content);
            Guard.Against.InvalidPriority(content);
            Guard.Against.InvalidRedirectUrl(content);
            Guard.Against.InvalidSeoCanonical(content);
        }

        /// <summary>
        /// Throws a <see cref="InvalidFieldException" /> if content type is changed to or from Baseline Page.
        /// </summary>
        /// <param name="guardClause"></param>
        /// <param name="storedContent"></param>
        /// <param name="content"></param>
        public static void BaselinePageTypeChangeForbidden(this IGuardClause guardClause, DbContent storedContent, Content content)
        {
            if(content.Type == ContentType.BaselinePage && content.Type != storedContent.Type)
            {
                throw new InvalidFieldException("BaselinePage type could not be changed from or to. BaselinePage type should be managed from admin page");
            }
        }

        /// <summary>
        /// Throws a <see cref="InvalidRequestorException" /> if <see cref="user" /> is invalid.
        /// </summary>
        /// <param name="guardClause"></param>
        /// <param name="content"></param>
        /// <exception cref="InvalidRequestorException"></exception>
        public static void InvalidRequestor(this IGuardClause guardClause, DbContent content, AppUser user)
        {
            if (!user.HasAccessToRecord(content.CreatedBy, content.OwnerSiteId))
            {
                throw new InvalidRequestorException();
            }
        }

        /// <summary>
        /// Throws a <see cref="InvalidRequestorException" /> if <see cref="user" /> is invalid.
        /// </summary>
        /// <param name="guardClause"></param>
        /// <param name="content"></param>
        /// <exception cref="InvalidRequestorException"></exception>
        public static void InvalidRequestor(this IGuardClause guardClause, Content content, AppUser user)
        {
            if (!user.HasAccessToRecord(content.CreatedBy, content.OwnerSiteId))
            {
                throw new InvalidRequestorException();
            }
        }

        /// <summary>
        /// Throws a <see cref="StringLengthLimitException" /> if <see cref="content.Title" /> length is invalid.
        /// </summary>
        /// <param name="guardClause"></param>
        /// <param name="content"></param>
        /// <exception cref="StringLengthLimitException"></exception>
        public static void InvalidHeadlineLength(this IGuardClause guardClause, Content content)
        {
            const int headlineMaxChars = 200;
            if (content.Title.Length > headlineMaxChars)
            {
                throw new StringLengthLimitException(
                    "Headline", headlineMaxChars.ToString());
            }
        }

        /// <summary>
        /// Throws a <see cref="MissingAuthorException" /> if <see cref="content.Authors" /> is invalid.
        /// </summary>
        /// <param name="guardClause"></param>
        /// <param name="content"></param>
        /// <exception cref="MissingAuthorException"></exception>
        public static void InvalidAuthors(this IGuardClause guardClause, Content content)
        {
            if (!string.Equals(content.Type, ContentType.NearMe) && (content.Authors == null || content.Authors.Count == 0))
            {
                throw new MissingAuthorException();
            }
        }

        /// <summary>
        /// Throws a <see cref="MissingCarouselException" /> if <see cref="images" /> are invalid.
        /// </summary>
        /// <param name="guardClause"></param>
        /// <param name="content"></param>
        /// <exception cref="MissingCarouselException"></exception>
        public static void InvalidCarousel(this IGuardClause guardClause, Content content, IEnumerable<ContentMedia> images)
        {
            if (content.Layout == ContentLayout.ContentImageCarousel)
            {
                var carousel = images.Where(i => !i.MainImage);
                if (!carousel.Any())
                {
                    throw new MissingCarouselException();
                }
            }
        }

        /// <summary>
        /// Throws a <see cref="MissingLocationTagException" /> if <see cref="content" /> is missing a location tag.
        /// </summary>
        /// <param name="guardClause"></param>
        /// <param name="content"></param>
        /// <exception cref="MissingLocationTagException"></exception>
        public static void MissingLocationTag(this IGuardClause guardClause, Content content)
        {
            if (content.Cities.IsNullOrEmpty()
                && content.States.IsNullOrEmpty()
                && content.Countries.IsNullOrEmpty()
                && content.Type != ContentType.Event
                && content.Type != ContentType.NearMe
                && content.Type != ContentType.BaselinePage)
            {
                throw new MissingLocationTagException();
            }
        }

        /// <summary>
        /// Throws a <see cref="CheckedOutContentException" /> if <see cref="content" /> is checked out by another user.
        /// </summary>
        /// <param name="guardClause"></param>
        /// <param name="content"></param>
        /// <exception cref="CheckedOutContentException"></exception>
        public static void CheckedOutContent(this IGuardClause guardClause, Content content, AppUser user)
        {
            if (content.CheckoutDate != null && content.CheckoutBy != user.Email)
            {
                throw new CheckedOutContentException();
            }
        }

        /// <summary>
        /// Throws a <see cref="InvalidRequestorException" /> if <see cref="user" /> does not
        /// have access to record and if <see cref="isInServiceArea" /> is false.
        /// </summary>
        /// <param name="guardClause"></param>
        /// <param name="isInServiceArea"></param>
        /// <param name="contentRecord"></param>
        /// <param name="user"></param>
        /// <exception cref="InvalidRequestorException"></exception>
        public static void InvalidRequestor(this IGuardClause guardClause, bool isInServiceArea, Content contentRecord, AppUser user)
        {
            var userHasUpdateAccess = user.HasAccessToRecord(
                contentRecord.CreatedBy, contentRecord.OwnerSiteId);

            if (!(isInServiceArea || userHasUpdateAccess))
            {
                throw new InvalidRequestorException();
            }
        }

        /// <summary>
        /// Throws a <see cref="InvalidEventRecurrenceException" /> if <see cref="eventRecurrence" />
        /// is not valid.
        /// </summary>
        /// <param name="guardClause"></param>
        /// <param name="eventRecurrence"></param>
        /// <exception cref="InvalidRequestorException"></exception>
        public static void InvalidEventRecurrence(this IGuardClause guardClause, EventRecurrence eventRecurrence)
        {
            var validate = new Calculator().ValidateRecurrence(eventRecurrence);

            if (!string.IsNullOrEmpty(validate))
            {
                throw new InvalidEventRecurrenceException(validate);
            }
        }

        /// <summary>
        /// Throws a <see cref="InvalidFieldException" /> if <see cref="content.RedirectUrl"/> is empty,
        /// or throws a <see cref="InvalidUrlException" /> if <see cref="content.RedirectUrl"/> is invalid.
        /// </summary>
        /// <param name="guardClause"></param>
        /// <param name="content"></param>
        /// <exception cref="InvalidFieldException"></exception>
        /// <exception cref="InvalidUrlException"></exception>
        public static void InvalidRedirectUrl(this IGuardClause guardClause, Content content)
        {
            if (content.HasRedirectUrl)
            {
                if (string.IsNullOrWhiteSpace(content.RedirectUrl))
                {
                    throw new InvalidFieldException("Redirect URL");
                }
                else
                {
                    Guard.Against.InvalidUrl(content.RedirectUrl);
                }

            }
        }

        /// <summary>
        /// Throws a <see cref="InvalidFieldException" /> if <see cref="content.SeoCanonical"/> is empty,
        /// or throws a <see cref="InvalidUrlException" /> if <see cref="content.SeoCanonical"/> is invalid,
        /// /// or throws a <see cref="ExternalUrlException" /> if <see cref="content.SeoCanonical"/> is external.
        /// </summary>
        /// <param name="guardClause"></param>
        /// <param name="content"></param>
        /// <exception cref="InvalidFieldException"></exception>
        /// <exception cref="InvalidUrlException"></exception>
        /// <exception cref="ExternalUrlException"></exception>
        public static void InvalidSeoCanonical(this IGuardClause guardClause, Content content)
        {
            if (content.HasSeoCanonical)
            {
                if (string.IsNullOrWhiteSpace(content.SeoCanonical))
                {
                    throw new InvalidFieldException("SEO Canonical URL");
                }
                else
                {
                    Guard.Against.InvalidUrl(content.SeoCanonical);
                    Guard.Against.ExternalUrl(content.SeoCanonical);
                }

            }
        }
    }
}
