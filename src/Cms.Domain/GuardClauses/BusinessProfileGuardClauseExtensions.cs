﻿using Ardalis.GuardClauses;
using Bbb.Core.Solr.Model;
using Cms.Domain.Exception;

namespace Cms.Domain.BusinessProfileGuardClauses
{
    public static class BusinessProfileGuardClauseExtensions
    {
        /// <summary>
        /// Throws a <see cref="RecordNotFoundException" /> if <see cref="cibrDoc" /> is invalid.
        /// </summary>
        /// <param name="guardClause"></param>
        /// <param name="cibrDoc"></param>
        /// <exception cref="RecordNotFoundException"></exception>
        public static void RecordNotFound(this IGuardClause guardClause, Cibr cibrDoc)
        {
            if (cibrDoc == null)
            {
                throw new RecordNotFoundException();
            }
        }

        /// <summary>
        /// Throws a <see cref="RecordNotFoundException" /> if <see cref="id" /> is invalid.
        /// </summary>
        /// <param name="guardClause"></param>
        /// <param name="id"></param>
        /// <exception cref="RecordNotFoundException"></exception>
        public static void InvalidId(this IGuardClause guardClause, string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new RecordNotFoundException();
            }

            if (!id.Contains("-"))
            {
                throw new RecordNotFoundException();
            }
        }
    }
}
