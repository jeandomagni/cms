﻿using Ardalis.GuardClauses;
using Cms.Domain.Exception;
using Cms.Model.Models.Content;
using System.Linq;

namespace Cms.Domain.ContentReviewGuardClauses
{
    public static class ContentReviewGuardClauseExtensions
    {
        /// <summary>
        /// Throws a <see cref="GeneralApiExceptions" /> if <see cref="request.ContentReview" /> is null.
        /// </summary>
        /// <param name="guardClause"></param>
        /// <param name="request"></param>
        /// <exception cref="GeneralApiExceptions"></exception>
        public static void MissingReviewRequest(this IGuardClause guardClause, ReviewRequest request)
        {
            if (request?.ContentReview == null)
            {
                throw new GeneralApiException();
            }
        }

        /// <summary>
        /// Throws a <see cref="InvalidContentStatusException" /> if <see cref="request.NewStatus" /> is invalid.
        /// </summary>
        /// <param name="guardClause"></param>
        /// <param name="request"></param>
        /// <exception cref="InvalidContentStatusException"></exception>
        public static void InvalidReviewStatus(this IGuardClause guardClause, ReviewRequest request)
        {
            if (!new[] { "approved", "rejected" }.Contains(
                request.NewStatus))
            {
                throw new InvalidContentStatusException();
            }
        }

        /// <summary>
        /// Throws a <see cref="RecordNotFoundException" /> if <see cref="contentReview" /> is null.
        /// </summary>
        /// <param name="guardClause"></param>
        /// <param name="contentReview"></param>
        /// <exception cref="RecordNotFoundException"></exception>
        public static void RecordNotFound(this IGuardClause guardClause, ContentReview contentReview)
        {
            if (contentReview == null)
            {
                throw new RecordNotFoundException();
            }
        }
    }
}
